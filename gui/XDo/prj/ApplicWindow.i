! UIMX ascii 2.0 key: 5856                                                      
*action.HelpACT: {\
/* char *s;\
 s = XmTextGetSelection(UxWidget); \
 \
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
*/}
*action.WriteHelp: {\
char s[100],comm[160]  ;\
extern char print_com[];\
Widget sw = UxWidget;\
\
strcpy(s,"");\
/*sprintf(comm,"WRITE/OUT here");\
AppendDialogText(comm);*/\
 \
if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {\
   strcpy(s,"Select the OST columns to be displayed into the scrolled window");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   } \
else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {\
   strcpy(s,"Activate interface for defining classification rules ");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }  \
else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {\
   strcpy(s,"Activate interface for classifying images");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {\
   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }\
else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {\
   strcpy(s,"Create the OST table");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {\
   strcpy(s,"Popdown this interface");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ \
   strcpy(s,"Name of the Observation Summary Table to be created ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ \
   strcpy(s,"List of frames to be processed ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }  \
  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  \
   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  \
   strcpy(s,"Push button to popup the File Selection Box");     \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  \
        strcpy(s,"Push button to popup the File Selection Box");  \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   } \
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("SHelp"),"");\
}
*action.SelectCommand: {\
\
 \
\
}
*action.nothing: {\
printf("toto:\n");\
}
*action.ChangeFor: {\
extern  swidget attri;\
extern int tidost;\
extern int  collabchan;\
char mylab[1+TBL_LABLEN];\
char *text,form[1+TBL_LABLEN];\
int dummy;\
text = XmTextGetSelection(UxGetWidget(UxFindSwidget("scrollabel")));\
if (text != NULL) strcpy(mylab,text);\
XtFree(text);\
TCLSER(tidost,mylab,&collabchan);\
TCFGET(tidost,collabchan,form,&dummy,&dummy);\
UxPutText(UxFindSwidget("newform"),form);\
UxPopupInterface(attri,no_grab);\
}
*action.HInit: {\
\
}

*translation.table: HelpTable
*translation.parent: ApplicWindow
*translation.policy: replace
*translation.<Btn1Up>: HelpACT()
*translation.<Btn1Down>: grab-focus()
*translation.<EnterWindow>: WriteHelp()
*translation.<LeaveWindow>: ClearHelp()

*translation.table: TextTable
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Up> : SelectCommand()

*translation.table: transTable1
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<PtrMoved>: nothing()

*translation.table: ChangeAttr
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Up>: ChangeFor()
*translation.<Btn1Down>: grab-focus()

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: false
*ApplicWindow.gbldecl: #include <ExternResources.h>\
#include <stdio.h>\
#include <Xm/Protocols.h>\
\
#include <Xm/Text.h>\
#include <Xm/LabelG.h>\
#include <midas_def.h>\
#include <tbldef.h>\
extern swidget scrolledtable;\
extern Widget sb;\
extern void myscrollv(),myscrollh();\
\
\
/* Definitions for History mechanism */\
\
#define SIZHIST 50\
#define SIZTEXT 40\
int  ItemNumber = 0, ItemPosition = 0;\
int  DispItem = 0; \
\
char ItemHistory[SIZHIST][SIZTEXT];\
char HistList[SIZHIST*SIZTEXT];\
\
Widget sb1,sb2,hbs;\
int height;\
\
/* Variables for SelectionArray */\
\
XmTextScanType Sarray[60];\
int            SarI = 0;\
\
\
/* Include for UxPut calls on the fileSelectionBox */\
\
void CreateWindowManagerProtocols();\
void ExitCB();\
void CreateSessionManagerProtocols();\
void SaveSessionCB();\
\
extern Boolean saving;  /* declared in file selection box declarations */\
extern swidget fileSelectionDialog;\
extern swidget fileSelectionBox;\
extern swidget exitDialog;\
extern swidget create_fileSelectionDialog();\
extern swidget create_exitDialog();
*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget popup_ApplicWindow()\

*ApplicWindow.funcname: popup_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<popup_ApplicWindow>(%)"
*ApplicWindow.icode:
*ApplicWindow.fcode: XtVaGetValues(UxGetWidget(UxFindSwidget("scrol2")),XmNverticalScrollBar,&sb1,NULL);\
XtVaGetValues(UxGetWidget(UxFindSwidget("scrol1")),XmNverticalScrollBar,&sb,NULL);\
/*UxUnmap(UxWidgetToSwidget(sb1));*/\
/*XtVaGetValues(sb,XmNmaximum,&height,NULL);\
printf("height:%d \n",height);*/\
/*UxPutMappedWhenManaged(UxWidgetToSwidget(sb1),"false");*/\
XtUnmapWidget(sb1);\
XtVaGetValues(UxGetWidget(UxFindSwidget("textWindow")),XmNhorizontalScrollBar,&sb2,NULL);\
XtUnmapWidget(sb2);\
XtVaGetValues(UxGetWidget(UxFindSwidget("textWindow")),XmNverticalScrollBar,&sb2,NULL);\
XtUnmapWidget(sb2);\
/*UxPutTranslations(UxWidgetToSwidget(sb),transTable1);*/\
/*XtAddCallback(sb,XmNvalueChangedCallback,myscrollv,XmVERTICAL);*/\
/*XtVaGetValues(UxGetWidget(UxFindSwidget("identlist")),XmNheight,&height,XmNvisibleItemCount,&count,NULL);\
itemsize = height/count;\
printf("size:%d,height: %d, count: %d",itemsize,height,count);*/\
\
\
XtRemoveAllCallbacks(sb,XmNdragCallback);\
XtRemoveAllCallbacks(sb,XmNvalueChangedCallback);\
XtRemoveAllCallbacks(sb,XmNincrementCallback);\
XtRemoveAllCallbacks(sb,XmNdecrementCallback);\
XtRemoveAllCallbacks(sb,XmNpageIncrementCallback);\
XtRemoveAllCallbacks(sb,XmNpageDecrementCallback);\
XtRemoveAllCallbacks(sb,XmNtoTopCallback);\
XtRemoveAllCallbacks(sb,XmNtoBottomCallback);\
\
XtAddCallback(sb,XmNdragCallback,myscrollv,NULL);\
XtAddCallback(sb,XmNincrementCallback,myscrollv,NULL);\
XtAddCallback(sb,XmNdecrementCallback,myscrollv,NULL);\
XtAddCallback(sb,XmNpageIncrementCallback,myscrollv,NULL);\
XtAddCallback(sb,XmNpageDecrementCallback,myscrollv,NULL);\
XtAddCallback(sb,XmNtoTopCallback,myscrollv,NULL);\
XtAddCallback(sb,XmNtoBottomCallback,myscrollv,NULL);\
\
XtVaGetValues(UxGetWidget(UxFindSwidget("scrol2")),XmNhorizontalScrollBar,&hbs,NULL);\
XtAddCallback(hbs,XmNdragCallback,myscrollh,NULL);\
XtAddCallback(hbs,XmNincrementCallback,myscrollh,NULL);\
return(rtrn);
*ApplicWindow.auxdecl:
*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 550
*ApplicWindow.y: 577
*ApplicWindow.width: 700
*ApplicWindow.height: 680
*ApplicWindow.title: "XDo"
*ApplicWindow.deleteResponse: "do_nothing"
*ApplicWindow.keyboardFocusPolicy.source: public
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.iconName: "New"
*ApplicWindow.popupCallback: {\
\
}
*ApplicWindow.background: WindowBackground
*ApplicWindow.geometry: "700x540+30+30"

*mainWindow.class: mainWindow
*mainWindow.parent: ApplicWindow
*mainWindow.static: true
*mainWindow.name: mainWindow
*mainWindow.x: 200
*mainWindow.y: 180
*mainWindow.width: 700
*mainWindow.height: 680
*mainWindow.background: WindowBackground

*pullDownMenu.class: rowColumn
*pullDownMenu.parent: mainWindow
*pullDownMenu.static: true
*pullDownMenu.name: pullDownMenu
*pullDownMenu.borderWidth: 0
*pullDownMenu.menuHelpWidget: "pullDownMenu_top_b1"
*pullDownMenu.rowColumnType: "menu_bar"
*pullDownMenu.menuAccelerator: "<KeyUp>F10"
*pullDownMenu.background: MenuBackground
*pullDownMenu.foreground: MenuForeground

*quitPane.class: rowColumn
*quitPane.parent: pullDownMenu
*quitPane.static: true
*quitPane.name: quitPane
*quitPane.rowColumnType: "menu_pulldown"
*quitPane.background: MenuBackground
*quitPane.foreground: MenuForeground

*quitPane_b2.class: pushButtonGadget
*quitPane_b2.parent: quitPane
*quitPane_b2.static: true
*quitPane_b2.name: quitPane_b2
*quitPane_b2.labelString: "Bye"
*quitPane_b2.activateCallback: {\
char mycomm[200];\
int exist;\
exist = SCFINF("TAB_COMM.tbl",99,&exist);\
if (exist == 0) SCFDEL("TAB_COMM.tbl"); \
 \
\
exit();}
*quitPane_b2.fontList: BoldTextFont

*UtilsPane.class: rowColumn
*UtilsPane.parent: pullDownMenu
*UtilsPane.static: true
*UtilsPane.name: UtilsPane
*UtilsPane.rowColumnType: "menu_pulldown"
*UtilsPane.background: MenuBackground
*UtilsPane.foreground: MenuForeground

*UtilsPane_b5.class: pushButtonGadget
*UtilsPane_b5.parent: UtilsPane
*UtilsPane_b5.static: true
*UtilsPane_b5.name: UtilsPane_b5
*UtilsPane_b5.labelString: "Open ..."
*UtilsPane_b5.fontList: BoldTextFont
*UtilsPane_b5.activateCallback: {\
char s[1000];\
extern swidget klist,tablelist;\
extern int do8;\
int strip =1;\
strcpy(s,"*.tbl");\
SetFileList(UxGetWidget(tablelist),strip,s);\
do8 = 0;\
UxPutTitle(UxFindSwidget("transientShell8"),"Select OST Table");\
UxPopupInterface(klist,no_grab);\
}

*UtilsPane_b6.class: pushButtonGadget
*UtilsPane_b6.parent: UtilsPane
*UtilsPane_b6.static: true
*UtilsPane_b6.name: UtilsPane_b6
*UtilsPane_b6.labelString: "New ..."
*UtilsPane_b6.fontList: BoldTextFont
*UtilsPane_b6.activateCallback: {\
extern swidget crea;\
UxPopupInterface(crea,no_grab);\
}

*UtilsPane_b7.class: separator
*UtilsPane_b7.parent: UtilsPane
*UtilsPane_b7.static: true
*UtilsPane_b7.name: UtilsPane_b7

*UtilsPane_b9.class: pushButtonGadget
*UtilsPane_b9.parent: UtilsPane
*UtilsPane_b9.static: true
*UtilsPane_b9.name: UtilsPane_b9
*UtilsPane_b9.labelString: "Add ..."
*UtilsPane_b9.fontList: BoldTextFont
*UtilsPane_b9.activateCallback.source: public
*UtilsPane_b9.activateCallback: 
*UtilsPane_b9.sensitive: "false"

*HelpPane.class: rowColumn
*HelpPane.parent: pullDownMenu
*HelpPane.static: true
*HelpPane.name: HelpPane
*HelpPane.rowColumnType: "menu_pulldown"
*HelpPane.background: MenuBackground
*HelpPane.foreground: MenuForeground

*pullDownMenu_p3_b1.class: pushButtonGadget
*pullDownMenu_p3_b1.parent: HelpPane
*pullDownMenu_p3_b1.static: true
*pullDownMenu_p3_b1.name: pullDownMenu_p3_b1
*pullDownMenu_p3_b1.labelString: "On XDo..."
*pullDownMenu_p3_b1.fontList: BoldTextFont
*pullDownMenu_p3_b1.activateCallback: {\
extern swidget help;\
 \
display_help (help,"XDo.help"); \
}

*HelpPane_b2.class: pushButtonGadget
*HelpPane_b2.parent: HelpPane
*HelpPane_b2.static: true
*HelpPane_b2.name: HelpPane_b2
*HelpPane_b2.labelString: "On NTT defaults..."
*HelpPane_b2.fontList: BoldTextFont
*HelpPane_b2.activateCallback: {\
extern swidget help;\
display_help(help,"XDo_default.help");\
} 

*HelpPane_b3.class: pushButtonGadget
*HelpPane_b3.parent: HelpPane
*HelpPane_b3.static: true
*HelpPane_b3.name: HelpPane_b3
*HelpPane_b3.labelString: "On scrolled lists..."
*HelpPane_b3.fontList: BoldTextFont
*HelpPane_b3.activateCallback: { \
extern swidget help;\
display_help(help,"XDo_list.help");  \
}

*pullDownMenu_top_b1.class: cascadeButton
*pullDownMenu_top_b1.parent: pullDownMenu
*pullDownMenu_top_b1.static: true
*pullDownMenu_top_b1.name: pullDownMenu_top_b1
*pullDownMenu_top_b1.labelString: "Quit"
*pullDownMenu_top_b1.mnemonic: "Q"
*pullDownMenu_top_b1.subMenuId: "quitPane"
*pullDownMenu_top_b1.background: MenuBackground
*pullDownMenu_top_b1.fontList: BoldTextFont
*pullDownMenu_top_b1.foreground: MenuForeground

*commandsCascade.class: cascadeButton
*commandsCascade.parent: pullDownMenu
*commandsCascade.static: true
*commandsCascade.name: commandsCascade
*commandsCascade.labelString: "File"
*commandsCascade.subMenuId: "UtilsPane"
*commandsCascade.background: MenuBackground
*commandsCascade.fontList: BoldTextFont
*commandsCascade.foreground: MenuForeground
*commandsCascade.mnemonic: "F"
*commandsCascade.x: 101

*pullDownMenu_top_b5.class: cascadeButton
*pullDownMenu_top_b5.parent: pullDownMenu
*pullDownMenu_top_b5.static: true
*pullDownMenu_top_b5.name: pullDownMenu_top_b5
*pullDownMenu_top_b5.labelString: "Help"
*pullDownMenu_top_b5.subMenuId: "HelpPane"
*pullDownMenu_top_b5.mnemonic: "H"
*pullDownMenu_top_b5.background: MenuBackground
*pullDownMenu_top_b5.fontList: BoldTextFont
*pullDownMenu_top_b5.foreground: MenuForeground

*workAreaForm.class: form
*workAreaForm.parent: mainWindow
*workAreaForm.static: true
*workAreaForm.name: workAreaForm
*workAreaForm.width: 490
*workAreaForm.height: 440
*workAreaForm.borderWidth: 0
*workAreaForm.background: WindowBackground

*workAreaFrame.class: frame
*workAreaFrame.parent: workAreaForm
*workAreaFrame.static: true
*workAreaFrame.name: workAreaFrame
*workAreaFrame.x: 50
*workAreaFrame.y: 30
*workAreaFrame.width: 390
*workAreaFrame.height: 230
*workAreaFrame.bottomAttachment: "attach_form"
*workAreaFrame.bottomOffset.source: public
*workAreaFrame.bottomOffset: 0
*workAreaFrame.leftAttachment: "attach_form"
*workAreaFrame.leftOffset.source: public
*workAreaFrame.leftOffset: 0
*workAreaFrame.rightAttachment: "attach_form"
*workAreaFrame.rightOffset.source: public
*workAreaFrame.rightOffset: 0
*workAreaFrame.topAttachment: "attach_form"
*workAreaFrame.topOffset: 0
*workAreaFrame.background: WindowBackground

*workArea.class: form
*workArea.parent: workAreaFrame
*workArea.static: true
*workArea.name: workArea
*workArea.x: 2
*workArea.y: 2
*workArea.width: 698
*workArea.height: 448
*workArea.borderWidth: 0
*workArea.createCallback: {\
\
}
*workArea.mapCallback: {\
\
}
*workArea.background: WindowBackground
*workArea.borderColor: WindowBackground
*workArea.marginHeight: 5
*workArea.marginWidth: 5

*SHelp.class: text
*SHelp.parent: workArea
*SHelp.static: true
*SHelp.name: SHelp
*SHelp.x: 290
*SHelp.y: 337
*SHelp.width: 401
*SHelp.height: 40
*SHelp.background: SHelpBackground
*SHelp.foreground: TextForeground
*SHelp.highlightColor: ApplicBackground
*SHelp.fontList: TextFont
*SHelp.cursorPositionVisible: "false"
*SHelp.translations: ""
*SHelp.activateCallback: {\
char  *s1;\
char  *s2;\
extern char contxt[], midvers[];\
\
\
}
*SHelp.pendingDelete: "true"
*SHelp.blinkRate: 500
*SHelp.editMode: "single_line_edit"
*SHelp.highlightOnEnter: "true"
*SHelp.wordWrap: "false"
*SHelp.accelerators: ""
*SHelp.leftAttachment: "attach_form"
*SHelp.leftOffset: 5
*SHelp.rightAttachment: "attach_form"
*SHelp.rightOffset: 5
*SHelp.bottomAttachment: "attach_form"
*SHelp.bottomOffset: 125

*scrol2.class: scrolledWindow
*scrol2.parent: workArea
*scrol2.static: true
*scrol2.name: scrol2
*scrol2.scrollingPolicy: "application_defined"
*scrol2.x: 176
*scrol2.y: 55
*scrol2.visualPolicy: "variable"
*scrol2.scrollBarDisplayPolicy: "static"
*scrol2.shadowThickness: 0
*scrol2.rightAttachment: "attach_form"
*scrol2.rightOffset: -20
*scrol2.bottomAttachment: "attach_form"
*scrol2.bottomOffset: 184
*scrol2.leftAttachment: "attach_form"
*scrol2.leftOffset: 176
*scrol2.topAttachment: "attach_form"
*scrol2.topOffset: 35
*scrol2.scrollBarPlacement: "bottom_right"
*scrol2.background: WindowBackground
*scrol2.width: 481
*scrol2.mappedWhenManaged: "true"

*readtable.class: scrolledList
*readtable.parent: scrol2
*readtable.static: true
*readtable.name: readtable
*readtable.width: 521
*readtable.height: 250
*readtable.scrollBarDisplayPolicy: "static"
*readtable.listSizePolicy: "resize_if_possible"
*readtable.visibleItemCount: 16
*readtable.background: TextBackground
*readtable.selectionPolicy: "extended_select"
*readtable.browseSelectionCallback: {\
printf("browse:");\
}
*readtable.extendedSelectionCallback: {\
extern char fitschoice[30];\
XmListCallbackStruct *cbs;\
char *choice;\
int i;\
/*cbs = (XmListCallbackStruct *)UxCallbackArg;\
printf("reason:%d \n",cbs->reason);\
if (cbs->reason == XmCR_BROWSE_SELECT)\
   printf("browse\n");\
else if (cbs->reason == XmCR_MULTIPLE_SELECT)\
   printf("nmult \n");\
else if (cbs->reason == XmCR_DEFAULT_ACTION)\
   printf("default\n");\
if (cbs->reason == XmCR_EXTENDED_SELECT ){\
printf("noitem %d\n",cbs->selected_item_count);\
for (i=0; i<cbs->selected_item_count; i++) {\
   XmStringGetLtoR(cbs->selected_items[i],XmSTRING_DEFAULT_CHARSET,&choice);\
    printf("choice: %s \n",choice);\
   XtFree(choice);\
   }\
}\
else {\
   XmStringGetLtoR(cbs->item,XmSTRING_DEFAULT_CHARSET,&choice);\
    printf("choice:%s\n",choice);\
   XtFree(choice);\
   }\
\
 */    \
}
*readtable.fontList: BoldSmallFont
*readtable.foreground: TextForeground

*scrol1.class: scrolledWindow
*scrol1.parent: workArea
*scrol1.static: true
*scrol1.name: scrol1
*scrol1.scrollingPolicy: "application_defined"
*scrol1.x: 10
*scrol1.y: 10
*scrol1.visualPolicy: "variable"
*scrol1.scrollBarDisplayPolicy: "static"
*scrol1.shadowThickness: 0
*scrol1.height: 302
*scrol1.bottomAttachment: "attach_form"
*scrol1.bottomOffset: 184
*scrol1.leftAttachment: "attach_form"
*scrol1.rightAttachment: "attach_none"
*scrol1.topAttachment: "attach_form"
*scrol1.topOffset: 35
*scrol1.scrollBarPlacement: "bottom_right"
*scrol1.background: WindowBackground
*scrol1.leftOffset: 5

*identlist.class: scrolledList
*identlist.parent: scrol1
*identlist.static: true
*identlist.name: identlist
*identlist.width: 150
*identlist.height: 302
*identlist.topShadowColor: "Gray80"
*identlist.background: TextBackground
*identlist.scrollBarDisplayPolicy: "static"
*identlist.listSizePolicy: "constant"
*identlist.visibleItemCount: 16 
*identlist.selectionPolicy: "extended_select"
*identlist.extendedSelectionCallback: {\
XmListCallbackStruct *cbs;\
char *choice;\
extern char *colchoice;\
int i;\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
if (cbs->reason == XmCR_EXTENDED_SELECT ){\
\
for (i=0; i<cbs->selected_item_count; i++) {\
   XmStringGetLtoR(cbs->selected_items[i],XmSTRING_DEFAULT_CHARSET,&choice);\
    printf("choice: %s \n",choice);\
   XtFree(choice);\
   }\
}else {\
   XmStringGetLtoR(cbs->item,XmSTRING_DEFAULT_CHARSET,&choice);\
    printf("choice:%s\n",choice);\
   XtFree(choice);\
   }\
\
\
}
*identlist.fontList: BoldSmallFont
*identlist.traversalOn: "false"
*identlist.navigationType: "tab_group"
*identlist.ancestorSensitive: "true"
*identlist.foreground: TextForeground

*ColButton.class: pushButton
*ColButton.parent: workArea
*ColButton.static: true
*ColButton.name: ColButton
*ColButton.x: 110
*ColButton.y: 380
*ColButton.width: 90
*ColButton.height: 30
*ColButton.background: ButtonBackground
*ColButton.labelString: "Sel Cols.."
*ColButton.fontList: BoldTextFont
*ColButton.foreground: ButtonForeground
*ColButton.activateCallback: {\
extern swidget clist,myerror;\
extern  int tidost,colno;\
char text[80];\
if (tidost < 0) {\
        UxPutMessageString(UxFindSwidget("errorDialog1"),"No OST opened");\
        UxPopupInterface(myerror,no_grab); \
        return; \
         }\
\
populate(UxGetWidget(UxFindSwidget("columnlist1")));\
read_table_column(UxGetWidget(UxFindSwidget("columnlist")));\
    \
if (colno)  selectlist("COPY",UxGetWidget(UxFindSwidget("columnlist")));\
UxPopupInterface(clist,no_grab);\
}
*ColButton.recomputeSize: "false"
*ColButton.highlightColor: "Black"
*ColButton.bottomAttachment: "attach_form"
*ColButton.bottomOffset: 25
*ColButton.topAttachment: "attach_widget"
*ColButton.topOffset: 55
*ColButton.topWidget: "SHelp"
*ColButton.leftAttachment: "attach_form"
*ColButton.leftOffset: 20
*ColButton.leftWidget: ""

*textWindow.class: scrolledWindow
*textWindow.parent: workArea
*textWindow.static: true
*textWindow.name: textWindow
*textWindow.scrollingPolicy: "application_defined"
*textWindow.x: 220
*textWindow.y: 0
*textWindow.visualPolicy: "variable"
*textWindow.scrollBarDisplayPolicy: "static"
*textWindow.shadowThickness: 0
*textWindow.background: WindowBackground
*textWindow.borderColor: "black"
*textWindow.leftAttachment: "attach_form"
*textWindow.leftOffset: 182
*textWindow.rightAttachment: "attach_form"
*textWindow.rightOffset: -30
*textWindow.bottomAttachment: "attach_form"
*textWindow.bottomOffset: 360
*textWindow.topAttachment: "attach_form"
*textWindow.topOffset: 11
*textWindow.topShadowColor: "#a3a3a3"
*textWindow.highlightColor: WindowBackground
*textWindow.foreground: "black"
*textWindow.bottomShadowColor: "#707070"
*textWindow.topPosition: 0
*textWindow.height: 70

*scrollabel.class: scrolledText
*scrollabel.parent: textWindow
*scrollabel.static: true
*scrollabel.name: scrollabel
*scrollabel.width: 460
*scrollabel.height: 60
*scrollabel.editMode: "single_line_edit"
*scrollabel.editable: "false"
*scrollabel.background: WindowBackground
*scrollabel.borderColor: WindowBackground
*scrollabel.fontList: BoldSmallFont
*scrollabel.foreground: "Red"
*scrollabel.cursorPositionVisible: "false"
*scrollabel.rows: 1
*scrollabel.text: ""
*scrollabel.cursorPosition: 100
*scrollabel.marginHeight: 0
*scrollabel.highlightThickness: 0
*scrollabel.bottomShadowColor: "#707070"
*scrollabel.highlightColor: "grey"
*scrollabel.shadowThickness: 0
*scrollabel.autoShowCursorPosition: "false"
*scrollabel.topShadowColor: "#a3a3a3"
*scrollabel.marginWidth: 0
*scrollabel.highlightOnEnter: "false"
*scrollabel.selectionArray: "select_word select_word select_word select_word"
*scrollabel.translations: ChangeAttr

*IDENT.class: label
*IDENT.parent: workArea
*IDENT.static: true
*IDENT.name: IDENT
*IDENT.x: 0
*IDENT.y: 0
*IDENT.width: 210
*IDENT.height: 50
*IDENT.background: WindowBackground
*IDENT.leftAttachment: "attach_form"
*IDENT.leftOffset: -20
*IDENT.topAttachment: "attach_form"
*IDENT.topOffset: 0
*IDENT.fontList: BoldSmallFont
*IDENT.bottomAttachment: "attach_widget"
*IDENT.labelString: "IDENT"
*IDENT.bottomWidget: "scrol1"
*IDENT.rightAttachment: "attach_widget"
*IDENT.rightOffset: 0
*IDENT.rightWidget: "textWindow"
*IDENT.foreground: ButtonForeground

*RowButton.class: pushButton
*RowButton.parent: workArea
*RowButton.static: true
*RowButton.name: RowButton
*RowButton.x: 20
*RowButton.y: 380
*RowButton.width: 90
*RowButton.height: 30
*RowButton.background: ButtonBackground
*RowButton.labelString: "Sel Rows.."
*RowButton.foreground: ButtonForeground
*RowButton.fontList: BoldTextFont
*RowButton.activateCallback: {\
\
}
*RowButton.recomputeSize: "false"
*RowButton.leftAttachment: "attach_widget"
*RowButton.leftOffset: 10
*RowButton.topAttachment: "attach_widget"
*RowButton.topOffset: 55
*RowButton.topWidget: "SHelp"
*RowButton.highlightColor: "Black"
*RowButton.bottomAttachment: "attach_form"
*RowButton.bottomOffset: 25
*RowButton.leftWidget: "ColButton"
*RowButton.sensitive: "false"

*label9.class: label
*label9.parent: workArea
*label9.static: true
*label9.name: label9
*label9.x: 30
*label9.y: 390
*label9.width: 75
*label9.height: 30
*label9.background: WindowBackground
*label9.fontList: BoldTextFont
*label9.labelString: "Selection"
*label9.leftAttachment: "attach_form"
*label9.leftOffset: 80
*label9.bottomAttachment: "attach_form"
*label9.bottomOffset: 75

*separator6.class: separator
*separator6.parent: workArea
*separator6.static: true
*separator6.name: separator6
*separator6.x: 20
*separator6.y: 410
*separator6.width: 205
*separator6.height: 10
*separator6.background: WindowBackground
*separator6.leftAttachment: "attach_form"
*separator6.leftOffset: 15

*separator7.class: separator
*separator7.parent: workArea
*separator7.static: true
*separator7.name: separator7
*separator7.x: 25
*separator7.y: 420
*separator7.width: 205
*separator7.height: 10
*separator7.background: WindowBackground
*separator7.leftAttachment: "attach_form"
*separator7.leftOffset: 15
*separator7.bottomAttachment: "attach_form"
*separator7.bottomOffset: 10

*separator8.class: separator
*separator8.parent: workArea
*separator8.static: true
*separator8.name: separator8
*separator8.x: 220
*separator8.y: 410
*separator8.width: 10
*separator8.height: 70
*separator8.orientation: "vertical"
*separator8.background: WindowBackground
*separator8.bottomAttachment: "attach_form"
*separator8.bottomOffset: 15
*separator8.leftAttachment: "attach_form"
*separator8.leftOffset: 215

*separator9.class: separator
*separator9.parent: workArea
*separator9.static: true
*separator9.name: separator9
*separator9.x: 230
*separator9.y: 422
*separator9.width: 10
*separator9.height: 70
*separator9.orientation: "vertical"
*separator9.background: WindowBackground
*separator9.leftAttachment: "attach_form"
*separator9.leftOffset: 10
*separator9.bottomAttachment: "attach_form"
*separator9.bottomOffset: 15

*label10.class: label
*label10.parent: workArea
*label10.static: true
*label10.name: label10
*label10.x: 80
*label10.y: 407
*label10.width: 100
*label10.height: 30
*label10.background: WindowBackground
*label10.fontList: BoldTextFont
*label10.labelString: "Classification"
*label10.leftAttachment: "attach_form"
*label10.leftOffset: 305
*label10.rightAttachment: "attach_none"
*label10.bottomAttachment: "attach_form"
*label10.bottomOffset: 75

*separator10.class: separator
*separator10.parent: workArea
*separator10.static: true
*separator10.name: separator10
*separator10.x: 225
*separator10.y: 427
*separator10.width: 10
*separator10.height: 70
*separator10.orientation: "vertical"
*separator10.background: WindowBackground
*separator10.leftAttachment: "attach_form"
*separator10.leftOffset: 245
*separator10.bottomAttachment: "attach_form"
*separator10.bottomOffset: 15

*separator11.class: separator
*separator11.parent: workArea
*separator11.static: true
*separator11.name: separator11
*separator11.x: 25
*separator11.y: 492
*separator11.width: 205
*separator11.height: 10
*separator11.background: WindowBackground
*separator11.leftAttachment: "attach_form"
*separator11.leftOffset: 250
*separator11.bottomAttachment: "attach_form"
*separator11.bottomOffset: 10

*separator12.class: separator
*separator12.parent: workArea
*separator12.static: true
*separator12.name: separator12
*separator12.x: 25
*separator12.y: 420
*separator12.width: 205
*separator12.height: 10
*separator12.background: WindowBackground
*separator12.leftAttachment: "attach_form"
*separator12.leftOffset: 250
*separator12.topAttachment: "attach_form"
*separator12.topOffset: 410

*separator13.class: separator
*separator13.parent: workArea
*separator13.static: true
*separator13.name: separator13
*separator13.x: 270
*separator13.y: 427
*separator13.width: 10
*separator13.height: 70
*separator13.orientation: "vertical"
*separator13.background: WindowBackground
*separator13.leftAttachment: "attach_form"
*separator13.leftOffset: 450
*separator13.bottomAttachment: "attach_form"
*separator13.bottomOffset: 15

*AssoButton1.class: pushButton
*AssoButton1.parent: workArea
*AssoButton1.static: true
*AssoButton1.name: AssoButton1
*AssoButton1.x: 380
*AssoButton1.y: 442
*AssoButton1.width: 90
*AssoButton1.height: 30
*AssoButton1.background: ButtonBackground
*AssoButton1.labelString: "Associate.."
*AssoButton1.fontList: BoldTextFont
*AssoButton1.foreground: ButtonForeground
*AssoButton1.activateCallback: {\
extern swidget assohaupt;\
\
UxPopupInterface(assohaupt,no_grab);\
\
}
*AssoButton1.recomputeSize: "false"
*AssoButton1.highlightColor: "Black"
*AssoButton1.rightAttachment: "attach_form"
*AssoButton1.rightOffset: 70
*AssoButton1.topAttachment: "attach_widget"
*AssoButton1.topOffset: 55
*AssoButton1.topWidget: "SHelp"
*AssoButton1.bottomAttachment: "attach_form"
*AssoButton1.bottomOffset: 25

*ClassiButton.class: pushButton
*ClassiButton.parent: workArea
*ClassiButton.static: true
*ClassiButton.name: ClassiButton
*ClassiButton.x: 180
*ClassiButton.y: 330
*ClassiButton.width: 90
*ClassiButton.height: 30
*ClassiButton.background: ButtonBackground
*ClassiButton.labelString: "Rule.."
*ClassiButton.foreground: ButtonForeground
*ClassiButton.fontList: BoldTextFont
*ClassiButton.activateCallback: {\
extern void myrow(),never_called(),checkdigit(),checkchar();\
extern swidget classi,myerror;\
extern Widget rowrule[256][2]; \
extern int tidost;\
XmString str; \
Widget w,manage[512];\
int i,j ,dummy,ncol,type;\
static int rowrul = 0;\
char mylabel[TBL_LABLEN+1],mytext[8],mycomm[180];\
        UxPutText(UxFindSwidget("criteria"),"l01");\
\
if (tidost < 0) {\
        UxPutMessageString(UxFindSwidget("errorDialog1"),"No OST opened");\
        UxPopupInterface(myerror,no_grab); \
        return; \
         }\
 TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy); \
if (!rowrule[0][0]) { \
  XtAddCallback(UxGetWidget(UxFindSwidget("rowdescr")),XmNentryCallback,myrow,NULL);\
 rowrul = ncol;\
 XtVaSetValues(UxGetWidget(UxFindSwidget("rowdescr")),\
                XmNnumColumns,ncol,NULL);\
  \
 for (i=1; i<=ncol; i++) {\
     TCLGET(tidost,i,mylabel);\
     TCBGET(tidost,i,&type,&dummy,&dummy); \
     rowrule[i-1][0]   = XtVaCreateWidget(mylabel,xmLabelGadgetClass,\
                            UxGetWidget(UxFindSwidget("rowdescr")),\
                            NULL);\
     sprintf(mytext,"text_%d",i);\
     rowrule[i-1][1] = XtVaCreateWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowdescr")),\
                   NULL);\
     UxPutForeground(UxWidgetToSwidget(rowrule[i-1][1]),TextForeground);\
     UxPutBackground(UxWidgetToSwidget(rowrule[i-1][1]),WindowBackground);\
     UxPutFontList(UxWidgetToSwidget(rowrule[i-1][1]),BoldSmallFont);\
     if (type == D_C_FORMAT)\
        XtAddCallback(rowrule[i-1][1],XmNmodifyVerifyCallback,checkchar,NULL);\
     else\
        XtAddCallback(rowrule[i-1][1],XmNmodifyVerifyCallback,checkdigit,NULL);\
 \
      }\
XtManageChildren((WidgetList) rowrule, ncol * 2); \
}\
else { \
 \
  if (ncol > rowrul) { \
          XtVaSetValues(UxGetWidget(UxFindSwidget("rowdescr")),\
                XmNnumColumns,ncol,NULL);  \
          rowrul = ncol;\
         } \
  else if (ncol < rowrul) {\
        for (j=ncol; j< rowrul;j++) {\
             XmTextSetString(rowrule[j][1],""); \
             str = XmStringCreateSimple("");\
             XtVaSetValues (rowrule[j][0],  XmNlabelString,str ,\
                                 NULL);\
             XmStringFree(str);\
          }\
  }\
  for (i=1; i<=ncol; i++) { \
    TCLGET(tidost,i,mylabel);\
    if (!rowrule[i-1][0])  \
      rowrule[i-1][0] = XtVaCreateWidget(mylabel,\
                          xmLabelGadgetClass,\
                          UxGetWidget(UxFindSwidget("rowdescr")),\
                          NULL); \
    else  { \
           str = XmStringCreateSimple(mylabel);\
           XtVaSetValues (rowrule[i-1][0],XmNlabelString,str ,\
                                 NULL);\
           XmStringFree(str); \
          }\
 \
     if (!rowrule[i-1][1]) {  \
        sprintf(mytext,"text_%d",i);\
        rowrule[i-1][1] = XtVaCreateWidget(mytext, xmTextWidgetClass,\
        UxGetWidget(UxFindSwidget("rowdescr")),\
        XmNrightAttachment,XmATTACH_WIDGET, \
        XmNrightWidget,UxGetWidget(UxFindSwidget("scrolledWindow6")),\
        XmNleftAttachment,XmATTACH_POSITION,\
        XmNrightPosition,200,\
        NULL);\
     UxPutForeground(UxWidgetToSwidget(rowrule[i-1][1]),TextForeground);\
     UxPutBackground(UxWidgetToSwidget(rowrule[i-1][1]),TextBackground);\
     UxPutFontList(UxWidgetToSwidget(rowrule[i-1][1]),BoldSmallFont);\
        }\
     }\
   XtManageChildren((WidgetList) rowrule, ncol * 2);\
   }\
UxPutText(UxFindSwidget("criteria"),"");\
UxPopupInterface(classi,no_grab);\
\
}
*ClassiButton.recomputeSize: "false"
*ClassiButton.topAttachment: "attach_widget"
*ClassiButton.topOffset: 55
*ClassiButton.topWidget: "SHelp"
*ClassiButton.leftAttachment: "attach_widget"
*ClassiButton.leftOffset: 45
*ClassiButton.leftWidget: "RowButton"
*ClassiButton.highlightColor: "Black"
*ClassiButton.bottomAttachment: "attach_form"
*ClassiButton.bottomOffset: 25

*AssoButton.class: pushButton
*AssoButton.parent: workArea
*AssoButton.static: true
*AssoButton.name: AssoButton
*AssoButton.x: 110
*AssoButton.y: 380
*AssoButton.width: 90
*AssoButton.height: 30
*AssoButton.background: ButtonBackground
*AssoButton.labelString: "Classify.."
*AssoButton.fontList: BoldTextFont
*AssoButton.foreground: ButtonForeground
*AssoButton.activateCallback: {\
extern swidget clashaupt;\
Widget w;\
extern Widget row1[40],row2[40],row3[40];\
int  rows = 5;\
extern int rowno;\
int i;\
char mytext[8];\
   \
XtVaSetValues(UxGetWidget(UxFindSwidget("rowproc")),XmNnumColumns,rows,NULL);\
if (!row1[0]) {\
   rowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"text1_%d",i);\
   row1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowproc")),NULL);\
   sprintf(mytext,"text2_%d",i);\
   UxPutForeground(UxWidgetToSwidget(row1[i]),TextForeground);\
   UxPutBackground(UxWidgetToSwidget(row1[i]),TextBackground);\
   UxPutFontList(UxWidgetToSwidget(row1[i]),BoldSmallFont);\
   row2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowproc")),NULL);\
   UxPutForeground(UxWidgetToSwidget(row2[i]),TextForeground);\
   UxPutBackground(UxWidgetToSwidget(row2[i]),TextBackground);\
   UxPutFontList(UxWidgetToSwidget(row2[i]),BoldSmallFont);\
   sprintf(mytext,"text3_%d",i);\
   row3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowproc")),NULL);\
   UxPutForeground(UxWidgetToSwidget(row3[i]),TextForeground);\
   UxPutBackground(UxWidgetToSwidget(row3[i]),TextBackground);\
   UxPutFontList(UxWidgetToSwidget(row3[i]),BoldSmallFont);\
 }\
 \
}\
\
\
UxPopupInterface(clashaupt,no_grab);\
\
}
*AssoButton.recomputeSize: "false"
*AssoButton.highlightColor: "Black"
*AssoButton.bottomAttachment: "attach_form"
*AssoButton.bottomOffset: 25
*AssoButton.leftAttachment: "attach_widget"
*AssoButton.leftOffset: 10
*AssoButton.leftWidget: "ClassiButton"
*AssoButton.topAttachment: "attach_widget"
*AssoButton.topOffset: 55
*AssoButton.topWidget: "SHelp"

