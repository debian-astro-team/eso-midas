! UIMX ascii 2.0 key: 4767                                                      

*SelectInstrument.class: selectionBox
*SelectInstrument.parent: NO_PARENT
*SelectInstrument.defaultShell: topLevelShell
*SelectInstrument.static: true
*SelectInstrument.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
char *osmmget();\

*SelectInstrument.ispecdecl:
*SelectInstrument.funcdecl: swidget create_SelectInstrument()\

*SelectInstrument.funcname: create_SelectInstrument
*SelectInstrument.funcdef: "swidget", "<create_SelectInstrument>(%)"
*SelectInstrument.icode:
*SelectInstrument.fcode: return(rtrn);\

*SelectInstrument.auxdecl:
*SelectInstrument.name: SelectInstrument
*SelectInstrument.resizePolicy: "resize_none"
*SelectInstrument.unitType: "pixels"
*SelectInstrument.x: 400 
*SelectInstrument.y: 80 
*SelectInstrument.width: 260
*SelectInstrument.height: 320
*SelectInstrument.background: WindowBackground
*SelectInstrument.buttonFontList: BoldTextFont
*SelectInstrument.labelFontList: BoldTextFont
*SelectInstrument.textFontList: BoldTextFont
*SelectInstrument.cancelCallback: {\
extern swidget ilist;\
UxPopdownInterface(ilist);\
}
*SelectInstrument.mustMatch: "true"
*SelectInstrument.createCallback: {\
XmString *str;\
\
int item_no,i ;\
item_no = 2;\
\
/*str = (XmString *)XtMalloc(item_no * sizeof(XmString));\
for (i=0; i<item_no; i++)\
     str[i] = XmStringCreateSimple(instr[i]);\
\
XtVaSetValues(UxGetWidget(UxFindSwidget("SelectInstrument")),\
               XmNlistItems,str,\
               XmNlistItemCount,item_no,\
               NULL);*/\
}
*SelectInstrument.listLabelString: "Instrument"
*SelectInstrument.okCallback: {\
char *value;\
char title[30],descr[257];\
extern swidget ilist;\
extern int tidost,doI;\
int uni,null,actval,index;\
XmSelectionBoxCallbackStruct *cbs;\
cbs = (XmSelectionBoxCallbackStruct *)UxCallbackArg;\
XmStringGetLtoR(cbs->value,XmSTRING_DEFAULT_CHARSET,&value);\
\
if ( doI) {\
   UxPutText(UxFindSwidget("clas_t1"),value);\
   oscfill(descr,257,'\0');\
   SCDRDC(tidost,value,1,1,256,&actval,descr,&uni,&null);\
   index = strbskip(descr,' ');\
    descr[index+1] = '\0';\
   UxPutText(UxFindSwidget("clas_t1"),value);\
  UxPutText(UxFindSwidget("criteria"),descr);\
   redecomp(descr);\
   }\
else {\
   UxPutText(UxFindSwidget("text3"),value);\
   UxPopdownInterface(ilist);\
   }\
}

