! UIMX ascii 2.0 key: 1778                                                      

*SelectFrame.class: fileSelectionBox
*SelectFrame.parent: NO_PARENT
*SelectFrame.defaultShell: topLevelShell
*SelectFrame.static: true
*SelectFrame.gbldecl: #include <stdio.h>\
#include <midas_def.h>\
#include <ExternResources.h>\
\
extern void my_select();\
\
char *filechoice;\
int fno;
*SelectFrame.ispecdecl:
*SelectFrame.funcdecl: swidget create_SelectFrame()\

*SelectFrame.funcname: create_SelectFrame
*SelectFrame.funcdef: "swidget", "<create_SelectFrame>(%)"
*SelectFrame.icode: filechoice = (char * )0;
*SelectFrame.fcode: return(rtrn);\

*SelectFrame.auxdecl:
*SelectFrame.name: SelectFrame
*SelectFrame.resizePolicy: "resize_none"
*SelectFrame.unitType: "pixels"
*SelectFrame.x: 370
*SelectFrame.y: 330
*SelectFrame.width: 350
*SelectFrame.height: 400
*SelectFrame.createCallback: {\
Widget fl,tl;\
\
fl = XmFileSelectionBoxGetChild(UxGetWidget(UxFindSwidget("SelectFrame")),XmDIALOG_FILE_LIST);\
tl = XmFileSelectionBoxGetChild(UxGetWidget(UxFindSwidget("SelectFrame")),XmDIALOG_TEXT);\
XmTextSetString(tl,""); \
\
XtVaSetValues(fl,XmNselectionPolicy,XmEXTENDED_SELECT,NULL);\
XtAddCallback(fl,XmNextendedSelectionCallback,my_select,NULL);\
}
*SelectFrame.background: WindowBackground
*SelectFrame.labelFontList: BoldTextFont
*SelectFrame.textFontList: BoldTextFont
*SelectFrame.buttonFontList: BoldTextFont
*SelectFrame.bottomShadowColor: ButtonBackground
*SelectFrame.applyCallback: {\
/*XmFileSelectionBoxCallbackStruct *cbs;\
char *filename; \
XmStringGetLtoR(cbs->value,XmSTRING_DEFAULT_CHARSET,&filename);\
printf("Filename: %s \n",filename);\
XtFree(filename);*/\
}
*SelectFrame.textString: ""
*SelectFrame.directoryValid: "true"
*SelectFrame.okCallback: {\
Widget text;\
extern swidget flist;\
extern char *filechoice;\
\
int tid,col,i,j,len,kun;\
char name[200],*nm;\
text = XmFileSelectionBoxGetChild(UxWidget,XmDIALOG_TEXT);\
/*XmTextSetString(text,"File List copied into table sel_temp.tbl ");*/\
\
if (filechoice) {\
 TCTINI("sel_temp",F_IO_MODE,F_TRANS,1,fno,&tid);\
 TCCINI(tid,D_C_FORMAT,200,"A15"," ","FILE",&col);\
 len = 0 ;\
 for (i=0; i<fno; i++) {\
    j = strloc(filechoice +len,',');\
    filechoice[len+j] = '\0';\
    j++;\
    strcpy(name,filechoice+len);\
    \
    len += j;\
    TCEWRC(tid,i+1,col,name);\
    }\
 TCTCLO(tid);\
  UxPutText(UxFindSwidget("ost_t2"),"sel_temp.tbl");\
}\
else {\
 nm = XmTextGetString(text);\
 UxPutText(UxFindSwidget("ost_t2"),nm);\
 \
 SCKWRC("INPUTC",1,"lola",1,4,&kun);\
 }\
UxPopdownInterface(flist);     \
}
*SelectFrame.cancelCallback: {\
extern swidget flist;\
UxPopdownInterface(flist);\
}

