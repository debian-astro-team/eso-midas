! UIMX ascii 2.0 key: 1627                                                      

*Association.class: transientShell
*Association.parent: NO_PARENT
*Association.static: true
*Association.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\

*Association.ispecdecl:
*Association.funcdecl: swidget create_Association()\

*Association.funcname: create_Association
*Association.funcdef: "swidget", "<create_Association>(%)"
*Association.icode:
*Association.fcode: return(rtrn);\

*Association.auxdecl:
*Association.name: Association
*Association.x: 630
*Association.y: 170
*Association.width: 610
*Association.height: 540
*Association.title: "Association of Images"

*form5.class: form
*form5.parent: Association
*form5.static: true
*form5.name: form5
*form5.resizePolicy: "resize_none"
*form5.unitType: "pixels"
*form5.x: 0
*form5.y: 0
*form5.width: 610
*form5.height: 540
*form5.background: WindowBackground

*scrolledWindow5.class: scrolledWindow
*scrolledWindow5.parent: form5
*scrolledWindow5.static: true
*scrolledWindow5.name: scrolledWindow5
*scrolledWindow5.scrollingPolicy: "automatic"
*scrolledWindow5.x: 280
*scrolledWindow5.y: 20
*scrolledWindow5.width: 550
*scrolledWindow5.height: 215
*scrolledWindow5.scrollBarPlacement: "bottom_left"
*scrolledWindow5.background: WindowBackground
*scrolledWindow5.topAttachment: "attach_form"
*scrolledWindow5.topOffset: 80
*scrolledWindow5.leftAttachment: "attach_form"
*scrolledWindow5.leftOffset: 35
*scrolledWindow5.rightAttachment: "attach_form"
*scrolledWindow5.rightOffset: 35

*rowColumn2.class: rowColumn
*rowColumn2.parent: scrolledWindow5
*rowColumn2.static: true
*rowColumn2.name: rowColumn2
*rowColumn2.x: 8
*rowColumn2.y: -2
*rowColumn2.width: 520
*rowColumn2.height: 220
*rowColumn2.orientation: "horizontal"
*rowColumn2.packing: "pack_none"
*rowColumn2.background: WindowBackground
*rowColumn2.sensitive: "true"
*rowColumn2.resizeWidth: "false"
*rowColumn2.radioAlwaysOne: "true"
*rowColumn2.popupEnabled: "true"
*rowColumn2.traversalOn: "false"

*t_1.class: toggleButton
*t_1.parent: rowColumn2
*t_1.static: true
*t_1.name: t_1
*t_1.x: 0
*t_1.y: 15
*t_1.width: 40
*t_1.height: 30
*t_1.labelString: ""
*t_1.background: WindowBackground
*t_1.indicatorType: "one_of_many"
*t_1.selectColor: "PaleGreen"

*t1_1.class: text
*t1_1.parent: rowColumn2
*t1_1.static: true
*t1_1.name: t1_1
*t1_1.x: 40
*t1_1.y: 10
*t1_1.width: 80
*t1_1.height: 30
*t1_1.background: TextBackground
*t1_1.fontList: BoldTextFont
*t1_1.text: "BIAS"

*t2_1.class: text
*t2_1.parent: rowColumn2
*t2_1.static: true
*t2_1.name: t2_1
*t2_1.x: 150
*t2_1.y: 10
*t2_1.width: 80
*t2_1.height: 30
*t2_1.background: TextBackground
*t2_1.fontList: TextFont

*t_2.class: toggleButton
*t_2.parent: rowColumn2
*t_2.static: true
*t_2.name: t_2
*t_2.x: 0
*t_2.y: 60
*t_2.width: 40
*t_2.height: 30
*t_2.labelString: ""
*t_2.background: WindowBackground
*t_2.indicatorType: "one_of_many"
*t_2.selectColor: "PaleGreen"

*t_3.class: toggleButton
*t_3.parent: rowColumn2
*t_3.static: true
*t_3.name: t_3
*t_3.x: 0
*t_3.y: 105
*t_3.width: 40
*t_3.height: 30
*t_3.labelString: ""
*t_3.background: WindowBackground
*t_3.indicatorType: "one_of_many"
*t_3.selectColor: "PaleGreen"

*t_4.class: toggleButton
*t_4.parent: rowColumn2
*t_4.static: true
*t_4.name: t_4
*t_4.x: 0
*t_4.y: 150
*t_4.width: 40
*t_4.height: 30
*t_4.labelString: ""
*t_4.background: WindowBackground
*t_4.indicatorType: "one_of_many"
*t_4.selectColor: "PaleGreen"

*t_5.class: toggleButton
*t_5.parent: rowColumn2
*t_5.static: true
*t_5.name: t_5
*t_5.x: 0
*t_5.y: 195
*t_5.width: 40
*t_5.height: 30
*t_5.labelString: ""
*t_5.background: WindowBackground
*t_5.indicatorType: "one_of_many"
*t_5.selectColor: "PaleGreen"

*t_6.class: toggleButton
*t_6.parent: rowColumn2
*t_6.static: true
*t_6.name: t_6
*t_6.x: 0
*t_6.y: 240
*t_6.width: 40
*t_6.height: 30
*t_6.labelString: ""
*t_6.background: WindowBackground
*t_6.indicatorType: "one_of_many"
*t_6.selectColor: "PaleGreen"

*t1_2.class: text
*t1_2.parent: rowColumn2
*t1_2.static: true
*t1_2.name: t1_2
*t1_2.x: 40
*t1_2.y: 55
*t1_2.width: 80
*t1_2.height: 20
*t1_2.background: TextBackground
*t1_2.fontList: BoldTextFont
*t1_2.text: "DARK"

*t1_3.class: text
*t1_3.parent: rowColumn2
*t1_3.static: true
*t1_3.name: t1_3
*t1_3.x: 40
*t1_3.y: 100
*t1_3.width: 80
*t1_3.height: 20
*t1_3.background: TextBackground
*t1_3.fontList: BoldTextFont
*t1_3.text: "FF"

*t1_4.class: text
*t1_4.parent: rowColumn2
*t1_4.static: true
*t1_4.name: t1_4
*t1_4.x: 40
*t1_4.y: 145
*t1_4.width: 80
*t1_4.height: 20
*t1_4.background: TextBackground
*t1_4.fontList: BoldTextFont
*t1_4.text: "WCAL"

*t1_5.class: text
*t1_5.parent: rowColumn2
*t1_5.static: true
*t1_5.name: t1_5
*t1_5.x: 40
*t1_5.y: 190
*t1_5.width: 80
*t1_5.height: 20
*t1_5.background: TextBackground
*t1_5.fontList: BoldTextFont

*t1_6.class: text
*t1_6.parent: rowColumn2
*t1_6.static: true
*t1_6.name: t1_6
*t1_6.x: 40
*t1_6.y: 235
*t1_6.width: 80
*t1_6.height: 20
*t1_6.background: TextBackground
*t1_6.fontList: BoldTextFont

*t2_2.class: text
*t2_2.parent: rowColumn2
*t2_2.static: true
*t2_2.name: t2_2
*t2_2.x: 150
*t2_2.y: 55
*t2_2.width: 80
*t2_2.height: 30
*t2_2.background: TextBackground
*t2_2.fontList: TextFont

*t2_3.class: text
*t2_3.parent: rowColumn2
*t2_3.static: true
*t2_3.name: t2_3
*t2_3.x: 150
*t2_3.y: 100
*t2_3.width: 80
*t2_3.height: 30
*t2_3.background: TextBackground
*t2_3.fontList: TextFont

*t2_4.class: text
*t2_4.parent: rowColumn2
*t2_4.static: true
*t2_4.name: t2_4
*t2_4.x: 150
*t2_4.y: 145
*t2_4.width: 80
*t2_4.height: 30
*t2_4.background: TextBackground
*t2_4.fontList: TextFont

*t2_5.class: text
*t2_5.parent: rowColumn2
*t2_5.static: true
*t2_5.name: t2_5
*t2_5.x: 150
*t2_5.y: 190
*t2_5.width: 80
*t2_5.height: 30
*t2_5.background: TextBackground
*t2_5.fontList: TextFont

*t3_1.class: text
*t3_1.parent: rowColumn2
*t3_1.static: true
*t3_1.name: t3_1
*t3_1.x: 250
*t3_1.y: 10
*t3_1.width: 180
*t3_1.height: 30
*t3_1.background: TextBackground
*t3_1.fontList: TextFont

*p_1.class: pushButton
*p_1.parent: rowColumn2
*p_1.static: true
*p_1.name: p_1
*p_1.x: 460
*p_1.y: 5
*p_1.width: 90
*p_1.height: 30
*p_1.recomputeSize: "true"
*p_1.fillOnArm: "true"
*p_1.background: ButtonBackground
*p_1.labelString: " ... "
*p_1.activateCallback: {\
extern  swidget asso;\
\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
Widget tl;\
int rows = 5;\
int i;\
extern int arowno;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);\
\
if (!arow1[0]) {\
   arowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"a1_%d",i);\
   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);\
   sprintf(mytext,"a2_%d",i);\
   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a3_%d",i);\
   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a4_%d",i);\
   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   }\
 \
}\
\
UxPutText(UxFindSwidget("arule_name"),"bias_assorule");\
UxPopupInterface(asso,no_grab);\
}

*p_2.class: pushButton
*p_2.parent: rowColumn2
*p_2.static: true
*p_2.name: p_2
*p_2.x: 460
*p_2.y: 50
*p_2.width: 90
*p_2.height: 30
*p_2.recomputeSize: "true"
*p_2.fillOnArm: "true"
*p_2.background: ButtonBackground
*p_2.labelString: " ... "
*p_2.activateCallback: {\
\
\
extern  swidget asso;\
\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
Widget tl;\
int rows = 5;\
int i;\
extern int arowno;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);\
\
if (!arow1[0]) {\
   arowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"a1_%d",i);\
   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);\
   sprintf(mytext,"a2_%d",i);\
   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a3_%d",i);\
   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a4_%d",i);\
   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   }\
 \
}\
\
\
UxPutText(UxFindSwidget("arule_name"),"dark_assorule");\
UxPopupInterface(asso,no_grab);\
}

*t2_6.class: text
*t2_6.parent: rowColumn2
*t2_6.static: true
*t2_6.name: t2_6
*t2_6.x: 150
*t2_6.y: 235
*t2_6.width: 80
*t2_6.height: 30
*t2_6.background: TextBackground
*t2_6.fontList: TextFont

*t3_2.class: text
*t3_2.parent: rowColumn2
*t3_2.static: true
*t3_2.name: t3_2
*t3_2.x: 250
*t3_2.y: 55
*t3_2.width: 180
*t3_2.height: 30
*t3_2.background: TextBackground
*t3_2.fontList: TextFont

*t3_3.class: text
*t3_3.parent: rowColumn2
*t3_3.static: true
*t3_3.name: t3_3
*t3_3.x: 250
*t3_3.y: 100
*t3_3.width: 180
*t3_3.height: 30
*t3_3.background: TextBackground
*t3_3.fontList: TextFont

*t3_4.class: text
*t3_4.parent: rowColumn2
*t3_4.static: true
*t3_4.name: t3_4
*t3_4.x: 250
*t3_4.y: 145
*t3_4.width: 180
*t3_4.height: 30
*t3_4.background: TextBackground
*t3_4.fontList: TextFont

*t3_5.class: text
*t3_5.parent: rowColumn2
*t3_5.static: true
*t3_5.name: t3_5
*t3_5.x: 250
*t3_5.y: 190
*t3_5.width: 180
*t3_5.height: 30
*t3_5.background: TextBackground
*t3_5.fontList: TextFont

*t3_6.class: text
*t3_6.parent: rowColumn2
*t3_6.static: true
*t3_6.name: t3_6
*t3_6.x: 250
*t3_6.y: 235
*t3_6.width: 180
*t3_6.height: 30
*t3_6.background: TextBackground
*t3_6.fontList: TextFont

*p_3.class: pushButton
*p_3.parent: rowColumn2
*p_3.static: true
*p_3.name: p_3
*p_3.x: 460
*p_3.y: 95
*p_3.width: 90
*p_3.height: 30
*p_3.recomputeSize: "true"
*p_3.fillOnArm: "true"
*p_3.background: ButtonBackground
*p_3.labelString: " ... "
*p_3.activateCallback: {\
extern swidget asso;\
\
\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
Widget tl;\
int rows = 5;\
int i;\
extern int arowno;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);\
\
if (!arow1[0]) {\
   arowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"a1_%d",i);\
   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);\
   sprintf(mytext,"a2_%d",i);\
   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a3_%d",i);\
   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a4_%d",i);\
   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   }\
 \
}\
\
UxPutText(UxFindSwidget("arule_name"),"ff_assorule");\
UxPopupInterface(asso,no_grab);\
}

*p_4.class: pushButton
*p_4.parent: rowColumn2
*p_4.static: true
*p_4.name: p_4
*p_4.x: 460
*p_4.y: 140
*p_4.width: 90
*p_4.height: 30
*p_4.recomputeSize: "true"
*p_4.fillOnArm: "true"
*p_4.background: ButtonBackground
*p_4.labelString: " ... "
*p_4.activateCallback: {\
extern swidget asso;\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
Widget tl;\
int rows = 5;\
int i;\
extern int arowno;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);\
\
if (!arow1[0]) {\
   arowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"a1_%d",i);\
   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);\
   sprintf(mytext,"a2_%d",i);\
   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a3_%d",i);\
   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a4_%d",i);\
   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   }\
 \
}\
\
UxPutText(UxFindSwidget("arule_name"),"wcal_assorule");\
UxPopupInterface(asso,no_grab);\
}

*p_5.class: pushButton
*p_5.parent: rowColumn2
*p_5.static: true
*p_5.name: p_5
*p_5.x: 460
*p_5.y: 185
*p_5.width: 90
*p_5.height: 30
*p_5.recomputeSize: "true"
*p_5.fillOnArm: "true"
*p_5.background: ButtonBackground
*p_5.labelString: " ... "
*p_5.activateCallback: {\
extern swidget asso;extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
Widget tl;\
int rows = 5;\
int i;\
extern int arowno;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);\
\
if (!arow1[0]) {\
   arowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"a1_%d",i);\
   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);\
   sprintf(mytext,"a2_%d",i);\
   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a3_%d",i);\
   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a4_%d",i);\
   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   }\
 \
}\
\
\
UxPopupInterface(asso,no_grab);\
}

*p_6.class: pushButton
*p_6.parent: rowColumn2
*p_6.static: true
*p_6.name: p_6
*p_6.x: 460
*p_6.y: 230
*p_6.width: 90
*p_6.height: 30
*p_6.recomputeSize: "true"
*p_6.fillOnArm: "true"
*p_6.background: ButtonBackground
*p_6.labelString: " ... "
*p_6.activateCallback: {\
extern swidget asso;extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
Widget tl;\
int rows = 5;\
int i;\
extern int arowno;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);\
\
if (!arow1[0]) {\
   arowno = 5;\
  for (i=0; i<rows; i++) \
   {\
   sprintf(mytext,"a1_%d",i);\
   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);\
   sprintf(mytext,"a2_%d",i);\
   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a3_%d",i);\
   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   sprintf(mytext,"a4_%d",i);\
   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);\
   }\
 \
}\
\
\
UxPopupInterface(asso,no_grab);\
}

*label18.class: label
*label18.parent: form5
*label18.static: true
*label18.name: label18
*label18.x: 170
*label18.y: 300
*label18.width: 200
*label18.height: 50
*label18.background: WindowBackground
*label18.fontList: BoldTextFont
*label18.labelString: "Output Table"
*label18.topAttachment: "attach_form"
*label18.topOffset: 340
*label18.leftOffset: 30
*label18.leftAttachment: "attach_form"

*text4.class: text
*text4.parent: form5
*text4.static: true
*text4.name: text4
*text4.x: 230
*text4.y: 320
*text4.width: 210
*text4.height: 40
*text4.background: TextBackground
*text4.topAttachment: "attach_form"
*text4.topOffset: 345
*text4.leftOffset: 260

*text5.class: text
*text5.parent: form5
*text5.static: true
*text5.name: text5
*text5.x: 10
*text5.y: 450
*text5.width: 570
*text5.height: 40
*text5.background: SHelpBackground
*text5.rightAttachment: "attach_form"
*text5.rightOffset: 5
*text5.leftOffset: 5
*text5.leftAttachment: "attach_form"
*text5.topAttachment: "attach_form"
*text5.topOffset: 400

*pushButton1.class: pushButton
*pushButton1.parent: form5
*pushButton1.static: true
*pushButton1.name: pushButton1
*pushButton1.x: 170
*pushButton1.y: 460
*pushButton1.width: 90
*pushButton1.height: 40
*pushButton1.background: ButtonBackground
*pushButton1.fontList: BoldTextFont
*pushButton1.foreground: ButtonForeground
*pushButton1.labelString: "Apply"
*pushButton1.bottomAttachment: "attach_form"
*pushButton1.bottomOffset: 30
*pushButton1.leftAttachment: "attach_form"
*pushButton1.leftOffset: 100
*pushButton1.activateCallback: {\
\
extern char ostchoice[60];\
extern int tidost;\
extern swidget myerror;\
char command[160];\
char myw[4],cbuf[8],expo[8],nexpo[8],mt1[5],filename[60],oname[60];\
int i,status,loop;\
loop = 0;\
if (tidost == -1 ) {\
  UxPutMessageString(UxFindSwidget("errorDialog1"),"NO OST opened");\
  UxPopupInterface(myerror,no_grab);\
  return;\
}\
  \
for (i=0; i<6;i++) {\
   sprintf(myw,"t_%d",i+1);\
   strcpy(cbuf,UxGetSet(UxFindSwidget(myw)));\
   strcpy(command,"ASSOCIATE/IMAGE ");\
   strcpy(oname,UxGetText(UxFindSwidget("text4")));\
   if (oname[0] == '\0') {\
       UxPutMessageString(UxFindSwidget("errorDialog1"),"No Output Table Name provided");\
       UxPopupInterface(myerror,no_grab);\
       return;\
       }\
if (cbuf[0] == 't') {\
       sprintf(mt1,"t3_%d",i+1);\
       strcpy(filename,UxGetText(UxFindSwidget(mt1)));\
       sprintf(mt1,"t1_%d",i+1);\
       strcpy(expo,UxGetText(UxFindSwidget(mt1)));\
       sprintf(mt1,"t2_%d",i+1);\
       strcpy(nexpo,UxGetText(UxFindSwidget(mt1)));\
       if (nexpo[0] == '\0') strcpy(nexpo,"1");\
       if (loop == 0)\
       sprintf(command,"ASSOCIATE/IMAGE %s %s %s %s C %s",ostchoice,expo,filename,oname,nexpo);\
       else\
       sprintf(command,"ASSOCIATE/IMAGE %s %s %s %s A %s",ostchoice,expo,filename,oname,nexpo);\
       loop++;\
       status = AppendDialogText(command);\
       }\
}\
}

*pushButton7.class: pushButton
*pushButton7.parent: form5
*pushButton7.static: true
*pushButton7.name: pushButton7
*pushButton7.x: 180
*pushButton7.y: 470
*pushButton7.width: 90
*pushButton7.height: 40
*pushButton7.background: ButtonBackground
*pushButton7.fontList: BoldTextFont
*pushButton7.foreground: ButtonForeground
*pushButton7.labelString: "Cancel"
*pushButton7.bottomAttachment: "attach_form"
*pushButton7.bottomOffset: 30
*pushButton7.rightOffset: 100
*pushButton7.rightAttachment: "attach_form"
*pushButton7.activateCallback: {\
extern swidget assohaupt;\
UxPopdownInterface(assohaupt);\
}

*label20.class: label
*label20.parent: form5
*label20.static: true
*label20.name: label20
*label20.x: 40
*label20.y: 350
*label20.width: 550
*label20.height: 50
*label20.background: WindowBackground
*label20.fontList: BoldTextFont
*label20.labelString: "Exposure Type        No of Exposures          Rule Table" 
*label20.topAttachment: "attach_form"
*label20.topOffset: 20
*label20.leftAttachment: "attach_form"
*label20.leftOffset: 60
*label20.labelType: "string"
*label20.alignment: "alignment_beginning"

