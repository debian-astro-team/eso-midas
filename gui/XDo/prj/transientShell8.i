! UIMX ascii 2.0 key: 7503                                                      

*transientShell8.class: transientShell
*transientShell8.parent: NO_PARENT
*transientShell8.static: true
*transientShell8.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <midas_def.h>
*transientShell8.ispecdecl:
*transientShell8.funcdecl: swidget create_transientShell8()\

*transientShell8.funcname: create_transientShell8
*transientShell8.funcdef: "swidget", "<create_transientShell8>(%)"
*transientShell8.icode:
*transientShell8.fcode: return(rtrn);\

*transientShell8.auxdecl:
*transientShell8.name: transientShell8
*transientShell8.x: 330
*transientShell8.y: 300
*transientShell8.width: 290
*transientShell8.height: 353
*transientShell8.title: "Select OST table"

*form1.class: form
*form1.parent: transientShell8
*form1.static: true
*form1.name: form1
*form1.resizePolicy: "resize_none"
*form1.unitType: "pixels"
*form1.x: 0
*form1.y: 0
*form1.width: 290
*form1.height: 240
*form1.background: ApplicBackground

*scrolledWindow2.class: scrolledWindow
*scrolledWindow2.parent: form1
*scrolledWindow2.static: true
*scrolledWindow2.name: scrolledWindow2
*scrolledWindow2.scrollingPolicy: "application_defined"
*scrolledWindow2.x: 10
*scrolledWindow2.y: 10
*scrolledWindow2.visualPolicy: "variable"
*scrolledWindow2.scrollBarDisplayPolicy: "static"
*scrolledWindow2.shadowThickness: 0
*scrolledWindow2.bottomAttachment: "attach_form"
*scrolledWindow2.bottomOffset: 45
*scrolledWindow2.height: 316
*scrolledWindow2.leftAttachment: "attach_form"
*scrolledWindow2.leftOffset: 2
*scrolledWindow2.rightAttachment: "attach_form"
*scrolledWindow2.rightOffset: 2
*scrolledWindow2.topAttachment: "attach_form"
*scrolledWindow2.topOffset: 0
*scrolledWindow2.width: 270
*scrolledWindow2.background: ApplicBackground
*scrolledWindow2.scrollBarPlacement: "bottom_left"

*tablelist.class: scrolledList
*tablelist.parent: scrolledWindow2
*tablelist.static: false
*tablelist.name: tablelist
*tablelist.width: 190
*tablelist.height: 320
*tablelist.background: TextBackground
*tablelist.fontList: TextFont
*tablelist.foreground: TextForeground
*tablelist.scrollBarDisplayPolicy: "static"
*tablelist.browseSelectionCallback: {\
extern char ostchoice[60],claschoice[60],assochoice[60];\
extern char *colchoice; \
extern int do8,tidcomm,tidost;\
extern swidget klist,text3;\
XmListCallbackStruct *cbs;\
char *choice,type[2]  ;\
char mycomm[200],table_descr[60] ;\
int upper,unit,null,actval,dummy,uni;\
 \
cbs = (XmListCallbackStruct *)UxCallbackArg;\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
if (!do8) {\
   strcpy(ostchoice,choice);\
   XtFree(choice);\
   UxPopdownInterface(klist);\
   if (read_ost_table())\
   display_ident_table(UxGetWidget(UxFindSwidget("identlist")));\
 /*  XtVaGetValues(UxGetWidget(UxFindSwidget("readtable")),XmNitemCount,&upper,NULL);\
   if (upper != 0) XmListDeleteAllItems(UxGetWidget(UxFindSwidget("readtable")));*/\
SCDFND(tidost,"table_descr",type,&dummy,&dummy);\
TCTOPN("TAB_COMM",F_IO_MODE,&tidcomm); \
if (type[0] != ' ') {\
  SCDRDC(tidost ,"table_descr",1 ,1,60,&actval,table_descr,&uni ,&null);\
  TCEWRC(tidcomm,1,3,table_descr);\
  }\
TCEWRC(tidcomm,1,2,ostchoice);\
TCTCLO(tidcomm);\
 if (*colchoice)  \
 \
   display_col_table(UxGetWidget(UxFindSwidget("readtable")));\
  \
}\
else if (do8 == 1){\
     strcpy(claschoice,choice);\
     XtFree(choice);\
     UxPopdownInterface(klist);\
     read_clas_table();\
     }\
else {\
     strcpy(assochoice,choice);\
     XtFree(choice);\
     UxPopdownInterface(klist);\
     read_asso_table();\
     }\
}
*tablelist.visibleItemCount: 16

*pushButton2.class: pushButton
*pushButton2.parent: form1
*pushButton2.static: true
*pushButton2.name: pushButton2
*pushButton2.x: 0
*pushButton2.y: 318
*pushButton2.width: 210
*pushButton2.height: 35
*pushButton2.background: ButtonBackground
*pushButton2.bottomAttachment: "attach_form"
*pushButton2.bottomOffset: 5
*pushButton2.leftAttachment: "attach_form"
*pushButton2.leftOffset: 10
*pushButton2.rightAttachment: "attach_form"
*pushButton2.rightOffset: 10
*pushButton2.labelString: "cancel"
*pushButton2.fontList: BoldTextFont
*pushButton2.foreground: CancelForeground
*pushButton2.activateCallback: {\
extern swidget klist;\
UxPopdownInterface(klist);\
}

