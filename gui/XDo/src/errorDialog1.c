
/*******************************************************************************
	errorDialog1.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxErrorD.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxerrorDialog1;
} _UxCerrorDialog1;

#define errorDialog1            UxErrorDialog1Context->UxerrorDialog1

static _UxCerrorDialog1	*UxErrorDialog1Context;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_errorDialog1();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	createCB_errorDialog1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCerrorDialog1        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxErrorDialog1Context;
	UxErrorDialog1Context = UxContext =
			(_UxCerrorDialog1 *) UxGetContext( UxThisWidget );
	{
	XtVaSetValues(UxWidget,XmNallowShellResize,True,NULL); 
	XtVaSetValues(UxWidget,XmNdialogType,XmDIALOG_ERROR,NULL);
	}
	UxErrorDialog1Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_errorDialog1()
{
	UxPutAllowShellResize( errorDialog1, "true" );
	UxPutMinimizeButtons( errorDialog1, "false" );
	UxPutLabelFontList( errorDialog1, TextFont );
	UxPutMessageString( errorDialog1, "" );
	UxPutDialogTitle( errorDialog1, "Error" );
	UxPutButtonFontList( errorDialog1, BoldTextFont );
	UxPutTextFontList( errorDialog1, BoldTextFont );
	UxPutBackground( errorDialog1, WindowBackground );
	UxPutHeight( errorDialog1, 140 );
	UxPutWidth( errorDialog1, 300 );
	UxPutY( errorDialog1, 150 );
	UxPutX( errorDialog1, 620 );
	UxPutUnitType( errorDialog1, "pixels" );
	UxPutDialogType( errorDialog1, "dialog_file_selection" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_errorDialog1()
{
	/* Create the swidgets */

	errorDialog1 = UxCreateErrorDialog( "errorDialog1", NO_PARENT );
	UxPutContext( errorDialog1, UxErrorDialog1Context );
	UxPutDefaultShell( errorDialog1, "topLevelShell" );


	_Uxinit_errorDialog1();

	/* Create the X widgets */

	UxCreateWidget( errorDialog1 );
	createCB_errorDialog1( UxGetWidget( errorDialog1 ),
			(XtPointer) UxErrorDialog1Context, (XtPointer) NULL );



	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( errorDialog1 );

	return ( errorDialog1 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_errorDialog1()
{
	swidget                 rtrn;
	_UxCerrorDialog1        *UxContext;

	UxErrorDialog1Context = UxContext =
		(_UxCerrorDialog1 *) UxMalloc( sizeof(_UxCerrorDialog1) );

	rtrn = _Uxbuild_errorDialog1();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_errorDialog1()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_errorDialog1();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

