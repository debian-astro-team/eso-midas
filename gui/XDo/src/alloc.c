/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        alloc.c
.MODULE       subroutines
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Routines for memory [de]allocation
.KEYWORDS     Memory management.
.VERSION 1.0  1-Feb-1991   Implementation
 
 090825		last modif
------------------------------------------------------------*/

#include <stdio.h>
#include <midas_def.h>



int *ivector( nl, nh ) 
int nl, nh;
/* Allocates a int vector with range [nl..nh] */
{
    int *v;

    v = (int *)osmmget( (unsigned) (nh-nl+1) * sizeof(int) );
    return( v - nl );
}


void free_ivector( v, nl, nh ) 
int *v;
int nl, nh;
/* Frees a vector allocated with ivector() */
{
    osmmfree( (char *) (v + nl) );
}

double **dmatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a double matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    double **m;

    /* Allocate pointers to rows */
    m = (double **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(double *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (double *)osmmget( (unsigned) (nch-ncl+1) * sizeof(double) );
        m[i] -= ncl;
    }
    return( m );
}

double *dvector( nl, nh ) 
int nl, nh;
/* Allocates a double vector with range [nl..nh] */
{
    double *v;

    v = (double *)osmmget( (unsigned) (nh-nl+1) * sizeof(double) );
    return( v - nl );
}

void free_dmatrix( m, nrl, nrh, ncl, nch ) 
double **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with dmatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_dvector( v, nl, nh ) 
double *v;
int nl, nh;
/* Frees a vector allocated with dvector() */
{
    osmmfree( (char *) (v + nl) );
}

float **fmatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a float matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    float **m;

    /* Allocate pointers to rows */
    m = (float **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(float *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (float *)osmmget( (unsigned) (nch-ncl+1) * sizeof(float) );
        m[i] -= ncl;
    }
    return( m );
}

float *fvector( nl, nh ) 
int nl, nh;
/* Allocates a float vector with range [nl..nh] */
{
    float *v;

    v = (float *)osmmget( (unsigned) (nh-nl+1) * sizeof(float) );
    return( v - nl );
}

void free_fmatrix( m, nrl, nrh, ncl, nch ) 
float **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with fmatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_fvector( v, nl, nh ) 
float *v;
int nl, nh;
/* Frees a vector allocated with fvector() */
{
    osmmfree( (char *) (v + nl) );
}

char **cmatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a char matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    char **m;

    /* Allocate pointers to rows */
    m = (char **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(char *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (char *)osmmget( (unsigned) (nch-ncl+1) * sizeof(char) );
        m[i] -= ncl;
    }
    return( m );
}

char *cvector( nl, nh ) 
int nl, nh;
/* Allocates a char vector with range [nl..nh] */
{
    char *v;

    v = (char *)osmmget( (unsigned) (nh-nl+1) * sizeof(char) );
    return( v - nl );
}

void free_cmatrix( m, nrl, nrh, ncl, nch ) 
char **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with cmatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_cvector( v, nl, nh ) 
char *v;
int nl, nh;
/* Frees a vector allocated with cvector() */
{
    osmmfree( (char *) (v + nl) );
}
