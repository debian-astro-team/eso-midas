
/*******************************************************************************
	SelectInstrument.c

.VERSION
 090825         last modif


*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSelBox.h"

#include "midas_def.h"
#include "str.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSelectInstrument;
} _UxCSelectInstrument;

#define SelectInstrument        UxSelectInstrumentContext->UxSelectInstrument

static _UxCSelectInstrument	*UxSelectInstrumentContext;

extern int redecomp();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SelectInstrument();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	okCallback_SelectInstrument( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectInstrument    *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectInstrumentContext;
	UxSelectInstrumentContext = UxContext =
			(_UxCSelectInstrument *) UxGetContext( UxThisWidget );
	{
	char *value;
	char descr[257];
	extern swidget ilist;
	extern int tidost,doI;
	int uni,null,actval,index;
	XmSelectionBoxCallbackStruct *cbs;
	cbs = (XmSelectionBoxCallbackStruct *)UxCallbackArg;
	XmStringGetLtoR(cbs->value,XmSTRING_DEFAULT_CHARSET,&value);
	
	if ( doI) {
	   UxPutText(UxFindSwidget("clas_t1"),value);
	   oscfill(descr,257,'\0');
	   SCDRDC(tidost,value,1,1,256,&actval,descr,&uni,&null);
	   index = strbskip(descr,' ');
	    descr[index+1] = '\0';
	   UxPutText(UxFindSwidget("clas_t1"),value);
	  UxPutText(UxFindSwidget("criteria"),descr);
	   redecomp(descr);
	   }
	else {
	   UxPutText(UxFindSwidget("text3"),value);
	   UxPopdownInterface(ilist);
	   }
	}
	UxSelectInstrumentContext = UxSaveCtx;
}

static void	createCB_SelectInstrument( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectInstrument    *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectInstrumentContext;
	UxSelectInstrumentContext = UxContext =
			(_UxCSelectInstrument *) UxGetContext( UxThisWidget );
	UxSelectInstrumentContext = UxSaveCtx;
}

static void	cancelCB_SelectInstrument( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectInstrument    *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectInstrumentContext;
	UxSelectInstrumentContext = UxContext =
			(_UxCSelectInstrument *) UxGetContext( UxThisWidget );
	{
	extern swidget ilist;
	UxPopdownInterface(ilist);
	}
	UxSelectInstrumentContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SelectInstrument()
{
	UxPutListLabelString( SelectInstrument, "Instrument" );
	UxPutMustMatch( SelectInstrument, "true" );
	UxPutTextFontList( SelectInstrument, BoldTextFont );
	UxPutLabelFontList( SelectInstrument, BoldTextFont );
	UxPutButtonFontList( SelectInstrument, BoldTextFont );
	UxPutBackground( SelectInstrument, WindowBackground );
	UxPutHeight( SelectInstrument, 320 );
	UxPutWidth( SelectInstrument, 260 );
	UxPutY( SelectInstrument, 80 );
	UxPutX( SelectInstrument, 400 );
	UxPutUnitType( SelectInstrument, "pixels" );
	UxPutResizePolicy( SelectInstrument, "resize_none" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SelectInstrument()
{
	/* Create the swidgets */

	SelectInstrument = UxCreateSelectionBox( "SelectInstrument", NO_PARENT );
	UxPutContext( SelectInstrument, UxSelectInstrumentContext );
	UxPutDefaultShell( SelectInstrument, "topLevelShell" );


	_Uxinit_SelectInstrument();

	/* Create the X widgets */

	UxCreateWidget( SelectInstrument );
	createCB_SelectInstrument( UxGetWidget( SelectInstrument ),
			(XtPointer) UxSelectInstrumentContext, (XtPointer) NULL );


	UxAddCallback( SelectInstrument, XmNokCallback,
			okCallback_SelectInstrument,
			(XtPointer) UxSelectInstrumentContext );
	UxAddCallback( SelectInstrument, XmNcancelCallback,
			cancelCB_SelectInstrument,
			(XtPointer) UxSelectInstrumentContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SelectInstrument );

	return ( SelectInstrument );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SelectInstrument()
{
	swidget                 rtrn;
	_UxCSelectInstrument    *UxContext;

	UxSelectInstrumentContext = UxContext =
		(_UxCSelectInstrument *) UxMalloc( sizeof(_UxCSelectInstrument) );

	rtrn = _Uxbuild_SelectInstrument();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SelectInstrument()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_SelectInstrument();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

