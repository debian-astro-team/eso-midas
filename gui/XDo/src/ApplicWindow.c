
/*
.VERSION
 090825		last modif
*/

/*******************************************************************************
	ApplicWindow.c

	Associated Resource file: ApplicWindow.rf
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "UxLib.h"
#include "UxLabel.h"
#include "UxScText.h"
#include "UxPushB.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxText.h"
#include "UxFrame.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <Xm/Protocols.h>

#include <Xm/Text.h>
#include <Xm/LabelG.h>
#include <midas_def.h>
#include <tbldef.h>
extern swidget scrolledtable;
extern Widget sb;
extern void myscrollv(),myscrollh();


/* Definitions for History mechanism */

#define SIZHIST 50
#define SIZTEXT 40
int  ItemNumber = 0, ItemPosition = 0;
int  DispItem = 0; 

char ItemHistory[SIZHIST][SIZTEXT];
char HistList[SIZHIST*SIZTEXT];

Widget sb1,sb2,hbs;
int height;

/* Variables for SelectionArray */

XmTextScanType Sarray[60];
int            SarI = 0;


/* Include for UxPut calls on the fileSelectionBox */

void CreateWindowManagerProtocols();
void ExitCB();
void CreateSessionManagerProtocols();
void SaveSessionCB();

extern Boolean saving;  /* declared in file selection box declarations */
extern swidget fileSelectionDialog;
extern swidget fileSelectionBox;
extern swidget exitDialog;
extern swidget create_fileSelectionDialog();
extern swidget create_exitDialog();

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxmainWindow;
	swidget	UxpullDownMenu;
	swidget	UxquitPane;
	swidget	UxquitPane_b2;
	swidget	UxpullDownMenu_top_b1;
	swidget	UxUtilsPane;
	swidget	UxUtilsPane_b5;
	swidget	UxUtilsPane_b6;
	swidget	UxUtilsPane_b7;
	swidget	UxUtilsPane_b9;
	swidget	UxcommandsCascade;
	swidget	UxHelpPane;
	swidget	UxpullDownMenu_p3_b1;
	swidget	UxHelpPane_b2;
	swidget	UxHelpPane_b3;
	swidget	UxpullDownMenu_top_b5;
	swidget	UxworkAreaForm;
	swidget	UxworkAreaFrame;
	swidget	UxworkArea;
	swidget	UxSHelp;
	swidget	Uxscrol2;
	swidget	Uxreadtable;
	swidget	Uxscrol1;
	swidget	Uxidentlist;
	swidget	UxColButton;
	swidget	UxtextWindow;
	swidget	Uxscrollabel;
	swidget	UxIDENT;
	swidget	UxRowButton;
	swidget	Uxlabel9;
	swidget	Uxseparator6;
	swidget	Uxseparator7;
	swidget	Uxseparator8;
	swidget	Uxseparator9;
	swidget	Uxlabel10;
	swidget	Uxseparator10;
	swidget	Uxseparator11;
	swidget	Uxseparator12;
	swidget	Uxseparator13;
	swidget	UxAssoButton1;
	swidget	UxClassiButton;
	swidget	UxAssoButton;
} _UxCApplicWindow;

#define mainWindow              UxApplicWindowContext->UxmainWindow
#define pullDownMenu            UxApplicWindowContext->UxpullDownMenu
#define quitPane                UxApplicWindowContext->UxquitPane
#define quitPane_b2             UxApplicWindowContext->UxquitPane_b2
#define pullDownMenu_top_b1     UxApplicWindowContext->UxpullDownMenu_top_b1
#define UtilsPane               UxApplicWindowContext->UxUtilsPane
#define UtilsPane_b5            UxApplicWindowContext->UxUtilsPane_b5
#define UtilsPane_b6            UxApplicWindowContext->UxUtilsPane_b6
#define UtilsPane_b7            UxApplicWindowContext->UxUtilsPane_b7
#define UtilsPane_b9            UxApplicWindowContext->UxUtilsPane_b9
#define commandsCascade         UxApplicWindowContext->UxcommandsCascade
#define HelpPane                UxApplicWindowContext->UxHelpPane
#define pullDownMenu_p3_b1      UxApplicWindowContext->UxpullDownMenu_p3_b1
#define HelpPane_b2             UxApplicWindowContext->UxHelpPane_b2
#define HelpPane_b3             UxApplicWindowContext->UxHelpPane_b3
#define pullDownMenu_top_b5     UxApplicWindowContext->UxpullDownMenu_top_b5
#define workAreaForm            UxApplicWindowContext->UxworkAreaForm
#define workAreaFrame           UxApplicWindowContext->UxworkAreaFrame
#define workArea                UxApplicWindowContext->UxworkArea
#define SHelp                   UxApplicWindowContext->UxSHelp
#define scrol2                  UxApplicWindowContext->Uxscrol2
#define readtable               UxApplicWindowContext->Uxreadtable
#define scrol1                  UxApplicWindowContext->Uxscrol1
#define identlist               UxApplicWindowContext->Uxidentlist
#define ColButton               UxApplicWindowContext->UxColButton
#define textWindow              UxApplicWindowContext->UxtextWindow
#define scrollabel              UxApplicWindowContext->Uxscrollabel
#define IDENT                   UxApplicWindowContext->UxIDENT
#define RowButton               UxApplicWindowContext->UxRowButton
#define label9                  UxApplicWindowContext->Uxlabel9
#define separator6              UxApplicWindowContext->Uxseparator6
#define separator7              UxApplicWindowContext->Uxseparator7
#define separator8              UxApplicWindowContext->Uxseparator8
#define separator9              UxApplicWindowContext->Uxseparator9
#define label10                 UxApplicWindowContext->Uxlabel10
#define separator10             UxApplicWindowContext->Uxseparator10
#define separator11             UxApplicWindowContext->Uxseparator11
#define separator12             UxApplicWindowContext->Uxseparator12
#define separator13             UxApplicWindowContext->Uxseparator13
#define AssoButton1             UxApplicWindowContext->UxAssoButton1
#define ClassiButton            UxApplicWindowContext->UxClassiButton
#define AssoButton              UxApplicWindowContext->UxAssoButton

static _UxCApplicWindow	*UxApplicWindowContext;

extern void SetFileList(), display_help();

extern int populate(), read_table_column(), selectlist();


swidget	ApplicWindow;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


static char	*ChangeAttr = "#override\n\
<Btn1Up>:ChangeFor()\n\
<Btn1Down>:grab-focus()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	popup_ApplicWindow();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HInit( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ChangeFor( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern  swidget attri;
	extern int tidost;
	extern int  collabchan;
	char mylab[1+TBL_LABLEN];
	char *text,form[1+TBL_LABLEN];
	int dummy;
	text = XmTextGetSelection(UxGetWidget(UxFindSwidget("scrollabel")));
	if (text != NULL) strcpy(mylab,text);
	XtFree(text);
	TCLSER(tidost,mylab,&collabchan);
	TCFGET(tidost,collabchan,form,&dummy,&dummy);
	UxPutText(UxFindSwidget("newform"),form);
	UxPopupInterface(attri,no_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_nothing( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	printf("toto:\n");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_SelectCommand( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	 
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SHelp"),"");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char s[100];
	Widget sw = UxWidget;
	
	strcpy(s,"");
	 
	if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {
	   strcpy(s,"Select the OST columns to be displayed into the scrolled window");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   } 
	else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {
	   strcpy(s,"Activate interface for defining classification rules ");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }  
	else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {
	   strcpy(s,"Activate interface for classifying images");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {
	   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }
	else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {
	   strcpy(s,"Create the OST table");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {
	   strcpy(s,"Popdown this interface");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ 
	   strcpy(s,"Name of the Observation Summary Table to be created ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ 
	   strcpy(s,"List of frames to be processed ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }  
	  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  
	   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  
	   strcpy(s,"Push button to popup the File Selection Box");     
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  
	        strcpy(s,"Push button to popup the File Selection Box");  
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   } 
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_HelpACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	/* char *s;
	 s = XmTextGetSelection(UxWidget); 
	 
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	*/}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_ApplicWindow( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_quitPane_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	int exist;
	exist = SCFINF("TAB_COMM.tbl",99,&exist);
	if (exist == 0) SCFDEL("TAB_COMM.tbl"); 
	 
	
	exit(0);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char s[1000];
	extern swidget klist,tablelist;
	extern int do8;
	int strip =1;
	strcpy(s,"*.tbl");
	SetFileList(UxGetWidget(tablelist),strip,s);
	do8 = 0;
	UxPutTitle(UxFindSwidget("transientShell8"),"Select OST Table");
	UxPopupInterface(klist,no_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget crea;
	UxPopupInterface(crea,no_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu_p3_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget help;
	 
	display_help (help,"XDo.help"); 
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_HelpPane_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget help;
	display_help(help,"XDo_default.help");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_HelpPane_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{ 
	extern swidget help;
	display_help(help,"XDo_list.help");  
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_workArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_workArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_SHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	UxApplicWindowContext = UxSaveCtx;
}

static void	extendedSelectionCB_readtable( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	UxApplicWindowContext = UxSaveCtx;
}

static void	browseSelectionCB_readtable( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	printf("browse:");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	extendedSelectionCB_identlist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	XmListCallbackStruct *cbs;
	char *choice;
	int i;
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	if (cbs->reason == XmCR_EXTENDED_SELECT ){
	
	for (i=0; i<cbs->selected_item_count; i++) {
	   XmStringGetLtoR(cbs->selected_items[i],XmSTRING_DEFAULT_CHARSET,&choice);
	    printf("choice: %s \n",choice);
	   XtFree(choice);
	   }
	}else {
	   XmStringGetLtoR(cbs->item,XmSTRING_DEFAULT_CHARSET,&choice);
	    printf("choice:%s\n",choice);
	   XtFree(choice);
	   }
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_ColButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget clist,myerror;
	extern  int tidost,colno;
	 
	if (tidost < 0) {
	        UxPutMessageString(UxFindSwidget("errorDialog1"),"No OST opened");
	        UxPopupInterface(myerror,no_grab); 
	        return; 
	         }
	
	populate(UxGetWidget(UxFindSwidget("columnlist1")));
	read_table_column(UxGetWidget(UxFindSwidget("columnlist")));
	    
	if (colno)  selectlist("COPY",UxGetWidget(UxFindSwidget("columnlist")));
	UxPopupInterface(clist,no_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_RowButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_AssoButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget assohaupt;
	
	UxPopupInterface(assohaupt,no_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_ClassiButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern void myrow(),never_called(),checkdigit(),checkchar();
	extern swidget classi,myerror;
	extern Widget rowrule[256][2]; 
	extern int tidost;
	XmString str; 
	int i,j ,dummy,ncol,type;
	static int rowrul = 0;
	char mylabel[TBL_LABLEN+1],mytext[8];
	        UxPutText(UxFindSwidget("criteria"),"l01");
	
	if (tidost < 0) {
	        UxPutMessageString(UxFindSwidget("errorDialog1"),"No OST opened");
	        UxPopupInterface(myerror,no_grab); 
	        return; 
	         }
	 TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy); 
	if (!rowrule[0][0]) { 
	  XtAddCallback(UxGetWidget(UxFindSwidget("rowdescr")),XmNentryCallback,myrow,NULL);
	 rowrul = ncol;
	 XtVaSetValues(UxGetWidget(UxFindSwidget("rowdescr")),
	                XmNnumColumns,ncol,NULL);
	  
	 for (i=1; i<=ncol; i++) {
	     TCLGET(tidost,i,mylabel);
	     TCBGET(tidost,i,&type,&dummy,&dummy); 
	     rowrule[i-1][0]   = XtVaCreateWidget(mylabel,xmLabelGadgetClass,
	                            UxGetWidget(UxFindSwidget("rowdescr")),
	                            NULL);
	     sprintf(mytext,"text_%d",i);
	     rowrule[i-1][1] = XtVaCreateWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowdescr")),
	                   NULL);
	     UxPutForeground(UxWidgetToSwidget(rowrule[i-1][1]),TextForeground);
	     UxPutBackground(UxWidgetToSwidget(rowrule[i-1][1]),WindowBackground);
	     UxPutFontList(UxWidgetToSwidget(rowrule[i-1][1]),BoldSmallFont);
	     if (type == D_C_FORMAT)
	        XtAddCallback(rowrule[i-1][1],XmNmodifyVerifyCallback,checkchar,NULL);
	     else
	        XtAddCallback(rowrule[i-1][1],XmNmodifyVerifyCallback,checkdigit,NULL);
	 
	      }
	XtManageChildren((WidgetList) rowrule, ncol * 2); 
	}
	else { 
	 
	  if (ncol > rowrul) { 
	          XtVaSetValues(UxGetWidget(UxFindSwidget("rowdescr")),
	                XmNnumColumns,ncol,NULL);  
	          rowrul = ncol;
	         } 
	  else if (ncol < rowrul) {
	        for (j=ncol; j< rowrul;j++) {
	             XmTextSetString(rowrule[j][1],""); 
	             str = XmStringCreateSimple("");
	             XtVaSetValues (rowrule[j][0],  XmNlabelString,str ,
	                                 NULL);
	             XmStringFree(str);
	          }
	  }
	  for (i=1; i<=ncol; i++) { 
	    TCLGET(tidost,i,mylabel);
	    if (!rowrule[i-1][0])  
	      rowrule[i-1][0] = XtVaCreateWidget(mylabel,
	                          xmLabelGadgetClass,
	                          UxGetWidget(UxFindSwidget("rowdescr")),
	                          NULL); 
	    else  { 
	           str = XmStringCreateSimple(mylabel);
	           XtVaSetValues (rowrule[i-1][0],XmNlabelString,str ,
	                                 NULL);
	           XmStringFree(str); 
	          }
	 
	     if (!rowrule[i-1][1]) {  
	        sprintf(mytext,"text_%d",i);
	        rowrule[i-1][1] = XtVaCreateWidget(mytext, xmTextWidgetClass,
	        UxGetWidget(UxFindSwidget("rowdescr")),
	        XmNrightAttachment,XmATTACH_WIDGET, 
	        XmNrightWidget,UxGetWidget(UxFindSwidget("scrolledWindow6")),
	        XmNleftAttachment,XmATTACH_POSITION,
	        XmNrightPosition,200,
	        NULL);
	     UxPutForeground(UxWidgetToSwidget(rowrule[i-1][1]),TextForeground);
	     UxPutBackground(UxWidgetToSwidget(rowrule[i-1][1]),TextBackground);
	     UxPutFontList(UxWidgetToSwidget(rowrule[i-1][1]),BoldSmallFont);
	        }
	     }
	   XtManageChildren((WidgetList) rowrule, ncol * 2);
	   }
	UxPutText(UxFindSwidget("criteria"),"");
	UxPopupInterface(classi,no_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_AssoButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget clashaupt;
	extern Widget row1[40],row2[40],row3[40];
	int  rows = 5;
	extern int rowno;
	int i;
	char mytext[8];
	   
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowproc")),XmNnumColumns,rows,NULL);
	if (!row1[0]) {
	   rowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"text1_%d",i);
	   row1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowproc")),NULL);
	   sprintf(mytext,"text2_%d",i);
	   UxPutForeground(UxWidgetToSwidget(row1[i]),TextForeground);
	   UxPutBackground(UxWidgetToSwidget(row1[i]),TextBackground);
	   UxPutFontList(UxWidgetToSwidget(row1[i]),BoldSmallFont);
	   row2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowproc")),NULL);
	   UxPutForeground(UxWidgetToSwidget(row2[i]),TextForeground);
	   UxPutBackground(UxWidgetToSwidget(row2[i]),TextBackground);
	   UxPutFontList(UxWidgetToSwidget(row2[i]),BoldSmallFont);
	   sprintf(mytext,"text3_%d",i);
	   row3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowproc")),NULL);
	   UxPutForeground(UxWidgetToSwidget(row3[i]),TextForeground);
	   UxPutBackground(UxWidgetToSwidget(row3[i]),TextBackground);
	   UxPutFontList(UxWidgetToSwidget(row3[i]),BoldSmallFont);
	 }
	 
	}
	
	
	UxPopupInterface(clashaupt,no_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutGeometry( ApplicWindow, "700x540+30+30" );
	UxPutBackground( ApplicWindow, WindowBackground );
	UxPutIconName( ApplicWindow, "New" );
	UxPutDeleteResponse( ApplicWindow, "do_nothing" );
	UxPutTitle( ApplicWindow, "XDo" );
	UxPutHeight( ApplicWindow, 680 );
	UxPutWidth( ApplicWindow, 700 );
	UxPutY( ApplicWindow, 577 );
	UxPutX( ApplicWindow, 550 );

	UxPutBackground( mainWindow, WindowBackground );
	UxPutHeight( mainWindow, 680 );
	UxPutWidth( mainWindow, 700 );
	UxPutY( mainWindow, 180 );
	UxPutX( mainWindow, 200 );

	UxPutForeground( pullDownMenu, MenuForeground );
	UxPutBackground( pullDownMenu, MenuBackground );
	UxPutMenuAccelerator( pullDownMenu, "<KeyUp>F10" );
	UxPutRowColumnType( pullDownMenu, "menu_bar" );
	UxPutBorderWidth( pullDownMenu, 0 );

	UxPutForeground( quitPane, MenuForeground );
	UxPutBackground( quitPane, MenuBackground );
	UxPutRowColumnType( quitPane, "menu_pulldown" );

	UxPutFontList( quitPane_b2, BoldTextFont );
	UxPutLabelString( quitPane_b2, "Bye" );

	UxPutForeground( pullDownMenu_top_b1, MenuForeground );
	UxPutFontList( pullDownMenu_top_b1, BoldTextFont );
	UxPutBackground( pullDownMenu_top_b1, MenuBackground );
	UxPutMnemonic( pullDownMenu_top_b1, "Q" );
	UxPutLabelString( pullDownMenu_top_b1, "Quit" );

	UxPutForeground( UtilsPane, MenuForeground );
	UxPutBackground( UtilsPane, MenuBackground );
	UxPutRowColumnType( UtilsPane, "menu_pulldown" );

	UxPutFontList( UtilsPane_b5, BoldTextFont );
	UxPutLabelString( UtilsPane_b5, "Open ..." );

	UxPutFontList( UtilsPane_b6, BoldTextFont );
	UxPutLabelString( UtilsPane_b6, "New ..." );

	UxPutSensitive( UtilsPane_b9, "false" );
	UxPutFontList( UtilsPane_b9, BoldTextFont );
	UxPutLabelString( UtilsPane_b9, "Add ..." );

	UxPutX( commandsCascade, 101 );
	UxPutMnemonic( commandsCascade, "F" );
	UxPutForeground( commandsCascade, MenuForeground );
	UxPutFontList( commandsCascade, BoldTextFont );
	UxPutBackground( commandsCascade, MenuBackground );
	UxPutLabelString( commandsCascade, "File" );

	UxPutForeground( HelpPane, MenuForeground );
	UxPutBackground( HelpPane, MenuBackground );
	UxPutRowColumnType( HelpPane, "menu_pulldown" );

	UxPutFontList( pullDownMenu_p3_b1, BoldTextFont );
	UxPutLabelString( pullDownMenu_p3_b1, "On XDo..." );

	UxPutFontList( HelpPane_b2, BoldTextFont );
	UxPutLabelString( HelpPane_b2, "On NTT defaults..." );

	UxPutFontList( HelpPane_b3, BoldTextFont );
	UxPutLabelString( HelpPane_b3, "On scrolled lists..." );

	UxPutForeground( pullDownMenu_top_b5, MenuForeground );
	UxPutFontList( pullDownMenu_top_b5, BoldTextFont );
	UxPutBackground( pullDownMenu_top_b5, MenuBackground );
	UxPutMnemonic( pullDownMenu_top_b5, "H" );
	UxPutLabelString( pullDownMenu_top_b5, "Help" );

	UxPutBackground( workAreaForm, WindowBackground );
	UxPutBorderWidth( workAreaForm, 0 );
	UxPutHeight( workAreaForm, 440 );
	UxPutWidth( workAreaForm, 490 );

	UxPutBackground( workAreaFrame, WindowBackground );
	UxPutHeight( workAreaFrame, 230 );
	UxPutWidth( workAreaFrame, 390 );
	UxPutY( workAreaFrame, 30 );
	UxPutX( workAreaFrame, 50 );

	UxPutMarginWidth( workArea, 5 );
	UxPutMarginHeight( workArea, 5 );
	UxPutBorderColor( workArea, WindowBackground );
	UxPutBackground( workArea, WindowBackground );
	UxPutBorderWidth( workArea, 0 );
	UxPutHeight( workArea, 448 );
	UxPutWidth( workArea, 698 );
	UxPutY( workArea, 2 );
	UxPutX( workArea, 2 );

	UxPutAccelerators( SHelp, "" );
	UxPutWordWrap( SHelp, "false" );
	UxPutHighlightOnEnter( SHelp, "true" );
	UxPutEditMode( SHelp, "single_line_edit" );
	UxPutBlinkRate( SHelp, 500 );
	UxPutPendingDelete( SHelp, "true" );
	UxPutTranslations( SHelp, "" );
	UxPutCursorPositionVisible( SHelp, "false" );
	UxPutFontList( SHelp, TextFont );
	UxPutHighlightColor( SHelp, ApplicBackground );
	UxPutForeground( SHelp, TextForeground );
	UxPutBackground( SHelp, SHelpBackground );
	UxPutHeight( SHelp, 40 );
	UxPutWidth( SHelp, 401 );
	UxPutY( SHelp, 337 );
	UxPutX( SHelp, 290 );

	UxPutMappedWhenManaged( scrol2, "true" );
	UxPutWidth( scrol2, 481 );
	UxPutBackground( scrol2, WindowBackground );
	UxPutScrollBarPlacement( scrol2, "bottom_right" );
	UxPutShadowThickness( scrol2, 0 );
	UxPutScrollBarDisplayPolicy( scrol2, "static" );
	UxPutVisualPolicy( scrol2, "variable" );
	UxPutY( scrol2, 55 );
	UxPutX( scrol2, 176 );
	UxPutScrollingPolicy( scrol2, "application_defined" );

	UxPutForeground( readtable, TextForeground );
	UxPutFontList( readtable, BoldSmallFont );
	UxPutSelectionPolicy( readtable, "extended_select" );
	UxPutBackground( readtable, TextBackground );
	UxPutVisibleItemCount( readtable, 16 );
	UxPutListSizePolicy( readtable, "resize_if_possible" );
	UxPutScrollBarDisplayPolicy( readtable, "static" );
	UxPutHeight( readtable, 250 );
	UxPutWidth( readtable, 521 );

	UxPutBackground( scrol1, WindowBackground );
	UxPutScrollBarPlacement( scrol1, "bottom_right" );
	UxPutHeight( scrol1, 302 );
	UxPutShadowThickness( scrol1, 0 );
	UxPutScrollBarDisplayPolicy( scrol1, "static" );
	UxPutVisualPolicy( scrol1, "variable" );
	UxPutY( scrol1, 10 );
	UxPutX( scrol1, 10 );
	UxPutScrollingPolicy( scrol1, "application_defined" );

	UxPutForeground( identlist, TextForeground );
	UxPutAncestorSensitive( identlist, "true" );
	UxPutNavigationType( identlist, "tab_group" );
	UxPutTraversalOn( identlist, "false" );
	UxPutFontList( identlist, BoldSmallFont );
	UxPutSelectionPolicy( identlist, "extended_select" );
	UxPutVisibleItemCount( identlist, 16 );
	UxPutListSizePolicy( identlist, "constant" );
	UxPutScrollBarDisplayPolicy( identlist, "static" );
	UxPutBackground( identlist, TextBackground );
	UxPutTopShadowColor( identlist, "Gray80" );
	UxPutHeight( identlist, 302 );
	UxPutWidth( identlist, 150 );

	UxPutHighlightColor( ColButton, "Black" );
	UxPutRecomputeSize( ColButton, "false" );
	UxPutForeground( ColButton, ButtonForeground );
	UxPutFontList( ColButton, BoldTextFont );
	UxPutLabelString( ColButton, "Sel Cols.." );
	UxPutBackground( ColButton, ButtonBackground );
	UxPutHeight( ColButton, 30 );
	UxPutWidth( ColButton, 90 );
	UxPutY( ColButton, 380 );
	UxPutX( ColButton, 110 );

	UxPutHeight( textWindow, 70 );
	UxPutBottomShadowColor( textWindow, "#707070" );
	UxPutForeground( textWindow, "black" );
	UxPutHighlightColor( textWindow, WindowBackground );
	UxPutTopShadowColor( textWindow, "#a3a3a3" );
	UxPutBorderColor( textWindow, "black" );
	UxPutBackground( textWindow, WindowBackground );
	UxPutShadowThickness( textWindow, 0 );
	UxPutScrollBarDisplayPolicy( textWindow, "static" );
	UxPutVisualPolicy( textWindow, "variable" );
	UxPutY( textWindow, 0 );
	UxPutX( textWindow, 220 );
	UxPutScrollingPolicy( textWindow, "application_defined" );

	UxPutTranslations( scrollabel, ChangeAttr );
	UxPutSelectionArray( scrollabel, "select_word select_word select_word select_word" );
	UxPutHighlightOnEnter( scrollabel, "false" );
	UxPutMarginWidth( scrollabel, 0 );
	UxPutTopShadowColor( scrollabel, "#a3a3a3" );
	UxPutAutoShowCursorPosition( scrollabel, "false" );
	UxPutShadowThickness( scrollabel, 0 );
	UxPutHighlightColor( scrollabel, "grey" );
	UxPutBottomShadowColor( scrollabel, "#707070" );
	UxPutHighlightThickness( scrollabel, 0 );
	UxPutMarginHeight( scrollabel, 0 );
	UxPutCursorPosition( scrollabel, 100 );
	UxPutText( scrollabel, "" );
	UxPutRows( scrollabel, 1 );
	UxPutCursorPositionVisible( scrollabel, "false" );
	UxPutForeground( scrollabel, "Red" );
	UxPutFontList( scrollabel, BoldSmallFont );
	UxPutBorderColor( scrollabel, WindowBackground );
	UxPutBackground( scrollabel, WindowBackground );
	UxPutEditable( scrollabel, "false" );
	UxPutEditMode( scrollabel, "single_line_edit" );
	UxPutHeight( scrollabel, 60 );
	UxPutWidth( scrollabel, 460 );

	UxPutForeground( IDENT, ButtonForeground );
	UxPutLabelString( IDENT, "IDENT" );
	UxPutFontList( IDENT, BoldSmallFont );
	UxPutBackground( IDENT, WindowBackground );
	UxPutHeight( IDENT, 50 );
	UxPutWidth( IDENT, 210 );
	UxPutY( IDENT, 0 );
	UxPutX( IDENT, 0 );

	UxPutSensitive( RowButton, "false" );
	UxPutHighlightColor( RowButton, "Black" );
	UxPutRecomputeSize( RowButton, "false" );
	UxPutFontList( RowButton, BoldTextFont );
	UxPutForeground( RowButton, ButtonForeground );
	UxPutLabelString( RowButton, "Sel Rows.." );
	UxPutBackground( RowButton, ButtonBackground );
	UxPutHeight( RowButton, 30 );
	UxPutWidth( RowButton, 90 );
	UxPutY( RowButton, 380 );
	UxPutX( RowButton, 20 );

	UxPutLabelString( label9, "Selection" );
	UxPutFontList( label9, BoldTextFont );
	UxPutBackground( label9, WindowBackground );
	UxPutHeight( label9, 30 );
	UxPutWidth( label9, 75 );
	UxPutY( label9, 390 );
	UxPutX( label9, 30 );

	UxPutBackground( separator6, WindowBackground );
	UxPutHeight( separator6, 10 );
	UxPutWidth( separator6, 205 );
	UxPutY( separator6, 410 );
	UxPutX( separator6, 20 );

	UxPutBackground( separator7, WindowBackground );
	UxPutHeight( separator7, 10 );
	UxPutWidth( separator7, 205 );
	UxPutY( separator7, 420 );
	UxPutX( separator7, 25 );

	UxPutBackground( separator8, WindowBackground );
	UxPutOrientation( separator8, "vertical" );
	UxPutHeight( separator8, 70 );
	UxPutWidth( separator8, 10 );
	UxPutY( separator8, 410 );
	UxPutX( separator8, 220 );

	UxPutBackground( separator9, WindowBackground );
	UxPutOrientation( separator9, "vertical" );
	UxPutHeight( separator9, 70 );
	UxPutWidth( separator9, 10 );
	UxPutY( separator9, 422 );
	UxPutX( separator9, 230 );

	UxPutLabelString( label10, "Classification" );
	UxPutFontList( label10, BoldTextFont );
	UxPutBackground( label10, WindowBackground );
	UxPutHeight( label10, 30 );
	UxPutWidth( label10, 100 );
	UxPutY( label10, 407 );
	UxPutX( label10, 80 );

	UxPutBackground( separator10, WindowBackground );
	UxPutOrientation( separator10, "vertical" );
	UxPutHeight( separator10, 70 );
	UxPutWidth( separator10, 10 );
	UxPutY( separator10, 427 );
	UxPutX( separator10, 225 );

	UxPutBackground( separator11, WindowBackground );
	UxPutHeight( separator11, 10 );
	UxPutWidth( separator11, 205 );
	UxPutY( separator11, 492 );
	UxPutX( separator11, 25 );

	UxPutBackground( separator12, WindowBackground );
	UxPutHeight( separator12, 10 );
	UxPutWidth( separator12, 205 );
	UxPutY( separator12, 420 );
	UxPutX( separator12, 25 );

	UxPutBackground( separator13, WindowBackground );
	UxPutOrientation( separator13, "vertical" );
	UxPutHeight( separator13, 70 );
	UxPutWidth( separator13, 10 );
	UxPutY( separator13, 427 );
	UxPutX( separator13, 270 );

	UxPutHighlightColor( AssoButton1, "Black" );
	UxPutRecomputeSize( AssoButton1, "false" );
	UxPutForeground( AssoButton1, ButtonForeground );
	UxPutFontList( AssoButton1, BoldTextFont );
	UxPutLabelString( AssoButton1, "Associate.." );
	UxPutBackground( AssoButton1, ButtonBackground );
	UxPutHeight( AssoButton1, 30 );
	UxPutWidth( AssoButton1, 90 );
	UxPutY( AssoButton1, 442 );
	UxPutX( AssoButton1, 380 );

	UxPutHighlightColor( ClassiButton, "Black" );
	UxPutRecomputeSize( ClassiButton, "false" );
	UxPutFontList( ClassiButton, BoldTextFont );
	UxPutForeground( ClassiButton, ButtonForeground );
	UxPutLabelString( ClassiButton, "Rule.." );
	UxPutBackground( ClassiButton, ButtonBackground );
	UxPutHeight( ClassiButton, 30 );
	UxPutWidth( ClassiButton, 90 );
	UxPutY( ClassiButton, 330 );
	UxPutX( ClassiButton, 180 );

	UxPutHighlightColor( AssoButton, "Black" );
	UxPutRecomputeSize( AssoButton, "false" );
	UxPutForeground( AssoButton, ButtonForeground );
	UxPutFontList( AssoButton, BoldTextFont );
	UxPutLabelString( AssoButton, "Classify.." );
	UxPutBackground( AssoButton, ButtonBackground );
	UxPutHeight( AssoButton, 30 );
	UxPutWidth( AssoButton, 90 );
	UxPutY( AssoButton, 380 );
	UxPutX( AssoButton, 110 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	mainWindow = UxCreateMainWindow( "mainWindow", ApplicWindow );
	pullDownMenu = UxCreateRowColumn( "pullDownMenu", mainWindow );
	quitPane = UxCreateRowColumn( "quitPane", pullDownMenu );
	quitPane_b2 = UxCreatePushButtonGadget( "quitPane_b2", quitPane );
	pullDownMenu_top_b1 = UxCreateCascadeButton( "pullDownMenu_top_b1", pullDownMenu );
	UtilsPane = UxCreateRowColumn( "UtilsPane", pullDownMenu );
	UtilsPane_b5 = UxCreatePushButtonGadget( "UtilsPane_b5", UtilsPane );
	UtilsPane_b6 = UxCreatePushButtonGadget( "UtilsPane_b6", UtilsPane );
	UtilsPane_b7 = UxCreateSeparator( "UtilsPane_b7", UtilsPane );
	UtilsPane_b9 = UxCreatePushButtonGadget( "UtilsPane_b9", UtilsPane );
	commandsCascade = UxCreateCascadeButton( "commandsCascade", pullDownMenu );
	HelpPane = UxCreateRowColumn( "HelpPane", pullDownMenu );
	pullDownMenu_p3_b1 = UxCreatePushButtonGadget( "pullDownMenu_p3_b1", HelpPane );
	HelpPane_b2 = UxCreatePushButtonGadget( "HelpPane_b2", HelpPane );
	HelpPane_b3 = UxCreatePushButtonGadget( "HelpPane_b3", HelpPane );
	pullDownMenu_top_b5 = UxCreateCascadeButton( "pullDownMenu_top_b5", pullDownMenu );
	workAreaForm = UxCreateForm( "workAreaForm", mainWindow );
	workAreaFrame = UxCreateFrame( "workAreaFrame", workAreaForm );
	workArea = UxCreateForm( "workArea", workAreaFrame );
	SHelp = UxCreateText( "SHelp", workArea );
	scrol2 = UxCreateScrolledWindow( "scrol2", workArea );
	readtable = UxCreateScrolledList( "readtable", scrol2 );
	scrol1 = UxCreateScrolledWindow( "scrol1", workArea );
	identlist = UxCreateScrolledList( "identlist", scrol1 );
	ColButton = UxCreatePushButton( "ColButton", workArea );
	textWindow = UxCreateScrolledWindow( "textWindow", workArea );
	scrollabel = UxCreateScrolledText( "scrollabel", textWindow );
	IDENT = UxCreateLabel( "IDENT", workArea );
	RowButton = UxCreatePushButton( "RowButton", workArea );
	label9 = UxCreateLabel( "label9", workArea );
	separator6 = UxCreateSeparator( "separator6", workArea );
	separator7 = UxCreateSeparator( "separator7", workArea );
	separator8 = UxCreateSeparator( "separator8", workArea );
	separator9 = UxCreateSeparator( "separator9", workArea );
	label10 = UxCreateLabel( "label10", workArea );
	separator10 = UxCreateSeparator( "separator10", workArea );
	separator11 = UxCreateSeparator( "separator11", workArea );
	separator12 = UxCreateSeparator( "separator12", workArea );
	separator13 = UxCreateSeparator( "separator13", workArea );
	AssoButton1 = UxCreatePushButton( "AssoButton1", workArea );
	ClassiButton = UxCreatePushButton( "ClassiButton", workArea );
	AssoButton = UxCreatePushButton( "AssoButton", workArea );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( mainWindow );
	UxCreateWidget( pullDownMenu );
	UxCreateWidget( quitPane );
	UxCreateWidget( quitPane_b2 );
	UxPutSubMenuId( pullDownMenu_top_b1, "quitPane" );
	UxCreateWidget( pullDownMenu_top_b1 );

	UxCreateWidget( UtilsPane );
	UxCreateWidget( UtilsPane_b5 );
	UxCreateWidget( UtilsPane_b6 );
	UxCreateWidget( UtilsPane_b7 );
	UxCreateWidget( UtilsPane_b9 );
	UxPutSubMenuId( commandsCascade, "UtilsPane" );
	UxCreateWidget( commandsCascade );

	UxCreateWidget( HelpPane );
	UxCreateWidget( pullDownMenu_p3_b1 );
	UxCreateWidget( HelpPane_b2 );
	UxCreateWidget( HelpPane_b3 );
	UxPutSubMenuId( pullDownMenu_top_b5, "HelpPane" );
	UxCreateWidget( pullDownMenu_top_b5 );

	UxCreateWidget( workAreaForm );
	UxPutTopOffset( workAreaFrame, 0 );
	UxPutTopAttachment( workAreaFrame, "attach_form" );
	UxPutRightAttachment( workAreaFrame, "attach_form" );
	UxPutLeftAttachment( workAreaFrame, "attach_form" );
	UxPutBottomAttachment( workAreaFrame, "attach_form" );
	UxCreateWidget( workAreaFrame );

	UxCreateWidget( workArea );
	createCB_workArea( UxGetWidget( workArea ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutBottomOffset( SHelp, 125 );
	UxPutBottomAttachment( SHelp, "attach_form" );
	UxPutRightOffset( SHelp, 5 );
	UxPutRightAttachment( SHelp, "attach_form" );
	UxPutLeftOffset( SHelp, 5 );
	UxPutLeftAttachment( SHelp, "attach_form" );
	UxCreateWidget( SHelp );

	UxPutTopOffset( scrol2, 35 );
	UxPutTopAttachment( scrol2, "attach_form" );
	UxPutLeftOffset( scrol2, 176 );
	UxPutLeftAttachment( scrol2, "attach_form" );
	UxPutBottomOffset( scrol2, 184 );
	UxPutBottomAttachment( scrol2, "attach_form" );
	UxPutRightOffset( scrol2, -20 );
	UxPutRightAttachment( scrol2, "attach_form" );
	UxCreateWidget( scrol2 );

	UxCreateWidget( readtable );
	UxPutLeftOffset( scrol1, 5 );
	UxPutTopOffset( scrol1, 35 );
	UxPutTopAttachment( scrol1, "attach_form" );
	UxPutRightAttachment( scrol1, "attach_none" );
	UxPutLeftAttachment( scrol1, "attach_form" );
	UxPutBottomOffset( scrol1, 184 );
	UxPutBottomAttachment( scrol1, "attach_form" );
	UxCreateWidget( scrol1 );

	UxCreateWidget( identlist );
	UxPutLeftWidget( ColButton, "" );
	UxPutLeftOffset( ColButton, 20 );
	UxPutLeftAttachment( ColButton, "attach_form" );
	UxPutTopWidget( ColButton, "SHelp" );
	UxPutTopOffset( ColButton, 55 );
	UxPutTopAttachment( ColButton, "attach_widget" );
	UxPutBottomOffset( ColButton, 25 );
	UxPutBottomAttachment( ColButton, "attach_form" );
	UxCreateWidget( ColButton );

	UxPutTopPosition( textWindow, 0 );
	UxPutTopOffset( textWindow, 11 );
	UxPutTopAttachment( textWindow, "attach_form" );
	UxPutBottomOffset( textWindow, 360 );
	UxPutBottomAttachment( textWindow, "attach_form" );
	UxPutRightOffset( textWindow, -30 );
	UxPutRightAttachment( textWindow, "attach_form" );
	UxPutLeftOffset( textWindow, 182 );
	UxPutLeftAttachment( textWindow, "attach_form" );
	UxCreateWidget( textWindow );

	UxCreateWidget( scrollabel );
	UxPutRightWidget( IDENT, "textWindow" );
	UxPutRightOffset( IDENT, 0 );
	UxPutRightAttachment( IDENT, "attach_widget" );
	UxPutBottomWidget( IDENT, "scrol1" );
	UxPutBottomAttachment( IDENT, "attach_widget" );
	UxPutTopOffset( IDENT, 0 );
	UxPutTopAttachment( IDENT, "attach_form" );
	UxPutLeftOffset( IDENT, -20 );
	UxPutLeftAttachment( IDENT, "attach_form" );
	UxCreateWidget( IDENT );

	UxPutLeftWidget( RowButton, "ColButton" );
	UxPutBottomOffset( RowButton, 25 );
	UxPutBottomAttachment( RowButton, "attach_form" );
	UxPutTopWidget( RowButton, "SHelp" );
	UxPutTopOffset( RowButton, 55 );
	UxPutTopAttachment( RowButton, "attach_widget" );
	UxPutLeftOffset( RowButton, 10 );
	UxPutLeftAttachment( RowButton, "attach_widget" );
	UxCreateWidget( RowButton );

	UxPutBottomOffset( label9, 75 );
	UxPutBottomAttachment( label9, "attach_form" );
	UxPutLeftOffset( label9, 80 );
	UxPutLeftAttachment( label9, "attach_form" );
	UxCreateWidget( label9 );

	UxPutLeftOffset( separator6, 15 );
	UxPutLeftAttachment( separator6, "attach_form" );
	UxCreateWidget( separator6 );

	UxPutBottomOffset( separator7, 10 );
	UxPutBottomAttachment( separator7, "attach_form" );
	UxPutLeftOffset( separator7, 15 );
	UxPutLeftAttachment( separator7, "attach_form" );
	UxCreateWidget( separator7 );

	UxPutLeftOffset( separator8, 215 );
	UxPutLeftAttachment( separator8, "attach_form" );
	UxPutBottomOffset( separator8, 15 );
	UxPutBottomAttachment( separator8, "attach_form" );
	UxCreateWidget( separator8 );

	UxPutBottomOffset( separator9, 15 );
	UxPutBottomAttachment( separator9, "attach_form" );
	UxPutLeftOffset( separator9, 10 );
	UxPutLeftAttachment( separator9, "attach_form" );
	UxCreateWidget( separator9 );

	UxPutBottomOffset( label10, 75 );
	UxPutBottomAttachment( label10, "attach_form" );
	UxPutRightAttachment( label10, "attach_none" );
	UxPutLeftOffset( label10, 305 );
	UxPutLeftAttachment( label10, "attach_form" );
	UxCreateWidget( label10 );

	UxPutBottomOffset( separator10, 15 );
	UxPutBottomAttachment( separator10, "attach_form" );
	UxPutLeftOffset( separator10, 245 );
	UxPutLeftAttachment( separator10, "attach_form" );
	UxCreateWidget( separator10 );

	UxPutBottomOffset( separator11, 10 );
	UxPutBottomAttachment( separator11, "attach_form" );
	UxPutLeftOffset( separator11, 250 );
	UxPutLeftAttachment( separator11, "attach_form" );
	UxCreateWidget( separator11 );

	UxPutTopOffset( separator12, 410 );
	UxPutTopAttachment( separator12, "attach_form" );
	UxPutLeftOffset( separator12, 250 );
	UxPutLeftAttachment( separator12, "attach_form" );
	UxCreateWidget( separator12 );

	UxPutBottomOffset( separator13, 15 );
	UxPutBottomAttachment( separator13, "attach_form" );
	UxPutLeftOffset( separator13, 450 );
	UxPutLeftAttachment( separator13, "attach_form" );
	UxCreateWidget( separator13 );

	UxPutBottomOffset( AssoButton1, 25 );
	UxPutBottomAttachment( AssoButton1, "attach_form" );
	UxPutTopWidget( AssoButton1, "SHelp" );
	UxPutTopOffset( AssoButton1, 55 );
	UxPutTopAttachment( AssoButton1, "attach_widget" );
	UxPutRightOffset( AssoButton1, 70 );
	UxPutRightAttachment( AssoButton1, "attach_form" );
	UxCreateWidget( AssoButton1 );

	UxPutBottomOffset( ClassiButton, 25 );
	UxPutBottomAttachment( ClassiButton, "attach_form" );
	UxPutLeftWidget( ClassiButton, "RowButton" );
	UxPutLeftOffset( ClassiButton, 45 );
	UxPutLeftAttachment( ClassiButton, "attach_widget" );
	UxPutTopWidget( ClassiButton, "SHelp" );
	UxPutTopOffset( ClassiButton, 55 );
	UxPutTopAttachment( ClassiButton, "attach_widget" );
	UxCreateWidget( ClassiButton );

	UxPutTopWidget( AssoButton, "SHelp" );
	UxPutTopOffset( AssoButton, 55 );
	UxPutTopAttachment( AssoButton, "attach_widget" );
	UxPutLeftWidget( AssoButton, "ClassiButton" );
	UxPutLeftOffset( AssoButton, 10 );
	UxPutLeftAttachment( AssoButton, "attach_widget" );
	UxPutBottomOffset( AssoButton, 25 );
	UxPutBottomAttachment( AssoButton, "attach_form" );
	UxCreateWidget( AssoButton );


	UxAddCallback( ApplicWindow, XmNpopupCallback,
			popupCB_ApplicWindow,
			(XtPointer) UxApplicWindowContext );

	UxPutMenuHelpWidget( pullDownMenu, "pullDownMenu_top_b1" );

	UxAddCallback( quitPane_b2, XmNactivateCallback,
			activateCB_quitPane_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( UtilsPane_b5, XmNactivateCallback,
			activateCB_UtilsPane_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( UtilsPane_b6, XmNactivateCallback,
			activateCB_UtilsPane_b6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pullDownMenu_p3_b1, XmNactivateCallback,
			activateCB_pullDownMenu_p3_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( HelpPane_b2, XmNactivateCallback,
			activateCB_HelpPane_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( HelpPane_b3, XmNactivateCallback,
			activateCB_HelpPane_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( workArea, XmNmapCallback,
			mapCB_workArea,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( SHelp, XmNactivateCallback,
			activateCB_SHelp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( readtable, XmNextendedSelectionCallback,
			extendedSelectionCB_readtable,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( readtable, XmNbrowseSelectionCallback,
			browseSelectionCB_readtable,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( identlist, XmNextendedSelectionCallback,
			extendedSelectionCB_identlist,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( ColButton, XmNactivateCallback,
			activateCB_ColButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( RowButton, XmNactivateCallback,
			activateCB_RowButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( AssoButton1, XmNactivateCallback,
			activateCB_AssoButton1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( ClassiButton, XmNactivateCallback,
			activateCB_ClassiButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( AssoButton, XmNactivateCallback,
			activateCB_AssoButton,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( mainWindow, pullDownMenu, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, workAreaForm );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_popup_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	rtrn = _Uxbuild_ApplicWindow();

	XtVaGetValues(UxGetWidget(UxFindSwidget("scrol2")),XmNverticalScrollBar,&sb1,NULL);
	XtVaGetValues(UxGetWidget(UxFindSwidget("scrol1")),XmNverticalScrollBar,&sb,NULL);
	/*UxUnmap(UxWidgetToSwidget(sb1));*/
	/*XtVaGetValues(sb,XmNmaximum,&height,NULL);
	printf("height:%d \n",height);*/
	/*UxPutMappedWhenManaged(UxWidgetToSwidget(sb1),"false");*/
	XtUnmapWidget(sb1);
	XtVaGetValues(UxGetWidget(UxFindSwidget("textWindow")),XmNhorizontalScrollBar,&sb2,NULL);
	XtUnmapWidget(sb2);
	XtVaGetValues(UxGetWidget(UxFindSwidget("textWindow")),XmNverticalScrollBar,&sb2,NULL);
	XtUnmapWidget(sb2);
	/*UxPutTranslations(UxWidgetToSwidget(sb),transTable1);*/
	/*XtAddCallback(sb,XmNvalueChangedCallback,myscrollv,XmVERTICAL);*/
	/*XtVaGetValues(UxGetWidget(UxFindSwidget("identlist")),XmNheight,&height,XmNvisibleItemCount,&count,NULL);
	itemsize = height/count;
	printf("size:%d,height: %d, count: %d",itemsize,height,count);*/
	
	
	XtRemoveAllCallbacks(sb,XmNdragCallback);
	XtRemoveAllCallbacks(sb,XmNvalueChangedCallback);
	XtRemoveAllCallbacks(sb,XmNincrementCallback);
	XtRemoveAllCallbacks(sb,XmNdecrementCallback);
	XtRemoveAllCallbacks(sb,XmNpageIncrementCallback);
	XtRemoveAllCallbacks(sb,XmNpageDecrementCallback);
	XtRemoveAllCallbacks(sb,XmNtoTopCallback);
	XtRemoveAllCallbacks(sb,XmNtoBottomCallback);
	
	XtAddCallback(sb,XmNdragCallback,myscrollv,NULL);
	XtAddCallback(sb,XmNincrementCallback,myscrollv,NULL);
	XtAddCallback(sb,XmNdecrementCallback,myscrollv,NULL);
	XtAddCallback(sb,XmNpageIncrementCallback,myscrollv,NULL);
	XtAddCallback(sb,XmNpageDecrementCallback,myscrollv,NULL);
	XtAddCallback(sb,XmNtoTopCallback,myscrollv,NULL);
	XtAddCallback(sb,XmNtoBottomCallback,myscrollv,NULL);
	
	XtVaGetValues(UxGetWidget(UxFindSwidget("scrol2")),XmNhorizontalScrollBar,&hbs,NULL);
	XtAddCallback(hbs,XmNdragCallback,myscrollh,NULL);
	XtAddCallback(hbs,XmNincrementCallback,myscrollh,NULL);
	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	popup_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HInit", action_HInit },
				{ "ChangeFor", action_ChangeFor },
				{ "nothing", action_nothing },
				{ "SelectCommand", action_SelectCommand },
				{ "ClearHelp", action_ClearHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "HelpACT", action_HelpACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		UxLoadResources( "ApplicWindow.rf" );
		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_popup_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

