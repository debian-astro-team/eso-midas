
/*******************************************************************************
	transientShell1.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <midas_def.h>
#include <tbldef.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell1;
	swidget	Uxform2;
	swidget	UxscrolledWindow3;
	swidget	UxpushButton3;
	swidget	Uxapply;
	swidget	UxscrolledWindow8;
	swidget	Uxlabel22;
	swidget	Uxlabel23;
} _UxCtransientShell1;

#define transientShell1         UxTransientShell1Context->UxtransientShell1
#define form2                   UxTransientShell1Context->Uxform2
#define scrolledWindow3         UxTransientShell1Context->UxscrolledWindow3
#define pushButton3             UxTransientShell1Context->UxpushButton3
#define apply                   UxTransientShell1Context->Uxapply
#define scrolledWindow8         UxTransientShell1Context->UxscrolledWindow8
#define label22                 UxTransientShell1Context->Uxlabel22
#define label23                 UxTransientShell1Context->Uxlabel23

static _UxCtransientShell1	*UxTransientShell1Context;

extern void display_col_table();
extern int selectlist(), getselectlist();



swidget	columnlist;
swidget	columnlist1;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell1();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	extendedSelectionCB_columnlist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	UxTransientShell1Context = UxSaveCtx;
}

static void	browseSelectionCB_columnlist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_pushButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	extern swidget clist;
	UxPopdownInterface(clist);
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_apply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	extern swidget clist;
	getselectlist(UxGetWidget(UxFindSwidget("columnlist")));
	display_col_table(UxGetWidget(UxFindSwidget("readtable")));
	
	UxPopdownInterface(clist);
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	extendedSelectionCB_columnlist1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	 
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	browseSelectionCB_columnlist1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	XmListCallbackStruct *cbs;
	char *choice,mymode[50];
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	strcpy(mymode,choice);
	selectlist( mymode,UxGetWidget( UxFindSwidget("columnlist")));
	 XtFree(choice);
	}
	UxTransientShell1Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell1()
{
	UxPutTitle( transientShell1, "Select Columns" );
	UxPutHeight( transientShell1, 580 );
	UxPutWidth( transientShell1, 290 );
	UxPutY( transientShell1, 100 );
	UxPutX( transientShell1, 790 );

	UxPutBackground( form2, ApplicBackground );
	UxPutHeight( form2, 240 );
	UxPutWidth( form2, 290 );
	UxPutY( form2, 0 );
	UxPutX( form2, 0 );
	UxPutUnitType( form2, "pixels" );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutScrollBarPlacement( scrolledWindow3, "bottom_left" );
	UxPutBackground( scrolledWindow3, ApplicBackground );
	UxPutWidth( scrolledWindow3, 270 );
	UxPutHeight( scrolledWindow3, 316 );
	UxPutShadowThickness( scrolledWindow3, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow3, "static" );
	UxPutVisualPolicy( scrolledWindow3, "variable" );
	UxPutY( scrolledWindow3, 10 );
	UxPutX( scrolledWindow3, 10 );
	UxPutScrollingPolicy( scrolledWindow3, "application_defined" );

	UxPutAutomaticSelection( columnlist, "false" );
	UxPutSelectionPolicy( columnlist, "extended_select" );
	UxPutVisibleItemCount( columnlist, 16 );
	UxPutScrollBarDisplayPolicy( columnlist, "static" );
	UxPutForeground( columnlist, TextForeground );
	UxPutFontList( columnlist, TextFont );
	UxPutBackground( columnlist, TextBackground );
	UxPutHeight( columnlist, 320 );
	UxPutWidth( columnlist, 190 );

	UxPutForeground( pushButton3, ButtonForeground );
	UxPutFontList( pushButton3, BoldTextFont );
	UxPutLabelString( pushButton3, "cancel" );
	UxPutBackground( pushButton3, ButtonBackground );
	UxPutHeight( pushButton3, 30 );
	UxPutWidth( pushButton3, 200 );
	UxPutY( pushButton3, 318 );
	UxPutX( pushButton3, 0 );

	UxPutForeground( apply, ButtonForeground );
	UxPutFontList( apply, BoldTextFont );
	UxPutLabelString( apply, "apply" );
	UxPutBackground( apply, ButtonBackground );
	UxPutHeight( apply, 30 );
	UxPutWidth( apply, 200 );
	UxPutY( apply, 330 );
	UxPutX( apply, 160 );

	UxPutScrollBarPlacement( scrolledWindow8, "bottom_left" );
	UxPutBackground( scrolledWindow8, ApplicBackground );
	UxPutWidth( scrolledWindow8, 270 );
	UxPutHeight( scrolledWindow8, 316 );
	UxPutShadowThickness( scrolledWindow8, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow8, "static" );
	UxPutVisualPolicy( scrolledWindow8, "variable" );
	UxPutY( scrolledWindow8, 10 );
	UxPutX( scrolledWindow8, 10 );
	UxPutScrollingPolicy( scrolledWindow8, "application_defined" );

	UxPutSelectionPolicy( columnlist1, "browse_select" );
	UxPutVisibleItemCount( columnlist1, 9 );
	UxPutScrollBarDisplayPolicy( columnlist1, "static" );
	UxPutForeground( columnlist1, TextForeground );
	UxPutFontList( columnlist1, TextFont );
	UxPutBackground( columnlist1, TextBackground );
	UxPutHeight( columnlist1, 320 );
	UxPutWidth( columnlist1, 190 );

	UxPutLabelString( label22, "Instrument Mode" );
	UxPutFontList( label22, BoldTextFont );
	UxPutBackground( label22, WindowBackground );
	UxPutHeight( label22, 30 );
	UxPutWidth( label22, 270 );
	UxPutY( label22, 0 );
	UxPutX( label22, 20 );

	UxPutLabelString( label23, "Columns " );
	UxPutFontList( label23, BoldTextFont );
	UxPutBackground( label23, WindowBackground );
	UxPutHeight( label23, 30 );
	UxPutWidth( label23, 270 );
	UxPutY( label23, 10 );
	UxPutX( label23, 10 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell1()
{
	/* Create the swidgets */

	transientShell1 = UxCreateTransientShell( "transientShell1", NO_PARENT );
	UxPutContext( transientShell1, UxTransientShell1Context );

	form2 = UxCreateForm( "form2", transientShell1 );
	scrolledWindow3 = UxCreateScrolledWindow( "scrolledWindow3", form2 );
	columnlist = UxCreateScrolledList( "columnlist", scrolledWindow3 );
	pushButton3 = UxCreatePushButton( "pushButton3", form2 );
	apply = UxCreatePushButton( "apply", form2 );
	scrolledWindow8 = UxCreateScrolledWindow( "scrolledWindow8", form2 );
	columnlist1 = UxCreateScrolledList( "columnlist1", scrolledWindow8 );
	label22 = UxCreateLabel( "label22", form2 );
	label23 = UxCreateLabel( "label23", form2 );

	_Uxinit_transientShell1();

	/* Create the X widgets */

	UxCreateWidget( transientShell1 );
	UxCreateWidget( form2 );
	UxPutTopOffset( scrolledWindow3, 225 );
	UxPutTopAttachment( scrolledWindow3, "attach_form" );
	UxPutRightOffset( scrolledWindow3, 2 );
	UxPutRightAttachment( scrolledWindow3, "attach_form" );
	UxPutLeftOffset( scrolledWindow3, 2 );
	UxPutLeftAttachment( scrolledWindow3, "attach_form" );
	UxPutBottomOffset( scrolledWindow3, 45 );
	UxPutBottomAttachment( scrolledWindow3, "attach_form" );
	UxCreateWidget( scrolledWindow3 );

	UxCreateWidget( columnlist );
	UxPutRightOffset( pushButton3, 10 );
	UxPutRightAttachment( pushButton3, "attach_form" );
	UxPutLeftOffset( pushButton3, 170 );
	UxPutLeftAttachment( pushButton3, "attach_form" );
	UxPutBottomOffset( pushButton3, 5 );
	UxPutBottomAttachment( pushButton3, "attach_form" );
	UxCreateWidget( pushButton3 );

	UxPutRightOffset( apply, 160 );
	UxPutRightAttachment( apply, "attach_form" );
	UxPutLeftAttachment( apply, "attach_form" );
	UxPutBottomOffset( apply, 5 );
	UxPutBottomAttachment( apply, "attach_form" );
	UxPutLeftOffset( apply, 10 );
	UxCreateWidget( apply );

	UxPutBottomOffset( scrolledWindow8, 380 );
	UxPutBottomAttachment( scrolledWindow8, "attach_form" );
	UxPutLeftOffset( scrolledWindow8, 0 );
	UxPutLeftAttachment( scrolledWindow8, "attach_form" );
	UxPutTopOffset( scrolledWindow8, 30 );
	UxPutTopAttachment( scrolledWindow8, "attach_form" );
	UxPutRightOffset( scrolledWindow8, 0 );
	UxPutRightAttachment( scrolledWindow8, "attach_form" );
	UxCreateWidget( scrolledWindow8 );

	UxCreateWidget( columnlist1 );
	UxPutTopOffset( label22, 0 );
	UxPutTopAttachment( label22, "attach_form" );
	UxPutRightOffset( label22, 0 );
	UxPutRightAttachment( label22, "attach_form" );
	UxPutLeftOffset( label22, 0 );
	UxPutLeftAttachment( label22, "attach_form" );
	UxCreateWidget( label22 );

	UxPutTopOffset( label23, 195 );
	UxPutTopAttachment( label23, "attach_form" );
	UxPutRightOffset( label23, 0 );
	UxPutRightAttachment( label23, "attach_form" );
	UxPutLeftOffset( label23, 0 );
	UxPutLeftAttachment( label23, "attach_form" );
	UxCreateWidget( label23 );


	UxAddCallback( columnlist, XmNextendedSelectionCallback,
			extendedSelectionCB_columnlist,
			(XtPointer) UxTransientShell1Context );
	UxAddCallback( columnlist, XmNbrowseSelectionCallback,
			browseSelectionCB_columnlist,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( pushButton3, XmNactivateCallback,
			activateCB_pushButton3,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( apply, XmNactivateCallback,
			activateCB_apply,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( columnlist1, XmNextendedSelectionCallback,
			extendedSelectionCB_columnlist1,
			(XtPointer) UxTransientShell1Context );
	UxAddCallback( columnlist1, XmNbrowseSelectionCallback,
			browseSelectionCB_columnlist1,
			(XtPointer) UxTransientShell1Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell1 );

	return ( transientShell1 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell1()
{
	swidget                 rtrn;
	_UxCtransientShell1     *UxContext;

	UxTransientShell1Context = UxContext =
		(_UxCtransientShell1 *) UxMalloc( sizeof(_UxCtransientShell1) );

	rtrn = _Uxbuild_transientShell1();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell1()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_transientShell1();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

