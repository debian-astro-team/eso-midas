
/*******************************************************************************
	Association.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxAssociation;
	swidget	Uxform5;
	swidget	UxscrolledWindow5;
	swidget	UxrowColumn2;
	swidget	Uxt_1;
	swidget	Uxt1_1;
	swidget	Uxt2_1;
	swidget	Uxt_2;
	swidget	Uxt_3;
	swidget	Uxt_4;
	swidget	Uxt_5;
	swidget	Uxt_6;
	swidget	Uxt1_2;
	swidget	Uxt1_3;
	swidget	Uxt1_4;
	swidget	Uxt1_5;
	swidget	Uxt1_6;
	swidget	Uxt2_2;
	swidget	Uxt2_3;
	swidget	Uxt2_4;
	swidget	Uxt2_5;
	swidget	Uxt3_1;
	swidget	Uxp_1;
	swidget	Uxp_2;
	swidget	Uxt2_6;
	swidget	Uxt3_2;
	swidget	Uxt3_3;
	swidget	Uxt3_4;
	swidget	Uxt3_5;
	swidget	Uxt3_6;
	swidget	Uxp_3;
	swidget	Uxp_4;
	swidget	Uxp_5;
	swidget	Uxp_6;
	swidget	Uxlabel18;
	swidget	Uxtext4;
	swidget	Uxtext5;
	swidget	UxpushButton1;
	swidget	UxpushButton7;
	swidget	Uxlabel20;
} _UxCAssociation;

#define Association             UxAssociationContext->UxAssociation
#define form5                   UxAssociationContext->Uxform5
#define scrolledWindow5         UxAssociationContext->UxscrolledWindow5
#define rowColumn2              UxAssociationContext->UxrowColumn2
#define t_1                     UxAssociationContext->Uxt_1
#define t1_1                    UxAssociationContext->Uxt1_1
#define t2_1                    UxAssociationContext->Uxt2_1
#define t_2                     UxAssociationContext->Uxt_2
#define t_3                     UxAssociationContext->Uxt_3
#define t_4                     UxAssociationContext->Uxt_4
#define t_5                     UxAssociationContext->Uxt_5
#define t_6                     UxAssociationContext->Uxt_6
#define t1_2                    UxAssociationContext->Uxt1_2
#define t1_3                    UxAssociationContext->Uxt1_3
#define t1_4                    UxAssociationContext->Uxt1_4
#define t1_5                    UxAssociationContext->Uxt1_5
#define t1_6                    UxAssociationContext->Uxt1_6
#define t2_2                    UxAssociationContext->Uxt2_2
#define t2_3                    UxAssociationContext->Uxt2_3
#define t2_4                    UxAssociationContext->Uxt2_4
#define t2_5                    UxAssociationContext->Uxt2_5
#define t3_1                    UxAssociationContext->Uxt3_1
#define p_1                     UxAssociationContext->Uxp_1
#define p_2                     UxAssociationContext->Uxp_2
#define t2_6                    UxAssociationContext->Uxt2_6
#define t3_2                    UxAssociationContext->Uxt3_2
#define t3_3                    UxAssociationContext->Uxt3_3
#define t3_4                    UxAssociationContext->Uxt3_4
#define t3_5                    UxAssociationContext->Uxt3_5
#define t3_6                    UxAssociationContext->Uxt3_6
#define p_3                     UxAssociationContext->Uxp_3
#define p_4                     UxAssociationContext->Uxp_4
#define p_5                     UxAssociationContext->Uxp_5
#define p_6                     UxAssociationContext->Uxp_6
#define label18                 UxAssociationContext->Uxlabel18
#define text4                   UxAssociationContext->Uxtext4
#define text5                   UxAssociationContext->Uxtext5
#define pushButton1             UxAssociationContext->UxpushButton1
#define pushButton7             UxAssociationContext->UxpushButton7
#define label20                 UxAssociationContext->Uxlabel20

static _UxCAssociation	*UxAssociationContext;

extern int AppendDialogText();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Association();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_p_1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	extern  swidget asso;
	
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	int rows = 5;
	int i;
	extern int arowno;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);
	
	if (!arow1[0]) {
	   arowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"a1_%d",i);
	   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);
	   sprintf(mytext,"a2_%d",i);
	   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a3_%d",i);
	   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a4_%d",i);
	   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   }
	 
	}
	
	UxPutText(UxFindSwidget("arule_name"),"bias_assorule");
	UxPopupInterface(asso,no_grab);
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_p_2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	
	
	extern  swidget asso;
	
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	int rows = 5;
	int i;
	extern int arowno;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);
	
	if (!arow1[0]) {
	   arowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"a1_%d",i);
	   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);
	   sprintf(mytext,"a2_%d",i);
	   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a3_%d",i);
	   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a4_%d",i);
	   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   }
	 
	}
	
	
	UxPutText(UxFindSwidget("arule_name"),"dark_assorule");
	UxPopupInterface(asso,no_grab);
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_p_3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	extern swidget asso;
	
	
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	int rows = 5;
	int i;
	extern int arowno;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);
	
	if (!arow1[0]) {
	   arowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"a1_%d",i);
	   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);
	   sprintf(mytext,"a2_%d",i);
	   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a3_%d",i);
	   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a4_%d",i);
	   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   }
	 
	}
	
	UxPutText(UxFindSwidget("arule_name"),"ff_assorule");
	UxPopupInterface(asso,no_grab);
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_p_4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	extern swidget asso;
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	int rows = 5;
	int i;
	extern int arowno;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);
	
	if (!arow1[0]) {
	   arowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"a1_%d",i);
	   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);
	   sprintf(mytext,"a2_%d",i);
	   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a3_%d",i);
	   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a4_%d",i);
	   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   }
	 
	}
	
	UxPutText(UxFindSwidget("arule_name"),"wcal_assorule");
	UxPopupInterface(asso,no_grab);
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_p_5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	extern swidget asso;extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	int rows = 5;
	int i;
	extern int arowno;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);
	
	if (!arow1[0]) {
	   arowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"a1_%d",i);
	   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);
	   sprintf(mytext,"a2_%d",i);
	   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a3_%d",i);
	   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a4_%d",i);
	   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   }
	 
	}
	
	
	UxPopupInterface(asso,no_grab);
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_p_6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	extern swidget asso;extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	int rows = 5;
	int i;
	extern int arowno;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,rows,NULL);
	
	if (!arow1[0]) {
	   arowno = 5;
	  for (i=0; i<rows; i++) 
	   {
	   sprintf(mytext,"a1_%d",i);
	   arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),XmNwidth,210,NULL);
	   sprintf(mytext,"a2_%d",i);
	   arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a3_%d",i);
	   arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   sprintf(mytext,"a4_%d",i);
	   arow4[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(UxFindSwidget("rowasso")),NULL);
	   }
	 
	}
	
	
	UxPopupInterface(asso,no_grab);
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_pushButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	
	extern char ostchoice[60];
	extern int tidost;
	extern swidget myerror;
	char command[160];
	char myw[4],cbuf[8],expo[8],nexpo[8],mt1[5],filename[60],oname[60];
	int i,status,loop;
	loop = 0;
	if (tidost == -1 ) {
	  UxPutMessageString(UxFindSwidget("errorDialog1"),"NO OST opened");
	  UxPopupInterface(myerror,no_grab);
	  return;
	}
	  
	for (i=0; i<6;i++) {
	   sprintf(myw,"t_%d",i+1);
	   strcpy(cbuf,UxGetSet(UxFindSwidget(myw)));
	   strcpy(command,"ASSOCIATE/IMAGE ");
	   strcpy(oname,UxGetText(UxFindSwidget("text4")));
	   if (oname[0] == '\0') {
	       UxPutMessageString(UxFindSwidget("errorDialog1"),"No Output Table Name provided");
	       UxPopupInterface(myerror,no_grab);
	       return;
	       }
	if (cbuf[0] == 't') {
	       sprintf(mt1,"t3_%d",i+1);
	       strcpy(filename,UxGetText(UxFindSwidget(mt1)));
	       sprintf(mt1,"t1_%d",i+1);
	       strcpy(expo,UxGetText(UxFindSwidget(mt1)));
	       sprintf(mt1,"t2_%d",i+1);
	       strcpy(nexpo,UxGetText(UxFindSwidget(mt1)));
	       if (nexpo[0] == '\0') strcpy(nexpo,"1");
	       if (loop == 0)
	       sprintf(command,"ASSOCIATE/IMAGE %s %s %s %s C %s",ostchoice,expo,filename,oname,nexpo);
	       else
	       sprintf(command,"ASSOCIATE/IMAGE %s %s %s %s A %s",ostchoice,expo,filename,oname,nexpo);
	       loop++;
	       status = AppendDialogText(command);
	       }
	}
	}
	UxAssociationContext = UxSaveCtx;
}

static void	activateCB_pushButton7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociation         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociationContext;
	UxAssociationContext = UxContext =
			(_UxCAssociation *) UxGetContext( UxThisWidget );
	{
	extern swidget assohaupt;
	UxPopdownInterface(assohaupt);
	}
	UxAssociationContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Association()
{
	UxPutTitle( Association, "Association of Images" );
	UxPutHeight( Association, 540 );
	UxPutWidth( Association, 610 );
	UxPutY( Association, 170 );
	UxPutX( Association, 630 );

	UxPutBackground( form5, WindowBackground );
	UxPutHeight( form5, 540 );
	UxPutWidth( form5, 610 );
	UxPutY( form5, 0 );
	UxPutX( form5, 0 );
	UxPutUnitType( form5, "pixels" );
	UxPutResizePolicy( form5, "resize_none" );

	UxPutBackground( scrolledWindow5, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow5, "bottom_left" );
	UxPutHeight( scrolledWindow5, 215 );
	UxPutWidth( scrolledWindow5, 550 );
	UxPutY( scrolledWindow5, 20 );
	UxPutX( scrolledWindow5, 280 );
	UxPutScrollingPolicy( scrolledWindow5, "automatic" );

	UxPutTraversalOn( rowColumn2, "false" );
	UxPutPopupEnabled( rowColumn2, "true" );
	UxPutRadioAlwaysOne( rowColumn2, "true" );
	UxPutResizeWidth( rowColumn2, "false" );
	UxPutSensitive( rowColumn2, "true" );
	UxPutBackground( rowColumn2, WindowBackground );
	UxPutPacking( rowColumn2, "pack_none" );
	UxPutOrientation( rowColumn2, "horizontal" );
	UxPutHeight( rowColumn2, 220 );
	UxPutWidth( rowColumn2, 520 );
	UxPutY( rowColumn2, -2 );
	UxPutX( rowColumn2, 8 );

	UxPutSelectColor( t_1, "PaleGreen" );
	UxPutIndicatorType( t_1, "one_of_many" );
	UxPutBackground( t_1, WindowBackground );
	UxPutLabelString( t_1, "" );
	UxPutHeight( t_1, 30 );
	UxPutWidth( t_1, 40 );
	UxPutY( t_1, 15 );
	UxPutX( t_1, 0 );

	UxPutText( t1_1, "BIAS" );
	UxPutFontList( t1_1, BoldTextFont );
	UxPutBackground( t1_1, TextBackground );
	UxPutHeight( t1_1, 30 );
	UxPutWidth( t1_1, 80 );
	UxPutY( t1_1, 10 );
	UxPutX( t1_1, 40 );

	UxPutFontList( t2_1, TextFont );
	UxPutBackground( t2_1, TextBackground );
	UxPutHeight( t2_1, 30 );
	UxPutWidth( t2_1, 80 );
	UxPutY( t2_1, 10 );
	UxPutX( t2_1, 150 );

	UxPutSelectColor( t_2, "PaleGreen" );
	UxPutIndicatorType( t_2, "one_of_many" );
	UxPutBackground( t_2, WindowBackground );
	UxPutLabelString( t_2, "" );
	UxPutHeight( t_2, 30 );
	UxPutWidth( t_2, 40 );
	UxPutY( t_2, 60 );
	UxPutX( t_2, 0 );

	UxPutSelectColor( t_3, "PaleGreen" );
	UxPutIndicatorType( t_3, "one_of_many" );
	UxPutBackground( t_3, WindowBackground );
	UxPutLabelString( t_3, "" );
	UxPutHeight( t_3, 30 );
	UxPutWidth( t_3, 40 );
	UxPutY( t_3, 105 );
	UxPutX( t_3, 0 );

	UxPutSelectColor( t_4, "PaleGreen" );
	UxPutIndicatorType( t_4, "one_of_many" );
	UxPutBackground( t_4, WindowBackground );
	UxPutLabelString( t_4, "" );
	UxPutHeight( t_4, 30 );
	UxPutWidth( t_4, 40 );
	UxPutY( t_4, 150 );
	UxPutX( t_4, 0 );

	UxPutSelectColor( t_5, "PaleGreen" );
	UxPutIndicatorType( t_5, "one_of_many" );
	UxPutBackground( t_5, WindowBackground );
	UxPutLabelString( t_5, "" );
	UxPutHeight( t_5, 30 );
	UxPutWidth( t_5, 40 );
	UxPutY( t_5, 195 );
	UxPutX( t_5, 0 );

	UxPutSelectColor( t_6, "PaleGreen" );
	UxPutIndicatorType( t_6, "one_of_many" );
	UxPutBackground( t_6, WindowBackground );
	UxPutLabelString( t_6, "" );
	UxPutHeight( t_6, 30 );
	UxPutWidth( t_6, 40 );
	UxPutY( t_6, 240 );
	UxPutX( t_6, 0 );

	UxPutText( t1_2, "DARK" );
	UxPutFontList( t1_2, BoldTextFont );
	UxPutBackground( t1_2, TextBackground );
	UxPutHeight( t1_2, 20 );
	UxPutWidth( t1_2, 80 );
	UxPutY( t1_2, 55 );
	UxPutX( t1_2, 40 );

	UxPutText( t1_3, "FF" );
	UxPutFontList( t1_3, BoldTextFont );
	UxPutBackground( t1_3, TextBackground );
	UxPutHeight( t1_3, 20 );
	UxPutWidth( t1_3, 80 );
	UxPutY( t1_3, 100 );
	UxPutX( t1_3, 40 );

	UxPutText( t1_4, "WCAL" );
	UxPutFontList( t1_4, BoldTextFont );
	UxPutBackground( t1_4, TextBackground );
	UxPutHeight( t1_4, 20 );
	UxPutWidth( t1_4, 80 );
	UxPutY( t1_4, 145 );
	UxPutX( t1_4, 40 );

	UxPutFontList( t1_5, BoldTextFont );
	UxPutBackground( t1_5, TextBackground );
	UxPutHeight( t1_5, 20 );
	UxPutWidth( t1_5, 80 );
	UxPutY( t1_5, 190 );
	UxPutX( t1_5, 40 );

	UxPutFontList( t1_6, BoldTextFont );
	UxPutBackground( t1_6, TextBackground );
	UxPutHeight( t1_6, 20 );
	UxPutWidth( t1_6, 80 );
	UxPutY( t1_6, 235 );
	UxPutX( t1_6, 40 );

	UxPutFontList( t2_2, TextFont );
	UxPutBackground( t2_2, TextBackground );
	UxPutHeight( t2_2, 30 );
	UxPutWidth( t2_2, 80 );
	UxPutY( t2_2, 55 );
	UxPutX( t2_2, 150 );

	UxPutFontList( t2_3, TextFont );
	UxPutBackground( t2_3, TextBackground );
	UxPutHeight( t2_3, 30 );
	UxPutWidth( t2_3, 80 );
	UxPutY( t2_3, 100 );
	UxPutX( t2_3, 150 );

	UxPutFontList( t2_4, TextFont );
	UxPutBackground( t2_4, TextBackground );
	UxPutHeight( t2_4, 30 );
	UxPutWidth( t2_4, 80 );
	UxPutY( t2_4, 145 );
	UxPutX( t2_4, 150 );

	UxPutFontList( t2_5, TextFont );
	UxPutBackground( t2_5, TextBackground );
	UxPutHeight( t2_5, 30 );
	UxPutWidth( t2_5, 80 );
	UxPutY( t2_5, 190 );
	UxPutX( t2_5, 150 );

	UxPutFontList( t3_1, TextFont );
	UxPutBackground( t3_1, TextBackground );
	UxPutHeight( t3_1, 30 );
	UxPutWidth( t3_1, 180 );
	UxPutY( t3_1, 10 );
	UxPutX( t3_1, 250 );

	UxPutLabelString( p_1, " ... " );
	UxPutBackground( p_1, ButtonBackground );
	UxPutFillOnArm( p_1, "true" );
	UxPutRecomputeSize( p_1, "true" );
	UxPutHeight( p_1, 30 );
	UxPutWidth( p_1, 90 );
	UxPutY( p_1, 5 );
	UxPutX( p_1, 460 );

	UxPutLabelString( p_2, " ... " );
	UxPutBackground( p_2, ButtonBackground );
	UxPutFillOnArm( p_2, "true" );
	UxPutRecomputeSize( p_2, "true" );
	UxPutHeight( p_2, 30 );
	UxPutWidth( p_2, 90 );
	UxPutY( p_2, 50 );
	UxPutX( p_2, 460 );

	UxPutFontList( t2_6, TextFont );
	UxPutBackground( t2_6, TextBackground );
	UxPutHeight( t2_6, 30 );
	UxPutWidth( t2_6, 80 );
	UxPutY( t2_6, 235 );
	UxPutX( t2_6, 150 );

	UxPutFontList( t3_2, TextFont );
	UxPutBackground( t3_2, TextBackground );
	UxPutHeight( t3_2, 30 );
	UxPutWidth( t3_2, 180 );
	UxPutY( t3_2, 55 );
	UxPutX( t3_2, 250 );

	UxPutFontList( t3_3, TextFont );
	UxPutBackground( t3_3, TextBackground );
	UxPutHeight( t3_3, 30 );
	UxPutWidth( t3_3, 180 );
	UxPutY( t3_3, 100 );
	UxPutX( t3_3, 250 );

	UxPutFontList( t3_4, TextFont );
	UxPutBackground( t3_4, TextBackground );
	UxPutHeight( t3_4, 30 );
	UxPutWidth( t3_4, 180 );
	UxPutY( t3_4, 145 );
	UxPutX( t3_4, 250 );

	UxPutFontList( t3_5, TextFont );
	UxPutBackground( t3_5, TextBackground );
	UxPutHeight( t3_5, 30 );
	UxPutWidth( t3_5, 180 );
	UxPutY( t3_5, 190 );
	UxPutX( t3_5, 250 );

	UxPutFontList( t3_6, TextFont );
	UxPutBackground( t3_6, TextBackground );
	UxPutHeight( t3_6, 30 );
	UxPutWidth( t3_6, 180 );
	UxPutY( t3_6, 235 );
	UxPutX( t3_6, 250 );

	UxPutLabelString( p_3, " ... " );
	UxPutBackground( p_3, ButtonBackground );
	UxPutFillOnArm( p_3, "true" );
	UxPutRecomputeSize( p_3, "true" );
	UxPutHeight( p_3, 30 );
	UxPutWidth( p_3, 90 );
	UxPutY( p_3, 95 );
	UxPutX( p_3, 460 );

	UxPutLabelString( p_4, " ... " );
	UxPutBackground( p_4, ButtonBackground );
	UxPutFillOnArm( p_4, "true" );
	UxPutRecomputeSize( p_4, "true" );
	UxPutHeight( p_4, 30 );
	UxPutWidth( p_4, 90 );
	UxPutY( p_4, 140 );
	UxPutX( p_4, 460 );

	UxPutLabelString( p_5, " ... " );
	UxPutBackground( p_5, ButtonBackground );
	UxPutFillOnArm( p_5, "true" );
	UxPutRecomputeSize( p_5, "true" );
	UxPutHeight( p_5, 30 );
	UxPutWidth( p_5, 90 );
	UxPutY( p_5, 185 );
	UxPutX( p_5, 460 );

	UxPutLabelString( p_6, " ... " );
	UxPutBackground( p_6, ButtonBackground );
	UxPutFillOnArm( p_6, "true" );
	UxPutRecomputeSize( p_6, "true" );
	UxPutHeight( p_6, 30 );
	UxPutWidth( p_6, 90 );
	UxPutY( p_6, 230 );
	UxPutX( p_6, 460 );

	UxPutLabelString( label18, "Output Table" );
	UxPutFontList( label18, BoldTextFont );
	UxPutBackground( label18, WindowBackground );
	UxPutHeight( label18, 50 );
	UxPutWidth( label18, 200 );
	UxPutY( label18, 300 );
	UxPutX( label18, 170 );

	UxPutBackground( text4, TextBackground );
	UxPutHeight( text4, 40 );
	UxPutWidth( text4, 210 );
	UxPutY( text4, 320 );
	UxPutX( text4, 230 );

	UxPutBackground( text5, SHelpBackground );
	UxPutHeight( text5, 40 );
	UxPutWidth( text5, 570 );
	UxPutY( text5, 450 );
	UxPutX( text5, 10 );

	UxPutLabelString( pushButton1, "Apply" );
	UxPutForeground( pushButton1, ButtonForeground );
	UxPutFontList( pushButton1, BoldTextFont );
	UxPutBackground( pushButton1, ButtonBackground );
	UxPutHeight( pushButton1, 40 );
	UxPutWidth( pushButton1, 90 );
	UxPutY( pushButton1, 460 );
	UxPutX( pushButton1, 170 );

	UxPutLabelString( pushButton7, "Cancel" );
	UxPutForeground( pushButton7, ButtonForeground );
	UxPutFontList( pushButton7, BoldTextFont );
	UxPutBackground( pushButton7, ButtonBackground );
	UxPutHeight( pushButton7, 40 );
	UxPutWidth( pushButton7, 90 );
	UxPutY( pushButton7, 470 );
	UxPutX( pushButton7, 180 );

	UxPutAlignment( label20, "alignment_beginning" );
	UxPutLabelType( label20, "string" );
	UxPutLabelString( label20, "Exposure Type        No of Exposures          Rule Table" );
	UxPutFontList( label20, BoldTextFont );
	UxPutBackground( label20, WindowBackground );
	UxPutHeight( label20, 50 );
	UxPutWidth( label20, 550 );
	UxPutY( label20, 350 );
	UxPutX( label20, 40 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Association()
{
	/* Create the swidgets */

	Association = UxCreateTransientShell( "Association", NO_PARENT );
	UxPutContext( Association, UxAssociationContext );

	form5 = UxCreateForm( "form5", Association );
	scrolledWindow5 = UxCreateScrolledWindow( "scrolledWindow5", form5 );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", scrolledWindow5 );
	t_1 = UxCreateToggleButton( "t_1", rowColumn2 );
	t1_1 = UxCreateText( "t1_1", rowColumn2 );
	t2_1 = UxCreateText( "t2_1", rowColumn2 );
	t_2 = UxCreateToggleButton( "t_2", rowColumn2 );
	t_3 = UxCreateToggleButton( "t_3", rowColumn2 );
	t_4 = UxCreateToggleButton( "t_4", rowColumn2 );
	t_5 = UxCreateToggleButton( "t_5", rowColumn2 );
	t_6 = UxCreateToggleButton( "t_6", rowColumn2 );
	t1_2 = UxCreateText( "t1_2", rowColumn2 );
	t1_3 = UxCreateText( "t1_3", rowColumn2 );
	t1_4 = UxCreateText( "t1_4", rowColumn2 );
	t1_5 = UxCreateText( "t1_5", rowColumn2 );
	t1_6 = UxCreateText( "t1_6", rowColumn2 );
	t2_2 = UxCreateText( "t2_2", rowColumn2 );
	t2_3 = UxCreateText( "t2_3", rowColumn2 );
	t2_4 = UxCreateText( "t2_4", rowColumn2 );
	t2_5 = UxCreateText( "t2_5", rowColumn2 );
	t3_1 = UxCreateText( "t3_1", rowColumn2 );
	p_1 = UxCreatePushButton( "p_1", rowColumn2 );
	p_2 = UxCreatePushButton( "p_2", rowColumn2 );
	t2_6 = UxCreateText( "t2_6", rowColumn2 );
	t3_2 = UxCreateText( "t3_2", rowColumn2 );
	t3_3 = UxCreateText( "t3_3", rowColumn2 );
	t3_4 = UxCreateText( "t3_4", rowColumn2 );
	t3_5 = UxCreateText( "t3_5", rowColumn2 );
	t3_6 = UxCreateText( "t3_6", rowColumn2 );
	p_3 = UxCreatePushButton( "p_3", rowColumn2 );
	p_4 = UxCreatePushButton( "p_4", rowColumn2 );
	p_5 = UxCreatePushButton( "p_5", rowColumn2 );
	p_6 = UxCreatePushButton( "p_6", rowColumn2 );
	label18 = UxCreateLabel( "label18", form5 );
	text4 = UxCreateText( "text4", form5 );
	text5 = UxCreateText( "text5", form5 );
	pushButton1 = UxCreatePushButton( "pushButton1", form5 );
	pushButton7 = UxCreatePushButton( "pushButton7", form5 );
	label20 = UxCreateLabel( "label20", form5 );

	_Uxinit_Association();

	/* Create the X widgets */

	UxCreateWidget( Association );
	UxCreateWidget( form5 );
	UxPutRightOffset( scrolledWindow5, 35 );
	UxPutRightAttachment( scrolledWindow5, "attach_form" );
	UxPutLeftOffset( scrolledWindow5, 35 );
	UxPutLeftAttachment( scrolledWindow5, "attach_form" );
	UxPutTopOffset( scrolledWindow5, 80 );
	UxPutTopAttachment( scrolledWindow5, "attach_form" );
	UxCreateWidget( scrolledWindow5 );

	UxCreateWidget( rowColumn2 );
	UxCreateWidget( t_1 );
	UxCreateWidget( t1_1 );
	UxCreateWidget( t2_1 );
	UxCreateWidget( t_2 );
	UxCreateWidget( t_3 );
	UxCreateWidget( t_4 );
	UxCreateWidget( t_5 );
	UxCreateWidget( t_6 );
	UxCreateWidget( t1_2 );
	UxCreateWidget( t1_3 );
	UxCreateWidget( t1_4 );
	UxCreateWidget( t1_5 );
	UxCreateWidget( t1_6 );
	UxCreateWidget( t2_2 );
	UxCreateWidget( t2_3 );
	UxCreateWidget( t2_4 );
	UxCreateWidget( t2_5 );
	UxCreateWidget( t3_1 );
	UxCreateWidget( p_1 );
	UxCreateWidget( p_2 );
	UxCreateWidget( t2_6 );
	UxCreateWidget( t3_2 );
	UxCreateWidget( t3_3 );
	UxCreateWidget( t3_4 );
	UxCreateWidget( t3_5 );
	UxCreateWidget( t3_6 );
	UxCreateWidget( p_3 );
	UxCreateWidget( p_4 );
	UxCreateWidget( p_5 );
	UxCreateWidget( p_6 );
	UxPutLeftAttachment( label18, "attach_form" );
	UxPutLeftOffset( label18, 30 );
	UxPutTopOffset( label18, 340 );
	UxPutTopAttachment( label18, "attach_form" );
	UxCreateWidget( label18 );

	UxPutLeftOffset( text4, 260 );
	UxPutTopOffset( text4, 345 );
	UxPutTopAttachment( text4, "attach_form" );
	UxCreateWidget( text4 );

	UxPutTopOffset( text5, 400 );
	UxPutTopAttachment( text5, "attach_form" );
	UxPutLeftAttachment( text5, "attach_form" );
	UxPutLeftOffset( text5, 5 );
	UxPutRightOffset( text5, 5 );
	UxPutRightAttachment( text5, "attach_form" );
	UxCreateWidget( text5 );

	UxPutLeftOffset( pushButton1, 100 );
	UxPutLeftAttachment( pushButton1, "attach_form" );
	UxPutBottomOffset( pushButton1, 30 );
	UxPutBottomAttachment( pushButton1, "attach_form" );
	UxCreateWidget( pushButton1 );

	UxPutRightAttachment( pushButton7, "attach_form" );
	UxPutRightOffset( pushButton7, 100 );
	UxPutBottomOffset( pushButton7, 30 );
	UxPutBottomAttachment( pushButton7, "attach_form" );
	UxCreateWidget( pushButton7 );

	UxPutLeftOffset( label20, 60 );
	UxPutLeftAttachment( label20, "attach_form" );
	UxPutTopOffset( label20, 20 );
	UxPutTopAttachment( label20, "attach_form" );
	UxCreateWidget( label20 );


	UxAddCallback( p_1, XmNactivateCallback,
			activateCB_p_1,
			(XtPointer) UxAssociationContext );

	UxAddCallback( p_2, XmNactivateCallback,
			activateCB_p_2,
			(XtPointer) UxAssociationContext );

	UxAddCallback( p_3, XmNactivateCallback,
			activateCB_p_3,
			(XtPointer) UxAssociationContext );

	UxAddCallback( p_4, XmNactivateCallback,
			activateCB_p_4,
			(XtPointer) UxAssociationContext );

	UxAddCallback( p_5, XmNactivateCallback,
			activateCB_p_5,
			(XtPointer) UxAssociationContext );

	UxAddCallback( p_6, XmNactivateCallback,
			activateCB_p_6,
			(XtPointer) UxAssociationContext );

	UxAddCallback( pushButton1, XmNactivateCallback,
			activateCB_pushButton1,
			(XtPointer) UxAssociationContext );

	UxAddCallback( pushButton7, XmNactivateCallback,
			activateCB_pushButton7,
			(XtPointer) UxAssociationContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Association );

	return ( Association );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Association()
{
	swidget                 rtrn;
	_UxCAssociation         *UxContext;

	UxAssociationContext = UxContext =
		(_UxCAssociation *) UxMalloc( sizeof(_UxCAssociation) );

	rtrn = _Uxbuild_Association();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Association()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_Association();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

