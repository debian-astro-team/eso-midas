
/*******************************************************************************
	Warning.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxMsgBD.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxWarning;
} _UxCWarning;

#define Warning                 UxWarningContext->UxWarning

static _UxCWarning	*UxWarningContext;

extern int AppendDialogText();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Warning();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	createCB_Warning( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWarning             *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWarningContext;
	UxWarningContext = UxContext =
			(_UxCWarning *) UxGetContext( UxThisWidget );
	{
	/*XtVaSetValues(UxWidget,XmNallowShellResize,True,NULL); 
	XtVaSetValues(UxWidget,XmNresizePolicy,XmRESIZE_ANY,NULL);*/
	XtVaSetValues(UxWidget,XmNdialogType,XmDIALOG_QUESTION,NULL);
	}
	UxWarningContext = UxSaveCtx;
}

static void	okCallback_Warning( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWarning             *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWarningContext;
	UxWarningContext = UxContext =
			(_UxCWarning *) UxGetContext( UxThisWidget );
	{
	        extern char commerr[180];
	 
	        AppendDialogText(commerr);
	
	}
	UxWarningContext = UxSaveCtx;
}

static void	cancelCB_Warning( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWarning             *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWarningContext;
	UxWarningContext = UxContext =
			(_UxCWarning *) UxGetContext( UxThisWidget );
	{
	
	}
	UxWarningContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Warning()
{
	UxPutResizePolicy( Warning, "resize_any" );
	UxPutAllowShellResize( Warning, "true" );
	UxPutBackground( Warning, WindowBackground );
	UxPutTextFontList( Warning, BoldTextFont );
	UxPutLabelFontList( Warning, TextFont );
	UxPutButtonFontList( Warning, BoldTextFont );
	UxPutDialogStyle( Warning, "dialog_modeless" );
	UxPutDialogType( Warning, "dialog_file_selection" );
	UxPutHeight( Warning, 150 );
	UxPutWidth( Warning, 290 );
	UxPutY( Warning, 600 );
	UxPutX( Warning, 600 );
	UxPutUnitType( Warning, "pixels" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Warning()
{
	/* Create the swidgets */

	Warning = UxCreateMessageBoxDialog( "Warning", NO_PARENT );
	UxPutContext( Warning, UxWarningContext );
	UxPutDefaultShell( Warning, "topLevelShell" );


	_Uxinit_Warning();

	/* Create the X widgets */

	UxCreateWidget( Warning );
	createCB_Warning( UxGetWidget( Warning ),
			(XtPointer) UxWarningContext, (XtPointer) NULL );


	UxAddCallback( Warning, XmNokCallback,
			okCallback_Warning,
			(XtPointer) UxWarningContext );
	UxAddCallback( Warning, XmNcancelCallback,
			cancelCB_Warning,
			(XtPointer) UxWarningContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Warning );

	return ( Warning );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Warning()
{
	swidget                 rtrn;
	_UxCWarning             *UxContext;

	UxWarningContext = UxContext =
		(_UxCWarning *) UxMalloc( sizeof(_UxCWarning) );

	rtrn = _Uxbuild_Warning();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Warning()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_Warning();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

