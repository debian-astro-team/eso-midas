
/*******************************************************************************
	transientShell8.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <midas_def.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell8;
	swidget	Uxform1;
	swidget	UxscrolledWindow2;
	swidget	UxpushButton2;
} _UxCtransientShell8;

#define transientShell8         UxTransientShell8Context->UxtransientShell8
#define form1                   UxTransientShell8Context->Uxform1
#define scrolledWindow2         UxTransientShell8Context->UxscrolledWindow2
#define pushButton2             UxTransientShell8Context->UxpushButton2

static _UxCtransientShell8	*UxTransientShell8Context;

extern int read_ost_table(), read_clas_table(), read_asso_table();

extern void add_ident_table(), display_col_table(), display_ident_table();




swidget	tablelist;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell8();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	browseSelectionCB_tablelist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell8     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell8Context;
	UxTransientShell8Context = UxContext =
			(_UxCtransientShell8 *) UxGetContext( UxThisWidget );
	{
	extern char ostchoice[60],claschoice[60],assochoice[60];
	extern char *colchoice; 
	extern int do8,tidcomm,tidost;
	extern swidget klist;
	XmListCallbackStruct *cbs;
	char *choice,type[2]  ;
	char table_descr[60] ;
	int null,actval,dummy,uni;
	 
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	if (!do8) {
	   strcpy(ostchoice,choice);
	   XtFree(choice);
	   UxPopdownInterface(klist);
	   if (read_ost_table())
	   display_ident_table(UxGetWidget(UxFindSwidget("identlist")));
	 /*  XtVaGetValues(UxGetWidget(UxFindSwidget("readtable")),XmNitemCount,&upper,NULL);
	   if (upper != 0) XmListDeleteAllItems(UxGetWidget(UxFindSwidget("readtable")));*/
	SCDFND(tidost,"table_descr",type,&dummy,&dummy);
	TCTOPN("TAB_COMM",F_IO_MODE,&tidcomm); 
	if (type[0] != ' ') {
	  SCDRDC(tidost ,"table_descr",1 ,1,60,&actval,table_descr,&uni ,&null);
	  TCEWRC(tidcomm,1,3,table_descr);
	  }
	TCEWRC(tidcomm,1,2,ostchoice);
	TCTCLO(tidcomm);
	 if (*colchoice)  
	 
	   display_col_table(UxGetWidget(UxFindSwidget("readtable")));
	  
	}
	else if (do8 == 1){
	     strcpy(claschoice,choice);
	     XtFree(choice);
	     UxPopdownInterface(klist);
	     read_clas_table();
	     }
	else {
	     strcpy(assochoice,choice);
	     XtFree(choice);
	     UxPopdownInterface(klist);
	     read_asso_table();
	     }
	}
	UxTransientShell8Context = UxSaveCtx;
}

static void	activateCB_pushButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell8     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell8Context;
	UxTransientShell8Context = UxContext =
			(_UxCtransientShell8 *) UxGetContext( UxThisWidget );
	{
	extern swidget klist;
	UxPopdownInterface(klist);
	}
	UxTransientShell8Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell8()
{
	UxPutTitle( transientShell8, "Select OST table" );
	UxPutHeight( transientShell8, 353 );
	UxPutWidth( transientShell8, 290 );
	UxPutY( transientShell8, 300 );
	UxPutX( transientShell8, 330 );

	UxPutBackground( form1, ApplicBackground );
	UxPutHeight( form1, 240 );
	UxPutWidth( form1, 290 );
	UxPutY( form1, 0 );
	UxPutX( form1, 0 );
	UxPutUnitType( form1, "pixels" );
	UxPutResizePolicy( form1, "resize_none" );

	UxPutScrollBarPlacement( scrolledWindow2, "bottom_left" );
	UxPutBackground( scrolledWindow2, ApplicBackground );
	UxPutWidth( scrolledWindow2, 270 );
	UxPutHeight( scrolledWindow2, 316 );
	UxPutShadowThickness( scrolledWindow2, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow2, "static" );
	UxPutVisualPolicy( scrolledWindow2, "variable" );
	UxPutY( scrolledWindow2, 10 );
	UxPutX( scrolledWindow2, 10 );
	UxPutScrollingPolicy( scrolledWindow2, "application_defined" );

	UxPutVisibleItemCount( tablelist, 16 );
	UxPutScrollBarDisplayPolicy( tablelist, "static" );
	UxPutForeground( tablelist, TextForeground );
	UxPutFontList( tablelist, TextFont );
	UxPutBackground( tablelist, TextBackground );
	UxPutHeight( tablelist, 320 );
	UxPutWidth( tablelist, 190 );

	UxPutForeground( pushButton2, CancelForeground );
	UxPutFontList( pushButton2, BoldTextFont );
	UxPutLabelString( pushButton2, "cancel" );
	UxPutBackground( pushButton2, ButtonBackground );
	UxPutHeight( pushButton2, 35 );
	UxPutWidth( pushButton2, 210 );
	UxPutY( pushButton2, 318 );
	UxPutX( pushButton2, 0 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell8()
{
	/* Create the swidgets */

	transientShell8 = UxCreateTransientShell( "transientShell8", NO_PARENT );
	UxPutContext( transientShell8, UxTransientShell8Context );

	form1 = UxCreateForm( "form1", transientShell8 );
	scrolledWindow2 = UxCreateScrolledWindow( "scrolledWindow2", form1 );
	tablelist = UxCreateScrolledList( "tablelist", scrolledWindow2 );
	pushButton2 = UxCreatePushButton( "pushButton2", form1 );

	_Uxinit_transientShell8();

	/* Create the X widgets */

	UxCreateWidget( transientShell8 );
	UxCreateWidget( form1 );
	UxPutTopOffset( scrolledWindow2, 0 );
	UxPutTopAttachment( scrolledWindow2, "attach_form" );
	UxPutRightOffset( scrolledWindow2, 2 );
	UxPutRightAttachment( scrolledWindow2, "attach_form" );
	UxPutLeftOffset( scrolledWindow2, 2 );
	UxPutLeftAttachment( scrolledWindow2, "attach_form" );
	UxPutBottomOffset( scrolledWindow2, 45 );
	UxPutBottomAttachment( scrolledWindow2, "attach_form" );
	UxCreateWidget( scrolledWindow2 );

	UxCreateWidget( tablelist );
	UxPutRightOffset( pushButton2, 10 );
	UxPutRightAttachment( pushButton2, "attach_form" );
	UxPutLeftOffset( pushButton2, 10 );
	UxPutLeftAttachment( pushButton2, "attach_form" );
	UxPutBottomOffset( pushButton2, 5 );
	UxPutBottomAttachment( pushButton2, "attach_form" );
	UxCreateWidget( pushButton2 );


	UxAddCallback( tablelist, XmNbrowseSelectionCallback,
			browseSelectionCB_tablelist,
			(XtPointer) UxTransientShell8Context );

	UxAddCallback( pushButton2, XmNactivateCallback,
			activateCB_pushButton2,
			(XtPointer) UxTransientShell8Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell8 );

	return ( transientShell8 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell8()
{
	swidget                 rtrn;
	_UxCtransientShell8     *UxContext;

	UxTransientShell8Context = UxContext =
		(_UxCtransientShell8 *) UxMalloc( sizeof(_UxCtransientShell8) );

	rtrn = _Uxbuild_transientShell8();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell8()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_transientShell8();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

