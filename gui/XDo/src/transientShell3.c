
/*******************************************************************************
	transientShell3.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxTogBG.h"
#include "UxRowCol.h"
#include "UxTogB.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <midas_def.h>
char *instr[] = {"EMMI","SUSI"};

extern char print_com[], mid_mail[];
extern swidget scrolledList1;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell3;
	swidget	UxworkArea3;
	swidget	UxSHelp3;
	swidget	UxNewsButton3;
	swidget	UxPrintButton3;
	swidget	Uxlabel4;
	swidget	Uxlabel3;
	swidget	Uxlabel1;
	swidget	Uxlabel2;
	swidget	Uxost_t1;
	swidget	Uxost_t2;
	swidget	UxpushButton4;
	swidget	UxpushButton5;
	swidget	Uxost_t3;
	swidget	Uxost_to1;
	swidget	Uxlabel6;
	swidget	Uxost_to2;
	swidget	UxrowColumn1;
	swidget	UxtoggleButtonGadget1;
	swidget	UxtoggleButtonGadget2;
	swidget	UxtoggleButtonGadget3;
	swidget	Uxseparator1;
	swidget	Uxseparator2;
	swidget	Uxseparator3;
	swidget	Uxseparator4;
} _UxCtransientShell3;

#define transientShell3         UxTransientShell3Context->UxtransientShell3
#define workArea3               UxTransientShell3Context->UxworkArea3
#define SHelp3                  UxTransientShell3Context->UxSHelp3
#define NewsButton3             UxTransientShell3Context->UxNewsButton3
#define PrintButton3            UxTransientShell3Context->UxPrintButton3
#define label4                  UxTransientShell3Context->Uxlabel4
#define label3                  UxTransientShell3Context->Uxlabel3
#define label1                  UxTransientShell3Context->Uxlabel1
#define label2                  UxTransientShell3Context->Uxlabel2
#define ost_t1                  UxTransientShell3Context->Uxost_t1
#define ost_t2                  UxTransientShell3Context->Uxost_t2
#define pushButton4             UxTransientShell3Context->UxpushButton4
#define pushButton5             UxTransientShell3Context->UxpushButton5
#define ost_t3                  UxTransientShell3Context->Uxost_t3
#define ost_to1                 UxTransientShell3Context->Uxost_to1
#define label6                  UxTransientShell3Context->Uxlabel6
#define ost_to2                 UxTransientShell3Context->Uxost_to2
#define rowColumn1              UxTransientShell3Context->UxrowColumn1
#define toggleButtonGadget1     UxTransientShell3Context->UxtoggleButtonGadget1
#define toggleButtonGadget2     UxTransientShell3Context->UxtoggleButtonGadget2
#define toggleButtonGadget3     UxTransientShell3Context->UxtoggleButtonGadget3
#define separator1              UxTransientShell3Context->Uxseparator1
#define separator2              UxTransientShell3Context->Uxseparator2
#define separator3              UxTransientShell3Context->Uxseparator3
#define separator4              UxTransientShell3Context->Uxseparator4

static _UxCtransientShell3	*UxTransientShell3Context;


extern int ostcrea(), read_ost_table();

extern void display_ident_table();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell3();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HInit( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SHelp"),"");
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	char s[100];
	Widget sw = UxWidget;
	
	strcpy(s,"");
	/*sprintf(comm,"WRITE/OUT here");
	AppendDialogText(comm);*/
	 
	if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {
	   strcpy(s,"Select the OST columns to be displayed into the scrolled window");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   } 
	else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {
	   strcpy(s,"Activate interface for defining classification rules ");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }  
	else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {
	   strcpy(s,"Activate interface for classifying images");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {
	   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }
	else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {
	   strcpy(s,"Create the OST table");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {
	   strcpy(s,"Popdown this interface");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ 
	   strcpy(s,"Name of the Observation Summary Table to be created ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ 
	   strcpy(s,"List of frames to be processed ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }  
	  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  
	   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  
	   strcpy(s,"Push button to popup the File Selection Box");     
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  
	        strcpy(s,"Push button to popup the File Selection Box");  
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   } 
	}
	UxTransientShell3Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_transientShell3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	mapCB_workArea3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	createCB_workArea3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_SHelp3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_NewsButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	char command[160],iframe[60],dtable[60],fflag[8],sflag[8],flag[4],pfix[5];
	extern char ostchoice[60];
	extern int tidost,tidcomm;
	 
	strcpy(ostchoice ,osfsupply(UxGetText(UxFindSwidget("ost_t1")),".tbl"));
	strncpy(iframe,UxGetText(UxFindSwidget("ost_t2")),60);
	/*printf("ostchoice: %s\n",iframe);*/
	if (iframe[0] == '\0') iframe[0] = '?';
	strncpy(dtable,UxGetText(UxFindSwidget("ost_t3")),60);
	strcpy(fflag ,UxGetSet(UxFindSwidget("ost_to1")));
	strcpy(sflag,UxGetSet(UxFindSwidget("ost_to2")));
	
	if (fflag[0] == 't') flag[0] = 'F';
	else flag[0] = 'M';
	if (sflag[0] == 't') flag[2] = 'F';
	else flag[2] = 'N';
	flag[1] = 'C';
	flag[3] = '\0';
	if (tidost != -1) {
	        TCTCLO(tidost);
	        tidost = -1;
	        }
	TCTOPN( "TAB_COMM",F_IO_MODE,&tidcomm);
	TCEWRC(tidcomm,1,2,ostchoice);
	TCEWRC(tidcomm,1,3,dtable);
	/*SCDWRC(tidost ,"table_descr",1,dtable,1,60,&unit);*/
	TCTCLO(tidcomm);
	tidcomm = -1; 
	strcpy(pfix,"+");
	ostcrea(iframe,pfix,dtable,ostchoice,flag);
	
	sprintf(command,"CREATE/OST %s ? %s %s %s",iframe,dtable,ostchoice,flag);
	/*AppendDialogText(command);*/
	
	if (read_ost_table()) {
	
	display_ident_table(UxGetWidget(UxFindSwidget("identlist")));
	}
	/*SCDWRC(tidost ,"table_descr",1,dtable,1,60,&unit);*/
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_PrintButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	extern swidget crea;
	
	UxPopdownInterface(crea);
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_pushButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	extern swidget flist;
	
	UxPopupInterface(flist,no_grab);
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_pushButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	extern swidget ilist,dlist;
	extern int doI;
	char cbuf1[8],cbuf2[8];
	XmString *str;
	int item_no,i;
	
	strcpy(cbuf1,UxGetSet(UxFindSwidget("toggleButtonGadget1")));
	strcpy(cbuf2,UxGetSet(UxFindSwidget("toggleButtonGadget2")));
	if (cbuf1[0] == 't') {
	     doI = 0;
	     item_no = 2;
	     str = (XmString *)XtMalloc(item_no * sizeof(XmString));
	     for (i=0; i<item_no; i++)
	        str[i] = XmStringCreateSimple(instr[i]);
	     XtVaSetValues(UxGetWidget(UxFindSwidget("SelectInstrument")),
	               XmNlistItems,str,
	               XmNlistItemCount,item_no,
	               NULL);
	     
	     UxPutListLabelString( UxFindSwidget("SelectInstrument"),"Instrument");
	     UxPutDialogTitle(UxFindSwidget("SelectInstrument"),"Select Instrument");
	     UxPopupInterface(ilist,no_grab);
	     }
	else if (cbuf2[0] == 't') {
	     /*  XtVaSetValues(UxGetWidget(dlist),XmNtitle,
	                     "Select Descriptor Table",NULL);*/
	       UxPutDialogTitle(UxFindSwidget("SelectDescrTable"),"Select Descriptor Table");
	       UxPopupInterface(dlist,no_grab);
	       }
	else {
	       UxPutDialogTitle(UxFindSwidget("SelectDescrTable"),"Select FITS File");
	       UxPopupInterface(dlist,no_grab);
	       }
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	valueChangedCB_ost_to1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	char cbuf[8];
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't') 
	    UxPutLabelString(UxThisWidget,"Fits");
	else
	    UxPutLabelString(UxThisWidget,"Midas");
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	valueChangedCB_ost_to2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	char cbuf[8];
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	    UxPutLabelString(UxThisWidget,"Yes");
	else
	    UxPutLabelString(UxThisWidget,"No");
	    
	}
	UxTransientShell3Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell3()
{
	UxPutTranslations( transientShell3, "" );
	UxPutTitle( transientShell3, "Creation of The OST" );
	UxPutHeight( transientShell3, 400 );
	UxPutWidth( transientShell3, 480 );
	UxPutY( transientShell3, 431 );
	UxPutX( transientShell3, 264 );

	UxPutResizePolicy( workArea3, "resize_none" );
	UxPutNoResize( workArea3, "true" );
	UxPutUnitType( workArea3, "pixels" );
	UxPutBackground( workArea3, WindowBackground );
	UxPutBorderWidth( workArea3, 0 );
	UxPutHeight( workArea3, 400 );
	UxPutWidth( workArea3, 480 );
	UxPutY( workArea3, 0 );
	UxPutX( workArea3, 0 );

	UxPutText( SHelp3, " " );
	UxPutEditable( SHelp3, "true" );
	UxPutAccelerators( SHelp3, "" );
	UxPutWordWrap( SHelp3, "false" );
	UxPutHighlightOnEnter( SHelp3, "true" );
	UxPutEditMode( SHelp3, "single_line_edit" );
	UxPutBlinkRate( SHelp3, 500 );
	UxPutPendingDelete( SHelp3, "true" );
	UxPutTranslations( SHelp3, "" );
	UxPutCursorPositionVisible( SHelp3, "true" );
	UxPutFontList( SHelp3, TextFont );
	UxPutHighlightColor( SHelp3, ApplicBackground );
	UxPutForeground( SHelp3, TextForeground );
	UxPutBackground( SHelp3, SHelpBackground );
	UxPutHeight( SHelp3, 40 );
	UxPutWidth( SHelp3, 325 );
	UxPutY( SHelp3, 380 );
	UxPutX( SHelp3, 30 );

	UxPutHighlightColor( NewsButton3, "Black" );
	UxPutRecomputeSize( NewsButton3, "false" );
	UxPutFontList( NewsButton3, BoldTextFont );
	UxPutForeground( NewsButton3, ButtonForeground );
	UxPutLabelString( NewsButton3, "Apply" );
	UxPutBackground( NewsButton3, ButtonBackground );
	UxPutHeight( NewsButton3, 30 );
	UxPutWidth( NewsButton3, 95 );
	UxPutY( NewsButton3, 630 );
	UxPutX( NewsButton3, 20 );

	UxPutHighlightColor( PrintButton3, "Black" );
	UxPutRecomputeSize( PrintButton3, "false" );
	UxPutFontList( PrintButton3, BoldTextFont );
	UxPutForeground( PrintButton3, ButtonForeground );
	UxPutLabelString( PrintButton3, "Cancel" );
	UxPutBackground( PrintButton3, ButtonBackground );
	UxPutHeight( PrintButton3, 30 );
	UxPutWidth( PrintButton3, 95 );
	UxPutY( PrintButton3, 330 );
	UxPutX( PrintButton3, 280 );

	UxPutLabelString( label4, "OST" );
	UxPutForeground( label4, TextForeground );
	UxPutFontList( label4, BoldTextFont );
	UxPutBackground( label4, LabelBackground );
	UxPutHeight( label4, 20 );
	UxPutWidth( label4, 130 );
	UxPutY( label4, 50 );
	UxPutX( label4, 10 );

	UxPutLabelString( label3, "Input Frames" );
	UxPutForeground( label3, TextForeground );
	UxPutFontList( label3, BoldTextFont );
	UxPutBackground( label3, LabelBackground );
	UxPutHeight( label3, 20 );
	UxPutWidth( label3, 130 );
	UxPutY( label3, 60 );
	UxPutX( label3, 5 );

	UxPutLabelString( label1, "Descriptor Table" );
	UxPutForeground( label1, TextForeground );
	UxPutFontList( label1, BoldTextFont );
	UxPutBackground( label1, LabelBackground );
	UxPutHeight( label1, 20 );
	UxPutWidth( label1, 130 );
	UxPutY( label1, 110 );
	UxPutX( label1, 0 );

	UxPutLabelString( label2, "Type" );
	UxPutForeground( label2, TextForeground );
	UxPutFontList( label2, BoldTextFont );
	UxPutBackground( label2, LabelBackground );
	UxPutHeight( label2, 20 );
	UxPutWidth( label2, 70 );
	UxPutY( label2, 160 );
	UxPutX( label2, 10 );

	UxPutCursorPosition( ost_t1, 0 );
	UxPutTopShadowColor( ost_t1, "Gray80" );
	UxPutBackground( ost_t1, TextBackground );
	UxPutForeground( ost_t1, TextForeground );
	UxPutFontList( ost_t1, TextFont );
	UxPutHeight( ost_t1, 40 );
	UxPutWidth( ost_t1, 160 );
	UxPutY( ost_t1, 30 );
	UxPutX( ost_t1, 200 );

	UxPutText( ost_t2, "" );
	UxPutBackground( ost_t2, TextBackground );
	UxPutForeground( ost_t2, TextForeground );
	UxPutFontList( ost_t2, TextFont );
	UxPutHeight( ost_t2, 40 );
	UxPutWidth( ost_t2, 160 );
	UxPutY( ost_t2, 50 );
	UxPutX( ost_t2, 200 );

	UxPutLabelString( pushButton4, "..." );
	UxPutBackground( pushButton4, ButtonBackground );
	UxPutForeground( pushButton4, ButtonForeground );
	UxPutFontList( pushButton4, BoldTextFont );
	UxPutHeight( pushButton4, 40 );
	UxPutWidth( pushButton4, 40 );
	UxPutY( pushButton4, 60 );
	UxPutX( pushButton4, 400 );

	UxPutLabelString( pushButton5, "..." );
	UxPutBackground( pushButton5, ButtonBackground );
	UxPutForeground( pushButton5, ButtonForeground );
	UxPutFontList( pushButton5, BoldTextFont );
	UxPutHeight( pushButton5, 40 );
	UxPutWidth( pushButton5, 40 );
	UxPutY( pushButton5, 100 );
	UxPutX( pushButton5, 400 );

	UxPutBackground( ost_t3, TextBackground );
	UxPutForeground( ost_t3, TextForeground );
	UxPutFontList( ost_t3, TextFont );
	UxPutHeight( ost_t3, 40 );
	UxPutWidth( ost_t3, 160 );
	UxPutY( ost_t3, 100 );
	UxPutX( ost_t3, 170 );

	UxPutSet( ost_to1, "true" );
	UxPutIndicatorType( ost_to1, "one_of_many" );
	UxPutLabelString( ost_to1, "Fits" );
	UxPutSelectColor( ost_to1, "PaleGreen" );
	UxPutForeground( ost_to1, TextForeground );
	UxPutFontList( ost_to1, BoldTextFont );
	UxPutBackground( ost_to1, TextBackground );
	UxPutHeight( ost_to1, 30 );
	UxPutWidth( ost_to1, 80 );
	UxPutY( ost_to1, 160 );
	UxPutX( ost_to1, 140 );

	UxPutLabelString( label6, "Statistics" );
	UxPutForeground( label6, TextForeground );
	UxPutFontList( label6, BoldTextFont );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 20 );
	UxPutWidth( label6, 70 );
	UxPutY( label6, 182 );
	UxPutX( label6, 40 );

	UxPutSet( ost_to2, "true" );
	UxPutIndicatorType( ost_to2, "one_of_many" );
	UxPutLabelString( ost_to2, "Yes" );
	UxPutSelectColor( ost_to2, "PaleGreen" );
	UxPutForeground( ost_to2, TextForeground );
	UxPutFontList( ost_to2, BoldTextFont );
	UxPutBackground( ost_to2, TextBackground );
	UxPutHeight( ost_to2, 30 );
	UxPutWidth( ost_to2, 80 );
	UxPutY( ost_to2, 178 );
	UxPutX( ost_to2, 130 );

	UxPutRadioBehavior( rowColumn1, "true" );
	UxPutBackground( rowColumn1, WindowBackground );
	UxPutHeight( rowColumn1, 110 );
	UxPutWidth( rowColumn1, 140 );
	UxPutY( rowColumn1, 130 );
	UxPutX( rowColumn1, 10 );

	UxPutSelectColor( toggleButtonGadget1, "PaleGreen" );
	UxPutFontList( toggleButtonGadget1, TextFont );
	UxPutLabelString( toggleButtonGadget1, "Get Standard Table" );
	UxPutIndicatorType( toggleButtonGadget1, "one_of_many" );
	UxPutHeight( toggleButtonGadget1, 20 );
	UxPutWidth( toggleButtonGadget1, 120 );
	UxPutY( toggleButtonGadget1, 10 );
	UxPutX( toggleButtonGadget1, 10 );

	UxPutSet( toggleButtonGadget2, "true" );
	UxPutSelectColor( toggleButtonGadget2, "PaleGreen" );
	UxPutFontList( toggleButtonGadget2, TextFont );
	UxPutLabelString( toggleButtonGadget2, "Select Own Table" );
	UxPutIndicatorType( toggleButtonGadget2, "one_of_many" );
	UxPutHeight( toggleButtonGadget2, 20 );
	UxPutWidth( toggleButtonGadget2, 120 );
	UxPutY( toggleButtonGadget2, 13 );
	UxPutX( toggleButtonGadget2, 13 );

	UxPutSelectColor( toggleButtonGadget3, "PaleGreen" );
	UxPutFontList( toggleButtonGadget3, TextFont );
	UxPutLabelString( toggleButtonGadget3, "Create from Fits File" );
	UxPutIndicatorType( toggleButtonGadget3, "one_of_many" );
	UxPutHeight( toggleButtonGadget3, 20 );
	UxPutWidth( toggleButtonGadget3, 120 );
	UxPutY( toggleButtonGadget3, 44 );
	UxPutX( toggleButtonGadget3, 13 );

	UxPutBackground( separator1, WindowBackground );
	UxPutOrientation( separator1, "vertical" );
	UxPutHeight( separator1, 106 );
	UxPutWidth( separator1, 10 );
	UxPutY( separator1, 140 );
	UxPutX( separator1, 180 );

	UxPutBackground( separator2, WindowBackground );
	UxPutOrientation( separator2, "vertical" );
	UxPutHeight( separator2, 106 );
	UxPutWidth( separator2, 10 );
	UxPutY( separator2, 145 );
	UxPutX( separator2, 190 );

	UxPutBackground( separator3, WindowBackground );
	UxPutHeight( separator3, 10 );
	UxPutWidth( separator3, 170 );
	UxPutY( separator3, 230 );
	UxPutX( separator3, 10 );

	UxPutBackground( separator4, WindowBackground );
	UxPutHeight( separator4, 10 );
	UxPutWidth( separator4, 170 );
	UxPutY( separator4, 130 );
	UxPutX( separator4, 10 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell3()
{
	/* Create the swidgets */

	transientShell3 = UxCreateTransientShell( "transientShell3", NO_PARENT );
	UxPutContext( transientShell3, UxTransientShell3Context );

	workArea3 = UxCreateForm( "workArea3", transientShell3 );
	SHelp3 = UxCreateText( "SHelp3", workArea3 );
	NewsButton3 = UxCreatePushButton( "NewsButton3", workArea3 );
	PrintButton3 = UxCreatePushButton( "PrintButton3", workArea3 );
	label4 = UxCreateLabel( "label4", workArea3 );
	label3 = UxCreateLabel( "label3", workArea3 );
	label1 = UxCreateLabel( "label1", workArea3 );
	label2 = UxCreateLabel( "label2", workArea3 );
	ost_t1 = UxCreateText( "ost_t1", workArea3 );
	ost_t2 = UxCreateText( "ost_t2", workArea3 );
	pushButton4 = UxCreatePushButton( "pushButton4", workArea3 );
	pushButton5 = UxCreatePushButton( "pushButton5", workArea3 );
	ost_t3 = UxCreateText( "ost_t3", workArea3 );
	ost_to1 = UxCreateToggleButton( "ost_to1", workArea3 );
	label6 = UxCreateLabel( "label6", workArea3 );
	ost_to2 = UxCreateToggleButton( "ost_to2", workArea3 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", workArea3 );
	toggleButtonGadget1 = UxCreateToggleButtonGadget( "toggleButtonGadget1", rowColumn1 );
	toggleButtonGadget2 = UxCreateToggleButtonGadget( "toggleButtonGadget2", rowColumn1 );
	toggleButtonGadget3 = UxCreateToggleButtonGadget( "toggleButtonGadget3", rowColumn1 );
	separator1 = UxCreateSeparator( "separator1", workArea3 );
	separator2 = UxCreateSeparator( "separator2", workArea3 );
	separator3 = UxCreateSeparator( "separator3", workArea3 );
	separator4 = UxCreateSeparator( "separator4", workArea3 );

	_Uxinit_transientShell3();

	/* Create the X widgets */

	UxCreateWidget( transientShell3 );
	UxCreateWidget( workArea3 );
	createCB_workArea3( UxGetWidget( workArea3 ),
			(XtPointer) UxTransientShell3Context, (XtPointer) NULL );

	UxPutBottomOffset( SHelp3, 50 );
	UxPutBottomAttachment( SHelp3, "attach_form" );
	UxPutRightOffset( SHelp3, 5 );
	UxPutRightAttachment( SHelp3, "attach_form" );
	UxPutLeftOffset( SHelp3, 5 );
	UxPutLeftAttachment( SHelp3, "attach_form" );
	UxCreateWidget( SHelp3 );

	UxPutLeftOffset( NewsButton3, 50 );
	UxPutLeftAttachment( NewsButton3, "attach_form" );
	UxPutTopWidget( NewsButton3, "SHelp3" );
	UxPutTopOffset( NewsButton3, 10 );
	UxPutTopAttachment( NewsButton3, "attach_widget" );
	UxCreateWidget( NewsButton3 );

	UxPutRightOffset( PrintButton3, 50 );
	UxPutRightAttachment( PrintButton3, "attach_form" );
	UxPutLeftWidget( PrintButton3, "NewsButton3" );
	UxPutLeftOffset( PrintButton3, 40 );
	UxPutLeftAttachment( PrintButton3, "attach_none" );
	UxPutTopWidget( PrintButton3, "SHelp3" );
	UxPutTopOffset( PrintButton3, 10 );
	UxPutTopAttachment( PrintButton3, "attach_widget" );
	UxCreateWidget( PrintButton3 );

	UxPutTopAttachment( label4, "attach_form" );
	UxPutTopOffset( label4, 20 );
	UxPutLeftOffset( label4, 35 );
	UxPutLeftAttachment( label4, "attach_form" );
	UxCreateWidget( label4 );

	UxPutLeftOffset( label3, 35 );
	UxPutLeftAttachment( label3, "attach_form" );
	UxPutTopOffset( label3, 65 );
	UxPutTopAttachment( label3, "attach_form" );
	UxCreateWidget( label3 );

	UxPutTopOffset( label1, 110 );
	UxPutTopAttachment( label1, "attach_form" );
	UxPutLeftOffset( label1, 35 );
	UxPutLeftAttachment( label1, "attach_form" );
	UxCreateWidget( label1 );

	UxPutTopOffset( label2, 250 );
	UxPutLeftOffset( label2, 30 );
	UxPutTopAttachment( label2, "attach_form" );
	UxPutLeftAttachment( label2, "attach_form" );
	UxCreateWidget( label2 );

	UxPutLeftOffset( ost_t1, 220 );
	UxPutLeftAttachment( ost_t1, "attach_form" );
	UxPutRightOffset( ost_t1, 20 );
	UxPutRightAttachment( ost_t1, "attach_none" );
	UxPutTopOffset( ost_t1, 10 );
	UxPutTopAttachment( ost_t1, "attach_form" );
	UxCreateWidget( ost_t1 );

	UxPutLeftOffset( ost_t2, 220 );
	UxPutLeftAttachment( ost_t2, "attach_form" );
	UxPutRightOffset( ost_t2, 20 );
	UxPutRightAttachment( ost_t2, "attach_none" );
	UxPutTopOffset( ost_t2, 58 );
	UxPutTopAttachment( ost_t2, "attach_form" );
	UxCreateWidget( ost_t2 );

	UxPutRightOffset( pushButton4, 40 );
	UxPutRightAttachment( pushButton4, "attach_form" );
	UxPutTopOffset( pushButton4, 58 );
	UxPutTopAttachment( pushButton4, "attach_form" );
	UxCreateWidget( pushButton4 );

	UxPutTopOffset( pushButton5, 158 );
	UxPutTopAttachment( pushButton5, "attach_form" );
	UxPutRightOffset( pushButton5, 40 );
	UxPutRightAttachment( pushButton5, "attach_form" );
	UxCreateWidget( pushButton5 );

	UxPutLeftOffset( ost_t3, 220 );
	UxPutLeftAttachment( ost_t3, "attach_form" );
	UxPutTopOffset( ost_t3, 158 );
	UxPutTopAttachment( ost_t3, "attach_form" );
	UxCreateWidget( ost_t3 );

	UxPutLeftOffset( ost_to1, 120 );
	UxPutLeftAttachment( ost_to1, "attach_form" );
	UxPutTopOffset( ost_to1, 245 );
	UxPutTopAttachment( ost_to1, "attach_form" );
	UxCreateWidget( ost_to1 );

	UxPutTopOffset( label6, 250 );
	UxPutTopAttachment( label6, "attach_form" );
	UxPutLeftOffset( label6, 250 );
	UxPutLeftAttachment( label6, "attach_form" );
	UxCreateWidget( label6 );

	UxPutRightOffset( ost_to2, 30 );
	UxPutRightAttachment( ost_to2, "attach_form" );
	UxPutTopOffset( ost_to2, 245 );
	UxPutTopAttachment( ost_to2, "attach_form" );
	UxPutLeftOffset( ost_to2, 30 );
	UxPutLeftAttachment( ost_to2, "attach_none" );
	UxCreateWidget( ost_to2 );

	UxPutLeftOffset( rowColumn1, 20 );
	UxPutLeftAttachment( rowColumn1, "attach_form" );
	UxCreateWidget( rowColumn1 );

	UxCreateWidget( toggleButtonGadget1 );
	UxCreateWidget( toggleButtonGadget2 );
	UxCreateWidget( toggleButtonGadget3 );
	UxPutTopOffset( separator1, 120 );
	UxPutTopAttachment( separator1, "attach_form" );
	UxPutLeftOffset( separator1, 180 );
	UxPutLeftAttachment( separator1, "attach_form" );
	UxCreateWidget( separator1 );

	UxPutLeftOffset( separator2, 10 );
	UxPutLeftAttachment( separator2, "attach_form" );
	UxPutTopOffset( separator2, 120 );
	UxPutTopAttachment( separator2, "attach_form" );
	UxCreateWidget( separator2 );

	UxPutTopOffset( separator3, 222 );
	UxPutTopAttachment( separator3, "attach_form" );
	UxPutLeftOffset( separator3, 15 );
	UxPutLeftAttachment( separator3, "attach_form" );
	UxCreateWidget( separator3 );

	UxPutLeftOffset( separator4, 15 );
	UxPutLeftAttachment( separator4, "attach_form" );
	UxPutTopOffset( separator4, 115 );
	UxPutTopAttachment( separator4, "attach_form" );
	UxCreateWidget( separator4 );


	UxAddCallback( transientShell3, XmNpopupCallback,
			popupCB_transientShell3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( workArea3, XmNmapCallback,
			mapCB_workArea3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( SHelp3, XmNactivateCallback,
			activateCB_SHelp3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( NewsButton3, XmNactivateCallback,
			activateCB_NewsButton3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( PrintButton3, XmNactivateCallback,
			activateCB_PrintButton3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( pushButton4, XmNactivateCallback,
			activateCB_pushButton4,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( pushButton5, XmNactivateCallback,
			activateCB_pushButton5,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( ost_to1, XmNvalueChangedCallback,
			valueChangedCB_ost_to1,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( ost_to2, XmNvalueChangedCallback,
			valueChangedCB_ost_to2,
			(XtPointer) UxTransientShell3Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell3 );

	return ( transientShell3 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell3()
{
	swidget                 rtrn;
	_UxCtransientShell3     *UxContext;

	UxTransientShell3Context = UxContext =
		(_UxCtransientShell3 *) UxMalloc( sizeof(_UxCtransientShell3) );

	rtrn = _Uxbuild_transientShell3();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell3()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HInit", action_HInit },
				{ "ClearHelp", action_ClearHelp },
				{ "WriteHelp", action_WriteHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell3();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

