
/*******************************************************************************
	Associate.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

#include "midas_def.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxAssociate;
	swidget	UxmainWindow3;
	swidget	UxpullDownMenu3;
	swidget	UxquitPane3;
	swidget	UxquitPane_b4;
	swidget	UxpullDownMenu_top_b4;
	swidget	UxUtilsPane3;
	swidget	UxUtilsPane_b11;
	swidget	UxUtilsPane_b12;
	swidget	UxcommandsCascade3;
	swidget	UxpullDownMenu1_p2;
	swidget	UxpullDownMenu1_p3_b4;
	swidget	UxpullDownMenu1_p1_b1;
	swidget	UxpullDownMenu1_top_b3;
	swidget	UxpullDownMenu3_p4;
	swidget	UxpullDownMenu3_p4_b1;
	swidget	UxpullDownMenu3_p4_b2;
	swidget	UxpullDownMenu3_top_b1;
	swidget	UxworkAreaForm3;
	swidget	Uxform4;
	swidget	UxscrolledWindow4;
	swidget	Uxrowasso;
	swidget	Uxlabel14;
	swidget	Uxlabel15;
	swidget	Uxlabel16;
	swidget	Uxlabel17;
	swidget	Uxlabel19;
	swidget	Uxarule_name;
} _UxCAssociate;

#define Associate               UxAssociateContext->UxAssociate
#define mainWindow3             UxAssociateContext->UxmainWindow3
#define pullDownMenu3           UxAssociateContext->UxpullDownMenu3
#define quitPane3               UxAssociateContext->UxquitPane3
#define quitPane_b4             UxAssociateContext->UxquitPane_b4
#define pullDownMenu_top_b4     UxAssociateContext->UxpullDownMenu_top_b4
#define UtilsPane3              UxAssociateContext->UxUtilsPane3
#define UtilsPane_b11           UxAssociateContext->UxUtilsPane_b11
#define UtilsPane_b12           UxAssociateContext->UxUtilsPane_b12
#define commandsCascade3        UxAssociateContext->UxcommandsCascade3
#define pullDownMenu1_p2        UxAssociateContext->UxpullDownMenu1_p2
#define pullDownMenu1_p3_b4     UxAssociateContext->UxpullDownMenu1_p3_b4
#define pullDownMenu1_p1_b1     UxAssociateContext->UxpullDownMenu1_p1_b1
#define pullDownMenu1_top_b3    UxAssociateContext->UxpullDownMenu1_top_b3
#define pullDownMenu3_p4        UxAssociateContext->UxpullDownMenu3_p4
#define pullDownMenu3_p4_b1     UxAssociateContext->UxpullDownMenu3_p4_b1
#define pullDownMenu3_p4_b2     UxAssociateContext->UxpullDownMenu3_p4_b2
#define pullDownMenu3_top_b1    UxAssociateContext->UxpullDownMenu3_top_b1
#define workAreaForm3           UxAssociateContext->UxworkAreaForm3
#define form4                   UxAssociateContext->Uxform4
#define scrolledWindow4         UxAssociateContext->UxscrolledWindow4
#define rowasso                 UxAssociateContext->Uxrowasso
#define label14                 UxAssociateContext->Uxlabel14
#define label15                 UxAssociateContext->Uxlabel15
#define label16                 UxAssociateContext->Uxlabel16
#define label17                 UxAssociateContext->Uxlabel17
#define label19                 UxAssociateContext->Uxlabel19
#define arule_name              UxAssociateContext->Uxarule_name

static _UxCAssociate	*UxAssociateContext;

extern void SetFileList();

extern int save_asso_table();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Associate();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_nothing( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	printf("toto:\n");
	}
	UxAssociateContext = UxSaveCtx;
}

static void	action_SelectCommand( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	
	 
	
	}
	UxAssociateContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SHelp"),"");
	}
	UxAssociateContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	char s[100];
	 
	Widget sw = UxWidget;
	
	strcpy(s,"");
	/*sprintf(comm,"WRITE/OUT here");
	AppendDialogText(comm);*/
	 
	if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {
	   strcpy(s,"Select the OST columns to be displayed into the scrolled window");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   } 
	else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {
	   strcpy(s,"Activate interface for defining classification rules ");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }  
	else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {
	   strcpy(s,"Activate interface for classifying images");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {
	   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }
	else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {
	   strcpy(s,"Create the OST table");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {
	   strcpy(s,"Popdown this interface");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ 
	   strcpy(s,"Name of the Observation Summary Table to be created ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ 
	   strcpy(s,"List of frames to be processed ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }  
	  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  
	   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  
	   strcpy(s,"Push button to popup the File Selection Box");     
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  
	        strcpy(s,"Push button to popup the File Selection Box");  
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   } 
	}
	UxAssociateContext = UxSaveCtx;
}

static void	action_HelpACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	/* char *s;
	 s = XmTextGetSelection(UxWidget); 
	 
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	*/}
	UxAssociateContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_quitPane_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	extern swidget asso;
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	extern int rowno;
	int i;
	for (i=0; i<5; i++) {
	    XmTextSetString(arow1[i],"");
	    XmTextSetString(arow2[i],"");
	    XmTextSetString(arow3[i],"");
	    XmTextSetString(arow4[i],"");
	    }
	for (i=5; i<rowno; i++) {
	    XtDestroyWidget(arow1[i]);
	    arow1[i] = (Widget )0;
	    XtDestroyWidget(arow2[i]); 
	    arow2[i] = (Widget )0;
	    XtDestroyWidget(arow3[i]);
	    arow3[i] = (Widget )0;
	     XtDestroyWidget(arow4[i]);
	    arow4[i] = (Widget )0;
	    } 
	rowno =5;
	UxPopdownInterface(asso);
	}
	UxAssociateContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	char s[1000];
	extern swidget klist,tablelist;
	
	extern int do8;
	int strip =1;
	strcpy(s,"*.tbl");
	SetFileList(UxGetWidget(tablelist),strip,s);
	UxPutTitle(UxFindSwidget("transientShell8"),"Select Classification Table");
	do8 = 2;
	UxPopupInterface(klist,no_grab);
	}
	UxAssociateContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	char mytext[60];
	int tid;
	strcpy(mytext,UxGetText(UxFindSwidget("arule_name")));
	
	tid = TCTID(mytext); 
	if (tid != -1) TCTCLO(tid);
	save_asso_table(mytext);
	
	}
	UxAssociateContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p3_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	extern int arowno;
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	arowno++;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,arowno,NULL);
	sprintf(mytext,"a1_%d",arowno-1);
	arow1[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	sprintf(mytext,"a2_%d",arowno-1);
	arow2[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	sprintf(mytext,"a3_%d",arowno-1);
	arow3[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	sprintf(mytext,"a4_%d",arowno-1);
	arow4[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	}
	UxAssociateContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	extern int arowno;
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	int i;
	for (i=0; i<arowno; i++) {
	   XmTextSetString(arow1[i],"");
	   XmTextSetString(arow2[i],"");
	   XmTextSetString(arow3[i],"");
	   XmTextSetString(arow4[i],"");
	    }
	}
	UxAssociateContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu3_p4_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	extern int arowno;
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	char mytext[8];
	arowno++;
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,arowno,NULL);
	sprintf(mytext,"a1_%d",arowno-1);
	arow1[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	sprintf(mytext,"a2_%d",arowno-1);
	arow2[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	sprintf(mytext,"a3_%d",arowno-1);
	arow3[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	sprintf(mytext,"a4_%d",arowno-1);
	arow4[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);
	}
	UxAssociateContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu3_p4_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	extern int arowno;
	extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
	int i;
	for (i=0; i<arowno; i++) {
	   XmTextSetString(arow1[i],"");
	   XmTextSetString(arow2[i],"");
	   XmTextSetString(arow3[i],"");
	   XmTextSetString(arow4[i],"");
	    }
	}
	UxAssociateContext = UxSaveCtx;
}

static void	entryCB_rowasso( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	/*XmRowColumnCallbackStruct *cbs;
	Widget pb = cbs->widget;
	cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;
	printf("%s: %s\n",XtName(pb),cbs->data);*/
	}
	UxAssociateContext = UxSaveCtx;
}

static void	createCB_rowasso( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAssociate           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAssociateContext;
	UxAssociateContext = UxContext =
			(_UxCAssociate *) UxGetContext( UxThisWidget );
	{
	
	}
	UxAssociateContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Associate()
{
	UxPutIconName( Associate, "Association Rules" );
	UxPutHeight( Associate, 363 );
	UxPutWidth( Associate, 920 );
	UxPutY( Associate, 397 );
	UxPutX( Associate, 666 );

	UxPutUnitType( mainWindow3, "pixels" );
	UxPutBackground( mainWindow3, WindowBackground );
	UxPutHeight( mainWindow3, 470 );
	UxPutWidth( mainWindow3, 700 );
	UxPutY( mainWindow3, 180 );
	UxPutX( mainWindow3, 200 );

	UxPutForeground( pullDownMenu3, MenuForeground );
	UxPutBackground( pullDownMenu3, MenuBackground );
	UxPutMenuAccelerator( pullDownMenu3, "<KeyUp>F10" );
	UxPutRowColumnType( pullDownMenu3, "menu_bar" );
	UxPutBorderWidth( pullDownMenu3, 0 );

	UxPutForeground( quitPane3, MenuForeground );
	UxPutBackground( quitPane3, MenuBackground );
	UxPutRowColumnType( quitPane3, "menu_pulldown" );

	UxPutFontList( quitPane_b4, BoldTextFont );
	UxPutLabelString( quitPane_b4, "Bye" );

	UxPutForeground( pullDownMenu_top_b4, MenuForeground );
	UxPutFontList( pullDownMenu_top_b4, BoldTextFont );
	UxPutBackground( pullDownMenu_top_b4, MenuBackground );
	UxPutMnemonic( pullDownMenu_top_b4, "Q" );
	UxPutLabelString( pullDownMenu_top_b4, "Quit" );

	UxPutForeground( UtilsPane3, MenuForeground );
	UxPutBackground( UtilsPane3, MenuBackground );
	UxPutRowColumnType( UtilsPane3, "menu_pulldown" );

	UxPutFontList( UtilsPane_b11, BoldTextFont );
	UxPutLabelString( UtilsPane_b11, "Open ..." );

	UxPutFontList( UtilsPane_b12, BoldTextFont );
	UxPutLabelString( UtilsPane_b12, "Save" );

	UxPutX( commandsCascade3, 101 );
	UxPutMnemonic( commandsCascade3, "F" );
	UxPutForeground( commandsCascade3, MenuForeground );
	UxPutFontList( commandsCascade3, BoldTextFont );
	UxPutBackground( commandsCascade3, MenuBackground );
	UxPutLabelString( commandsCascade3, "File" );

	UxPutForeground( pullDownMenu1_p2, MenuForeground );
	UxPutBackground( pullDownMenu1_p2, MenuBackground );
	UxPutRowColumnType( pullDownMenu1_p2, "menu_pulldown" );

	UxPutFontList( pullDownMenu1_p3_b4, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p3_b4, "Add" );

	UxPutFontList( pullDownMenu1_p1_b1, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p1_b1, "Reset" );

	UxPutForeground( pullDownMenu1_top_b3, MenuForeground );
	UxPutFontList( pullDownMenu1_top_b3, BoldTextFont );
	UxPutBackground( pullDownMenu1_top_b3, MenuBackground );
	UxPutMnemonic( pullDownMenu1_top_b3, "E" );
	UxPutLabelString( pullDownMenu1_top_b3, "Edit" );

	UxPutForeground( pullDownMenu3_p4, MenuForeground );
	UxPutBackground( pullDownMenu3_p4, MenuBackground );
	UxPutRowColumnType( pullDownMenu3_p4, "menu_pulldown" );

	UxPutFontList( pullDownMenu3_p4_b1, BoldTextFont );
	UxPutLabelString( pullDownMenu3_p4_b1, "On this interface..." );

	UxPutFontList( pullDownMenu3_p4_b2, BoldTextFont );
	UxPutLabelString( pullDownMenu3_p4_b2, "On importing data..." );

	UxPutForeground( pullDownMenu3_top_b1, MenuForeground );
	UxPutFontList( pullDownMenu3_top_b1, BoldTextFont );
	UxPutBackground( pullDownMenu3_top_b1, MenuBackground );
	UxPutMnemonic( pullDownMenu3_top_b1, "H" );
	UxPutLabelString( pullDownMenu3_top_b1, "Help" );

	UxPutBackground( workAreaForm3, WindowBackground );
	UxPutBorderWidth( workAreaForm3, 0 );
	UxPutHeight( workAreaForm3, 400 );
	UxPutWidth( workAreaForm3, 800 );

	UxPutBackground( form4, WindowBackground );
	UxPutHeight( form4, 300 );
	UxPutWidth( form4, 900 );
	UxPutY( form4, 10 );
	UxPutX( form4, 10 );
	UxPutUnitType( form4, "pixels" );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutVisualPolicy( scrolledWindow4, "variable" );
	UxPutScrollBarDisplayPolicy( scrolledWindow4, "static" );
	UxPutScrollBarPlacement( scrolledWindow4, "bottom_left" );
	UxPutBackground( scrolledWindow4, WindowBackground );
	UxPutHeight( scrolledWindow4, 300 );
	UxPutWidth( scrolledWindow4, 900 );
	UxPutY( scrolledWindow4, 30 );
	UxPutX( scrolledWindow4, 10 );
	UxPutScrollingPolicy( scrolledWindow4, "automatic" );

	UxPutIsAligned( rowasso, "true" );
	UxPutResizeWidth( rowasso, "true" );
	UxPutNumColumns( rowasso, 5 );
	UxPutPacking( rowasso, "pack_column" );
	UxPutOrientation( rowasso, "horizontal" );
	UxPutEntryAlignment( rowasso, "alignment_center" );
	UxPutBackground( rowasso, WindowBackground );
	UxPutHeight( rowasso, 370 );
	UxPutWidth( rowasso, 860 );
	UxPutY( rowasso, 8 );
	UxPutX( rowasso, 8 );

	UxPutAlignment( label14, "alignment_beginning" );
	UxPutLabelString( label14, "FUNCTION" );
	UxPutFontList( label14, BoldTextFont );
	UxPutBackground( label14, WindowBackground );
	UxPutHeight( label14, 40 );
	UxPutWidth( label14, 100 );
	UxPutY( label14, 420 );
	UxPutX( label14, 10 );

	UxPutAlignment( label15, "alignment_beginning" );
	UxPutLabelString( label15, "RANGE_1" );
	UxPutFontList( label15, BoldTextFont );
	UxPutBackground( label15, WindowBackground );
	UxPutHeight( label15, 40 );
	UxPutWidth( label15, 100 );
	UxPutY( label15, 10 );
	UxPutX( label15, 90 );

	UxPutAlignment( label16, "alignment_beginning" );
	UxPutLabelString( label16, "RANGE_2" );
	UxPutFontList( label16, BoldTextFont );
	UxPutBackground( label16, WindowBackground );
	UxPutHeight( label16, 40 );
	UxPutWidth( label16, 100 );
	UxPutY( label16, 10 );
	UxPutX( label16, 310 );

	UxPutAlignment( label17, "alignment_beginning" );
	UxPutLabelString( label17, "WEIGHT" );
	UxPutFontList( label17, BoldTextFont );
	UxPutBackground( label17, WindowBackground );
	UxPutHeight( label17, 40 );
	UxPutWidth( label17, 100 );
	UxPutY( label17, 10 );
	UxPutX( label17, 510 );

	UxPutLabelString( label19, "Table Name" );
	UxPutFontList( label19, BoldTextFont );
	UxPutBackground( label19, WindowBackground );
	UxPutHeight( label19, 50 );
	UxPutWidth( label19, 260 );
	UxPutY( label19, 10 );
	UxPutX( label19, 210 );

	UxPutFontList( arule_name, TextFont );
	UxPutBackground( arule_name, TextBackground );
	UxPutHeight( arule_name, 50 );
	UxPutWidth( arule_name, 360 );
	UxPutY( arule_name, 10 );
	UxPutX( arule_name, 480 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Associate()
{
	/* Create the swidgets */

	Associate = UxCreateApplicationShell( "Associate", NO_PARENT );
	UxPutContext( Associate, UxAssociateContext );

	mainWindow3 = UxCreateMainWindow( "mainWindow3", Associate );
	pullDownMenu3 = UxCreateRowColumn( "pullDownMenu3", mainWindow3 );
	quitPane3 = UxCreateRowColumn( "quitPane3", pullDownMenu3 );
	quitPane_b4 = UxCreatePushButtonGadget( "quitPane_b4", quitPane3 );
	pullDownMenu_top_b4 = UxCreateCascadeButton( "pullDownMenu_top_b4", pullDownMenu3 );
	UtilsPane3 = UxCreateRowColumn( "UtilsPane3", pullDownMenu3 );
	UtilsPane_b11 = UxCreatePushButtonGadget( "UtilsPane_b11", UtilsPane3 );
	UtilsPane_b12 = UxCreatePushButtonGadget( "UtilsPane_b12", UtilsPane3 );
	commandsCascade3 = UxCreateCascadeButton( "commandsCascade3", pullDownMenu3 );
	pullDownMenu1_p2 = UxCreateRowColumn( "pullDownMenu1_p2", pullDownMenu3 );
	pullDownMenu1_p3_b4 = UxCreatePushButtonGadget( "pullDownMenu1_p3_b4", pullDownMenu1_p2 );
	pullDownMenu1_p1_b1 = UxCreatePushButtonGadget( "pullDownMenu1_p1_b1", pullDownMenu1_p2 );
	pullDownMenu1_top_b3 = UxCreateCascadeButton( "pullDownMenu1_top_b3", pullDownMenu3 );
	pullDownMenu3_p4 = UxCreateRowColumn( "pullDownMenu3_p4", pullDownMenu3 );
	pullDownMenu3_p4_b1 = UxCreatePushButtonGadget( "pullDownMenu3_p4_b1", pullDownMenu3_p4 );
	pullDownMenu3_p4_b2 = UxCreatePushButtonGadget( "pullDownMenu3_p4_b2", pullDownMenu3_p4 );
	pullDownMenu3_top_b1 = UxCreateCascadeButton( "pullDownMenu3_top_b1", pullDownMenu3 );
	workAreaForm3 = UxCreateForm( "workAreaForm3", mainWindow3 );
	form4 = UxCreateForm( "form4", workAreaForm3 );
	scrolledWindow4 = UxCreateScrolledWindow( "scrolledWindow4", form4 );
	rowasso = UxCreateRowColumn( "rowasso", scrolledWindow4 );
	label14 = UxCreateLabel( "label14", form4 );
	label15 = UxCreateLabel( "label15", form4 );
	label16 = UxCreateLabel( "label16", form4 );
	label17 = UxCreateLabel( "label17", form4 );
	label19 = UxCreateLabel( "label19", form4 );
	arule_name = UxCreateText( "arule_name", form4 );

	_Uxinit_Associate();

	/* Create the X widgets */

	UxCreateWidget( Associate );
	UxCreateWidget( mainWindow3 );
	UxCreateWidget( pullDownMenu3 );
	UxCreateWidget( quitPane3 );
	UxCreateWidget( quitPane_b4 );
	UxPutSubMenuId( pullDownMenu_top_b4, "quitPane3" );
	UxCreateWidget( pullDownMenu_top_b4 );

	UxCreateWidget( UtilsPane3 );
	UxCreateWidget( UtilsPane_b11 );
	UxCreateWidget( UtilsPane_b12 );
	UxPutSubMenuId( commandsCascade3, "UtilsPane3" );
	UxCreateWidget( commandsCascade3 );

	UxCreateWidget( pullDownMenu1_p2 );
	UxCreateWidget( pullDownMenu1_p3_b4 );
	UxCreateWidget( pullDownMenu1_p1_b1 );
	UxPutSubMenuId( pullDownMenu1_top_b3, "pullDownMenu1_p2" );
	UxCreateWidget( pullDownMenu1_top_b3 );

	UxCreateWidget( pullDownMenu3_p4 );
	UxCreateWidget( pullDownMenu3_p4_b1 );
	UxCreateWidget( pullDownMenu3_p4_b2 );
	UxPutSubMenuId( pullDownMenu3_top_b1, "pullDownMenu3_p4" );
	UxCreateWidget( pullDownMenu3_top_b1 );

	UxCreateWidget( workAreaForm3 );
	UxCreateWidget( form4 );
	UxPutTopAttachment( scrolledWindow4, "attach_form" );
	UxPutTopOffset( scrolledWindow4, 120 );
	UxPutBottomOffset( scrolledWindow4, 10 );
	UxPutBottomAttachment( scrolledWindow4, "attach_form" );
	UxPutLeftOffset( scrolledWindow4, 10 );
	UxPutRightOffset( scrolledWindow4, 10 );
	UxPutRightAttachment( scrolledWindow4, "attach_form" );
	UxPutLeftAttachment( scrolledWindow4, "attach_form" );
	UxCreateWidget( scrolledWindow4 );

	UxCreateWidget( rowasso );
	createCB_rowasso( UxGetWidget( rowasso ),
			(XtPointer) UxAssociateContext, (XtPointer) NULL );

	UxPutLeftOffset( label14, 80 );
	UxPutLeftAttachment( label14, "attach_form" );
	UxPutTopAttachment( label14, "attach_form" );
	UxPutTopOffset( label14, 70 );
	UxCreateWidget( label14 );

	UxPutTopOffset( label15, 70 );
	UxPutLeftOffset( label15, 310 );
	UxPutLeftAttachment( label15, "attach_form" );
	UxPutTopAttachment( label15, "attach_form" );
	UxCreateWidget( label15 );

	UxPutTopOffset( label16, 70 );
	UxPutTopAttachment( label16, "attach_form" );
	UxPutLeftOffset( label16, 510 );
	UxPutLeftAttachment( label16, "attach_form" );
	UxCreateWidget( label16 );

	UxPutTopOffset( label17, 70 );
	UxPutTopAttachment( label17, "attach_form" );
	UxPutLeftOffset( label17, 720 );
	UxPutLeftAttachment( label17, "attach_form" );
	UxCreateWidget( label17 );

	UxPutTopAttachment( label19, "attach_form" );
	UxPutLeftOffset( label19, 100 );
	UxPutLeftAttachment( label19, "attach_form" );
	UxCreateWidget( label19 );

	UxPutLeftOffset( arule_name, 400 );
	UxPutLeftAttachment( arule_name, "attach_form" );
	UxPutTopAttachment( arule_name, "attach_form" );
	UxCreateWidget( arule_name );


	UxPutMenuHelpWidget( pullDownMenu3, "pullDownMenu_top_b4" );

	UxAddCallback( quitPane_b4, XmNactivateCallback,
			activateCB_quitPane_b4,
			(XtPointer) UxAssociateContext );

	UxAddCallback( UtilsPane_b11, XmNactivateCallback,
			activateCB_UtilsPane_b11,
			(XtPointer) UxAssociateContext );

	UxAddCallback( UtilsPane_b12, XmNactivateCallback,
			activateCB_UtilsPane_b12,
			(XtPointer) UxAssociateContext );

	UxAddCallback( pullDownMenu1_p3_b4, XmNactivateCallback,
			activateCB_pullDownMenu1_p3_b4,
			(XtPointer) UxAssociateContext );

	UxAddCallback( pullDownMenu1_p1_b1, XmNactivateCallback,
			activateCB_pullDownMenu1_p1_b1,
			(XtPointer) UxAssociateContext );

	UxAddCallback( pullDownMenu3_p4_b1, XmNactivateCallback,
			activateCB_pullDownMenu3_p4_b1,
			(XtPointer) UxAssociateContext );

	UxAddCallback( pullDownMenu3_p4_b2, XmNactivateCallback,
			activateCB_pullDownMenu3_p4_b2,
			(XtPointer) UxAssociateContext );

	UxAddCallback( rowasso, XmNentryCallback,
			entryCB_rowasso,
			(XtPointer) UxAssociateContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Associate );

	UxMainWindowSetAreas( mainWindow3, pullDownMenu3, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, workAreaForm3 );
	return ( Associate );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Associate()
{
	swidget                 rtrn;
	_UxCAssociate           *UxContext;

	UxAssociateContext = UxContext =
		(_UxCAssociate *) UxMalloc( sizeof(_UxCAssociate) );

	rtrn = _Uxbuild_Associate();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Associate()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "nothing", action_nothing },
				{ "SelectCommand", action_SelectCommand },
				{ "ClearHelp", action_ClearHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "HelpACT", action_HelpACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_Associate();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

