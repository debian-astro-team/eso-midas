
/*******************************************************************************
	ColAttribute.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <midas_def.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxColAttribute;
	swidget	Uxform8;
	swidget	Uxlabel24;
	swidget	Uxnewform;
	swidget	UxpushButton8;
	swidget	UxpushButton9;
} _UxCColAttribute;

#define ColAttribute            UxColAttributeContext->UxColAttribute
#define form8                   UxColAttributeContext->Uxform8
#define label24                 UxColAttributeContext->Uxlabel24
#define newform                 UxColAttributeContext->Uxnewform
#define pushButton8             UxColAttributeContext->UxpushButton8
#define pushButton9             UxColAttributeContext->UxpushButton9

static _UxCColAttribute	*UxColAttributeContext;

extern int AppendDialogText();

extern void display_col_table();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ColAttribute();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCColAttribute        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxColAttributeContext;
	UxColAttributeContext = UxContext =
			(_UxCColAttribute *) UxGetContext( UxThisWidget );
	{
	extern int tidost;
	extern char ostchoice[60];
	extern int collabchan;
	char *text;
	char newf[TBL_FORLEN+1],comm[160];

	text = UxGetText(UxFindSwidget("newform"));
	if (text != NULL) strcpy(newf,text);
	XtFree(text);
	sprintf(comm,"NAME/COLUMN %s #%d %s",ostchoice,collabchan,newf);
	AppendDialogText(comm);
	TCTCLO(tidost);
	TCTOPN(ostchoice,F_I_MODE,&tidost);
	 display_col_table(UxGetWidget(UxFindSwidget("readtable")));
	
	}
	UxColAttributeContext = UxSaveCtx;
}

static void	activateCB_pushButton9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCColAttribute        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxColAttributeContext;
	UxColAttributeContext = UxContext =
			(_UxCColAttribute *) UxGetContext( UxThisWidget );
	{
	extern swidget attri;
	UxPopdownInterface(attri);
	}
	UxColAttributeContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ColAttribute()
{
	UxPutTitle( ColAttribute, "Column Format" );
	UxPutBackground( ColAttribute, WindowBackground );
	UxPutHeight( ColAttribute, 130 );
	UxPutWidth( ColAttribute, 300 );
	UxPutY( ColAttribute, 460 );
	UxPutX( ColAttribute, 780 );

	UxPutBackground( form8, WindowBackground );
	UxPutHeight( form8, 120 );
	UxPutWidth( form8, 300 );
	UxPutY( form8, 10 );
	UxPutX( form8, 10 );
	UxPutUnitType( form8, "pixels" );
	UxPutResizePolicy( form8, "resize_none" );

	UxPutLabelString( label24, "New Format" );
	UxPutForeground( label24, TextForeground );
	UxPutFontList( label24, BoldTextFont );
	UxPutBackground( label24, LabelBackground );
	UxPutHeight( label24, 30 );
	UxPutWidth( label24, 110 );
	UxPutY( label24, 20 );
	UxPutX( label24, 10 );

	UxPutForeground( newform, TextForeground );
	UxPutFontList( newform, TextFont );
	UxPutBackground( newform, TextBackground );
	UxPutHeight( newform, 40 );
	UxPutWidth( newform, 150 );
	UxPutY( newform, 20 );
	UxPutX( newform, 120 );

	UxPutLabelString( pushButton8, "Apply" );
	UxPutForeground( pushButton8, ButtonForeground );
	UxPutFontList( pushButton8, BoldTextFont );
	UxPutBackground( pushButton8, ButtonBackground );
	UxPutHeight( pushButton8, 30 );
	UxPutWidth( pushButton8, 110 );
	UxPutY( pushButton8, 90 );
	UxPutX( pushButton8, 20 );

	UxPutLabelString( pushButton9, "Cancel" );
	UxPutForeground( pushButton9, ButtonForeground );
	UxPutFontList( pushButton9, BoldTextFont );
	UxPutBackground( pushButton9, ButtonBackground );
	UxPutHeight( pushButton9, 30 );
	UxPutWidth( pushButton9, 110 );
	UxPutY( pushButton9, 90 );
	UxPutX( pushButton9, 150 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ColAttribute()
{
	/* Create the swidgets */

	ColAttribute = UxCreateTransientShell( "ColAttribute", NO_PARENT );
	UxPutContext( ColAttribute, UxColAttributeContext );

	form8 = UxCreateForm( "form8", ColAttribute );
	label24 = UxCreateLabel( "label24", form8 );
	newform = UxCreateText( "newform", form8 );
	pushButton8 = UxCreatePushButton( "pushButton8", form8 );
	pushButton9 = UxCreatePushButton( "pushButton9", form8 );

	_Uxinit_ColAttribute();

	/* Create the X widgets */

	UxCreateWidget( ColAttribute );
	UxCreateWidget( form8 );
	UxCreateWidget( label24 );
	UxCreateWidget( newform );
	UxCreateWidget( pushButton8 );
	UxCreateWidget( pushButton9 );

	UxAddCallback( pushButton8, XmNactivateCallback,
			activateCB_pushButton8,
			(XtPointer) UxColAttributeContext );

	UxAddCallback( pushButton9, XmNactivateCallback,
			activateCB_pushButton9,
			(XtPointer) UxColAttributeContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ColAttribute );

	return ( ColAttribute );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ColAttribute()
{
	swidget                 rtrn;
	_UxCColAttribute        *UxContext;

	UxColAttributeContext = UxContext =
		(_UxCColAttribute *) UxMalloc( sizeof(_UxCColAttribute) );

	rtrn = _Uxbuild_ColAttribute();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ColAttribute()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_ColAttribute();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

