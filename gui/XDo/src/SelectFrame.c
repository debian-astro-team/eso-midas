
/*******************************************************************************
	SelectFrame.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxFsBox.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <midas_def.h>
#include <str.h>

#include <ExternResources.h>

extern void my_select();

char *filechoice;
int fno;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSelectFrame;
} _UxCSelectFrame;

#define SelectFrame             UxSelectFrameContext->UxSelectFrame

static _UxCSelectFrame	*UxSelectFrameContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SelectFrame();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	cancelCB_SelectFrame( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectFrame         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectFrameContext;
	UxSelectFrameContext = UxContext =
			(_UxCSelectFrame *) UxGetContext( UxThisWidget );
	{
	extern swidget flist;
	UxPopdownInterface(flist);
	}
	UxSelectFrameContext = UxSaveCtx;
}

static void	okCallback_SelectFrame( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectFrame         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectFrameContext;
	UxSelectFrameContext = UxContext =
			(_UxCSelectFrame *) UxGetContext( UxThisWidget );
	{
	Widget text;
	extern swidget flist;
	extern char *filechoice;
	
	int tid,col,i,j,len,kun;
	char name[200],*nm;
	text = XmFileSelectionBoxGetChild(UxWidget,XmDIALOG_TEXT);
	/*XmTextSetString(text,"File List copied into table sel_temp.tbl ");*/
	
	if (filechoice) {
	 TCTINI("sel_temp",F_IO_MODE,F_TRANS,1,fno,&tid);
	 TCCINI(tid,D_C_FORMAT,200,"A15"," ","FILE",&col);
	 len = 0 ;
	 for (i=0; i<fno; i++) {
	    j = strloc(filechoice +len,',');
	    filechoice[len+j] = '\0';
	    j++;
	    strcpy(name,filechoice+len);
	    
	    len += j;
	    TCEWRC(tid,i+1,col,name);
	    }
	 TCTCLO(tid);
	  UxPutText(UxFindSwidget("ost_t2"),"sel_temp.tbl");
	}
	else {
	 nm = XmTextGetString(text);
	 UxPutText(UxFindSwidget("ost_t2"),nm);
	 
	 SCKWRC("INPUTC",1,"lola",1,4,&kun);
	 }
	UxPopdownInterface(flist);     
	}
	UxSelectFrameContext = UxSaveCtx;
}

static void	applyCB_SelectFrame( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectFrame         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectFrameContext;
	UxSelectFrameContext = UxContext =
			(_UxCSelectFrame *) UxGetContext( UxThisWidget );
	{
	/*XmFileSelectionBoxCallbackStruct *cbs;
	char *filename; 
	XmStringGetLtoR(cbs->value,XmSTRING_DEFAULT_CHARSET,&filename);
	printf("Filename: %s \n",filename);
	XtFree(filename);*/
	}
	UxSelectFrameContext = UxSaveCtx;
}

static void	createCB_SelectFrame( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectFrame         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectFrameContext;
	UxSelectFrameContext = UxContext =
			(_UxCSelectFrame *) UxGetContext( UxThisWidget );
	{
	Widget fl,tl;
	
	fl = XmFileSelectionBoxGetChild(UxGetWidget(UxFindSwidget("SelectFrame")),XmDIALOG_FILE_LIST);
	tl = XmFileSelectionBoxGetChild(UxGetWidget(UxFindSwidget("SelectFrame")),XmDIALOG_TEXT);
	XmTextSetString(tl,""); 
	
	XtVaSetValues(fl,XmNselectionPolicy,XmEXTENDED_SELECT,NULL);
	XtAddCallback(fl,XmNextendedSelectionCallback,my_select,NULL);
	}
	UxSelectFrameContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SelectFrame()
{
	UxPutDirectoryValid( SelectFrame, "true" );
	UxPutTextString( SelectFrame, "" );
	UxPutBottomShadowColor( SelectFrame, ButtonBackground );
	UxPutButtonFontList( SelectFrame, BoldTextFont );
	UxPutTextFontList( SelectFrame, BoldTextFont );
	UxPutLabelFontList( SelectFrame, BoldTextFont );
	UxPutBackground( SelectFrame, WindowBackground );
	UxPutHeight( SelectFrame, 400 );
	UxPutWidth( SelectFrame, 350 );
	UxPutY( SelectFrame, 330 );
	UxPutX( SelectFrame, 370 );
	UxPutUnitType( SelectFrame, "pixels" );
	UxPutResizePolicy( SelectFrame, "resize_none" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SelectFrame()
{
	/* Create the swidgets */

	SelectFrame = UxCreateFileSelectionBox( "SelectFrame", NO_PARENT );
	UxPutContext( SelectFrame, UxSelectFrameContext );
	UxPutDefaultShell( SelectFrame, "topLevelShell" );


	_Uxinit_SelectFrame();

	/* Create the X widgets */

	UxCreateWidget( SelectFrame );
	createCB_SelectFrame( UxGetWidget( SelectFrame ),
			(XtPointer) UxSelectFrameContext, (XtPointer) NULL );


	UxAddCallback( SelectFrame, XmNcancelCallback,
			cancelCB_SelectFrame,
			(XtPointer) UxSelectFrameContext );
	UxAddCallback( SelectFrame, XmNokCallback,
			okCallback_SelectFrame,
			(XtPointer) UxSelectFrameContext );
	UxAddCallback( SelectFrame, XmNapplyCallback,
			applyCB_SelectFrame,
			(XtPointer) UxSelectFrameContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SelectFrame );

	return ( SelectFrame );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SelectFrame()
{
	swidget                 rtrn;
	_UxCSelectFrame         *UxContext;

	UxSelectFrameContext = UxContext =
		(_UxCSelectFrame *) UxMalloc( sizeof(_UxCSelectFrame) );

	{
		filechoice = (char * )0;
		rtrn = _Uxbuild_SelectFrame();

		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SelectFrame()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_SelectFrame();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

