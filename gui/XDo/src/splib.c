/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE     C
.AUTHOR       M.Peron - ESO Garching
.KEYWORDS     calibration
.VERSION 1.0  1-Mar-1994   Implementation
 
 090825		last modif
------------------------------------------------------------*/

#include <stdio.h>
#include <midas_def.h>
#include <tbldef.h>
#include <do.h>
#include "UxLib.h"


extern void XmListSetPos(), XmListAddItems (), XmListDeleteAllItems();



extern int tidost;
extern int *ivector();

extern float *fvector();

extern void free_ivector(), free_cvector();

extern char **cmatrix();
extern char *cvector();
extern char *osmmget();


extern char ostchoice[60];

int read_ident( lc, ost)
LCTAB *lc;
char *ost;
{
    int inull;
    float rnull;
    double dnull;
    int nulval, sortcol, aw, ar, ncols; 
    int nrows = 0;
    int col_ident;
    int i;
    char str[128];
    extern swidget myerror;
    strcpy( lc->name, ostchoice );

    TCMNUL( &inull, &rnull, &dnull ); /* obtain NULL values */
    if (tidost !=-1) {
         TCTCLO(tidost);
         tidost = -1;
         }
    if ( TCTOPN( ost, F_I_MODE, &tidost ) != 0 ) {
	sprintf( str, "Table %s couldn't be opened.", ost );
	SCTPUT( str );
	return FALSE;
    }
    TCIGET( tidost, &ncols, &lc->nrows, &sortcol, &aw, &ar );
    
    TCCSER( tidost, ":IDENT", &col_ident );
    if ( col_ident == -1 ) {
      UxPutMessageString(UxFindSwidget("errorDialog1"),"Column IDENT missing");
      UxPopupInterface(myerror,no_grab);
      return FALSE;
    }

    /* allocate space for data */
    lc->row    = ivector( 0, lc->nrows - 1 );
    lc->ident   = cmatrix( 0, lc->nrows - 1, 0, MAXION-1 );

    for ( i = 0; i < lc->nrows; i++ ) {
	TCERDC( tidost, i+1, col_ident, lc->ident[nrows], &nulval );
	lc->row[nrows] = i+1;
	nrows++;
    }
    lc->nrows = nrows; /* no. of effective rows */
/*    TCTCLO( tidost ); */
    return TRUE;
}

void free_catalog_table( lc )
LCTAB *lc;
{
    free_ivector(lc->row,    0, lc->nrows - 1);
    free_cvector(lc->ident,   0, (lc->nrows - 1) * (MAXION-1));

    osmmfree((char *)lc);
}

void nrerror( msg ) /* for management of math local routine errors */
char *msg;
{
    fprintf(stderr, "* %s *\r\n", msg);
}
int read_table_column(wlist)
Widget wlist;
{
LCLAB *Lcl;
int i,dummy;
XmStringTable str_list;
char mylabel[TBL_LABLEN+1];

Lcl  = (LCLAB *)osmmget( sizeof(LCLAB) );
TCIGET(tidost,&Lcl->ncol,&dummy,&dummy,&dummy,&dummy);
Lcl->name = cmatrix(0,Lcl->ncol-1,0,TBL_LABLEN);

for (i=0; i<Lcl->ncol; i++) {
   TCLGET(tidost,i+1,mylabel);
   strcpy(Lcl->name[i],mylabel);
   }
str_list = (XmStringTable)XtMalloc(Lcl->ncol *sizeof(XmString *));
for (i=0; i<Lcl->ncol; i++) 
    str_list[i] = XmStringCreateSimple(Lcl->name[i]);
XmListSetPos(wlist, 1);
XmListDeleteAllItems(wlist);
XmListAddItems(wlist, str_list, Lcl->ncol, 1);

for ( i = 0; i <Lcl->ncol; i++ )
     XmStringFree(str_list[i]);
XtFree((char *)str_list);

return 0;
}
