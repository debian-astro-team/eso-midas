/*------------------------------------------------------------
 * This is the project main program file for Ux generated  
 * code. You may add application dependent source code
 * at the appropriate places. 
 * 			     
 * Do not modify the statements preceded by the dollar
 * sign ($), these statements will be replaced with
 * the appropriate source code when the main program is
 * automatically generated. 
 *			
 * $Date: 2009-08-25 13:23:09 $  		$Revision: 1.2 $ 
 *-----------------------------------------------------------*/

/*
.VERSION
 090825		last modif
*/

#ifdef XOPEN_CATALOG
#include <locale.h>
#endif

#include <stdlib.h>
#include <signal.h>

#include <UxLib.h>
#include <midas_def.h>
#include <tbldef.h>

#include <StdResources.h>


extern int initmytable(), read_ost_table();

extern void add_ident_table(), add_col_table(), display_ident_table();




/*----------------------------------------------
 * Insert application global declarations here
 *---------------------------------------------*/

char *osmmget();
 static void reread();
char session[3];
char ostchoice[60],claschoice[60],assochoice[60],*colchoice,*coltemp,*rowtext[256];
char commerr[180];
int tidost,tidclas,tidasso,colno,itemsize,do8,doI,rowno,arowno,MonitorPid;
int msgvalue,coltempno,collabchan;
int tidcomm;
Widget row1[40],row2[40],row3[40],rowrule[256][2];
Widget arow1[20],arow2[20],arow3[20],arow4[20];
Widget FileListWidget,sb;
swidget pb, save, crea, ctx, hist, srch, klist,clist,flist,olist,dlist,ilist;
swidget classi,clashaupt,assohaupt,asso,myerror,mymsg,help,attri;
extern swidget create_transientShell3(), 
               create_transientShell8(),
               create_transientShell1(),
               create_applicationClas(),
               create_Classify(), 
               create_SelectFrame(),
               create_SelectDescrTable(),
               create_SelectInstrument(),
               create_Association(),
               create_Associate(),
               create_Warning(),
               create_errorDialog1(),  
               create_HelpDo(),
               create_ColAttribute();                 



int main(argc,argv)
	int 	argc;
	char 	*argv[];
{
	/*-----------------------------------------------------------
	 * Declarations.
	 * The default identifier - mainIface will only be declared  
	 * if the interface function is global and of type swidget. 
	 * To change the identifier to a different name, modify the
	 * string mainIface in the file "main.dat".  If "mainIface"
	 * is declared, it will be used below where the return value 
	 * of  PJ_INTERFACE_FUNCTION_CALL will be assigned to it.
         *-----------------------------------------------------------*/ 

  	swidget mainIface;
        int nbytes,i;


 	/*--------------------------------
	 * Interface function declaration
	 *-------------------------------*/	

 	swidget popup_ApplicWindow();

	

	/*---------------------
	 * Initialize Program
	 *--------------------*/

#ifdef XOPEN_CATALOG
	setlocale(LC_ALL, "");
#endif

  	UxTopLevel = UxInitialize("Tempdo", &argc, argv);

	/*------------------------------------------------------
	 * Insert initialization code for your application here
	 *-----------------------------------------------------*/
        /*
	
        */ 
       
       MonitorPid = atoi(argv[1]);
       strcpy(session, argv[2]);
       session[2] = '\0';    
      
 

        /* printf("Version: %s, Mail: %s, Print: %s\nContext: %s\nMidwork: %s\nContexts: %s\n",
               midvers, mid_mail, print_com, mid_ctx, mid_work, contxt); */

          SCSPRO("-1"); 
         osscatch(SIGUSR1,reread);         
         nbytes = (TBL_LABLEN+1) * 256;
         colchoice = osmmget(nbytes);
       /*  strcpy(colchoice,"IDENT");
    
         colno = 1;*/
         oscfill(colchoice,'\0',nbytes);
         for (i=0; i<60; i++) ostchoice[i] = '\0'; 
 
         msgvalue = 1;     
         nbytes = (TBL_LABLEN+1) * 20;
         coltemp = osmmget(nbytes);
      
         crea  = create_transientShell3(); 

         klist = create_transientShell8(); 
         clist = create_transientShell1(); 
         flist = create_SelectFrame();
         dlist = create_SelectDescrTable();
         ilist = create_SelectInstrument();
         classi = create_applicationClas();
         clashaupt = create_Classify();
         assohaupt = create_Association(); 
         myerror = create_errorDialog1();   
         asso = create_Associate(); 
         mymsg = create_Warning();
         help = create_HelpDo();
         attri = create_ColAttribute();
         for (i=0; i<256; i++) {
            rowrule[i][0] = (Widget )0;
            rowrule[i][1] = (Widget )0; 
            } 
         for (i=0; i<40; i++) {
            row1[i] = (Widget )0;
            row2[i] = (Widget )0;
            row3[i] = (Widget )0;
            }
         for (i=0; i<20; i++) {
            arow1[i] = (Widget )0;
            arow2[i] = (Widget )0;
            arow3[i] = (Widget )0;
            arow4[i] = (Widget )0;
            }
         tidclas = -1;
         tidost = -1;
         tidasso = -1; 
         tidcomm = -1;
         initmytable();
 
 

	/*----------------------------------------------------------------
	 * Create and popup the first window of the interface.  The 	 
	 * return value can be used in the popdown or destroy functions.
	 * The swidget return value of  PJ_INTERFACE_FUNCTION_CALL will
	 * be assigned to "mainIface" from  PJ_INTERFACE_RETVAL_TYPE. 
	 *---------------------------------------------------------------*/

	mainIface = popup_ApplicWindow();

	UxPopupInterface(mainIface, no_grab);

	/*-------------------------
	 * Enter the event loop 
	 *------------------------*/

  	UxMainLoop();

return 0;
}

static  void reread()
{
extern int tidost,tidcomm ;
extern char ostchoice[60];
int openflag,null;
 
osscatch(SIGUSR1,reread);
if (tidost != -1 ) { 
   TCTCLO(tidost);
   tidost = -1 ;
   }
TCTOPN("TAB_COMM.tbl",F_I_MODE,&tidcomm);
TCERDI(tidcomm,1,4,&openflag,&null);
 
if (openflag == 1) {
   TCTOPN(ostchoice,F_I_MODE,&tidost);
 
   add_ident_table(UxGetWidget(UxFindSwidget("identlist"))); 
   add_col_table(UxGetWidget(UxFindSwidget("readtable")));
   } 
else {  
   strcpy(ostchoice,"ost.tbl"); 
   if (read_ost_table()) display_ident_table(UxGetWidget(UxFindSwidget("identlist")));
    }
TCTCLO(tidcomm);
}

 
 
