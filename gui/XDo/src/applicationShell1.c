
/*******************************************************************************
	applicationShell1.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxScText.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <Xm/LabelG.h>
#include <midas_def.h>
#include <tbldef.h>
char *osmmget();
char *rowlab[256];

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxapplicationClas;
	swidget	UxmainWindow1;
	swidget	UxpullDownMenu1;
	swidget	UxquitPane1;
	swidget	UxquitPane_b1;
	swidget	UxpullDownMenu_top_b2;
	swidget	UxUtilsPane1;
	swidget	UxUtilsPane_b1;
	swidget	UxUtilsPane_b2;
	swidget	UxcommandsCascade1;
	swidget	UxpullDownMenu1_p3;
	swidget	UxpullDownMenu1_p3_b1;
	swidget	UxpullDownMenu1_p3_b2;
	swidget	UxpullDownMenu1_top_b1;
	swidget	UxpullDownMenu1_p4;
	swidget	UxpullDownMenu1_p4_b1;
	swidget	UxpullDownMenu1_p4_b2;
	swidget	UxpullDownMenu1_top_b4;
	swidget	UxworkAreaForm1;
	swidget	Uxform6;
	swidget	UxscrolledWindow6;
	swidget	Uxrowdescr;
	swidget	Uxlabel7;
	swidget	UxpushButton13;
	swidget	UxpushButton14;
	swidget	Uxlabel8;
	swidget	Uxclas_t1;
	swidget	UxscrolledWindow7;
	swidget	Uxcriteria;
} _UxCapplicationClas;

#define applicationClas         UxApplicationClasContext->UxapplicationClas
#define mainWindow1             UxApplicationClasContext->UxmainWindow1
#define pullDownMenu1           UxApplicationClasContext->UxpullDownMenu1
#define quitPane1               UxApplicationClasContext->UxquitPane1
#define quitPane_b1             UxApplicationClasContext->UxquitPane_b1
#define pullDownMenu_top_b2     UxApplicationClasContext->UxpullDownMenu_top_b2
#define UtilsPane1              UxApplicationClasContext->UxUtilsPane1
#define UtilsPane_b1            UxApplicationClasContext->UxUtilsPane_b1
#define UtilsPane_b2            UxApplicationClasContext->UxUtilsPane_b2
#define commandsCascade1        UxApplicationClasContext->UxcommandsCascade1
#define pullDownMenu1_p3        UxApplicationClasContext->UxpullDownMenu1_p3
#define pullDownMenu1_p3_b1     UxApplicationClasContext->UxpullDownMenu1_p3_b1
#define pullDownMenu1_p3_b2     UxApplicationClasContext->UxpullDownMenu1_p3_b2
#define pullDownMenu1_top_b1    UxApplicationClasContext->UxpullDownMenu1_top_b1
#define pullDownMenu1_p4        UxApplicationClasContext->UxpullDownMenu1_p4
#define pullDownMenu1_p4_b1     UxApplicationClasContext->UxpullDownMenu1_p4_b1
#define pullDownMenu1_p4_b2     UxApplicationClasContext->UxpullDownMenu1_p4_b2
#define pullDownMenu1_top_b4    UxApplicationClasContext->UxpullDownMenu1_top_b4
#define workAreaForm1           UxApplicationClasContext->UxworkAreaForm1
#define form6                   UxApplicationClasContext->Uxform6
#define scrolledWindow6         UxApplicationClasContext->UxscrolledWindow6
#define rowdescr                UxApplicationClasContext->Uxrowdescr
#define label7                  UxApplicationClasContext->Uxlabel7
#define pushButton13            UxApplicationClasContext->UxpushButton13
#define pushButton14            UxApplicationClasContext->UxpushButton14
#define label8                  UxApplicationClasContext->Uxlabel8
#define clas_t1                 UxApplicationClasContext->Uxclas_t1
#define scrolledWindow7         UxApplicationClasContext->UxscrolledWindow7
#define criteria                UxApplicationClasContext->Uxcriteria

static _UxCapplicationClas	*UxApplicationClasContext;

extern int  decrypt1(), AppendDialogText(), read_descr();

extern void display_help();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_applicationClas();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_nothing( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	printf("toto:\n");
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	action_SelectCommand( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	
	 
	
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SHelp"),"");
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	char s[100];


	Widget sw = UxWidget;
	
	strcpy(s,"");
	 
	if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {
	   strcpy(s,"Select the OST columns to be displayed into the scrolled window");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   } 
	else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {
	   strcpy(s,"Activate interface for defining classification rules ");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }  
	else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {
	   strcpy(s,"Activate interface for classifying images");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {
	   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }
	else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {
	   strcpy(s,"Create the OST table");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {
	   strcpy(s,"Popdown this interface");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ 
	   strcpy(s,"Name of the Observation Summary Table to be created ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ 
	   strcpy(s,"List of frames to be processed ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }  
	  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  
	   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  
	   strcpy(s,"Push button to popup the File Selection Box");     
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  
	        strcpy(s,"Push button to popup the File Selection Box");  
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   } 
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	action_HelpACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	/* char *s;
	 s = XmTextGetSelection(UxWidget); 
	 
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	*/}
	UxApplicationClasContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_quitPane_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern swidget classi,ilist;
	UxPopdownInterface(ilist);  
	UxPopdownInterface(classi);
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern char ostchoice[60]; 
	char command[160];
	sprintf(command,"COPY/dd template.bdf  *,3 %s",ostchoice);
	AppendDialogText(command);
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern int tidost,msgvalue;
	extern Widget rowrule[256][2];
	extern swidget mymsg,myerror;
	extern char ostchoice[60],commerr[180];
	char descr[16],command[180],type[2],selecrit[256];
	char temp[256],mytext[256],mylabel[TBL_LABLEN+2];
	int bytelem,noelem,ncol,dummy,i,tidtemp,coltemp;
	strcpy(descr,UxGetText(UxFindSwidget("clas_t1")));
	if (descr[0] == '\0') {
	   UxPutMessageString(UxFindSwidget("errorDialog1"),"No rule name specified");
	   UxPopupInterface(myerror,no_grab);
	   return;
	    }
	SCDFND(tidost,descr,type,&noelem,&bytelem);
	strcpy(selecrit,UxGetText(UxFindSwidget("criteria"))); 
	if (selecrit[0] == '\0') {
	
	   TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);
	   oscfill(selecrit,256,'\0');
	   for (i=1; i<=ncol; i++) {
	          strcpy(mytext, XmTextGetString(rowrule[i-1][1])); 
	          if (*mytext) {
	             mylabel[0] = ':';   
	             TCLGET(tidost,i,mylabel+1);
	            if (selecrit[0] != '\0') strcat(selecrit,".AND.");
	            decrypt1(mylabel,mytext,selecrit,temp);
	            }
	   }
	}
	  sprintf( command," @s saverule %s %s TMP_RULE ",ostchoice  ,descr );
	 
	 TCTINI("TMP_RULE" ,F_IO_MODE,F_TRANS,1,2,&tidtemp);
	 TCCINI(tidtemp,D_C_FORMAT,strlen(selecrit),"A20"," ","VAL",&coltemp);
	 TCEWRC(tidtemp, 1,coltemp,selecrit);
	 TCTCLO(tidtemp);
	if (type[0] == ' ')  AppendDialogText(command);
	 
	else 
	{
	  msgvalue = 1; 
	  sprintf(commerr," @s  saverule %s %s TMP_RULE ",ostchoice  ,descr );
	  UxPutMessageString( UxFindSwidget("Warning"),"The rule already exists, do you want to overwrite it?");
	  UxPopupInterface(mymsg,exclusive_grab);
	}
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p3_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern swidget ilist;
	extern int doI;
	UxPutListLabelString(UxFindSwidget("SelectInstrument"),"Rule");
	UxPutDialogTitle(UxFindSwidget("SelectInstrument"),"Select Rule");
	doI = 1;
	read_descr(UxGetWidget(UxFindSwidget("SelectInstrument")));
	
	
	UxPopupInterface(ilist,no_grab);}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p4_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern swidget help;
	display_help(help,"Rule.help");
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p4_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern swidget help;
	display_help(help,"Rule_default.help");
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	entryCB_rowdescr( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	/*XmRowColumnCallbackStruct *cbs;
	Widget pb = cbs->widget;
	cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;
	printf("%s: %s\n",XtName(pb),cbs->data);*/
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	createCB_rowdescr( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_pushButton13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern int tidost;
	extern Widget rowrule[256][2];
	int ncol,dummy,i,lennew;
	char mylabel[TBL_LABLEN+2],*temp,selecrit[256],mytext[256];
	TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);
	temp = osmmget(256);
	oscfill(selecrit,256,'\0');
	for (i=1; i<=ncol; i++) {
	          strcpy(mytext, XmTextGetString(rowrule[i-1][1])); 
	     if (*mytext) {
	         mylabel[0] = ':';   
	         TCLGET(tidost,i,mylabel+1);
	         if (selecrit[0] != '\0') strcat(selecrit,".AND.");
	         decrypt1(mylabel,mytext,selecrit,temp);
	         lennew = strlen(selecrit);
	          }
	}
	UxPutText(UxFindSwidget("criteria"),selecrit);
	/*SCDWRC(tidost,"test",1,selecrit,1,lennew,&unit);*/
	             
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	activateCB_pushButton14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	extern Widget rowrule[256][2];
	extern int tidost;
	int dummy,i,ncol;
	TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);
	for (i=0; i<ncol; i++) {
	   XmTextSetString(rowrule[i][1],"");
	   }
	UxPutText(UxFindSwidget("criteria"),"");
	}
	UxApplicationClasContext = UxSaveCtx;
}

static void	modifyVerifyCB_criteria( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	UxApplicationClasContext = UxSaveCtx;
}

static void	valueChangedCB_criteria( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCapplicationClas     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicationClasContext;
	UxApplicationClasContext = UxContext =
			(_UxCapplicationClas *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicationClasContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_applicationClas()
{
	UxPutTitle( applicationClas, "Classification Rules" );
	UxPutHeight( applicationClas, 670 );
	UxPutWidth( applicationClas, 370 );
	UxPutY( applicationClas, 260 );
	UxPutX( applicationClas, 690 );

	UxPutUnitType( mainWindow1, "pixels" );
	UxPutBackground( mainWindow1, WindowBackground );
	UxPutHeight( mainWindow1, 470 );
	UxPutWidth( mainWindow1, 700 );
	UxPutY( mainWindow1, 180 );
	UxPutX( mainWindow1, 200 );

	UxPutForeground( pullDownMenu1, MenuForeground );
	UxPutBackground( pullDownMenu1, MenuBackground );
	UxPutMenuAccelerator( pullDownMenu1, "<KeyUp>F10" );
	UxPutRowColumnType( pullDownMenu1, "menu_bar" );
	UxPutBorderWidth( pullDownMenu1, 0 );

	UxPutForeground( quitPane1, MenuForeground );
	UxPutBackground( quitPane1, MenuBackground );
	UxPutRowColumnType( quitPane1, "menu_pulldown" );

	UxPutFontList( quitPane_b1, BoldTextFont );
	UxPutLabelString( quitPane_b1, "Bye" );

	UxPutForeground( pullDownMenu_top_b2, MenuForeground );
	UxPutFontList( pullDownMenu_top_b2, BoldTextFont );
	UxPutBackground( pullDownMenu_top_b2, MenuBackground );
	UxPutMnemonic( pullDownMenu_top_b2, "Q" );
	UxPutLabelString( pullDownMenu_top_b2, "Quit" );

	UxPutForeground( UtilsPane1, MenuForeground );
	UxPutBackground( UtilsPane1, MenuBackground );
	UxPutRowColumnType( UtilsPane1, "menu_pulldown" );

	UxPutFontList( UtilsPane_b1, BoldTextFont );
	UxPutLabelString( UtilsPane_b1, "Import " );

	UxPutFontList( UtilsPane_b2, BoldTextFont );
	UxPutLabelString( UtilsPane_b2, "Save" );

	UxPutX( commandsCascade1, 101 );
	UxPutMnemonic( commandsCascade1, "R" );
	UxPutForeground( commandsCascade1, MenuForeground );
	UxPutFontList( commandsCascade1, BoldTextFont );
	UxPutBackground( commandsCascade1, MenuBackground );
	UxPutLabelString( commandsCascade1, "Rule" );

	UxPutForeground( pullDownMenu1_p3, MenuForeground );
	UxPutBackground( pullDownMenu1_p3, MenuBackground );
	UxPutRowColumnType( pullDownMenu1_p3, "menu_pulldown" );

	UxPutFontList( pullDownMenu1_p3_b1, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p3_b1, "Editor..." );

	UxPutSensitive( pullDownMenu1_p3_b2, "false" );
	UxPutFontList( pullDownMenu1_p3_b2, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p3_b2, "Delete" );

	UxPutForeground( pullDownMenu1_top_b1, MenuForeground );
	UxPutFontList( pullDownMenu1_top_b1, BoldTextFont );
	UxPutBackground( pullDownMenu1_top_b1, MenuBackground );
	UxPutMnemonic( pullDownMenu1_top_b1, "E" );
	UxPutLabelString( pullDownMenu1_top_b1, "Edit" );

	UxPutForeground( pullDownMenu1_p4, MenuForeground );
	UxPutBackground( pullDownMenu1_p4, MenuBackground );
	UxPutRowColumnType( pullDownMenu1_p4, "menu_pulldown" );

	UxPutFontList( pullDownMenu1_p4_b1, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p4_b1, "On this interface..." );

	UxPutFontList( pullDownMenu1_p4_b2, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p4_b2, "On NTT defaults..." );

	UxPutForeground( pullDownMenu1_top_b4, MenuForeground );
	UxPutFontList( pullDownMenu1_top_b4, BoldTextFont );
	UxPutBackground( pullDownMenu1_top_b4, MenuBackground );
	UxPutMnemonic( pullDownMenu1_top_b4, "H" );
	UxPutLabelString( pullDownMenu1_top_b4, "Help" );

	UxPutBackground( workAreaForm1, WindowBackground );
	UxPutBorderWidth( workAreaForm1, 0 );
	UxPutHeight( workAreaForm1, 440 );
	UxPutWidth( workAreaForm1, 490 );

	UxPutBackground( form6, WindowBackground );
	UxPutHeight( form6, 610 );
	UxPutWidth( form6, 350 );
	UxPutY( form6, 10 );
	UxPutX( form6, 10 );
	UxPutUnitType( form6, "pixels" );
	UxPutResizePolicy( form6, "resize_none" );

	UxPutScrollBarDisplayPolicy( scrolledWindow6, "static" );
	UxPutScrollBarPlacement( scrolledWindow6, "bottom_left" );
	UxPutBackground( scrolledWindow6, WindowBackground );
	UxPutHeight( scrolledWindow6, 380 );
	UxPutWidth( scrolledWindow6, 350 );
	UxPutY( scrolledWindow6, 30 );
	UxPutX( scrolledWindow6, 10 );
	UxPutScrollingPolicy( scrolledWindow6, "automatic" );

	UxPutResizeWidth( rowdescr, "true" );
	UxPutNumColumns( rowdescr, 2 );
	UxPutPacking( rowdescr, "pack_column" );
	UxPutOrientation( rowdescr, "horizontal" );
	UxPutEntryAlignment( rowdescr, "alignment_center" );
	UxPutBackground( rowdescr, WindowBackground );
	UxPutHeight( rowdescr, 370 );
	UxPutWidth( rowdescr, 350 );
	UxPutY( rowdescr, 8 );
	UxPutX( rowdescr, 8 );

	UxPutAlignment( label7, "alignment_beginning" );
	UxPutLabelString( label7, "Selection Criteria" );
	UxPutFontList( label7, BoldTextFont );
	UxPutBackground( label7, WindowBackground );
	UxPutHeight( label7, 40 );
	UxPutWidth( label7, 240 );
	UxPutY( label7, 420 );
	UxPutX( label7, 10 );

	UxPutLabelString( pushButton13, "Apply" );
	UxPutForeground( pushButton13, ButtonForeground );
	UxPutFontList( pushButton13, BoldTextFont );
	UxPutBackground( pushButton13, ButtonBackground );
	UxPutHeight( pushButton13, 40 );
	UxPutWidth( pushButton13, 80 );
	UxPutY( pushButton13, 530 );
	UxPutX( pushButton13, 140 );

	UxPutLabelString( pushButton14, "Clear" );
	UxPutForeground( pushButton14, ButtonForeground );
	UxPutFontList( pushButton14, BoldTextFont );
	UxPutBackground( pushButton14, ButtonBackground );
	UxPutHeight( pushButton14, 40 );
	UxPutWidth( pushButton14, 80 );
	UxPutY( pushButton14, 530 );
	UxPutX( pushButton14, 250 );

	UxPutAlignment( label8, "alignment_beginning" );
	UxPutLabelString( label8, "Rule Name" );
	UxPutFontList( label8, BoldTextFont );
	UxPutBackground( label8, WindowBackground );
	UxPutHeight( label8, 40 );
	UxPutWidth( label8, 100 );
	UxPutY( label8, 410 );
	UxPutX( label8, 20 );

	UxPutForeground( clas_t1, TextForeground );
	UxPutFontList( clas_t1, BoldSmallFont );
	UxPutBackground( clas_t1, TextBackground );
	UxPutHeight( clas_t1, 40 );
	UxPutWidth( clas_t1, 200 );
	UxPutY( clas_t1, 450 );
	UxPutX( clas_t1, 20 );

	UxPutScrollBarPlacement( scrolledWindow7, "bottom_left" );
	UxPutHeight( scrolledWindow7, 60 );
	UxPutBackground( scrolledWindow7, WindowBackground );
	UxPutShadowThickness( scrolledWindow7, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow7, "static" );
	UxPutVisualPolicy( scrolledWindow7, "variable" );
	UxPutY( scrolledWindow7, 490 );
	UxPutX( scrolledWindow7, 0 );
	UxPutScrollingPolicy( scrolledWindow7, "application_defined" );

	UxPutForeground( criteria, TextForeground );
	UxPutEditable( criteria, "true" );
	UxPutFontList( criteria, BoldTextFont );
	UxPutBackground( criteria, TextBackground );
	UxPutHeight( criteria, 60 );
	UxPutWidth( criteria, 350 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_applicationClas()
{
	/* Create the swidgets */

	applicationClas = UxCreateApplicationShell( "applicationClas", NO_PARENT );
	UxPutContext( applicationClas, UxApplicationClasContext );

	mainWindow1 = UxCreateMainWindow( "mainWindow1", applicationClas );
	pullDownMenu1 = UxCreateRowColumn( "pullDownMenu1", mainWindow1 );
	quitPane1 = UxCreateRowColumn( "quitPane1", pullDownMenu1 );
	quitPane_b1 = UxCreatePushButtonGadget( "quitPane_b1", quitPane1 );
	pullDownMenu_top_b2 = UxCreateCascadeButton( "pullDownMenu_top_b2", pullDownMenu1 );
	UtilsPane1 = UxCreateRowColumn( "UtilsPane1", pullDownMenu1 );
	UtilsPane_b1 = UxCreatePushButtonGadget( "UtilsPane_b1", UtilsPane1 );
	UtilsPane_b2 = UxCreatePushButtonGadget( "UtilsPane_b2", UtilsPane1 );
	commandsCascade1 = UxCreateCascadeButton( "commandsCascade1", pullDownMenu1 );
	pullDownMenu1_p3 = UxCreateRowColumn( "pullDownMenu1_p3", pullDownMenu1 );
	pullDownMenu1_p3_b1 = UxCreatePushButtonGadget( "pullDownMenu1_p3_b1", pullDownMenu1_p3 );
	pullDownMenu1_p3_b2 = UxCreatePushButtonGadget( "pullDownMenu1_p3_b2", pullDownMenu1_p3 );
	pullDownMenu1_top_b1 = UxCreateCascadeButton( "pullDownMenu1_top_b1", pullDownMenu1 );
	pullDownMenu1_p4 = UxCreateRowColumn( "pullDownMenu1_p4", pullDownMenu1 );
	pullDownMenu1_p4_b1 = UxCreatePushButtonGadget( "pullDownMenu1_p4_b1", pullDownMenu1_p4 );
	pullDownMenu1_p4_b2 = UxCreatePushButtonGadget( "pullDownMenu1_p4_b2", pullDownMenu1_p4 );
	pullDownMenu1_top_b4 = UxCreateCascadeButton( "pullDownMenu1_top_b4", pullDownMenu1 );
	workAreaForm1 = UxCreateForm( "workAreaForm1", mainWindow1 );
	form6 = UxCreateForm( "form6", workAreaForm1 );
	scrolledWindow6 = UxCreateScrolledWindow( "scrolledWindow6", form6 );
	rowdescr = UxCreateRowColumn( "rowdescr", scrolledWindow6 );
	label7 = UxCreateLabel( "label7", form6 );
	pushButton13 = UxCreatePushButton( "pushButton13", form6 );
	pushButton14 = UxCreatePushButton( "pushButton14", form6 );
	label8 = UxCreateLabel( "label8", form6 );
	clas_t1 = UxCreateText( "clas_t1", form6 );
	scrolledWindow7 = UxCreateScrolledWindow( "scrolledWindow7", form6 );
	criteria = UxCreateScrolledText( "criteria", scrolledWindow7 );

	_Uxinit_applicationClas();

	/* Create the X widgets */

	UxCreateWidget( applicationClas );
	UxCreateWidget( mainWindow1 );
	UxCreateWidget( pullDownMenu1 );
	UxCreateWidget( quitPane1 );
	UxCreateWidget( quitPane_b1 );
	UxPutSubMenuId( pullDownMenu_top_b2, "quitPane1" );
	UxCreateWidget( pullDownMenu_top_b2 );

	UxCreateWidget( UtilsPane1 );
	UxCreateWidget( UtilsPane_b1 );
	UxCreateWidget( UtilsPane_b2 );
	UxPutSubMenuId( commandsCascade1, "UtilsPane1" );
	UxCreateWidget( commandsCascade1 );

	UxCreateWidget( pullDownMenu1_p3 );
	UxCreateWidget( pullDownMenu1_p3_b1 );
	UxCreateWidget( pullDownMenu1_p3_b2 );
	UxPutSubMenuId( pullDownMenu1_top_b1, "pullDownMenu1_p3" );
	UxCreateWidget( pullDownMenu1_top_b1 );

	UxCreateWidget( pullDownMenu1_p4 );
	UxCreateWidget( pullDownMenu1_p4_b1 );
	UxCreateWidget( pullDownMenu1_p4_b2 );
	UxPutSubMenuId( pullDownMenu1_top_b4, "pullDownMenu1_p4" );
	UxCreateWidget( pullDownMenu1_top_b4 );

	UxCreateWidget( workAreaForm1 );
	UxCreateWidget( form6 );
	UxPutTopAttachment( scrolledWindow6, "attach_form" );
	UxPutTopOffset( scrolledWindow6, 50 );
	UxPutBottomOffset( scrolledWindow6, 70 );
	UxPutBottomAttachment( scrolledWindow6, "attach_none" );
	UxPutLeftOffset( scrolledWindow6, 10 );
	UxPutRightOffset( scrolledWindow6, 10 );
	UxPutRightAttachment( scrolledWindow6, "attach_form" );
	UxPutLeftAttachment( scrolledWindow6, "attach_form" );
	UxCreateWidget( scrolledWindow6 );

	UxCreateWidget( rowdescr );
	createCB_rowdescr( UxGetWidget( rowdescr ),
			(XtPointer) UxApplicationClasContext, (XtPointer) NULL );

	UxPutTopAttachment( label7, "attach_form" );
	UxPutTopOffset( label7, 440 );
	UxCreateWidget( label7 );

	UxPutLeftOffset( pushButton13, 40 );
	UxPutLeftAttachment( pushButton13, "attach_form" );
	UxPutBottomOffset( pushButton13, 10 );
	UxPutBottomAttachment( pushButton13, "attach_form" );
	UxCreateWidget( pushButton13 );

	UxPutBottomOffset( pushButton14, 10 );
	UxPutBottomAttachment( pushButton14, "attach_form" );
	UxPutRightOffset( pushButton14, 40 );
	UxPutRightAttachment( pushButton14, "attach_form" );
	UxPutLeftWidget( pushButton14, "pushButton13" );
	UxPutLeftOffset( pushButton14, 40 );
	UxPutLeftAttachment( pushButton14, "attach_none" );
	UxCreateWidget( pushButton14 );

	UxPutLeftAttachment( label8, "attach_form" );
	UxPutLeftOffset( label8, 10 );
	UxPutTopOffset( label8, 0 );
	UxPutTopAttachment( label8, "attach_form" );
	UxCreateWidget( label8 );

	UxPutLeftOffset( clas_t1, 130 );
	UxPutLeftAttachment( clas_t1, "attach_form" );
	UxPutTopOffset( clas_t1, 0 );
	UxPutTopAttachment( clas_t1, "attach_form" );
	UxCreateWidget( clas_t1 );

	UxPutTopOffset( scrolledWindow7, 480 );
	UxPutTopAttachment( scrolledWindow7, "attach_form" );
	UxPutRightOffset( scrolledWindow7, 10 );
	UxPutRightAttachment( scrolledWindow7, "attach_form" );
	UxPutLeftOffset( scrolledWindow7, 10 );
	UxPutLeftAttachment( scrolledWindow7, "attach_form" );
	UxCreateWidget( scrolledWindow7 );

	UxCreateWidget( criteria );

	UxPutMenuHelpWidget( pullDownMenu1, "pullDownMenu_top_b2" );

	UxAddCallback( quitPane_b1, XmNactivateCallback,
			activateCB_quitPane_b1,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( UtilsPane_b1, XmNactivateCallback,
			activateCB_UtilsPane_b1,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( UtilsPane_b2, XmNactivateCallback,
			activateCB_UtilsPane_b2,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( pullDownMenu1_p3_b1, XmNactivateCallback,
			activateCB_pullDownMenu1_p3_b1,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( pullDownMenu1_p4_b1, XmNactivateCallback,
			activateCB_pullDownMenu1_p4_b1,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( pullDownMenu1_p4_b2, XmNactivateCallback,
			activateCB_pullDownMenu1_p4_b2,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( rowdescr, XmNentryCallback,
			entryCB_rowdescr,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( pushButton13, XmNactivateCallback,
			activateCB_pushButton13,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( pushButton14, XmNactivateCallback,
			activateCB_pushButton14,
			(XtPointer) UxApplicationClasContext );

	UxAddCallback( criteria, XmNmodifyVerifyCallback,
			modifyVerifyCB_criteria,
			(XtPointer) UxApplicationClasContext );
	UxAddCallback( criteria, XmNvalueChangedCallback,
			valueChangedCB_criteria,
			(XtPointer) UxApplicationClasContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( applicationClas );

	UxMainWindowSetAreas( mainWindow1, pullDownMenu1, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, workAreaForm1 );
	return ( applicationClas );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_applicationClas()
{
	swidget                 rtrn;
	_UxCapplicationClas     *UxContext;

	UxApplicationClasContext = UxContext =
		(_UxCapplicationClas *) UxMalloc( sizeof(_UxCapplicationClas) );

	rtrn = _Uxbuild_applicationClas();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_applicationClas()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "nothing", action_nothing },
				{ "SelectCommand", action_SelectCommand },
				{ "ClearHelp", action_ClearHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "HelpACT", action_HelpACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_applicationClas();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

