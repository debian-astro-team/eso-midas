
/*******************************************************************************
	Classify.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <midas_def.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxClassify;
	swidget	UxmainWindow2;
	swidget	UxpullDownMenu2;
	swidget	UxquitPane2;
	swidget	UxquitPane_b3;
	swidget	UxpullDownMenu_top_b3;
	swidget	UxUtilsPane2;
	swidget	UxUtilsPane_b3;
	swidget	UxUtilsPane_b8;
	swidget	UxcommandsCascade2;
	swidget	UxpullDownMenu1_p1;
	swidget	UxpullDownMenu1_p3_b3;
	swidget	UxpullDownMenu1_p1_b3;
	swidget	UxpullDownMenu1_top_b2;
	swidget	UxpullDownMenu2_p4;
	swidget	UxpullDownMenu2_p4_b1;
	swidget	UxpullDownMenu2_p4_b2;
	swidget	UxpullDownMenu2_top_b1;
	swidget	UxworkAreaForm2;
	swidget	Uxform3;
	swidget	UxscrolledWindow1;
	swidget	Uxrowproc;
	swidget	Uxlabel5;
	swidget	UxpushButton6;
	swidget	Uxlabel11;
	swidget	Uxlabel12;
	swidget	Uxlabel13;
	swidget	Uxlabel21;
	swidget	Uxsclas_t1;
} _UxCClassify;

#define Classify                UxClassifyContext->UxClassify
#define mainWindow2             UxClassifyContext->UxmainWindow2
#define pullDownMenu2           UxClassifyContext->UxpullDownMenu2
#define quitPane2               UxClassifyContext->UxquitPane2
#define quitPane_b3             UxClassifyContext->UxquitPane_b3
#define pullDownMenu_top_b3     UxClassifyContext->UxpullDownMenu_top_b3
#define UtilsPane2              UxClassifyContext->UxUtilsPane2
#define UtilsPane_b3            UxClassifyContext->UxUtilsPane_b3
#define UtilsPane_b8            UxClassifyContext->UxUtilsPane_b8
#define commandsCascade2        UxClassifyContext->UxcommandsCascade2
#define pullDownMenu1_p1        UxClassifyContext->UxpullDownMenu1_p1
#define pullDownMenu1_p3_b3     UxClassifyContext->UxpullDownMenu1_p3_b3
#define pullDownMenu1_p1_b3     UxClassifyContext->UxpullDownMenu1_p1_b3
#define pullDownMenu1_top_b2    UxClassifyContext->UxpullDownMenu1_top_b2
#define pullDownMenu2_p4        UxClassifyContext->UxpullDownMenu2_p4
#define pullDownMenu2_p4_b1     UxClassifyContext->UxpullDownMenu2_p4_b1
#define pullDownMenu2_p4_b2     UxClassifyContext->UxpullDownMenu2_p4_b2
#define pullDownMenu2_top_b1    UxClassifyContext->UxpullDownMenu2_top_b1
#define workAreaForm2           UxClassifyContext->UxworkAreaForm2
#define form3                   UxClassifyContext->Uxform3
#define scrolledWindow1         UxClassifyContext->UxscrolledWindow1
#define rowproc                 UxClassifyContext->Uxrowproc
#define label5                  UxClassifyContext->Uxlabel5
#define pushButton6             UxClassifyContext->UxpushButton6
#define label11                 UxClassifyContext->Uxlabel11
#define label12                 UxClassifyContext->Uxlabel12
#define label13                 UxClassifyContext->Uxlabel13
#define label21                 UxClassifyContext->Uxlabel21
#define sclas_t1                UxClassifyContext->Uxsclas_t1

static _UxCClassify	*UxClassifyContext;


extern void  display_col_table(), display_help(), SetFileList();

extern int classify(), save_clas_table();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Classify();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HInit( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	
	}
	UxClassifyContext = UxSaveCtx;
}

static void	action_nothing( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	printf("toto:\n");
	}
	UxClassifyContext = UxSaveCtx;
}

static void	action_SelectCommand( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	
	 
	
	}
	UxClassifyContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SHelp"),"");
	}
	UxClassifyContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	char s[100];
	 
	Widget sw = UxWidget;
	
	strcpy(s,"");
	/*sprintf(comm,"WRITE/OUT here");
	AppendDialogText(comm);*/
	 
	if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {
	   strcpy(s,"Select the OST columns to be displayed into the scrolled window");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   } 
	else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {
	   strcpy(s,"Activate interface for defining classification rules ");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }  
	else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {
	   strcpy(s,"Activate interface for classifying images");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {
	   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");
	   UxPutText(UxFindSwidget("SHelp") ,s); 
	    }
	else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {
	   strcpy(s,"Create the OST table");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {
	   strcpy(s,"Popdown this interface");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ 
	   strcpy(s,"Name of the Observation Summary Table to be created ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ 
	   strcpy(s,"List of frames to be processed ");
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }  
	  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  
	   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  
	   strcpy(s,"Push button to popup the File Selection Box");     
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   }
	  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  
	        strcpy(s,"Push button to popup the File Selection Box");  
	   UxPutText(UxFindSwidget("SHelp3") ,s); 
	   } 
	}
	UxClassifyContext = UxSaveCtx;
}

static void	action_HelpACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	/* char *s;
	 s = XmTextGetSelection(UxWidget); 
	 
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	*/}
	UxClassifyContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_quitPane_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	extern swidget clashaupt;
	extern Widget row1[40],row2[40],row3[40];
	extern int rowno;
	int i;
	for (i=0; i<5; i++) {
	    XmTextSetString(row1[i],"");
	    XmTextSetString(row2[i],"");
	    XmTextSetString(row3[i],"");
	    }
	for (i=5; i<rowno; i++) {
	    XtDestroyWidget(row1[i]);
	    row1[i] = (Widget )0;
	    XtDestroyWidget(row2[i]); 
	    row2[i] = (Widget )0;
	    XtDestroyWidget(row3[i]);
	    row2[i] = (Widget )0;
	    } 
	rowno = 5;
	UxPopdownInterface(clashaupt);
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	char s[1000];
	extern swidget klist,tablelist;
	
	extern int do8;
	int strip =1;
	strcpy(s,"*.tbl");
	SetFileList(UxGetWidget(tablelist),strip,s);
	UxPutTitle(UxFindSwidget("transientShell8"),"Select Classification Table");
	do8 = 1;
	UxPopupInterface(klist,no_grab);
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	char mytext[60];
	int tid;
	strcpy(mytext,UxGetText(UxFindSwidget("sclas_t1")));
	
	tid = TCTID(mytext); 
	if (tid != -1) TCTCLO(tid);
	save_clas_table(mytext);
	
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p3_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	extern int rowno;
	extern Widget row1[40],row2[40],row3[40];
	char mytext[8];
	rowno++; 
	XtVaSetValues(UxGetWidget(UxFindSwidget("rowproc")),XmNnumColumns,rowno,NULL);
	sprintf(mytext,"text1_%d",rowno-1);
	row1[rowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowproc")),NULL);
	sprintf(mytext,"text2_%d",rowno-1);
	row2[rowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowproc")),NULL);
	sprintf(mytext,"text3_%d",rowno-1);
	row3[rowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowproc")),NULL);
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu1_p1_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	extern int rowno;
	extern Widget row1[40],row2[40],row3[40];
	int i;
	for (i=0; i<rowno; i++) {
	   XmTextSetString(row1[i],"");
	   XmTextSetString(row2[i],"");
	   XmTextSetString(row3[i],""); 
	    }
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu2_p4_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	extern swidget help;
	display_help(help,"Classi.help"); 
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_pullDownMenu2_p4_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	 
	}
	UxClassifyContext = UxSaveCtx;
}

static void	entryCB_rowproc( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	/*XmRowColumnCallbackStruct *cbs;
	Widget pb = cbs->widget;
	cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;
	printf("%s: %s\n",XtName(pb),cbs->data);*/
	}
	UxClassifyContext = UxSaveCtx;
}

static void	createCB_rowproc( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	
	}
	UxClassifyContext = UxSaveCtx;
}

static void	activateCB_pushButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCClassify            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxClassifyContext;
	UxClassifyContext = UxContext =
			(_UxCClassify *) UxGetContext( UxThisWidget );
	{
	extern char ostchoice[60];
	extern int tidost;
	extern swidget myerror;
	char command[160],mycval[81],column[18];
	if (tidost == -1) {
	  UxPutMessageString(UxFindSwidget("errorDialog1"),"No OST opened");
	  UxPopupInterface(myerror,no_grab);
	  return;
	  }
	save_clas_table("TEMP_CLAS");
	sprintf(command,"CLASSIFY/IMAGE %s TEMP_CLAS.tbl",ostchoice);
	/*AppendDialogText(command);*/
	mycval[0] = '\0';
	column[0] = '\0';
        TCTCLO(tidost);
	classify(ostchoice,"TEMP_CLAS.tbl",column,mycval);
	TCTOPN(ostchoice,F_I_MODE,&tidost);
	display_col_table(UxGetWidget(UxFindSwidget("readtable")));
	
	}
	UxClassifyContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Classify()
{
	UxPutHeight( Classify, 450 );
	UxPutWidth( Classify, 500 );
	UxPutY( Classify, 387 );
	UxPutX( Classify, 29 );

	UxPutUnitType( mainWindow2, "pixels" );
	UxPutBackground( mainWindow2, WindowBackground );
	UxPutHeight( mainWindow2, 470 );
	UxPutWidth( mainWindow2, 700 );
	UxPutY( mainWindow2, 180 );
	UxPutX( mainWindow2, 200 );

	UxPutForeground( pullDownMenu2, MenuForeground );
	UxPutBackground( pullDownMenu2, MenuBackground );
	UxPutMenuAccelerator( pullDownMenu2, "<KeyUp>F10" );
	UxPutRowColumnType( pullDownMenu2, "menu_bar" );
	UxPutBorderWidth( pullDownMenu2, 0 );

	UxPutForeground( quitPane2, MenuForeground );
	UxPutBackground( quitPane2, MenuBackground );
	UxPutRowColumnType( quitPane2, "menu_pulldown" );

	UxPutFontList( quitPane_b3, BoldTextFont );
	UxPutLabelString( quitPane_b3, "Bye" );

	UxPutForeground( pullDownMenu_top_b3, MenuForeground );
	UxPutFontList( pullDownMenu_top_b3, BoldTextFont );
	UxPutBackground( pullDownMenu_top_b3, MenuBackground );
	UxPutMnemonic( pullDownMenu_top_b3, "Q" );
	UxPutLabelString( pullDownMenu_top_b3, "Quit" );

	UxPutForeground( UtilsPane2, MenuForeground );
	UxPutBackground( UtilsPane2, MenuBackground );
	UxPutRowColumnType( UtilsPane2, "menu_pulldown" );

	UxPutFontList( UtilsPane_b3, BoldTextFont );
	UxPutLabelString( UtilsPane_b3, "Open ..." );

	UxPutFontList( UtilsPane_b8, BoldTextFont );
	UxPutLabelString( UtilsPane_b8, "Save" );

	UxPutX( commandsCascade2, 101 );
	UxPutMnemonic( commandsCascade2, "F" );
	UxPutForeground( commandsCascade2, MenuForeground );
	UxPutFontList( commandsCascade2, BoldTextFont );
	UxPutBackground( commandsCascade2, MenuBackground );
	UxPutLabelString( commandsCascade2, "File" );

	UxPutForeground( pullDownMenu1_p1, MenuForeground );
	UxPutBackground( pullDownMenu1_p1, MenuBackground );
	UxPutRowColumnType( pullDownMenu1_p1, "menu_pulldown" );

	UxPutFontList( pullDownMenu1_p3_b3, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p3_b3, "Add" );

	UxPutFontList( pullDownMenu1_p1_b3, BoldTextFont );
	UxPutLabelString( pullDownMenu1_p1_b3, "Clear" );

	UxPutForeground( pullDownMenu1_top_b2, MenuForeground );
	UxPutFontList( pullDownMenu1_top_b2, BoldTextFont );
	UxPutBackground( pullDownMenu1_top_b2, MenuBackground );
	UxPutMnemonic( pullDownMenu1_top_b2, "E" );
	UxPutLabelString( pullDownMenu1_top_b2, "Edit" );

	UxPutForeground( pullDownMenu2_p4, MenuForeground );
	UxPutBackground( pullDownMenu2_p4, MenuBackground );
	UxPutRowColumnType( pullDownMenu2_p4, "menu_pulldown" );

	UxPutFontList( pullDownMenu2_p4_b1, BoldTextFont );
	UxPutLabelString( pullDownMenu2_p4_b1, "On this interface..." );

	UxPutFontList( pullDownMenu2_p4_b2, BoldTextFont );
	UxPutLabelString( pullDownMenu2_p4_b2, "On importing data..." );

	UxPutForeground( pullDownMenu2_top_b1, MenuForeground );
	UxPutFontList( pullDownMenu2_top_b1, BoldTextFont );
	UxPutBackground( pullDownMenu2_top_b1, MenuBackground );
	UxPutMnemonic( pullDownMenu2_top_b1, "H" );
	UxPutLabelString( pullDownMenu2_top_b1, "Help" );

	UxPutBackground( workAreaForm2, WindowBackground );
	UxPutBorderWidth( workAreaForm2, 0 );
	UxPutHeight( workAreaForm2, 400 );
	UxPutWidth( workAreaForm2, 700 );

	UxPutBackground( form3, WindowBackground );
	UxPutHeight( form3, 400 );
	UxPutWidth( form3, 480 );
	UxPutY( form3, 10 );
	UxPutX( form3, 10 );
	UxPutUnitType( form3, "pixels" );
	UxPutResizePolicy( form3, "resize_none" );

	UxPutVisualPolicy( scrolledWindow1, "variable" );
	UxPutScrollBarDisplayPolicy( scrolledWindow1, "static" );
	UxPutScrollBarPlacement( scrolledWindow1, "bottom_left" );
	UxPutBackground( scrolledWindow1, WindowBackground );
	UxPutHeight( scrolledWindow1, 300 );
	UxPutWidth( scrolledWindow1, 350 );
	UxPutY( scrolledWindow1, 30 );
	UxPutX( scrolledWindow1, 10 );
	UxPutScrollingPolicy( scrolledWindow1, "automatic" );

	UxPutResizeWidth( rowproc, "true" );
	UxPutNumColumns( rowproc, 3 );
	UxPutPacking( rowproc, "pack_column" );
	UxPutOrientation( rowproc, "horizontal" );
	UxPutEntryAlignment( rowproc, "alignment_center" );
	UxPutBackground( rowproc, WindowBackground );
	UxPutHeight( rowproc, 370 );
	UxPutWidth( rowproc, 500 );
	UxPutY( rowproc, 8 );
	UxPutX( rowproc, 8 );

	UxPutAlignment( label5, "alignment_beginning" );
	UxPutLabelString( label5, "Selection Criteria" );
	UxPutFontList( label5, BoldTextFont );
	UxPutBackground( label5, WindowBackground );
	UxPutHeight( label5, 40 );
	UxPutWidth( label5, 240 );
	UxPutY( label5, 420 );
	UxPutX( label5, 10 );

	UxPutLabelString( pushButton6, "Apply" );
	UxPutForeground( pushButton6, ButtonForeground );
	UxPutFontList( pushButton6, BoldTextFont );
	UxPutBackground( pushButton6, ButtonBackground );
	UxPutHeight( pushButton6, 40 );
	UxPutWidth( pushButton6, 80 );
	UxPutY( pushButton6, 530 );
	UxPutX( pushButton6, 140 );

	UxPutLabelString( label11, "DESCR" );
	UxPutFontList( label11, BoldTextFont );
	UxPutBackground( label11, WindowBackground );
	UxPutHeight( label11, 30 );
	UxPutWidth( label11, 130 );
	UxPutY( label11, 10 );
	UxPutX( label11, 40 );

	UxPutLabelString( label12, "OUTCOL" );
	UxPutFontList( label12, BoldTextFont );
	UxPutBackground( label12, WindowBackground );
	UxPutHeight( label12, 30 );
	UxPutWidth( label12, 130 );
	UxPutY( label12, 20 );
	UxPutX( label12, 50 );

	UxPutLabelString( label13, "OUTCHAR" );
	UxPutFontList( label13, BoldTextFont );
	UxPutBackground( label13, WindowBackground );
	UxPutHeight( label13, 30 );
	UxPutWidth( label13, 130 );
	UxPutY( label13, 20 );
	UxPutX( label13, 190 );

	UxPutLabelString( label21, "Classification Table Name" );
	UxPutFontList( label21, BoldTextFont );
	UxPutBackground( label21, WindowBackground );
	UxPutHeight( label21, 30 );
	UxPutWidth( label21, 200 );
	UxPutY( label21, 110 );
	UxPutX( label21, 50 );

	UxPutForeground( sclas_t1, TextForeground );
	UxPutFontList( sclas_t1, BoldTextFont );
	UxPutBackground( sclas_t1, TextBackground );
	UxPutHeight( sclas_t1, 40 );
	UxPutWidth( sclas_t1, 240 );
	UxPutY( sclas_t1, 20 );
	UxPutX( sclas_t1, 230 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Classify()
{
	/* Create the swidgets */

	Classify = UxCreateApplicationShell( "Classify", NO_PARENT );
	UxPutContext( Classify, UxClassifyContext );

	mainWindow2 = UxCreateMainWindow( "mainWindow2", Classify );
	pullDownMenu2 = UxCreateRowColumn( "pullDownMenu2", mainWindow2 );
	quitPane2 = UxCreateRowColumn( "quitPane2", pullDownMenu2 );
	quitPane_b3 = UxCreatePushButtonGadget( "quitPane_b3", quitPane2 );
	pullDownMenu_top_b3 = UxCreateCascadeButton( "pullDownMenu_top_b3", pullDownMenu2 );
	UtilsPane2 = UxCreateRowColumn( "UtilsPane2", pullDownMenu2 );
	UtilsPane_b3 = UxCreatePushButtonGadget( "UtilsPane_b3", UtilsPane2 );
	UtilsPane_b8 = UxCreatePushButtonGadget( "UtilsPane_b8", UtilsPane2 );
	commandsCascade2 = UxCreateCascadeButton( "commandsCascade2", pullDownMenu2 );
	pullDownMenu1_p1 = UxCreateRowColumn( "pullDownMenu1_p1", pullDownMenu2 );
	pullDownMenu1_p3_b3 = UxCreatePushButtonGadget( "pullDownMenu1_p3_b3", pullDownMenu1_p1 );
	pullDownMenu1_p1_b3 = UxCreatePushButtonGadget( "pullDownMenu1_p1_b3", pullDownMenu1_p1 );
	pullDownMenu1_top_b2 = UxCreateCascadeButton( "pullDownMenu1_top_b2", pullDownMenu2 );
	pullDownMenu2_p4 = UxCreateRowColumn( "pullDownMenu2_p4", pullDownMenu2 );
	pullDownMenu2_p4_b1 = UxCreatePushButtonGadget( "pullDownMenu2_p4_b1", pullDownMenu2_p4 );
	pullDownMenu2_p4_b2 = UxCreatePushButtonGadget( "pullDownMenu2_p4_b2", pullDownMenu2_p4 );
	pullDownMenu2_top_b1 = UxCreateCascadeButton( "pullDownMenu2_top_b1", pullDownMenu2 );
	workAreaForm2 = UxCreateForm( "workAreaForm2", mainWindow2 );
	form3 = UxCreateForm( "form3", workAreaForm2 );
	scrolledWindow1 = UxCreateScrolledWindow( "scrolledWindow1", form3 );
	rowproc = UxCreateRowColumn( "rowproc", scrolledWindow1 );
	label5 = UxCreateLabel( "label5", form3 );
	pushButton6 = UxCreatePushButton( "pushButton6", form3 );
	label11 = UxCreateLabel( "label11", form3 );
	label12 = UxCreateLabel( "label12", form3 );
	label13 = UxCreateLabel( "label13", form3 );
	label21 = UxCreateLabel( "label21", form3 );
	sclas_t1 = UxCreateText( "sclas_t1", form3 );

	_Uxinit_Classify();

	/* Create the X widgets */

	UxCreateWidget( Classify );
	UxCreateWidget( mainWindow2 );
	UxCreateWidget( pullDownMenu2 );
	UxCreateWidget( quitPane2 );
	UxCreateWidget( quitPane_b3 );
	UxPutSubMenuId( pullDownMenu_top_b3, "quitPane2" );
	UxCreateWidget( pullDownMenu_top_b3 );

	UxCreateWidget( UtilsPane2 );
	UxCreateWidget( UtilsPane_b3 );
	UxCreateWidget( UtilsPane_b8 );
	UxPutSubMenuId( commandsCascade2, "UtilsPane2" );
	UxCreateWidget( commandsCascade2 );

	UxCreateWidget( pullDownMenu1_p1 );
	UxCreateWidget( pullDownMenu1_p3_b3 );
	UxCreateWidget( pullDownMenu1_p1_b3 );
	UxPutSubMenuId( pullDownMenu1_top_b2, "pullDownMenu1_p1" );
	UxCreateWidget( pullDownMenu1_top_b2 );

	UxCreateWidget( pullDownMenu2_p4 );
	UxCreateWidget( pullDownMenu2_p4_b1 );
	UxCreateWidget( pullDownMenu2_p4_b2 );
	UxPutSubMenuId( pullDownMenu2_top_b1, "pullDownMenu2_p4" );
	UxCreateWidget( pullDownMenu2_top_b1 );

	UxCreateWidget( workAreaForm2 );
	UxCreateWidget( form3 );
	UxPutTopAttachment( scrolledWindow1, "attach_form" );
	UxPutTopOffset( scrolledWindow1, 150 );
	UxPutBottomOffset( scrolledWindow1, 60 );
	UxPutBottomAttachment( scrolledWindow1, "attach_form" );
	UxPutLeftOffset( scrolledWindow1, 10 );
	UxPutRightOffset( scrolledWindow1, 10 );
	UxPutRightAttachment( scrolledWindow1, "attach_form" );
	UxPutLeftAttachment( scrolledWindow1, "attach_form" );
	UxCreateWidget( scrolledWindow1 );

	UxCreateWidget( rowproc );
	createCB_rowproc( UxGetWidget( rowproc ),
			(XtPointer) UxClassifyContext, (XtPointer) NULL );

	UxPutTopAttachment( label5, "attach_form" );
	UxPutTopOffset( label5, 440 );
	UxCreateWidget( label5 );

	UxPutLeftOffset( pushButton6, 200 );
	UxPutLeftAttachment( pushButton6, "attach_form" );
	UxPutBottomOffset( pushButton6, 10 );
	UxPutBottomAttachment( pushButton6, "attach_form" );
	UxCreateWidget( pushButton6 );

	UxPutLeftOffset( label11, 40 );
	UxPutLeftAttachment( label11, "attach_form" );
	UxPutTopOffset( label11, 100 );
	UxPutTopAttachment( label11, "attach_form" );
	UxCreateWidget( label11 );

	UxPutTopOffset( label12, 100 );
	UxPutTopAttachment( label12, "attach_form" );
	UxPutLeftOffset( label12, 180 );
	UxPutLeftAttachment( label12, "attach_form" );
	UxCreateWidget( label12 );

	UxPutTopOffset( label13, 100 );
	UxPutTopAttachment( label13, "attach_form" );
	UxPutLeftOffset( label13, 320 );
	UxPutLeftAttachment( label13, "attach_form" );
	UxCreateWidget( label13 );

	UxPutLeftOffset( label21, 20 );
	UxPutLeftAttachment( label21, "attach_form" );
	UxPutTopOffset( label21, 30 );
	UxPutTopAttachment( label21, "attach_form" );
	UxCreateWidget( label21 );

	UxPutTopOffset( sclas_t1, 25 );
	UxPutTopAttachment( sclas_t1, "attach_form" );
	UxCreateWidget( sclas_t1 );


	UxPutMenuHelpWidget( pullDownMenu2, "pullDownMenu_top_b3" );

	UxAddCallback( quitPane_b3, XmNactivateCallback,
			activateCB_quitPane_b3,
			(XtPointer) UxClassifyContext );

	UxAddCallback( UtilsPane_b3, XmNactivateCallback,
			activateCB_UtilsPane_b3,
			(XtPointer) UxClassifyContext );

	UxAddCallback( UtilsPane_b8, XmNactivateCallback,
			activateCB_UtilsPane_b8,
			(XtPointer) UxClassifyContext );

	UxAddCallback( pullDownMenu1_p3_b3, XmNactivateCallback,
			activateCB_pullDownMenu1_p3_b3,
			(XtPointer) UxClassifyContext );

	UxAddCallback( pullDownMenu1_p1_b3, XmNactivateCallback,
			activateCB_pullDownMenu1_p1_b3,
			(XtPointer) UxClassifyContext );

	UxAddCallback( pullDownMenu2_p4_b1, XmNactivateCallback,
			activateCB_pullDownMenu2_p4_b1,
			(XtPointer) UxClassifyContext );

	UxAddCallback( pullDownMenu2_p4_b2, XmNactivateCallback,
			activateCB_pullDownMenu2_p4_b2,
			(XtPointer) UxClassifyContext );

	UxAddCallback( rowproc, XmNentryCallback,
			entryCB_rowproc,
			(XtPointer) UxClassifyContext );

	UxAddCallback( pushButton6, XmNactivateCallback,
			activateCB_pushButton6,
			(XtPointer) UxClassifyContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Classify );

	UxMainWindowSetAreas( mainWindow2, pullDownMenu2, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, workAreaForm2 );
	return ( Classify );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Classify()
{
	swidget                 rtrn;
	_UxCClassify            *UxContext;

	UxClassifyContext = UxContext =
		(_UxCClassify *) UxMalloc( sizeof(_UxCClassify) );

	rtrn = _Uxbuild_Classify();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Classify()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HInit", action_HInit },
				{ "nothing", action_nothing },
				{ "SelectCommand", action_SelectCommand },
				{ "ClearHelp", action_ClearHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "HelpACT", action_HelpACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_Classify();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

