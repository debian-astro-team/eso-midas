/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        applic.c
.MODULE       subroutines -- gmidas.exe
.LANGUAGE     C
.AUTHOR       M.Peron- ESO 
.PURPOSE      
.KEYWORDS     application routines, Midas related routines.
.VERSION 1.0  1-Mar-1994   Implementation
 
 090825		last modif
------------------------------------------------------------*/

#include <stdio.h>
#include <math.h>

#include <do.h>
#include "UxLib.h"
#include <midas_def.h>
#include <str.h>
#include <tbldef.h>
#include <osparms.h>

#include <Xm/List.h>



extern int file_exists(), read_ident(), stsnum();

extern void free_cmatrix(), free_ivector(); 

char *mode[] = {"IMAGING (RED)","IMAGING (BLUE)", "IMAGING (SUSI)",
                "GRISM SPECTROSCOPY","GRATING SPECTROSCOPY (RED)",
                "GRATING SPECTROSCOPY (BLUE)", 
                 "ECHELLE SPECTROSCOPY"};
char *all[] = {"EXPTIME","RA","DEC","DATE","EXPTYPE","OPATH"};
char *imar[] = {"R_FILTER_ID"};
char *imab[] = {"B_FILTER_ID"};
char *ech[] = {"R_FILTER_ID","GRISM_ID","R_GRATING_ID","R_GRATING_WLEN"};
char *gr[] = {"R_FILTER_ID","GRISM_ID"}; 
char *gtr[] = {"R_FILTER_ID","R_GRATING_ID","R_GRATING_WLEN"};
char *gtb[] = {"B_FILTER_ID","B_GRATING_ID","B_GRATING_WLEN"};
char *susi[] = {"S_FILTER_ID"};
char *osmmget();

/*******************
  Local variables
 *******************/
static LCTAB  *Lc = NULL;	/* Line catalog table */
static LCCOL  *Lcc = NULL;
static char *   List[10240];
static int	OstAllocated = FALSE;
static int oldcbs;
/**********************************
   application external variables 
 **********************************/
extern char     ostchoice[60];  /* OST table*/
extern char     claschoice[60]; /* Clas table*/
extern char     *rowtext[256];  /*Text Widget*/
extern Widget sb;

int size;
double atof();
float *fvector();
int *ivector();
char **cmatrix();
char *cvector();
extern int tidost,tidclas,colno,fno;
extern char *colchoice;
extern char *filechoice;
void free_ost();
int read_ost_table()
{
    if ( ! file_exists( ostchoice, ".tbl" ) ) {
    	SCTPUT( "*** OST table doesn't exist ***" );
    	return(FALSE);
    }

    if ( Lc != NULL ) { /* deallocate space */
    	free_cmatrix(Lc->ident, 0, Lc->nrows - 1, 0, MAXION - 1);
        free_ivector(Lc->row,0,Lc->nrows-1);
	osmmfree((char *)Lc);
    }
    Lc  = (LCTAB *)osmmget( sizeof(LCTAB) );

    /* call to a library routine */
    if( !read_ident( Lc,ostchoice) ) {
	Lc = NULL;
	return FALSE;
    }
    return TRUE;
}


void display_ident_table( wlist )
Widget wlist;
{
    int i;
    XmStringTable str_list;

   if ( OstAllocated )
	free_ost();
    OstAllocated = TRUE;
    for ( i = 0; i < Lc->nrows; i++ )
    	List[i] = (char *)osmmget( 80 );
    List[Lc->nrows] = NULL; 

    for ( i = 0; i < Lc->nrows; i++ )
    	strcpy( List[i], Lc->ident[i]);  

    str_list = (XmStringTable)XtMalloc(Lc->nrows * sizeof(XmString *));
    for ( i = 0; i < Lc->nrows; i++ ) {
        str_list[i] = XmStringCreateSimple(List[i]);
        }
    XmListSetPos(wlist, 1);
    XmListDeleteAllItems(wlist);
    XmListAddItems(wlist, str_list, Lc->nrows, 1);

    for ( i = 0; i <Lc->nrows; i++ )
        XmStringFree(str_list[i]);
    XtFree((char*)str_list);


}


void display_col_table( wlist )
Widget wlist;
{
    extern swidget myerror;
    int i,j,k,dummy,mycol[256],len,mylen,nulls[256],pos;
    int top,visible;
    char form[TBL_FORLEN+1];
    char title[500],mylabel[TBL_LABLEN+1],mymess[60];
    XmStringTable str_list;
    Position x1,x2,y;

    if (Lcc != NULL  ) {
       free_cmatrix(Lcc->value,0,Lcc->nrows-1,0,MAXVAL-1);
       osmmfree((char *)Lcc);
       Lcc = (LCCOL *)0;
       }
    for (i=0; i<500; i++) title[i] = '\0'; 
    mylen = 1;
    k = 0;
/*    sprintf(mymess,"WRITE/OUT %s",colchoice);
    AppendDialogText(mymess);*/
    for (i=0; i<colno; i++) {
     TCLSER(tidost,colchoice + i*(TBL_LABLEN+1),&mycol[i]);
     if (mycol[i] == -1) {
       XmListDeleteAllItems(UxGetWidget(UxFindSwidget("readtable")));
       XmTextSetString(UxGetWidget(UxFindSwidget("scrollabel")),"");
       sprintf(mymess,"Column %s missing",colchoice+i*(TBL_LABLEN+1));
       UxPutMessageString(UxFindSwidget("errorDialog1"),mymess);
       UxPopupInterface(myerror,no_grab);
       return ;
       }

       strncpy(mylabel,colchoice + i*(TBL_LABLEN+1),1+TBL_LABLEN);
       TCFGET(tidost,mycol[i],form,&len,&dummy);
       title[k++] = ' ';
       for (j=0; mylabel[j] && (j<len); j++) title[k++] = mylabel[j]  ;
       for (;   j < len; j++) title[k++] = ' ';
       mylen += len;
       mylen++;
       }
    Lcc = (LCCOL *)osmmget(sizeof(LCCOL)) ;
    TCIGET( tidost, &dummy, &Lcc->nrows, &dummy, &dummy, &dummy );
    Lcc->value = cmatrix(0,Lcc->nrows-1,0,mylen);
    str_list = (XmStringTable)XtMalloc(Lcc->nrows *sizeof(XmString *));
   for (i=0; i< Lcc->nrows; i++)  {
       TCRRDC(tidost,i+1,colno,mycol,Lcc->value[i],nulls);
       str_list[i] = XmStringCreateSimple(Lcc->value[i]);
       }
XmListSetPos(wlist, 1);
XmListDeleteAllItems(wlist);
XmListAddItems(wlist, str_list, Lcc->nrows, 1);
XtVaGetValues(UxGetWidget(UxFindSwidget("identlist")),
             XmNtopItemPosition,&top,
             XmNvisibleItemCount,&visible,
             NULL);
XmListSetBottomPos(wlist,top+visible-1);
for ( i = 0; i <Lcc->nrows; i++ )
       XmStringFree(str_list[i]);
  XtFree((char *)str_list);
XmTextSetString(UxGetWidget(UxFindSwidget("scrollabel")),title);
XmTextShowPosition(UxGetWidget(UxFindSwidget("scrollabel")),(XmTextPosition) 0);
pos = XmTextGetTopCharacter(UxGetWidget(UxFindSwidget("scrollabel")));
XmTextPosToXY(UxGetWidget(UxFindSwidget("scrollabel")),pos,&x1,&y);
pos = pos+1;
XmTextPosToXY(UxGetWidget(UxFindSwidget("scrollabel")),pos,&x2,&y);
size = x2-x1;
oldcbs = 0;
}
void free_ost()
{
    int i;

    for ( i = 0; i < Lc->nrows; i++ )
    	osmmfree( List[i] );
}


void myscrollv(scrollbar,client_data,cbs)
Widget scrollbar;
XtPointer client_data;
XmScrollBarCallbackStruct *cbs;
{

   XmListSetPos(UxGetWidget(UxFindSwidget("readtable")),cbs->value+1); 
   XmListSetPos(UxGetWidget(UxFindSwidget("identlist")),cbs->value+1); 
}

void myscrollh(scrollbar,client_data,cbs)
Widget scrollbar;
XtPointer client_data;
XmScrollBarCallbackStruct *cbs;
{
XmTextPosition pos;
short int column;
XtVaGetValues(UxGetWidget(UxFindSwidget("scrollabel")),XmNcolumns,&column,NULL);
pos = cbs->value /size;
if (cbs->value > oldcbs) {
XmTextShowPosition(UxGetWidget(UxFindSwidget("scrollabel")),(XmTextPosition) (pos+column));
}
else {
XmTextShowPosition(UxGetWidget(UxFindSwidget("scrollabel")),(XmTextPosition) (pos));
}
oldcbs = cbs->value;
}
void myscrollh2(scrollbar,client_data,cbs)
Widget scrollbar;
XtPointer client_data;
XmScrollBarCallbackStruct *cbs;
{
/*XmListSetPos(UxGetWidget(UxFindSwidget("sl2")),cbs->pixel); */
}

void my_select(list_w,client_data,cbs)
Widget list_w;
XtPointer client_data;
XmListCallbackStruct *cbs;
{
int i, nbytes;
char *choice ;
if (filechoice) osmmfree(filechoice);
nbytes = 200 * cbs->selected_item_count;
fno = cbs->selected_item_count;
filechoice = osmmget(nbytes);
for (i=0; i<cbs->selected_item_count ;i++) {
  XmStringGetLtoR(cbs->selected_items[i],XmSTRING_DEFAULT_CHARSET,&choice);
  strcat(filechoice,choice); 
  filechoice[strlen(filechoice)] = ',';
  filechoice[strlen(filechoice)+1] = '\0';
   }
XtFree(choice);
}

void myrow(widget,client_data,cbs)
Widget widget;
XtPointer client_data;
XmRowColumnCallbackStruct *cbs;
{
}

void never_called(text_w,i)
Widget text_w;
int i;
{
rowtext[i-1] = XmTextGetString(text_w); 
}
void read_descr(mytext)
Widget mytext;
{
int indx,status,lbuf,numbuf,comma,item_no,i;
XmString *str;
char buf[32];
char *descr[200];
lbuf = 32;
item_no = 0;
indx = 1;
while (!(status = SCDINF(tidost,indx,4,buf,lbuf,&numbuf))){
      if (buf[0] == ' ' ) break;
      comma = stuindex(buf,",");
      if (buf[comma+1] == 'C' && strncmp(buf,"TLABL",5) !=0 && strncmp(buf,"TSELT",5) !=0 && strncmp(buf,"HISTORY",7) !=0 ) {
          descr[item_no] = osmmget(16);
          strncpy(descr[item_no],buf,comma-1);
          comma = stuindex(descr[item_no]," ") ;
          if (*(descr[item_no]+comma)) *(descr[item_no]+comma) = '\0';
          item_no++;
          } 
      indx++; 
     }
str = (XmString *)XtMalloc(item_no * sizeof(XmString));
for (i=0; i<item_no; i++)
   str[i] = XmStringCreateSimple(descr[i]);

XtVaSetValues(mytext,
              XmNlistItems,str,
              XmNlistItemCount,item_no,
              NULL);
}

int redecomp(descr)
char descr[257];
{
extern Widget rowrule[256][2];
extern swidget myerror;
extern int tidost;
int i,col,len,fin,colnew,ncol,dummy,index;
char label[TBL_LABLEN+1],op[3],crit[256];
char mymess[60];
index = strscans(descr,":#");
if (!descr[index]) {
   sprintf(mymess,"This is probably not a classification rule");
   UxPutMessageString(UxFindSwidget("errorDialog1"),mymess);
   UxPopupInterface(myerror,no_grab);
   return(-1);
}
TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);
for (i=0; i<ncol; i++) XmTextSetString(rowrule[i][1],"");
oscfill(label,TBL_LABLEN+1,'\0');
len = strlen(descr);
i =   stuindex(descr,".");
strncpy(label,descr,i);
TCLSER(tidost,label+1,&colnew);
descr = descr + i+1;
while (*descr) {
   oscfill(crit,256,'\0');
   fin = 1;
   col = colnew;
   while (fin) {
       if (*descr == ':') {
          i =   stuindex(descr,".");
          oscfill(label,TBL_LABLEN+1,'\0');
          strncpy(label,descr,i);
          TCLSER(tidost,label+1,&col);
          descr = descr + i+1;
          }
       else if (*descr == 'O' || *descr == 'A') {
          if (*descr == 'O') {
               strcat(crit,"|");
               descr = descr+3;
               i = stuindex(descr,".");
               descr = descr+i+1;
               }
          else {
              descr = descr +4; 
              i =   stuindex(descr,".");
              oscfill(label,17,'\0');
              strncpy(label,descr,i);
              TCLSER(tidost,label+1,&colnew);
              descr = descr + i+1;
              if (col ==colnew) strcat(crit,"&");
              else fin = 0;
              }
       }
       else if (stucomp(descr,"NULL") == 0) {
          descr+= 4;
          } 
       else {
         if (strncmp(descr,"E",1) == 0) strcpy(op,"=");
         else if (strncmp(descr,"N",1) == 0) strcpy(op,"!=");
         else if (strncmp(descr,"LE",2) == 0) strcpy(op,"<=");
         else if (strncmp(descr,"LT",2) == 0) strcpy(op,"<");
         else if (strncmp(descr,"GE",2) == 0) strcpy(op,">=");
         else if (strncmp(descr,"GT",2) == 0)  strcpy(op,">");
         else {
              while (*descr == ' ') descr++ ;
              fin = 0;
              continue;
              }
         strcat(crit,op);
         descr = descr + 3;
         if (*descr == '"') {
                    descr++;
                    if (*descr == '"') descr +=2;
                    else {
                        i = strloc(descr,'"');
                        strncat(crit,descr,i);
                        descr = descr+i+2;
                        }
                        if (!*descr) fin = 0;
                    }
         else {
                    i = stsnum(descr);
                    strncat(crit,descr,i);
                    descr = descr+i;
                    if (!*descr) fin=0;
                    else if (*descr == '.') descr++;      
                    }
         }
   }
/*  UxPutText(UxWidgetToSwidget(row[col-1]),crit);  */
 XmTextSetString(rowrule[col-1][1],crit);
  }
descr = descr-len;
return 0;
} 


int read_clas_table()
{
extern Widget row1[40],row2[40],row3[40];
extern swidget myerror;
extern int rowno;
int cold,colo,cols,nrow,dummy,i,null;
char mytext[8],descr[TBL_LABLEN+1],mystring[80];
if (tidclas !=-1) TCTCLO(tidclas);
TCTOPN(claschoice,F_I_MODE,&tidclas);
UxPutText(UxFindSwidget("sclas_t1"),claschoice);
TCLSER(tidclas,"DESCR",&cold);
if (cold == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column DESCR missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCLSER(tidclas,"OUTCOL",&colo);
if (colo == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column OUTCOL missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCLSER(tidclas,"OUTCHAR",&cols);
if (cols == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column OUTCHAR missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCIGET(tidclas,&dummy,&nrow,&dummy,&dummy,&dummy);
if (row1[0]) for (i=0; i<rowno; i++) {
      XmTextSetString(row1[i],"");
      XmTextSetString(row2[i],"");
      XmTextSetString(row3[i],"");
      }
if (nrow>5 && nrow > rowno ) XtVaSetValues(UxGetWidget(UxFindSwidget("rowproc")),XmNnumColumns,nrow,NULL);
for (i=0; i<nrow; i++) {
    if (!row1[i]) {
      sprintf(mytext,"text1_%d",i);
      row1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowproc")),NULL);
      sprintf(mytext,"text2_%d",i);
      row2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowproc")),NULL);
      sprintf(mytext,"text3_%d",i);
      row3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowproc")),NULL);
      }
 }
if(nrow>5 && nrow > rowno) rowno=nrow;
for (i=0; i<nrow; i++) {
    TCERDC(tidclas,i+1,cold,descr,&null);
    XmTextSetString(row1[i],descr);
    TCERDC(tidclas,i+1,colo,descr,&null);
    XmTextSetString(row2[i],descr);
    TCERDC(tidclas,i+1,cols,mystring,&null);
    XmTextSetString(row3[i],mystring);
    }
return 0;

}
int read_asso_table()
{
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
extern swidget myerror;
extern int arowno,tidasso;
extern char assochoice[60];
int colf,colr1,colr2,colw,nrow,dummy,i,null;
char mytext[8],function[100],range1[20],range2[20],weight[10];
if (tidasso !=-1) TCTCLO(tidasso);
TCTOPN(assochoice,F_IO_MODE,&tidasso);
TCLSER(tidasso,"FUNCTION",&colf);
if (colf == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column FUNCTION missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCLSER(tidasso,"RANGE_1",&colr1);
if (colr1 == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column RANGE_1 missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCLSER(tidasso,"RANGE_2",&colr2);
if (colr2 == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column RANGE_2 missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCLSER(tidasso,"WEIGHT",&colw);
if (colw == -1) {
  UxPutMessageString(UxFindSwidget("errorDialog1"),"Column WEIGHT missing"); 
  UxPopupInterface(myerror,no_grab);
  return -1;
  }
TCIGET(tidasso,&dummy,&nrow,&dummy,&dummy,&dummy);
if (nrow>5 && nrow > arowno) XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,nrow,NULL);
if (arow1[0]) for (i=0; i<arowno; i++) {
      XmTextSetString(arow1[i],"");
      XmTextSetString(arow2[i],"");
      XmTextSetString(arow3[i],"");
      XmTextSetString(arow4[i],"");
      }
for (i=0; i<nrow; i++) {
    if (!arow1[i]) {
      sprintf(mytext,"a1_%d",i);
      arow1[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowasso")),NULL);
      sprintf(mytext,"a2_%d",i);
      arow2[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowasso")),NULL);
      sprintf(mytext,"a3_%d",i);
      arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowasso")),NULL);

      sprintf(mytext,"a4_%d",i);
      arow3[i] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget(                UxFindSwidget("rowasso")),NULL);
      }
 }
if(nrow>5 && nrow > arowno) arowno=nrow;
for (i=0; i<nrow; i++) {
    TCERDC(tidasso,i+1,colf,function,&null);
    XmTextSetString(arow1[i],function);
    TCERDC(tidasso,i+1,colr1,range1,&null);
    XmTextSetString(arow2[i],range1);
    TCERDC(tidasso,i+1,colr2,range2,&null);
    XmTextSetString(arow3[i],range2);
    TCERDC(tidasso,i+1,colw,weight,&null);
    XmTextSetString(arow4[i],weight);
    }
UxPutText(UxFindSwidget("arule_name"),assochoice);
return 0;
}


int save_clas_table(table)
char *table;
{
extern Widget row1[40],row2[40],row3[40];
extern int rowno;
int tid,cold,colo,cols,i,index;
char *mytext,lab1[TBL_LABLEN+1],lab2[80];

TCTINI(table,F_TRANS,F_O_MODE,3,rowno,&tid);
TCCINI(tid,D_C_FORMAT,16,"A10"," ","DESCR",&cold);
TCCINI(tid,D_C_FORMAT,16,"A10"," ","OUTCOL",&colo);
TCCINI(tid,D_C_FORMAT,80,"A20"," ","OUTCHAR",&cols);
for (i=0; i<rowno;i++) {
    mytext = XmTextGetString(row1[i]);
    index = strskip(mytext,' ');
    strcpy(lab1,mytext+index);
    XtFree(mytext);
    if (lab1[0] == '\0') continue;
    index = strbskip(lab1,' ');
    if (lab1[index+1] !='\0' ) lab1[index+1] = '\0';
    TCEWRC(tid,i+1,cold,lab1);
    mytext = XmTextGetString(row2[i]);
    index = strskip(mytext,' ');
    strcpy(lab1,mytext+index);
    XtFree(mytext);
    index = strbskip(lab1,' ');
    if (lab1[index+1] !='\0' ) lab1[index+1] = '\0';
    TCEWRC(tid,i+1,colo,lab1);
    mytext = XmTextGetString(row3[i]);
    index = strskip(mytext,' ');
    strcpy(lab2,mytext+index);
    XtFree(mytext);
    index = strbskip(lab2,' ');
    if (lab2[index+1] !='\0' ) lab2[index+1] = '\0';
    TCEWRC(tid,i+1,cols,lab2);
   }
TCTCLO(tid);
return 0;
}

int save_asso_table(table)
char *table;
{
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];
extern int arowno;
int tid,colf,colr1,colr2,colw,i,index;
char *mytext,lab1[100],lab2[20];
double weight;
TCTINI(table,F_TRANS,F_O_MODE,4,arowno,&tid);
TCCINI(tid,D_C_FORMAT,100,"A50"," ","FUNCTION",&colf);
TCCINI(tid,D_C_FORMAT,20,"A20"," ","RANGE_1",&colr1);
TCCINI(tid,D_C_FORMAT,20,"A20"," ","RANGE_2",&colr2);
TCCINI(tid,D_R4_FORMAT,1,"F5.2"," ","WEIGHT",&colw);
for (i=0; i<arowno;i++) {
    mytext= XmTextGetString(arow1[i]);
    if (mytext[0] == '\0') continue;
    index = strskip(mytext,' ');
    strcpy(lab1,mytext+index);
    XtFree(mytext);
    index = strbskip(lab1,' ');
    if (lab1[index+1] !='\0') lab1[index+1] = '\0';
    TCEWRC(tid,i+1,colf,lab1);
    mytext =  XmTextGetString(arow2[i]);
    index = strskip(mytext,' ');
    strcpy(lab2,mytext+index);
    XtFree(mytext);
    index = strbskip(lab2,' ');
    if (lab2[index+1] !='\0') lab2[index+1] = '\0';
    TCEWRC(tid,i+1,colr1,lab2);
    mytext =  XmTextGetString(arow3[i]);
    index = strskip(mytext,' ');
    strcpy(lab2,mytext+index);
    XtFree(mytext);
    index = strbskip(lab2,' ');
    if (lab2[index+1] !='\0') lab2[index+1] = '\0';
    TCEWRC(tid,i+1,colr2,lab2);
    mytext = XmTextGetString(arow4[i]);
    weight = atof(mytext);
    XtFree(mytext);
    TCEWRD(tid,i+1,colw,&weight);
   }
TCTCLO(tid);
return 0;
}

void add_ident_table(wlist)
Widget wlist;
{
   extern int tidost;
   int dummy,nrow,col_ident,nul,upper,visible,top,item_no;
   char mident[MAXION];
   XmString item;
   XtVaGetValues(wlist,XmNitemCount,&upper,
                       XmNvisibleItemCount,&visible,
                       XmNtopItemPosition,&top,
                       NULL);
   TCIGET( tidost, &dummy, &nrow, &dummy, &dummy, &dummy );
   if (nrow > upper) {
     TCLSER(tidost,"IDENT",&col_ident);
     TCERDC(tidost,nrow,col_ident,mident,&nul);
     item = XmStringCreateSimple(mident);
     XmListAddItemUnselected(wlist,item,0);
     item_no = upper+1;
     if (item_no >=top+visible) XmListSetBottomPos(wlist,item_no);
     XmStringFree(item);
     }
}


void add_col_table(wlist)
Widget wlist;
{
  int mylen,mycol[256],nulls[256],i,nrow, len,dummy,upper;
  int top,visible,item_no;   
  char *myvalue,form[TBL_FORLEN+1];
  XmString item;
  mylen = 0;
  
  XtVaGetValues(wlist,XmNitemCount,&upper,
                       XmNvisibleItemCount,&visible,
                       XmNtopItemPosition,&top,
                       NULL);
  for (i=0; i<colno; i++) {
       TCLSER(tidost,colchoice + i*(TBL_LABLEN+1),&mycol[i]);
       TCFGET(tidost,mycol[i],form,&len,&dummy);
       mylen += len;
       mylen++;
       }
  TCIGET( tidost, &dummy, &nrow, &dummy, &dummy, &dummy );
  if (nrow > upper) {
     myvalue = osmmget(mylen+1);
     TCRRDC(tidost,nrow,colno,mycol,myvalue,nulls);
     item = XmStringCreateSimple(myvalue);
     XmListAddItemUnselected(wlist,item,0);
     item_no = upper+1;
     if (item_no >=top+visible) XmListSetBottomPos(wlist,item_no);
     XmStringFree(item);
     osmmfree(myvalue);
     }
}
int initmytable()
{
extern int tidcomm;
int col,pid,val;
char cval[2];
TCTINI("TAB_COMM",F_TRANS,F_O_MODE,3,1,&tidcomm);
TCCINI(tidcomm,D_I4_FORMAT,1,"I6"," ","PID",&col);
TCCINI(tidcomm,D_C_FORMAT,60,"A20"," ","OST",&col);
TCCINI(tidcomm,D_C_FORMAT,60,"A20"," ","DESCR",&col);
TCCINI(tidcomm,D_I4_FORMAT,1,"I6"," ","OPFLAG",&col);
pid = oshpid();
TCEWRI(tidcomm,1,1,&pid);
strcpy(cval,"?");
TCEWRC(tidcomm,1,2,cval);
val = 1;
TCEWRI(tidcomm,1,4,&val);
TCTCLO(tidcomm);
return 0;
}

int populate(wlist)
Widget wlist;
{
XmStringTable str_list;
int i, item_no;
 
item_no = 7;
str_list = (XmStringTable)XtMalloc(item_no * sizeof(XmString));
for (i=0; i<item_no; i++) {
   str_list[i] = XmStringCreateSimple(mode[i]);
   }
XmListSetPos(wlist,1);
XmListDeleteAllItems(wlist);
XmListAddItems(wlist, str_list, item_no, 1);

for ( i = 0; i <item_no; i++ )
        XmStringFree(str_list[i]);
    XtFree((char *)str_list);

return 0;
}

int selectlist(mymode,wlist)
char *mymode;
Widget wlist;
{
int item_no,i;
XmString str;
XtVaSetValues(wlist,XmNselectionPolicy,XmMULTIPLE_SELECT,NULL);
XmListDeselectAllItems(wlist);
if (stucomp(mymode,"COPY") == 0) {
  for (i=0; i<colno; i++) {
       str= XmStringCreateSimple(colchoice+i*(TBL_LABLEN+1));
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
       }
XtVaSetValues(wlist,XmNselectionPolicy,XmEXTENDED_SELECT,NULL);
return(0);
}
item_no = 6;
for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(all[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
}

if (stucomp(mymode,"GRISM SPECTROSCOPY") == 0) {
   item_no = 2;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(gr[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
else if (stucomp(mymode,"GRATING SPECTROSCOPY (RED)") == 0) {
   item_no = 3;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(gtr[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
else if (stucomp(mymode,"GRATING SPECTROSCOPY (BLUE)") == 0) {
   item_no = 3;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(gtb[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
else if (stucomp(mymode,"ECHELLE SPECTROSCOPY") == 0) {
   item_no = 4;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(ech[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
else if (stucomp(mymode,"IMAGING (RED)") == 0) {
   item_no = 1;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(imar[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
else if (stucomp(mymode,"IMAGING (BLUE)") == 0) {
   item_no = 1;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(imab[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
else if (stucomp(mymode,"IMAGING (SUSI)") == 0) {
   item_no = 1;
   for (i=0; i<item_no; i++) {
       str= XmStringCreateSimple(imab[i]);
       XmListSelectItem(wlist,str,False);
       XmStringFree(str);
     }
}
XtVaSetValues(wlist,XmNselectionPolicy,XmEXTENDED_SELECT,NULL);
return 0;
}


int getselectlist(wlist)
Widget wlist;
{
XmString *str_list;
int pos_cnt,i;
char *choice;
colno = 0;
XtVaGetValues(wlist,XmNselectedItemCount,&pos_cnt,
                    XmNselectedItems,&str_list,
                    NULL);
for (i=0; i<pos_cnt; i++ ) {
     XmStringGetLtoR(str_list[i],XmSTRING_DEFAULT_CHARSET,&choice);   
     strcpy(colchoice+(TBL_LABLEN+1)*colno,choice);
     XtFree(choice);
     colno++;
     } 
return 0;
}

void display_help(wtext,file)
swidget wtext;
char file[20];

{
extern swidget help;
int fid,nobytes,mm;
char *pbuf,cfile[160];
 
OSY_TRNLOG("XDO_HELP",cfile,120,&mm);
cfile[mm] = '/';
strcpy(&cfile[mm+1],file);
fid = osaopen(cfile,F_I_MODE);
nobytes = 100000;
pbuf = (char *)osmmget(nobytes);
osdread(fid,pbuf,nobytes);
XmTextSetString(UxGetWidget(UxFindSwidget("scrolledText1")),pbuf); 
osaclose(fid);
 UxPopupInterface(help,no_grab);
osmmfree(pbuf);
} 

void checkdigit(text_w,unused,cbs)
Widget text_w;
XtPointer unused;
XmTextVerifyCallbackStruct *cbs;
{
int len;
if (cbs->text->ptr == NULL)
      return;
for (len=0; len<cbs->text->length; len++) {
  if (isalpha(cbs->text->ptr[len])) {
        int  i;
         for (i=len; (i+1) <cbs->text->length; i++)
            cbs->text->ptr[i] = cbs->text->ptr[i+1];
         cbs->text->length--;
         len--;
        }
  } 
}
void checkchar(text_w,unused,cbs)
Widget text_w;
XtPointer unused;
XmTextVerifyCallbackStruct *cbs;
{
int len;
if (cbs->text->ptr == NULL)
      return;
for (len=0; len<cbs->text->length; len++) {
  if (cbs->text->ptr[len] == '<' || cbs->text->ptr[len] == '>' ) {
        int  i;
         for (i=len; (i+1) <cbs->text->length; i++)
            cbs->text->ptr[i] = cbs->text->ptr[i+1];
         cbs->text->length--;
         len--;
        }
}
}
