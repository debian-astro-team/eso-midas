
/*******************************************************************************
	SelectDescrTable.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxFsBox.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSelectDescrTable;
} _UxCSelectDescrTable;

#define SelectDescrTable        UxSelectDescrTableContext->UxSelectDescrTable

static _UxCSelectDescrTable	*UxSelectDescrTableContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SelectDescrTable();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	okCallback_SelectDescrTable( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectDescrTable    *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectDescrTableContext;
	UxSelectDescrTableContext = UxContext =
			(_UxCSelectDescrTable *) UxGetContext( UxThisWidget );
	{
	XmFileSelectionBoxCallbackStruct *cbs;
	char *value;
	
	cbs = (XmFileSelectionBoxCallbackStruct *)UxCallbackArg;
	 XmStringGetLtoR(cbs->value,XmSTRING_DEFAULT_CHARSET,&value);
	UxPutText(UxFindSwidget("ost_t3"),value);
	XmTextShowPosition(UxGetWidget(UxFindSwidget("ost_t3")),strlen(value));
	}
	UxSelectDescrTableContext = UxSaveCtx;
}

static void	cancelCB_SelectDescrTable( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSelectDescrTable    *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSelectDescrTableContext;
	UxSelectDescrTableContext = UxContext =
			(_UxCSelectDescrTable *) UxGetContext( UxThisWidget );
	{
	extern swidget dlist;
	UxPopdownInterface(dlist);
	}
	UxSelectDescrTableContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SelectDescrTable()
{
	UxPutDialogTitle( SelectDescrTable, "" );
	UxPutTextFontList( SelectDescrTable, BoldTextFont );
	UxPutLabelFontList( SelectDescrTable, BoldTextFont );
	UxPutButtonFontList( SelectDescrTable, BoldTextFont );
	UxPutBackground( SelectDescrTable, WindowBackground );
	UxPutHeight( SelectDescrTable, 400 );
	UxPutWidth( SelectDescrTable, 350 );
	UxPutY( SelectDescrTable, 330 );
	UxPutX( SelectDescrTable, 770 );
	UxPutUnitType( SelectDescrTable, "pixels" );
	UxPutResizePolicy( SelectDescrTable, "resize_none" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SelectDescrTable()
{
	/* Create the swidgets */

	SelectDescrTable = UxCreateFileSelectionBox( "SelectDescrTable", NO_PARENT );
	UxPutContext( SelectDescrTable, UxSelectDescrTableContext );
	UxPutDefaultShell( SelectDescrTable, "topLevelShell" );


	_Uxinit_SelectDescrTable();

	/* Create the X widgets */

	UxCreateWidget( SelectDescrTable );

	UxAddCallback( SelectDescrTable, XmNokCallback,
			okCallback_SelectDescrTable,
			(XtPointer) UxSelectDescrTableContext );
	UxAddCallback( SelectDescrTable, XmNcancelCallback,
			cancelCB_SelectDescrTable,
			(XtPointer) UxSelectDescrTableContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SelectDescrTable );

	return ( SelectDescrTable );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SelectDescrTable()
{
	swidget                 rtrn;
	_UxCSelectDescrTable    *UxContext;

	UxSelectDescrTableContext = UxContext =
		(_UxCSelectDescrTable *) UxMalloc( sizeof(_UxCSelectDescrTable) );

	rtrn = _Uxbuild_SelectDescrTable();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SelectDescrTable()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_SelectDescrTable();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

