
/*******************************************************************************
	HelpDo.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxHelpDo;
	swidget	Uxform7;
	swidget	UxscrolledWindow9;
	swidget	UxscrolledText1;
	swidget	UxCloseHelp;
} _UxCHelpDo;

#define HelpDo                  UxHelpDoContext->UxHelpDo
#define form7                   UxHelpDoContext->Uxform7
#define scrolledWindow9         UxHelpDoContext->UxscrolledWindow9
#define scrolledText1           UxHelpDoContext->UxscrolledText1
#define CloseHelp               UxHelpDoContext->UxCloseHelp

static _UxCHelpDo	*UxHelpDoContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_HelpDo();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_CloseHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpDo              *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpDoContext;
	UxHelpDoContext = UxContext =
			(_UxCHelpDo *) UxGetContext( UxThisWidget );
	{
	extern swidget help;
	UxPopdownInterface(help);
	}
	UxHelpDoContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_HelpDo()
{
	UxPutIconName( HelpDo, "Helpwindow" );
	UxPutBackground( HelpDo, WindowBackground );
	UxPutHeight( HelpDo, 260 );
	UxPutWidth( HelpDo, 540 );
	UxPutY( HelpDo, 10 );
	UxPutX( HelpDo, 630 );

	UxPutBackground( form7, WindowBackground );
	UxPutHeight( form7, 240 );
	UxPutWidth( form7, 520 );
	UxPutY( form7, 10 );
	UxPutX( form7, 0 );
	UxPutUnitType( form7, "pixels" );
	UxPutResizePolicy( form7, "resize_none" );

	UxPutForeground( scrolledWindow9, TextForeground );
	UxPutBackground( scrolledWindow9, ApplicBackground );
	UxPutShadowThickness( scrolledWindow9, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow9, "static" );
	UxPutVisualPolicy( scrolledWindow9, "variable" );
	UxPutY( scrolledWindow9, 10 );
	UxPutX( scrolledWindow9, 0 );
	UxPutScrollingPolicy( scrolledWindow9, "application_defined" );

	UxPutScrollHorizontal( scrolledText1, "false" );
	UxPutForeground( scrolledText1, TextForeground );
	UxPutFontList( scrolledText1, TextFont );
	UxPutEditable( scrolledText1, "false" );
	UxPutEditMode( scrolledText1, "multi_line_edit" );
	UxPutBackground( scrolledText1, TextBackground );
	UxPutHeight( scrolledText1, 200 );
	UxPutWidth( scrolledText1, 520 );

	UxPutLabelString( CloseHelp, "Close" );
	UxPutForeground( CloseHelp, CancelForeground );
	UxPutFontList( CloseHelp, BoldTextFont );
	UxPutBackground( CloseHelp, ButtonBackground );
	UxPutHeight( CloseHelp, 30 );
	UxPutWidth( CloseHelp, 100 );
	UxPutY( CloseHelp, 230 );
	UxPutX( CloseHelp, 190 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_HelpDo()
{
	/* Create the swidgets */

	HelpDo = UxCreateTopLevelShell( "HelpDo", NO_PARENT );
	UxPutContext( HelpDo, UxHelpDoContext );

	form7 = UxCreateForm( "form7", HelpDo );
	scrolledWindow9 = UxCreateScrolledWindow( "scrolledWindow9", form7 );
	scrolledText1 = UxCreateScrolledText( "scrolledText1", scrolledWindow9 );
	CloseHelp = UxCreatePushButton( "CloseHelp", form7 );

	_Uxinit_HelpDo();

	/* Create the X widgets */

	UxCreateWidget( HelpDo );
	UxCreateWidget( form7 );
	UxPutLeftOffset( scrolledWindow9, 0 );
	UxPutLeftAttachment( scrolledWindow9, "attach_form" );
	UxPutRightOffset( scrolledWindow9, 0 );
	UxPutRightAttachment( scrolledWindow9, "attach_form" );
	UxPutTopOffset( scrolledWindow9, 0 );
	UxPutTopAttachment( scrolledWindow9, "attach_form" );
	UxCreateWidget( scrolledWindow9 );

	UxCreateWidget( scrolledText1 );
	UxPutRightOffset( CloseHelp, 215 );
	UxPutRightAttachment( CloseHelp, "attach_form" );
	UxPutLeftOffset( CloseHelp, 215 );
	UxPutLeftAttachment( CloseHelp, "attach_form" );
	UxPutBottomOffset( CloseHelp, 5 );
	UxPutBottomAttachment( CloseHelp, "attach_form" );
	UxCreateWidget( CloseHelp );


	UxAddCallback( CloseHelp, XmNactivateCallback,
			activateCB_CloseHelp,
			(XtPointer) UxHelpDoContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( HelpDo );

	return ( HelpDo );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_HelpDo()
{
	swidget                 rtrn;
	_UxCHelpDo              *UxContext;

	UxHelpDoContext = UxContext =
		(_UxCHelpDo *) UxMalloc( sizeof(_UxCHelpDo) );

	rtrn = _Uxbuild_HelpDo();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_HelpDo()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_HelpDo();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

