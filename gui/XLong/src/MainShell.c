/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	MainShell.c


.VERSION
 090819         last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "UxLib.h"
#include "UxLabel.h"
#include "UxTextF.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxSepG.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

#include "midas_def.h"

#include <xm_defs.h>
#include <spec_comm.h>


/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxApplicWindow;
	swidget	UxMainWindow;
	swidget	Uxmenu1;
	swidget	Uxmenu1_p1;
	swidget	Uxmenu1_p1_b1;
	swidget	Uxmenu1_p1_b2;
	swidget	Uxmenu1_p1_b3;
	swidget	Uxmenu1_p1_b4;
	swidget	Uxmenu1_top_b1;
	swidget	Uxmenu1_p4;
	swidget	Uxmenu1_p4_b1;
	swidget	Uxmenu1_p4_b2;
	swidget	Uxmenu1_p4_b8;
	swidget	Uxmenu1_p4_b10;
	swidget	Uxmenu1_p4_b13;
	swidget	Uxmenu1_p4_b12;
	swidget	Uxmenu1_p4_b3;
	swidget	Uxmenu1_p4_b4;
	swidget	Uxmenu1_p4_b9;
	swidget	Uxmenu1_p4_b5;
	swidget	Uxmenu1_p4_b11;
	swidget	Uxmenu1_p4_b14;
	swidget	Uxmenu1_p4_b15;
	swidget	Uxmenu1_top_b4;
	swidget	Uxmenu1_p5;
	swidget	Uxmenu_help_context;
	swidget	Uxmenu_help_help;
	swidget	Uxmenu1_p5_b5;
	swidget	Uxmenu_help_tutorial;
	swidget	Uxmenu_help_version;
	swidget	Uxmenu1_top_b5;
	swidget	Uxform1;
	swidget	Uxform5;
	swidget	Uxpb_main_search;
	swidget	Uxpb_main_ident;
	swidget	Uxpb_main_calib;
	swidget	Uxpb_main_rebin;
	swidget	Uxpb_main_extract;
	swidget	Uxpb_main_flux;
	swidget	Uxpb_main_batch;
	swidget	Uxseparator12;
	swidget	Uxshelp_main;
	swidget	Uxseparator1;
	swidget	Uxtf_imin;
	swidget	Uxlabel6;
	swidget	Uxtf_wrang1;
	swidget	Uxlabel5;
	swidget	Uxtf_wrang2;
	swidget	Uxtf_lincat;
	swidget	Uxlabel4;
	swidget	Uxtf_ystart;
	swidget	Uxlabel3;
	swidget	Uxtf_instrume;
	swidget	Uxlabel8;
	swidget	Uxlabel1;
	swidget	Uxtf_wlc;
	swidget	Uxtf_session;
	swidget	Uxlabel2;
	swidget	Uxlabel7;
} _UxCApplicWindow;

#define ApplicWindow            UxApplicWindowContext->UxApplicWindow
#define MainWindow              UxApplicWindowContext->UxMainWindow
#define menu1                   UxApplicWindowContext->Uxmenu1
#define menu1_p1                UxApplicWindowContext->Uxmenu1_p1
#define menu1_p1_b1             UxApplicWindowContext->Uxmenu1_p1_b1
#define menu1_p1_b2             UxApplicWindowContext->Uxmenu1_p1_b2
#define menu1_p1_b3             UxApplicWindowContext->Uxmenu1_p1_b3
#define menu1_p1_b4             UxApplicWindowContext->Uxmenu1_p1_b4
#define menu1_top_b1            UxApplicWindowContext->Uxmenu1_top_b1
#define menu1_p4                UxApplicWindowContext->Uxmenu1_p4
#define menu1_p4_b1             UxApplicWindowContext->Uxmenu1_p4_b1
#define menu1_p4_b2             UxApplicWindowContext->Uxmenu1_p4_b2
#define menu1_p4_b8             UxApplicWindowContext->Uxmenu1_p4_b8
#define menu1_p4_b10            UxApplicWindowContext->Uxmenu1_p4_b10
#define menu1_p4_b13            UxApplicWindowContext->Uxmenu1_p4_b13
#define menu1_p4_b12            UxApplicWindowContext->Uxmenu1_p4_b12
#define menu1_p4_b3             UxApplicWindowContext->Uxmenu1_p4_b3
#define menu1_p4_b4             UxApplicWindowContext->Uxmenu1_p4_b4
#define menu1_p4_b9             UxApplicWindowContext->Uxmenu1_p4_b9
#define menu1_p4_b5             UxApplicWindowContext->Uxmenu1_p4_b5
#define menu1_p4_b11            UxApplicWindowContext->Uxmenu1_p4_b11
#define menu1_p4_b14            UxApplicWindowContext->Uxmenu1_p4_b14
#define menu1_p4_b15            UxApplicWindowContext->Uxmenu1_p4_b15
#define menu1_top_b4            UxApplicWindowContext->Uxmenu1_top_b4
#define menu1_p5                UxApplicWindowContext->Uxmenu1_p5
#define menu_help_context       UxApplicWindowContext->Uxmenu_help_context
#define menu_help_help          UxApplicWindowContext->Uxmenu_help_help
#define menu1_p5_b5             UxApplicWindowContext->Uxmenu1_p5_b5
#define menu_help_tutorial      UxApplicWindowContext->Uxmenu_help_tutorial
#define menu_help_version       UxApplicWindowContext->Uxmenu_help_version
#define menu1_top_b5            UxApplicWindowContext->Uxmenu1_top_b5
#define form1                   UxApplicWindowContext->Uxform1
#define form5                   UxApplicWindowContext->Uxform5
#define pb_main_search          UxApplicWindowContext->Uxpb_main_search
#define pb_main_ident           UxApplicWindowContext->Uxpb_main_ident
#define pb_main_calib           UxApplicWindowContext->Uxpb_main_calib
#define pb_main_rebin           UxApplicWindowContext->Uxpb_main_rebin
#define pb_main_extract         UxApplicWindowContext->Uxpb_main_extract
#define pb_main_flux            UxApplicWindowContext->Uxpb_main_flux
#define pb_main_batch           UxApplicWindowContext->Uxpb_main_batch
#define separator12             UxApplicWindowContext->Uxseparator12
#define shelp_main              UxApplicWindowContext->Uxshelp_main
#define separator1              UxApplicWindowContext->Uxseparator1
#define tf_imin                 UxApplicWindowContext->Uxtf_imin
#define label6                  UxApplicWindowContext->Uxlabel6
#define tf_wrang1               UxApplicWindowContext->Uxtf_wrang1
#define label5                  UxApplicWindowContext->Uxlabel5
#define tf_wrang2               UxApplicWindowContext->Uxtf_wrang2
#define tf_lincat               UxApplicWindowContext->Uxtf_lincat
#define label4                  UxApplicWindowContext->Uxlabel4
#define tf_ystart               UxApplicWindowContext->Uxtf_ystart
#define label3                  UxApplicWindowContext->Uxlabel3
#define tf_instrume             UxApplicWindowContext->Uxtf_instrume
#define label8                  UxApplicWindowContext->Uxlabel8
#define label1                  UxApplicWindowContext->Uxlabel1
#define tf_wlc                  UxApplicWindowContext->Uxtf_wlc
#define tf_session              UxApplicWindowContext->Uxtf_session
#define label2                  UxApplicWindowContext->Uxlabel2
#define label7                  UxApplicWindowContext->Uxlabel7

static _UxCApplicWindow	*UxApplicWindowContext;

extern void DisplayExtendedHelp(), DisplayShortHelp(), PopupLong();
extern void SetFileList(), WriteKeyword();

extern int file_exists(), PopupList(), AppendDialogText();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SelectFileMain = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ApplicWindow();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("shelp_main"), "");
	UxPutText(UxFindSwidget("shelp_search"), "");
	UxPutText(UxFindSwidget("shelp_calib"), "");
	UxPutText(UxFindSwidget("shelp_rebin"), "");
	UxPutText(UxFindSwidget("shelp_extract"), "");
	UxPutText(UxFindSwidget("shelp_flux"), "");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_menu1_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	PopupList(LIST_SESSION);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern char Session[];
	char command[256];
	
	sprintf(command, "%s%s", C_SAVE, Session);
	AppendDialogText(command);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern int DialogType;
	extern char Session[];
	
	SET_DIALOG_PROMPT("Output parameters table :");
	
	XmTextSetString(UxGetWidget(UxFindSwidget("tf_file_dialog")), Session);
	DialogType = DIALOG_SESSION;
	UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_CLEAN);
	SCSEPI();
	exit(0);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("make/display");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("graph/long");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{        
#include <spec_comm.h>
	
	PopupList(LIST_LOAD_IMA);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("clear/channel over");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/display laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/graph laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("get/gcursor");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("get/cursor");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char midashome[256];
        extern char  DirSpecs[];

	
	osfphname("MIDASHOME", midashome);
	sprintf(DirSpecs, "%s/calib/data/", midashome);
	
	PopupList(LIST_BROWSER);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_context( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_help( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_tutorial( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_version( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("SearchShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_ident( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
#include <spec_comm.h>
	
	extern char Lincat[], Wlc[];
	
	if ( !file_exists(Wlc, ".bdf") ) {
	    SCTPUT("*** Error: invalid calibration frame ***");
	    return;
	}
	if ( !file_exists(Lincat, ".tbl") ) {
	    SCTPUT("*** Error: invalid line catalog table ***");
	    return;
	}
	
	AppendDialogText(C_IDENT);
	}
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_calib( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
#include <spec_comm.h>
	
	AppendDialogText(C_CALIB_ERAS);
	PopupLong("CalibShell");
	}
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_rebin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("RebinShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_extract( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("ExtractShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_flux( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("FluxShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_batch( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
#include <spec_comm.h>
	
	AppendDialogText(C_BATCHRED);
	}
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_imin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Imin;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Imin ) {
	    Imin = val;
	    WriteKeyword(text, K_IMIN);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wrang1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Wrang[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Wrang[0] ) {
	    Wrang[0] = val;
	    WriteKeyword(text, K_WRANG1);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wrang2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Wrang[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Wrang[1] ) {
	    Wrang[1] = val;
	    WriteKeyword(text, K_WRANG2);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_lincat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Lincat[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Lincat) ) {
	    strcpy(Lincat, text);
	    WriteKeyword(text, K_LINCAT);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ystart( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Ystart;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Ystart ) {
	    Ystart = val;
	    WriteKeyword(text, K_YSTART);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutGeometry( ApplicWindow, "+0+0" );
	UxPutTitle( ApplicWindow, "Long slit reduction" );
	UxPutKeyboardFocusPolicy( ApplicWindow, "pointer" );
	UxPutIconName( ApplicWindow, "Long Slit" );
	UxPutHeight( ApplicWindow, 460 );
	UxPutWidth( ApplicWindow, 447 );
	UxPutY( ApplicWindow, 21 );
	UxPutX( ApplicWindow, 2 );

	UxPutBackground( MainWindow, WindowBackground );
	UxPutHeight( MainWindow, 460 );
	UxPutWidth( MainWindow, 440 );
	UxPutY( MainWindow, 0 );
	UxPutX( MainWindow, 10 );
	UxPutUnitType( MainWindow, "pixels" );

	UxPutBackground( menu1, MenuBackground );
	UxPutMenuAccelerator( menu1, "<KeyUp>F10" );
	UxPutRowColumnType( menu1, "menu_bar" );

	UxPutForeground( menu1_p1, MenuForeground );
	UxPutBackground( menu1_p1, MenuBackground );
	UxPutRowColumnType( menu1_p1, "menu_pulldown" );

	UxPutFontList( menu1_p1_b1, BoldTextFont );
	UxPutMnemonic( menu1_p1_b1, "O" );
	UxPutLabelString( menu1_p1_b1, "Open" );

	UxPutFontList( menu1_p1_b2, BoldTextFont );
	UxPutMnemonic( menu1_p1_b2, "S" );
	UxPutLabelString( menu1_p1_b2, "Save" );

	UxPutFontList( menu1_p1_b3, BoldTextFont );
	UxPutMnemonic( menu1_p1_b3, "S" );
	UxPutLabelString( menu1_p1_b3, "Save As ..." );

	UxPutFontList( menu1_p1_b4, BoldTextFont );
	UxPutAccelerator( menu1_p1_b4, "E" );
	UxPutLabelString( menu1_p1_b4, "Exit" );

	UxPutForeground( menu1_top_b1, MenuForeground );
	UxPutFontList( menu1_top_b1, BoldTextFont );
	UxPutBackground( menu1_top_b1, MenuBackground );
	UxPutMnemonic( menu1_top_b1, "F" );
	UxPutLabelString( menu1_top_b1, "File" );

	UxPutForeground( menu1_p4, MenuForeground );
	UxPutBackground( menu1_p4, MenuBackground );
	UxPutRowColumnType( menu1_p4, "menu_pulldown" );

	UxPutFontList( menu1_p4_b1, BoldTextFont );
	UxPutMnemonic( menu1_p4_b1, "D" );
	UxPutLabelString( menu1_p4_b1, "Create Display" );

	UxPutFontList( menu1_p4_b2, BoldTextFont );
	UxPutMnemonic( menu1_p4_b2, "G" );
	UxPutLabelString( menu1_p4_b2, "Create Graphics" );

	UxPutMnemonic( menu1_p4_b10, "L" );
	UxPutFontList( menu1_p4_b10, BoldTextFont );
	UxPutLabelString( menu1_p4_b10, "Load image" );

	UxPutMnemonic( menu1_p4_b13, "o" );
	UxPutFontList( menu1_p4_b13, BoldTextFont );
	UxPutLabelString( menu1_p4_b13, "Clear overlay" );

	UxPutFontList( menu1_p4_b3, BoldTextFont );
	UxPutMnemonic( menu1_p4_b3, "P" );
	UxPutLabelString( menu1_p4_b3, "Print Display" );

	UxPutFontList( menu1_p4_b4, BoldTextFont );
	UxPutMnemonic( menu1_p4_b4, "r" );
	UxPutLabelString( menu1_p4_b4, "Print Graphics" );

	UxPutFontList( menu1_p4_b5, BoldTextFont );
	UxPutMnemonic( menu1_p4_b5, "C" );
	UxPutLabelString( menu1_p4_b5, "Graphics Cursor" );

	UxPutFontList( menu1_p4_b11, BoldTextFont );
	UxPutMnemonic( menu1_p4_b11, "u" );
	UxPutLabelString( menu1_p4_b11, "Display Cursor" );

	UxPutFontList( menu1_p4_b15, BoldTextFont );
	UxPutLabelString( menu1_p4_b15, "get MIDAS table" );

	UxPutForeground( menu1_top_b4, MenuForeground );
	UxPutFontList( menu1_top_b4, BoldTextFont );
	UxPutBackground( menu1_top_b4, MenuBackground );
	UxPutMnemonic( menu1_top_b4, "U" );
	UxPutLabelString( menu1_top_b4, "Utils" );

	UxPutForeground( menu1_p5, MenuForeground );
	UxPutBackground( menu1_p5, MenuBackground );
	UxPutRowColumnType( menu1_p5, "menu_pulldown" );

	UxPutFontList( menu_help_context, BoldTextFont );
	UxPutMnemonic( menu_help_context, "C" );
	UxPutLabelString( menu_help_context, "On Context ..." );

	UxPutFontList( menu_help_help, BoldTextFont );
	UxPutMnemonic( menu_help_help, "H" );
	UxPutLabelString( menu_help_help, "On Help ..." );

	UxPutFontList( menu_help_tutorial, BoldTextFont );
	UxPutMnemonic( menu_help_tutorial, "T" );
	UxPutLabelString( menu_help_tutorial, "Tutorial" );

	UxPutFontList( menu_help_version, BoldTextFont );
	UxPutMnemonic( menu_help_version, "V" );
	UxPutLabelString( menu_help_version, "On Version..." );

	UxPutForeground( menu1_top_b5, MenuForeground );
	UxPutFontList( menu1_top_b5, BoldTextFont );
	UxPutBackground( menu1_top_b5, MenuBackground );
	UxPutMnemonic( menu1_top_b5, "H" );
	UxPutLabelString( menu1_top_b5, "Help" );

	UxPutBackground( form1, WindowBackground );

	UxPutBackground( form5, ButtonBackground );
	UxPutHeight( form5, 72 );
	UxPutWidth( form5, 451 );
	UxPutY( form5, 368 );
	UxPutX( form5, -1 );
	UxPutResizePolicy( form5, "resize_none" );

	UxPutLabelString( pb_main_search, "Search ..." );
	UxPutForeground( pb_main_search, ButtonForeground );
	UxPutFontList( pb_main_search, BoldTextFont );
	UxPutBackground( pb_main_search, ButtonBackground );
	UxPutHeight( pb_main_search, 30 );
	UxPutWidth( pb_main_search, 86 );
	UxPutY( pb_main_search, 6 );
	UxPutX( pb_main_search, 4 );

	UxPutLabelString( pb_main_ident, "Identify ..." );
	UxPutForeground( pb_main_ident, ButtonForeground );
	UxPutFontList( pb_main_ident, BoldTextFont );
	UxPutBackground( pb_main_ident, ButtonBackground );
	UxPutHeight( pb_main_ident, 30 );
	UxPutWidth( pb_main_ident, 86 );
	UxPutY( pb_main_ident, 6 );
	UxPutX( pb_main_ident, 94 );

	UxPutLabelString( pb_main_calib, "Calibrate ..." );
	UxPutForeground( pb_main_calib, ButtonForeground );
	UxPutFontList( pb_main_calib, BoldTextFont );
	UxPutBackground( pb_main_calib, ButtonBackground );
	UxPutHeight( pb_main_calib, 30 );
	UxPutWidth( pb_main_calib, 86 );
	UxPutY( pb_main_calib, 6 );
	UxPutX( pb_main_calib, 182 );

	UxPutLabelString( pb_main_rebin, "Rebin ..." );
	UxPutForeground( pb_main_rebin, ButtonForeground );
	UxPutFontList( pb_main_rebin, BoldTextFont );
	UxPutBackground( pb_main_rebin, ButtonBackground );
	UxPutHeight( pb_main_rebin, 30 );
	UxPutWidth( pb_main_rebin, 86 );
	UxPutY( pb_main_rebin, 6 );
	UxPutX( pb_main_rebin, 270 );

	UxPutLabelString( pb_main_extract, "Extract ..." );
	UxPutForeground( pb_main_extract, ButtonForeground );
	UxPutFontList( pb_main_extract, BoldTextFont );
	UxPutBackground( pb_main_extract, ButtonBackground );
	UxPutHeight( pb_main_extract, 30 );
	UxPutWidth( pb_main_extract, 86 );
	UxPutY( pb_main_extract, 6 );
	UxPutX( pb_main_extract, 358 );

	UxPutLabelString( pb_main_flux, "Flux ..." );
	UxPutForeground( pb_main_flux, ButtonForeground );
	UxPutFontList( pb_main_flux, BoldTextFont );
	UxPutBackground( pb_main_flux, ButtonBackground );
	UxPutHeight( pb_main_flux, 30 );
	UxPutWidth( pb_main_flux, 86 );
	UxPutY( pb_main_flux, 38 );
	UxPutX( pb_main_flux, 4 );

	UxPutLabelString( pb_main_batch, "Batch ..." );
	UxPutForeground( pb_main_batch, ButtonForeground );
	UxPutFontList( pb_main_batch, BoldTextFont );
	UxPutBackground( pb_main_batch, ButtonBackground );
	UxPutHeight( pb_main_batch, 30 );
	UxPutWidth( pb_main_batch, 86 );
	UxPutY( pb_main_batch, 38 );
	UxPutX( pb_main_batch, 94 );

	UxPutBackground( separator12, LabelBackground );
	UxPutHeight( separator12, 6 );
	UxPutWidth( separator12, 450 );
	UxPutY( separator12, 360 );
	UxPutX( separator12, 0 );

	UxPutFontList( shelp_main, TextFont );
	UxPutEditable( shelp_main, "false" );
	UxPutCursorPositionVisible( shelp_main, "false" );
	UxPutBackground( shelp_main, SHelpBackground );
	UxPutHeight( shelp_main, 50 );
	UxPutWidth( shelp_main, 448 );
	UxPutY( shelp_main, 310 );
	UxPutX( shelp_main, 0 );

	UxPutBackground( separator1, LabelBackground );
	UxPutHeight( separator1, 6 );
	UxPutWidth( separator1, 450 );
	UxPutY( separator1, 304 );
	UxPutX( separator1, -2 );

	UxPutForeground( tf_imin, TextForeground );
	UxPutHighlightOnEnter( tf_imin, "true" );
	UxPutFontList( tf_imin, TextFont );
	UxPutBackground( tf_imin, TextBackground );
	UxPutHeight( tf_imin, 37 );
	UxPutWidth( tf_imin, 68 );
	UxPutY( tf_imin, 268 );
	UxPutX( tf_imin, 204 );

	UxPutForeground( label6, TextForeground );
	UxPutAlignment( label6, "alignment_beginning" );
	UxPutLabelString( label6, "Minimal Intensity in Catalog :" );
	UxPutFontList( label6, TextFont );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 30 );
	UxPutWidth( label6, 190 );
	UxPutY( label6, 270 );
	UxPutX( label6, 12 );

	UxPutForeground( tf_wrang1, TextForeground );
	UxPutHighlightOnEnter( tf_wrang1, "true" );
	UxPutFontList( tf_wrang1, TextFont );
	UxPutBackground( tf_wrang1, TextBackground );
	UxPutHeight( tf_wrang1, 37 );
	UxPutWidth( tf_wrang1, 68 );
	UxPutY( tf_wrang1, 230 );
	UxPutX( tf_wrang1, 204 );

	UxPutForeground( label5, TextForeground );
	UxPutAlignment( label5, "alignment_beginning" );
	UxPutLabelString( label5, "Wavelength Range :" );
	UxPutFontList( label5, TextFont );
	UxPutBackground( label5, LabelBackground );
	UxPutHeight( label5, 30 );
	UxPutWidth( label5, 190 );
	UxPutY( label5, 232 );
	UxPutX( label5, 12 );

	UxPutForeground( tf_wrang2, TextForeground );
	UxPutHighlightOnEnter( tf_wrang2, "true" );
	UxPutFontList( tf_wrang2, TextFont );
	UxPutBackground( tf_wrang2, TextBackground );
	UxPutHeight( tf_wrang2, 37 );
	UxPutWidth( tf_wrang2, 68 );
	UxPutY( tf_wrang2, 230 );
	UxPutX( tf_wrang2, 272 );

	UxPutTranslations( tf_lincat, SelectFileMain );
	UxPutForeground( tf_lincat, TextForeground );
	UxPutHighlightOnEnter( tf_lincat, "true" );
	UxPutFontList( tf_lincat, TextFont );
	UxPutBackground( tf_lincat, TextBackground );
	UxPutHeight( tf_lincat, 37 );
	UxPutWidth( tf_lincat, 236 );
	UxPutY( tf_lincat, 192 );
	UxPutX( tf_lincat, 204 );

	UxPutForeground( label4, TextForeground );
	UxPutAlignment( label4, "alignment_beginning" );
	UxPutLabelString( label4, "Line Catalog :" );
	UxPutFontList( label4, TextFont );
	UxPutBackground( label4, LabelBackground );
	UxPutHeight( label4, 30 );
	UxPutWidth( label4, 190 );
	UxPutY( label4, 196 );
	UxPutX( label4, 12 );

	UxPutForeground( tf_ystart, TextForeground );
	UxPutHighlightOnEnter( tf_ystart, "true" );
	UxPutFontList( tf_ystart, TextFont );
	UxPutBackground( tf_ystart, TextBackground );
	UxPutHeight( tf_ystart, 37 );
	UxPutWidth( tf_ystart, 71 );
	UxPutY( tf_ystart, 156 );
	UxPutX( tf_ystart, 204 );

	UxPutForeground( label3, TextForeground );
	UxPutAlignment( label3, "alignment_beginning" );
	UxPutLabelString( label3, "Calibration Starting Row :" );
	UxPutFontList( label3, TextFont );
	UxPutBackground( label3, LabelBackground );
	UxPutHeight( label3, 30 );
	UxPutWidth( label3, 190 );
	UxPutY( label3, 158 );
	UxPutX( label3, 12 );

	UxPutBorderWidth( tf_instrume, 0 );
	UxPutForeground( tf_instrume, TextForeground );
	UxPutHighlightOnEnter( tf_instrume, "true" );
	UxPutFontList( tf_instrume, TextFont );
	UxPutBackground( tf_instrume, TextBackground );
	UxPutHeight( tf_instrume, 37 );
	UxPutWidth( tf_instrume, 236 );
	UxPutY( tf_instrume, 118 );
	UxPutX( tf_instrume, 204 );

	UxPutForeground( label8, TextForeground );
	UxPutAlignment( label8, "alignment_beginning" );
	UxPutLabelString( label8, "Instrument :" );
	UxPutFontList( label8, TextFont );
	UxPutBackground( label8, LabelBackground );
	UxPutHeight( label8, 30 );
	UxPutWidth( label8, 190 );
	UxPutY( label8, 122 );
	UxPutX( label8, 12 );

	UxPutForeground( label1, TextForeground );
	UxPutAlignment( label1, "alignment_beginning" );
	UxPutLabelString( label1, "Calibration Frame :" );
	UxPutFontList( label1, TextFont );
	UxPutBackground( label1, LabelBackground );
	UxPutHeight( label1, 30 );
	UxPutWidth( label1, 190 );
	UxPutY( label1, 84 );
	UxPutX( label1, 12 );

	UxPutBorderWidth( tf_wlc, 1 );
	UxPutEditable( tf_wlc, "false" );
	UxPutCursorPositionVisible( tf_wlc, "false" );
	UxPutForeground( tf_wlc, TextForeground );
	UxPutHighlightOnEnter( tf_wlc, "false" );
	UxPutFontList( tf_wlc, TextFont );
	UxPutBackground( tf_wlc, TextBackground );
	UxPutHeight( tf_wlc, 37 );
	UxPutWidth( tf_wlc, 235 );
	UxPutY( tf_wlc, 80 );
	UxPutX( tf_wlc, 204 );

	UxPutBorderWidth( tf_session, 1 );
	UxPutEditable( tf_session, "false" );
	UxPutCursorPositionVisible( tf_session, "false" );
	UxPutForeground( tf_session, TextForeground );
	UxPutHighlightOnEnter( tf_session, "false" );
	UxPutFontList( tf_session, TextFont );
	UxPutBackground( tf_session, TextBackground );
	UxPutHeight( tf_session, 37 );
	UxPutWidth( tf_session, 235 );
	UxPutY( tf_session, 42 );
	UxPutX( tf_session, 204 );

	UxPutForeground( label2, TextForeground );
	UxPutAlignment( label2, "alignment_beginning" );
	UxPutLabelString( label2, "Session :" );
	UxPutFontList( label2, TextFont );
	UxPutBackground( label2, LabelBackground );
	UxPutHeight( label2, 30 );
	UxPutWidth( label2, 190 );
	UxPutY( label2, 44 );
	UxPutX( label2, 12 );

	UxPutForeground( label7, TextForeground );
	UxPutAlignment( label7, "alignment_beginning" );
	UxPutLabelString( label7, "GENERAL PARAMETERS" );
	UxPutFontList( label7, TextFont );
	UxPutBackground( label7, LabelBackground );
	UxPutHeight( label7, 30 );
	UxPutWidth( label7, 180 );
	UxPutY( label7, 8 );
	UxPutX( label7, 2 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	MainWindow = UxCreateMainWindow( "MainWindow", ApplicWindow );
	menu1 = UxCreateRowColumn( "menu1", MainWindow );
	menu1_p1 = UxCreateRowColumn( "menu1_p1", menu1 );
	menu1_p1_b1 = UxCreatePushButtonGadget( "menu1_p1_b1", menu1_p1 );
	menu1_p1_b2 = UxCreatePushButtonGadget( "menu1_p1_b2", menu1_p1 );
	menu1_p1_b3 = UxCreatePushButtonGadget( "menu1_p1_b3", menu1_p1 );
	menu1_p1_b4 = UxCreatePushButtonGadget( "menu1_p1_b4", menu1_p1 );
	menu1_top_b1 = UxCreateCascadeButton( "menu1_top_b1", menu1 );
	menu1_p4 = UxCreateRowColumn( "menu1_p4", menu1 );
	menu1_p4_b1 = UxCreatePushButtonGadget( "menu1_p4_b1", menu1_p4 );
	menu1_p4_b2 = UxCreatePushButtonGadget( "menu1_p4_b2", menu1_p4 );
	menu1_p4_b8 = UxCreateSeparatorGadget( "menu1_p4_b8", menu1_p4 );
	menu1_p4_b10 = UxCreatePushButtonGadget( "menu1_p4_b10", menu1_p4 );
	menu1_p4_b13 = UxCreatePushButtonGadget( "menu1_p4_b13", menu1_p4 );
	menu1_p4_b12 = UxCreateSeparatorGadget( "menu1_p4_b12", menu1_p4 );
	menu1_p4_b3 = UxCreatePushButtonGadget( "menu1_p4_b3", menu1_p4 );
	menu1_p4_b4 = UxCreatePushButtonGadget( "menu1_p4_b4", menu1_p4 );
	menu1_p4_b9 = UxCreateSeparatorGadget( "menu1_p4_b9", menu1_p4 );
	menu1_p4_b5 = UxCreatePushButtonGadget( "menu1_p4_b5", menu1_p4 );
	menu1_p4_b11 = UxCreatePushButtonGadget( "menu1_p4_b11", menu1_p4 );
	menu1_p4_b14 = UxCreateSeparatorGadget( "menu1_p4_b14", menu1_p4 );
	menu1_p4_b15 = UxCreatePushButtonGadget( "menu1_p4_b15", menu1_p4 );
	menu1_top_b4 = UxCreateCascadeButton( "menu1_top_b4", menu1 );
	menu1_p5 = UxCreateRowColumn( "menu1_p5", menu1 );
	menu_help_context = UxCreatePushButtonGadget( "menu_help_context", menu1_p5 );
	menu_help_help = UxCreatePushButtonGadget( "menu_help_help", menu1_p5 );
	menu1_p5_b5 = UxCreateSeparatorGadget( "menu1_p5_b5", menu1_p5 );
	menu_help_tutorial = UxCreatePushButtonGadget( "menu_help_tutorial", menu1_p5 );
	menu_help_version = UxCreatePushButtonGadget( "menu_help_version", menu1_p5 );
	menu1_top_b5 = UxCreateCascadeButton( "menu1_top_b5", menu1 );
	form1 = UxCreateForm( "form1", MainWindow );
	form5 = UxCreateForm( "form5", form1 );
	pb_main_search = UxCreatePushButton( "pb_main_search", form5 );
	pb_main_ident = UxCreatePushButton( "pb_main_ident", form5 );
	pb_main_calib = UxCreatePushButton( "pb_main_calib", form5 );
	pb_main_rebin = UxCreatePushButton( "pb_main_rebin", form5 );
	pb_main_extract = UxCreatePushButton( "pb_main_extract", form5 );
	pb_main_flux = UxCreatePushButton( "pb_main_flux", form5 );
	pb_main_batch = UxCreatePushButton( "pb_main_batch", form5 );
	separator12 = UxCreateSeparator( "separator12", form1 );
	shelp_main = UxCreateText( "shelp_main", form1 );
	separator1 = UxCreateSeparator( "separator1", form1 );
	tf_imin = UxCreateTextField( "tf_imin", form1 );
	label6 = UxCreateLabel( "label6", form1 );
	tf_wrang1 = UxCreateTextField( "tf_wrang1", form1 );
	label5 = UxCreateLabel( "label5", form1 );
	tf_wrang2 = UxCreateTextField( "tf_wrang2", form1 );
	tf_lincat = UxCreateTextField( "tf_lincat", form1 );
	label4 = UxCreateLabel( "label4", form1 );
	tf_ystart = UxCreateTextField( "tf_ystart", form1 );
	label3 = UxCreateLabel( "label3", form1 );
	tf_instrume = UxCreateTextField( "tf_instrume", form1 );
	label8 = UxCreateLabel( "label8", form1 );
	label1 = UxCreateLabel( "label1", form1 );
	tf_wlc = UxCreateTextField( "tf_wlc", form1 );
	tf_session = UxCreateTextField( "tf_session", form1 );
	label2 = UxCreateLabel( "label2", form1 );
	label7 = UxCreateLabel( "label7", form1 );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( MainWindow );
	UxCreateWidget( menu1 );
	UxCreateWidget( menu1_p1 );
	UxCreateWidget( menu1_p1_b1 );
	UxCreateWidget( menu1_p1_b2 );
	UxCreateWidget( menu1_p1_b3 );
	UxCreateWidget( menu1_p1_b4 );
	UxPutSubMenuId( menu1_top_b1, "menu1_p1" );
	UxCreateWidget( menu1_top_b1 );

	UxCreateWidget( menu1_p4 );
	UxCreateWidget( menu1_p4_b1 );
	UxCreateWidget( menu1_p4_b2 );
	UxCreateWidget( menu1_p4_b8 );
	UxCreateWidget( menu1_p4_b10 );
	UxCreateWidget( menu1_p4_b13 );
	UxCreateWidget( menu1_p4_b12 );
	UxCreateWidget( menu1_p4_b3 );
	UxCreateWidget( menu1_p4_b4 );
	UxCreateWidget( menu1_p4_b9 );
	UxCreateWidget( menu1_p4_b5 );
	UxCreateWidget( menu1_p4_b11 );
	UxCreateWidget( menu1_p4_b14 );
	UxCreateWidget( menu1_p4_b15 );
	UxPutSubMenuId( menu1_top_b4, "menu1_p4" );
	UxCreateWidget( menu1_top_b4 );

	UxCreateWidget( menu1_p5 );
	UxCreateWidget( menu_help_context );
	UxCreateWidget( menu_help_help );
	UxCreateWidget( menu1_p5_b5 );
	UxCreateWidget( menu_help_tutorial );
	UxCreateWidget( menu_help_version );
	UxPutSubMenuId( menu1_top_b5, "menu1_p5" );
	UxCreateWidget( menu1_top_b5 );

	UxCreateWidget( form1 );
	UxPutBottomAttachment( form5, "attach_form" );
	UxCreateWidget( form5 );

	UxCreateWidget( pb_main_search );
	UxCreateWidget( pb_main_ident );
	UxCreateWidget( pb_main_calib );
	UxCreateWidget( pb_main_rebin );
	UxCreateWidget( pb_main_extract );
	UxCreateWidget( pb_main_flux );
	UxCreateWidget( pb_main_batch );
	UxPutBottomWidget( separator12, "form5" );
	UxPutBottomAttachment( separator12, "attach_widget" );
	UxCreateWidget( separator12 );

	UxPutBottomWidget( shelp_main, "separator12" );
	UxPutBottomAttachment( shelp_main, "attach_widget" );
	UxCreateWidget( shelp_main );

	UxPutBottomWidget( separator1, "shelp_main" );
	UxPutBottomAttachment( separator1, "attach_widget" );
	UxCreateWidget( separator1 );

	UxPutBottomWidget( tf_imin, "separator1" );
	UxPutBottomAttachment( tf_imin, "attach_widget" );
	UxCreateWidget( tf_imin );

	UxPutBottomWidget( label6, "separator1" );
	UxPutBottomAttachment( label6, "attach_widget" );
	UxCreateWidget( label6 );

	UxPutBottomWidget( tf_wrang1, "tf_imin" );
	UxPutBottomAttachment( tf_wrang1, "attach_widget" );
	UxCreateWidget( tf_wrang1 );

	UxPutBottomWidget( label5, "tf_imin" );
	UxPutBottomAttachment( label5, "attach_widget" );
	UxCreateWidget( label5 );

	UxPutBottomWidget( tf_wrang2, "tf_imin" );
	UxPutBottomAttachment( tf_wrang2, "attach_widget" );
	UxCreateWidget( tf_wrang2 );

	UxPutBottomWidget( tf_lincat, "tf_wrang1" );
	UxPutBottomAttachment( tf_lincat, "attach_widget" );
	UxCreateWidget( tf_lincat );

	UxPutBottomWidget( label4, "tf_wrang1" );
	UxPutBottomAttachment( label4, "attach_widget" );
	UxCreateWidget( label4 );

	UxPutBottomWidget( tf_ystart, "tf_lincat" );
	UxPutBottomAttachment( tf_ystart, "attach_widget" );
	UxCreateWidget( tf_ystart );

	UxPutBottomWidget( label3, "tf_lincat" );
	UxPutBottomAttachment( label3, "attach_widget" );
	UxCreateWidget( label3 );

	UxPutBottomWidget( tf_instrume, "tf_ystart" );
	UxPutBottomAttachment( tf_instrume, "attach_widget" );
	UxCreateWidget( tf_instrume );

	UxPutBottomWidget( label8, "tf_ystart" );
	UxPutBottomAttachment( label8, "attach_widget" );
	UxCreateWidget( label8 );

	UxPutBottomWidget( label1, "tf_instrume" );
	UxPutBottomAttachment( label1, "attach_widget" );
	UxCreateWidget( label1 );

	UxPutBottomWidget( tf_wlc, "tf_instrume" );
	UxPutBottomAttachment( tf_wlc, "attach_widget" );
	UxCreateWidget( tf_wlc );

	UxPutBottomWidget( tf_session, "tf_wlc" );
	UxPutBottomAttachment( tf_session, "attach_widget" );
	UxCreateWidget( tf_session );

	UxPutBottomWidget( label2, "tf_wlc" );
	UxPutBottomAttachment( label2, "attach_widget" );
	UxCreateWidget( label2 );

	UxPutBottomWidget( label7, "tf_session" );
	UxPutBottomAttachment( label7, "attach_widget" );
	UxCreateWidget( label7 );


	UxPutMenuHelpWidget( menu1, "menu1_top_b5" );

	UxAddCallback( menu1_p1_b1, XmNactivateCallback,
			activateCB_menu1_p1_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b2, XmNactivateCallback,
			activateCB_menu1_p1_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b3, XmNactivateCallback,
			activateCB_menu1_p1_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b4, XmNactivateCallback,
			activateCB_menu1_p1_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b1, XmNactivateCallback,
			activateCB_menu1_p4_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b2, XmNactivateCallback,
			activateCB_menu1_p4_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b10, XmNactivateCallback,
			activateCB_menu1_p4_b10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b13, XmNactivateCallback,
			activateCB_menu1_p4_b13,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b3, XmNactivateCallback,
			activateCB_menu1_p4_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b4, XmNactivateCallback,
			activateCB_menu1_p4_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b5, XmNactivateCallback,
			activateCB_menu1_p4_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b11, XmNactivateCallback,
			activateCB_menu1_p4_b11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b15, XmNactivateCallback,
			activateCB_menu1_p4_b15,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_context, XmNactivateCallback,
			activateCB_menu_help_context,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_help, XmNactivateCallback,
			activateCB_menu_help_help,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_tutorial, XmNactivateCallback,
			activateCB_menu_help_tutorial,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_version, XmNactivateCallback,
			activateCB_menu_help_version,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search, XmNactivateCallback,
			activateCB_pb_main_search,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_ident, XmNactivateCallback,
			activateCB_pb_main_ident,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_calib, XmNactivateCallback,
			activateCB_pb_main_calib,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_rebin, XmNactivateCallback,
			activateCB_pb_main_rebin,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_extract, XmNactivateCallback,
			activateCB_pb_main_extract,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_flux, XmNactivateCallback,
			activateCB_pb_main_flux,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_batch, XmNactivateCallback,
			activateCB_pb_main_batch,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_imin, XmNlosingFocusCallback,
			losingFocusCB_tf_imin,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_wrang1, XmNlosingFocusCallback,
			losingFocusCB_tf_wrang1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_wrang2, XmNlosingFocusCallback,
			losingFocusCB_tf_wrang2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_lincat, XmNlosingFocusCallback,
			losingFocusCB_tf_lincat,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_ystart, XmNlosingFocusCallback,
			losingFocusCB_tf_ystart,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( MainWindow, menu1, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, form1 );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	{
		extern swidget create_SearchShell();
		extern swidget create_CalibShell();
		extern swidget create_RebinShell();
		extern swidget create_FluxShell();
		extern swidget create_file_dialog();
		extern swidget create_extin_dialog();
		extern swidget create_resid_dialog();
		extern swidget create_ExtractShell();
		extern swidget create_HelpShell();
		rtrn = _Uxbuild_ApplicWindow();

		create_SearchShell();
		create_CalibShell();
		create_RebinShell();
		create_FluxShell();
		create_file_dialog();
		create_extin_dialog();
		create_resid_dialog();
		create_ExtractShell();
		create_HelpShell();
		
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "ClearHelp", action_ClearHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

