/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	CalibShell.c

.VERSION
 090819         last modif


*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxPushBG.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxTextF.h"
#include "UxSepG.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxCalibShell;
	swidget	Uxform3;
	swidget	UxrowColumn2;
	swidget	Uxrb_wlcmtd_iden;
	swidget	Uxrb_wlcmtd_gues;
	swidget	Uxlabel14;
	swidget	UxseparatorGadget6;
	swidget	UxseparatorGadget7;
	swidget	UxseparatorGadget8;
	swidget	Uxlabel16;
	swidget	Uxtf_tol;
	swidget	Uxlabel17;
	swidget	Uxlabel18;
	swidget	Uxtf_dcx1;
	swidget	Uxtf_alpha;
	swidget	Uxlabel19;
	swidget	Uxlabel20;
	swidget	Uxtf_maxdev;
	swidget	Uxguess_session_label;
	swidget	Uxtf_guess;
	swidget	Uxseparator4;
	swidget	Uxshelp_calib;
	swidget	Uxseparator5;
	swidget	Uxtf_dcx2;
	swidget	Uxtf_wlcniter1;
	swidget	Uxtf_wlcniter2;
	swidget	Uxmenu2_p1;
	swidget	Uxmn_tol_angstroms;
	swidget	Uxmn_tol_pixels;
	swidget	Uxmn_tol;
	swidget	Uxtg_twodopt;
	swidget	Uxtg_coropt;
	swidget	Uxform20;
	swidget	Uxpb_calib_calibrate;
	swidget	UxpushButton40;
	swidget	Uxpb_calib_disper;
	swidget	Uxpb_calib_resid;
	swidget	Uxpb_calib_spec;
	swidget	Uxpb_calib_edit;
	swidget	Uxpb_calib_shape;
	swidget	Uxpb_calib_twice;
	swidget	Uxpb_calib_all;
	swidget	Uxpb_calib_getcur;
} _UxCCalibShell;

#define CalibShell              UxCalibShellContext->UxCalibShell
#define form3                   UxCalibShellContext->Uxform3
#define rowColumn2              UxCalibShellContext->UxrowColumn2
#define rb_wlcmtd_iden          UxCalibShellContext->Uxrb_wlcmtd_iden
#define rb_wlcmtd_gues          UxCalibShellContext->Uxrb_wlcmtd_gues
#define label14                 UxCalibShellContext->Uxlabel14
#define separatorGadget6        UxCalibShellContext->UxseparatorGadget6
#define separatorGadget7        UxCalibShellContext->UxseparatorGadget7
#define separatorGadget8        UxCalibShellContext->UxseparatorGadget8
#define label16                 UxCalibShellContext->Uxlabel16
#define tf_tol                  UxCalibShellContext->Uxtf_tol
#define label17                 UxCalibShellContext->Uxlabel17
#define label18                 UxCalibShellContext->Uxlabel18
#define tf_dcx1                 UxCalibShellContext->Uxtf_dcx1
#define tf_alpha                UxCalibShellContext->Uxtf_alpha
#define label19                 UxCalibShellContext->Uxlabel19
#define label20                 UxCalibShellContext->Uxlabel20
#define tf_maxdev               UxCalibShellContext->Uxtf_maxdev
#define guess_session_label     UxCalibShellContext->Uxguess_session_label
#define tf_guess                UxCalibShellContext->Uxtf_guess
#define separator4              UxCalibShellContext->Uxseparator4
#define shelp_calib             UxCalibShellContext->Uxshelp_calib
#define separator5              UxCalibShellContext->Uxseparator5
#define tf_dcx2                 UxCalibShellContext->Uxtf_dcx2
#define tf_wlcniter1            UxCalibShellContext->Uxtf_wlcniter1
#define tf_wlcniter2            UxCalibShellContext->Uxtf_wlcniter2
#define menu2_p1                UxCalibShellContext->Uxmenu2_p1
#define mn_tol_angstroms        UxCalibShellContext->Uxmn_tol_angstroms
#define mn_tol_pixels           UxCalibShellContext->Uxmn_tol_pixels
#define mn_tol                  UxCalibShellContext->Uxmn_tol
#define tg_twodopt              UxCalibShellContext->Uxtg_twodopt
#define tg_coropt               UxCalibShellContext->Uxtg_coropt
#define form20                  UxCalibShellContext->Uxform20
#define pb_calib_calibrate      UxCalibShellContext->Uxpb_calib_calibrate
#define pushButton40            UxCalibShellContext->UxpushButton40
#define pb_calib_disper         UxCalibShellContext->Uxpb_calib_disper
#define pb_calib_resid          UxCalibShellContext->Uxpb_calib_resid
#define pb_calib_spec           UxCalibShellContext->Uxpb_calib_spec
#define pb_calib_edit           UxCalibShellContext->Uxpb_calib_edit
#define pb_calib_shape          UxCalibShellContext->Uxpb_calib_shape
#define pb_calib_twice          UxCalibShellContext->Uxpb_calib_twice
#define pb_calib_all            UxCalibShellContext->Uxpb_calib_all
#define pb_calib_getcur         UxCalibShellContext->Uxpb_calib_getcur

static _UxCCalibShell	*UxCalibShellContext;

extern void WriteKeyword(), SetFileList(), UpdateRebinParameters();

extern int SCTPUT(), AppendDialogText();
extern int  read_lincat_table(), display_lincat_table();

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SelectFileCalib = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_CalibShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	valueChangedCB_rb_wlcmtd_gues( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( XmToggleButtonGetState(UxWidget) ) { /*selected */
	    if ( UpdateToggle )
	        WriteKeyword("GUESS", K_WLCMTD);
	    XtSetSensitive(UxGetWidget(guess_session_label), TRUE);
	    XtSetSensitive(UxGetWidget(tf_guess), TRUE);
	/* later
	    XtSetSensitive(UxGetWidget(shift_label), TRUE);
	    XtSetSensitive(UxGetWidget(tf_shift), TRUE);
	*/
	    XtSetSensitive(UxGetWidget(tg_coropt), TRUE);
	}
	else {
	    if ( UpdateToggle )
	        WriteKeyword("IDENT", K_WLCMTD);
	    XtSetSensitive(UxGetWidget(guess_session_label), FALSE);
	    XtSetSensitive(UxGetWidget(tf_guess), FALSE);
	/* later
	    XtSetSensitive(UxGetWidget(shift_label), FALSE);
	    XtSetSensitive(UxGetWidget(tf_shift), FALSE);
	*/
	    XtSetSensitive(UxGetWidget(tg_coropt), FALSE);
	}
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_tol( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text, newtext[32];
	float val;
	extern float Tol;
	extern int TolPixels;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Tol ) {
	    Tol = val;
	    sprintf(newtext, "%f", (TolPixels ? val : -val));
	    WriteKeyword(newtext, K_TOL);
	}
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_dcx1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Dcx[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Dcx[0] ) {
	    Dcx[0] = val;
	    WriteKeyword(text, K_DCX1);
	}
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Alpha;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Alpha ) {
	    Alpha = val;
	    WriteKeyword(text, K_ALPHA);
	}
	
	XtFree(text);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_maxdev( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Maxdev;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Maxdev ) {
	    Maxdev = val;
	    WriteKeyword(text, K_MAXDEV);
	}
	
	XtFree(text);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_guess( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Guess[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Guess) ) {
	    strcpy(Guess, text);
	    WriteKeyword(text, K_GUESS);
	}
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_dcx2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Dcx[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Dcx[1] ) {
	    Dcx[1] = val;
	    WriteKeyword(text, K_DCX2);
	}
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wlcniter1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Wlcniter[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Wlcniter[0] ) {
	    Wlcniter[0] = val;
	    WriteKeyword(text, K_WLCNITER1);
	}
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wlcniter2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Wlcniter[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Wlcniter[1] ) {
	    Wlcniter[1] = val;
	    WriteKeyword(text, K_WLCNITER2);
	}
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char text[32];
	extern float Tol;
	extern int TolPixels;
	
	TolPixels = FALSE;
	
	sprintf(text, "%f", -Tol);
	WriteKeyword(text, K_TOL);
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char text[32];
	extern float Tol;
	extern int TolPixels;
	
	TolPixels = TRUE;
	
	sprintf(text, "%f", Tol);
	WriteKeyword(text, K_TOL);
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_tg_twodopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_TWODOPT);
	else
	    WriteKeyword("NO", K_TWODOPT);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_tg_coropt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( !UpdateToggle )
	    return;
	
	if ( XmToggleButtonGetState(UxWidget) )
	    AppendDialogText("set/long CORVISU=YES COROPT=YES");
	else
	    AppendDialogText("set/long CORVISU=NO COROPT=NO");
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_calibrate( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern char Plotmode[];
	
	AppendDialogText(C_SELINE);
	AppendDialogText(C_CALIB_RBR);
	AppendDialogText(C_PLOT_DELTA);
	strcpy(Plotmode, C_PLOT_DELTA);
	UpdateRebinParameters();
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pushButton40( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	AppendDialogText(C_SELCAT_ALL);
	UxPopdownInterface(UxFindSwidget("CalibShell"));
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_disper( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern char Plotmode[];
	
	AppendDialogText(C_PLOT_DELTA);
	strcpy(Plotmode, C_PLOT_DELTA);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_resid( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	extern int Ystart;
	char str[20];
	
	sprintf(str, "%d", Ystart);
	XmTextSetString(UxGetWidget(UxFindSwidget("tf_residual")), str);
	UxPopupInterface(UxFindSwidget("resid_dialog"), exclusive_grab);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_spec( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern char Plotmode[];
	
	AppendDialogText(C_PLOT_CALIB);
	strcpy(Plotmode, C_PLOT_CALIB);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_edit( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
        extern char Plotmode[];

	if ( !strcmp(Plotmode, C_PLOT_DELTA) )  
	    AppendDialogText(C_CALIB_EDIT_D);
	else if ( !strcmp(Plotmode, C_PLOT_CALIB) )  
	    AppendDialogText(C_CALIB_EDIT_C);
	else {
	    SCTPUT("** Edition can only be done in the dispersion or the spectrum plot.");
	    return;
	}
	
	AppendDialogText(C_CALIB_ERAS);
	UpdateRebinParameters();
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_shape( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	if ( !read_lincat_table() ) {
	    SCTPUT( "Error: cannot read Line Catalog Table");
	    return;
	}
	display_lincat_table(UxGetWidget(UxFindSwidget("sl_lincat_list")));
	UxPopupInterface(UxFindSwidget("lincat_list"), exclusive_grab);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_twice( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_SELLIN_ALL);
	AppendDialogText(C_CALIB_RBR2);
	UpdateRebinParameters();
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_all( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	AppendDialogText(C_SELLIN_ALL);
	AppendDialogText(C_CALIB_RBR);
	UpdateRebinParameters();
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_getcur( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_CALIB_CURS);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_CalibShell()
{
	UxPutGeometry( CalibShell, "+10+60" );
	UxPutDefaultFontList( CalibShell, TextFont );
	UxPutKeyboardFocusPolicy( CalibShell, "pointer" );
	UxPutIconName( CalibShell, "Calibration" );
	UxPutHeight( CalibShell, 387 );
	UxPutWidth( CalibShell, 490 );
	UxPutY( CalibShell, 367 );
	UxPutX( CalibShell, 449 );

	UxPutBackground( form3, "grey80" );
	UxPutHeight( form3, 548 );
	UxPutWidth( form3, 490 );
	UxPutY( form3, 4 );
	UxPutX( form3, 4 );
	UxPutUnitType( form3, "pixels" );
	UxPutResizePolicy( form3, "resize_none" );

	UxPutIsAligned( rowColumn2, "true" );
	UxPutEntryAlignment( rowColumn2, "alignment_beginning" );
	UxPutBorderWidth( rowColumn2, 0 );
	UxPutShadowThickness( rowColumn2, 0 );
	UxPutLabelString( rowColumn2, "" );
	UxPutEntryBorder( rowColumn2, 0 );
	UxPutBackground( rowColumn2, "grey80" );
	UxPutRadioBehavior( rowColumn2, "true" );
	UxPutHeight( rowColumn2, 67 );
	UxPutWidth( rowColumn2, 82 );
	UxPutY( rowColumn2, 113 );
	UxPutX( rowColumn2, 344 );

	UxPutIndicatorSize( rb_wlcmtd_iden, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_iden, "true" );
	UxPutForeground( rb_wlcmtd_iden, TextForeground );
	UxPutSelectColor( rb_wlcmtd_iden, SelectColor );
	UxPutSet( rb_wlcmtd_iden, "true" );
	UxPutLabelString( rb_wlcmtd_iden, "IDENT" );
	UxPutFontList( rb_wlcmtd_iden, TextFont );
	UxPutBackground( rb_wlcmtd_iden, WindowBackground );
	UxPutHeight( rb_wlcmtd_iden, 20 );
	UxPutWidth( rb_wlcmtd_iden, 76 );
	UxPutY( rb_wlcmtd_iden, 2 );
	UxPutX( rb_wlcmtd_iden, 3 );

	UxPutIndicatorSize( rb_wlcmtd_gues, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues, "true" );
	UxPutSet( rb_wlcmtd_gues, "false" );
	UxPutForeground( rb_wlcmtd_gues, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues, "GUESS" );
	UxPutFontList( rb_wlcmtd_gues, TextFont );
	UxPutBackground( rb_wlcmtd_gues, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues, 66 );
	UxPutWidth( rb_wlcmtd_gues, 76 );
	UxPutY( rb_wlcmtd_gues, 34 );
	UxPutX( rb_wlcmtd_gues, 3 );

	UxPutForeground( label14, TextForeground );
	UxPutAlignment( label14, "alignment_beginning" );
	UxPutLabelString( label14, "Calibration method" );
	UxPutFontList( label14, TextFont );
	UxPutBackground( label14, WindowBackground );
	UxPutHeight( label14, 18 );
	UxPutWidth( label14, 129 );
	UxPutY( label14, 94 );
	UxPutX( label14, 329 );

	UxPutOrientation( separatorGadget6, "vertical" );
	UxPutHeight( separatorGadget6, 84 );
	UxPutWidth( separatorGadget6, 7 );
	UxPutY( separatorGadget6, 102 );
	UxPutX( separatorGadget6, 321 );

	UxPutHeight( separatorGadget7, 8 );
	UxPutWidth( separatorGadget7, 136 );
	UxPutY( separatorGadget7, 182 );
	UxPutX( separatorGadget7, 323 );

	UxPutOrientation( separatorGadget8, "vertical" );
	UxPutHeight( separatorGadget8, 84 );
	UxPutWidth( separatorGadget8, 8 );
	UxPutY( separatorGadget8, 102 );
	UxPutX( separatorGadget8, 457 );

	UxPutForeground( label16, TextForeground );
	UxPutAlignment( label16, "alignment_beginning" );
	UxPutLabelString( label16, "Tolerance :" );
	UxPutFontList( label16, TextFont );
	UxPutBackground( label16, LabelBackground );
	UxPutHeight( label16, 30 );
	UxPutWidth( label16, 203 );
	UxPutY( label16, 12 );
	UxPutX( label16, 12 );

	UxPutForeground( tf_tol, TextForeground );
	UxPutHighlightOnEnter( tf_tol, "true" );
	UxPutFontList( tf_tol, TextFont );
	UxPutBackground( tf_tol, TextBackground );
	UxPutHeight( tf_tol, 34 );
	UxPutWidth( tf_tol, 82 );
	UxPutY( tf_tol, 6 );
	UxPutX( tf_tol, 216 );

	UxPutForeground( label17, TextForeground );
	UxPutAlignment( label17, "alignment_beginning" );
	UxPutLabelString( label17, "Degree of polynomial (x, y) :" );
	UxPutFontList( label17, TextFont );
	UxPutBackground( label17, LabelBackground );
	UxPutHeight( label17, 30 );
	UxPutWidth( label17, 198 );
	UxPutY( label17, 50 );
	UxPutX( label17, 12 );

	UxPutForeground( label18, TextForeground );
	UxPutAlignment( label18, "alignment_beginning" );
	UxPutLabelString( label18, "Min. and Max. number of iter. :" );
	UxPutFontList( label18, TextFont );
	UxPutBackground( label18, LabelBackground );
	UxPutHeight( label18, 30 );
	UxPutWidth( label18, 200 );
	UxPutY( label18, 88 );
	UxPutX( label18, 12 );

	UxPutForeground( tf_dcx1, TextForeground );
	UxPutHighlightOnEnter( tf_dcx1, "true" );
	UxPutFontList( tf_dcx1, TextFont );
	UxPutBackground( tf_dcx1, TextBackground );
	UxPutHeight( tf_dcx1, 34 );
	UxPutWidth( tf_dcx1, 44 );
	UxPutY( tf_dcx1, 44 );
	UxPutX( tf_dcx1, 216 );

	UxPutForeground( tf_alpha, TextForeground );
	UxPutHighlightOnEnter( tf_alpha, "true" );
	UxPutFontList( tf_alpha, TextFont );
	UxPutBackground( tf_alpha, TextBackground );
	UxPutHeight( tf_alpha, 34 );
	UxPutWidth( tf_alpha, 82 );
	UxPutY( tf_alpha, 120 );
	UxPutX( tf_alpha, 216 );

	UxPutForeground( label19, TextForeground );
	UxPutAlignment( label19, "alignment_beginning" );
	UxPutLabelString( label19, "Matching parameter :" );
	UxPutFontList( label19, TextFont );
	UxPutBackground( label19, LabelBackground );
	UxPutHeight( label19, 30 );
	UxPutWidth( label19, 205 );
	UxPutY( label19, 126 );
	UxPutX( label19, 12 );

	UxPutForeground( label20, TextForeground );
	UxPutAlignment( label20, "alignment_beginning" );
	UxPutLabelString( label20, "Maximum deviation (pixels) :" );
	UxPutFontList( label20, TextFont );
	UxPutBackground( label20, LabelBackground );
	UxPutHeight( label20, 30 );
	UxPutWidth( label20, 205 );
	UxPutY( label20, 166 );
	UxPutX( label20, 12 );

	UxPutForeground( tf_maxdev, TextForeground );
	UxPutHighlightOnEnter( tf_maxdev, "true" );
	UxPutFontList( tf_maxdev, TextFont );
	UxPutBackground( tf_maxdev, TextBackground );
	UxPutHeight( tf_maxdev, 34 );
	UxPutWidth( tf_maxdev, 82 );
	UxPutY( tf_maxdev, 158 );
	UxPutX( tf_maxdev, 216 );

	UxPutSensitive( guess_session_label, "false" );
	UxPutForeground( guess_session_label, TextForeground );
	UxPutAlignment( guess_session_label, "alignment_beginning" );
	UxPutLabelString( guess_session_label, "Initial guess :" );
	UxPutFontList( guess_session_label, TextFont );
	UxPutBackground( guess_session_label, LabelBackground );
	UxPutHeight( guess_session_label, 30 );
	UxPutWidth( guess_session_label, 90 );
	UxPutY( guess_session_label, 204 );
	UxPutX( guess_session_label, 12 );

	UxPutTranslations( tf_guess, SelectFileCalib );
	UxPutSensitive( tf_guess, "false" );
	UxPutForeground( tf_guess, TextForeground );
	UxPutHighlightOnEnter( tf_guess, "true" );
	UxPutFontList( tf_guess, TextFont );
	UxPutBackground( tf_guess, TextBackground );
	UxPutHeight( tf_guess, 40 );
	UxPutWidth( tf_guess, 170 );
	UxPutY( tf_guess, 200 );
	UxPutX( tf_guess, 104 );

	UxPutBackground( separator4, WindowBackground );
	UxPutHeight( separator4, 10 );
	UxPutWidth( separator4, 492 );
	UxPutY( separator4, 238 );
	UxPutX( separator4, 0 );

	UxPutFontList( shelp_calib, TextFont );
	UxPutEditable( shelp_calib, "false" );
	UxPutCursorPositionVisible( shelp_calib, "false" );
	UxPutBackground( shelp_calib, SHelpBackground );
	UxPutHeight( shelp_calib, 50 );
	UxPutWidth( shelp_calib, 490 );
	UxPutY( shelp_calib, 248 );
	UxPutX( shelp_calib, 0 );

	UxPutBackground( separator5, WindowBackground );
	UxPutHeight( separator5, 6 );
	UxPutWidth( separator5, 496 );
	UxPutY( separator5, 306 );
	UxPutX( separator5, -4 );

	UxPutForeground( tf_dcx2, TextForeground );
	UxPutHighlightOnEnter( tf_dcx2, "true" );
	UxPutFontList( tf_dcx2, TextFont );
	UxPutBackground( tf_dcx2, TextBackground );
	UxPutHeight( tf_dcx2, 34 );
	UxPutWidth( tf_dcx2, 44 );
	UxPutY( tf_dcx2, 44 );
	UxPutX( tf_dcx2, 260 );

	UxPutForeground( tf_wlcniter1, TextForeground );
	UxPutHighlightOnEnter( tf_wlcniter1, "true" );
	UxPutFontList( tf_wlcniter1, TextFont );
	UxPutBackground( tf_wlcniter1, TextBackground );
	UxPutHeight( tf_wlcniter1, 34 );
	UxPutWidth( tf_wlcniter1, 44 );
	UxPutY( tf_wlcniter1, 82 );
	UxPutX( tf_wlcniter1, 216 );

	UxPutForeground( tf_wlcniter2, TextForeground );
	UxPutHighlightOnEnter( tf_wlcniter2, "true" );
	UxPutFontList( tf_wlcniter2, TextFont );
	UxPutBackground( tf_wlcniter2, TextBackground );
	UxPutHeight( tf_wlcniter2, 34 );
	UxPutWidth( tf_wlcniter2, 44 );
	UxPutY( tf_wlcniter2, 82 );
	UxPutX( tf_wlcniter2, 260 );

	UxPutForeground( menu2_p1, TextForeground );
	UxPutBackground( menu2_p1, WindowBackground );
	UxPutRowColumnType( menu2_p1, "menu_pulldown" );

	UxPutFontList( mn_tol_angstroms, TextFont );
	UxPutLabelString( mn_tol_angstroms, " Angstroms " );

	UxPutFontList( mn_tol_pixels, TextFont );
	UxPutLabelString( mn_tol_pixels, " Pixels " );

	UxPutLabelString( mn_tol, " " );
	UxPutSpacing( mn_tol, 0 );
	UxPutMarginWidth( mn_tol, 0 );
	UxPutForeground( mn_tol, TextForeground );
	UxPutBackground( mn_tol, WindowBackground );
	UxPutY( mn_tol, 8 );
	UxPutX( mn_tol, 296 );
	UxPutRowColumnType( mn_tol, "menu_option" );

	UxPutIndicatorSize( tg_twodopt, 16 );
	UxPutSensitive( tg_twodopt, "true" );
	UxPutHighlightOnEnter( tg_twodopt, "true" );
	UxPutForeground( tg_twodopt, TextForeground );
	UxPutSelectColor( tg_twodopt, SelectColor );
	UxPutSet( tg_twodopt, "false" );
	UxPutLabelString( tg_twodopt, "Compute 2-D solution" );
	UxPutFontList( tg_twodopt, TextFont );
	UxPutBackground( tg_twodopt, WindowBackground );
	UxPutHeight( tg_twodopt, 31 );
	UxPutWidth( tg_twodopt, 172 );
	UxPutY( tg_twodopt, 46 );
	UxPutX( tg_twodopt, 306 );

	UxPutIndicatorSize( tg_coropt, 16 );
	UxPutSensitive( tg_coropt, "false" );
	UxPutHighlightOnEnter( tg_coropt, "true" );
	UxPutForeground( tg_coropt, TextForeground );
	UxPutSelectColor( tg_coropt, SelectColor );
	UxPutSet( tg_coropt, "false" );
	UxPutLabelString( tg_coropt, "Compute Cross-correlation" );
	UxPutFontList( tg_coropt, TextFont );
	UxPutBackground( tg_coropt, WindowBackground );
	UxPutHeight( tg_coropt, 26 );
	UxPutWidth( tg_coropt, 206 );
	UxPutY( tg_coropt, 204 );
	UxPutX( tg_coropt, 282 );

	UxPutBackground( form20, ButtonBackground );
	UxPutHeight( form20, 76 );
	UxPutWidth( form20, 490 );
	UxPutY( form20, 310 );
	UxPutX( form20, 0 );
	UxPutResizePolicy( form20, "resize_none" );

	UxPutLabelString( pb_calib_calibrate, "Calibrate" );
	UxPutForeground( pb_calib_calibrate, ApplyForeground );
	UxPutFontList( pb_calib_calibrate, BoldTextFont );
	UxPutBackground( pb_calib_calibrate, ButtonBackground );
	UxPutHeight( pb_calib_calibrate, 30 );
	UxPutWidth( pb_calib_calibrate, 86 );
	UxPutY( pb_calib_calibrate, 6 );
	UxPutX( pb_calib_calibrate, 10 );

	UxPutLabelString( pushButton40, "Cancel" );
	UxPutForeground( pushButton40, CancelForeground );
	UxPutFontList( pushButton40, BoldTextFont );
	UxPutBackground( pushButton40, ButtonBackground );
	UxPutHeight( pushButton40, 30 );
	UxPutWidth( pushButton40, 86 );
	UxPutY( pushButton40, 40 );
	UxPutX( pushButton40, 384 );

	UxPutLabelString( pb_calib_disper, "Dispersion" );
	UxPutForeground( pb_calib_disper, ButtonForeground );
	UxPutFontList( pb_calib_disper, BoldTextFont );
	UxPutBackground( pb_calib_disper, ButtonBackground );
	UxPutHeight( pb_calib_disper, 30 );
	UxPutWidth( pb_calib_disper, 86 );
	UxPutY( pb_calib_disper, 40 );
	UxPutX( pb_calib_disper, 10 );

	UxPutLabelString( pb_calib_resid, "Residuals" );
	UxPutForeground( pb_calib_resid, ButtonForeground );
	UxPutFontList( pb_calib_resid, BoldTextFont );
	UxPutBackground( pb_calib_resid, ButtonBackground );
	UxPutHeight( pb_calib_resid, 30 );
	UxPutWidth( pb_calib_resid, 86 );
	UxPutY( pb_calib_resid, 40 );
	UxPutX( pb_calib_resid, 102 );

	UxPutLabelString( pb_calib_spec, "Spectrum" );
	UxPutForeground( pb_calib_spec, ButtonForeground );
	UxPutFontList( pb_calib_spec, BoldTextFont );
	UxPutBackground( pb_calib_spec, ButtonBackground );
	UxPutHeight( pb_calib_spec, 30 );
	UxPutWidth( pb_calib_spec, 86 );
	UxPutY( pb_calib_spec, 40 );
	UxPutX( pb_calib_spec, 196 );

	UxPutLabelString( pb_calib_edit, "Edit" );
	UxPutForeground( pb_calib_edit, ApplyForeground );
	UxPutFontList( pb_calib_edit, BoldTextFont );
	UxPutBackground( pb_calib_edit, ButtonBackground );
	UxPutHeight( pb_calib_edit, 30 );
	UxPutWidth( pb_calib_edit, 86 );
	UxPutY( pb_calib_edit, 6 );
	UxPutX( pb_calib_edit, 102 );

	UxPutLabelString( pb_calib_shape, "Line shape" );
	UxPutForeground( pb_calib_shape, ButtonForeground );
	UxPutFontList( pb_calib_shape, BoldTextFont );
	UxPutBackground( pb_calib_shape, ButtonBackground );
	UxPutHeight( pb_calib_shape, 30 );
	UxPutWidth( pb_calib_shape, 86 );
	UxPutY( pb_calib_shape, 40 );
	UxPutX( pb_calib_shape, 290 );

	UxPutLabelString( pb_calib_twice, "Calib twice" );
	UxPutForeground( pb_calib_twice, ApplyForeground );
	UxPutFontList( pb_calib_twice, BoldTextFont );
	UxPutBackground( pb_calib_twice, ButtonBackground );
	UxPutHeight( pb_calib_twice, 30 );
	UxPutWidth( pb_calib_twice, 86 );
	UxPutY( pb_calib_twice, 6 );
	UxPutX( pb_calib_twice, 384 );

	UxPutLabelString( pb_calib_all, "Calib all" );
	UxPutForeground( pb_calib_all, ApplyForeground );
	UxPutFontList( pb_calib_all, BoldTextFont );
	UxPutBackground( pb_calib_all, ButtonBackground );
	UxPutHeight( pb_calib_all, 30 );
	UxPutWidth( pb_calib_all, 86 );
	UxPutY( pb_calib_all, 6 );
	UxPutX( pb_calib_all, 290 );

	UxPutLabelString( pb_calib_getcur, "Get cursor" );
	UxPutForeground( pb_calib_getcur, ApplyForeground );
	UxPutFontList( pb_calib_getcur, BoldTextFont );
	UxPutBackground( pb_calib_getcur, ButtonBackground );
	UxPutHeight( pb_calib_getcur, 30 );
	UxPutWidth( pb_calib_getcur, 86 );
	UxPutY( pb_calib_getcur, 6 );
	UxPutX( pb_calib_getcur, 196 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_CalibShell()
{
	/* Create the swidgets */

	CalibShell = UxCreateApplicationShell( "CalibShell", NO_PARENT );
	UxPutContext( CalibShell, UxCalibShellContext );

	form3 = UxCreateForm( "form3", CalibShell );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", form3 );
	rb_wlcmtd_iden = UxCreateToggleButton( "rb_wlcmtd_iden", rowColumn2 );
	rb_wlcmtd_gues = UxCreateToggleButton( "rb_wlcmtd_gues", rowColumn2 );
	label14 = UxCreateLabel( "label14", form3 );
	separatorGadget6 = UxCreateSeparatorGadget( "separatorGadget6", form3 );
	separatorGadget7 = UxCreateSeparatorGadget( "separatorGadget7", form3 );
	separatorGadget8 = UxCreateSeparatorGadget( "separatorGadget8", form3 );
	label16 = UxCreateLabel( "label16", form3 );
	tf_tol = UxCreateTextField( "tf_tol", form3 );
	label17 = UxCreateLabel( "label17", form3 );
	label18 = UxCreateLabel( "label18", form3 );
	tf_dcx1 = UxCreateTextField( "tf_dcx1", form3 );
	tf_alpha = UxCreateTextField( "tf_alpha", form3 );
	label19 = UxCreateLabel( "label19", form3 );
	label20 = UxCreateLabel( "label20", form3 );
	tf_maxdev = UxCreateTextField( "tf_maxdev", form3 );
	guess_session_label = UxCreateLabel( "guess_session_label", form3 );
	tf_guess = UxCreateTextField( "tf_guess", form3 );
	separator4 = UxCreateSeparator( "separator4", form3 );
	shelp_calib = UxCreateText( "shelp_calib", form3 );
	separator5 = UxCreateSeparator( "separator5", form3 );
	tf_dcx2 = UxCreateTextField( "tf_dcx2", form3 );
	tf_wlcniter1 = UxCreateTextField( "tf_wlcniter1", form3 );
	tf_wlcniter2 = UxCreateTextField( "tf_wlcniter2", form3 );
	menu2_p1 = UxCreateRowColumn( "menu2_p1", form3 );
	mn_tol_angstroms = UxCreatePushButtonGadget( "mn_tol_angstroms", menu2_p1 );
	mn_tol_pixels = UxCreatePushButtonGadget( "mn_tol_pixels", menu2_p1 );
	mn_tol = UxCreateRowColumn( "mn_tol", form3 );
	tg_twodopt = UxCreateToggleButton( "tg_twodopt", form3 );
	tg_coropt = UxCreateToggleButton( "tg_coropt", form3 );
	form20 = UxCreateForm( "form20", form3 );
	pb_calib_calibrate = UxCreatePushButton( "pb_calib_calibrate", form20 );
	pushButton40 = UxCreatePushButton( "pushButton40", form20 );
	pb_calib_disper = UxCreatePushButton( "pb_calib_disper", form20 );
	pb_calib_resid = UxCreatePushButton( "pb_calib_resid", form20 );
	pb_calib_spec = UxCreatePushButton( "pb_calib_spec", form20 );
	pb_calib_edit = UxCreatePushButton( "pb_calib_edit", form20 );
	pb_calib_shape = UxCreatePushButton( "pb_calib_shape", form20 );
	pb_calib_twice = UxCreatePushButton( "pb_calib_twice", form20 );
	pb_calib_all = UxCreatePushButton( "pb_calib_all", form20 );
	pb_calib_getcur = UxCreatePushButton( "pb_calib_getcur", form20 );

	_Uxinit_CalibShell();

	/* Create the X widgets */

	UxCreateWidget( CalibShell );
	UxCreateWidget( form3 );
	UxCreateWidget( rowColumn2 );
	UxCreateWidget( rb_wlcmtd_iden );
	UxCreateWidget( rb_wlcmtd_gues );
	UxCreateWidget( label14 );
	UxCreateWidget( separatorGadget6 );
	UxCreateWidget( separatorGadget7 );
	UxCreateWidget( separatorGadget8 );
	UxCreateWidget( label16 );
	UxCreateWidget( tf_tol );
	UxCreateWidget( label17 );
	UxCreateWidget( label18 );
	UxCreateWidget( tf_dcx1 );
	UxCreateWidget( tf_alpha );
	UxCreateWidget( label19 );
	UxCreateWidget( label20 );
	UxCreateWidget( tf_maxdev );
	UxCreateWidget( guess_session_label );
	UxCreateWidget( tf_guess );
	UxCreateWidget( separator4 );
	UxCreateWidget( shelp_calib );
	UxCreateWidget( separator5 );
	UxCreateWidget( tf_dcx2 );
	UxCreateWidget( tf_wlcniter1 );
	UxCreateWidget( tf_wlcniter2 );
	UxCreateWidget( menu2_p1 );
	UxCreateWidget( mn_tol_angstroms );
	UxCreateWidget( mn_tol_pixels );
	UxPutSubMenuId( mn_tol, "menu2_p1" );
	UxCreateWidget( mn_tol );

	UxCreateWidget( tg_twodopt );
	UxCreateWidget( tg_coropt );
	UxCreateWidget( form20 );
	UxCreateWidget( pb_calib_calibrate );
	UxCreateWidget( pushButton40 );
	UxCreateWidget( pb_calib_disper );
	UxCreateWidget( pb_calib_resid );
	UxCreateWidget( pb_calib_spec );
	UxCreateWidget( pb_calib_edit );
	UxCreateWidget( pb_calib_shape );
	UxCreateWidget( pb_calib_twice );
	UxCreateWidget( pb_calib_all );
	UxCreateWidget( pb_calib_getcur );

	UxAddCallback( rb_wlcmtd_gues, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_tol, XmNlosingFocusCallback,
			losingFocusCB_tf_tol,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_dcx1, XmNlosingFocusCallback,
			losingFocusCB_tf_dcx1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_alpha, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_maxdev, XmNlosingFocusCallback,
			losingFocusCB_tf_maxdev,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_guess, XmNlosingFocusCallback,
			losingFocusCB_tf_guess,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_dcx2, XmNlosingFocusCallback,
			losingFocusCB_tf_dcx2,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_wlcniter1, XmNlosingFocusCallback,
			losingFocusCB_tf_wlcniter1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_wlcniter2, XmNlosingFocusCallback,
			losingFocusCB_tf_wlcniter2,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( mn_tol_angstroms, XmNactivateCallback,
			activateCB_mn_tol_angstroms,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( mn_tol_pixels, XmNactivateCallback,
			activateCB_mn_tol_pixels,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tg_twodopt, XmNvalueChangedCallback,
			valueChangedCB_tg_twodopt,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tg_coropt, XmNvalueChangedCallback,
			valueChangedCB_tg_coropt,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_calibrate, XmNactivateCallback,
			activateCB_pb_calib_calibrate,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pushButton40, XmNactivateCallback,
			activateCB_pushButton40,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_disper, XmNactivateCallback,
			activateCB_pb_calib_disper,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_resid, XmNactivateCallback,
			activateCB_pb_calib_resid,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_spec, XmNactivateCallback,
			activateCB_pb_calib_spec,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_edit, XmNactivateCallback,
			activateCB_pb_calib_edit,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_shape, XmNactivateCallback,
			activateCB_pb_calib_shape,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_twice, XmNactivateCallback,
			activateCB_pb_calib_twice,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_all, XmNactivateCallback,
			activateCB_pb_calib_all,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_getcur, XmNactivateCallback,
			activateCB_pb_calib_getcur,
			(XtPointer) UxCalibShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( CalibShell );

	return ( CalibShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_CalibShell()
{
	swidget                 rtrn;
	_UxCCalibShell          *UxContext;

	UxCalibShellContext = UxContext =
		(_UxCCalibShell *) UxMalloc( sizeof(_UxCCalibShell) );

	rtrn = _Uxbuild_CalibShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_CalibShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_CalibShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

