/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	resid_dialog.c

.VERSION
 090821         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTogB.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxresid_dialog;
	swidget	Uxform18;
	swidget	Uxform19;
	swidget	UxpushButton29;
	swidget	UxpushButton36;
	swidget	Uxlabel25;
	swidget	Uxtf_residual;
	swidget	Uxtg_residual;
} _UxCresid_dialog;

#define resid_dialog            UxResid_dialogContext->Uxresid_dialog
#define form18                  UxResid_dialogContext->Uxform18
#define form19                  UxResid_dialogContext->Uxform19
#define pushButton29            UxResid_dialogContext->UxpushButton29
#define pushButton36            UxResid_dialogContext->UxpushButton36
#define label25                 UxResid_dialogContext->Uxlabel25
#define tf_residual             UxResid_dialogContext->Uxtf_residual
#define tg_residual             UxResid_dialogContext->Uxtg_residual

static _UxCresid_dialog	*UxResid_dialogContext;

extern int AppendDialogText();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_resid_dialog();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton29( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCresid_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxResid_dialogContext;
	UxResid_dialogContext = UxContext =
			(_UxCresid_dialog *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	 
	char command[256];
	char *text;
	extern char Plotmode[];
	
	strcpy(Plotmode, C_PLOT_RESID);
	if ( XmToggleButtonGetState(UxGetWidget(UxFindSwidget("tg_residual")))) 
	    sprintf(command, "%sALL", C_PLOT_RESID);
	else {
	    text = XmTextGetString(UxGetWidget(UxFindSwidget("tf_residual")));
	    sprintf(command, "%s%s", C_PLOT_RESID, text);
	    XtFree(text);
	}
	AppendDialogText(command);
	UxPopdownInterface(UxFindSwidget("resid_dialog"));
	}
	UxResid_dialogContext = UxSaveCtx;
}

static void	activateCB_pushButton36( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCresid_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxResid_dialogContext;
	UxResid_dialogContext = UxContext =
			(_UxCresid_dialog *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("resid_dialog"));
	}
	UxResid_dialogContext = UxSaveCtx;
}

static void	valueChangedCB_tg_residual( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCresid_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxResid_dialogContext;
	UxResid_dialogContext = UxContext =
			(_UxCresid_dialog *) UxGetContext( UxThisWidget );
	{
	extern int AllResidual;
	
	AllResidual = XmToggleButtonGetState(UxWidget);
	XtSetSensitive(UxGetWidget(UxFindSwidget("tf_residual")), !AllResidual);
	}
	UxResid_dialogContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_resid_dialog()
{
	UxPutBackground( resid_dialog, WindowBackground );
	UxPutGeometry( resid_dialog, "+180+360" );
	UxPutKeyboardFocusPolicy( resid_dialog, "pointer" );
	UxPutTitle( resid_dialog, "Residuals plot" );
	UxPutHeight( resid_dialog, 105 );
	UxPutWidth( resid_dialog, 246 );
	UxPutY( resid_dialog, 121 );
	UxPutX( resid_dialog, 102 );

	UxPutBackground( form18, WindowBackground );
	UxPutHeight( form18, 348 );
	UxPutWidth( form18, 408 );
	UxPutY( form18, 0 );
	UxPutX( form18, 0 );
	UxPutUnitType( form18, "pixels" );
	UxPutResizePolicy( form18, "resize_none" );

	UxPutBackground( form19, ButtonBackground );
	UxPutHeight( form19, 40 );
	UxPutWidth( form19, 452 );
	UxPutY( form19, 62 );
	UxPutX( form19, 0 );
	UxPutResizePolicy( form19, "resize_none" );

	UxPutLabelString( pushButton29, "Ok" );
	UxPutForeground( pushButton29, ApplyForeground );
	UxPutFontList( pushButton29, BoldTextFont );
	UxPutBackground( pushButton29, ButtonBackground );
	UxPutHeight( pushButton29, 30 );
	UxPutWidth( pushButton29, 80 );
	UxPutY( pushButton29, 4 );
	UxPutX( pushButton29, 8 );

	UxPutLabelString( pushButton36, "Cancel" );
	UxPutForeground( pushButton36, CancelForeground );
	UxPutFontList( pushButton36, BoldTextFont );
	UxPutBackground( pushButton36, ButtonBackground );
	UxPutHeight( pushButton36, 30 );
	UxPutWidth( pushButton36, 80 );
	UxPutY( pushButton36, 4 );
	UxPutX( pushButton36, 100 );

	UxPutForeground( label25, TextForeground );
	UxPutAlignment( label25, "alignment_beginning" );
	UxPutLabelString( label25, "Row number :" );
	UxPutFontList( label25, TextFont );
	UxPutBackground( label25, LabelBackground );
	UxPutHeight( label25, 36 );
	UxPutWidth( label25, 94 );
	UxPutY( label25, 10 );
	UxPutX( label25, 22 );

	UxPutForeground( tf_residual, TextForeground );
	UxPutHighlightOnEnter( tf_residual, "true" );
	UxPutFontList( tf_residual, TextFont );
	UxPutBackground( tf_residual, TextBackground );
	UxPutHeight( tf_residual, 40 );
	UxPutWidth( tf_residual, 54 );
	UxPutY( tf_residual, 8 );
	UxPutX( tf_residual, 120 );

	UxPutIndicatorSize( tg_residual, 16 );
	UxPutHighlightOnEnter( tg_residual, "true" );
	UxPutForeground( tg_residual, TextForeground );
	UxPutSelectColor( tg_residual, SelectColor );
	UxPutSet( tg_residual, "false" );
	UxPutLabelString( tg_residual, "ALL" );
	UxPutFontList( tg_residual, TextFont );
	UxPutBackground( tg_residual, WindowBackground );
	UxPutHeight( tg_residual, 36 );
	UxPutWidth( tg_residual, 60 );
	UxPutY( tg_residual, 10 );
	UxPutX( tg_residual, 176 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_resid_dialog()
{
	/* Create the swidgets */

	resid_dialog = UxCreateApplicationShell( "resid_dialog", NO_PARENT );
	UxPutContext( resid_dialog, UxResid_dialogContext );

	form18 = UxCreateForm( "form18", resid_dialog );
	form19 = UxCreateForm( "form19", form18 );
	pushButton29 = UxCreatePushButton( "pushButton29", form19 );
	pushButton36 = UxCreatePushButton( "pushButton36", form19 );
	label25 = UxCreateLabel( "label25", form18 );
	tf_residual = UxCreateTextField( "tf_residual", form18 );
	tg_residual = UxCreateToggleButton( "tg_residual", form18 );

	_Uxinit_resid_dialog();

	/* Create the X widgets */

	UxCreateWidget( resid_dialog );
	UxCreateWidget( form18 );
	UxCreateWidget( form19 );
	UxCreateWidget( pushButton29 );
	UxCreateWidget( pushButton36 );
	UxCreateWidget( label25 );
	UxCreateWidget( tf_residual );
	UxCreateWidget( tg_residual );

	UxAddCallback( pushButton29, XmNactivateCallback,
			activateCB_pushButton29,
			(XtPointer) UxResid_dialogContext );

	UxAddCallback( pushButton36, XmNactivateCallback,
			activateCB_pushButton36,
			(XtPointer) UxResid_dialogContext );

	UxAddCallback( tg_residual, XmNvalueChangedCallback,
			valueChangedCB_tg_residual,
			(XtPointer) UxResid_dialogContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( resid_dialog );

	return ( resid_dialog );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_resid_dialog()
{
	swidget                 rtrn;
	_UxCresid_dialog        *UxContext;

	UxResid_dialogContext = UxContext =
		(_UxCresid_dialog *) UxMalloc( sizeof(_UxCresid_dialog) );

	rtrn = _Uxbuild_resid_dialog();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_resid_dialog()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_resid_dialog();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

