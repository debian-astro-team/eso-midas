/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	SessionShell.c

.VERSION
 090820         last modif


*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxfile_dialog;
	swidget	Uxform12;
	swidget	Uxform13;
	swidget	UxpushButton25;
	swidget	UxpushButton26;
	swidget	Uxlb_file_dialog;
	swidget	Uxtf_file_dialog;
} _UxCfile_dialog;

#define file_dialog             UxFile_dialogContext->Uxfile_dialog
#define form12                  UxFile_dialogContext->Uxform12
#define form13                  UxFile_dialogContext->Uxform13
#define pushButton25            UxFile_dialogContext->UxpushButton25
#define pushButton26            UxFile_dialogContext->UxpushButton26
#define lb_file_dialog          UxFile_dialogContext->Uxlb_file_dialog
#define tf_file_dialog          UxFile_dialogContext->Uxtf_file_dialog

static _UxCfile_dialog	*UxFile_dialogContext;

extern void CallbackDialog();



/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_file_dialog();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_dialog         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_dialogContext;
	UxFile_dialogContext = UxContext =
			(_UxCfile_dialog *) UxGetContext( UxThisWidget );
	{
	CallbackDialog();
	}
	UxFile_dialogContext = UxSaveCtx;
}

static void	activateCB_pushButton26( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_dialog         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_dialogContext;
	UxFile_dialogContext = UxContext =
			(_UxCfile_dialog *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("file_dialog"));
	}
	UxFile_dialogContext = UxSaveCtx;
}

static void	activateCB_tf_file_dialog( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_dialog         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_dialogContext;
	UxFile_dialogContext = UxContext =
			(_UxCfile_dialog *) UxGetContext( UxThisWidget );
	{
	CallbackDialog();
	}
	UxFile_dialogContext = UxSaveCtx;
}

static void	losingFocusCB_tf_file_dialog( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_dialog         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_dialogContext;
	UxFile_dialogContext = UxContext =
			(_UxCfile_dialog *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char Session[];
	
	text = XmTextGetString(UxWidget);
	strcpy(Session, text);
	
	XtFree(text);
	}
	UxFile_dialogContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_file_dialog()
{
	UxPutBackground( file_dialog, WindowBackground );
	UxPutGeometry( file_dialog, "+100+100" );
	UxPutKeyboardFocusPolicy( file_dialog, "pointer" );
	UxPutTitle( file_dialog, "" );
	UxPutHeight( file_dialog, 105 );
	UxPutWidth( file_dialog, 408 );
	UxPutY( file_dialog, 121 );
	UxPutX( file_dialog, 102 );

	UxPutBackground( form12, WindowBackground );
	UxPutHeight( form12, 348 );
	UxPutWidth( form12, 408 );
	UxPutY( form12, 0 );
	UxPutX( form12, 0 );
	UxPutUnitType( form12, "pixels" );
	UxPutResizePolicy( form12, "resize_none" );

	UxPutBackground( form13, ButtonBackground );
	UxPutHeight( form13, 40 );
	UxPutWidth( form13, 452 );
	UxPutY( form13, 62 );
	UxPutX( form13, 0 );
	UxPutResizePolicy( form13, "resize_none" );

	UxPutLabelString( pushButton25, "Ok" );
	UxPutForeground( pushButton25, ApplyForeground );
	UxPutFontList( pushButton25, BoldTextFont );
	UxPutBackground( pushButton25, ButtonBackground );
	UxPutHeight( pushButton25, 30 );
	UxPutWidth( pushButton25, 80 );
	UxPutY( pushButton25, 4 );
	UxPutX( pushButton25, 8 );

	UxPutLabelString( pushButton26, "Cancel" );
	UxPutForeground( pushButton26, CancelForeground );
	UxPutFontList( pushButton26, BoldTextFont );
	UxPutBackground( pushButton26, ButtonBackground );
	UxPutHeight( pushButton26, 30 );
	UxPutWidth( pushButton26, 80 );
	UxPutY( pushButton26, 4 );
	UxPutX( pushButton26, 100 );

	UxPutForeground( lb_file_dialog, TextForeground );
	UxPutAlignment( lb_file_dialog, "alignment_beginning" );
	UxPutLabelString( lb_file_dialog, "Output parameters file :" );
	UxPutFontList( lb_file_dialog, TextFont );
	UxPutBackground( lb_file_dialog, LabelBackground );
	UxPutHeight( lb_file_dialog, 30 );
	UxPutWidth( lb_file_dialog, 176 );
	UxPutY( lb_file_dialog, 12 );
	UxPutX( lb_file_dialog, 10 );

	UxPutForeground( tf_file_dialog, TextForeground );
	UxPutHighlightOnEnter( tf_file_dialog, "true" );
	UxPutFontList( tf_file_dialog, TextFont );
	UxPutBackground( tf_file_dialog, TextBackground );
	UxPutHeight( tf_file_dialog, 34 );
	UxPutWidth( tf_file_dialog, 210 );
	UxPutY( tf_file_dialog, 10 );
	UxPutX( tf_file_dialog, 190 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_file_dialog()
{
	/* Create the swidgets */

	file_dialog = UxCreateApplicationShell( "file_dialog", NO_PARENT );
	UxPutContext( file_dialog, UxFile_dialogContext );

	form12 = UxCreateForm( "form12", file_dialog );
	form13 = UxCreateForm( "form13", form12 );
	pushButton25 = UxCreatePushButton( "pushButton25", form13 );
	pushButton26 = UxCreatePushButton( "pushButton26", form13 );
	lb_file_dialog = UxCreateLabel( "lb_file_dialog", form12 );
	tf_file_dialog = UxCreateTextField( "tf_file_dialog", form12 );

	_Uxinit_file_dialog();

	/* Create the X widgets */

	UxCreateWidget( file_dialog );
	UxCreateWidget( form12 );
	UxCreateWidget( form13 );
	UxCreateWidget( pushButton25 );
	UxCreateWidget( pushButton26 );
	UxCreateWidget( lb_file_dialog );
	UxCreateWidget( tf_file_dialog );

	UxAddCallback( pushButton25, XmNactivateCallback,
			activateCB_pushButton25,
			(XtPointer) UxFile_dialogContext );

	UxAddCallback( pushButton26, XmNactivateCallback,
			activateCB_pushButton26,
			(XtPointer) UxFile_dialogContext );

	UxAddCallback( tf_file_dialog, XmNactivateCallback,
			activateCB_tf_file_dialog,
			(XtPointer) UxFile_dialogContext );
	UxAddCallback( tf_file_dialog, XmNlosingFocusCallback,
			losingFocusCB_tf_file_dialog,
			(XtPointer) UxFile_dialogContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( file_dialog );

	return ( file_dialog );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_file_dialog()
{
	swidget                 rtrn;
	_UxCfile_dialog         *UxContext;

	UxFile_dialogContext = UxContext =
		(_UxCfile_dialog *) UxMalloc( sizeof(_UxCfile_dialog) );

	rtrn = _Uxbuild_file_dialog();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_file_dialog()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_file_dialog();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

