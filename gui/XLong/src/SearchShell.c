/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	SearchShell.c

.VERSION
 090819         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxSepG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSearchShell;
	swidget	Uxform2;
	swidget	Uxlabel9;
	swidget	Uxtf_ywidth;
	swidget	Uxlabel10;
	swidget	Uxlabel11;
	swidget	Uxtf_ystep;
	swidget	Uxtf_width;
	swidget	UxrowColumn1;
	swidget	Uxrb_seamtd_gaus;
	swidget	Uxrb_seamtd_grav;
	swidget	Uxrb_seamtd_maxi;
	swidget	Uxlabel13;
	swidget	UxseparatorGadget2;
	swidget	UxseparatorGadget3;
	swidget	UxseparatorGadget4;
	swidget	UxseparatorGadget5;
	swidget	Uxtf_thres;
	swidget	Uxform4;
	swidget	Uxpb_search_search;
	swidget	UxpushButton2;
	swidget	Uxpb_search_plot;
	swidget	Uxshelp_search;
	swidget	Uxseparator2;
	swidget	Uxseparator3;
	swidget	UxseparatorGadget1;
	swidget	Uxlabel12;
} _UxCSearchShell;

#define SearchShell             UxSearchShellContext->UxSearchShell
#define form2                   UxSearchShellContext->Uxform2
#define label9                  UxSearchShellContext->Uxlabel9
#define tf_ywidth               UxSearchShellContext->Uxtf_ywidth
#define label10                 UxSearchShellContext->Uxlabel10
#define label11                 UxSearchShellContext->Uxlabel11
#define tf_ystep                UxSearchShellContext->Uxtf_ystep
#define tf_width                UxSearchShellContext->Uxtf_width
#define rowColumn1              UxSearchShellContext->UxrowColumn1
#define rb_seamtd_gaus          UxSearchShellContext->Uxrb_seamtd_gaus
#define rb_seamtd_grav          UxSearchShellContext->Uxrb_seamtd_grav
#define rb_seamtd_maxi          UxSearchShellContext->Uxrb_seamtd_maxi
#define label13                 UxSearchShellContext->Uxlabel13
#define separatorGadget2        UxSearchShellContext->UxseparatorGadget2
#define separatorGadget3        UxSearchShellContext->UxseparatorGadget3
#define separatorGadget4        UxSearchShellContext->UxseparatorGadget4
#define separatorGadget5        UxSearchShellContext->UxseparatorGadget5
#define tf_thres                UxSearchShellContext->Uxtf_thres
#define form4                   UxSearchShellContext->Uxform4
#define pb_search_search        UxSearchShellContext->Uxpb_search_search
#define pushButton2             UxSearchShellContext->UxpushButton2
#define pb_search_plot          UxSearchShellContext->Uxpb_search_plot
#define shelp_search            UxSearchShellContext->Uxshelp_search
#define separator2              UxSearchShellContext->Uxseparator2
#define separator3              UxSearchShellContext->Uxseparator3
#define separatorGadget1        UxSearchShellContext->UxseparatorGadget1
#define label12                 UxSearchShellContext->Uxlabel12

static _UxCSearchShell	*UxSearchShellContext;

extern int PopupList(), AppendDialogText();
extern void WriteKeyword();



/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SearchShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	losingFocusCB_tf_ywidth( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Ywidth;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Ywidth ) {
	    Ywidth = val;
	    WriteKeyword(text, K_YWIDTH);
	}
	
	XtFree(text);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ystep( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Ystep;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Ystep ) {
	    Ystep = val;
	    WriteKeyword(text, K_YSTEP);
	}
	
	XtFree(text);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_width( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Width;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Width ) {
	    Width = val;
	    WriteKeyword(text, K_WIDTH);
	}
	
	XtFree(text);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("GAUSS", K_SEAMTD);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("GRAVITY", K_SEAMTD);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_maxi( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("MAXIMUM", K_SEAMTD);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Thres;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Thres ) {
	    Thres = val;
	    WriteKeyword(text, K_THRES);
	}
	
	XtFree(text);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_search( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_WLC);
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pushButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("SearchShell"));
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_PLOT_SEARCH);
	}
	UxSearchShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SearchShell()
{
	UxPutBackground( SearchShell, WindowBackground );
	UxPutGeometry( SearchShell, "+10+60" );
	UxPutKeyboardFocusPolicy( SearchShell, "pointer" );
	UxPutTitle( SearchShell, "Search" );
	UxPutHeight( SearchShell, 281 );
	UxPutWidth( SearchShell, 490 );
	UxPutY( SearchShell, 40 );
	UxPutX( SearchShell, 30 );

	UxPutBackground( form2, WindowBackground );
	UxPutHeight( form2, 348 );
	UxPutWidth( form2, 408 );
	UxPutY( form2, 0 );
	UxPutX( form2, 0 );
	UxPutUnitType( form2, "pixels" );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutForeground( label9, TextForeground );
	UxPutAlignment( label9, "alignment_beginning" );
	UxPutLabelString( label9, "Row smoothing factor :" );
	UxPutFontList( label9, TextFont );
	UxPutBackground( label9, LabelBackground );
	UxPutHeight( label9, 30 );
	UxPutWidth( label9, 203 );
	UxPutY( label9, 92 );
	UxPutX( label9, 16 );

	UxPutForeground( tf_ywidth, TextForeground );
	UxPutHighlightOnEnter( tf_ywidth, "true" );
	UxPutFontList( tf_ywidth, TextFont );
	UxPutBackground( tf_ywidth, TextBackground );
	UxPutHeight( tf_ywidth, 34 );
	UxPutWidth( tf_ywidth, 82 );
	UxPutY( tf_ywidth, 90 );
	UxPutX( tf_ywidth, 230 );

	UxPutForeground( label10, TextForeground );
	UxPutAlignment( label10, "alignment_beginning" );
	UxPutLabelString( label10, "Row binning step :" );
	UxPutFontList( label10, TextFont );
	UxPutBackground( label10, LabelBackground );
	UxPutHeight( label10, 30 );
	UxPutWidth( label10, 205 );
	UxPutY( label10, 128 );
	UxPutX( label10, 16 );

	UxPutForeground( label11, TextForeground );
	UxPutAlignment( label11, "alignment_beginning" );
	UxPutLabelString( label11, "Threshold :" );
	UxPutFontList( label11, TextFont );
	UxPutBackground( label11, LabelBackground );
	UxPutHeight( label11, 30 );
	UxPutWidth( label11, 207 );
	UxPutY( label11, 16 );
	UxPutX( label11, 16 );

	UxPutForeground( tf_ystep, TextForeground );
	UxPutHighlightOnEnter( tf_ystep, "true" );
	UxPutFontList( tf_ystep, TextFont );
	UxPutBackground( tf_ystep, TextBackground );
	UxPutHeight( tf_ystep, 34 );
	UxPutWidth( tf_ystep, 82 );
	UxPutY( tf_ystep, 128 );
	UxPutX( tf_ystep, 230 );

	UxPutForeground( tf_width, TextForeground );
	UxPutHighlightOnEnter( tf_width, "true" );
	UxPutFontList( tf_width, TextFont );
	UxPutBackground( tf_width, TextBackground );
	UxPutHeight( tf_width, 34 );
	UxPutWidth( tf_width, 82 );
	UxPutY( tf_width, 52 );
	UxPutX( tf_width, 230 );

	UxPutIsAligned( rowColumn1, "true" );
	UxPutAdjustMargin( rowColumn1, "true" );
	UxPutAdjustLast( rowColumn1, "false" );
	UxPutEntryAlignment( rowColumn1, "alignment_beginning" );
	UxPutBorderWidth( rowColumn1, 0 );
	UxPutShadowThickness( rowColumn1, 0 );
	UxPutLabelString( rowColumn1, "" );
	UxPutEntryBorder( rowColumn1, 0 );
	UxPutBackground( rowColumn1, WindowBackground );
	UxPutRadioBehavior( rowColumn1, "true" );
	UxPutHeight( rowColumn1, 96 );
	UxPutWidth( rowColumn1, 106 );
	UxPutY( rowColumn1, 40 );
	UxPutX( rowColumn1, 354 );

	UxPutForeground( rb_seamtd_gaus, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus, "true" );
	UxPutSelectColor( rb_seamtd_gaus, SelectColor );
	UxPutSet( rb_seamtd_gaus, "true" );
	UxPutLabelString( rb_seamtd_gaus, "GAUSS" );
	UxPutFontList( rb_seamtd_gaus, TextFont );
	UxPutBackground( rb_seamtd_gaus, WindowBackground );
	UxPutHeight( rb_seamtd_gaus, 30 );
	UxPutWidth( rb_seamtd_gaus, 148 );
	UxPutY( rb_seamtd_gaus, 12 );
	UxPutX( rb_seamtd_gaus, 12 );

	UxPutForeground( rb_seamtd_grav, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav, "true" );
	UxPutSelectColor( rb_seamtd_grav, SelectColor );
	UxPutLabelString( rb_seamtd_grav, "GRAVITY" );
	UxPutFontList( rb_seamtd_grav, TextFont );
	UxPutBackground( rb_seamtd_grav, WindowBackground );
	UxPutHeight( rb_seamtd_grav, 30 );
	UxPutWidth( rb_seamtd_grav, 148 );
	UxPutY( rb_seamtd_grav, 85 );
	UxPutX( rb_seamtd_grav, 5 );

	UxPutForeground( rb_seamtd_maxi, TextForeground );
	UxPutIndicatorSize( rb_seamtd_maxi, 16 );
	UxPutHighlightOnEnter( rb_seamtd_maxi, "true" );
	UxPutSelectColor( rb_seamtd_maxi, SelectColor );
	UxPutLabelString( rb_seamtd_maxi, "MAXIMUM" );
	UxPutFontList( rb_seamtd_maxi, TextFont );
	UxPutBackground( rb_seamtd_maxi, WindowBackground );
	UxPutHeight( rb_seamtd_maxi, 24 );
	UxPutWidth( rb_seamtd_maxi, 96 );
	UxPutY( rb_seamtd_maxi, 66 );
	UxPutX( rb_seamtd_maxi, 4 );

	UxPutForeground( label13, TextForeground );
	UxPutAlignment( label13, "alignment_beginning" );
	UxPutLabelString( label13, "Search method" );
	UxPutFontList( label13, TextFont );
	UxPutBackground( label13, LabelBackground );
	UxPutHeight( label13, 30 );
	UxPutWidth( label13, 106 );
	UxPutY( label13, 10 );
	UxPutX( label13, 358 );

	UxPutOrientation( separatorGadget2, "vertical" );
	UxPutHeight( separatorGadget2, 120 );
	UxPutWidth( separatorGadget2, 12 );
	UxPutY( separatorGadget2, 24 );
	UxPutX( separatorGadget2, 472 );

	UxPutOrientation( separatorGadget3, "vertical" );
	UxPutHeight( separatorGadget3, 120 );
	UxPutWidth( separatorGadget3, 12 );
	UxPutY( separatorGadget3, 24 );
	UxPutX( separatorGadget3, 336 );

	UxPutHeight( separatorGadget4, 10 );
	UxPutWidth( separatorGadget4, 16 );
	UxPutY( separatorGadget4, 22 );
	UxPutX( separatorGadget4, 340 );

	UxPutHeight( separatorGadget5, 10 );
	UxPutWidth( separatorGadget5, 14 );
	UxPutY( separatorGadget5, 22 );
	UxPutX( separatorGadget5, 464 );

	UxPutForeground( tf_thres, TextForeground );
	UxPutHighlightOnEnter( tf_thres, "true" );
	UxPutFontList( tf_thres, TextFont );
	UxPutBackground( tf_thres, TextBackground );
	UxPutHeight( tf_thres, 34 );
	UxPutWidth( tf_thres, 82 );
	UxPutY( tf_thres, 14 );
	UxPutX( tf_thres, 230 );

	UxPutBackground( form4, ButtonBackground );
	UxPutHeight( form4, 40 );
	UxPutWidth( form4, 490 );
	UxPutY( form4, 240 );
	UxPutX( form4, 0 );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutLabelString( pb_search_search, "Search On..." );
	UxPutForeground( pb_search_search, ApplyForeground );
	UxPutFontList( pb_search_search, BoldTextFont );
	UxPutBackground( pb_search_search, ButtonBackground );
	UxPutHeight( pb_search_search, 30 );
	UxPutWidth( pb_search_search, 112 );
	UxPutY( pb_search_search, 6 );
	UxPutX( pb_search_search, 8 );

	UxPutLabelString( pushButton2, "Cancel" );
	UxPutForeground( pushButton2, CancelForeground );
	UxPutFontList( pushButton2, BoldTextFont );
	UxPutBackground( pushButton2, ButtonBackground );
	UxPutHeight( pushButton2, 30 );
	UxPutWidth( pushButton2, 86 );
	UxPutY( pushButton2, 6 );
	UxPutX( pushButton2, 210 );

	UxPutLabelString( pb_search_plot, "Plot" );
	UxPutForeground( pb_search_plot, ButtonForeground );
	UxPutFontList( pb_search_plot, BoldTextFont );
	UxPutBackground( pb_search_plot, ButtonBackground );
	UxPutHeight( pb_search_plot, 30 );
	UxPutWidth( pb_search_plot, 86 );
	UxPutY( pb_search_plot, 6 );
	UxPutX( pb_search_plot, 122 );

	UxPutFontList( shelp_search, TextFont );
	UxPutEditable( shelp_search, "false" );
	UxPutCursorPositionVisible( shelp_search, "false" );
	UxPutBackground( shelp_search, SHelpBackground );
	UxPutHeight( shelp_search, 50 );
	UxPutWidth( shelp_search, 484 );
	UxPutY( shelp_search, 180 );
	UxPutX( shelp_search, 2 );

	UxPutBackground( separator2, WindowBackground );
	UxPutHeight( separator2, 10 );
	UxPutWidth( separator2, 492 );
	UxPutY( separator2, 170 );
	UxPutX( separator2, 0 );

	UxPutBackground( separator3, WindowBackground );
	UxPutHeight( separator3, 10 );
	UxPutWidth( separator3, 492 );
	UxPutY( separator3, 228 );
	UxPutX( separator3, -2 );

	UxPutHeight( separatorGadget1, 10 );
	UxPutWidth( separatorGadget1, 138 );
	UxPutY( separatorGadget1, 138 );
	UxPutX( separatorGadget1, 340 );

	UxPutForeground( label12, TextForeground );
	UxPutAlignment( label12, "alignment_beginning" );
	UxPutLabelString( label12, "Width of search window :" );
	UxPutFontList( label12, TextFont );
	UxPutBackground( label12, LabelBackground );
	UxPutHeight( label12, 30 );
	UxPutWidth( label12, 205 );
	UxPutY( label12, 52 );
	UxPutX( label12, 16 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SearchShell()
{
	/* Create the swidgets */

	SearchShell = UxCreateApplicationShell( "SearchShell", NO_PARENT );
	UxPutContext( SearchShell, UxSearchShellContext );

	form2 = UxCreateForm( "form2", SearchShell );
	label9 = UxCreateLabel( "label9", form2 );
	tf_ywidth = UxCreateTextField( "tf_ywidth", form2 );
	label10 = UxCreateLabel( "label10", form2 );
	label11 = UxCreateLabel( "label11", form2 );
	tf_ystep = UxCreateTextField( "tf_ystep", form2 );
	tf_width = UxCreateTextField( "tf_width", form2 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", form2 );
	rb_seamtd_gaus = UxCreateToggleButton( "rb_seamtd_gaus", rowColumn1 );
	rb_seamtd_grav = UxCreateToggleButton( "rb_seamtd_grav", rowColumn1 );
	rb_seamtd_maxi = UxCreateToggleButton( "rb_seamtd_maxi", rowColumn1 );
	label13 = UxCreateLabel( "label13", form2 );
	separatorGadget2 = UxCreateSeparatorGadget( "separatorGadget2", form2 );
	separatorGadget3 = UxCreateSeparatorGadget( "separatorGadget3", form2 );
	separatorGadget4 = UxCreateSeparatorGadget( "separatorGadget4", form2 );
	separatorGadget5 = UxCreateSeparatorGadget( "separatorGadget5", form2 );
	tf_thres = UxCreateTextField( "tf_thres", form2 );
	form4 = UxCreateForm( "form4", form2 );
	pb_search_search = UxCreatePushButton( "pb_search_search", form4 );
	pushButton2 = UxCreatePushButton( "pushButton2", form4 );
	pb_search_plot = UxCreatePushButton( "pb_search_plot", form4 );
	shelp_search = UxCreateText( "shelp_search", form2 );
	separator2 = UxCreateSeparator( "separator2", form2 );
	separator3 = UxCreateSeparator( "separator3", form2 );
	separatorGadget1 = UxCreateSeparatorGadget( "separatorGadget1", form2 );
	label12 = UxCreateLabel( "label12", form2 );

	_Uxinit_SearchShell();

	/* Create the X widgets */

	UxCreateWidget( SearchShell );
	UxCreateWidget( form2 );
	UxCreateWidget( label9 );
	UxCreateWidget( tf_ywidth );
	UxCreateWidget( label10 );
	UxCreateWidget( label11 );
	UxCreateWidget( tf_ystep );
	UxCreateWidget( tf_width );
	UxCreateWidget( rowColumn1 );
	UxCreateWidget( rb_seamtd_gaus );
	UxCreateWidget( rb_seamtd_grav );
	UxCreateWidget( rb_seamtd_maxi );
	UxCreateWidget( label13 );
	UxCreateWidget( separatorGadget2 );
	UxCreateWidget( separatorGadget3 );
	UxCreateWidget( separatorGadget4 );
	UxCreateWidget( separatorGadget5 );
	UxCreateWidget( tf_thres );
	UxCreateWidget( form4 );
	UxCreateWidget( pb_search_search );
	UxCreateWidget( pushButton2 );
	UxCreateWidget( pb_search_plot );
	UxCreateWidget( shelp_search );
	UxCreateWidget( separator2 );
	UxCreateWidget( separator3 );
	UxCreateWidget( separatorGadget1 );
	UxCreateWidget( label12 );

	UxAddCallback( tf_ywidth, XmNlosingFocusCallback,
			losingFocusCB_tf_ywidth,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_ystep, XmNlosingFocusCallback,
			losingFocusCB_tf_ystep,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_width, XmNlosingFocusCallback,
			losingFocusCB_tf_width,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( rb_seamtd_gaus, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( rb_seamtd_grav, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( rb_seamtd_maxi, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_maxi,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_thres, XmNlosingFocusCallback,
			losingFocusCB_tf_thres,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_search_search, XmNactivateCallback,
			activateCB_pb_search_search,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pushButton2, XmNactivateCallback,
			activateCB_pushButton2,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_search_plot, XmNactivateCallback,
			activateCB_pb_search_plot,
			(XtPointer) UxSearchShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SearchShell );

	return ( SearchShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SearchShell()
{
	swidget                 rtrn;
	_UxCSearchShell         *UxContext;

	UxSearchShellContext = UxContext =
		(_UxCSearchShell *) UxMalloc( sizeof(_UxCSearchShell) );

	rtrn = _Uxbuild_SearchShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SearchShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_SearchShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

