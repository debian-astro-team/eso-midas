/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	RebinShell.c

.VERSION
 090821         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxSepG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxRebinShell;
	swidget	Uxform7;
	swidget	Uxlabel21;
	swidget	Uxtf_rebstrt;
	swidget	Uxlabel22;
	swidget	Uxlabel23;
	swidget	Uxtf_rebend;
	swidget	UxrowColumn3;
	swidget	Uxrb_rebmtd_line;
	swidget	Uxrb_rebmtd_quad;
	swidget	Uxrb_rebmtd_spli;
	swidget	Uxlabel24;
	swidget	UxseparatorGadget11;
	swidget	UxseparatorGadget12;
	swidget	Uxtf_rebstp;
	swidget	Uxform8;
	swidget	Uxpb_rebin_rbr;
	swidget	UxpushButton13;
	swidget	Uxpb_rebin_plot;
	swidget	Uxpb_rebin_table;
	swidget	Uxpb_rebin_2d;
	swidget	Uxshelp_rebin;
	swidget	Uxseparator6;
	swidget	Uxseparator7;
	swidget	UxseparatorGadget15;
} _UxCRebinShell;

#define RebinShell              UxRebinShellContext->UxRebinShell
#define form7                   UxRebinShellContext->Uxform7
#define label21                 UxRebinShellContext->Uxlabel21
#define tf_rebstrt              UxRebinShellContext->Uxtf_rebstrt
#define label22                 UxRebinShellContext->Uxlabel22
#define label23                 UxRebinShellContext->Uxlabel23
#define tf_rebend               UxRebinShellContext->Uxtf_rebend
#define rowColumn3              UxRebinShellContext->UxrowColumn3
#define rb_rebmtd_line          UxRebinShellContext->Uxrb_rebmtd_line
#define rb_rebmtd_quad          UxRebinShellContext->Uxrb_rebmtd_quad
#define rb_rebmtd_spli          UxRebinShellContext->Uxrb_rebmtd_spli
#define label24                 UxRebinShellContext->Uxlabel24
#define separatorGadget11       UxRebinShellContext->UxseparatorGadget11
#define separatorGadget12       UxRebinShellContext->UxseparatorGadget12
#define tf_rebstp               UxRebinShellContext->Uxtf_rebstp
#define form8                   UxRebinShellContext->Uxform8
#define pb_rebin_rbr            UxRebinShellContext->Uxpb_rebin_rbr
#define pushButton13            UxRebinShellContext->UxpushButton13
#define pb_rebin_plot           UxRebinShellContext->Uxpb_rebin_plot
#define pb_rebin_table          UxRebinShellContext->Uxpb_rebin_table
#define pb_rebin_2d             UxRebinShellContext->Uxpb_rebin_2d
#define shelp_rebin             UxRebinShellContext->Uxshelp_rebin
#define separator6              UxRebinShellContext->Uxseparator6
#define separator7              UxRebinShellContext->Uxseparator7
#define separatorGadget15       UxRebinShellContext->UxseparatorGadget15

static _UxCRebinShell	*UxRebinShellContext;

extern void WriteKeyword(), SetFileList();

extern int PopupList(), AppendDialogText();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_RebinShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxRebinShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	losingFocusCB_tf_rebstrt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	double val;
	extern double Rebstrt;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%lf", &val);
	
	if ( val != Rebstrt ) {
	    Rebstrt = val;
	    WriteKeyword(text, K_REBSTRT);
	}
	
	XtFree(text);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_rebend( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	double val;
	extern double Rebend;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%lf", &val);
	
	if ( val != Rebend ) {
	    Rebend = val;
	    WriteKeyword(text, K_REBEND);
	}
	
	XtFree(text);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_rebmtd_line( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("LINEAR", K_REBMTD);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_rebmtd_quad( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("QUADRATIC", K_REBMTD);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_rebmtd_spli( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("SPLINE", K_REBMTD);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_rebstp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	double val;
	extern double Rebstp;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%lf", &val);
	
	if ( val != Rebstp ) {
	    Rebstp = val;
	    WriteKeyword(text, K_REBSTP);
	}
	
	XtFree(text);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_pb_rebin_rbr( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_REBIN_RBR);
	
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_pushButton13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("RebinShell"));
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_pb_rebin_plot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char command[128], *out_file;
	
	out_file = XmTextGetString(UxGetWidget(UxFindSwidget("tf_file_dialog")));
	
	sprintf(command, "%s%s", C_PLOT_SPEC, out_file);
	
	AppendDialogText(command);
	
	XtFree(out_file);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_pb_rebin_table( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_REBIN_TBL);
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_pb_rebin_2d( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_REBIN_2D);
	}
	UxRebinShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_RebinShell()
{
	UxPutBackground( RebinShell, WindowBackground );
	UxPutGeometry( RebinShell, "+10+60" );
	UxPutKeyboardFocusPolicy( RebinShell, "pointer" );
	UxPutTitle( RebinShell, "Rebinning" );
	UxPutHeight( RebinShell, 263 );
	UxPutWidth( RebinShell, 490 );
	UxPutY( RebinShell, 105 );
	UxPutX( RebinShell, 383 );

	UxPutBackground( form7, WindowBackground );
	UxPutHeight( form7, 348 );
	UxPutWidth( form7, 408 );
	UxPutY( form7, 0 );
	UxPutX( form7, 0 );
	UxPutUnitType( form7, "pixels" );
	UxPutResizePolicy( form7, "resize_none" );

	UxPutForeground( label21, TextForeground );
	UxPutAlignment( label21, "alignment_beginning" );
	UxPutLabelString( label21, "Starting wavelength (A) :" );
	UxPutFontList( label21, TextFont );
	UxPutBackground( label21, LabelBackground );
	UxPutHeight( label21, 30 );
	UxPutWidth( label21, 168 );
	UxPutY( label21, 30 );
	UxPutX( label21, 24 );

	UxPutForeground( tf_rebstrt, TextForeground );
	UxPutHighlightOnEnter( tf_rebstrt, "true" );
	UxPutFontList( tf_rebstrt, TextFont );
	UxPutBackground( tf_rebstrt, TextBackground );
	UxPutHeight( tf_rebstrt, 34 );
	UxPutWidth( tf_rebstrt, 96 );
	UxPutY( tf_rebstrt, 24 );
	UxPutX( tf_rebstrt, 201 );

	UxPutForeground( label22, TextForeground );
	UxPutAlignment( label22, "alignment_beginning" );
	UxPutLabelString( label22, "Final wavelength (A) :" );
	UxPutFontList( label22, TextFont );
	UxPutBackground( label22, LabelBackground );
	UxPutHeight( label22, 30 );
	UxPutWidth( label22, 156 );
	UxPutY( label22, 66 );
	UxPutX( label22, 24 );

	UxPutForeground( label23, TextForeground );
	UxPutAlignment( label23, "alignment_beginning" );
	UxPutLabelString( label23, "Wavelength step (A) :" );
	UxPutFontList( label23, TextFont );
	UxPutBackground( label23, LabelBackground );
	UxPutHeight( label23, 30 );
	UxPutWidth( label23, 158 );
	UxPutY( label23, 102 );
	UxPutX( label23, 24 );

	UxPutForeground( tf_rebend, TextForeground );
	UxPutHighlightOnEnter( tf_rebend, "true" );
	UxPutFontList( tf_rebend, TextFont );
	UxPutBackground( tf_rebend, TextBackground );
	UxPutHeight( tf_rebend, 34 );
	UxPutWidth( tf_rebend, 94 );
	UxPutY( tf_rebend, 62 );
	UxPutX( tf_rebend, 201 );

	UxPutIsAligned( rowColumn3, "true" );
	UxPutAdjustMargin( rowColumn3, "true" );
	UxPutAdjustLast( rowColumn3, "false" );
	UxPutEntryAlignment( rowColumn3, "alignment_beginning" );
	UxPutBorderWidth( rowColumn3, 0 );
	UxPutShadowThickness( rowColumn3, 0 );
	UxPutLabelString( rowColumn3, "" );
	UxPutEntryBorder( rowColumn3, 0 );
	UxPutBackground( rowColumn3, WindowBackground );
	UxPutRadioBehavior( rowColumn3, "true" );
	UxPutHeight( rowColumn3, 96 );
	UxPutWidth( rowColumn3, 106 );
	UxPutY( rowColumn3, 35 );
	UxPutX( rowColumn3, 327 );

	UxPutForeground( rb_rebmtd_line, TextForeground );
	UxPutIndicatorSize( rb_rebmtd_line, 16 );
	UxPutHighlightOnEnter( rb_rebmtd_line, "true" );
	UxPutSelectColor( rb_rebmtd_line, SelectColor );
	UxPutSet( rb_rebmtd_line, "true" );
	UxPutLabelString( rb_rebmtd_line, "LINEAR" );
	UxPutFontList( rb_rebmtd_line, TextFont );
	UxPutBackground( rb_rebmtd_line, WindowBackground );
	UxPutHeight( rb_rebmtd_line, 30 );
	UxPutWidth( rb_rebmtd_line, 148 );
	UxPutY( rb_rebmtd_line, 12 );
	UxPutX( rb_rebmtd_line, 12 );

	UxPutForeground( rb_rebmtd_quad, TextForeground );
	UxPutIndicatorSize( rb_rebmtd_quad, 16 );
	UxPutHighlightOnEnter( rb_rebmtd_quad, "true" );
	UxPutSelectColor( rb_rebmtd_quad, SelectColor );
	UxPutLabelString( rb_rebmtd_quad, "QUADRATIC" );
	UxPutFontList( rb_rebmtd_quad, TextFont );
	UxPutBackground( rb_rebmtd_quad, WindowBackground );
	UxPutHeight( rb_rebmtd_quad, 30 );
	UxPutWidth( rb_rebmtd_quad, 148 );
	UxPutY( rb_rebmtd_quad, 85 );
	UxPutX( rb_rebmtd_quad, 5 );

	UxPutForeground( rb_rebmtd_spli, TextForeground );
	UxPutIndicatorSize( rb_rebmtd_spli, 16 );
	UxPutHighlightOnEnter( rb_rebmtd_spli, "true" );
	UxPutSelectColor( rb_rebmtd_spli, SelectColor );
	UxPutLabelString( rb_rebmtd_spli, "SPLINE" );
	UxPutFontList( rb_rebmtd_spli, TextFont );
	UxPutBackground( rb_rebmtd_spli, WindowBackground );
	UxPutHeight( rb_rebmtd_spli, 24 );
	UxPutWidth( rb_rebmtd_spli, 96 );
	UxPutY( rb_rebmtd_spli, 66 );
	UxPutX( rb_rebmtd_spli, 4 );

	UxPutForeground( label24, TextForeground );
	UxPutAlignment( label24, "alignment_beginning" );
	UxPutLabelString( label24, "Rebinning method" );
	UxPutFontList( label24, TextFont );
	UxPutBackground( label24, LabelBackground );
	UxPutHeight( label24, 27 );
	UxPutWidth( label24, 126 );
	UxPutY( label24, 6 );
	UxPutX( label24, 326 );

	UxPutOrientation( separatorGadget11, "vertical" );
	UxPutHeight( separatorGadget11, 120 );
	UxPutWidth( separatorGadget11, 12 );
	UxPutY( separatorGadget11, 21 );
	UxPutX( separatorGadget11, 451 );

	UxPutOrientation( separatorGadget12, "vertical" );
	UxPutHeight( separatorGadget12, 120 );
	UxPutWidth( separatorGadget12, 12 );
	UxPutY( separatorGadget12, 21 );
	UxPutX( separatorGadget12, 315 );

	UxPutForeground( tf_rebstp, TextForeground );
	UxPutHighlightOnEnter( tf_rebstp, "true" );
	UxPutFontList( tf_rebstp, TextFont );
	UxPutBackground( tf_rebstp, TextBackground );
	UxPutHeight( tf_rebstp, 34 );
	UxPutWidth( tf_rebstp, 94 );
	UxPutY( tf_rebstp, 100 );
	UxPutX( tf_rebstp, 201 );

	UxPutBackground( form8, ButtonBackground );
	UxPutHeight( form8, 40 );
	UxPutWidth( form8, 490 );
	UxPutY( form8, 222 );
	UxPutX( form8, -2 );
	UxPutResizePolicy( form8, "resize_none" );

	UxPutLabelString( pb_rebin_rbr, "Rebin RBR..." );
	UxPutForeground( pb_rebin_rbr, ApplyForeground );
	UxPutFontList( pb_rebin_rbr, BoldTextFont );
	UxPutBackground( pb_rebin_rbr, ButtonBackground );
	UxPutHeight( pb_rebin_rbr, 30 );
	UxPutWidth( pb_rebin_rbr, 100 );
	UxPutY( pb_rebin_rbr, 6 );
	UxPutX( pb_rebin_rbr, 4 );

	UxPutLabelString( pushButton13, "Cancel" );
	UxPutForeground( pushButton13, CancelForeground );
	UxPutFontList( pushButton13, BoldTextFont );
	UxPutBackground( pushButton13, ButtonBackground );
	UxPutHeight( pushButton13, 30 );
	UxPutWidth( pushButton13, 86 );
	UxPutY( pushButton13, 6 );
	UxPutX( pushButton13, 400 );

	UxPutLabelString( pb_rebin_plot, "Plot table" );
	UxPutForeground( pb_rebin_plot, ButtonForeground );
	UxPutFontList( pb_rebin_plot, BoldTextFont );
	UxPutBackground( pb_rebin_plot, ButtonBackground );
	UxPutHeight( pb_rebin_plot, 30 );
	UxPutWidth( pb_rebin_plot, 86 );
	UxPutY( pb_rebin_plot, 6 );
	UxPutX( pb_rebin_plot, 312 );

	UxPutLabelString( pb_rebin_table, "Rebin table..." );
	UxPutForeground( pb_rebin_table, ApplyForeground );
	UxPutFontList( pb_rebin_table, BoldTextFont );
	UxPutBackground( pb_rebin_table, ButtonBackground );
	UxPutHeight( pb_rebin_table, 30 );
	UxPutWidth( pb_rebin_table, 100 );
	UxPutY( pb_rebin_table, 6 );
	UxPutX( pb_rebin_table, 206 );

	UxPutLabelString( pb_rebin_2d, "Rebin 2D..." );
	UxPutForeground( pb_rebin_2d, ApplyForeground );
	UxPutFontList( pb_rebin_2d, BoldTextFont );
	UxPutBackground( pb_rebin_2d, ButtonBackground );
	UxPutHeight( pb_rebin_2d, 30 );
	UxPutWidth( pb_rebin_2d, 100 );
	UxPutY( pb_rebin_2d, 6 );
	UxPutX( pb_rebin_2d, 106 );

	UxPutFontList( shelp_rebin, TextFont );
	UxPutEditable( shelp_rebin, "false" );
	UxPutCursorPositionVisible( shelp_rebin, "false" );
	UxPutBackground( shelp_rebin, SHelpBackground );
	UxPutHeight( shelp_rebin, 50 );
	UxPutWidth( shelp_rebin, 484 );
	UxPutY( shelp_rebin, 162 );
	UxPutX( shelp_rebin, 0 );

	UxPutBackground( separator6, WindowBackground );
	UxPutHeight( separator6, 10 );
	UxPutWidth( separator6, 492 );
	UxPutY( separator6, 152 );
	UxPutX( separator6, -2 );

	UxPutBackground( separator7, WindowBackground );
	UxPutHeight( separator7, 10 );
	UxPutWidth( separator7, 492 );
	UxPutY( separator7, 210 );
	UxPutX( separator7, -4 );

	UxPutHeight( separatorGadget15, 10 );
	UxPutWidth( separatorGadget15, 138 );
	UxPutY( separatorGadget15, 135 );
	UxPutX( separatorGadget15, 319 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_RebinShell()
{
	/* Create the swidgets */

	RebinShell = UxCreateApplicationShell( "RebinShell", NO_PARENT );
	UxPutContext( RebinShell, UxRebinShellContext );

	form7 = UxCreateForm( "form7", RebinShell );
	label21 = UxCreateLabel( "label21", form7 );
	tf_rebstrt = UxCreateTextField( "tf_rebstrt", form7 );
	label22 = UxCreateLabel( "label22", form7 );
	label23 = UxCreateLabel( "label23", form7 );
	tf_rebend = UxCreateTextField( "tf_rebend", form7 );
	rowColumn3 = UxCreateRowColumn( "rowColumn3", form7 );
	rb_rebmtd_line = UxCreateToggleButton( "rb_rebmtd_line", rowColumn3 );
	rb_rebmtd_quad = UxCreateToggleButton( "rb_rebmtd_quad", rowColumn3 );
	rb_rebmtd_spli = UxCreateToggleButton( "rb_rebmtd_spli", rowColumn3 );
	label24 = UxCreateLabel( "label24", form7 );
	separatorGadget11 = UxCreateSeparatorGadget( "separatorGadget11", form7 );
	separatorGadget12 = UxCreateSeparatorGadget( "separatorGadget12", form7 );
	tf_rebstp = UxCreateTextField( "tf_rebstp", form7 );
	form8 = UxCreateForm( "form8", form7 );
	pb_rebin_rbr = UxCreatePushButton( "pb_rebin_rbr", form8 );
	pushButton13 = UxCreatePushButton( "pushButton13", form8 );
	pb_rebin_plot = UxCreatePushButton( "pb_rebin_plot", form8 );
	pb_rebin_table = UxCreatePushButton( "pb_rebin_table", form8 );
	pb_rebin_2d = UxCreatePushButton( "pb_rebin_2d", form8 );
	shelp_rebin = UxCreateText( "shelp_rebin", form7 );
	separator6 = UxCreateSeparator( "separator6", form7 );
	separator7 = UxCreateSeparator( "separator7", form7 );
	separatorGadget15 = UxCreateSeparatorGadget( "separatorGadget15", form7 );

	_Uxinit_RebinShell();

	/* Create the X widgets */

	UxCreateWidget( RebinShell );
	UxCreateWidget( form7 );
	UxCreateWidget( label21 );
	UxCreateWidget( tf_rebstrt );
	UxCreateWidget( label22 );
	UxCreateWidget( label23 );
	UxCreateWidget( tf_rebend );
	UxCreateWidget( rowColumn3 );
	UxCreateWidget( rb_rebmtd_line );
	UxCreateWidget( rb_rebmtd_quad );
	UxCreateWidget( rb_rebmtd_spli );
	UxCreateWidget( label24 );
	UxCreateWidget( separatorGadget11 );
	UxCreateWidget( separatorGadget12 );
	UxCreateWidget( tf_rebstp );
	UxCreateWidget( form8 );
	UxCreateWidget( pb_rebin_rbr );
	UxCreateWidget( pushButton13 );
	UxCreateWidget( pb_rebin_plot );
	UxCreateWidget( pb_rebin_table );
	UxCreateWidget( pb_rebin_2d );
	UxCreateWidget( shelp_rebin );
	UxCreateWidget( separator6 );
	UxCreateWidget( separator7 );
	UxCreateWidget( separatorGadget15 );

	UxAddCallback( tf_rebstrt, XmNlosingFocusCallback,
			losingFocusCB_tf_rebstrt,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( tf_rebend, XmNlosingFocusCallback,
			losingFocusCB_tf_rebend,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( rb_rebmtd_line, XmNvalueChangedCallback,
			valueChangedCB_rb_rebmtd_line,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( rb_rebmtd_quad, XmNvalueChangedCallback,
			valueChangedCB_rb_rebmtd_quad,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( rb_rebmtd_spli, XmNvalueChangedCallback,
			valueChangedCB_rb_rebmtd_spli,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( tf_rebstp, XmNlosingFocusCallback,
			losingFocusCB_tf_rebstp,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( pb_rebin_rbr, XmNactivateCallback,
			activateCB_pb_rebin_rbr,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( pushButton13, XmNactivateCallback,
			activateCB_pushButton13,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( pb_rebin_plot, XmNactivateCallback,
			activateCB_pb_rebin_plot,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( pb_rebin_table, XmNactivateCallback,
			activateCB_pb_rebin_table,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( pb_rebin_2d, XmNactivateCallback,
			activateCB_pb_rebin_2d,
			(XtPointer) UxRebinShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( RebinShell );

	return ( RebinShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_RebinShell()
{
	swidget                 rtrn;
	_UxCRebinShell          *UxContext;

	UxRebinShellContext = UxContext =
		(_UxCRebinShell *) UxMalloc( sizeof(_UxCRebinShell) );

	rtrn = _Uxbuild_RebinShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_RebinShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_RebinShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

