! UIMX ascii 2.0 key: 6784                                                      

*file_selection.class: applicationShell
*file_selection.parent: NO_PARENT
*file_selection.static: true
*file_selection.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*file_selection.ispecdecl:
*file_selection.funcdecl: swidget create_file_selection()\

*file_selection.funcname: create_file_selection
*file_selection.funcdef: "swidget", "<create_file_selection>(%)"
*file_selection.icode:
*file_selection.fcode: return(rtrn);\

*file_selection.auxdecl:
*file_selection.name: file_selection
*file_selection.x: 612
*file_selection.y: 388
*file_selection.width: 346
*file_selection.height: 320
*file_selection.geometry: "+100+100"
*file_selection.iconName: "List selection"
*file_selection.title: "List selection"
*file_selection.defaultFontList: SmallFont
*file_selection.keyboardFocusPolicy: "pointer"
*file_selection.background: ApplicBackground

*fb_file_selection.class: fileSelectionBox
*fb_file_selection.parent: file_selection
*fb_file_selection.static: true
*fb_file_selection.name: fb_file_selection
*fb_file_selection.resizePolicy: "resize_none"
*fb_file_selection.unitType: "pixels"
*fb_file_selection.width: 345
*fb_file_selection.height: 300
*fb_file_selection.background: WindowBackground
*fb_file_selection.okCallback: {\
char s[128];\
extern swidget TextFieldSwidget;\
extern int FillTextField;\
\
strcpy(s, UxGetTextString(UxThisWidget)); \
if ( FillTextField )\
    UxPutValue(TextFieldSwidget, s);\
UxPopdownInterface(UxThisWidget);\
}
*fb_file_selection.foreground: TextForeground
*fb_file_selection.cancelCallback: {\
extern swidget FileListInterface;\
UxPopdownInterface (FileListInterface);\
}
*fb_file_selection.buttonFontList: BoldTextFont
*fb_file_selection.textFontList: TextFont
*fb_file_selection.labelFontList: BoldTextFont
*fb_file_selection.minimizeButtons: "false"
*fb_file_selection.listVisibleItemCount: 3

