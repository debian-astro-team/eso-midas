! UIMX ascii 2.0 key: 2365                                                      

*file_dialog.class: applicationShell
*file_dialog.parent: NO_PARENT
*file_dialog.static: true
*file_dialog.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*file_dialog.ispecdecl:
*file_dialog.funcdecl: swidget create_file_dialog()\

*file_dialog.funcname: create_file_dialog
*file_dialog.funcdef: "swidget", "<create_file_dialog>(%)"
*file_dialog.icode:
*file_dialog.fcode: return(rtrn);\

*file_dialog.auxdecl:
*file_dialog.name: file_dialog
*file_dialog.x: 102
*file_dialog.y: 121
*file_dialog.width: 408
*file_dialog.height: 105
*file_dialog.title: ""
*file_dialog.keyboardFocusPolicy: "pointer"
*file_dialog.geometry: "+100+100"
*file_dialog.background: WindowBackground

*form12.class: form
*form12.parent: file_dialog
*form12.static: true
*form12.name: form12
*form12.resizePolicy: "resize_none"
*form12.unitType: "pixels"
*form12.x: 0
*form12.y: 0
*form12.width: 408
*form12.height: 348
*form12.background: WindowBackground

*form13.class: form
*form13.parent: form12
*form13.static: true
*form13.name: form13
*form13.resizePolicy: "resize_none"
*form13.x: 0
*form13.y: 62
*form13.width: 452
*form13.height: 40
*form13.background: ButtonBackground

*pushButton25.class: pushButton
*pushButton25.parent: form13
*pushButton25.static: true
*pushButton25.name: pushButton25
*pushButton25.x: 8
*pushButton25.y: 4
*pushButton25.width: 80
*pushButton25.height: 30
*pushButton25.background: ButtonBackground
*pushButton25.fontList: BoldTextFont
*pushButton25.foreground: ApplyForeground
*pushButton25.labelString: "Ok"
*pushButton25.activateCallback: {\
CallbackDialog();\
}

*pushButton26.class: pushButton
*pushButton26.parent: form13
*pushButton26.static: true
*pushButton26.name: pushButton26
*pushButton26.x: 100
*pushButton26.y: 4
*pushButton26.width: 80
*pushButton26.height: 30
*pushButton26.background: ButtonBackground
*pushButton26.fontList: BoldTextFont
*pushButton26.foreground: CancelForeground
*pushButton26.labelString: "Cancel"
*pushButton26.activateCallback: {\
UxPopdownInterface(UxFindSwidget("file_dialog"));\
}

*lb_file_dialog.class: label
*lb_file_dialog.parent: form12
*lb_file_dialog.static: true
*lb_file_dialog.name: lb_file_dialog
*lb_file_dialog.x: 10
*lb_file_dialog.y: 12
*lb_file_dialog.width: 176
*lb_file_dialog.height: 30
*lb_file_dialog.background: LabelBackground
*lb_file_dialog.fontList: TextFont
*lb_file_dialog.labelString: "Output parameters file :"
*lb_file_dialog.alignment: "alignment_beginning"
*lb_file_dialog.foreground: TextForeground

*tf_file_dialog.class: textField
*tf_file_dialog.parent: form12
*tf_file_dialog.static: true
*tf_file_dialog.name: tf_file_dialog
*tf_file_dialog.x: 190
*tf_file_dialog.y: 10
*tf_file_dialog.width: 210
*tf_file_dialog.height: 34
*tf_file_dialog.background: TextBackground
*tf_file_dialog.fontList: TextFont
*tf_file_dialog.highlightOnEnter: "true"
*tf_file_dialog.foreground: TextForeground
*tf_file_dialog.losingFocusCallback: {\
char *text;\
extern char Session[];\
\
text = XmTextGetString(UxWidget);\
strcpy(Session, text);\
\
XtFree(text);\
}
*tf_file_dialog.activateCallback: {\
CallbackDialog();\
}

