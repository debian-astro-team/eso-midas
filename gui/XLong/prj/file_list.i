! UIMX ascii 2.0 key: 8485                                                      

*file_list.class: topLevelShell
*file_list.parent: NO_PARENT
*file_list.static: true
*file_list.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*file_list.ispecdecl:
*file_list.funcdecl: swidget create_file_list()\

*file_list.funcname: create_file_list
*file_list.funcdef: "swidget", "<create_file_list>(%)"
*file_list.icode:
*file_list.fcode: return(rtrn);\

*file_list.auxdecl:
*file_list.name: file_list
*file_list.x: 302
*file_list.y: 321
*file_list.width: 260
*file_list.height: 353
*file_list.iconName: "List selection"
*file_list.keyboardFocusPolicy: "pointer"
*file_list.background: "AntiqueWhite"
*file_list.geometry: "+100+100"
*file_list.title: "List selection"

*form6.class: form
*form6.parent: file_list
*form6.static: true
*form6.name: form6
*form6.resizePolicy: "resize_none"
*form6.unitType: "pixels"
*form6.x: 0
*form6.y: 0
*form6.width: 214
*form6.height: 352
*form6.background: ApplicBackground

*pushButton9.class: pushButton
*pushButton9.parent: form6
*pushButton9.static: true
*pushButton9.name: pushButton9
*pushButton9.x: 0
*pushButton9.y: 318
*pushButton9.width: 260
*pushButton9.height: 35
*pushButton9.background: ButtonBackground
*pushButton9.fontList: BoldTextFont
*pushButton9.labelString: "Cancel"
*pushButton9.activateCallback: {\
extern swidget FileListInterface;\
\
UxPopdownInterface(FileListInterface);\
}
*pushButton9.foreground: ButtonForeground

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form6
*scrolledWindow1.static: true
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "application_defined"
*scrolledWindow1.x: 21
*scrolledWindow1.y: 2
*scrolledWindow1.visualPolicy: "variable"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.shadowThickness: 0
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.background: ListBackground
*scrolledWindow1.height: 316
*scrolledWindow1.rightAttachment: "attach_form"
*scrolledWindow1.rightOffset: 2
*scrolledWindow1.leftAttachment: "attach_form"
*scrolledWindow1.leftOffset: 2
*scrolledWindow1.width: 270

*sl_file_list.class: scrolledList
*sl_file_list.parent: scrolledWindow1
*sl_file_list.static: true
*sl_file_list.name: sl_file_list
*sl_file_list.width: 191
*sl_file_list.height: 316
*sl_file_list.scrollBarDisplayPolicy: "static"
*sl_file_list.listSizePolicy: "variable"
*sl_file_list.background: TextBackground
*sl_file_list.fontList: SmallFont
*sl_file_list.visibleItemCount: 19
*sl_file_list.browseSelectionCallback: {\
char *choice;\
XmListCallbackStruct *cbs;\
\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
\
CallbackList(choice);\
\
}
*sl_file_list.createCallback: {\
extern Widget FileListWidget;\
\
FileListWidget = UxWidget;\
}
*sl_file_list.foreground: ListForeground

