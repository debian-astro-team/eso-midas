! UIMX ascii 2.0 key: 5240                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}

*translation.table: SelectFileCalib
*translation.parent: CalibShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*CalibShell.class: applicationShell
*CalibShell.parent: NO_PARENT
*CalibShell.static: true
*CalibShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*CalibShell.ispecdecl:
*CalibShell.funcdecl: swidget create_CalibShell()\

*CalibShell.funcname: create_CalibShell
*CalibShell.funcdef: "swidget", "<create_CalibShell>(%)"
*CalibShell.icode:
*CalibShell.fcode: return(rtrn);\

*CalibShell.auxdecl:
*CalibShell.name: CalibShell
*CalibShell.x: 449
*CalibShell.y: 367
*CalibShell.width: 490
*CalibShell.height: 387
*CalibShell.iconName: "Calibration"
*CalibShell.keyboardFocusPolicy: "pointer"
*CalibShell.defaultFontList: TextFont
*CalibShell.geometry: "+10+60"

*form3.class: form
*form3.parent: CalibShell
*form3.static: true
*form3.name: form3
*form3.resizePolicy: "resize_none"
*form3.unitType: "pixels"
*form3.x: 4
*form3.y: 4
*form3.width: 490
*form3.height: 548
*form3.background: "grey80"

*rowColumn2.class: rowColumn
*rowColumn2.parent: form3
*rowColumn2.static: true
*rowColumn2.name: rowColumn2
*rowColumn2.x: 344
*rowColumn2.y: 113
*rowColumn2.width: 82
*rowColumn2.height: 67
*rowColumn2.radioBehavior: "true"
*rowColumn2.background: "grey80"
*rowColumn2.entryBorder: 0
*rowColumn2.labelString: ""
*rowColumn2.shadowThickness: 0
*rowColumn2.borderWidth: 0
*rowColumn2.entryAlignment: "alignment_beginning"
*rowColumn2.isAligned: "true"

*rb_wlcmtd_iden.class: toggleButton
*rb_wlcmtd_iden.parent: rowColumn2
*rb_wlcmtd_iden.static: true
*rb_wlcmtd_iden.name: rb_wlcmtd_iden
*rb_wlcmtd_iden.x: 3
*rb_wlcmtd_iden.y: 2
*rb_wlcmtd_iden.width: 76
*rb_wlcmtd_iden.height: 20
*rb_wlcmtd_iden.background: WindowBackground
*rb_wlcmtd_iden.fontList: TextFont
*rb_wlcmtd_iden.labelString: "IDENT"
*rb_wlcmtd_iden.set: "true"
*rb_wlcmtd_iden.selectColor: SelectColor
*rb_wlcmtd_iden.foreground: TextForeground
*rb_wlcmtd_iden.highlightOnEnter: "true"
*rb_wlcmtd_iden.indicatorSize: 16

*rb_wlcmtd_gues.class: toggleButton
*rb_wlcmtd_gues.parent: rowColumn2
*rb_wlcmtd_gues.static: true
*rb_wlcmtd_gues.name: rb_wlcmtd_gues
*rb_wlcmtd_gues.x: 3
*rb_wlcmtd_gues.y: 34
*rb_wlcmtd_gues.width: 76
*rb_wlcmtd_gues.height: 66
*rb_wlcmtd_gues.background: WindowBackground
*rb_wlcmtd_gues.fontList: TextFont
*rb_wlcmtd_gues.labelString: "GUESS"
*rb_wlcmtd_gues.selectColor: SelectColor
*rb_wlcmtd_gues.foreground: TextForeground
*rb_wlcmtd_gues.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( XmToggleButtonGetState(UxWidget) ) { /*selected */\
    if ( UpdateToggle )\
        WriteKeyword("GUESS", K_WLCMTD);\
    XtSetSensitive(UxGetWidget(guess_session_label), TRUE);\
    XtSetSensitive(UxGetWidget(tf_guess), TRUE);\
/* later\
    XtSetSensitive(UxGetWidget(shift_label), TRUE);\
    XtSetSensitive(UxGetWidget(tf_shift), TRUE);\
*/\
    XtSetSensitive(UxGetWidget(tg_coropt), TRUE);\
}\
else {\
    if ( UpdateToggle )\
        WriteKeyword("IDENT", K_WLCMTD);\
    XtSetSensitive(UxGetWidget(guess_session_label), FALSE);\
    XtSetSensitive(UxGetWidget(tf_guess), FALSE);\
/* later\
    XtSetSensitive(UxGetWidget(shift_label), FALSE);\
    XtSetSensitive(UxGetWidget(tf_shift), FALSE);\
*/\
    XtSetSensitive(UxGetWidget(tg_coropt), FALSE);\
}\
\
}
*rb_wlcmtd_gues.set: "false"
*rb_wlcmtd_gues.highlightOnEnter: "true"
*rb_wlcmtd_gues.indicatorSize: 16

*label14.class: label
*label14.parent: form3
*label14.static: true
*label14.name: label14
*label14.x: 329
*label14.y: 94
*label14.width: 129
*label14.height: 18
*label14.background: WindowBackground
*label14.fontList: TextFont
*label14.labelString: "Calibration method"
*label14.alignment: "alignment_beginning"
*label14.foreground: TextForeground

*separatorGadget6.class: separatorGadget
*separatorGadget6.parent: form3
*separatorGadget6.static: true
*separatorGadget6.name: separatorGadget6
*separatorGadget6.x: 321
*separatorGadget6.y: 102
*separatorGadget6.width: 7
*separatorGadget6.height: 84
*separatorGadget6.orientation: "vertical"

*separatorGadget7.class: separatorGadget
*separatorGadget7.parent: form3
*separatorGadget7.static: true
*separatorGadget7.name: separatorGadget7
*separatorGadget7.x: 323
*separatorGadget7.y: 182
*separatorGadget7.width: 136
*separatorGadget7.height: 8

*separatorGadget8.class: separatorGadget
*separatorGadget8.parent: form3
*separatorGadget8.static: true
*separatorGadget8.name: separatorGadget8
*separatorGadget8.x: 457
*separatorGadget8.y: 102
*separatorGadget8.width: 8
*separatorGadget8.height: 84
*separatorGadget8.orientation: "vertical"

*label16.class: label
*label16.parent: form3
*label16.static: true
*label16.name: label16
*label16.x: 12
*label16.y: 12
*label16.width: 203
*label16.height: 30
*label16.background: LabelBackground
*label16.fontList: TextFont
*label16.labelString: "Tolerance :"
*label16.alignment: "alignment_beginning"
*label16.foreground: TextForeground

*tf_tol.class: textField
*tf_tol.parent: form3
*tf_tol.static: true
*tf_tol.name: tf_tol
*tf_tol.x: 216
*tf_tol.y: 6
*tf_tol.width: 82
*tf_tol.height: 34
*tf_tol.background: TextBackground
*tf_tol.fontList: TextFont
*tf_tol.highlightOnEnter: "true"
*tf_tol.foreground: TextForeground
*tf_tol.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text, newtext[32];\
float val;\
extern float Tol;\
extern int TolPixels;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Tol ) {\
    Tol = val;\
    sprintf(newtext, "%f", (TolPixels ? val : -val));\
    WriteKeyword(newtext, K_TOL);\
}\
\
XtFree(text);\
}

*label17.class: label
*label17.parent: form3
*label17.static: true
*label17.name: label17
*label17.x: 12
*label17.y: 50
*label17.width: 198
*label17.height: 30
*label17.background: LabelBackground
*label17.fontList: TextFont
*label17.labelString: "Degree of polynomial (x, y) :"
*label17.alignment: "alignment_beginning"
*label17.foreground: TextForeground

*label18.class: label
*label18.parent: form3
*label18.static: true
*label18.name: label18
*label18.x: 12
*label18.y: 88
*label18.width: 200
*label18.height: 30
*label18.background: LabelBackground
*label18.fontList: TextFont
*label18.labelString: "Min. and Max. number of iter. :"
*label18.alignment: "alignment_beginning"
*label18.foreground: TextForeground

*tf_dcx1.class: textField
*tf_dcx1.parent: form3
*tf_dcx1.static: true
*tf_dcx1.name: tf_dcx1
*tf_dcx1.x: 216
*tf_dcx1.y: 44
*tf_dcx1.width: 44
*tf_dcx1.height: 34
*tf_dcx1.background: TextBackground
*tf_dcx1.fontList: TextFont
*tf_dcx1.highlightOnEnter: "true"
*tf_dcx1.foreground: TextForeground
*tf_dcx1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Dcx[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Dcx[0] ) {\
    Dcx[0] = val;\
    WriteKeyword(text, K_DCX1);\
}\
\
XtFree(text);\
}

*tf_alpha.class: textField
*tf_alpha.parent: form3
*tf_alpha.static: true
*tf_alpha.name: tf_alpha
*tf_alpha.x: 216
*tf_alpha.y: 120
*tf_alpha.width: 82
*tf_alpha.height: 34
*tf_alpha.background: TextBackground
*tf_alpha.fontList: TextFont
*tf_alpha.highlightOnEnter: "true"
*tf_alpha.foreground: TextForeground
*tf_alpha.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Alpha;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Alpha ) {\
    Alpha = val;\
    WriteKeyword(text, K_ALPHA);\
}\
\
XtFree(text);\
\
}

*label19.class: label
*label19.parent: form3
*label19.static: true
*label19.name: label19
*label19.x: 12
*label19.y: 126
*label19.width: 205
*label19.height: 30
*label19.background: LabelBackground
*label19.fontList: TextFont
*label19.labelString: "Matching parameter :"
*label19.alignment: "alignment_beginning"
*label19.foreground: TextForeground

*label20.class: label
*label20.parent: form3
*label20.static: true
*label20.name: label20
*label20.x: 12
*label20.y: 166
*label20.width: 205
*label20.height: 30
*label20.background: LabelBackground
*label20.fontList: TextFont
*label20.labelString: "Maximum deviation (pixels) :"
*label20.alignment: "alignment_beginning"
*label20.foreground: TextForeground

*tf_maxdev.class: textField
*tf_maxdev.parent: form3
*tf_maxdev.static: true
*tf_maxdev.name: tf_maxdev
*tf_maxdev.x: 216
*tf_maxdev.y: 158
*tf_maxdev.width: 82
*tf_maxdev.height: 34
*tf_maxdev.background: TextBackground
*tf_maxdev.fontList: TextFont
*tf_maxdev.highlightOnEnter: "true"
*tf_maxdev.foreground: TextForeground
*tf_maxdev.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Maxdev;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Maxdev ) {\
    Maxdev = val;\
    WriteKeyword(text, K_MAXDEV);\
}\
\
XtFree(text);\
\
}

*guess_session_label.class: label
*guess_session_label.parent: form3
*guess_session_label.static: true
*guess_session_label.name: guess_session_label
*guess_session_label.x: 12
*guess_session_label.y: 204
*guess_session_label.width: 90
*guess_session_label.height: 30
*guess_session_label.background: LabelBackground
*guess_session_label.fontList: TextFont
*guess_session_label.labelString: "Initial guess :"
*guess_session_label.alignment: "alignment_beginning"
*guess_session_label.foreground: TextForeground
*guess_session_label.sensitive: "false"

*tf_guess.class: textField
*tf_guess.parent: form3
*tf_guess.static: true
*tf_guess.name: tf_guess
*tf_guess.x: 104
*tf_guess.y: 200
*tf_guess.width: 170
*tf_guess.height: 40
*tf_guess.background: TextBackground
*tf_guess.fontList: TextFont
*tf_guess.highlightOnEnter: "true"
*tf_guess.foreground: TextForeground
*tf_guess.sensitive: "false"
*tf_guess.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Guess[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Guess) ) {\
    strcpy(Guess, text);\
    WriteKeyword(text, K_GUESS);\
}\
\
XtFree(text);\
}
*tf_guess.translations: SelectFileCalib

*separator4.class: separator
*separator4.parent: form3
*separator4.static: true
*separator4.name: separator4
*separator4.x: 0
*separator4.y: 238
*separator4.width: 492
*separator4.height: 10
*separator4.background: WindowBackground

*shelp_calib.class: text
*shelp_calib.parent: form3
*shelp_calib.static: true
*shelp_calib.name: shelp_calib
*shelp_calib.x: 0
*shelp_calib.y: 248
*shelp_calib.width: 490
*shelp_calib.height: 50
*shelp_calib.background: SHelpBackground
*shelp_calib.cursorPositionVisible: "false"
*shelp_calib.editable: "false"
*shelp_calib.fontList: TextFont

*separator5.class: separator
*separator5.parent: form3
*separator5.static: true
*separator5.name: separator5
*separator5.x: -4
*separator5.y: 306
*separator5.width: 496
*separator5.height: 6
*separator5.background: WindowBackground

*tf_dcx2.class: textField
*tf_dcx2.parent: form3
*tf_dcx2.static: true
*tf_dcx2.name: tf_dcx2
*tf_dcx2.x: 260
*tf_dcx2.y: 44
*tf_dcx2.width: 44
*tf_dcx2.height: 34
*tf_dcx2.background: TextBackground
*tf_dcx2.fontList: TextFont
*tf_dcx2.highlightOnEnter: "true"
*tf_dcx2.foreground: TextForeground
*tf_dcx2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Dcx[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Dcx[1] ) {\
    Dcx[1] = val;\
    WriteKeyword(text, K_DCX2);\
}\
\
XtFree(text);\
}

*tf_wlcniter1.class: textField
*tf_wlcniter1.parent: form3
*tf_wlcniter1.static: true
*tf_wlcniter1.name: tf_wlcniter1
*tf_wlcniter1.x: 216
*tf_wlcniter1.y: 82
*tf_wlcniter1.width: 44
*tf_wlcniter1.height: 34
*tf_wlcniter1.background: TextBackground
*tf_wlcniter1.fontList: TextFont
*tf_wlcniter1.highlightOnEnter: "true"
*tf_wlcniter1.foreground: TextForeground
*tf_wlcniter1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Wlcniter[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Wlcniter[0] ) {\
    Wlcniter[0] = val;\
    WriteKeyword(text, K_WLCNITER1);\
}\
\
XtFree(text);\
}

*tf_wlcniter2.class: textField
*tf_wlcniter2.parent: form3
*tf_wlcniter2.static: true
*tf_wlcniter2.name: tf_wlcniter2
*tf_wlcniter2.x: 260
*tf_wlcniter2.y: 82
*tf_wlcniter2.width: 44
*tf_wlcniter2.height: 34
*tf_wlcniter2.background: TextBackground
*tf_wlcniter2.fontList: TextFont
*tf_wlcniter2.highlightOnEnter: "true"
*tf_wlcniter2.foreground: TextForeground
*tf_wlcniter2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Wlcniter[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Wlcniter[1] ) {\
    Wlcniter[1] = val;\
    WriteKeyword(text, K_WLCNITER2);\
}\
\
XtFree(text);\
}

*mn_tol.class: rowColumn
*mn_tol.parent: form3
*mn_tol.static: true
*mn_tol.name: mn_tol
*mn_tol.rowColumnType: "menu_option"
*mn_tol.subMenuId: "menu2_p1"
*mn_tol.x: 296
*mn_tol.y: 8
*mn_tol.background: WindowBackground
*mn_tol.foreground: TextForeground
*mn_tol.marginWidth: 0
*mn_tol.spacing: 0
*mn_tol.labelString: " "

*menu2_p1.class: rowColumn
*menu2_p1.parent: mn_tol
*menu2_p1.static: true
*menu2_p1.name: menu2_p1
*menu2_p1.rowColumnType: "menu_pulldown"
*menu2_p1.background: WindowBackground
*menu2_p1.foreground: TextForeground

*mn_tol_angstroms.class: pushButtonGadget
*mn_tol_angstroms.parent: menu2_p1
*mn_tol_angstroms.static: true
*mn_tol_angstroms.name: mn_tol_angstroms
*mn_tol_angstroms.labelString: " Angstroms "
*mn_tol_angstroms.activateCallback: {\
#include <spec_comm.h>\
\
char text[32];\
extern float Tol;\
extern int TolPixels;\
\
TolPixels = FALSE;\
\
sprintf(text, "%f", -Tol);\
WriteKeyword(text, K_TOL);\
\
XtFree(text);\
}
*mn_tol_angstroms.fontList: TextFont

*mn_tol_pixels.class: pushButtonGadget
*mn_tol_pixels.parent: menu2_p1
*mn_tol_pixels.static: true
*mn_tol_pixels.name: mn_tol_pixels
*mn_tol_pixels.labelString: " Pixels "
*mn_tol_pixels.activateCallback: {\
#include <spec_comm.h>\
\
char text[32];\
extern float Tol;\
extern int TolPixels;\
\
TolPixels = TRUE;\
\
sprintf(text, "%f", Tol);\
WriteKeyword(text, K_TOL);\
\
XtFree(text);\
}
*mn_tol_pixels.fontList: TextFont

*tg_twodopt.class: toggleButton
*tg_twodopt.parent: form3
*tg_twodopt.static: true
*tg_twodopt.name: tg_twodopt
*tg_twodopt.x: 306
*tg_twodopt.y: 46
*tg_twodopt.width: 172
*tg_twodopt.height: 31
*tg_twodopt.background: WindowBackground
*tg_twodopt.fontList: TextFont
*tg_twodopt.labelString: "Compute 2-D solution"
*tg_twodopt.set: "false"
*tg_twodopt.selectColor: SelectColor
*tg_twodopt.foreground: TextForeground
*tg_twodopt.highlightOnEnter: "true"
*tg_twodopt.sensitive: "true"
*tg_twodopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_TWODOPT);\
else\
    WriteKeyword("NO", K_TWODOPT);\
}
*tg_twodopt.indicatorSize: 16

*tg_coropt.class: toggleButton
*tg_coropt.parent: form3
*tg_coropt.static: true
*tg_coropt.name: tg_coropt
*tg_coropt.x: 282
*tg_coropt.y: 204
*tg_coropt.width: 206
*tg_coropt.height: 26
*tg_coropt.background: WindowBackground
*tg_coropt.fontList: TextFont
*tg_coropt.labelString: "Compute Cross-correlation"
*tg_coropt.set: "false"
*tg_coropt.selectColor: SelectColor
*tg_coropt.foreground: TextForeground
*tg_coropt.highlightOnEnter: "true"
*tg_coropt.sensitive: "false"
*tg_coropt.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( !UpdateToggle )\
    return;\
\
if ( XmToggleButtonGetState(UxWidget) )\
    AppendDialogText("set/long CORVISU=YES COROPT=YES");\
else\
    AppendDialogText("set/long CORVISU=NO COROPT=NO");\
}
*tg_coropt.indicatorSize: 16

*form20.class: form
*form20.parent: form3
*form20.static: true
*form20.name: form20
*form20.resizePolicy: "resize_none"
*form20.x: 0
*form20.y: 310
*form20.width: 490
*form20.height: 76
*form20.background: ButtonBackground

*pb_calib_calibrate.class: pushButton
*pb_calib_calibrate.parent: form20
*pb_calib_calibrate.static: true
*pb_calib_calibrate.name: pb_calib_calibrate
*pb_calib_calibrate.x: 10
*pb_calib_calibrate.y: 6
*pb_calib_calibrate.width: 86
*pb_calib_calibrate.height: 30
*pb_calib_calibrate.background: ButtonBackground
*pb_calib_calibrate.fontList: BoldTextFont
*pb_calib_calibrate.foreground: ApplyForeground
*pb_calib_calibrate.labelString: "Calibrate"
*pb_calib_calibrate.activateCallback: {\
#include <spec_comm.h>\
\
extern char Plotmode[];\
\
AppendDialogText(C_SELINE);\
AppendDialogText(C_CALIB_RBR);\
AppendDialogText(C_PLOT_DELTA);\
strcpy(Plotmode, C_PLOT_DELTA);\
UpdateRebinParameters();\
}

*pushButton40.class: pushButton
*pushButton40.parent: form20
*pushButton40.static: true
*pushButton40.name: pushButton40
*pushButton40.x: 384
*pushButton40.y: 40
*pushButton40.width: 86
*pushButton40.height: 30
*pushButton40.background: ButtonBackground
*pushButton40.fontList: BoldTextFont
*pushButton40.foreground: CancelForeground
*pushButton40.labelString: "Cancel"
*pushButton40.activateCallback: {\
AppendDialogText(C_SELCAT_ALL);\
UxPopdownInterface(UxFindSwidget("CalibShell"));\
}

*pb_calib_disper.class: pushButton
*pb_calib_disper.parent: form20
*pb_calib_disper.static: true
*pb_calib_disper.name: pb_calib_disper
*pb_calib_disper.x: 10
*pb_calib_disper.y: 40
*pb_calib_disper.width: 86
*pb_calib_disper.height: 30
*pb_calib_disper.background: ButtonBackground
*pb_calib_disper.fontList: BoldTextFont
*pb_calib_disper.foreground: ButtonForeground
*pb_calib_disper.labelString: "Dispersion"
*pb_calib_disper.activateCallback: {\
#include <spec_comm.h>\
\
extern char Plotmode[];\
\
AppendDialogText(C_PLOT_DELTA);\
strcpy(Plotmode, C_PLOT_DELTA);\
}

*pb_calib_resid.class: pushButton
*pb_calib_resid.parent: form20
*pb_calib_resid.static: true
*pb_calib_resid.name: pb_calib_resid
*pb_calib_resid.x: 102
*pb_calib_resid.y: 40
*pb_calib_resid.width: 86
*pb_calib_resid.height: 30
*pb_calib_resid.background: ButtonBackground
*pb_calib_resid.fontList: BoldTextFont
*pb_calib_resid.foreground: ButtonForeground
*pb_calib_resid.labelString: "Residuals"
*pb_calib_resid.activateCallback: {\
extern int Ystart;\
char str[20];\
\
sprintf(str, "%d", Ystart);\
XmTextSetString(UxGetWidget(UxFindSwidget("tf_residual")), str);\
UxPopupInterface(UxFindSwidget("resid_dialog"), exclusive_grab);\
}

*pb_calib_spec.class: pushButton
*pb_calib_spec.parent: form20
*pb_calib_spec.static: true
*pb_calib_spec.name: pb_calib_spec
*pb_calib_spec.x: 196
*pb_calib_spec.y: 40
*pb_calib_spec.width: 86
*pb_calib_spec.height: 30
*pb_calib_spec.background: ButtonBackground
*pb_calib_spec.fontList: BoldTextFont
*pb_calib_spec.foreground: ButtonForeground
*pb_calib_spec.labelString: "Spectrum"
*pb_calib_spec.activateCallback: {\
#include <spec_comm.h>\
\
extern char Plotmode[];\
\
AppendDialogText(C_PLOT_CALIB);\
strcpy(Plotmode, C_PLOT_CALIB);\
}

*pb_calib_edit.class: pushButton
*pb_calib_edit.parent: form20
*pb_calib_edit.static: true
*pb_calib_edit.name: pb_calib_edit
*pb_calib_edit.x: 102
*pb_calib_edit.y: 6
*pb_calib_edit.width: 86
*pb_calib_edit.height: 30
*pb_calib_edit.background: ButtonBackground
*pb_calib_edit.fontList: BoldTextFont
*pb_calib_edit.foreground: ApplyForeground
*pb_calib_edit.labelString: "Edit"
*pb_calib_edit.activateCallback: {\
#include <spec_comm.h>\
\
if ( !strcmp(Plotmode, C_PLOT_DELTA) )  \
    AppendDialogText(C_CALIB_EDIT_D);\
else if ( !strcmp(Plotmode, C_PLOT_CALIB) )  \
    AppendDialogText(C_CALIB_EDIT_C);\
else {\
    SCTPUT("** Edition can only be done in the dispersion or the spectrum plot.");\
    return;\
}\
\
AppendDialogText(C_CALIB_ERAS);\
UpdateRebinParameters();\
}

*pb_calib_shape.class: pushButton
*pb_calib_shape.parent: form20
*pb_calib_shape.static: true
*pb_calib_shape.name: pb_calib_shape
*pb_calib_shape.x: 290
*pb_calib_shape.y: 40
*pb_calib_shape.width: 86
*pb_calib_shape.height: 30
*pb_calib_shape.background: ButtonBackground
*pb_calib_shape.fontList: BoldTextFont
*pb_calib_shape.foreground: ButtonForeground
*pb_calib_shape.labelString: "Line shape"
*pb_calib_shape.activateCallback: {\
if ( !read_lincat_table() ) {\
    SCTPUT( "Error: cannot read Line Catalog Table");\
    return;\
}\
display_lincat_table(UxGetWidget(UxFindSwidget("sl_lincat_list")));\
UxPopupInterface(UxFindSwidget("lincat_list"), exclusive_grab);\
}

*pb_calib_twice.class: pushButton
*pb_calib_twice.parent: form20
*pb_calib_twice.static: true
*pb_calib_twice.name: pb_calib_twice
*pb_calib_twice.x: 384
*pb_calib_twice.y: 6
*pb_calib_twice.width: 86
*pb_calib_twice.height: 30
*pb_calib_twice.background: ButtonBackground
*pb_calib_twice.fontList: BoldTextFont
*pb_calib_twice.foreground: ApplyForeground
*pb_calib_twice.labelString: "Calib twice"
*pb_calib_twice.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_SELLIN_ALL);\
AppendDialogText(C_CALIB_RBR2);\
UpdateRebinParameters();\
}

*pb_calib_all.class: pushButton
*pb_calib_all.parent: form20
*pb_calib_all.static: true
*pb_calib_all.name: pb_calib_all
*pb_calib_all.x: 290
*pb_calib_all.y: 6
*pb_calib_all.width: 86
*pb_calib_all.height: 30
*pb_calib_all.background: ButtonBackground
*pb_calib_all.fontList: BoldTextFont
*pb_calib_all.foreground: ApplyForeground
*pb_calib_all.labelString: "Calib all"
*pb_calib_all.activateCallback: {\
#include <spec_comm.h>\
AppendDialogText(C_SELLIN_ALL);\
AppendDialogText(C_CALIB_RBR);\
UpdateRebinParameters();\
}

*pb_calib_getcur.class: pushButton
*pb_calib_getcur.parent: form20
*pb_calib_getcur.static: true
*pb_calib_getcur.name: pb_calib_getcur
*pb_calib_getcur.x: 196
*pb_calib_getcur.y: 6
*pb_calib_getcur.width: 86
*pb_calib_getcur.height: 30
*pb_calib_getcur.background: ButtonBackground
*pb_calib_getcur.fontList: BoldTextFont
*pb_calib_getcur.foreground: ApplyForeground
*pb_calib_getcur.labelString: "Get cursor"
*pb_calib_getcur.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_CALIB_CURS);\
\
}

