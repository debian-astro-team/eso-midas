! UIMX ascii 2.0 key: 8738                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("shelp_main"), "");\
UxPutText(UxFindSwidget("shelp_search"), "");\
UxPutText(UxFindSwidget("shelp_calib"), "");\
UxPutText(UxFindSwidget("shelp_rebin"), "");\
UxPutText(UxFindSwidget("shelp_extract"), "");\
UxPutText(UxFindSwidget("shelp_flux"), "");\
}
*action.WriteHelp: {\
DisplayShortHelp(UxWidget);\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}

*translation.table: SelectFileMain
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: true
*ApplicWindow.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget create_ApplicWindow()\

*ApplicWindow.funcname: create_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<create_ApplicWindow>(%)"
*ApplicWindow.icode: extern swidget create_SearchShell();\
extern swidget create_CalibShell();\
extern swidget create_RebinShell();\
extern swidget create_FluxShell();\
extern swidget create_file_dialog();\
extern swidget create_extin_dialog();\
extern swidget create_resid_dialog();\
extern swidget create_ExtractShell();\
extern swidget create_HelpShell();\
\
\

*ApplicWindow.fcode: create_SearchShell();\
create_CalibShell();\
create_RebinShell();\
create_FluxShell();\
create_file_dialog();\
create_extin_dialog();\
create_resid_dialog();\
create_ExtractShell();\
create_HelpShell();\
\
return(rtrn);\

*ApplicWindow.auxdecl:
*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 2
*ApplicWindow.y: 21
*ApplicWindow.width: 447
*ApplicWindow.height: 460
*ApplicWindow.iconName: "Long Slit"
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.title: "Long slit reduction"
*ApplicWindow.geometry: "+0+0"

*MainWindow.class: mainWindow
*MainWindow.parent: ApplicWindow
*MainWindow.static: true
*MainWindow.name: MainWindow
*MainWindow.unitType: "pixels"
*MainWindow.x: 10
*MainWindow.y: 0
*MainWindow.width: 440
*MainWindow.height: 460
*MainWindow.background: WindowBackground

*menu1.class: rowColumn
*menu1.parent: MainWindow
*menu1.static: true
*menu1.name: menu1
*menu1.rowColumnType: "menu_bar"
*menu1.menuAccelerator: "<KeyUp>F10"
*menu1.menuHelpWidget: "menu1_top_b5"
*menu1.background: MenuBackground

*menu1_p1.class: rowColumn
*menu1_p1.parent: menu1
*menu1_p1.static: true
*menu1_p1.name: menu1_p1
*menu1_p1.rowColumnType: "menu_pulldown"
*menu1_p1.background: MenuBackground
*menu1_p1.foreground: MenuForeground

*menu1_p1_b1.class: pushButtonGadget
*menu1_p1_b1.parent: menu1_p1
*menu1_p1_b1.static: true
*menu1_p1_b1.name: menu1_p1_b1
*menu1_p1_b1.labelString: "Open"
*menu1_p1_b1.mnemonic: "O"
*menu1_p1_b1.fontList: BoldTextFont
*menu1_p1_b1.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_SESSION);\
}

*menu1_p1_b2.class: pushButtonGadget
*menu1_p1_b2.parent: menu1_p1
*menu1_p1_b2.static: true
*menu1_p1_b2.name: menu1_p1_b2
*menu1_p1_b2.labelString: "Save"
*menu1_p1_b2.mnemonic: "S"
*menu1_p1_b2.fontList: BoldTextFont
*menu1_p1_b2.activateCallback: {\
#include <spec_comm.h>\
\
extern char Session[];\
char command[256];\
\
sprintf(command, "%s%s", C_SAVE, Session);\
AppendDialogText(command);\
}

*menu1_p1_b3.class: pushButtonGadget
*menu1_p1_b3.parent: menu1_p1
*menu1_p1_b3.static: true
*menu1_p1_b3.name: menu1_p1_b3
*menu1_p1_b3.labelString: "Save As ..."
*menu1_p1_b3.mnemonic: "S"
*menu1_p1_b3.fontList: BoldTextFont
*menu1_p1_b3.activateCallback: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern int DialogType;\
extern char Session[];\
\
SET_DIALOG_PROMPT("Output parameters table :");\
\
XmTextSetString(UxGetWidget(UxFindSwidget("tf_file_dialog")), Session);\
DialogType = DIALOG_SESSION;\
UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);\
}

*menu1_p1_b4.class: pushButtonGadget
*menu1_p1_b4.parent: menu1_p1
*menu1_p1_b4.static: true
*menu1_p1_b4.name: menu1_p1_b4
*menu1_p1_b4.labelString: "Exit"
*menu1_p1_b4.accelerator: "E"
*menu1_p1_b4.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_CLEAN);\
SCSEPI();\
exit(0);\
\
}
*menu1_p1_b4.fontList: BoldTextFont

*menu1_p4.class: rowColumn
*menu1_p4.parent: menu1
*menu1_p4.static: true
*menu1_p4.name: menu1_p4
*menu1_p4.rowColumnType: "menu_pulldown"
*menu1_p4.background: MenuBackground
*menu1_p4.foreground: MenuForeground

*menu1_p4_b1.class: pushButtonGadget
*menu1_p4_b1.parent: menu1_p4
*menu1_p4_b1.static: true
*menu1_p4_b1.name: menu1_p4_b1
*menu1_p4_b1.labelString: "Create Display"
*menu1_p4_b1.mnemonic: "D"
*menu1_p4_b1.fontList: BoldTextFont
*menu1_p4_b1.activateCallback: AppendDialogText("make/display");

*menu1_p4_b2.class: pushButtonGadget
*menu1_p4_b2.parent: menu1_p4
*menu1_p4_b2.static: true
*menu1_p4_b2.name: menu1_p4_b2
*menu1_p4_b2.labelString: "Create Graphics"
*menu1_p4_b2.mnemonic: "G"
*menu1_p4_b2.fontList: BoldTextFont
*menu1_p4_b2.activateCallback: AppendDialogText("graph/long");

*menu1_p4_b8.class: separatorGadget
*menu1_p4_b8.parent: menu1_p4
*menu1_p4_b8.static: true
*menu1_p4_b8.name: menu1_p4_b8

*menu1_p4_b10.class: pushButtonGadget
*menu1_p4_b10.parent: menu1_p4
*menu1_p4_b10.static: true
*menu1_p4_b10.name: menu1_p4_b10
*menu1_p4_b10.labelString: "Load image"
*menu1_p4_b10.activateCallback: {        \
#include <spec_comm.h>\
\
PopupList(LIST_LOAD_IMA);\
}
*menu1_p4_b10.fontList: BoldTextFont
*menu1_p4_b10.mnemonic: "L"

*menu1_p4_b13.class: pushButtonGadget
*menu1_p4_b13.parent: menu1_p4
*menu1_p4_b13.static: true
*menu1_p4_b13.name: menu1_p4_b13
*menu1_p4_b13.labelString: "Clear overlay"
*menu1_p4_b13.activateCallback: AppendDialogText("clear/channel over");
*menu1_p4_b13.fontList: BoldTextFont
*menu1_p4_b13.mnemonic: "o"

*menu1_p4_b12.class: separatorGadget
*menu1_p4_b12.parent: menu1_p4
*menu1_p4_b12.static: true
*menu1_p4_b12.name: menu1_p4_b12

*menu1_p4_b3.class: pushButtonGadget
*menu1_p4_b3.parent: menu1_p4
*menu1_p4_b3.static: true
*menu1_p4_b3.name: menu1_p4_b3
*menu1_p4_b3.labelString: "Print Display"
*menu1_p4_b3.mnemonic: "P"
*menu1_p4_b3.fontList: BoldTextFont
*menu1_p4_b3.activateCallback: AppendDialogText("copy/display laser");

*menu1_p4_b4.class: pushButtonGadget
*menu1_p4_b4.parent: menu1_p4
*menu1_p4_b4.static: true
*menu1_p4_b4.name: menu1_p4_b4
*menu1_p4_b4.labelString: "Print Graphics"
*menu1_p4_b4.mnemonic: "r"
*menu1_p4_b4.fontList: BoldTextFont
*menu1_p4_b4.activateCallback: AppendDialogText("copy/graph laser");

*menu1_p4_b9.class: separatorGadget
*menu1_p4_b9.parent: menu1_p4
*menu1_p4_b9.static: true
*menu1_p4_b9.name: menu1_p4_b9

*menu1_p4_b5.class: pushButtonGadget
*menu1_p4_b5.parent: menu1_p4
*menu1_p4_b5.static: true
*menu1_p4_b5.name: menu1_p4_b5
*menu1_p4_b5.labelString: "Graphics Cursor"
*menu1_p4_b5.mnemonic: "C"
*menu1_p4_b5.fontList: BoldTextFont
*menu1_p4_b5.activateCallback: AppendDialogText("get/gcursor");

*menu1_p4_b11.class: pushButtonGadget
*menu1_p4_b11.parent: menu1_p4
*menu1_p4_b11.static: true
*menu1_p4_b11.name: menu1_p4_b11
*menu1_p4_b11.labelString: "Display Cursor"
*menu1_p4_b11.mnemonic: "u"
*menu1_p4_b11.activateCallback: AppendDialogText("get/cursor");
*menu1_p4_b11.fontList: BoldTextFont

*menu1_p4_b14.class: separatorGadget
*menu1_p4_b14.parent: menu1_p4
*menu1_p4_b14.static: true
*menu1_p4_b14.name: menu1_p4_b14

*menu1_p4_b15.class: pushButtonGadget
*menu1_p4_b15.parent: menu1_p4
*menu1_p4_b15.static: true
*menu1_p4_b15.name: menu1_p4_b15
*menu1_p4_b15.labelString: "get MIDAS table"
*menu1_p4_b15.activateCallback: {\
#include <spec_comm.h>\
\
char midashome[256];\
\
osfphname("MIDASHOME", midashome);\
sprintf(DirSpecs, "%s/calib/data/", midashome);\
\
PopupList(LIST_BROWSER);\
}
*menu1_p4_b15.fontList: BoldTextFont

*menu1_p5.class: rowColumn
*menu1_p5.parent: menu1
*menu1_p5.static: true
*menu1_p5.name: menu1_p5
*menu1_p5.rowColumnType: "menu_pulldown"
*menu1_p5.background: MenuBackground
*menu1_p5.foreground: MenuForeground

*menu_help_context.class: pushButtonGadget
*menu_help_context.parent: menu1_p5
*menu_help_context.static: true
*menu_help_context.name: menu_help_context
*menu_help_context.labelString: "On Context ..."
*menu_help_context.mnemonic: "C"
*menu_help_context.fontList: BoldTextFont
*menu_help_context.activateCallback: DisplayExtendedHelp(UxWidget);

*menu_help_help.class: pushButtonGadget
*menu_help_help.parent: menu1_p5
*menu_help_help.static: true
*menu_help_help.name: menu_help_help
*menu_help_help.labelString: "On Help ..."
*menu_help_help.mnemonic: "H"
*menu_help_help.fontList: BoldTextFont
*menu_help_help.activateCallback: DisplayExtendedHelp(UxWidget);

*menu1_p5_b5.class: separatorGadget
*menu1_p5_b5.parent: menu1_p5
*menu1_p5_b5.static: true
*menu1_p5_b5.name: menu1_p5_b5

*menu_help_tutorial.class: pushButtonGadget
*menu_help_tutorial.parent: menu1_p5
*menu_help_tutorial.static: true
*menu_help_tutorial.name: menu_help_tutorial
*menu_help_tutorial.labelString: "Tutorial"
*menu_help_tutorial.mnemonic: "T"
*menu_help_tutorial.fontList: BoldTextFont
*menu_help_tutorial.activateCallback: DisplayExtendedHelp(UxWidget);

*menu_help_version.class: pushButtonGadget
*menu_help_version.parent: menu1_p5
*menu_help_version.static: true
*menu_help_version.name: menu_help_version
*menu_help_version.labelString: "On Version..."
*menu_help_version.mnemonic: "V"
*menu_help_version.fontList: BoldTextFont
*menu_help_version.activateCallback: DisplayExtendedHelp(UxWidget);

*menu1_top_b1.class: cascadeButton
*menu1_top_b1.parent: menu1
*menu1_top_b1.static: true
*menu1_top_b1.name: menu1_top_b1
*menu1_top_b1.labelString: "File"
*menu1_top_b1.mnemonic: "F"
*menu1_top_b1.subMenuId: "menu1_p1"
*menu1_top_b1.background: MenuBackground
*menu1_top_b1.fontList: BoldTextFont
*menu1_top_b1.foreground: MenuForeground

*menu1_top_b4.class: cascadeButton
*menu1_top_b4.parent: menu1
*menu1_top_b4.static: true
*menu1_top_b4.name: menu1_top_b4
*menu1_top_b4.labelString: "Utils"
*menu1_top_b4.mnemonic: "U"
*menu1_top_b4.subMenuId: "menu1_p4"
*menu1_top_b4.background: MenuBackground
*menu1_top_b4.fontList: BoldTextFont
*menu1_top_b4.foreground: MenuForeground

*menu1_top_b5.class: cascadeButton
*menu1_top_b5.parent: menu1
*menu1_top_b5.static: true
*menu1_top_b5.name: menu1_top_b5
*menu1_top_b5.labelString: "Help"
*menu1_top_b5.mnemonic: "H"
*menu1_top_b5.subMenuId: "menu1_p5"
*menu1_top_b5.background: MenuBackground
*menu1_top_b5.fontList: BoldTextFont
*menu1_top_b5.foreground: MenuForeground

*form1.class: form
*form1.parent: MainWindow
*form1.static: true
*form1.name: form1
*form1.background: WindowBackground

*form5.class: form
*form5.parent: form1
*form5.static: true
*form5.name: form5
*form5.resizePolicy: "resize_none"
*form5.x: -1
*form5.y: 368
*form5.width: 451
*form5.height: 72
*form5.background: ButtonBackground
*form5.bottomAttachment: "attach_form"

*pb_main_search.class: pushButton
*pb_main_search.parent: form5
*pb_main_search.static: true
*pb_main_search.name: pb_main_search
*pb_main_search.x: 4
*pb_main_search.y: 6
*pb_main_search.width: 86
*pb_main_search.height: 30
*pb_main_search.background: ButtonBackground
*pb_main_search.fontList: BoldTextFont
*pb_main_search.foreground: ButtonForeground
*pb_main_search.labelString: "Search ..."
*pb_main_search.activateCallback: {\
PopupLong("SearchShell");\
}

*pb_main_ident.class: pushButton
*pb_main_ident.parent: form5
*pb_main_ident.static: true
*pb_main_ident.name: pb_main_ident
*pb_main_ident.x: 94
*pb_main_ident.y: 6
*pb_main_ident.width: 86
*pb_main_ident.height: 30
*pb_main_ident.background: ButtonBackground
*pb_main_ident.fontList: BoldTextFont
*pb_main_ident.foreground: ButtonForeground
*pb_main_ident.labelString: "Identify ..."
*pb_main_ident.activateCallback: {\
{\
#include <spec_comm.h>\
\
extern char Lincat[], Wlc[];\
\
if ( !file_exists(Wlc, ".bdf") ) {\
    SCTPUT("*** Error: invalid calibration frame ***");\
    return;\
}\
if ( !file_exists(Lincat, ".tbl") ) {\
    SCTPUT("*** Error: invalid line catalog table ***");\
    return;\
}\
\
AppendDialogText(C_IDENT);\
}\
}

*pb_main_calib.class: pushButton
*pb_main_calib.parent: form5
*pb_main_calib.static: true
*pb_main_calib.name: pb_main_calib
*pb_main_calib.x: 182
*pb_main_calib.y: 6
*pb_main_calib.width: 86
*pb_main_calib.height: 30
*pb_main_calib.background: ButtonBackground
*pb_main_calib.fontList: BoldTextFont
*pb_main_calib.foreground: ButtonForeground
*pb_main_calib.labelString: "Calibrate ..."
*pb_main_calib.activateCallback: {\
{\
#include <spec_comm.h>\
\
AppendDialogText(C_CALIB_ERAS);\
PopupLong("CalibShell");\
}\
\
}

*pb_main_rebin.class: pushButton
*pb_main_rebin.parent: form5
*pb_main_rebin.static: true
*pb_main_rebin.name: pb_main_rebin
*pb_main_rebin.x: 270
*pb_main_rebin.y: 6
*pb_main_rebin.width: 86
*pb_main_rebin.height: 30
*pb_main_rebin.background: ButtonBackground
*pb_main_rebin.fontList: BoldTextFont
*pb_main_rebin.foreground: ButtonForeground
*pb_main_rebin.labelString: "Rebin ..."
*pb_main_rebin.activateCallback: {\
PopupLong("RebinShell");\
}

*pb_main_extract.class: pushButton
*pb_main_extract.parent: form5
*pb_main_extract.static: true
*pb_main_extract.name: pb_main_extract
*pb_main_extract.x: 358
*pb_main_extract.y: 6
*pb_main_extract.width: 86
*pb_main_extract.height: 30
*pb_main_extract.background: ButtonBackground
*pb_main_extract.fontList: BoldTextFont
*pb_main_extract.foreground: ButtonForeground
*pb_main_extract.labelString: "Extract ..."
*pb_main_extract.activateCallback: {\
PopupLong("ExtractShell");\
}

*pb_main_flux.class: pushButton
*pb_main_flux.parent: form5
*pb_main_flux.static: true
*pb_main_flux.name: pb_main_flux
*pb_main_flux.x: 4
*pb_main_flux.y: 38
*pb_main_flux.width: 86
*pb_main_flux.height: 30
*pb_main_flux.background: ButtonBackground
*pb_main_flux.fontList: BoldTextFont
*pb_main_flux.foreground: ButtonForeground
*pb_main_flux.labelString: "Flux ..."
*pb_main_flux.activateCallback: {\
PopupLong("FluxShell");\
}

*pb_main_batch.class: pushButton
*pb_main_batch.parent: form5
*pb_main_batch.static: true
*pb_main_batch.name: pb_main_batch
*pb_main_batch.x: 94
*pb_main_batch.y: 38
*pb_main_batch.width: 86
*pb_main_batch.height: 30
*pb_main_batch.background: ButtonBackground
*pb_main_batch.fontList: BoldTextFont
*pb_main_batch.foreground: ButtonForeground
*pb_main_batch.labelString: "Batch ..."
*pb_main_batch.activateCallback: {\
{\
#include <spec_comm.h>\
\
AppendDialogText(C_BATCHRED);\
}\
}

*separator12.class: separator
*separator12.parent: form1
*separator12.static: true
*separator12.name: separator12
*separator12.x: 0
*separator12.y: 360
*separator12.width: 450
*separator12.height: 6
*separator12.background: LabelBackground
*separator12.bottomAttachment: "attach_widget"
*separator12.bottomWidget: "form5"

*shelp_main.class: text
*shelp_main.parent: form1
*shelp_main.static: true
*shelp_main.name: shelp_main
*shelp_main.x: 0
*shelp_main.y: 310
*shelp_main.width: 448
*shelp_main.height: 50
*shelp_main.background: SHelpBackground
*shelp_main.cursorPositionVisible: "false"
*shelp_main.editable: "false"
*shelp_main.fontList: TextFont
*shelp_main.bottomAttachment: "attach_widget"
*shelp_main.bottomWidget: "separator12"

*separator1.class: separator
*separator1.parent: form1
*separator1.static: true
*separator1.name: separator1
*separator1.x: -2
*separator1.y: 304
*separator1.width: 450
*separator1.height: 6
*separator1.background: LabelBackground
*separator1.bottomAttachment: "attach_widget"
*separator1.bottomWidget: "shelp_main"

*tf_imin.class: textField
*tf_imin.parent: form1
*tf_imin.static: true
*tf_imin.name: tf_imin
*tf_imin.x: 204
*tf_imin.y: 268
*tf_imin.width: 68
*tf_imin.height: 37
*tf_imin.background: TextBackground
*tf_imin.fontList: TextFont
*tf_imin.highlightOnEnter: "true"
*tf_imin.foreground: TextForeground
*tf_imin.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Imin;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Imin ) {\
    Imin = val;\
    WriteKeyword(text, K_IMIN);\
}\
\
XtFree(text);\
}
*tf_imin.bottomAttachment: "attach_widget"
*tf_imin.bottomWidget: "separator1"

*label6.class: label
*label6.parent: form1
*label6.static: true
*label6.name: label6
*label6.x: 12
*label6.y: 270
*label6.width: 190
*label6.height: 30
*label6.background: LabelBackground
*label6.fontList: TextFont
*label6.labelString: "Minimal Intensity in Catalog :"
*label6.alignment: "alignment_beginning"
*label6.foreground: TextForeground
*label6.bottomAttachment: "attach_widget"
*label6.bottomWidget: "separator1"

*tf_wrang1.class: textField
*tf_wrang1.parent: form1
*tf_wrang1.static: true
*tf_wrang1.name: tf_wrang1
*tf_wrang1.x: 204
*tf_wrang1.y: 230
*tf_wrang1.width: 68
*tf_wrang1.height: 37
*tf_wrang1.background: TextBackground
*tf_wrang1.fontList: TextFont
*tf_wrang1.highlightOnEnter: "true"
*tf_wrang1.foreground: TextForeground
*tf_wrang1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Wrang[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Wrang[0] ) {\
    Wrang[0] = val;\
    WriteKeyword(text, K_WRANG1);\
}\
\
XtFree(text);\
}
*tf_wrang1.bottomAttachment: "attach_widget"
*tf_wrang1.bottomWidget: "tf_imin"

*label5.class: label
*label5.parent: form1
*label5.static: true
*label5.name: label5
*label5.x: 12
*label5.y: 232
*label5.width: 190
*label5.height: 30
*label5.background: LabelBackground
*label5.fontList: TextFont
*label5.labelString: "Wavelength Range :"
*label5.alignment: "alignment_beginning"
*label5.foreground: TextForeground
*label5.bottomAttachment: "attach_widget"
*label5.bottomWidget: "tf_imin"

*tf_wrang2.class: textField
*tf_wrang2.parent: form1
*tf_wrang2.static: true
*tf_wrang2.name: tf_wrang2
*tf_wrang2.x: 272
*tf_wrang2.y: 230
*tf_wrang2.width: 68
*tf_wrang2.height: 37
*tf_wrang2.background: TextBackground
*tf_wrang2.fontList: TextFont
*tf_wrang2.highlightOnEnter: "true"
*tf_wrang2.foreground: TextForeground
*tf_wrang2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Wrang[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Wrang[1] ) {\
    Wrang[1] = val;\
    WriteKeyword(text, K_WRANG2);\
}\
\
XtFree(text);\
}
*tf_wrang2.bottomAttachment: "attach_widget"
*tf_wrang2.bottomWidget: "tf_imin"

*tf_lincat.class: textField
*tf_lincat.parent: form1
*tf_lincat.static: true
*tf_lincat.name: tf_lincat
*tf_lincat.x: 204
*tf_lincat.y: 192
*tf_lincat.width: 236
*tf_lincat.height: 37
*tf_lincat.background: TextBackground
*tf_lincat.fontList: TextFont
*tf_lincat.highlightOnEnter: "true"
*tf_lincat.foreground: TextForeground
*tf_lincat.translations: SelectFileMain
*tf_lincat.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Lincat[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Lincat) ) {\
    strcpy(Lincat, text);\
    WriteKeyword(text, K_LINCAT);\
}\
\
XtFree(text);\
}
*tf_lincat.bottomAttachment: "attach_widget"
*tf_lincat.bottomWidget: "tf_wrang1"

*label4.class: label
*label4.parent: form1
*label4.static: true
*label4.name: label4
*label4.x: 12
*label4.y: 196
*label4.width: 190
*label4.height: 30
*label4.background: LabelBackground
*label4.fontList: TextFont
*label4.labelString: "Line Catalog :"
*label4.alignment: "alignment_beginning"
*label4.foreground: TextForeground
*label4.bottomAttachment: "attach_widget"
*label4.bottomWidget: "tf_wrang1"

*tf_ystart.class: textField
*tf_ystart.parent: form1
*tf_ystart.static: true
*tf_ystart.name: tf_ystart
*tf_ystart.x: 204
*tf_ystart.y: 156
*tf_ystart.width: 71
*tf_ystart.height: 37
*tf_ystart.background: TextBackground
*tf_ystart.fontList: TextFont
*tf_ystart.highlightOnEnter: "true"
*tf_ystart.foreground: TextForeground
*tf_ystart.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Ystart;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Ystart ) {\
    Ystart = val;\
    WriteKeyword(text, K_YSTART);\
}\
\
XtFree(text);\
}
*tf_ystart.bottomAttachment: "attach_widget"
*tf_ystart.bottomWidget: "tf_lincat"

*label3.class: label
*label3.parent: form1
*label3.static: true
*label3.name: label3
*label3.x: 12
*label3.y: 158
*label3.width: 190
*label3.height: 30
*label3.background: LabelBackground
*label3.fontList: TextFont
*label3.labelString: "Calibration Starting Row :"
*label3.alignment: "alignment_beginning"
*label3.foreground: TextForeground
*label3.bottomAttachment: "attach_widget"
*label3.bottomWidget: "tf_lincat"

*tf_instrume.class: textField
*tf_instrume.parent: form1
*tf_instrume.static: true
*tf_instrume.name: tf_instrume
*tf_instrume.x: 204
*tf_instrume.y: 118
*tf_instrume.width: 236
*tf_instrume.height: 37
*tf_instrume.background: TextBackground
*tf_instrume.fontList: TextFont
*tf_instrume.highlightOnEnter: "true"
*tf_instrume.foreground: TextForeground
*tf_instrume.borderWidth: 0
*tf_instrume.bottomAttachment: "attach_widget"
*tf_instrume.bottomWidget: "tf_ystart"

*label8.class: label
*label8.parent: form1
*label8.static: true
*label8.name: label8
*label8.x: 12
*label8.y: 122
*label8.width: 190
*label8.height: 30
*label8.background: LabelBackground
*label8.fontList: TextFont
*label8.labelString: "Instrument :"
*label8.alignment: "alignment_beginning"
*label8.foreground: TextForeground
*label8.bottomAttachment: "attach_widget"
*label8.bottomWidget: "tf_ystart"

*label1.class: label
*label1.parent: form1
*label1.static: true
*label1.name: label1
*label1.x: 12
*label1.y: 84
*label1.width: 190
*label1.height: 30
*label1.background: LabelBackground
*label1.fontList: TextFont
*label1.labelString: "Calibration Frame :"
*label1.alignment: "alignment_beginning"
*label1.foreground: TextForeground
*label1.bottomAttachment: "attach_widget"
*label1.bottomWidget: "tf_instrume"

*tf_wlc.class: textField
*tf_wlc.parent: form1
*tf_wlc.static: true
*tf_wlc.name: tf_wlc
*tf_wlc.x: 204
*tf_wlc.y: 80
*tf_wlc.width: 235
*tf_wlc.height: 37
*tf_wlc.background: TextBackground
*tf_wlc.fontList: TextFont
*tf_wlc.highlightOnEnter: "false"
*tf_wlc.foreground: TextForeground
*tf_wlc.cursorPositionVisible: "false"
*tf_wlc.editable: "false"
*tf_wlc.borderWidth: 1
*tf_wlc.bottomAttachment: "attach_widget"
*tf_wlc.bottomWidget: "tf_instrume"

*tf_session.class: textField
*tf_session.parent: form1
*tf_session.static: true
*tf_session.name: tf_session
*tf_session.x: 204
*tf_session.y: 42
*tf_session.width: 235
*tf_session.height: 37
*tf_session.background: TextBackground
*tf_session.fontList: TextFont
*tf_session.highlightOnEnter: "false"
*tf_session.foreground: TextForeground
*tf_session.cursorPositionVisible: "false"
*tf_session.editable: "false"
*tf_session.borderWidth: 1
*tf_session.bottomAttachment: "attach_widget"
*tf_session.bottomWidget: "tf_wlc"

*label2.class: label
*label2.parent: form1
*label2.static: true
*label2.name: label2
*label2.x: 12
*label2.y: 44
*label2.width: 190
*label2.height: 30
*label2.background: LabelBackground
*label2.fontList: TextFont
*label2.labelString: "Session :"
*label2.alignment: "alignment_beginning"
*label2.foreground: TextForeground
*label2.bottomAttachment: "attach_widget"
*label2.bottomWidget: "tf_wlc"

*label7.class: label
*label7.parent: form1
*label7.static: true
*label7.name: label7
*label7.x: 2
*label7.y: 8
*label7.width: 180
*label7.height: 30
*label7.background: LabelBackground
*label7.fontList: TextFont
*label7.labelString: "GENERAL PARAMETERS"
*label7.alignment: "alignment_beginning"
*label7.foreground: TextForeground
*label7.bottomAttachment: "attach_widget"
*label7.bottomWidget: "tf_session"

