! UIMX ascii 2.0 key: 3585                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}

*translation.table: SelectFileRebin
*translation.parent: RebinShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*RebinShell.class: applicationShell
*RebinShell.parent: NO_PARENT
*RebinShell.static: true
*RebinShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*RebinShell.ispecdecl:
*RebinShell.funcdecl: swidget create_RebinShell()\

*RebinShell.funcname: create_RebinShell
*RebinShell.funcdef: "swidget", "<create_RebinShell>(%)"
*RebinShell.icode:
*RebinShell.fcode: return(rtrn);\

*RebinShell.auxdecl:
*RebinShell.name: RebinShell
*RebinShell.x: 383
*RebinShell.y: 105
*RebinShell.width: 490
*RebinShell.height: 263
*RebinShell.title: "Rebinning"
*RebinShell.keyboardFocusPolicy: "pointer"
*RebinShell.geometry: "+10+60"
*RebinShell.background: WindowBackground

*form7.class: form
*form7.parent: RebinShell
*form7.static: true
*form7.name: form7
*form7.resizePolicy: "resize_none"
*form7.unitType: "pixels"
*form7.x: 0
*form7.y: 0
*form7.width: 408
*form7.height: 348
*form7.background: WindowBackground

*label21.class: label
*label21.parent: form7
*label21.static: true
*label21.name: label21
*label21.x: 24
*label21.y: 30
*label21.width: 168
*label21.height: 30
*label21.background: LabelBackground
*label21.fontList: TextFont
*label21.labelString: "Starting wavelength (A) :"
*label21.alignment: "alignment_beginning"
*label21.foreground: TextForeground

*tf_rebstrt.class: textField
*tf_rebstrt.parent: form7
*tf_rebstrt.static: true
*tf_rebstrt.name: tf_rebstrt
*tf_rebstrt.x: 201
*tf_rebstrt.y: 24
*tf_rebstrt.width: 96
*tf_rebstrt.height: 34
*tf_rebstrt.background: TextBackground
*tf_rebstrt.fontList: TextFont
*tf_rebstrt.highlightOnEnter: "true"
*tf_rebstrt.foreground: TextForeground
*tf_rebstrt.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
double val;\
extern double Rebstrt;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%lf", &val);\
\
if ( val != Rebstrt ) {\
    Rebstrt = val;\
    WriteKeyword(text, K_REBSTRT);\
}\
\
XtFree(text);\
}

*label22.class: label
*label22.parent: form7
*label22.static: true
*label22.name: label22
*label22.x: 24
*label22.y: 66
*label22.width: 156
*label22.height: 30
*label22.background: LabelBackground
*label22.fontList: TextFont
*label22.labelString: "Final wavelength (A) :"
*label22.alignment: "alignment_beginning"
*label22.foreground: TextForeground

*label23.class: label
*label23.parent: form7
*label23.static: true
*label23.name: label23
*label23.x: 24
*label23.y: 102
*label23.width: 158
*label23.height: 30
*label23.background: LabelBackground
*label23.fontList: TextFont
*label23.labelString: "Wavelength step (A) :"
*label23.alignment: "alignment_beginning"
*label23.foreground: TextForeground

*tf_rebend.class: textField
*tf_rebend.parent: form7
*tf_rebend.static: true
*tf_rebend.name: tf_rebend
*tf_rebend.x: 201
*tf_rebend.y: 62
*tf_rebend.width: 94
*tf_rebend.height: 34
*tf_rebend.background: TextBackground
*tf_rebend.fontList: TextFont
*tf_rebend.highlightOnEnter: "true"
*tf_rebend.foreground: TextForeground
*tf_rebend.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
double val;\
extern double Rebend;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%lf", &val);\
\
if ( val != Rebend ) {\
    Rebend = val;\
    WriteKeyword(text, K_REBEND);\
}\
\
XtFree(text);\
}

*rowColumn3.class: rowColumn
*rowColumn3.parent: form7
*rowColumn3.static: true
*rowColumn3.name: rowColumn3
*rowColumn3.x: 327
*rowColumn3.y: 35
*rowColumn3.width: 106
*rowColumn3.height: 96
*rowColumn3.radioBehavior: "true"
*rowColumn3.background: WindowBackground
*rowColumn3.entryBorder: 0
*rowColumn3.labelString: ""
*rowColumn3.shadowThickness: 0
*rowColumn3.borderWidth: 0
*rowColumn3.entryAlignment: "alignment_beginning"
*rowColumn3.adjustLast: "false"
*rowColumn3.adjustMargin: "true"
*rowColumn3.isAligned: "true"

*rb_rebmtd_line.class: toggleButton
*rb_rebmtd_line.parent: rowColumn3
*rb_rebmtd_line.static: true
*rb_rebmtd_line.name: rb_rebmtd_line
*rb_rebmtd_line.x: 12
*rb_rebmtd_line.y: 12
*rb_rebmtd_line.width: 148
*rb_rebmtd_line.height: 30
*rb_rebmtd_line.background: WindowBackground
*rb_rebmtd_line.fontList: TextFont
*rb_rebmtd_line.labelString: "LINEAR"
*rb_rebmtd_line.set: "true"
*rb_rebmtd_line.selectColor: SelectColor
*rb_rebmtd_line.highlightOnEnter: "true"
*rb_rebmtd_line.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("LINEAR", K_REBMTD);\
}
*rb_rebmtd_line.indicatorSize: 16
*rb_rebmtd_line.foreground: TextForeground

*rb_rebmtd_quad.class: toggleButton
*rb_rebmtd_quad.parent: rowColumn3
*rb_rebmtd_quad.static: true
*rb_rebmtd_quad.name: rb_rebmtd_quad
*rb_rebmtd_quad.x: 5
*rb_rebmtd_quad.y: 85
*rb_rebmtd_quad.width: 148
*rb_rebmtd_quad.height: 30
*rb_rebmtd_quad.background: WindowBackground
*rb_rebmtd_quad.fontList: TextFont
*rb_rebmtd_quad.labelString: "QUADRATIC"
*rb_rebmtd_quad.selectColor: SelectColor
*rb_rebmtd_quad.highlightOnEnter: "true"
*rb_rebmtd_quad.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("QUADRATIC", K_REBMTD);\
}
*rb_rebmtd_quad.indicatorSize: 16
*rb_rebmtd_quad.foreground: TextForeground

*rb_rebmtd_spli.class: toggleButton
*rb_rebmtd_spli.parent: rowColumn3
*rb_rebmtd_spli.static: true
*rb_rebmtd_spli.name: rb_rebmtd_spli
*rb_rebmtd_spli.x: 4
*rb_rebmtd_spli.y: 66
*rb_rebmtd_spli.width: 96
*rb_rebmtd_spli.height: 24
*rb_rebmtd_spli.background: WindowBackground
*rb_rebmtd_spli.fontList: TextFont
*rb_rebmtd_spli.labelString: "SPLINE"
*rb_rebmtd_spli.selectColor: SelectColor
*rb_rebmtd_spli.highlightOnEnter: "true"
*rb_rebmtd_spli.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("SPLINE", K_REBMTD);\
}
*rb_rebmtd_spli.indicatorSize: 16
*rb_rebmtd_spli.foreground: TextForeground

*label24.class: label
*label24.parent: form7
*label24.static: true
*label24.name: label24
*label24.x: 326
*label24.y: 6
*label24.width: 126
*label24.height: 27
*label24.background: LabelBackground
*label24.fontList: TextFont
*label24.labelString: "Rebinning method"
*label24.alignment: "alignment_beginning"
*label24.foreground: TextForeground

*separatorGadget11.class: separatorGadget
*separatorGadget11.parent: form7
*separatorGadget11.static: true
*separatorGadget11.name: separatorGadget11
*separatorGadget11.x: 451
*separatorGadget11.y: 21
*separatorGadget11.width: 12
*separatorGadget11.height: 120
*separatorGadget11.orientation: "vertical"

*separatorGadget12.class: separatorGadget
*separatorGadget12.parent: form7
*separatorGadget12.static: true
*separatorGadget12.name: separatorGadget12
*separatorGadget12.x: 315
*separatorGadget12.y: 21
*separatorGadget12.width: 12
*separatorGadget12.height: 120
*separatorGadget12.orientation: "vertical"

*tf_rebstp.class: textField
*tf_rebstp.parent: form7
*tf_rebstp.static: true
*tf_rebstp.name: tf_rebstp
*tf_rebstp.x: 201
*tf_rebstp.y: 100
*tf_rebstp.width: 94
*tf_rebstp.height: 34
*tf_rebstp.background: TextBackground
*tf_rebstp.fontList: TextFont
*tf_rebstp.highlightOnEnter: "true"
*tf_rebstp.foreground: TextForeground
*tf_rebstp.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
double val;\
extern double Rebstp;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%lf", &val);\
\
if ( val != Rebstp ) {\
    Rebstp = val;\
    WriteKeyword(text, K_REBSTP);\
}\
\
XtFree(text);\
}

*form8.class: form
*form8.parent: form7
*form8.static: true
*form8.name: form8
*form8.resizePolicy: "resize_none"
*form8.x: -2
*form8.y: 222
*form8.width: 490
*form8.height: 40
*form8.background: ButtonBackground

*pb_rebin_rbr.class: pushButton
*pb_rebin_rbr.parent: form8
*pb_rebin_rbr.static: true
*pb_rebin_rbr.name: pb_rebin_rbr
*pb_rebin_rbr.x: 4
*pb_rebin_rbr.y: 6
*pb_rebin_rbr.width: 100
*pb_rebin_rbr.height: 30
*pb_rebin_rbr.background: ButtonBackground
*pb_rebin_rbr.fontList: BoldTextFont
*pb_rebin_rbr.foreground: ApplyForeground
*pb_rebin_rbr.labelString: "Rebin RBR..."
*pb_rebin_rbr.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_REBIN_RBR);\
\
}

*pushButton13.class: pushButton
*pushButton13.parent: form8
*pushButton13.static: true
*pushButton13.name: pushButton13
*pushButton13.x: 400
*pushButton13.y: 6
*pushButton13.width: 86
*pushButton13.height: 30
*pushButton13.background: ButtonBackground
*pushButton13.fontList: BoldTextFont
*pushButton13.foreground: CancelForeground
*pushButton13.labelString: "Cancel"
*pushButton13.activateCallback: {\
UxPopdownInterface(UxFindSwidget("RebinShell"));\
}

*pb_rebin_plot.class: pushButton
*pb_rebin_plot.parent: form8
*pb_rebin_plot.static: true
*pb_rebin_plot.name: pb_rebin_plot
*pb_rebin_plot.x: 312
*pb_rebin_plot.y: 6
*pb_rebin_plot.width: 86
*pb_rebin_plot.height: 30
*pb_rebin_plot.background: ButtonBackground
*pb_rebin_plot.fontList: BoldTextFont
*pb_rebin_plot.foreground: ButtonForeground
*pb_rebin_plot.labelString: "Plot table"
*pb_rebin_plot.activateCallback: {\
#include <spec_comm.h>\
\
char command[128], *out_file;\
\
out_file = XmTextGetString(UxGetWidget(UxFindSwidget("tf_file_dialog")));\
\
sprintf(command, "%s%s", C_PLOT_SPEC, out_file);\
\
AppendDialogText(command);\
\
XtFree(out_file);\
}

*pb_rebin_table.class: pushButton
*pb_rebin_table.parent: form8
*pb_rebin_table.static: true
*pb_rebin_table.name: pb_rebin_table
*pb_rebin_table.x: 206
*pb_rebin_table.y: 6
*pb_rebin_table.width: 100
*pb_rebin_table.height: 30
*pb_rebin_table.background: ButtonBackground
*pb_rebin_table.fontList: BoldTextFont
*pb_rebin_table.foreground: ApplyForeground
*pb_rebin_table.labelString: "Rebin table..."
*pb_rebin_table.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_REBIN_TBL);\
}

*pb_rebin_2d.class: pushButton
*pb_rebin_2d.parent: form8
*pb_rebin_2d.static: true
*pb_rebin_2d.name: pb_rebin_2d
*pb_rebin_2d.x: 106
*pb_rebin_2d.y: 6
*pb_rebin_2d.width: 100
*pb_rebin_2d.height: 30
*pb_rebin_2d.background: ButtonBackground
*pb_rebin_2d.fontList: BoldTextFont
*pb_rebin_2d.foreground: ApplyForeground
*pb_rebin_2d.labelString: "Rebin 2D..."
*pb_rebin_2d.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_REBIN_2D);\
}

*shelp_rebin.class: text
*shelp_rebin.parent: form7
*shelp_rebin.static: true
*shelp_rebin.name: shelp_rebin
*shelp_rebin.x: 0
*shelp_rebin.y: 162
*shelp_rebin.width: 484
*shelp_rebin.height: 50
*shelp_rebin.background: SHelpBackground
*shelp_rebin.cursorPositionVisible: "false"
*shelp_rebin.editable: "false"
*shelp_rebin.fontList: TextFont

*separator6.class: separator
*separator6.parent: form7
*separator6.static: true
*separator6.name: separator6
*separator6.x: -2
*separator6.y: 152
*separator6.width: 492
*separator6.height: 10
*separator6.background: WindowBackground

*separator7.class: separator
*separator7.parent: form7
*separator7.static: true
*separator7.name: separator7
*separator7.x: -4
*separator7.y: 210
*separator7.width: 492
*separator7.height: 10
*separator7.background: WindowBackground

*separatorGadget15.class: separatorGadget
*separatorGadget15.parent: form7
*separatorGadget15.static: true
*separatorGadget15.name: separatorGadget15
*separatorGadget15.x: 319
*separatorGadget15.y: 135
*separatorGadget15.width: 138
*separatorGadget15.height: 10

