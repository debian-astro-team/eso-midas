! UIMX ascii 2.0 key: 4158                                                      

*SearchShell.class: applicationShell
*SearchShell.parent: NO_PARENT
*SearchShell.static: true
*SearchShell.gbldecl: #include <stdio.h> \
#include <ExternResources.h>  
*SearchShell.ispecdecl:
*SearchShell.funcdecl: swidget create_SearchShell()\

*SearchShell.funcname: create_SearchShell
*SearchShell.funcdef: "swidget", "<create_SearchShell>(%)"
*SearchShell.icode:
*SearchShell.fcode: return(rtrn);\

*SearchShell.auxdecl:
*SearchShell.name: SearchShell
*SearchShell.x: 30
*SearchShell.y: 40
*SearchShell.width: 490
*SearchShell.height: 281
*SearchShell.title: "Search"
*SearchShell.keyboardFocusPolicy: "pointer"
*SearchShell.geometry: "+10+60"
*SearchShell.background: WindowBackground

*form2.class: form
*form2.parent: SearchShell
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.unitType: "pixels"
*form2.x: 0
*form2.y: 0
*form2.width: 408
*form2.height: 348
*form2.background: WindowBackground

*label9.class: label
*label9.parent: form2
*label9.static: true
*label9.name: label9
*label9.x: 16
*label9.y: 92
*label9.width: 203
*label9.height: 30
*label9.background: LabelBackground
*label9.fontList: TextFont
*label9.labelString: "Row smoothing factor :"
*label9.alignment: "alignment_beginning"
*label9.foreground: TextForeground

*tf_ywidth.class: textField
*tf_ywidth.parent: form2
*tf_ywidth.static: true
*tf_ywidth.name: tf_ywidth
*tf_ywidth.x: 230
*tf_ywidth.y: 90
*tf_ywidth.width: 82
*tf_ywidth.height: 34
*tf_ywidth.background: TextBackground
*tf_ywidth.fontList: TextFont
*tf_ywidth.highlightOnEnter: "true"
*tf_ywidth.foreground: TextForeground
*tf_ywidth.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Ywidth;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Ywidth ) {\
    Ywidth = val;\
    WriteKeyword(text, K_YWIDTH);\
}\
\
XtFree(text);\
}

*label10.class: label
*label10.parent: form2
*label10.static: true
*label10.name: label10
*label10.x: 16
*label10.y: 128
*label10.width: 205
*label10.height: 30
*label10.background: LabelBackground
*label10.fontList: TextFont
*label10.labelString: "Row binning step :"
*label10.alignment: "alignment_beginning"
*label10.foreground: TextForeground

*label11.class: label
*label11.parent: form2
*label11.static: true
*label11.name: label11
*label11.x: 16
*label11.y: 16
*label11.width: 207
*label11.height: 30
*label11.background: LabelBackground
*label11.fontList: TextFont
*label11.labelString: "Threshold :"
*label11.alignment: "alignment_beginning"
*label11.foreground: TextForeground

*tf_ystep.class: textField
*tf_ystep.parent: form2
*tf_ystep.static: true
*tf_ystep.name: tf_ystep
*tf_ystep.x: 230
*tf_ystep.y: 128
*tf_ystep.width: 82
*tf_ystep.height: 34
*tf_ystep.background: TextBackground
*tf_ystep.fontList: TextFont
*tf_ystep.highlightOnEnter: "true"
*tf_ystep.foreground: TextForeground
*tf_ystep.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Ystep;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Ystep ) {\
    Ystep = val;\
    WriteKeyword(text, K_YSTEP);\
}\
\
XtFree(text);\
}

*tf_width.class: textField
*tf_width.parent: form2
*tf_width.static: true
*tf_width.name: tf_width
*tf_width.x: 230
*tf_width.y: 52
*tf_width.width: 82
*tf_width.height: 34
*tf_width.background: TextBackground
*tf_width.fontList: TextFont
*tf_width.highlightOnEnter: "true"
*tf_width.foreground: TextForeground
*tf_width.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Width;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Width ) {\
    Width = val;\
    WriteKeyword(text, K_WIDTH);\
}\
\
XtFree(text);\
}

*rowColumn1.class: rowColumn
*rowColumn1.parent: form2
*rowColumn1.static: true
*rowColumn1.name: rowColumn1
*rowColumn1.x: 354
*rowColumn1.y: 40
*rowColumn1.width: 106
*rowColumn1.height: 96
*rowColumn1.radioBehavior: "true"
*rowColumn1.background: WindowBackground
*rowColumn1.entryBorder: 0
*rowColumn1.labelString: ""
*rowColumn1.shadowThickness: 0
*rowColumn1.borderWidth: 0
*rowColumn1.entryAlignment: "alignment_beginning"
*rowColumn1.adjustLast: "false"
*rowColumn1.adjustMargin: "true"
*rowColumn1.isAligned: "true"

*rb_seamtd_gaus.class: toggleButton
*rb_seamtd_gaus.parent: rowColumn1
*rb_seamtd_gaus.static: true
*rb_seamtd_gaus.name: rb_seamtd_gaus
*rb_seamtd_gaus.x: 12
*rb_seamtd_gaus.y: 12
*rb_seamtd_gaus.width: 148
*rb_seamtd_gaus.height: 30
*rb_seamtd_gaus.background: WindowBackground
*rb_seamtd_gaus.fontList: TextFont
*rb_seamtd_gaus.labelString: "GAUSS"
*rb_seamtd_gaus.set: "true"
*rb_seamtd_gaus.selectColor: SelectColor
*rb_seamtd_gaus.highlightOnEnter: "true"
*rb_seamtd_gaus.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("GAUSS", K_SEAMTD);\
}
*rb_seamtd_gaus.indicatorSize: 16
*rb_seamtd_gaus.foreground: TextForeground

*rb_seamtd_grav.class: toggleButton
*rb_seamtd_grav.parent: rowColumn1
*rb_seamtd_grav.static: true
*rb_seamtd_grav.name: rb_seamtd_grav
*rb_seamtd_grav.x: 5
*rb_seamtd_grav.y: 85
*rb_seamtd_grav.width: 148
*rb_seamtd_grav.height: 30
*rb_seamtd_grav.background: WindowBackground
*rb_seamtd_grav.fontList: TextFont
*rb_seamtd_grav.labelString: "GRAVITY"
*rb_seamtd_grav.selectColor: SelectColor
*rb_seamtd_grav.highlightOnEnter: "true"
*rb_seamtd_grav.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("GRAVITY", K_SEAMTD);\
}
*rb_seamtd_grav.indicatorSize: 16
*rb_seamtd_grav.foreground: TextForeground

*rb_seamtd_maxi.class: toggleButton
*rb_seamtd_maxi.parent: rowColumn1
*rb_seamtd_maxi.static: true
*rb_seamtd_maxi.name: rb_seamtd_maxi
*rb_seamtd_maxi.x: 4
*rb_seamtd_maxi.y: 66
*rb_seamtd_maxi.width: 96
*rb_seamtd_maxi.height: 24
*rb_seamtd_maxi.background: WindowBackground
*rb_seamtd_maxi.fontList: TextFont
*rb_seamtd_maxi.labelString: "MAXIMUM"
*rb_seamtd_maxi.selectColor: SelectColor
*rb_seamtd_maxi.highlightOnEnter: "true"
*rb_seamtd_maxi.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( UpdateToggle && XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("MAXIMUM", K_SEAMTD);\
}
*rb_seamtd_maxi.indicatorSize: 16
*rb_seamtd_maxi.foreground: TextForeground

*label13.class: label
*label13.parent: form2
*label13.static: true
*label13.name: label13
*label13.x: 358
*label13.y: 10
*label13.width: 106
*label13.height: 30
*label13.background: LabelBackground
*label13.fontList: TextFont
*label13.labelString: "Search method"
*label13.alignment: "alignment_beginning"
*label13.foreground: TextForeground

*separatorGadget2.class: separatorGadget
*separatorGadget2.parent: form2
*separatorGadget2.static: true
*separatorGadget2.name: separatorGadget2
*separatorGadget2.x: 472
*separatorGadget2.y: 24
*separatorGadget2.width: 12
*separatorGadget2.height: 120
*separatorGadget2.orientation: "vertical"

*separatorGadget3.class: separatorGadget
*separatorGadget3.parent: form2
*separatorGadget3.static: true
*separatorGadget3.name: separatorGadget3
*separatorGadget3.x: 336
*separatorGadget3.y: 24
*separatorGadget3.width: 12
*separatorGadget3.height: 120
*separatorGadget3.orientation: "vertical"

*separatorGadget4.class: separatorGadget
*separatorGadget4.parent: form2
*separatorGadget4.static: true
*separatorGadget4.name: separatorGadget4
*separatorGadget4.x: 340
*separatorGadget4.y: 22
*separatorGadget4.width: 16
*separatorGadget4.height: 10

*separatorGadget5.class: separatorGadget
*separatorGadget5.parent: form2
*separatorGadget5.static: true
*separatorGadget5.name: separatorGadget5
*separatorGadget5.x: 464
*separatorGadget5.y: 22
*separatorGadget5.width: 14
*separatorGadget5.height: 10

*tf_thres.class: textField
*tf_thres.parent: form2
*tf_thres.static: true
*tf_thres.name: tf_thres
*tf_thres.x: 230
*tf_thres.y: 14
*tf_thres.width: 82
*tf_thres.height: 34
*tf_thres.background: TextBackground
*tf_thres.fontList: TextFont
*tf_thres.highlightOnEnter: "true"
*tf_thres.foreground: TextForeground
*tf_thres.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Thres;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Thres ) {\
    Thres = val;\
    WriteKeyword(text, K_THRES);\
}\
\
XtFree(text);\
}

*form4.class: form
*form4.parent: form2
*form4.static: true
*form4.name: form4
*form4.resizePolicy: "resize_none"
*form4.x: 0
*form4.y: 240
*form4.width: 490
*form4.height: 40
*form4.background: ButtonBackground

*pb_search_search.class: pushButton
*pb_search_search.parent: form4
*pb_search_search.static: true
*pb_search_search.name: pb_search_search
*pb_search_search.x: 8
*pb_search_search.y: 6
*pb_search_search.width: 112
*pb_search_search.height: 30
*pb_search_search.background: ButtonBackground
*pb_search_search.fontList: BoldTextFont
*pb_search_search.foreground: ApplyForeground
*pb_search_search.labelString: "Search On..." 
*pb_search_search.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_WLC);\
\
}

*pushButton2.class: pushButton
*pushButton2.parent: form4
*pushButton2.static: true
*pushButton2.name: pushButton2
*pushButton2.x: 210
*pushButton2.y: 6
*pushButton2.width: 86
*pushButton2.height: 30
*pushButton2.background: ButtonBackground
*pushButton2.fontList: BoldTextFont
*pushButton2.foreground: CancelForeground
*pushButton2.labelString: "Cancel"
*pushButton2.activateCallback: {\
UxPopdownInterface(UxFindSwidget("SearchShell"));\
}

*pb_search_plot.class: pushButton
*pb_search_plot.parent: form4
*pb_search_plot.static: true
*pb_search_plot.name: pb_search_plot
*pb_search_plot.x: 122
*pb_search_plot.y: 6
*pb_search_plot.width: 86
*pb_search_plot.height: 30
*pb_search_plot.background: ButtonBackground
*pb_search_plot.fontList: BoldTextFont
*pb_search_plot.foreground: ButtonForeground
*pb_search_plot.labelString: "Plot"
*pb_search_plot.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_PLOT_SEARCH);\
}

*shelp_search.class: text
*shelp_search.parent: form2
*shelp_search.static: true
*shelp_search.name: shelp_search
*shelp_search.x: 2
*shelp_search.y: 180
*shelp_search.width: 484
*shelp_search.height: 50
*shelp_search.background: SHelpBackground
*shelp_search.cursorPositionVisible: "false"
*shelp_search.editable: "false"
*shelp_search.fontList: TextFont

*separator2.class: separator
*separator2.parent: form2
*separator2.static: true
*separator2.name: separator2
*separator2.x: 0
*separator2.y: 170
*separator2.width: 492
*separator2.height: 10
*separator2.background: WindowBackground

*separator3.class: separator
*separator3.parent: form2
*separator3.static: true
*separator3.name: separator3
*separator3.x: -2
*separator3.y: 228
*separator3.width: 492
*separator3.height: 10
*separator3.background: WindowBackground

*separatorGadget1.class: separatorGadget
*separatorGadget1.parent: form2
*separatorGadget1.static: true
*separatorGadget1.name: separatorGadget1
*separatorGadget1.x: 340
*separatorGadget1.y: 138
*separatorGadget1.width: 138
*separatorGadget1.height: 10

*label12.class: label
*label12.parent: form2
*label12.static: true
*label12.name: label12
*label12.x: 16
*label12.y: 52
*label12.width: 205
*label12.height: 30
*label12.background: LabelBackground
*label12.fontList: TextFont
*label12.labelString: "Width of search window :"
*label12.alignment: "alignment_beginning"
*label12.foreground: TextForeground

