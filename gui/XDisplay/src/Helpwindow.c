/* @(#)Helpwindow.c	19.1 (ES0-DMD) 02/25/03 13:44:21 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	Helpwindow.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxHelpwindow;
	swidget	Uxform11;
	swidget	UxscrolledWindow3;
	swidget	UxscrolledText1;
	swidget	UxpushButton10;
} _UxCHelpwindow;

#define Helpwindow              UxHelpwindowContext->UxHelpwindow
#define form11                  UxHelpwindowContext->Uxform11
#define scrolledWindow3         UxHelpwindowContext->UxscrolledWindow3
#define scrolledText1           UxHelpwindowContext->UxscrolledText1
#define pushButton10            UxHelpwindowContext->UxpushButton10

static _UxCHelpwindow	*UxHelpwindowContext;


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable2 = "#augment\n\
<Btn3Down>:ListBeginSelect()\n\
<Btn3Up>:ListEndSelect()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Helpwindow();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	createCB_scrolledText1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpwindow          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpwindowContext;
	UxHelpwindowContext = UxContext =
			(_UxCHelpwindow *) UxGetContext( UxThisWidget );
	{
	extern swidget shtext;
	
	shtext = UxWidgetToSwidget(UxWidget);
	
	}
	UxHelpwindowContext = UxSaveCtx;
}

static void	activateCB_pushButton10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpwindow          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpwindowContext;
	UxHelpwindowContext = UxContext =
			(_UxCHelpwindow *) UxGetContext( UxThisWidget );
	{
	extern int  kick[5];
	
	extern swidget swhelp;
	
	kick[4] = 0;			/* no more help window */
	
	UxPopdownInterface(swhelp);
	}
	UxHelpwindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Helpwindow()
{
	UxPutIconName( Helpwindow, "Helpwindow" );
	UxPutKeyboardFocusPolicy( Helpwindow, "pointer" );
	UxPutBackground( Helpwindow, TextBackground );
	UxPutHeight( Helpwindow, 220 );
	UxPutWidth( Helpwindow, 500 );
	UxPutY( Helpwindow, 130 );
	UxPutX( Helpwindow, 610 );

	UxPutBackground( form11, ApplicBackground );
	UxPutHeight( form11, 220 );
	UxPutWidth( form11, 460 );
	UxPutY( form11, 0 );
	UxPutX( form11, 0 );
	UxPutUnitType( form11, "pixels" );
	UxPutResizePolicy( form11, "resize_none" );

	UxPutSpacing( scrolledWindow3, 2 );
	UxPutScrolledWindowMarginWidth( scrolledWindow3, 1 );
	UxPutScrolledWindowMarginHeight( scrolledWindow3, 1 );
	UxPutScrollBarPlacement( scrolledWindow3, "bottom_left" );
	UxPutForeground( scrolledWindow3, TextForeground );
	UxPutBackground( scrolledWindow3, ApplicBackground );
	UxPutShadowThickness( scrolledWindow3, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow3, "static" );
	UxPutVisualPolicy( scrolledWindow3, "variable" );
	UxPutY( scrolledWindow3, 20 );
	UxPutX( scrolledWindow3, 20 );
	UxPutScrollingPolicy( scrolledWindow3, "application_defined" );

	UxPutEditable( scrolledText1, "false" );
	UxPutEditMode( scrolledText1, "multi_line_edit" );
	UxPutScrollLeftSide( scrolledText1, "false" );
	UxPutMarginWidth( scrolledText1, 2 );
	UxPutMarginHeight( scrolledText1, 1 );
	UxPutForeground( scrolledText1, TextForeground );
	UxPutFontList( scrolledText1, TextFont );
	UxPutBackground( scrolledText1, TextBackground );
	UxPutHeight( scrolledText1, 150 );
	UxPutWidth( scrolledText1, 380 );

	UxPutForeground( pushButton10, CancelForeground );
	UxPutLabelString( pushButton10, "Close" );
	UxPutFontList( pushButton10, BoldTextFont );
	UxPutBackground( pushButton10, ButtonBackground );
	UxPutHeight( pushButton10, 30 );
	UxPutWidth( pushButton10, 100 );
	UxPutY( pushButton10, 92 );
	UxPutX( pushButton10, 125 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Helpwindow()
{
	/* Create the swidgets */

	Helpwindow = UxCreateTopLevelShell( "Helpwindow", NO_PARENT );
	UxPutContext( Helpwindow, UxHelpwindowContext );

	form11 = UxCreateForm( "form11", Helpwindow );
	scrolledWindow3 = UxCreateScrolledWindow( "scrolledWindow3", form11 );
	scrolledText1 = UxCreateScrolledText( "scrolledText1", scrolledWindow3 );
	pushButton10 = UxCreatePushButton( "pushButton10", form11 );

	_Uxinit_Helpwindow();

	/* Create the X widgets */

	UxCreateWidget( Helpwindow );
	UxCreateWidget( form11 );
	UxPutTopOffset( scrolledWindow3, 2 );
	UxPutTopAttachment( scrolledWindow3, "attach_form" );
	UxPutRightOffset( scrolledWindow3, 2 );
	UxPutRightAttachment( scrolledWindow3, "attach_form" );
	UxPutLeftOffset( scrolledWindow3, 2 );
	UxPutLeftAttachment( scrolledWindow3, "attach_form" );
	UxPutBottomOffset( scrolledWindow3, 33 );
	UxPutBottomAttachment( scrolledWindow3, "attach_form" );
	UxCreateWidget( scrolledWindow3 );

	UxCreateWidget( scrolledText1 );
	createCB_scrolledText1( UxGetWidget( scrolledText1 ),
			(XtPointer) UxHelpwindowContext, (XtPointer) NULL );

	UxPutLeftOffset( pushButton10, 180 );
	UxPutLeftAttachment( pushButton10, "attach_form" );
	UxPutRightOffset( pushButton10, 100 );
	UxPutRightAttachment( pushButton10, "attach_none" );
	UxPutBottomOffset( pushButton10, 2 );
	UxPutBottomAttachment( pushButton10, "attach_form" );
	UxCreateWidget( pushButton10 );


	UxAddCallback( pushButton10, XmNactivateCallback,
			activateCB_pushButton10,
			(XtPointer) UxHelpwindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Helpwindow );

	return ( Helpwindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Helpwindow()
{
	swidget                 rtrn;
	_UxCHelpwindow          *UxContext;

	UxHelpwindowContext = UxContext =
		(_UxCHelpwindow *) UxMalloc( sizeof(_UxCHelpwindow) );

	rtrn = _Uxbuild_Helpwindow();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Helpwindow()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_Helpwindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

