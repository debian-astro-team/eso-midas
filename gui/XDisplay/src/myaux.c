/* @(#)myaux.c	19.1 (ES0-DMD) 02/25/03 22:42:42 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
    this file `myaux.c'  is included as an auxiliary function 
    use the Declaration Editor of UIMX to store it there 

    if modified it has to be copied over to ../src so that it is found
    in the make
*/

#include <midas_def.h>

/******************************************************************
   This function establishes a protocol callback that detects 
   the window manager Close command.  */

void CreateWindowManagerProtocols(shell)
     Widget shell;
{
  Atom  xa_WM_DELETE_WINDOW;

  xa_WM_DELETE_WINDOW = XInternAtom (UxDisplay, "WM_DELETE_WINDOW", False);
/*
  XmAddWMProtocolCallback (shell, xa_WM_DELETE_WINDOW, ExitCB, NULL);
*/
}

/* This function pops up the Exit dialog box. */
/*
void ExitCB(w, client_data, call_data)
     Widget  w;
     caddr_t client_data;
     caddr_t call_data;
{
  UxPopupInterface (exitDialog, no_grab);
}
*/


/****************************************************************** 
   This function establishes a protocol callback to detect a "save 
   yourself" message from the session manager. */

void CreateSessionManagerProtocols()
{
  Atom  xa_WM_SAVE_YOURSELF;
  
  xa_WM_SAVE_YOURSELF = XInternAtom (UxDisplay, "WM_SAVE_YOURSELF", False);
  XmAddWMProtocolCallback (UxTopLevel, xa_WM_SAVE_YOURSELF, SaveSessionCB, NULL);
}


/*****************************************************************
   This callback function is executed whenever this application 
   receives a "save yourself" callback from the session manager. */

void SaveSessionCB()
{

  printf("The session manager is saving a session ...\n");
  printf(" ... (New Application) should save itself now!\n");

}

/*

*/



/* `applycom.c'  the Activate Callback of the `Apply' button */


#define OKSTAT     0

#define NOEXSTAT   -999
#define SENDSTAT  -1000
#define BUSYSTAT  -1001
#define RETSTAT   -1002

#define UNDEFINED -1009

applycom(flag)
int flag;

{

extern int MonitorPid;

char  command[160], *single, *singlf, *cpp, *cqq;
char  cswk[40][8], cbuf[80], cd;

extern char  actdisp[8];
static char  dispbuf[8] = "false", appitt[8] = "false";
static char  Qlut[40] = "";
static char  Qima[64] = "";

extern int menuact[8];

int  kk, jj, loop, looplim, miderr[2], comndno;

short int jswk[12];
swidget  swk[12];


/* first check for the menu generated commands ... */

if (flag > 0)
   {
   comndno = 100 + flag;
   switch (flag)
      {
     case 1:
      strcpy(command,"CLEAR/CHAN OVER");
      goto send_comnd;

     case 2:
      strcpy(command,"CLEAR/ITT");
      goto send_comnd;

     case 3:
      strcpy(command,"SET/ITT");
      goto send_comnd;

     case 4:
      strcpy(command,"SET/LUT");
      goto send_comnd;

     case 5:
      strcpy(command,"CLEAR/LUT");
      goto send_comnd;

     case 6:
      strcpy(command,"CLEAR/DISPLAY");
      goto send_comnd;

     case 7:
      strcpy(command,"CLEAR/CHAN");
      goto send_comnd;

     case 8:
      strcpy(command,"RESET/DISPLAY");
      goto send_comnd;

     case 9:
      strcpy(command,"CLEAR/OVER");
      goto send_comnd;

     case 10:
      strcpy(command,"SET/OVER");
      goto send_comnd;

     case 11:
      strcpy(command,"DISPLAY/LUT");
      goto send_comnd;

     case 12:
      strcpy(command,"DISPLAY/LUT OFF");
      goto send_comnd;

     case 13:
      strcpy(command,"DELETE/DISPLAY");
      goto send_comnd;

     case 14:
      strcpy(command,"DELETE/GRAPHICS");
      goto send_comnd;
      }
   }

   
/* collect the different fields */

begin:
if (Widget_managed[0] == UxGetWidget(form2))	/* Load Image */
   {
   single = XmTextGetString(UxGetWidget(text41));	/* get display ID */
   if (strcmp(actdisp,single) != 0)
      {
      strcpy(actdisp,single);
      comndno = 0;
      strcpy(command,"ASSIGN/DISP ");
      strcpy(&command[12],actdisp);
      goto send_comnd;
      }

   comndno = 1;
   strcpy(command,"LOAD/IMAG ");
   kk = 10;
   XtFree(single);

   looplim = 8;
   for (loop=0; loop<looplim; loop++)
      jswk[loop] = 0;		/* = 0, indicates `text swidget' */

   single = XmTextGetString(UxGetWidget(text16));
   cpp = single;
   while (*cpp == ' ') cpp++;
   cd = *cpp;
   XtFree(single);
   if (*cpp == '\0') goto no_command;		/* no image name there... */

   swk[0] = text16;	/* image name */
   swk[1] = text2;	/* channel */
   swk[2] = text39;	/* X scale */
   jswk[2] = 1;
   swk[3] = text40;	/* Y scale */
   swk[4] = text20;	/* X center */
   jswk[4] = 1;
   swk[5] = text24;	/* Y center */
   swk[6] = text15;	/* low cut */
   jswk[6] = 1;
   swk[7] = text22;	/* hi cut */
   }

else if (Widget_managed[0] == UxGetWidget(form5))	/* Quick Load */
   {
   comndno = 9;
   strcpy(cbuf,UxGetSet(toggleButton13));
   if (cbuf[0] == 't')
      {
      strcpy(command,"CREATE/DISP 0 ");
      single = XmTextGetString(UxGetWidget(text30)); 
      cpp = single;
      while (*cpp == ' ') cpp++;
      singlf = XmTextGetString(UxGetWidget(text32));
      cqq = singlf;
      while (*cqq == ' ') cqq++;
      if ((*cpp != '\0') && (*cqq != '\0'))
         {
         strcat(command,cpp);
         kk = strlen(command);
         command[kk++] = ',';
         strcpy(&command[kk],cqq);
         strcat(command,",630,330");
         }

      XtFree(single);
      XtFree(singlf);
      kk = strlen(command);
      command[kk++] = ';';
      command[kk] = '\0';
   
      UxPutSet(toggleButton13,"false");
      UxPutLabelString(toggleButton13,"No");
      XtSetSensitive(UxGetWidget(label44),FALSE);
      XtSetSensitive(UxGetWidget(label49),FALSE);
      XtSetSensitive(UxGetWidget(rowColumn23),FALSE);
      XtSetSensitive(UxGetWidget(rowColumn25),FALSE);

      Qima[0] = '\0';			/* force new loading */
      Qlut[0] = '\0';
      }
   else
      {
      command[0] = '\0';
      kk = 0;
      }

qload:
   single = XmTextGetString(UxGetWidget(text28)); 	/* get file name */
   cpp = single;
   while (*cpp == ' ') cpp++;
   if (*cpp != '\0')
      {
      if (strcmp(Qima,cpp) != 0)
         {
         strcpy(Qima,cpp);                      /* save new image name */
         strcpy(&command[kk],"LOAD/IMAGE ");
         kk += 11;
         strcpy(&command[kk],cpp);
         kk = strlen(command);
         strcpy(&command[kk]," ? ?");
         kk += 4; 
         singlf = XmTextGetString(UxGetWidget(text31));	/* scaleX */
         cpp = singlf;
         while (*cpp == ' ') cpp++;
         if (*cpp != '\0')
            {
            strcpy(&command[--kk],cpp);
            kk = strlen(command);
            }
         XtFree(singlf);
         singlf = XmTextGetString(UxGetWidget(text33));    /* scaleY */
         cpp = singlf;
         while (*cpp == ' ') cpp++;
         if (*cpp != '\0')
            {
            command[kk++] = ',';
            strcpy(&command[kk],cpp);
            kk = strlen(command);
            }
         XtFree(singlf);
         strcpy(&command[kk]," ? ");
         kk += 3; 
         singlf = XmTextGetString(UxGetWidget(text34));    /* low cut */
         cpp = singlf;
         while (*cpp == ' ') cpp++;
         if (*cpp != '\0')
            {
            strcpy(cbuf,cpp);
            jj = strlen(cbuf);
            XtFree(singlf);
     
            singlf = XmTextGetString(UxGetWidget(text35));    /* high cut */
            cpp = singlf;
            while (*cpp == ' ') cpp++;
            if (*cpp != '\0')
               {
               cbuf[jj++] = ',';
               strcpy(&cbuf[jj],cpp);
               strcpy(&command[kk],cbuf);
               }
            }
         XtFree(singlf);
         }
      }
   XtFree(single);
 
   kk = strlen(command);
   if ((kk > 0) && (command[kk-1] != ';'))
      {
      command[kk++] = ';';
      command[kk] = '\0';
      }

   single = XmTextGetString(UxGetWidget(text29));       /* get LUT name */
   cpp = single;
   while (*cpp == ' ') cpp++;
   if (*cpp != '\0')
      {
      if (strcmp(Qlut,cpp) != 0)
         {
         strcpy(Qlut,cpp);			/* save new LUT */
         strcpy(&command[kk],"LOAD/LUT ");
         kk += 9;
         strcpy(&command[kk],cpp);
         }
      }

   XtFree(single);
   kk = strlen(command);
   if (kk > 0)
      {
      kk --;
      if (command[kk] == ';') command[kk] = '\0';
      goto send_comnd;
      }
   else
      {
      if (comndno == 9)
         {
         comndno = 9000;
         strcpy(Qima," !@#");			/* if nothing load image ... */
         goto qload;
         }
      return;
      }

   }


else if (Widget_managed[0] == UxGetWidget(form6))  /* Create Display/Graphics */
   {
   comndno = 2;
   strcpy(cbuf,UxGetSet(toggleButton15));
   if (cbuf[0] == 't')
      {
      cd = 'D';
      looplim = 9;
      }
   else
      {
      cd = 'G';
      looplim = 7;
      }

   for (loop=0; loop<looplim; loop++)
      jswk[loop] = 0;		/* = 0, indicates `text swidget' */

   swk[0] = text1;		/* display id */
   swk[1] = text4;		/* dspsizeX */
   jswk[1] = 1;			/* add `,' and append following `swk' */
   swk[2] = text5;		/* dspsizeY */
   jswk[2] = 1;
   swk[3] = text6;		/* dspoffsetX */
   jswk[3] = 1;
   swk[4] = text8;		/* dspoffsetY */
   if (cd == 'D')
      {
      jswk[5] = -2;		/* meminfo  is set to ? */
      strcpy(cbuf,UxGetSet(toggleButton25));
      if (cbuf[0] == 't')
         strcpy(cswk[6] ,"Yes");
      else
         strcpy(cswk[6] ,"No");	/* alpha_flag */
      jswk[6] = -3;		/* = -3, indicates: use cswk[6]  */
      swk[7] = text13;		/* Gsize */
      swk[8] = text21;		/* Xstation */
      strcpy(command,"CREATE/DISP ");
      kk = 12;
      }
   else 
      {
      swk[5] = text13;		/* Gsize */
      swk[6] = text21;		/* Xstation */
      strcpy(command,"CREATE/GRAPH ");
      kk = 13;
      }
   }

else if (Widget_managed[0] == UxGetWidget(form4))	/* LOAD/LUT + ITT */
   {	
   comndno = 3;
   kk = 0;
   single = XmTextGetString(UxGetWidget(text9));	/* LUT name */
   if (*single != '\0')
      {
      strcpy(command,"LOAD/LUT ");
      kk = 9;
      strcpy(&command[kk],single);
      kk = strlen(command);
      command[kk++] = ';';
      command[kk] = '\0';
      }
   XtFree(single);

   strcpy(cbuf,UxGetSet(toggleButton10));		/* DisplayLUT */
   if (strcmp(dispbuf,cbuf) != 0)
      {
      strcpy(dispbuf,cbuf);
      if (dispbuf[0] == 't')
         strcpy(&command[kk],"DISPLAY/LUT  ON;");
      else
         strcpy(&command[kk],"DISPLAY/LUT OFF;");
      kk += 16;
      }

   single = XmTextGetString(UxGetWidget(text23));	/* ITT name */
   if (*single != '\0')
      {
      strcpy(&command[kk],"LOAD/ITT ");
      kk += 9;
      strcpy(&command[kk],single);
      }
   XtFree(single);

   if (kk > 0)				/* anything to send?  */
      {
      kk = strlen(command) - 1;
      if (command[kk] == ';') command[kk] = '\0';	/* omit last ';'  */
      goto send_comnd;
      }
   else
      return;
   }


else if (Widget_managed[0] == UxGetWidget(form8))	/* MODIFY/LUT + ITT */
   {
   comndno = 4;
   strcpy(cbuf,UxGetSet(toggleButton3));		/* check LUT button */
   kk = 0;
   if (cbuf[0] == 't')
      {
      strcpy(command,"MODIFY/LUT ");
      kk = 11;
      strcpy(cbuf,UxGetMenuHistory(method2));
      strcpy(&command[kk],UxGetLabelString(UxFindSwidget(cbuf)));
      kk = strlen(command);
      command[kk++] = ' ';
      strcpy(cbuf,UxGetMenuHistory(color1));
      strcpy(&command[kk],UxGetLabelString(UxFindSwidget(cbuf)));
      kk = strlen(command);
      }

   strcpy(cbuf,UxGetSet(toggleButton5));		/* check ITT button */
   if (cbuf[0] == 't')
      {
      if (kk > 0)			/* also MODIFY/LUT selected ... */
         {
         command[kk++] = ';';
         command[kk++] = ' ';
         }
      strcpy(&command[kk],"MODIFY/ITT ");
      kk += 11;
      strcpy(cbuf,UxGetMenuHistory(method1));
      strcpy(&command[kk],UxGetLabelString(UxFindSwidget(cbuf)));
      if (command[kk] == 'B')		/* for BAND we also need value */
         {
         kk = strlen(command);
         command[kk++] = ' ';
         single = XmTextGetString(UxGetWidget(text27));		/* ITT value */
         strcpy(&command[kk],single);
         XtFree(single);
         }
      }
   goto send_comnd;
   }


else if (Widget_managed[0] == UxGetWidget(form3))	/* VIEW/IMAGE */
   {
   comndno = 5;
   for (loop=0; loop<4; loop++)
      jswk[loop] = 0;		/* = 0, indicates `text swidget' */

   swk[0] = text10;		/* image name */
   swk[1] = text17;		/* out_table */
   strcpy(cbuf,UxGetSet(toggleButton2));		/* plot_option */
   if (cbuf[0] == 't')
      strcpy(cswk[2],"Plot");
   else
      strcpy(cswk[2],"NoPlot");
   jswk[2] = -3;
   strcpy(cbuf,UxGetSet(toggleButton4));		/* file_format */
   if (cbuf[0] == 't')
      strcpy(cswk[3],"Midas");
   else
      strcpy(cswk[3],"FITS");
   jswk[3] = -3;
   looplim = 4;
   strcpy(command,"VIEW/IMAG ");
   kk = 10;
   }

else if (Widget_managed[0] == UxGetWidget(form7))	/* GET/CURSOR */
   {
   comndno = 6;
   swk[0] = text3;		/* out_specs */
   jswk[0] = 0;		/* = 0, indicates `text swidget' */
   jswk[1] = -2;		/* option  is set to ? */
   strcpy(cbuf,UxGetSet(toggleButton14));		/* mark */
   if (cbuf[0] == 't')
      strcpy(cswk[2],"YN");
   else
      strcpy(cswk[2],"NN");
   jswk[2] = -3;
   strcpy(cbuf,UxGetSet(toggleButton9));		/* No_Curs */
   if (cbuf[0] == 't')
      strcpy(cswk[3],"9999,1");
   else
      strcpy(cswk[3],"9999,2");
   jswk[3] = -3;
   strcpy(cbuf,UxGetSet(toggleButton8));		/* zoom_window */
   if (cbuf[0] == 't')
      strcpy(cswk[4],"N");
   else
      strcpy(cswk[4],"W");
   jswk[4] = -3;
   looplim = 5;
   strcpy(command,"GET/CURSOR ");
   kk = 11;
   }


else if (Widget_managed[0] == UxGetWidget(form1))	/* EXTRACT/TRACE */
   {
   comndno = 7;  
   swk[0] = text14;		/* stepsize */
   jswk[0] = 0;
   swk[1] = text7;		/* frame */
   jswk[1] = 0;
   strcpy(cbuf,UxGetMenuHistory(plotflag));
   swk[2] = UxFindSwidget(cbuf);	/* plot_flag */
   jswk[2] = -1;
   strcpy(cbuf,UxGetSet(toggleButton6));		/* cut_option */
   if (cbuf[0] == 't')
      strcpy(cswk[3],"NoCut");
   else
      strcpy(cswk[3],"Cut");
   jswk[3] = -3;
   looplim = 4;
   strcpy(command,"EXTRACT/TRACE ");
   kk = 14;
   }




else
   {
   printf("We should not come here...\n");
   return;
   }




for (loop=0; loop<looplim; loop++)
   {
   if (jswk[loop] == -1)
      {
      strcpy(cbuf,UxGetLabelString(swk[loop]));
      single = cbuf;
      }
   else if (jswk[loop] == -2)
      *single = '\0';
   else if (jswk[loop] == -3)
      single = cswk[loop];
   else
      single = XmTextGetString(UxGetWidget(swk[loop]));

   if ((loop > 1) && (jswk[loop-1] == 1))	/* glue together */
      {
      if (single[0] != '\0')
         {
         cpp = single;
         while (*cpp == ' ') cpp++;		/* skip blanks */
         command[kk-1] = ',';			/* change ' ' to ','  */
         strcpy(&command[kk],cpp);
         kk = strlen(command);
         }
      }
   else
      {
      if (single[0] == '\0')
         command[kk++] = '?';
      else
         {
         strcpy(&command[kk],single);
         kk = strlen(command);
         }
      }

   if (jswk[loop] >= 0) XtFree(single);
   command[kk++] = ' ';
   }

command[kk] = '\0';


send_comnd:
if (MonitorPid == 0)
   printf("Midas> %s\n",command);
else
   {
   UxPutText(SHelp,"Command sent to Midas.");
   XtSetSensitive(UxGetWidget(pushButton1),FALSE);


/* XFlush(*UxDisplay);  <===  gives trouble on the HP + DECStation!!    */


/* send command to Midas + wait for response */

   kk = AppendDialogText(command);
   if (kk != OKSTAT)
      {
      miderr[0] = -1;
      strcpy(cbuf,"WARNING: ");
      if (kk == NOEXSTAT)
         strcpy(&cbuf[9],"No Midas there to talk to..."); 
      else if (kk == SENDSTAT)
         strcpy(&cbuf[9],"Could not write Midas command file..."); 
      else if (kk == RETSTAT)
         strcpy(&cbuf[9],"Could not read Midas response file...");
      else if (kk == BUSYSTAT)
         strcpy(&cbuf[9],"Midas is busy - resubmit command again...");
      }
   else
      {
      StatMidas("get",miderr,cbuf);		/* get  Midas return status */
      if (miderr[0] == 0) strcpy(cbuf,"all O.K.");
      }
   UxPutText(SHelp,cbuf);

   if ((miderr[0] == 0) && (comndno == 0)) goto begin;

   XtSetSensitive(UxGetWidget(pushButton1),TRUE);
   return;
   }

no_command:
   UxPutText(SHelp,"No command sent to Midas !!!");


}


/*

*/




utila(flag)
int  flag;

{


extern double stepsize[2];

char *cpp;
char cbuf[80];

int  dval[9], null, iav, imnoa;
int  mstatus;

int unit;

float  cuts[4], cutdelt;
double start[2];


/*  for chosen frame read the descriptors */

if (Widget_managed[0] == UxGetWidget(form2))    /* Load Image */
   {
   stepsize[0] = 1.0;
   stepsize[1] = 1.0;

   strcpy(cbuf,UxGetSet(toggleButton1));	/* test DescrVal flag */
   if (cbuf[0] != 't')
      {
      cutdelt = 1.0;
      sprintf(cbuf,"%8.4f",cutdelt);
      UxPutText(text25,cbuf);
      return;
      }

   cpp = XmTextGetString(UxGetWidget(text16));
   mstatus = SCFOPN(cpp,D_OLD_FORMAT,0,F_IMA_TYPE,&imnoa);
   if (mstatus != ERR_NORMAL) 
      {
      XtFree(cpp);
      strcpy(cbuf,"Could not open image...");
      UxPutText(SHelp,cbuf);
      return;
      }
   mstatus = SCDRDI(imnoa,"DISPLAY_DATA",1,9,&iav,dval,&unit,&null);
   if (mstatus != ERR_NORMAL)
      {
      dval[0] = 1;              /* default to scale 1,1 */
      dval[1] = 1;
      }
   else
      {
      if (dval[0] != 1) dval[0] = -dval[0];
      if (dval[1] != 1) dval[1] = -dval[1];
      }
   sprintf(cbuf,"%d",dval[0]);		/* X-scale */
   UxPutText(text39,cbuf);
   sprintf(cbuf,"%d",dval[1]);          /* Y-scale */
   UxPutText(text40,cbuf);

   mstatus = SCDRDD(imnoa,"START",1,2,&iav,start,&unit,&null);
   if (mstatus == ERR_NORMAL)
      mstatus = SCDRDD(imnoa,"STEP",1,2,&iav,stepsize,&unit,&null);
   if (mstatus != ERR_NORMAL)
      {
      strcpy(cbuf,"Descr. START/STEP not found...");
      UxPutText(SHelp,cbuf);
      goto close_it;
      }

   cutdelt = ((dval[2]-1)*stepsize[0]) + start[0];	/* convert to WC */
   sprintf(cbuf,"%8.4f",cutdelt);
   UxPutText(text20,cbuf);
   cutdelt = ((dval[3]-1)*stepsize[1]) + start[1];	/* convert to WC */
   sprintf(cbuf,"%8.4f",cutdelt);
   UxPutText(text24,cbuf);

   mstatus = SCDRDR(imnoa,"LHCUTS",1,4,&iav,cuts,&unit,&null);
   if (mstatus != ERR_NORMAL)
      {
      strcpy(cbuf,"     ");
      UxPutText(text11,cbuf);
      UxPutText(text12,cbuf);
      UxPutText(text15,cbuf);
      UxPutText(text22,cbuf);
      UxPutText(text25,cbuf);
      }
   else
      {
      sprintf(cbuf,"%8.4f",cuts[2]);            /* min + max */
      UxPutText(text11,cbuf);
      sprintf(cbuf,"%8.4f",cuts[3]);
      UxPutText(text12,cbuf);

      sprintf(cbuf,"%8.4f",cuts[0]);            /* low cut */
      UxPutText(text15,cbuf);
      sprintf(cbuf,"%8.4f",cuts[1]);            /* high cut */
      UxPutText(text22,cbuf);

      cutdelt = (cuts[1]-cuts[0])*0.1;
      if (cutdelt <= 10.e-20) cutdelt = 1.0;
      sprintf(cbuf,"%8.4f",cutdelt);
      UxPutText(text25,cbuf);
      }

close_it:
   SCFCLO(imnoa);
   XtFree(cpp);
   }

else if (Widget_managed[0] == UxGetWidget(form5))    /* EZ Load */
   {
   stepsize[0] = 1.0;
   stepsize[1] = 1.0;

   strcpy(cbuf,UxGetSet(toggleButton7));	/* test DescrVal flag */
   if (cbuf[0] != 't')
      {
      cutdelt = 1.0;
      sprintf(cbuf,"%8.4f",cutdelt);
      UxPutText(text36,cbuf);
      return;
      }

   cpp = XmTextGetString(UxGetWidget(text28));
   mstatus = SCFOPN(cpp,D_OLD_FORMAT,0,F_IMA_TYPE,&imnoa);
   if (mstatus != ERR_NORMAL) 
      {
      XtFree(cpp);
      strcpy(cbuf,"Could not open image...");
      UxPutText(SHelp,cbuf);
      return;
      }
   mstatus = SCDRDI(imnoa,"DISPLAY_DATA",1,9,&iav,dval,&unit,&null);
   if (mstatus != ERR_NORMAL)
      {
      dval[0] = 1;              /* default to scale 1,1 */
      dval[1] = 1;
      }
   else
      {
      if (dval[0] != 1) dval[0] = -dval[0];
      if (dval[1] != 1) dval[1] = -dval[1];
      }

   sprintf(cbuf,"%d",dval[0]);		/* X-scale */
   UxPutText(text31,cbuf);
   sprintf(cbuf,"%d",dval[1]);		/* Y-scale */
   UxPutText(text33,cbuf);

   mstatus = SCDRDR(imnoa,"LHCUTS",1,4,&iav,cuts,&unit,&null);
   if (mstatus != ERR_NORMAL)
      {
      strcpy(cbuf,"     ");
      UxPutText(text37,cbuf);
      UxPutText(text38,cbuf);
      UxPutText(text34,cbuf);
      UxPutText(text35,cbuf);
      UxPutText(text36,cbuf);
      }
   else
      {
      sprintf(cbuf,"%8.4f",cuts[2]);            /* min + max */
      UxPutText(text37,cbuf);
      sprintf(cbuf,"%8.4f",cuts[3]);
      UxPutText(text38,cbuf);
  
      sprintf(cbuf,"%8.4f",cuts[0]);            /* low cut */
      UxPutText(text34,cbuf);
      sprintf(cbuf,"%8.4f",cuts[1]);            /* high cut */
      UxPutText(text35,cbuf);
  
      cutdelt = (cuts[1]-cuts[0])*0.1;
      if (cutdelt <= 10.e-20) cutdelt = 1.0;
      sprintf(cbuf,"%8.4f",cutdelt);
      UxPutText(text36,cbuf);
      }

close_ita:
   SCFCLO(imnoa);
   XtFree(cpp);
   }

}

/*

*/

XD_Help(flg)
int  flg;


{

extern int kick[5];

extern Widget help_widget;

extern swidget  swhelp, shtext;

char   help[2000];

kick[4] = 0;				/* remember which help was last */

if (flg == 0)                           /* the general help */
   {
   strcpy(help,
   "The `XDisplay' graphical user interface provides a collection\n ");
   strcat(help,
   "of interfaces for the commonly used Display related MIDAS commands.\n\n");
   strcat(help,
   "At the top, there is a menu bar containing pull-down menus for MIDAS\n");
   strcat(help,
   "commands, the help menu and the QUIT menu for exiting XDisplay.\n");
   strcat(help,
   "You activate a menu by clicking the left mouse button, then click again\n");
   strcat(help,
   "on the desired menu item.\n");
   strcat(help,
   "Below the menu bar is the space for the individual interfaces.\n\n");
   strcat(help,
   "Below that is a `Short Help' field. In here, help text is displayed, when\n");
   strcat(help,
   "you move the mouse over the labels of the interfaces. Also, status\n");
   strcat(help,
"messages from `XDisplay' will be displayed in the `Short Help' field.\n\n");
   strcat(help,
   "Below the `Short Help' field are different `Command' buttons and \nthe ");
   strcat(help,
   "`Apply' button.\n");
   strcat(help,
   "You choose an interface by clicking with the left mouse button on a \n");
   strcat(help,
    "specific command button. ");
  strcat(help,
  "The corresponding interface pops up and\nthe command button is disabled.\n");
  strcat(help,
   "You fill in the desired parameters and execute the command by\n");
   strcat(help,"clicking on the `Apply' button (left mouse button).\n\n");
  strcat(help,
   "Messages about the status of that command will be displayed\n");
  strcat(help,
   "in the `Short Help' field\n\n");
  strcat(help,
 "Not all options of the used MIDAS commands can be accessed via\nXDisplay. ");
  strcat(help,
"Use the MIDAS help to find out about more exotic options of\ne.g. LOAD/IMAGE.");
   }

else if (flg == 2)                           /* the editing help */
   {
   strcpy(help,"Editing a text field is done the Motif-style:\n");
   strcat(help,
"Moving the text cursor around in a text field is done via the arrow keys\n");
   strcat(help,
"of the terminal or by clicking at the desired position.\n");
   strcat(help,
   "If you want to delete a character (done with the `Backspace' key),\n");
   strcat(help,
   "you have to first move the text cursor to the right of that character.\n\n");
   strcat(help,
   "If there are arrows (up/down) next to a field with numerical data, you\n");
   strcat(help,"can click on these arrows to in/decrease the data values.\n");
   strcat(help,"A double click on the arrows changes the speed of the in/decrease.\n");
   }

else if (flg == 3)				/* the selection help */
   {
   strcpy(help,
   "With a File_Selection_Window there are two ways to select a file:\n");
   strcat(help,
 "Click the left mouse button to select a file and return to the interface.\n");
   strcat(help,
   "Click the right mouse button to select a file and immediately send the");
   strcat(help,"\ncommand to MIDAS.\n");
   strcat(help,
   "I.e., in that case there is no need to push the `Apply' button...\n");
   }

else if (flg == 1)				/* the selection help */
   {
   kick[4] = 1;

if (Widget_managed[0] == UxGetWidget(form5))    /* Quick Load */
   {
   strcpy(help,"This is the interface to quickly load an image. \n\n");
   strcat(help,
     "You type/edit the image name in the text field labelled `Frame' or\n");
   strcat(help,
   "click the right button of the mouse in there to get a list of all\n");
   strcat(help,
   "available `bdf' files in your directory.\n\n");
   strcat(help,"If `Use Descr Values' is toggled to `Yes', then the Scale ");
   strcat(help,"and Cut\nvalues are taken from the descriptors of the image.\n");
   strcat(help,"The `MinMax' fields show the physical Minimum,  Maximum");
   strcat(help," of the image\nand cannot be edited.\n \n");
   strcat(help,
     "Type/edit the name of the LUT in the text field labelled `LUT' or click\n");
   strcat(help,"the right button of the mouse in there ");
   strcat(help, "to get a list of all available LUTs.\n\n");
   strcat(help,
     "If you also want to create a display window,");
   strcat(help," specify the size\nin the fileds labelled `SizeX', `SizeY'.\n");
   strcat(help,"If not, toggle the `Create Display' button to `No'. \n");
   strcat(help,"\nThe corresponding MIDAS commands are:\nLOAD/IMA, LOAD/LUT, CREATE/DISPLAY.");
   }

else if (Widget_managed[0] == UxGetWidget(form2))    /* Load Image */
   {
   strcpy(help,
   "This is the interface to load an image into an existing display. \n\n");
   strcat(help,
     "You type/edit the image name in the text field labelled `Frame' \n");
   strcat(help,
   "or click the right button of the mouse in there to get a list of all\n");
   strcat(help,
   "available `bdf' files in your directory.\n\n");
   strcat(help,"If `Use Descr Values' is toggled to `Yes', then the Scale ");
strcat(help,"and Cut\n values are taken from the descriptors of the image.\n");
   strcat(help,"The `MinMax' fields show the physical Minimum,  Maximum");
   strcat(help," of the image\nand cannot be edited.\n \n");
   strcat(help,
   "Modify the center image pixels via the arrows or type the values \n");
   strcat(help,"directly into the text fields. \n");
   strcat(help,"\nThe corresponding MIDAS command is: LOAD/IMAGE.");
   }

else if (Widget_managed[0] == UxGetWidget(form4))    /* Load LUT */
   {
   strcpy(help,"This is the interface to load a LUT and/or ITT. \n\n");
   strcat(help,
     "You type/edit the name of the LUT in the text field labelled `LUT' or\n");
   strcat(help,
   "click the right button of the mouse in there to get a list of all\n");
   strcat(help, "available LUTs.\n");
   strcat(help,
   "If `Display LUT' is toggled to `Yes', then a LUT bar is displayed at");
   strcat(help," the\nbottom of the display.\n\n");
   strcat(help,
     "Type/edit the name of the ITT in the text field labelled `ITT' or click\n");
   strcat(help,
   "the right button of the mouse in there to get a list");
   strcat(help, "of all available ITTs.\n");
   strcat(help,"\nThe corresponding MIDAS commands are: LOAD/LUT, DISPLAY/LUT, LOAD/ITT.");
   }
 

else if (Widget_managed[0] == UxGetWidget(form7))    /* Get Cursor */
   {
   strcpy(help,"This is the interface to obtain image coordinates via ");
   strcat(help,"the cursor (mouse). \n\n");
   strcat(help,"You specify in the field labelled `Out_specs' where ");
   strcat(help,"the results should go\n");
   strcat(help,"to: either a table name, or a descriptor (via descr,DESC) or ");
   strcat(help,"nothing\nif results are just displayed on the terminal.\n");
   strcat(help,
   "If `Mark Position' is toggled to `Yes', then the cursor position ");
   strcat(help,"in the\n");
   strcat(help,"display is marked with a cross or rectangle. \n");
   strcat(help,
   "With `No. Curs' you choose if one or two cursors are going to be used.\n");
   strcat(help,
   "If `Zoom Window' is toggled to `Yes', an additional window is opened;\n");
   strcat(help,"clicking in the main window will choose a region which is");
   strcat(help," zoomed and\nloaded into the zoom window.\n");
   strcat(help,"Cursor coordinates are then picked in that window.\n");
   strcat(help,"\nThe corresponding MIDAS command is: GET/CURSOR.");
   } 


else if (Widget_managed[0] == UxGetWidget(form1))    /* Extract Line */
   {
   strcpy(help,"This is the interface to extract a 1-dim line from the ");
   strcat(help,"displayed image\nusing the mouse + arrow keys. \n\n");
   strcat(help,"You specify in the field labelled `Out_Frame' the ");
   strcat(help,"name of the resulting\n1-dim image (defaulted to `trace'). \n");
   strcat(help,"With the menu labelled `Plot_Flag' you choose if the trace ");
   strcat(help,
   "is plotted\neither in the overlay plane of the display (Draw),\n");
   strcat(help,"or in the graphics window (Plot),\nor not at all (None).\n");
   strcat(help,
   "If `Cut_Option' is toggled to `NoCut', then the trace extends through\n");
   strcat(help,"the two cursor points to the ");
   strcat(help,"borders of the image\n");
   strcat(help,"if toggled to `Cut' only the line between the two cursors ");
   strcat(help,
   "is extracted.\n");
   strcat(help,"In the field labelled `Stepsize' you set the sampling size ");
   strcat(help,"along the\nextracted line ");
   strcat(help,"(defaulted to the Stepsize in X of the displayed frame). \n");
   strcat(help,"\nThe corresponding MIDAS command is: EXTRACT/TRACE.");
   }

else if (Widget_managed[0] == UxGetWidget(form8))    /* Modify LUT */
   {
   strcpy(help,
   "This is the interface to interactively modify the current LUT and/or ITT. \n");
   strcat(help,
   "Do not forget, that the cursor must be inside the image display.\n\n");
   strcat(help,"With the menu labelled `LUT Method' you choose the method ");
   strcat(help,"used for\nthe LUT modification. ");
   strcat(help,
   "And with the menu `LUT Colour' you specify\nthe colour to which ");
   strcat(help,"this method is applied.\n");
   strcat(help,"With the menu `ITT Method' you choose the method ");
   strcat(help,"used for the ITT\nmodification. ");
   strcat(help,
   "And in the field labelled `ITT value' you specify the value\nwhich ");
   strcat(help,"is used in the chosen method.\n");
   strcat(help,
   "The toggle buttons to the right indicate which command is executed,\n");
   strcat(help,
   "either the LUT modification, or the ITT modfication, or both.\n");
   strcat(help,
    "\nThe corresponding MIDAS commands are: MODIFY/LUT, MODIFY/ITT.");
   }

else if (Widget_managed[0] == UxGetWidget(form3))    /* Explore Image */
   {
   strcpy(help,
   "This is the interface to interactively explore an image in detail.\n\n");
   strcat(help,
     "If you leave the text field labelled `Frame' blank, the currently");   
   strcat(help,
   " displayed\nframe will be used. ");
   strcat(help,
     "You can type/edit the image name in the text field\nlabelled `Frame' ");
   strcat(help,
   "or click the right button of the mouse in there to get a list\n");
   strcat(help,
   "of all available `bdf' files in your directory.\n");
   strcat(help,
   "If `Plot_Option' is toggled to `Plot', histograms, line/column traces,");
   strcat(help,
" etc.\nare plotted in a graphics window, else nothing is plotted.\n");
   strcat(help,
   "If `File_Format' is toggled to `FITS', the input frame specified in the");
   strcat(help,
"\n`Frame' field should be in FITS format else it should be a MIDAS file.\n");
   strcat(help,"In the field labelled `Out_Table' you specify an optional ");
   strcat(help,
"table name for\nsaving all image coordinates you click on in that table.\n");
   strcat(help,
    "\nThe corresponding MIDAS command is: VIEW/IMAGE.");
   }

else if (Widget_managed[0] == UxGetWidget(form6))    /* Create Displ/Graphics */
   {
   strcpy(help,
   "This is the interface to create a display or graphics window.\n\n");
   strcat(help,
   "Toggle `Type' to Display or Graphics, in order to create a display or\n");
   strcat(help,"graphics window.\n");
   strcat(help,"In the `Window_ID' field the display or graphics ID is set. ");
   strcat(help,
"Specify the\nsize and offset of the window via the fields which are");
   strcat(help,
" labelled `SizeX',\n`SizeY', `OffsetX' and `OffsetY'.\n");
   strcat(help,
"Only for a display window toggle `Alpha_Memory' to get an alphanumeric\n");
   strcat(help, "field at the bottom of the display or not.\n");
   strcat(help,
 "The field labelled `Gsize' holds the no. of graphic segments saved for the\n");
   strcat(help,
"refreshing of the window. This value has to be increased to e.g. 100000\n");
   strcat(help, "for very big spectra.\n");
   strcat(help,
"To create a display/graphics window on a different display, edit\n`Xstation'");
   strcat(help, " to specify that.\n");
   strcat(help,
    "\nThe corresponding MIDAS commands are: CREATE/DISPLAY,\nCREATE/GRAPHICS.");
   }

   } 					/* flag = 1 section end */

else
   return;

UxPutText(shtext,help);

UxPopupInterface(swhelp,no_grab);

}

