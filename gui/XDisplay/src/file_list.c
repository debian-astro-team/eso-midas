/* @(#)file_list.c	19.1 (ES0-DMD) 02/25/03 13:44:21 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	file_list.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxfile_list;
	swidget	Uxform12;
	swidget	UxpushButton29;
	swidget	UxscrolledWindow1;
	swidget	Uxls_init;
} _UxCfile_list;

#define file_list               UxFile_listContext->Uxfile_list
#define form12                  UxFile_listContext->Uxform12
#define pushButton29            UxFile_listContext->UxpushButton29
#define scrolledWindow1         UxFile_listContext->UxscrolledWindow1
#define ls_init                 UxFile_listContext->Uxls_init

static _UxCfile_list	*UxFile_listContext;


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*Browse = "#augment\n\
<Btn3Down>:ListBeginSelect()\n\
<Btn3Up>:ListEndSelect()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_file_list();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton29( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_list           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_listContext;
	UxFile_listContext = UxContext =
			(_UxCfile_list *) UxGetContext( UxThisWidget );
	{
	extern swidget swfs;
	
	UxPopdownInterface(swfs);
	}
	UxFile_listContext = UxSaveCtx;
}

static void	createCB_ls_init( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_list           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_listContext;
	UxFile_listContext = UxContext =
			(_UxCfile_list *) UxGetContext( UxThisWidget );
	{
	extern Widget filelist_widget;
	
	filelist_widget = UxWidget;
	}
	UxFile_listContext = UxSaveCtx;
}

static void	browseSelectionCB_ls_init( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_list           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_listContext;
	UxFile_listContext = UxContext =
			(_UxCfile_list *) UxGetContext( UxThisWidget );
	{
	
	/*  browselect.c' holds this Callback code */
	
	char *choice;
	char cbuf[80];
	
	
	XButtonEvent  *ev;
	XmListCallbackStruct *cbs;
	
	extern swidget swfs, stxt;
	
	
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	ev = (XButtonEvent *) cbs->event;	/* test originating event */
	
	
	UxPutValue(stxt,choice);
	XtFree(choice);
	
	UxPopdownInterface(swfs);
	
	utila(0);		/* if LOAD/IMAGE, get descriptors */
	
	if (ev->button == 3) applycom(0);
	
	}
	UxFile_listContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_file_list()
{
	UxPutGeometry( file_list, "" );
	UxPutBackground( file_list, TextBackground );
	UxPutKeyboardFocusPolicy( file_list, "pointer" );
	UxPutIconName( file_list, "List selection" );
	UxPutHeight( file_list, 353 );
	UxPutWidth( file_list, 290 );
	UxPutY( file_list, 300 );
	UxPutX( file_list, 330 );

	UxPutBackground( form12, ApplicBackground );
	UxPutHeight( form12, 240 );
	UxPutWidth( form12, 290 );
	UxPutY( form12, 0 );
	UxPutX( form12, 0 );
	UxPutUnitType( form12, "pixels" );
	UxPutResizePolicy( form12, "resize_none" );

	UxPutForeground( pushButton29, CancelForeground );
	UxPutLabelString( pushButton29, "cancel" );
	UxPutFontList( pushButton29, BoldTextFont );
	UxPutBackground( pushButton29, ButtonBackground );
	UxPutHeight( pushButton29, 35 );
	UxPutWidth( pushButton29, 210 );
	UxPutY( pushButton29, 318 );
	UxPutX( pushButton29, 0 );

	UxPutSpacing( scrolledWindow1, 2 );
	UxPutScrolledWindowMarginWidth( scrolledWindow1, 1 );
	UxPutScrolledWindowMarginHeight( scrolledWindow1, 1 );
	UxPutForeground( scrolledWindow1, TextBackground );
	UxPutTranslations( scrolledWindow1, "" );
	UxPutWidth( scrolledWindow1, 270 );
	UxPutHeight( scrolledWindow1, 316 );
	UxPutBackground( scrolledWindow1, ApplicBackground );
	UxPutScrollBarPlacement( scrolledWindow1, "bottom_left" );
	UxPutShadowThickness( scrolledWindow1, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow1, "static" );
	UxPutVisualPolicy( scrolledWindow1, "variable" );
	UxPutY( scrolledWindow1, 2 );
	UxPutX( scrolledWindow1, 0 );
	UxPutScrollingPolicy( scrolledWindow1, "application_defined" );

	UxPutListSpacing( ls_init, 1 );
	UxPutListMarginWidth( ls_init, 2 );
	UxPutListMarginHeight( ls_init, 1 );
	UxPutHighlightThickness( ls_init, 0 );
	UxPutTranslations( ls_init, Browse );
	UxPutForeground( ls_init, TextForeground );
	UxPutVisibleItemCount( ls_init, 18 );
	UxPutFontList( ls_init, TextFont );
	UxPutBackground( ls_init, TextBackground );
	UxPutListSizePolicy( ls_init, "variable" );
	UxPutScrollBarDisplayPolicy( ls_init, "static" );
	UxPutHeight( ls_init, 184 );
	UxPutWidth( ls_init, 220 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_file_list()
{
	/* Create the swidgets */

	file_list = UxCreateTopLevelShell( "file_list", NO_PARENT );
	UxPutContext( file_list, UxFile_listContext );

	form12 = UxCreateForm( "form12", file_list );
	pushButton29 = UxCreatePushButton( "pushButton29", form12 );
	scrolledWindow1 = UxCreateScrolledWindow( "scrolledWindow1", form12 );
	ls_init = UxCreateScrolledList( "ls_init", scrolledWindow1 );

	_Uxinit_file_list();

	/* Create the X widgets */

	UxCreateWidget( file_list );
	UxCreateWidget( form12 );
	UxPutLeftOffset( pushButton29, 10 );
	UxPutLeftAttachment( pushButton29, "attach_form" );
	UxPutBottomOffset( pushButton29, 5 );
	UxPutBottomAttachment( pushButton29, "attach_form" );
	UxPutTopOffset( pushButton29, 5 );
	UxPutTopAttachment( pushButton29, "attach_none" );
	UxPutRightOffset( pushButton29, 10 );
	UxPutRightAttachment( pushButton29, "attach_form" );
	UxCreateWidget( pushButton29 );

	UxPutTopOffset( scrolledWindow1, 0 );
	UxPutTopAttachment( scrolledWindow1, "attach_form" );
	UxPutBottomOffset( scrolledWindow1, 45 );
	UxPutBottomAttachment( scrolledWindow1, "attach_form" );
	UxPutLeftOffset( scrolledWindow1, 2 );
	UxPutLeftAttachment( scrolledWindow1, "attach_form" );
	UxPutRightOffset( scrolledWindow1, 2 );
	UxPutRightAttachment( scrolledWindow1, "attach_form" );
	UxCreateWidget( scrolledWindow1 );

	UxCreateWidget( ls_init );
	createCB_ls_init( UxGetWidget( ls_init ),
			(XtPointer) UxFile_listContext, (XtPointer) NULL );


	UxAddCallback( pushButton29, XmNactivateCallback,
			activateCB_pushButton29,
			(XtPointer) UxFile_listContext );

	UxAddCallback( ls_init, XmNbrowseSelectionCallback,
			browseSelectionCB_ls_init,
			(XtPointer) UxFile_listContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( file_list );

	return ( file_list );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_file_list()
{
	swidget                 rtrn;
	_UxCfile_list           *UxContext;

	UxFile_listContext = UxContext =
		(_UxCfile_list *) UxMalloc( sizeof(_UxCfile_list) );

	rtrn = _Uxbuild_file_list();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_file_list()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_file_list();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

