/*===========================================================================
  Copyright (C) 1995-2006 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ApplicWindow.c

	Associated Resource file: ApplicWindow.rf

.VERSION
 060203		last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "UxLib.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxPushBG.h"
#include "UxPushB.h"
#include "UxTogB.h"
#include "UxArrB.h"
#include "UxRowCol.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxFrame.h"
#include "UxForm.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <Xm/Protocols.h>
#include "UxFsBox.h"

extern Widget Widget_managed[2];

extern Widget Button_insensitive;


/* Include for UxPut calls on the fileSelectionBox */

void CreateWindowManagerProtocols();
/* void ExitCB(); */
void CreateSessionManagerProtocols();
void SaveSessionCB();

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxmainWindow;
	swidget	UxworkAreaForm;
	swidget	UxworkAreaFrame;
	swidget	UxworkArea;
	swidget	Uxform2;
	swidget	Uxlabel14;
	swidget	Uxtext16;
	swidget	Uxlabel27;
	swidget	Uxlabel24;
	swidget	Uxlabel25;
	swidget	Uxlabel15;
	swidget	UxrowColumn13;
	swidget	UxrowColumn20;
	swidget	UxarrowButton5;
	swidget	UxarrowButton6;
	swidget	Uxtext2;
	swidget	Uxlabel32;
	swidget	UxrowColumn18;
	swidget	Uxtext20;
	swidget	UxarrowButton22;
	swidget	UxarrowButton21;
	swidget	UxarrowButton23;
	swidget	UxarrowButton24;
	swidget	Uxtext24;
	swidget	UxrowColumn31;
	swidget	Uxtext15;
	swidget	UxrowColumn32;
	swidget	UxarrowButton25;
	swidget	UxarrowButton26;
	swidget	Uxlabel28;
	swidget	UxrowColumn33;
	swidget	Uxtext22;
	swidget	UxrowColumn34;
	swidget	UxarrowButton27;
	swidget	UxarrowButton28;
	swidget	Uxlabel35;
	swidget	UxtoggleButton1;
	swidget	Uxlabel36;
	swidget	Uxtext25;
	swidget	Uxlabel10;
	swidget	UxrowColumn17;
	swidget	Uxtext11;
	swidget	Uxtext12;
	swidget	UxrowColumn29;
	swidget	UxrowColumn30;
	swidget	UxarrowButton39;
	swidget	UxarrowButton40;
	swidget	Uxtext39;
	swidget	Uxlabel52;
	swidget	UxrowColumn44;
	swidget	UxrowColumn45;
	swidget	UxarrowButton45;
	swidget	UxarrowButton46;
	swidget	Uxtext40;
	swidget	UxrowColumn22;
	swidget	UxrowColumn28;
	swidget	UxarrowButton47;
	swidget	UxarrowButton48;
	swidget	Uxtext41;
	swidget	Uxlabel53;
	swidget	UxSHelp;
	swidget	UxpushButton1;
	swidget	Uxform4;
	swidget	Uxlabel2;
	swidget	Uxtext9;
	swidget	Uxlabel1;
	swidget	UxtoggleButton10;
	swidget	Uxlabel26;
	swidget	Uxtext23;
	swidget	Uxform3;
	swidget	Uxlabel9;
	swidget	Uxtext10;
	swidget	Uxlabel13;
	swidget	Uxtext17;
	swidget	Uxlabel11;
	swidget	Uxlabel12;
	swidget	UxtoggleButton2;
	swidget	UxtoggleButton4;
	swidget	Uxform6;
	swidget	Uxlabel16;
	swidget	Uxlabel17;
	swidget	Uxlabel5;
	swidget	Uxlabel19;
	swidget	Uxlabel20;
	swidget	Uxlabel21;
	swidget	Uxtext21;
	swidget	UxrowColumn1;
	swidget	UxrowColumn9;
	swidget	UxarrowButton7;
	swidget	UxarrowButton8;
	swidget	Uxtext1;
	swidget	UxrowColumn2;
	swidget	UxrowColumn10;
	swidget	UxarrowButton3;
	swidget	UxarrowButton4;
	swidget	Uxtext4;
	swidget	Uxlabel18;
	swidget	UxrowColumn3;
	swidget	UxrowColumn11;
	swidget	UxarrowButton9;
	swidget	UxarrowButton10;
	swidget	Uxtext5;
	swidget	UxrowColumn5;
	swidget	UxrowColumn12;
	swidget	UxarrowButton11;
	swidget	UxarrowButton12;
	swidget	Uxtext6;
	swidget	Uxlabel22;
	swidget	UxrowColumn4;
	swidget	UxrowColumn14;
	swidget	UxarrowButton17;
	swidget	UxarrowButton18;
	swidget	Uxtext8;
	swidget	UxtoggleButton25;
	swidget	UxrowColumn6;
	swidget	UxrowColumn7;
	swidget	UxarrowButton13;
	swidget	UxarrowButton14;
	swidget	Uxtext13;
	swidget	Uxlabel54;
	swidget	UxtoggleButton15;
	swidget	Uxform7;
	swidget	Uxlabel3;
	swidget	Uxtext3;
	swidget	Uxlabel7;
	swidget	Uxlabel8;
	swidget	Uxlabel4;
	swidget	UxtoggleButton8;
	swidget	UxtoggleButton9;
	swidget	UxtoggleButton14;
	swidget	UxpushButton6;
	swidget	Uxform1;
	swidget	Uxlabel6;
	swidget	Uxtext7;
	swidget	Uxlabel31;
	swidget	Uxplotflag_p1;
	swidget	Uxplotflag_p1_b1;
	swidget	Uxplotflag_p1_b2;
	swidget	Uxplotflag_p1_b3;
	swidget	Uxplotflag;
	swidget	Uxlabel29;
	swidget	Uxlabel30;
	swidget	Uxtext14;
	swidget	UxtoggleButton6;
	swidget	Uxform8;
	swidget	Uxlabel39;
	swidget	Uxlabel40;
	swidget	Uxmethod_p3;
	swidget	Uxmethod_p1_b9;
	swidget	Uxmethod_p1_b10;
	swidget	Uxmethod_p1_b11;
	swidget	Uxmethod_p1_b12;
	swidget	Uxmethod2;
	swidget	Uxcolor_p2;
	swidget	Uxcolor_p1_b12;
	swidget	Uxcolor_p1_b13;
	swidget	Uxcolor_p1_b14;
	swidget	Uxcolor_p1_b15;
	swidget	Uxcolor_p1_b16;
	swidget	Uxcolor_p1_b17;
	swidget	Uxcolor_p1_b18;
	swidget	Uxcolor_p1_b19;
	swidget	Uxcolor_p1_b20;
	swidget	Uxcolor_p1_b21;
	swidget	Uxcolor_p1_b22;
	swidget	Uxcolor1;
	swidget	Uxseparator1;
	swidget	Uxlabel23;
	swidget	Uxmethod_p1;
	swidget	Uxmethod_p1_b1;
	swidget	Uxmethod_p1_b2;
	swidget	Uxmethod_p1_b3;
	swidget	Uxmethod_p1_b4;
	swidget	Uxmethod1;
	swidget	Uxlabel33;
	swidget	UxrowColumn19;
	swidget	UxrowColumn21;
	swidget	UxarrowButton1;
	swidget	UxarrowButton2;
	swidget	Uxtext27;
	swidget	UxtoggleButton3;
	swidget	UxtoggleButton5;
	swidget	UxpushButton2;
	swidget	UxpushButton9;
	swidget	UxpushButton4;
	swidget	UxpushButton7;
	swidget	UxpushButton3;
	swidget	UxpushButton8;
	swidget	UxpushButton5;
	swidget	Uxform5;
	swidget	Uxlabel37;
	swidget	Uxtext28;
	swidget	Uxlabel41;
	swidget	UxrowColumn35;
	swidget	UxrowColumn36;
	swidget	UxarrowButton35;
	swidget	UxarrowButton36;
	swidget	Uxtext31;
	swidget	Uxlabel45;
	swidget	UxrowColumn40;
	swidget	Uxtext35;
	swidget	UxrowColumn41;
	swidget	UxarrowButton43;
	swidget	UxarrowButton44;
	swidget	Uxlabel46;
	swidget	UxtoggleButton7;
	swidget	Uxlabel47;
	swidget	Uxtext36;
	swidget	Uxlabel48;
	swidget	UxrowColumn42;
	swidget	Uxtext37;
	swidget	Uxtext38;
	swidget	Uxlabel38;
	swidget	UxrowColumn38;
	swidget	Uxtext34;
	swidget	UxrowColumn39;
	swidget	UxarrowButton41;
	swidget	UxarrowButton42;
	swidget	Uxlabel42;
	swidget	Uxtext29;
	swidget	Uxlabel43;
	swidget	UxtoggleButton13;
	swidget	Uxlabel44;
	swidget	UxrowColumn23;
	swidget	UxrowColumn24;
	swidget	UxarrowButton29;
	swidget	UxarrowButton30;
	swidget	Uxtext30;
	swidget	Uxlabel49;
	swidget	UxrowColumn25;
	swidget	UxrowColumn26;
	swidget	UxarrowButton33;
	swidget	UxarrowButton34;
	swidget	Uxtext32;
	swidget	Uxlabel50;
	swidget	Uxlabel51;
	swidget	UxrowColumn37;
	swidget	UxrowColumn43;
	swidget	UxarrowButton37;
	swidget	UxarrowButton38;
	swidget	Uxtext33;
	swidget	UxXDMenus;
	swidget	UxcommandPane;
	swidget	Uxcommand_b2;
	swidget	Uxcommand_b3;
	swidget	UxcommandPane_b17;
	swidget	Uxcommand_b9;
	swidget	Uxcommand_b10;
	swidget	UxcommandPane_b14;
	swidget	UxcommandPane_b15;
	swidget	UxcommandPane_b16;
	swidget	UxcommandsCascade;
	swidget	UxLUTCommsPane;
	swidget	UxLUTcomm_b1;
	swidget	UxLUTcomm_b2;
	swidget	UxLUTcomm_b3;
	swidget	UxLUTcomm_b4;
	swidget	UxLUTcomm_b5;
	swidget	UxLUTcomm_b6;
	swidget	UxLUTCommsCascade;
	swidget	UxhelpPane;
	swidget	UxXDHelp;
	swidget	UxInterfHelp;
	swidget	UxEditHelp;
	swidget	UxSelectHelp;
	swidget	UxhelpCascade;
	swidget	UxquitPane;
	swidget	Uxquit_b1;
	swidget	UxquitCascade;
} _UxCApplicWindow;

#define mainWindow              UxApplicWindowContext->UxmainWindow
#define workAreaForm            UxApplicWindowContext->UxworkAreaForm
#define workAreaFrame           UxApplicWindowContext->UxworkAreaFrame
#define workArea                UxApplicWindowContext->UxworkArea
#define form2                   UxApplicWindowContext->Uxform2
#define label14                 UxApplicWindowContext->Uxlabel14
#define text16                  UxApplicWindowContext->Uxtext16
#define label27                 UxApplicWindowContext->Uxlabel27
#define label24                 UxApplicWindowContext->Uxlabel24
#define label25                 UxApplicWindowContext->Uxlabel25
#define label15                 UxApplicWindowContext->Uxlabel15
#define rowColumn13             UxApplicWindowContext->UxrowColumn13
#define rowColumn20             UxApplicWindowContext->UxrowColumn20
#define arrowButton5            UxApplicWindowContext->UxarrowButton5
#define arrowButton6            UxApplicWindowContext->UxarrowButton6
#define text2                   UxApplicWindowContext->Uxtext2
#define label32                 UxApplicWindowContext->Uxlabel32
#define rowColumn18             UxApplicWindowContext->UxrowColumn18
#define text20                  UxApplicWindowContext->Uxtext20
#define arrowButton22           UxApplicWindowContext->UxarrowButton22
#define arrowButton21           UxApplicWindowContext->UxarrowButton21
#define arrowButton23           UxApplicWindowContext->UxarrowButton23
#define arrowButton24           UxApplicWindowContext->UxarrowButton24
#define text24                  UxApplicWindowContext->Uxtext24
#define rowColumn31             UxApplicWindowContext->UxrowColumn31
#define text15                  UxApplicWindowContext->Uxtext15
#define rowColumn32             UxApplicWindowContext->UxrowColumn32
#define arrowButton25           UxApplicWindowContext->UxarrowButton25
#define arrowButton26           UxApplicWindowContext->UxarrowButton26
#define label28                 UxApplicWindowContext->Uxlabel28
#define rowColumn33             UxApplicWindowContext->UxrowColumn33
#define text22                  UxApplicWindowContext->Uxtext22
#define rowColumn34             UxApplicWindowContext->UxrowColumn34
#define arrowButton27           UxApplicWindowContext->UxarrowButton27
#define arrowButton28           UxApplicWindowContext->UxarrowButton28
#define label35                 UxApplicWindowContext->Uxlabel35
#define toggleButton1           UxApplicWindowContext->UxtoggleButton1
#define label36                 UxApplicWindowContext->Uxlabel36
#define text25                  UxApplicWindowContext->Uxtext25
#define label10                 UxApplicWindowContext->Uxlabel10
#define rowColumn17             UxApplicWindowContext->UxrowColumn17
#define text11                  UxApplicWindowContext->Uxtext11
#define text12                  UxApplicWindowContext->Uxtext12
#define rowColumn29             UxApplicWindowContext->UxrowColumn29
#define rowColumn30             UxApplicWindowContext->UxrowColumn30
#define arrowButton39           UxApplicWindowContext->UxarrowButton39
#define arrowButton40           UxApplicWindowContext->UxarrowButton40
#define text39                  UxApplicWindowContext->Uxtext39
#define label52                 UxApplicWindowContext->Uxlabel52
#define rowColumn44             UxApplicWindowContext->UxrowColumn44
#define rowColumn45             UxApplicWindowContext->UxrowColumn45
#define arrowButton45           UxApplicWindowContext->UxarrowButton45
#define arrowButton46           UxApplicWindowContext->UxarrowButton46
#define text40                  UxApplicWindowContext->Uxtext40
#define rowColumn22             UxApplicWindowContext->UxrowColumn22
#define rowColumn28             UxApplicWindowContext->UxrowColumn28
#define arrowButton47           UxApplicWindowContext->UxarrowButton47
#define arrowButton48           UxApplicWindowContext->UxarrowButton48
#define text41                  UxApplicWindowContext->Uxtext41
#define label53                 UxApplicWindowContext->Uxlabel53
#define SHelp                   UxApplicWindowContext->UxSHelp
#define pushButton1             UxApplicWindowContext->UxpushButton1
#define form4                   UxApplicWindowContext->Uxform4
#define label2                  UxApplicWindowContext->Uxlabel2
#define text9                   UxApplicWindowContext->Uxtext9
#define label1                  UxApplicWindowContext->Uxlabel1
#define toggleButton10          UxApplicWindowContext->UxtoggleButton10
#define label26                 UxApplicWindowContext->Uxlabel26
#define text23                  UxApplicWindowContext->Uxtext23
#define form3                   UxApplicWindowContext->Uxform3
#define label9                  UxApplicWindowContext->Uxlabel9
#define text10                  UxApplicWindowContext->Uxtext10
#define label13                 UxApplicWindowContext->Uxlabel13
#define text17                  UxApplicWindowContext->Uxtext17
#define label11                 UxApplicWindowContext->Uxlabel11
#define label12                 UxApplicWindowContext->Uxlabel12
#define toggleButton2           UxApplicWindowContext->UxtoggleButton2
#define toggleButton4           UxApplicWindowContext->UxtoggleButton4
#define form6                   UxApplicWindowContext->Uxform6
#define label16                 UxApplicWindowContext->Uxlabel16
#define label17                 UxApplicWindowContext->Uxlabel17
#define label5                  UxApplicWindowContext->Uxlabel5
#define label19                 UxApplicWindowContext->Uxlabel19
#define label20                 UxApplicWindowContext->Uxlabel20
#define label21                 UxApplicWindowContext->Uxlabel21
#define text21                  UxApplicWindowContext->Uxtext21
#define rowColumn1              UxApplicWindowContext->UxrowColumn1
#define rowColumn9              UxApplicWindowContext->UxrowColumn9
#define arrowButton7            UxApplicWindowContext->UxarrowButton7
#define arrowButton8            UxApplicWindowContext->UxarrowButton8
#define text1                   UxApplicWindowContext->Uxtext1
#define rowColumn2              UxApplicWindowContext->UxrowColumn2
#define rowColumn10             UxApplicWindowContext->UxrowColumn10
#define arrowButton3            UxApplicWindowContext->UxarrowButton3
#define arrowButton4            UxApplicWindowContext->UxarrowButton4
#define text4                   UxApplicWindowContext->Uxtext4
#define label18                 UxApplicWindowContext->Uxlabel18
#define rowColumn3              UxApplicWindowContext->UxrowColumn3
#define rowColumn11             UxApplicWindowContext->UxrowColumn11
#define arrowButton9            UxApplicWindowContext->UxarrowButton9
#define arrowButton10           UxApplicWindowContext->UxarrowButton10
#define text5                   UxApplicWindowContext->Uxtext5
#define rowColumn5              UxApplicWindowContext->UxrowColumn5
#define rowColumn12             UxApplicWindowContext->UxrowColumn12
#define arrowButton11           UxApplicWindowContext->UxarrowButton11
#define arrowButton12           UxApplicWindowContext->UxarrowButton12
#define text6                   UxApplicWindowContext->Uxtext6
#define label22                 UxApplicWindowContext->Uxlabel22
#define rowColumn4              UxApplicWindowContext->UxrowColumn4
#define rowColumn14             UxApplicWindowContext->UxrowColumn14
#define arrowButton17           UxApplicWindowContext->UxarrowButton17
#define arrowButton18           UxApplicWindowContext->UxarrowButton18
#define text8                   UxApplicWindowContext->Uxtext8
#define toggleButton25          UxApplicWindowContext->UxtoggleButton25
#define rowColumn6              UxApplicWindowContext->UxrowColumn6
#define rowColumn7              UxApplicWindowContext->UxrowColumn7
#define arrowButton13           UxApplicWindowContext->UxarrowButton13
#define arrowButton14           UxApplicWindowContext->UxarrowButton14
#define text13                  UxApplicWindowContext->Uxtext13
#define label54                 UxApplicWindowContext->Uxlabel54
#define toggleButton15          UxApplicWindowContext->UxtoggleButton15
#define form7                   UxApplicWindowContext->Uxform7
#define label3                  UxApplicWindowContext->Uxlabel3
#define text3                   UxApplicWindowContext->Uxtext3
#define label7                  UxApplicWindowContext->Uxlabel7
#define label8                  UxApplicWindowContext->Uxlabel8
#define label4                  UxApplicWindowContext->Uxlabel4
#define toggleButton8           UxApplicWindowContext->UxtoggleButton8
#define toggleButton9           UxApplicWindowContext->UxtoggleButton9
#define toggleButton14          UxApplicWindowContext->UxtoggleButton14
#define pushButton6             UxApplicWindowContext->UxpushButton6
#define form1                   UxApplicWindowContext->Uxform1
#define label6                  UxApplicWindowContext->Uxlabel6
#define text7                   UxApplicWindowContext->Uxtext7
#define label31                 UxApplicWindowContext->Uxlabel31
#define plotflag_p1             UxApplicWindowContext->Uxplotflag_p1
#define plotflag_p1_b1          UxApplicWindowContext->Uxplotflag_p1_b1
#define plotflag_p1_b2          UxApplicWindowContext->Uxplotflag_p1_b2
#define plotflag_p1_b3          UxApplicWindowContext->Uxplotflag_p1_b3
#define plotflag                UxApplicWindowContext->Uxplotflag
#define label29                 UxApplicWindowContext->Uxlabel29
#define label30                 UxApplicWindowContext->Uxlabel30
#define text14                  UxApplicWindowContext->Uxtext14
#define toggleButton6           UxApplicWindowContext->UxtoggleButton6
#define form8                   UxApplicWindowContext->Uxform8
#define label39                 UxApplicWindowContext->Uxlabel39
#define label40                 UxApplicWindowContext->Uxlabel40
#define method_p3               UxApplicWindowContext->Uxmethod_p3
#define method_p1_b9            UxApplicWindowContext->Uxmethod_p1_b9
#define method_p1_b10           UxApplicWindowContext->Uxmethod_p1_b10
#define method_p1_b11           UxApplicWindowContext->Uxmethod_p1_b11
#define method_p1_b12           UxApplicWindowContext->Uxmethod_p1_b12
#define method2                 UxApplicWindowContext->Uxmethod2
#define color_p2                UxApplicWindowContext->Uxcolor_p2
#define color_p1_b12            UxApplicWindowContext->Uxcolor_p1_b12
#define color_p1_b13            UxApplicWindowContext->Uxcolor_p1_b13
#define color_p1_b14            UxApplicWindowContext->Uxcolor_p1_b14
#define color_p1_b15            UxApplicWindowContext->Uxcolor_p1_b15
#define color_p1_b16            UxApplicWindowContext->Uxcolor_p1_b16
#define color_p1_b17            UxApplicWindowContext->Uxcolor_p1_b17
#define color_p1_b18            UxApplicWindowContext->Uxcolor_p1_b18
#define color_p1_b19            UxApplicWindowContext->Uxcolor_p1_b19
#define color_p1_b20            UxApplicWindowContext->Uxcolor_p1_b20
#define color_p1_b21            UxApplicWindowContext->Uxcolor_p1_b21
#define color_p1_b22            UxApplicWindowContext->Uxcolor_p1_b22
#define color1                  UxApplicWindowContext->Uxcolor1
#define separator1              UxApplicWindowContext->Uxseparator1
#define label23                 UxApplicWindowContext->Uxlabel23
#define method_p1               UxApplicWindowContext->Uxmethod_p1
#define method_p1_b1            UxApplicWindowContext->Uxmethod_p1_b1
#define method_p1_b2            UxApplicWindowContext->Uxmethod_p1_b2
#define method_p1_b3            UxApplicWindowContext->Uxmethod_p1_b3
#define method_p1_b4            UxApplicWindowContext->Uxmethod_p1_b4
#define method1                 UxApplicWindowContext->Uxmethod1
#define label33                 UxApplicWindowContext->Uxlabel33
#define rowColumn19             UxApplicWindowContext->UxrowColumn19
#define rowColumn21             UxApplicWindowContext->UxrowColumn21
#define arrowButton1            UxApplicWindowContext->UxarrowButton1
#define arrowButton2            UxApplicWindowContext->UxarrowButton2
#define text27                  UxApplicWindowContext->Uxtext27
#define toggleButton3           UxApplicWindowContext->UxtoggleButton3
#define toggleButton5           UxApplicWindowContext->UxtoggleButton5
#define pushButton2             UxApplicWindowContext->UxpushButton2
#define pushButton9             UxApplicWindowContext->UxpushButton9
#define pushButton4             UxApplicWindowContext->UxpushButton4
#define pushButton7             UxApplicWindowContext->UxpushButton7
#define pushButton3             UxApplicWindowContext->UxpushButton3
#define pushButton8             UxApplicWindowContext->UxpushButton8
#define pushButton5             UxApplicWindowContext->UxpushButton5
#define form5                   UxApplicWindowContext->Uxform5
#define label37                 UxApplicWindowContext->Uxlabel37
#define text28                  UxApplicWindowContext->Uxtext28
#define label41                 UxApplicWindowContext->Uxlabel41
#define rowColumn35             UxApplicWindowContext->UxrowColumn35
#define rowColumn36             UxApplicWindowContext->UxrowColumn36
#define arrowButton35           UxApplicWindowContext->UxarrowButton35
#define arrowButton36           UxApplicWindowContext->UxarrowButton36
#define text31                  UxApplicWindowContext->Uxtext31
#define label45                 UxApplicWindowContext->Uxlabel45
#define rowColumn40             UxApplicWindowContext->UxrowColumn40
#define text35                  UxApplicWindowContext->Uxtext35
#define rowColumn41             UxApplicWindowContext->UxrowColumn41
#define arrowButton43           UxApplicWindowContext->UxarrowButton43
#define arrowButton44           UxApplicWindowContext->UxarrowButton44
#define label46                 UxApplicWindowContext->Uxlabel46
#define toggleButton7           UxApplicWindowContext->UxtoggleButton7
#define label47                 UxApplicWindowContext->Uxlabel47
#define text36                  UxApplicWindowContext->Uxtext36
#define label48                 UxApplicWindowContext->Uxlabel48
#define rowColumn42             UxApplicWindowContext->UxrowColumn42
#define text37                  UxApplicWindowContext->Uxtext37
#define text38                  UxApplicWindowContext->Uxtext38
#define label38                 UxApplicWindowContext->Uxlabel38
#define rowColumn38             UxApplicWindowContext->UxrowColumn38
#define text34                  UxApplicWindowContext->Uxtext34
#define rowColumn39             UxApplicWindowContext->UxrowColumn39
#define arrowButton41           UxApplicWindowContext->UxarrowButton41
#define arrowButton42           UxApplicWindowContext->UxarrowButton42
#define label42                 UxApplicWindowContext->Uxlabel42
#define text29                  UxApplicWindowContext->Uxtext29
#define label43                 UxApplicWindowContext->Uxlabel43
#define toggleButton13          UxApplicWindowContext->UxtoggleButton13
#define label44                 UxApplicWindowContext->Uxlabel44
#define rowColumn23             UxApplicWindowContext->UxrowColumn23
#define rowColumn24             UxApplicWindowContext->UxrowColumn24
#define arrowButton29           UxApplicWindowContext->UxarrowButton29
#define arrowButton30           UxApplicWindowContext->UxarrowButton30
#define text30                  UxApplicWindowContext->Uxtext30
#define label49                 UxApplicWindowContext->Uxlabel49
#define rowColumn25             UxApplicWindowContext->UxrowColumn25
#define rowColumn26             UxApplicWindowContext->UxrowColumn26
#define arrowButton33           UxApplicWindowContext->UxarrowButton33
#define arrowButton34           UxApplicWindowContext->UxarrowButton34
#define text32                  UxApplicWindowContext->Uxtext32
#define label50                 UxApplicWindowContext->Uxlabel50
#define label51                 UxApplicWindowContext->Uxlabel51
#define rowColumn37             UxApplicWindowContext->UxrowColumn37
#define rowColumn43             UxApplicWindowContext->UxrowColumn43
#define arrowButton37           UxApplicWindowContext->UxarrowButton37
#define arrowButton38           UxApplicWindowContext->UxarrowButton38
#define text33                  UxApplicWindowContext->Uxtext33
#define XDMenus                 UxApplicWindowContext->UxXDMenus
#define commandPane             UxApplicWindowContext->UxcommandPane
#define command_b2              UxApplicWindowContext->Uxcommand_b2
#define command_b3              UxApplicWindowContext->Uxcommand_b3
#define commandPane_b17         UxApplicWindowContext->UxcommandPane_b17
#define command_b9              UxApplicWindowContext->Uxcommand_b9
#define command_b10             UxApplicWindowContext->Uxcommand_b10
#define commandPane_b14         UxApplicWindowContext->UxcommandPane_b14
#define commandPane_b15         UxApplicWindowContext->UxcommandPane_b15
#define commandPane_b16         UxApplicWindowContext->UxcommandPane_b16
#define commandsCascade         UxApplicWindowContext->UxcommandsCascade
#define LUTCommsPane            UxApplicWindowContext->UxLUTCommsPane
#define LUTcomm_b1              UxApplicWindowContext->UxLUTcomm_b1
#define LUTcomm_b2              UxApplicWindowContext->UxLUTcomm_b2
#define LUTcomm_b3              UxApplicWindowContext->UxLUTcomm_b3
#define LUTcomm_b4              UxApplicWindowContext->UxLUTcomm_b4
#define LUTcomm_b5              UxApplicWindowContext->UxLUTcomm_b5
#define LUTcomm_b6              UxApplicWindowContext->UxLUTcomm_b6
#define LUTCommsCascade         UxApplicWindowContext->UxLUTCommsCascade
#define helpPane                UxApplicWindowContext->UxhelpPane
#define XDHelp                  UxApplicWindowContext->UxXDHelp
#define InterfHelp              UxApplicWindowContext->UxInterfHelp
#define EditHelp                UxApplicWindowContext->UxEditHelp
#define SelectHelp              UxApplicWindowContext->UxSelectHelp
#define helpCascade             UxApplicWindowContext->UxhelpCascade
#define quitPane                UxApplicWindowContext->UxquitPane
#define quit_b1                 UxApplicWindowContext->Uxquit_b1
#define quitCascade             UxApplicWindowContext->UxquitCascade

static _UxCApplicWindow	*UxApplicWindowContext;

swidget	ApplicWindow;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*MapUnmap = "#override\n\
<Btn1Down>:MapACT()\n\
<Btn3Down>:MapACTX()\n\
<EnterWindow>:WriteHelp()\n\
<LeaveWindow>:ClearHelp()\n";

static char	*SelectFile = "#override\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>Delete:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Btn3Down>:FileSelectACT1()\n";

static char	*Arrows2 = "#override\n\
<Btn1Down>:ArrowACT2()\n\
<Btn1Down>(2):ArrowCHNG()\n";

static char	*Arrows6 = "#override\n\
<Btn1Down>:ArrowACT6()\n\
<Btn1Down>(2):ArrowCHNG()\n";

static char	*Arrows9 = "#override\n\
<Btn1Down>:ArrowACT9()\n\
<Btn1Down>(2):ArrowCHNG()\n";

static char	*Arrows8 = "#override\n\
<Btn1Down>:ArrowACT8()\n\
<Btn1Down>(2):ArrowCHNG()\n";

static char	*Arrows5 = "#override\n\
<Btn1Down>:ArrowACT5()\n\
<Btn1Down>(2):ArrowCHNG()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	popup_ApplicWindow();

/*******************************************************************************
	Auxiliary code from the Declarations Editor:
*******************************************************************************/

#include "myaux.c"

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ArrowACT5( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	
	/*  `arrows5.c'  the action for Button1 down in an arrow widget 
	                 in "form5" (EZ Load)                         */
	
#include <stdio.h>
	
	
	extern int speedo;
	extern double  stepsize[2];
	
	char  cbuf[20], cd, *single;
	int   nh, kdelta, szx, szy, dspid, kval[2];
	
	float cuthi, cutlo, cutdelt;
	
	
	 
	
	if (speedo == 0)
	   {
	   kdelta = 10;
	   nh = 1;
	   }
	else
	   {
	   kdelta = 50;
	   nh = 5;
	   }
	
	
	if (UxWidget == UxGetWidget(arrowButton36))
	   {                                 /* ScaleX down arrow */
	   single = XmTextGetString(UxGetWidget(text31));
	   sscanf(single,"%d",&dspid);
	   dspid -= nh;
	   if (dspid < -100) dspid = -100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text31,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton35))
	   {                                 /* ScaleX up arrow */
	   single = XmTextGetString(UxGetWidget(text31));
	   sscanf(single,"%d",&dspid);
	   dspid += nh;
	   if (dspid > 100) dspid = 100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text31,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton38))
	   {                                 /* ScaleY down arrow */
	   single = XmTextGetString(UxGetWidget(text33));
	   sscanf(single,"%d",&dspid);
	   dspid -= nh;
	   if (dspid < -100) dspid = -100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text33,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton37))
	   {                                 /* ScaleY up arrow */
	   single = XmTextGetString(UxGetWidget(text33));
	   sscanf(single,"%d",&dspid);
	   dspid += nh;
	   if (dspid > 100) dspid = 100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text33,cbuf);
	   }
	
	
	else 
	   {
	   single = XmTextGetString(UxGetWidget(text36));
	   sscanf(single,"%f",&cutdelt);
	   }
	
	XtFree(single);
	
	if (UxWidget == UxGetWidget(arrowButton42))
	   {                             /* LowCut down arrow */
	   single = XmTextGetString(UxGetWidget(text34));
	   sscanf(single,"%f",&cutlo);
	   cutlo -= nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cutlo);
	   UxPutText(text34,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton41))
	   {                                 /* LowCut up arrow */
	   single = XmTextGetString(UxGetWidget(text34));
	   sscanf(single,"%f",&cutlo);
	   cutlo += nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cutlo);
	   UxPutText(text34,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton44))
	   {                                 /* HighCut down arrow */
	   single = XmTextGetString(UxGetWidget(text35));
	   sscanf(single,"%f",&cuthi);
	   cuthi -= nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cuthi);
	   UxPutText(text35,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton43))
	   {                                 /* HighCut up arrow */
	   single = XmTextGetString(UxGetWidget(text35));
	   sscanf(single,"%f",&cuthi);
	   cuthi += nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cuthi);
	   UxPutText(text35,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton30))
	   {					/* DisplaySzX down arrow */
	   strcpy(cbuf,UxGetText(text30));
	   sscanf(cbuf,"%d",&szx);
	   nh = szx >> 1;
	   if (nh < 1) nh = 1;
	   szx -= nh;
	   if (szx < 2) szx = 2;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text30,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton29))
	   {					/* DisplaySzX up arrow */
	   strcpy(cbuf,UxGetText(text30));
	   sscanf(cbuf,"%d",&szx);
	   nh = szx >> 1;
	   if (nh < 1) nh = 1;
	   szx += nh;
	   if (szx >1024) szx = 1024;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text30,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton34))
	   {					/* DisplaySzY down arrow */
	   strcpy(cbuf,UxGetText(text32));
	   sscanf(cbuf,"%d",&szy);
	   nh = szy >> 1;
	   if (nh < 1) nh = 1;
	   szy -= nh;
	   if (szy < 2) szy = 2;
	   sprintf(cbuf,"%d",szy);
	   UxPutText(text32,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton33))
	   {					/* DisplaySzY up arrow */
	   strcpy(cbuf,UxGetText(text32));
	   sscanf(cbuf,"%d",&szy);
	   nh = szy >> 1;
	   if (nh < 1) nh = 1;
	   szy += nh;
	   if (szy > 1024) szy = 1024;
	   sprintf(cbuf,"%d",szy);
	   UxPutText(text32,cbuf);
	   }
	
	 
	XtFree(single);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ArrowACT8( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	
	/*  `arrows8.c'  the action for Button1 down in an arrow widget
	                 in "form8" (MODIFY/ITT)   */
	
	
	
	extern int speedo;
	
	char  cbuf[20], *single;
	int   nh, ittval;
	
	
	 
	/* MODIFY/ITT */
	
	if (speedo == 0)
	   nh = 1;
	else
	   nh = 5;
	
	
	if (UxWidget == UxGetWidget(arrowButton2))
	   {                                 /* ITT value down arrow */
	   single = XmTextGetString(UxGetWidget(text27));
	   sscanf(single,"%d",&ittval);
	   ittval -= nh;
	   if (ittval < 0) ittval = 0;
	   sprintf(cbuf,"%d",ittval);
	   UxPutText(text27,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton1))
	   {                                 /* ITT value up arrow */
	   single = XmTextGetString(UxGetWidget(text27));
	   sscanf(single,"%d",&ittval);
	   ittval += nh;
	   if (ittval > 255) ittval = 255;
	   sprintf(cbuf,"%d",ittval);
	   UxPutText(text27,cbuf);
	   }
	
	 
	XtFree(single);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ArrowACT6( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	
	/*  `arrows6.c'  the action for Button1 down in an arrow widget 
	                 in "form6" (CREATE/DISPLAY)                    */
	
	
	
	extern int speedo;
	
	char  cbuf[20], cbo[2], cd, *single;
	int   nh, szx, szy, dspid, kdelta;
	
	
	/* CREATE/DISPLAY */
	
	strcpy(cbuf,UxGetSet(toggleButton15));
	if (cbuf[0] == 't') 
	   cd = 'D';
	else
	   cd = 'G';
	
	if (speedo == 0)
	   kdelta = 10;
	else
	   kdelta = 50;
	
	if (UxWidget == UxGetWidget(arrowButton4))
	   {					/* DisplaySzX down arrow */
	   strcpy(cbuf,UxGetText(text4));
	   sscanf(cbuf,"%d",&szx);
	   nh = szx >> 1;
	   if (nh < 1) nh = 1;
	   szx -= nh;
	   if (szx < 2) szx = 2;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text4,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton3))
	   {					/* DisplaySzX up arrow */
	   strcpy(cbuf,UxGetText(text4));
	   sscanf(cbuf,"%d",&szx);
	   nh = szx >> 1;
	   if (nh < 1) nh = 1;
	   szx += nh;
	   if (szx >1024) szx = 1024;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text4,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton10))
	   {					/* DisplaySzY down arrow */
	   strcpy(cbuf,UxGetText(text5));
	   sscanf(cbuf,"%d",&szy);
	   nh = szy >> 1;
	   if (nh < 1) nh = 1;
	   szy -= nh;
	   if (szy < 2) szy = 2;
	   sprintf(cbuf,"%d",szy);
	   UxPutText(text5,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton9))
	   {					/* DisplaySzY up arrow */
	   strcpy(cbuf,UxGetText(text5));
	   sscanf(cbuf,"%d",&szy);
	   nh = szy >> 1;
	   if (nh < 1) nh = 1;
	   szy += nh;
	   if (szy > 1024) szy = 1024;
	   sprintf(cbuf,"%d",szy);
	   UxPutText(text5,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton12))
	   {					/* DisplayOffX down arrow */
	   strcpy(cbuf,UxGetText(text6));
	   sscanf(cbuf,"%d",&szx);
	   szx -= kdelta;
	   if (szx < 0) szx = 0;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text6,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton11))
	   {					/* DisplayOffX up arrow */
	   strcpy(cbuf,UxGetText(text6));
	   sscanf(cbuf,"%d",&szx);
	   szx += kdelta;
	   if (szx > 1020) szx = 1020;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text6,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton18))
	   {					/* DisplayOffY down arrow */
	   strcpy(cbuf,UxGetText(text8));
	   sscanf(cbuf,"%d",&szy);
	   szy -= kdelta;
	   if (szy < 0) szy = 0;
	   sprintf(cbuf,"%d",szy);
	   UxPutText(text8,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton17))
	   {					/* DisplayOffY up arrow */
	   strcpy(cbuf,UxGetText(text8));
	   sscanf(cbuf,"%d",&szy);
	   szy += kdelta;
	   if (szy > 1020) szy = 1020;
	   sprintf(cbuf,"%d",szy);
	   UxPutText(text8,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton8))
	   {					/* Display_Id down arrow */
	   single = XmTextGetString(UxGetWidget(text1));
	   dspid = (*single) - 48;
	   if (dspid > 0) dspid --;
	   cbo[0] = dspid + 48;
	   cbo[1] = '\0';
	   UxPutText(text1,cbo);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton7))
	   {					/* Display_Id up arrow */
	   single = XmTextGetString(UxGetWidget(text1));
	   dspid = (*single) - 48;
	   if (cd == 'D')
	      {
	      if (dspid < 9) dspid ++;
	      }
	   else
	      {
	      if (dspid < 3) dspid ++;
	      }
	   cbo[0] = dspid + 48;
	   cbo[1] = '\0';
	   UxPutText(text1,cbo);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton14))
	   {					/* Display Gsize down arrow */
	   strcpy(cbuf,UxGetText(text13));
	   sscanf(cbuf,"%d",&szx);
	   kdelta *= 1000;		/* 10, 50  =>  10000, 50000  */
	   szx -= kdelta;
	   if (szx < 10000) szx = 10000;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text13,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton13))
	   {					/* Display Gsize up arrow */
	   strcpy(cbuf,UxGetText(text13));
	   sscanf(cbuf,"%d",&szx);
	   kdelta *= 1000;		/* 10, 50  =>  10000, 50000  */
	   szx += kdelta;
	   if (szx > 2000000) szx = 2000000;
	   sprintf(cbuf,"%d",szx);
	   UxPutText(text13,cbuf);
	   }
	
	 
	XtFree(single);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ArrowCHNG( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern  int  speedo;
	
	if (speedo == 1)
	   speedo = 0;
	else
	   speedo = 1;
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ArrowACT2( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	
	/*  `arrows2.c'  the action for Button1 down in an arrow widget 
	                 in "form2" (LOAD/IMAGE)                         */
	
#include <stdio.h>
	
	
	extern int speedo;
	extern double  stepsize[2];
	
	char  cbuf[20], cd, *single;
	int   nh, szx, szy, dspid, kval[2];
	
	float cuthi, cutlo, cutdelt;
	
	
	 
	/* LOAD/IMAGE */
	
	
	if (speedo == 0)
	   nh = 1;
	else
	   nh = 5;
	
	
	if (UxWidget == UxGetWidget(arrowButton6))
	   {                                 /* Channel down arrow */
	   single = XmTextGetString(UxGetWidget(text2));
	   dspid = (*single) - 48;
	   if (dspid > 0) dspid --;
	   cbuf[0] = dspid + 48;
	   cbuf[1] = '\0';
	   UxPutText(text2,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton5))
	   {                                 /* Channel up arrow */
	   single = XmTextGetString(UxGetWidget(text2));
	   dspid = (*single) - 48;
	   if (dspid < 3) dspid ++;
	   cbuf[0] = dspid + 48;
	   cbuf[1] = '\0';
	   UxPutText(text2,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton48))
	   {                                 /* Display_Id down arrow */
	   single = XmTextGetString(UxGetWidget(text41));
	   dspid = (*single) - 48;
	   if (dspid > 0) dspid --;
	   cbuf[0] = dspid + 48;
	   cbuf[1] = '\0';
	   UxPutText(text41,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton47))
	   {                                 /* Display_Id up arrow */
	   single = XmTextGetString(UxGetWidget(text41));
	   dspid = (*single) - 48;
	   if (dspid < 9) dspid ++;
	   cbuf[0] = dspid + 48;
	   cbuf[1] = '\0';
	   UxPutText(text41,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton40))
	   {                                 /* ScaleX down arrow */
	   single = XmTextGetString(UxGetWidget(text39));
	   sscanf(single,"%d",&dspid);
	   dspid -= nh;
	   if (dspid < -100) dspid = -100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text39,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton39))
	   {                                 /* ScaleX up arrow */
	   single = XmTextGetString(UxGetWidget(text39));
	   sscanf(single,"%d",&dspid);
	   dspid += nh;
	   if (dspid > 100) dspid = 100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text39,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton46))
	   {                                 /* ScaleY down arrow */
	   single = XmTextGetString(UxGetWidget(text40));
	   sscanf(single,"%d",&dspid);
	   dspid -= nh;
	   if (dspid < -100) dspid = -100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text40,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton45))
	   {                                 /* ScaleY up arrow */
	   single = XmTextGetString(UxGetWidget(text40));
	   sscanf(single,"%d",&dspid);
	   dspid += nh;
	   if (dspid > 100) dspid = 100;
	   sprintf(cbuf,"%d",dspid);
	   UxPutText(text40,cbuf);
	   }
	
	
	
	
	else if (UxWidget == UxGetWidget(arrowButton23))
	   {                                 /* Center left arrow */
	   single = XmTextGetString(UxGetWidget(text20));
	   sscanf(single,"%d",&kval[0]);
	   kval[0] += nh*stepsize[0];
	   sprintf(cbuf,"%d",kval[0]);
	   UxPutText(text20,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton24))
	   {                                 /* Center right arrow */
	   single = XmTextGetString(UxGetWidget(text20));
	   sscanf(single,"%d",&kval[0]);
	   kval[0] -= nh*stepsize[0];
	   sprintf(cbuf,"%d",kval[0]);
	   UxPutText(text20,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton22))
	   {                                 /* Center down arrow */
	   single = XmTextGetString(UxGetWidget(text24));
	   sscanf(single,"%d",&kval[1]);
	   kval[1] += nh*stepsize[1];
	   sprintf(cbuf,"%d",kval[1]);
	   UxPutText(text24,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton21))
	   {                                 /* Center up arrow */
	   single = XmTextGetString(UxGetWidget(text24));
	   sscanf(single,"%d",&kval[1]);
	   kval[1] -= nh*stepsize[1];
	   sprintf(cbuf,"%d",kval[1]);
	   UxPutText(text24,cbuf);
	   }
	
	else 
	    {
	   single = XmTextGetString(UxGetWidget(text25));
	   sscanf(single,"%f",&cutdelt);
	
	if (UxWidget == UxGetWidget(arrowButton26))
	   {                             /* LowCut down arrow */
	   single = XmTextGetString(UxGetWidget(text15));
	   sscanf(single,"%f",&cutlo);
	   cutlo -= nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cutlo);
	   UxPutText(text15,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton25))
	   {                                 /* LowCut up arrow */
	   single = XmTextGetString(UxGetWidget(text15));
	   sscanf(single,"%f",&cutlo);
	   cutlo += nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cutlo);
	   UxPutText(text15,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton28))
	   {                                 /* HighCut down arrow */
	   single = XmTextGetString(UxGetWidget(text22));
	   sscanf(single,"%f",&cuthi);
	   cuthi -= nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cuthi);
	   UxPutText(text22,cbuf);
	   }
	
	else if (UxWidget == UxGetWidget(arrowButton27))
	   {                                 /* HighCut up arrow */
	   single = XmTextGetString(UxGetWidget(text22));
	   sscanf(single,"%f",&cuthi);
	   cuthi += nh*cutdelt;
	   sprintf(cbuf,"%8.4f",cuthi);
	   UxPutText(text22,cbuf);
	   }
	   }
	
	XtFree(single);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_FileSelectACT1( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `fileselect.c'  the action routine for `FileSelectACT1'  */
	
#include <fsydef.h>
	
	
	extern swidget swfs, stxt;
	extern Widget  filelist_widget;
	extern char  dir_specs[128];
	extern int  pushb;
	
	Widget  ww = UxWidget;
	int  strip, n, k;
	
	
	
	stxt = UxThisWidget;
	strip = 1;				/* default to stripping ,off */
	
	if ((ww == UxGetWidget(text9)) || (ww == UxGetWidget(text29)))
	  {
	   osfphname("MID_SYSTAB",dir_specs);
	   n = strlen(dir_specs);
	   k = n - 1;
	   if (dir_specs[k] != FSY_DIREND)
	      {
	      dir_specs[n] = FSY_DIREND;
	      n++;
	      }
	   strcpy(&dir_specs[n],"*.lut");	/* list the LUTs */
	   }
	 
	
	else if (ww == UxGetWidget(text23))
	   {
	   osfphname("MID_SYSTAB",dir_specs);
	   n = strlen(dir_specs);
	   k = n - 1;
	   if (dir_specs[k] != FSY_DIREND)
	      {
	      dir_specs[n] = FSY_DIREND;
	      n++;
	      }
	   strcpy(&dir_specs[n],"*.itt");	/* list the ITTs */
	   }
	
	
	else if ((ww == UxGetWidget(text16)) || (ww == UxGetWidget(text10)) ||
	         (ww == UxGetWidget(text28)))
	   strcpy(dir_specs,"*.bdf");		/* list all bedf files */
	
	
	else
	   {
	   strip = 0;			/* leave filenames as they are */
	   strcpy(dir_specs,"*.*");
	   }
	
	SetFileList(filelist_widget,strip,dir_specs);
	
	UxPopupInterface(swfs, no_grab);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	UxPutText(SHelp,"");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{ 
	
	/*   `writehelp.c'  the action for displaying help  */
	
	
	extern Widget Widget_managed[2];
	
	char s[100], cbuf[8];
	Widget sw = UxWidget;
	
	strcpy(s," ");
	
	
	/* first treat the push buttons */
	
	if (sw == UxGetWidget(pushButton7))
	   {
	   strcpy(s,"Map interface for displaying an image");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton2))
	   {
	   strcpy(s,"Map interface for loading a LUT and/or an ITT");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton3))
	   {
	   strcpy(s,"Map interface for exploring an image");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton4))
	   {
	   strcpy(s,"Map interface for modifying the LUT and/or the ITT");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton8))
	   {
	   strcpy(s,"Map interface for creating display or graphics window");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton6))
	   {
	   strcpy(s,
	   "Map interface for retrieving image coordinates via the cursor");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton9))
	   {
	   strcpy(s,
	   "Map interface for extracting a trace from the displayed image");
	   goto Text_out;
	   }
	
	else if (sw == UxGetWidget(pushButton1))
	   {
	   strcpy(s,"Click here to execute the command in MIDAS");
	   goto Text_out;
	   }
	
	
	
	
	
	
	/*  Quick Load  */
	
	if (Widget_managed[0] == UxGetWidget(form5))
	   {
	   if (sw == UxGetWidget(label37) || sw == UxGetWidget(text28))
	      strcpy(s,"Name of the image to be loaded");
	   else if (sw == UxGetWidget(label41) || sw == UxGetWidget(text31))
	      strcpy(s,"Scaling factor in X");
	   else if (sw == UxGetWidget(label51) || sw == UxGetWidget(text33))
	      strcpy(s,"Scaling factor in Y");
	   else if (sw == UxGetWidget(label38) || sw == UxGetWidget(text34))
	      strcpy(s,"Low cut values for mapping data to colour range");
	   else if (sw == UxGetWidget(label45) || sw == UxGetWidget(text35))
	      strcpy(s,"High cut values for mapping data to colour range");
	   else if (sw == UxGetWidget(label47) || sw == UxGetWidget(text36))
	      strcpy(s,"Value for in/decreasing low and high cut values");
	   else if (sw == UxGetWidget(label46)) 
	      strcpy(s,"Yes = use descr. values of image, No = do not use them");
	   else if (sw == UxGetWidget(label42) || sw == UxGetWidget(text29))  
	      strcpy(s,"Name of Colour Lookup Table");
	   else if (sw == UxGetWidget(label44) || sw == UxGetWidget(text30))
	      strcpy(s,"Display xsize in pixels");
	   else if (sw == UxGetWidget(label49) || sw == UxGetWidget(text32))
	      strcpy(s,"Display ysize in pixels");
	   else if (sw == UxGetWidget(label43) || sw == UxGetWidget(label50))
	      strcpy(s,"Yes = Also, create display, No = do not create display");
	   }
	
	
	/*  Load Image  */
	
	else if (Widget_managed[0] == UxGetWidget(form2))
	   {
	   if (sw == UxGetWidget(label14) || sw == UxGetWidget(text16))  
	      strcpy(s,"Name of the image to be loaded");
	   else if (sw == UxGetWidget(label27) || sw == UxGetWidget(text2))  
	      strcpy(s,"Channel no. of display");
	   else if (sw == UxGetWidget(label32) || sw == UxGetWidget(text41))  
	         strcpy(s,"Display ID as single digit in [0,9] ");
	   else if (sw == UxGetWidget(label24) || sw == UxGetWidget(text39))  
	      strcpy(s,"Scaling factor in X");
	   else if (sw == UxGetWidget(label52) || sw == UxGetWidget(text40))  
	      strcpy(s,"Scaling factor in Y");
	   else if (sw == UxGetWidget(label15) || sw == UxGetWidget(text15))
	      strcpy(s,"Low cut values for mapping data to colour range");
	   else if (sw == UxGetWidget(label28) || sw == UxGetWidget(text22))
	      strcpy(s,"High cut values for mapping data to colour range");
	   else if (sw == UxGetWidget(label36) || sw == UxGetWidget(text25))
	      strcpy(s,"Value for in/decreasing low and high cut values");
	   else if (sw == UxGetWidget(label25))
	      strcpy(s,
	      "Image center pixels as `@xpix' and `@ypix' or `xcoord' and `ycoord'");
	   else if (sw == UxGetWidget(text20))
	      strcpy(s, "Image x-center pixel as `@xpix' or `xcoord'");
	   else if (sw == UxGetWidget(text24))
	      strcpy(s, "Image y-center pixel as `@ypix' or `ycoord'");
	   else if (sw == UxGetWidget(label53) || sw == UxGetWidget(label35))
	      strcpy(s,"Yes = use descr. values of image, No = do not use them");
	   }
	
	
	/* CREATE/DISPLAY + CREATE/GRAPHICS */
	
	else if (Widget_managed[0] == UxGetWidget(form6))
	   {
	   if (sw == UxGetWidget(label54))  
	      strcpy(s,"Toggle between Display and Graphics Window");
	
	   strcpy(cbuf,UxGetSet(toggleButton15));
	   if (cbuf[0] == 't')			/* DISPLAY */
	      {
	      if (sw == UxGetWidget(label16) || sw == UxGetWidget(text1)) 
	         strcpy(s,"Display ID as single digit in [0,9] ");
	      else if (sw == UxGetWidget(label17) || sw == UxGetWidget(text4))
	         strcpy(s,"Display xsize in pixels");
	      else if (sw == UxGetWidget(label18) || sw == UxGetWidget(text5))
	         strcpy(s,"Display ysize in pixels");
	      else if (sw == UxGetWidget(label5) || sw == UxGetWidget(text6))
	         strcpy(s,"Display xoffset- lower left corner = (0,0)");
	      else if (sw == UxGetWidget(label22) || sw == UxGetWidget(text8))
	         strcpy(s,"Display yoffset- lower left corner = (0,0)");
	      else if (sw == UxGetWidget(label19) || sw == UxGetWidget(toggleButton25))  
	         strcpy(s,"Yes or No for alpha memory or not");
	      }
	
	   else					/* GRAPHIC */
	      {
	      if (sw == UxGetWidget(label16) || sw == UxGetWidget(text1))  
	         strcpy(s,"Graphic ID as single digit in [0,3] ");
	      else if (sw == UxGetWidget(label17) || sw == UxGetWidget(text4))
	         strcpy(s,"Graphic xsize in pixels");
	      else if (sw == UxGetWidget(label18) || sw == UxGetWidget(text5))
	         strcpy(s,"Graphic ysize in pixels");
	      else if (sw == UxGetWidget(label5) || sw == UxGetWidget(text6))
	         strcpy(s,"Graphic xoffset- lower left corner = (0,0)");
	      else if (sw == UxGetWidget(label22) || sw == UxGetWidget(text8))
	         strcpy(s,"Graphic yoffset- lower left corner = (0,0)");
	      }
	
	   if (sw == UxGetWidget(label21) || sw == UxGetWidget(text21))  
	      strcpy(s,"Name of Xworkstation/Xterminal screen in X11 syntax");
	   else if (sw == UxGetWidget(label20) || sw == UxGetWidget(text13))  
	      strcpy(s,"No. of graphic segments used for refreshing of plots");
	   }
	
	
	/* LOAD/LUT + DISPLAY/LUT + LOAD/ITT */
	
	else if (Widget_managed[0] == UxGetWidget(form4))
	   {
	   if (sw == UxGetWidget(label2) || sw == UxGetWidget(text9))  
	      strcpy(s,"Name of Colour Lookup Table");
	   else if (sw == UxGetWidget(label1)) 	
	      strcpy(s,"Yes or No to display a colour bar with LUT");
	   else if (sw == UxGetWidget(label26) || sw == UxGetWidget(text23))  
	      strcpy(s,"Name of Intensity Transfer Table");
	   } 
	
	
	/* MODIFY/LUT + MODIFY/ITT */
	
	else if (Widget_managed[0] == UxGetWidget(form8))
	   {
	   if (sw == UxGetWidget(label39))
	      strcpy(s,"method for LUT modification");
	   else if (sw == UxGetWidget(label40))
	      strcpy(s,"colour for LUT modification");
	   else if (sw == UxGetWidget(label23))
	      strcpy(s,"method for ITT modification");
	   else if (sw == UxGetWidget(label33))
	      strcpy(s,"value for ITT modification");
	   }
	  
	
	/* VIEW/IMAGE */
	
	else if (Widget_managed[0] == UxGetWidget(form3))
	   {
	   if (sw == UxGetWidget(label9) || sw == UxGetWidget(text10))  
	      strcpy(s,"Name of the image to be viewed (examined)");
	   else if (sw == UxGetWidget(label13) || sw == UxGetWidget(text17))  
	      strcpy(s,"Name of optional table for coords of visited regions");
	   else if (sw == UxGetWidget(label11))   
	      strcpy(s,"Plot or NoPlot for plotting extracted info in graph_window or not");
	   else if (sw == UxGetWidget(label12)) 
	      strcpy(s,"FITS for FITS files or Midas for native Midas format");
	   }
	
	
	/* GET/CURSOR */
	
	else if (Widget_managed[0] == UxGetWidget(form7))
	   {
	   if (sw == UxGetWidget(label3) || sw == UxGetWidget(text3))
	      strcpy(s,"Specs for storing displayed info:\ntable name or descr,DESC");
	   else if (sw == UxGetWidget(label7))
	      strcpy(s,"`Yes' or `No' for for marking cursor position in overlay plane");
	   else if (sw == UxGetWidget(label8))
	      strcpy(s,"No. of cursors, 1 or 2 cursors");
	   else if (sw == UxGetWidget(label4))
	      strcpy(s,"Use zoom window or not");
	   }
	
	
	/* EXTRACT/TRACE */
	
	else if (Widget_managed[0] == UxGetWidget(form1))
	   {
	   if (sw == UxGetWidget(label6) || sw == UxGetWidget(text7))
	      strcpy(s,"Name of result frame");
	   else if (sw == UxGetWidget(label31) || sw == UxGetWidget(plotflag))
	      strcpy(s,
	  "`Draw', `Plot' or `None' for plotting in overlay, graph.window or no plots");
	   else if (sw == UxGetWidget(label29))
	      strcpy(s,"`NoCut' or `Cut' line after cursors");
	   else if (sw == UxGetWidget(label30) || sw == UxGetWidget(text14))
	      strcpy(s,"Stepsize for result frame if different from input frame");
	   }
	
	else if (sw == UxGetWidget(pushButton5))
	   {
	   strcpy(s,"Activate interface for displaying an image \n load a LUT");
	   strcat(s,"and create the display window");
	   }
	
	else if (sw == UxGetWidget(pushButton7))
	   strcpy(s,"Activate interface for displaying an image");
	
	else if (sw == UxGetWidget(pushButton4))
	   strcpy(s,"Activate interface for loading a LUT and/or an ITT");
	
	else if (sw == UxGetWidget(pushButton3))
	   strcpy(s,"Activate interface for exploring an image");
	
	else if (sw == UxGetWidget(pushButton4))
	   strcpy(s,"Activate interface for modifying the LUT and/or the ITT");
	
	else if (sw == UxGetWidget(pushButton8))
	   strcpy(s,"Activate interface for creating display or graphics window");
	
	else if (sw == UxGetWidget(pushButton7))
	   strcpy(s,
	   "Activate interface for retrieving image coordinates via the cursor");
	
	else if (sw == UxGetWidget(pushButton9))
	   strcpy(s,
	   "Activate interface for extracting a trace from the displayed image");
	
	else if (sw == UxGetWidget(pushButton1))
	   strcpy(s,"Click here to execute the command in MIDAS");
	
	
	
	
	Text_out:
	UxPutText(SHelp,s);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_MapACTX( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  saved in file  `mapactx.c' (same as `mapact.c' but fires off the
	    command in the end via "applycom"
	 */
	
	extern  Widget Widget_managed[2];
	extern swidget pushbt[2];
	
	extern int speedo;
	extern int menuact[8], kick[5];
	
	swidget  swmap, swpush;
	
	
	
	speedo = 0;		/* reset arrow accelerator */
	
	if (Widget_managed[0] != 0)
	   {
	   UxUnmap(UxWidgetToSwidget(Widget_managed[0]));	/* unmap current form */
	   XtSetSensitive(UxGetWidget(pushbt[0]),TRUE);	/* connected pushButton -> sensitive */
	   }
	
	
	if (UxWidget == UxGetWidget(pushButton7))
	   {
	   swmap = form2;		/* LOAD/IMAGE */
	   swpush = pushButton7;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton8))
	   {
	   swmap = form6;		/* CREATE/DISPLAY + CREATE/GRAPH */  
	   swpush = pushButton8;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton2))
	   {
	   swmap = form4;		/* LOAD/LUT + DISP/LUT */
	   swpush = pushButton2;
	   }
	   
	else if (UxWidget == UxGetWidget(pushButton4))
	   {
	   swmap = form8;		/* MODIFY/LUT */  
	   swpush = pushButton4;
	   menuact[0] = 0;
	   menuact[1] = 0;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton3))
	   {
	   swmap = form3;		/* View Image */  
	   swpush = pushButton3;
	   }
	   
	else if (UxWidget == UxGetWidget(pushButton6))
	   {
	   swmap = form7;		/* Get Cursor */  
	   swpush = pushButton6;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton9))
	   {
	   swmap = form1;		/* EXTRACT/TRACE */  
	   swpush = pushButton9;
	   menuact[2] = 0;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton5))
	   {
	   swmap = form5;		/* Quick Load */  
	   swpush = pushButton5;
	   }
	
	
	
	else
	   {
	/*   printf("MapACT: UxWidget = %d\n",UxWidget); */
	   return;
	   }
	
	
	UxMap(swmap);
	Widget_managed[0] = UxGetWidget(swmap);
	
	XtSetSensitive(UxGetWidget(swpush),FALSE);
	pushbt[0] = swpush;			/* update current pushButton */
	
	
	if (kick[4] == 1)
	   XD_Help(1);
	
	
	applycom(0);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_MapACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  saved in file  `mapact.c'  */
	
	extern  Widget Widget_managed[2];
	extern swidget pushbt[2];
	
	extern int speedo;
	extern int menuact[8], kick[5];
	
	swidget  swmap, swpush;
	
	
	
	speedo = 0;		/* reset arrow accelerator */
	
	if (Widget_managed[0] != 0)
	   {
	   UxUnmap(UxWidgetToSwidget(Widget_managed[0]));	/* unmap current form */
	   XtSetSensitive(UxGetWidget(pushbt[0]),TRUE);	/* connected pushButton -> sensitive */
	   }
	
	
	if (UxWidget == UxGetWidget(pushButton7))
	   {
	   swmap = form2;		/* LOAD/IMAGE */
	   swpush = pushButton7;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton8))
	   {
	   swmap = form6;		/* CREATE/DISPLAY + CREATE/GRAPH */  
	   swpush = pushButton8;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton2))
	   {
	   swmap = form4;		/* LOAD/LUT + DISP/LUT */
	   swpush = pushButton2;
	   }
	   
	else if (UxWidget == UxGetWidget(pushButton4))
	   {
	   swmap = form8;		/* MODIFY/LUT */  
	   swpush = pushButton4;
	   menuact[0] = 0;
	   menuact[1] = 0;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton3))
	   {
	   swmap = form3;		/* View Image */  
	   swpush = pushButton3;
	   }
	   
	else if (UxWidget == UxGetWidget(pushButton6))
	   {
	   swmap = form7;		/* Get Cursor */  
	   swpush = pushButton6;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton9))
	   {
	   swmap = form1;		/* EXTRACT/TRACE */  
	   swpush = pushButton9;
	   menuact[2] = 0;
	   }
	
	else if (UxWidget == UxGetWidget(pushButton5))
	   {
	   swmap = form5;		/* Quick Load */  
	   swpush = pushButton5;
	   }
	
	
	
	else
	   {
	/*   printf("MapACT: UxWidget = %d\n",UxWidget); */
	   return;
	   }
	
	
	UxMap(swmap);
	Widget_managed[0] = UxGetWidget(swmap);
	
	XtSetSensitive(UxGetWidget(swpush),FALSE);
	pushbt[0] = swpush;			/* update current pushButton */
	
	if (kick[4] == 1)			/* if interface help, update it */
	   XD_Help(1);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_ApplicWindow( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `popup.c'   the Popup Callback of main ApplicWindow  */
	
	extern  Widget Widget_managed[2];
	extern swidget pushbt[2];
	
	extern char  actdisp[8];
	
	swidget  swmap;
	
	
	/*  map + unmap all forms in their order */
	
	UxPutText(text2,"0");
	UxPutText(text41,"0");
	UxUnmap(form2);		/* was mapped before */
	
	UxMap(form4);
	UxUnmap(form4);
	
	UxMap(form3);
	UxUnmap(form3);
	
	UxMap(form8);
	strcpy(actdisp,"255");
	UxPutText(text27,actdisp);
	UxUnmap(form8);
	
	UxMap(form6);
	strcpy(actdisp,"0");
	UxPutText(text1,actdisp);
	UxPutText(text4,"512");
	UxPutText(text5,"512");
	UxPutText(text6,"630");
	UxPutText(text8,"330");
	UxPutText(text21,"default");
	UxPutText(text13,"30000");
	UxUnmap(form6);
	
	UxMap(form7);
	UxUnmap(form7);
	
	UxMap(form5);
	UxPutText(text30,"512");
	UxPutText(text32,"512");
	UxPutText(text29,"rainbow");
	UxUnmap(form5);
	
	UxMap(form1);
	UxPutText(text7,"trace");
	UxUnmap(form1);
	
	swmap = form5;		/* begin with Quick Load */
	UxMap(swmap);
	Widget_managed[0] = UxGetWidget(swmap);
	
	pushbt[0] = pushButton5;
	XtSetSensitive(UxGetWidget(pushbt[0]),FALSE);
	
	UxMap(XDMenus);			/* map the menu bar */  
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_workArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_workArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_form2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text22( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"No");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	applycom(0);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"No");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_form3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Plot");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"NoPlot");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Midas");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"FITS");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"No");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/* 
	 `toggle6.c'  the Value change callback for toggle Button `Display/Graphics'  
	*/
	
	
	extern char dspinfo[4][8], grainfo[4][8];
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Display");
	   XtSetSensitive(UxGetWidget(label19),TRUE);
	   XtSetSensitive(UxGetWidget(toggleButton25),TRUE);
	
	   strcpy(grainfo[0],UxGetText(text4));
	   strcpy(grainfo[1],UxGetText(text5));
	   strcpy(grainfo[2],UxGetText(text6));
	   strcpy(grainfo[3],UxGetText(text8));
	 
	   UxPutText(text4,dspinfo[0]);
	   UxPutText(text5,dspinfo[1]);
	   UxPutText(text6,dspinfo[2]);
	   UxPutText(text8,dspinfo[3]);
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"Graphics");
	   XtSetSensitive(UxGetWidget(label19),FALSE);
	   XtSetSensitive(UxGetWidget(toggleButton25),FALSE);
	
	   strcpy(dspinfo[0],UxGetText(text4));
	   strcpy(dspinfo[1],UxGetText(text5));
	   strcpy(dspinfo[2],UxGetText(text6));
	   strcpy(dspinfo[3],UxGetText(text8));
	 
	   UxPutText(text4,grainfo[0]);
	   UxPutText(text5,grainfo[1]);
	   UxPutText(text6,grainfo[2]);
	   UxPutText(text8,grainfo[3]);
	   }
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_form7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"No");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"1");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"2");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"No");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_form1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_plotflag_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_plotflag_p1_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"NoCut");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"Cut");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_method_p3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	extern int menuact[8];
	
	menuact[0] = 0;		/* reset ITT activator */
	
	XmToggleButtonSetState(UxGetWidget(toggleButton3),TRUE,FALSE);
	
	XmToggleButtonSetState(UxGetWidget(toggleButton5),FALSE,FALSE);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `actmeth2a.c'  the Activate Callback of the 1. button of (BAND)
	    `method2' menu in MODIFY/LUT form                           */
	
	
	
	char  cur_color[24];
	
	XtSetSensitive(UxGetWidget(color1),TRUE);
	
	strcpy(cur_color,UxGetMenuHistory(color1));
	if (cur_color[10] == '4')			/* ALL -> RED */
	   UxPutMenuHistory(color1,"color_p1_b12");
	
	XtSetSensitive(UxGetWidget(color_p1_b15),FALSE);
	
	
	XtSetSensitive(UxGetWidget(color_p1_b12),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b13),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b14),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b16),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b17),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b18),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b19),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b20),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b21),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b22),TRUE);
	
	
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `actmeth2b.c'  the Activate Callback of the 2. button (ROTATE) of
	   `method2' menu in MODIFY/LUT form */
	
	
	char  cur_color[24];
	
	XtSetSensitive(UxGetWidget(color1),TRUE);
	
	strcpy(cur_color,UxGetMenuHistory(color1));
	if ( (cur_color[10] == '2') ||			/* b20, ... */
	     (cur_color[11] > '5') )			/* b16 -> b19 */
	   UxPutMenuHistory(color1,"color_p1_b15");
	
	XtSetSensitive(UxGetWidget(color_p1_b15),TRUE);
	
	XtSetSensitive(UxGetWidget(color_p1_b16),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b17),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b18),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b19),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b20),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b21),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b22),FALSE);
	
	
	
	
	
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `actmeth2c.c' the Activate Callback of the 3. button (SQEEZE) of
	    `method2' menu in MODIFY/LUT */
	
	
	UxPutMenuHistory(color1,"color_p1_b15");		/* force to ALL */
	
	XtSetSensitive(UxGetWidget(color1),FALSE);
	
	
	
	
	
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `actmeth2d.c' the Activate Callback of the 4. button (GRAPH) of
	    `method2' menu in MODIFY/LUT  */
	
	char  cur_color[24];
	
	
	XtSetSensitive(UxGetWidget(color1),TRUE);
	
	strcpy(cur_color,UxGetMenuHistory(color1));	/* get current color1 */
	if ( (cur_color[10] == '2') ||
	     (cur_color[11] > '5') )
	   UxPutMenuHistory(color1,"color_p1_b15");
	
	
	XtSetSensitive(UxGetWidget(color_p1_b12),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b13),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b14),TRUE);
	XtSetSensitive(UxGetWidget(color_p1_b15),TRUE);
	
	XtSetSensitive(UxGetWidget(color_p1_b16),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b17),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b18),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b19),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b20),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b21),FALSE);
	XtSetSensitive(UxGetWidget(color_p1_b22),FALSE);
	
	
	
	
	
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_method2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_color_p2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	extern int menuact[8];
	
	menuact[0] = 0;		/* reset ITT activator */
	
	XmToggleButtonSetState(UxGetWidget(toggleButton3),TRUE,FALSE);
	
	XmToggleButtonSetState(UxGetWidget(toggleButton5),FALSE,FALSE);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b20( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b21( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_color_p1_b22( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_method_p1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern int menuact[8];
	
	menuact[0] = 1;
	
	XmToggleButtonSetState(UxGetWidget(toggleButton3),FALSE,FALSE);
	XmToggleButtonSetState(UxGetWidget(toggleButton5),TRUE,FALSE);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `actmeth1a.c'  the Activate Callback of the 1. button of (BAND)
	    `method1' menu in MODIFY/ITT form                           */
	
	
	
	XtSetSensitive(UxGetWidget(label33),TRUE);
	XtSetSensitive(UxGetWidget(rowColumn19),TRUE);
	
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `actmeth1b.c'  the Activate Callback of the 2. button (ROTATE) of
	   `method1' menu in MODIFY/ITT form */
	
	
	
	XtSetSensitive(UxGetWidget(label33),FALSE);
	XtSetSensitive(UxGetWidget(rowColumn19),FALSE);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `actmeth1c.c'  the Activate Callback of the 3. button (SQUEEZE) of
	   `method1' menu in MODIFY/ITT form */
	
	
	
	XtSetSensitive(UxGetWidget(label33),FALSE);
	XtSetSensitive(UxGetWidget(rowColumn19),FALSE);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_method_p1_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `actmeth1d.c'  the Activate Callback of the 4. button (CONTRAST) of
	   `method1' menu in MODIFY/ITT form */
	
	
	
	XtSetSensitive(UxGetWidget(label33),FALSE);
	XtSetSensitive(UxGetWidget(rowColumn19),FALSE);
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_method1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_pushButton9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_pushButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_pushButton8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pushButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_form5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_form5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text35( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle.c'  the Value change callback for toggle Button  */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"No");
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text36( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text37( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text38( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text34( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 
	/*  `toggle5b.c'  the Value change callback for 
	    toggle Button:  Create Display                 */
	
	
	char  cbuf[8];
	
	strcpy(cbuf,UxGetSet(UxThisWidget));
	if (cbuf[0] == 't')
	   {
	   UxPutLabelString(UxThisWidget,"Yes");
	
	   XtSetSensitive(UxGetWidget(label44),TRUE);
	   XtSetSensitive(UxGetWidget(label49),TRUE);
	
	   XtSetSensitive(UxGetWidget(rowColumn23),TRUE);
	   XtSetSensitive(UxGetWidget(rowColumn25),TRUE);
	   }
	else
	   {
	   UxPutLabelString(UxThisWidget,"No");
	
	   XtSetSensitive(UxGetWidget(label44),FALSE);
	   XtSetSensitive(UxGetWidget(label49),FALSE);
	
	   XtSetSensitive(UxGetWidget(rowColumn23),FALSE);
	   XtSetSensitive(UxGetWidget(rowColumn25),FALSE);
	   }
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_command_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* for `ClearChannel' button (CLEAR/CHAN) */
	
	applycom(7);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_command_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `ClearDisplay' button (CLEAR/DISPLAY) */
	
	applycom(6);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_commandPane_b17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{applycom(1);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_command_b9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `DisableOverlay'  button  (CLEAR/OVER)  */
	
	applycom(9);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_command_b10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/*  `EnableOverlay'  button  (SET/OVER)  */
	
	applycom(10);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_commandPane_b14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{applycom(13);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_commandPane_b15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{applycom(14);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_commandPane_b16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{applycom(8);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_LUTcomm_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `LUT_Off' button (CLEAR/LUT)   */
	
	applycom(5);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_LUTcomm_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `LUT_On' button (SET/LUT)  */
	
	applycom(4);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_LUTcomm_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `LUT Display On' button (DISPLAY/LUT)  */
	
	applycom(11);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_LUTcomm_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `LUT Display Offn' button (DISPLAY/LUT OFF)  */
	
	applycom(12);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_LUTcomm_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `ITT_Off'  button (CLEAR/ITT) */
	
	applycom(2);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_LUTcomm_b6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	/* `ITT_On' button (SET/ITT)  */
	
	applycom(3);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_XDHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{XD_Help(0);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_InterfHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{XD_Help(1);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_EditHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{XD_Help(2);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_SelectHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{XD_Help(3);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_quit_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{exit(0);}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutDefaultFontList( ApplicWindow, TextFont );
	UxPutBackground( ApplicWindow, WindowBackground );
	UxPutIconName( ApplicWindow, "XDisplay" );
	UxPutDeleteResponse( ApplicWindow, "do_nothing" );
	UxPutTitle( ApplicWindow, "XDisplay" );
	UxPutHeight( ApplicWindow, 450 );
	UxPutWidth( ApplicWindow, 450 );
	UxPutY( ApplicWindow, 200 );
	UxPutX( ApplicWindow, 160 );

	UxPutBackground( mainWindow, WindowBackground );
	UxPutHeight( mainWindow, 100 );
	UxPutWidth( mainWindow, 100 );
	UxPutY( mainWindow, 180 );
	UxPutX( mainWindow, 200 );

	UxPutTextFontList( workAreaForm, TextFont );
	UxPutBackground( workAreaForm, WindowBackground );
	UxPutBorderWidth( workAreaForm, 0 );
	UxPutHeight( workAreaForm, 340 );
	UxPutWidth( workAreaForm, 490 );

	UxPutBackground( workAreaFrame, WindowBackground );
	UxPutHeight( workAreaFrame, 230 );
	UxPutWidth( workAreaFrame, 390 );
	UxPutY( workAreaFrame, 30 );
	UxPutX( workAreaFrame, 50 );

	UxPutTextFontList( workArea, TextFont );
	UxPutBackground( workArea, WindowBackground );
	UxPutBorderWidth( workArea, 0 );
	UxPutHeight( workArea, 498 );
	UxPutWidth( workArea, 408 );
	UxPutY( workArea, 2 );
	UxPutX( workArea, 20 );

	UxPutNoResize( form2, "true" );
	UxPutMappedWhenManaged( form2, "false" );
	UxPutBorderWidth( form2, 1 );
	UxPutBackground( form2, ApplicBackground );
	UxPutHeight( form2, 240 );
	UxPutWidth( form2, 430 );
	UxPutY( form2, 10 );
	UxPutX( form2, 0 );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutFontList( label14, BoldTextFont );
	UxPutTranslations( label14, "" );
	UxPutRecomputeSize( label14, "false" );
	UxPutAlignment( label14, "alignment_center" );
	UxPutBackground( label14, LabelBackground );
	UxPutForeground( label14, TextForeground );
	UxPutLabelString( label14, "Frame" );
	UxPutHeight( label14, 25 );
	UxPutWidth( label14, 60 );
	UxPutY( label14, 10 );
	UxPutX( label14, 10 );

	UxPutMarginWidth( text16, 2 );
	UxPutMarginHeight( text16, 2 );
	UxPutColumns( text16, 30 );
	UxPutFontList( text16, TextFont );
	UxPutForeground( text16, TextForeground );
	UxPutTranslations( text16, SelectFile );
	UxPutBackground( text16, TextBackground );
	UxPutHeight( text16, 32 );
	UxPutWidth( text16, 250 );
	UxPutY( text16, 39 );
	UxPutX( text16, 10 );

	UxPutFontList( label27, BoldTextFont );
	UxPutTranslations( label27, "" );
	UxPutRecomputeSize( label27, "false" );
	UxPutAlignment( label27, "alignment_center" );
	UxPutBackground( label27, LabelBackground );
	UxPutForeground( label27, TextForeground );
	UxPutLabelString( label27, "Channel" );
	UxPutHeight( label27, 25 );
	UxPutWidth( label27, 60 );
	UxPutY( label27, 79 );
	UxPutX( label27, 10 );

	UxPutFontList( label24, BoldTextFont );
	UxPutTranslations( label24, "" );
	UxPutRecomputeSize( label24, "false" );
	UxPutAlignment( label24, "alignment_center" );
	UxPutBackground( label24, LabelBackground );
	UxPutForeground( label24, TextForeground );
	UxPutLabelString( label24, "ScaleX" );
	UxPutHeight( label24, 25 );
	UxPutWidth( label24, 60 );
	UxPutY( label24, 119 );
	UxPutX( label24, 10 );

	UxPutFontList( label25, BoldTextFont );
	UxPutTranslations( label25, "" );
	UxPutRecomputeSize( label25, "false" );
	UxPutAlignment( label25, "alignment_center" );
	UxPutBackground( label25, LabelBackground );
	UxPutForeground( label25, TextForeground );
	UxPutLabelString( label25, "Center" );
	UxPutHeight( label25, 25 );
	UxPutWidth( label25, 85 );
	UxPutY( label25, 159 );
	UxPutX( label25, 10 );

	UxPutFontList( label15, BoldTextFont );
	UxPutTranslations( label15, "" );
	UxPutRecomputeSize( label15, "false" );
	UxPutAlignment( label15, "alignment_center" );
	UxPutBackground( label15, LabelBackground );
	UxPutForeground( label15, TextForeground );
	UxPutLabelString( label15, "LowCut" );
	UxPutHeight( label15, 25 );
	UxPutWidth( label15, 60 );
	UxPutY( label15, 199 );
	UxPutX( label15, 10 );

	UxPutBackground( rowColumn13, ApplicBackground );
	UxPutNumColumns( rowColumn13, 2 );
	UxPutMarginWidth( rowColumn13, 0 );
	UxPutMarginHeight( rowColumn13, 0 );
	UxPutResizeWidth( rowColumn13, "false" );
	UxPutResizeHeight( rowColumn13, "false" );
	UxPutSpacing( rowColumn13, 0 );
	UxPutPacking( rowColumn13, "pack_none" );
	UxPutOrientation( rowColumn13, "horizontal" );
	UxPutHeight( rowColumn13, 44 );
	UxPutWidth( rowColumn13, 60 );
	UxPutY( rowColumn13, 0 );
	UxPutX( rowColumn13, 119 );

	UxPutEntryAlignment( rowColumn20, "alignment_center" );
	UxPutAdjustLast( rowColumn20, "true" );
	UxPutAdjustMargin( rowColumn20, "true" );
	UxPutSpacing( rowColumn20, 0 );
	UxPutPacking( rowColumn20, "pack_none" );
	UxPutMarginWidth( rowColumn20, 0 );
	UxPutMarginHeight( rowColumn20, 0 );
	UxPutBackground( rowColumn20, ApplicBackground );
	UxPutResizeWidth( rowColumn20, "false" );
	UxPutResizeHeight( rowColumn20, "false" );
	UxPutHeight( rowColumn20, 32 );
	UxPutWidth( rowColumn20, 23 );
	UxPutY( rowColumn20, 6 );
	UxPutX( rowColumn20, 25 );

	UxPutHighlightThickness( arrowButton5, 0 );
	UxPutForeground( arrowButton5, "PaleGreen" );
	UxPutTranslations( arrowButton5, Arrows2 );
	UxPutShadowThickness( arrowButton5, 0 );
	UxPutBackground( arrowButton5, TextBackground );
	UxPutHeight( arrowButton5, 15 );
	UxPutWidth( arrowButton5, 23 );
	UxPutY( arrowButton5, 0 );
	UxPutX( arrowButton5, 0 );

	UxPutHighlightThickness( arrowButton6, 0 );
	UxPutBorderColor( arrowButton6, ApplicBackground );
	UxPutForeground( arrowButton6, "PaleGreen" );
	UxPutTranslations( arrowButton6, Arrows2 );
	UxPutArrowDirection( arrowButton6, "arrow_down" );
	UxPutShadowThickness( arrowButton6, 0 );
	UxPutBackground( arrowButton6, TextBackground );
	UxPutHeight( arrowButton6, 15 );
	UxPutWidth( arrowButton6, 23 );
	UxPutY( arrowButton6, 16 );

	UxPutColumns( text2, 1 );
	UxPutMaxLength( text2, 100 );
	UxPutMarginWidth( text2, 1 );
	UxPutMarginHeight( text2, 2 );
	UxPutShadowThickness( text2, 2 );
	UxPutHighlightThickness( text2, 2 );
	UxPutForeground( text2, TextForeground );
	UxPutFontList( text2, TextFont );
	UxPutBackground( text2, TextBackground );
	UxPutHeight( text2, 32 );
	UxPutWidth( text2, 25 );
	UxPutY( text2, 7 );
	UxPutX( text2, 1 );

	UxPutFontList( label32, BoldTextFont );
	UxPutTranslations( label32, "" );
	UxPutRecomputeSize( label32, "false" );
	UxPutAlignment( label32, "alignment_center" );
	UxPutBackground( label32, LabelBackground );
	UxPutForeground( label32, TextForeground );
	UxPutLabelString( label32, "Window_ID" );
	UxPutHeight( label32, 25 );
	UxPutWidth( label32, 80 );
	UxPutY( label32, 65 );
	UxPutX( label32, 20 );

	UxPutBackground( rowColumn18, ApplicBackground );
	UxPutNumColumns( rowColumn18, 2 );
	UxPutMarginWidth( rowColumn18, 0 );
	UxPutMarginHeight( rowColumn18, 0 );
	UxPutResizeWidth( rowColumn18, "false" );
	UxPutResizeHeight( rowColumn18, "false" );
	UxPutSpacing( rowColumn18, 0 );
	UxPutPacking( rowColumn18, "pack_none" );
	UxPutOrientation( rowColumn18, "horizontal" );
	UxPutHeight( rowColumn18, 55 );
	UxPutWidth( rowColumn18, 210 );
	UxPutY( rowColumn18, 100 );
	UxPutX( rowColumn18, 108 );

	UxPutColumns( text20, 8 );
	UxPutMaxLength( text20, 100 );
	UxPutMarginWidth( text20, 1 );
	UxPutMarginHeight( text20, 2 );
	UxPutShadowThickness( text20, 2 );
	UxPutHighlightThickness( text20, 2 );
	UxPutForeground( text20, TextForeground );
	UxPutFontList( text20, TextFont );
	UxPutBackground( text20, TextBackground );
	UxPutHeight( text20, 32 );
	UxPutWidth( text20, 65 );
	UxPutY( text20, 14 );
	UxPutX( text20, 25 );

	UxPutHighlightThickness( arrowButton22, 0 );
	UxPutBorderColor( arrowButton22, ApplicBackground );
	UxPutForeground( arrowButton22, "PaleGreen" );
	UxPutTranslations( arrowButton22, Arrows2 );
	UxPutArrowDirection( arrowButton22, "arrow_down" );
	UxPutShadowThickness( arrowButton22, 0 );
	UxPutBackground( arrowButton22, TextBackground );
	UxPutHeight( arrowButton22, 15 );
	UxPutWidth( arrowButton22, 23 );
	UxPutY( arrowButton22, 40 );
	UxPutX( arrowButton22, 92 );

	UxPutHighlightThickness( arrowButton21, 0 );
	UxPutForeground( arrowButton21, "PaleGreen" );
	UxPutTranslations( arrowButton21, Arrows2 );
	UxPutShadowThickness( arrowButton21, 0 );
	UxPutBackground( arrowButton21, TextBackground );
	UxPutHeight( arrowButton21, 15 );
	UxPutWidth( arrowButton21, 23 );
	UxPutY( arrowButton21, 0 );
	UxPutX( arrowButton21, 92 );

	UxPutTranslations( arrowButton23, Arrows2 );
	UxPutShadowThickness( arrowButton23, 0 );
	UxPutHighlightThickness( arrowButton23, 0 );
	UxPutForeground( arrowButton23, "PaleGreen" );
	UxPutBackground( arrowButton23, TextBackground );
	UxPutArrowDirection( arrowButton23, "arrow_left" );
	UxPutHeight( arrowButton23, 23 );
	UxPutWidth( arrowButton23, 15 );
	UxPutY( arrowButton23, 16 );
	UxPutX( arrowButton23, 0 );

	UxPutTranslations( arrowButton24, Arrows2 );
	UxPutShadowThickness( arrowButton24, 0 );
	UxPutHighlightThickness( arrowButton24, 0 );
	UxPutForeground( arrowButton24, "PaleGreen" );
	UxPutBackground( arrowButton24, TextBackground );
	UxPutArrowDirection( arrowButton24, "arrow_right" );
	UxPutHeight( arrowButton24, 23 );
	UxPutWidth( arrowButton24, 15 );
	UxPutY( arrowButton24, 16 );
	UxPutX( arrowButton24, 195 );

	UxPutColumns( text24, 8 );
	UxPutMaxLength( text24, 100 );
	UxPutMarginWidth( text24, 1 );
	UxPutMarginHeight( text24, 2 );
	UxPutShadowThickness( text24, 2 );
	UxPutHighlightThickness( text24, 2 );
	UxPutForeground( text24, TextForeground );
	UxPutFontList( text24, TextFont );
	UxPutBackground( text24, TextBackground );
	UxPutHeight( text24, 32 );
	UxPutWidth( text24, 65 );
	UxPutY( text24, 14 );
	UxPutX( text24, 120 );

	UxPutBackground( rowColumn31, ApplicBackground );
	UxPutNumColumns( rowColumn31, 2 );
	UxPutMarginWidth( rowColumn31, 0 );
	UxPutMarginHeight( rowColumn31, 0 );
	UxPutResizeWidth( rowColumn31, "false" );
	UxPutResizeHeight( rowColumn31, "false" );
	UxPutSpacing( rowColumn31, 0 );
	UxPutPacking( rowColumn31, "pack_none" );
	UxPutOrientation( rowColumn31, "horizontal" );
	UxPutHeight( rowColumn31, 40 );
	UxPutWidth( rowColumn31, 105 );
	UxPutY( rowColumn31, 100 );
	UxPutX( rowColumn31, 98 );

	UxPutMarginWidth( text15, 1 );
	UxPutMarginHeight( text15, 2 );
	UxPutColumns( text15, 8 );
	UxPutMaxLength( text15, 240 );
	UxPutFontList( text15, TextFont );
	UxPutForeground( text15, TextForeground );
	UxPutBackground( text15, TextBackground );
	UxPutHeight( text15, 32 );
	UxPutWidth( text15, 65 );
	UxPutY( text15, 7 );
	UxPutX( text15, 5 );

	UxPutEntryAlignment( rowColumn32, "alignment_center" );
	UxPutAdjustLast( rowColumn32, "true" );
	UxPutAdjustMargin( rowColumn32, "true" );
	UxPutSpacing( rowColumn32, 0 );
	UxPutPacking( rowColumn32, "pack_none" );
	UxPutMarginWidth( rowColumn32, 0 );
	UxPutMarginHeight( rowColumn32, 0 );
	UxPutBackground( rowColumn32, ApplicBackground );
	UxPutResizeWidth( rowColumn32, "false" );
	UxPutResizeHeight( rowColumn32, "false" );
	UxPutHeight( rowColumn32, 32 );
	UxPutWidth( rowColumn32, 23 );
	UxPutY( rowColumn32, 4 );
	UxPutX( rowColumn32, 72 );

	UxPutHighlightThickness( arrowButton25, 0 );
	UxPutForeground( arrowButton25, "PaleGreen" );
	UxPutTranslations( arrowButton25, Arrows2 );
	UxPutShadowThickness( arrowButton25, 0 );
	UxPutBackground( arrowButton25, TextBackground );
	UxPutHeight( arrowButton25, 15 );
	UxPutWidth( arrowButton25, 23 );
	UxPutY( arrowButton25, 0 );
	UxPutX( arrowButton25, 0 );

	UxPutHighlightThickness( arrowButton26, 0 );
	UxPutBorderColor( arrowButton26, ApplicBackground );
	UxPutForeground( arrowButton26, "PaleGreen" );
	UxPutTranslations( arrowButton26, Arrows2 );
	UxPutArrowDirection( arrowButton26, "arrow_down" );
	UxPutShadowThickness( arrowButton26, 0 );
	UxPutBackground( arrowButton26, TextBackground );
	UxPutHeight( arrowButton26, 15 );
	UxPutWidth( arrowButton26, 23 );
	UxPutY( arrowButton26, 16 );
	UxPutX( arrowButton26, 0 );

	UxPutFontList( label28, BoldTextFont );
	UxPutTranslations( label28, "" );
	UxPutRecomputeSize( label28, "false" );
	UxPutAlignment( label28, "alignment_center" );
	UxPutBackground( label28, LabelBackground );
	UxPutForeground( label28, TextForeground );
	UxPutLabelString( label28, "HighCut" );
	UxPutHeight( label28, 25 );
	UxPutWidth( label28, 60 );
	UxPutY( label28, 199 );
	UxPutX( label28, 10 );

	UxPutBackground( rowColumn33, ApplicBackground );
	UxPutNumColumns( rowColumn33, 2 );
	UxPutMarginWidth( rowColumn33, 0 );
	UxPutMarginHeight( rowColumn33, 0 );
	UxPutResizeWidth( rowColumn33, "false" );
	UxPutResizeHeight( rowColumn33, "false" );
	UxPutSpacing( rowColumn33, 0 );
	UxPutPacking( rowColumn33, "pack_none" );
	UxPutOrientation( rowColumn33, "horizontal" );
	UxPutHeight( rowColumn33, 40 );
	UxPutWidth( rowColumn33, 105 );
	UxPutY( rowColumn33, 100 );
	UxPutX( rowColumn33, 98 );

	UxPutMarginWidth( text22, 1 );
	UxPutMarginHeight( text22, 2 );
	UxPutColumns( text22, 8 );
	UxPutMaxLength( text22, 240 );
	UxPutFontList( text22, TextFont );
	UxPutForeground( text22, TextForeground );
	UxPutBackground( text22, TextBackground );
	UxPutHeight( text22, 32 );
	UxPutWidth( text22, 65 );
	UxPutY( text22, 7 );
	UxPutX( text22, 5 );

	UxPutEntryAlignment( rowColumn34, "alignment_center" );
	UxPutAdjustLast( rowColumn34, "true" );
	UxPutAdjustMargin( rowColumn34, "true" );
	UxPutSpacing( rowColumn34, 0 );
	UxPutPacking( rowColumn34, "pack_none" );
	UxPutMarginWidth( rowColumn34, 0 );
	UxPutMarginHeight( rowColumn34, 0 );
	UxPutBackground( rowColumn34, ApplicBackground );
	UxPutResizeWidth( rowColumn34, "false" );
	UxPutResizeHeight( rowColumn34, "false" );
	UxPutHeight( rowColumn34, 32 );
	UxPutWidth( rowColumn34, 23 );
	UxPutY( rowColumn34, 6 );
	UxPutX( rowColumn34, 72 );

	UxPutHighlightThickness( arrowButton27, 0 );
	UxPutForeground( arrowButton27, "PaleGreen" );
	UxPutTranslations( arrowButton27, Arrows2 );
	UxPutShadowThickness( arrowButton27, 0 );
	UxPutBackground( arrowButton27, TextBackground );
	UxPutHeight( arrowButton27, 15 );
	UxPutWidth( arrowButton27, 23 );
	UxPutY( arrowButton27, 0 );
	UxPutX( arrowButton27, 0 );

	UxPutHighlightThickness( arrowButton28, 0 );
	UxPutBorderColor( arrowButton28, ApplicBackground );
	UxPutForeground( arrowButton28, "PaleGreen" );
	UxPutTranslations( arrowButton28, Arrows2 );
	UxPutArrowDirection( arrowButton28, "arrow_down" );
	UxPutShadowThickness( arrowButton28, 0 );
	UxPutBackground( arrowButton28, TextBackground );
	UxPutHeight( arrowButton28, 15 );
	UxPutWidth( arrowButton28, 23 );
	UxPutY( arrowButton28, 16 );
	UxPutX( arrowButton28, 0 );

	UxPutFontList( label35, BoldTextFont );
	UxPutTranslations( label35, "" );
	UxPutRecomputeSize( label35, "false" );
	UxPutAlignment( label35, "alignment_center" );
	UxPutBackground( label35, LabelBackground );
	UxPutForeground( label35, TextForeground );
	UxPutLabelString( label35, "Descr Values" );
	UxPutHeight( label35, 20 );
	UxPutWidth( label35, 100 );
	UxPutY( label35, 110 );
	UxPutX( label35, 20 );

	UxPutSelectColor( toggleButton1, "Palegreen" );
	UxPutIndicatorSize( toggleButton1, 16 );
	UxPutIndicatorOn( toggleButton1, "true" );
	UxPutSet( toggleButton1, "true" );
	UxPutBorderWidth( toggleButton1, 1 );
	UxPutRecomputeSize( toggleButton1, "false" );
	UxPutForeground( toggleButton1, TextForeground );
	UxPutFontList( toggleButton1, BoldTextFont );
	UxPutBackground( toggleButton1, TextBackground );
	UxPutIndicatorType( toggleButton1, "one_of_many" );
	UxPutLabelString( toggleButton1, "Yes" );
	UxPutHeight( toggleButton1, 28 );
	UxPutWidth( toggleButton1, 70 );
	UxPutY( toggleButton1, 0 );
	UxPutX( toggleButton1, 0 );

	UxPutFontList( label36, BoldTextFont );
	UxPutTranslations( label36, "" );
	UxPutRecomputeSize( label36, "false" );
	UxPutAlignment( label36, "alignment_center" );
	UxPutBackground( label36, LabelBackground );
	UxPutForeground( label36, TextForeground );
	UxPutLabelString( label36, "CutDelta" );
	UxPutHeight( label36, 25 );
	UxPutWidth( label36, 62 );
	UxPutY( label36, 180 );
	UxPutX( label36, 15 );

	UxPutMarginWidth( text25, 1 );
	UxPutMarginHeight( text25, 2 );
	UxPutColumns( text25, 8 );
	UxPutMaxLength( text25, 240 );
	UxPutFontList( text25, TextFont );
	UxPutForeground( text25, TextForeground );
	UxPutBackground( text25, TextBackground );
	UxPutHeight( text25, 30 );
	UxPutWidth( text25, 65 );
	UxPutY( text25, 7 );
	UxPutX( text25, 5 );

	UxPutFontList( label10, BoldTextFont );
	UxPutTranslations( label10, "" );
	UxPutRecomputeSize( label10, "false" );
	UxPutAlignment( label10, "alignment_center" );
	UxPutBackground( label10, LabelBackground );
	UxPutForeground( label10, TextForeground );
	UxPutLabelString( label10, "MinMax" );
	UxPutHeight( label10, 25 );
	UxPutWidth( label10, 65 );
	UxPutY( label10, 119 );
	UxPutX( label10, 10 );

	UxPutBackground( rowColumn17, ApplicBackground );
	UxPutNumColumns( rowColumn17, 2 );
	UxPutMarginWidth( rowColumn17, 0 );
	UxPutMarginHeight( rowColumn17, 0 );
	UxPutResizeWidth( rowColumn17, "false" );
	UxPutResizeHeight( rowColumn17, "false" );
	UxPutSpacing( rowColumn17, 0 );
	UxPutPacking( rowColumn17, "pack_none" );
	UxPutOrientation( rowColumn17, "horizontal" );
	UxPutHeight( rowColumn17, 32 );
	UxPutWidth( rowColumn17, 170 );
	UxPutY( rowColumn17, 100 );
	UxPutX( rowColumn17, 98 );

	UxPutCursorPositionVisible( text11, "false" );
	UxPutHighlightThickness( text11, 0 );
	UxPutEditable( text11, "false" );
	UxPutMarginWidth( text11, 1 );
	UxPutMarginHeight( text11, 2 );
	UxPutColumns( text11, 8 );
	UxPutMaxLength( text11, 40 );
	UxPutFontList( text11, TextFont );
	UxPutForeground( text11, TextForeground );
	UxPutBackground( text11, SHelpBackground );
	UxPutHeight( text11, 32 );
	UxPutWidth( text11, 60 );
	UxPutY( text11, 2 );
	UxPutX( text11, 0 );

	UxPutCursorPositionVisible( text12, "false" );
	UxPutHighlightThickness( text12, 0 );
	UxPutEditable( text12, "false" );
	UxPutMarginWidth( text12, 1 );
	UxPutMarginHeight( text12, 2 );
	UxPutColumns( text12, 8 );
	UxPutMaxLength( text12, 40 );
	UxPutFontList( text12, TextFont );
	UxPutForeground( text12, TextForeground );
	UxPutBackground( text12, SHelpBackground );
	UxPutHeight( text12, 32 );
	UxPutWidth( text12, 60 );
	UxPutY( text12, 2 );
	UxPutX( text12, 85 );

	UxPutBackground( rowColumn29, ApplicBackground );
	UxPutNumColumns( rowColumn29, 2 );
	UxPutMarginWidth( rowColumn29, 0 );
	UxPutMarginHeight( rowColumn29, 0 );
	UxPutResizeWidth( rowColumn29, "false" );
	UxPutResizeHeight( rowColumn29, "false" );
	UxPutSpacing( rowColumn29, 0 );
	UxPutPacking( rowColumn29, "pack_none" );
	UxPutOrientation( rowColumn29, "horizontal" );
	UxPutHeight( rowColumn29, 40 );
	UxPutWidth( rowColumn29, 70 );
	UxPutY( rowColumn29, 55 );
	UxPutX( rowColumn29, 98 );

	UxPutEntryAlignment( rowColumn30, "alignment_center" );
	UxPutAdjustLast( rowColumn30, "true" );
	UxPutAdjustMargin( rowColumn30, "true" );
	UxPutSpacing( rowColumn30, 0 );
	UxPutPacking( rowColumn30, "pack_none" );
	UxPutMarginWidth( rowColumn30, 0 );
	UxPutMarginHeight( rowColumn30, 0 );
	UxPutBackground( rowColumn30, ApplicBackground );
	UxPutResizeWidth( rowColumn30, "false" );
	UxPutResizeHeight( rowColumn30, "false" );
	UxPutHeight( rowColumn30, 32 );
	UxPutWidth( rowColumn30, 23 );
	UxPutY( rowColumn30, 6 );
	UxPutX( rowColumn30, 44 );

	UxPutHighlightThickness( arrowButton39, 0 );
	UxPutForeground( arrowButton39, "PaleGreen" );
	UxPutTranslations( arrowButton39, Arrows2 );
	UxPutShadowThickness( arrowButton39, 0 );
	UxPutBackground( arrowButton39, TextBackground );
	UxPutHeight( arrowButton39, 15 );
	UxPutWidth( arrowButton39, 23 );
	UxPutY( arrowButton39, 0 );
	UxPutX( arrowButton39, 0 );

	UxPutHighlightThickness( arrowButton40, 0 );
	UxPutBorderColor( arrowButton40, ApplicBackground );
	UxPutForeground( arrowButton40, "PaleGreen" );
	UxPutTranslations( arrowButton40, Arrows2 );
	UxPutArrowDirection( arrowButton40, "arrow_down" );
	UxPutShadowThickness( arrowButton40, 0 );
	UxPutBackground( arrowButton40, TextBackground );
	UxPutHeight( arrowButton40, 15 );
	UxPutWidth( arrowButton40, 23 );
	UxPutY( arrowButton40, 16 );
	UxPutX( arrowButton40, 0 );

	UxPutMaxLength( text39, 100 );
	UxPutMarginWidth( text39, 2 );
	UxPutMarginHeight( text39, 2 );
	UxPutShadowThickness( text39, 2 );
	UxPutHighlightThickness( text39, 2 );
	UxPutForeground( text39, TextForeground );
	UxPutFontList( text39, TextFont );
	UxPutBackground( text39, TextBackground );
	UxPutHeight( text39, 32 );
	UxPutWidth( text39, 40 );
	UxPutY( text39, 7 );
	UxPutX( text39, 0 );

	UxPutFontList( label52, BoldTextFont );
	UxPutTranslations( label52, "" );
	UxPutRecomputeSize( label52, "false" );
	UxPutAlignment( label52, "alignment_center" );
	UxPutBackground( label52, LabelBackground );
	UxPutForeground( label52, TextForeground );
	UxPutLabelString( label52, "ScaleY" );
	UxPutHeight( label52, 25 );
	UxPutWidth( label52, 60 );
	UxPutY( label52, 119 );
	UxPutX( label52, 10 );

	UxPutBackground( rowColumn44, ApplicBackground );
	UxPutNumColumns( rowColumn44, 2 );
	UxPutMarginWidth( rowColumn44, 0 );
	UxPutMarginHeight( rowColumn44, 0 );
	UxPutResizeWidth( rowColumn44, "false" );
	UxPutResizeHeight( rowColumn44, "false" );
	UxPutSpacing( rowColumn44, 0 );
	UxPutPacking( rowColumn44, "pack_none" );
	UxPutOrientation( rowColumn44, "horizontal" );
	UxPutHeight( rowColumn44, 40 );
	UxPutWidth( rowColumn44, 70 );
	UxPutY( rowColumn44, 55 );
	UxPutX( rowColumn44, 98 );

	UxPutEntryAlignment( rowColumn45, "alignment_center" );
	UxPutAdjustLast( rowColumn45, "true" );
	UxPutAdjustMargin( rowColumn45, "true" );
	UxPutSpacing( rowColumn45, 0 );
	UxPutPacking( rowColumn45, "pack_none" );
	UxPutMarginWidth( rowColumn45, 0 );
	UxPutMarginHeight( rowColumn45, 0 );
	UxPutBackground( rowColumn45, ApplicBackground );
	UxPutResizeWidth( rowColumn45, "false" );
	UxPutResizeHeight( rowColumn45, "false" );
	UxPutHeight( rowColumn45, 32 );
	UxPutWidth( rowColumn45, 23 );
	UxPutY( rowColumn45, 6 );
	UxPutX( rowColumn45, 44 );

	UxPutHighlightThickness( arrowButton45, 0 );
	UxPutForeground( arrowButton45, "PaleGreen" );
	UxPutTranslations( arrowButton45, Arrows2 );
	UxPutShadowThickness( arrowButton45, 0 );
	UxPutBackground( arrowButton45, TextBackground );
	UxPutHeight( arrowButton45, 15 );
	UxPutWidth( arrowButton45, 23 );
	UxPutY( arrowButton45, 0 );
	UxPutX( arrowButton45, 0 );

	UxPutHighlightThickness( arrowButton46, 0 );
	UxPutBorderColor( arrowButton46, ApplicBackground );
	UxPutForeground( arrowButton46, "PaleGreen" );
	UxPutTranslations( arrowButton46, Arrows2 );
	UxPutArrowDirection( arrowButton46, "arrow_down" );
	UxPutShadowThickness( arrowButton46, 0 );
	UxPutBackground( arrowButton46, TextBackground );
	UxPutHeight( arrowButton46, 15 );
	UxPutWidth( arrowButton46, 23 );
	UxPutY( arrowButton46, 16 );
	UxPutX( arrowButton46, 0 );

	UxPutMaxLength( text40, 100 );
	UxPutMarginWidth( text40, 2 );
	UxPutMarginHeight( text40, 2 );
	UxPutShadowThickness( text40, 2 );
	UxPutHighlightThickness( text40, 2 );
	UxPutForeground( text40, TextForeground );
	UxPutFontList( text40, TextFont );
	UxPutBackground( text40, TextBackground );
	UxPutHeight( text40, 32 );
	UxPutWidth( text40, 40 );
	UxPutY( text40, 7 );
	UxPutX( text40, 0 );

	UxPutBackground( rowColumn22, ApplicBackground );
	UxPutNumColumns( rowColumn22, 2 );
	UxPutMarginWidth( rowColumn22, 0 );
	UxPutMarginHeight( rowColumn22, 0 );
	UxPutResizeWidth( rowColumn22, "false" );
	UxPutResizeHeight( rowColumn22, "false" );
	UxPutSpacing( rowColumn22, 0 );
	UxPutPacking( rowColumn22, "pack_none" );
	UxPutOrientation( rowColumn22, "horizontal" );
	UxPutHeight( rowColumn22, 44 );
	UxPutWidth( rowColumn22, 50 );
	UxPutY( rowColumn22, 0 );
	UxPutX( rowColumn22, 119 );

	UxPutEntryAlignment( rowColumn28, "alignment_center" );
	UxPutAdjustLast( rowColumn28, "true" );
	UxPutAdjustMargin( rowColumn28, "true" );
	UxPutSpacing( rowColumn28, 0 );
	UxPutPacking( rowColumn28, "pack_none" );
	UxPutMarginWidth( rowColumn28, 0 );
	UxPutMarginHeight( rowColumn28, 0 );
	UxPutBackground( rowColumn28, ApplicBackground );
	UxPutResizeWidth( rowColumn28, "false" );
	UxPutResizeHeight( rowColumn28, "false" );
	UxPutHeight( rowColumn28, 32 );
	UxPutWidth( rowColumn28, 23 );
	UxPutY( rowColumn28, 6 );
	UxPutX( rowColumn28, 25 );

	UxPutHighlightThickness( arrowButton47, 0 );
	UxPutForeground( arrowButton47, "PaleGreen" );
	UxPutTranslations( arrowButton47, Arrows2 );
	UxPutShadowThickness( arrowButton47, 0 );
	UxPutBackground( arrowButton47, TextBackground );
	UxPutHeight( arrowButton47, 15 );
	UxPutWidth( arrowButton47, 23 );
	UxPutY( arrowButton47, 0 );
	UxPutX( arrowButton47, 0 );

	UxPutHighlightThickness( arrowButton48, 0 );
	UxPutBorderColor( arrowButton48, ApplicBackground );
	UxPutForeground( arrowButton48, "PaleGreen" );
	UxPutTranslations( arrowButton48, Arrows2 );
	UxPutArrowDirection( arrowButton48, "arrow_down" );
	UxPutShadowThickness( arrowButton48, 0 );
	UxPutBackground( arrowButton48, TextBackground );
	UxPutHeight( arrowButton48, 15 );
	UxPutWidth( arrowButton48, 23 );
	UxPutY( arrowButton48, 16 );

	UxPutColumns( text41, 1 );
	UxPutMaxLength( text41, 100 );
	UxPutMarginWidth( text41, 1 );
	UxPutMarginHeight( text41, 2 );
	UxPutShadowThickness( text41, 2 );
	UxPutHighlightThickness( text41, 2 );
	UxPutForeground( text41, TextForeground );
	UxPutFontList( text41, TextFont );
	UxPutBackground( text41, TextBackground );
	UxPutHeight( text41, 32 );
	UxPutWidth( text41, 25 );
	UxPutY( text41, 7 );
	UxPutX( text41, 1 );

	UxPutFontList( label53, BoldTextFont );
	UxPutTranslations( label53, "" );
	UxPutRecomputeSize( label53, "false" );
	UxPutAlignment( label53, "alignment_center" );
	UxPutBackground( label53, LabelBackground );
	UxPutForeground( label53, TextForeground );
	UxPutLabelString( label53, "Use" );
	UxPutHeight( label53, 20 );
	UxPutWidth( label53, 100 );
	UxPutY( label53, 110 );
	UxPutX( label53, 20 );

	UxPutFontList( SHelp, TextFont );
	UxPutForeground( SHelp, TextForeground );
	UxPutBackground( SHelp, SHelpBackground );
	UxPutHeight( SHelp, 50 );
	UxPutWidth( SHelp, 410 );
	UxPutY( SHelp, 100 );
	UxPutX( SHelp, 10 );

	UxPutRecomputeSize( pushButton1, "false" );
	UxPutForeground( pushButton1, "PaleGreen" );
	UxPutFontList( pushButton1, BoldTextFont );
	UxPutTranslations( pushButton1, "" );
	UxPutLabelString( pushButton1, "Apply" );
	UxPutBackground( pushButton1, ButtonBackground );
	UxPutHeight( pushButton1, 40 );
	UxPutWidth( pushButton1, 50 );
	UxPutY( pushButton1, 480 );
	UxPutX( pushButton1, 350 );

	UxPutMappedWhenManaged( form4, "false" );
	UxPutBorderWidth( form4, 1 );
	UxPutBackground( form4, ApplicBackground );
	UxPutHeight( form4, 240 );
	UxPutWidth( form4, 430 );
	UxPutY( form4, 10 );
	UxPutX( form4, 10 );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutFontList( label2, BoldTextFont );
	UxPutTranslations( label2, "" );
	UxPutRecomputeSize( label2, "false" );
	UxPutAlignment( label2, "alignment_center" );
	UxPutBackground( label2, LabelBackground );
	UxPutForeground( label2, TextForeground );
	UxPutLabelString( label2, "LUT" );
	UxPutHeight( label2, 25 );
	UxPutWidth( label2, 89 );
	UxPutY( label2, 10 );
	UxPutX( label2, 10 );

	UxPutMarginWidth( text9, 2 );
	UxPutMarginHeight( text9, 2 );
	UxPutFontList( text9, TextFont );
	UxPutForeground( text9, TextForeground );
	UxPutTranslations( text9, SelectFile );
	UxPutBackground( text9, TextBackground );
	UxPutHeight( text9, 30 );
	UxPutWidth( text9, 379 );
	UxPutY( text9, 39 );
	UxPutX( text9, 10 );

	UxPutFontList( label1, BoldTextFont );
	UxPutTranslations( label1, "" );
	UxPutRecomputeSize( label1, "false" );
	UxPutAlignment( label1, "alignment_center" );
	UxPutBackground( label1, LabelBackground );
	UxPutForeground( label1, TextForeground );
	UxPutLabelString( label1, "DisplayLUT" );
	UxPutHeight( label1, 25 );
	UxPutWidth( label1, 120 );
	UxPutY( label1, 99 );
	UxPutX( label1, 9 );

	UxPutSelectColor( toggleButton10, "Palegreen" );
	UxPutIndicatorSize( toggleButton10, 16 );
	UxPutIndicatorOn( toggleButton10, "true" );
	UxPutSet( toggleButton10, "true" );
	UxPutBorderWidth( toggleButton10, 1 );
	UxPutRecomputeSize( toggleButton10, "false" );
	UxPutForeground( toggleButton10, TextForeground );
	UxPutFontList( toggleButton10, BoldTextFont );
	UxPutBackground( toggleButton10, TextBackground );
	UxPutIndicatorType( toggleButton10, "one_of_many" );
	UxPutLabelString( toggleButton10, "Yes" );
	UxPutHeight( toggleButton10, 28 );
	UxPutWidth( toggleButton10, 70 );
	UxPutY( toggleButton10, 99 );
	UxPutX( toggleButton10, 129 );

	UxPutFontList( label26, BoldTextFont );
	UxPutTranslations( label26, "" );
	UxPutRecomputeSize( label26, "false" );
	UxPutAlignment( label26, "alignment_center" );
	UxPutBackground( label26, LabelBackground );
	UxPutForeground( label26, TextForeground );
	UxPutLabelString( label26, "ITT" );
	UxPutHeight( label26, 25 );
	UxPutWidth( label26, 89 );
	UxPutY( label26, 30 );
	UxPutX( label26, 15 );

	UxPutTopCharacter( text23, 1 );
	UxPutMarginWidth( text23, 2 );
	UxPutMarginHeight( text23, 2 );
	UxPutFontList( text23, TextFont );
	UxPutForeground( text23, TextForeground );
	UxPutTranslations( text23, SelectFile );
	UxPutBackground( text23, TextBackground );
	UxPutHeight( text23, 30 );
	UxPutWidth( text23, 379 );
	UxPutY( text23, 39 );
	UxPutX( text23, 10 );

	UxPutMappedWhenManaged( form3, "false" );
	UxPutBorderWidth( form3, 1 );
	UxPutBackground( form3, ApplicBackground );
	UxPutHeight( form3, 240 );
	UxPutWidth( form3, 430 );
	UxPutY( form3, 10 );
	UxPutX( form3, 10 );
	UxPutResizePolicy( form3, "resize_none" );

	UxPutFontList( label9, BoldTextFont );
	UxPutTranslations( label9, "" );
	UxPutRecomputeSize( label9, "false" );
	UxPutAlignment( label9, "alignment_center" );
	UxPutBackground( label9, LabelBackground );
	UxPutForeground( label9, TextForeground );
	UxPutLabelString( label9, "Frame" );
	UxPutHeight( label9, 25 );
	UxPutWidth( label9, 89 );
	UxPutY( label9, 10 );
	UxPutX( label9, 10 );

	UxPutMarginWidth( text10, 2 );
	UxPutMarginHeight( text10, 2 );
	UxPutColumns( text10, 30 );
	UxPutFontList( text10, TextFont );
	UxPutForeground( text10, TextForeground );
	UxPutTranslations( text10, SelectFile );
	UxPutBackground( text10, TextBackground );
	UxPutHeight( text10, 30 );
	UxPutWidth( text10, 250 );
	UxPutY( text10, 39 );
	UxPutX( text10, 9 );

	UxPutFontList( label13, BoldTextFont );
	UxPutTranslations( label13, "" );
	UxPutRecomputeSize( label13, "false" );
	UxPutAlignment( label13, "alignment_center" );
	UxPutBackground( label13, LabelBackground );
	UxPutForeground( label13, TextForeground );
	UxPutLabelString( label13, "Out_Table" );
	UxPutHeight( label13, 25 );
	UxPutWidth( label13, 89 );
	UxPutY( label13, 79 );
	UxPutX( label13, 10 );

	UxPutMarginWidth( text17, 2 );
	UxPutMarginHeight( text17, 2 );
	UxPutColumns( text17, 25 );
	UxPutFontList( text17, TextFont );
	UxPutForeground( text17, TextForeground );
	UxPutBackground( text17, TextBackground );
	UxPutHeight( text17, 32 );
	UxPutWidth( text17, 340 );
	UxPutY( text17, 79 );
	UxPutX( text17, 119 );

	UxPutFontList( label11, BoldTextFont );
	UxPutTranslations( label11, "" );
	UxPutRecomputeSize( label11, "false" );
	UxPutAlignment( label11, "alignment_center" );
	UxPutBackground( label11, LabelBackground );
	UxPutForeground( label11, TextForeground );
	UxPutLabelString( label11, "Plot_Option" );
	UxPutHeight( label11, 25 );
	UxPutWidth( label11, 89 );
	UxPutY( label11, 119 );
	UxPutX( label11, 10 );

	UxPutFontList( label12, BoldTextFont );
	UxPutTranslations( label12, "" );
	UxPutRecomputeSize( label12, "false" );
	UxPutAlignment( label12, "alignment_center" );
	UxPutBackground( label12, LabelBackground );
	UxPutForeground( label12, TextForeground );
	UxPutLabelString( label12, "File_Format" );
	UxPutHeight( label12, 25 );
	UxPutWidth( label12, 89 );
	UxPutY( label12, 159 );
	UxPutX( label12, 10 );

	UxPutMarginRight( toggleButton2, 0 );
	UxPutMarginLeft( toggleButton2, 25 );
	UxPutSelectColor( toggleButton2, "Palegreen" );
	UxPutIndicatorSize( toggleButton2, 16 );
	UxPutIndicatorOn( toggleButton2, "true" );
	UxPutSet( toggleButton2, "true" );
	UxPutBorderWidth( toggleButton2, 1 );
	UxPutRecomputeSize( toggleButton2, "false" );
	UxPutForeground( toggleButton2, TextForeground );
	UxPutFontList( toggleButton2, BoldTextFont );
	UxPutBackground( toggleButton2, TextBackground );
	UxPutIndicatorType( toggleButton2, "one_of_many" );
	UxPutLabelString( toggleButton2, "Plot" );
	UxPutHeight( toggleButton2, 28 );
	UxPutWidth( toggleButton2, 75 );
	UxPutY( toggleButton2, 10 );
	UxPutX( toggleButton2, 10 );

	UxPutTopShadowColor( toggleButton4, LabelBackground );
	UxPutSelectColor( toggleButton4, "Palegreen" );
	UxPutIndicatorSize( toggleButton4, 16 );
	UxPutIndicatorOn( toggleButton4, "true" );
	UxPutSet( toggleButton4, "true" );
	UxPutBorderWidth( toggleButton4, 1 );
	UxPutRecomputeSize( toggleButton4, "false" );
	UxPutForeground( toggleButton4, TextForeground );
	UxPutFontList( toggleButton4, BoldTextFont );
	UxPutBackground( toggleButton4, TextBackground );
	UxPutIndicatorType( toggleButton4, "one_of_many" );
	UxPutLabelString( toggleButton4, "Midas" );
	UxPutHeight( toggleButton4, 28 );
	UxPutWidth( toggleButton4, 75 );
	UxPutY( toggleButton4, 10 );
	UxPutX( toggleButton4, 10 );

	UxPutMappedWhenManaged( form6, "true" );
	UxPutBorderWidth( form6, 1 );
	UxPutBackground( form6, ApplicBackground );
	UxPutHeight( form6, 240 );
	UxPutWidth( form6, 430 );
	UxPutY( form6, 10 );
	UxPutX( form6, 10 );
	UxPutResizePolicy( form6, "resize_none" );

	UxPutFontList( label16, BoldTextFont );
	UxPutTranslations( label16, "" );
	UxPutRecomputeSize( label16, "false" );
	UxPutAlignment( label16, "alignment_center" );
	UxPutBackground( label16, LabelBackground );
	UxPutForeground( label16, TextForeground );
	UxPutLabelString( label16, "Window_ID" );
	UxPutHeight( label16, 25 );
	UxPutWidth( label16, 84 );
	UxPutY( label16, 9 );
	UxPutX( label16, 9 );

	UxPutFontList( label17, BoldTextFont );
	UxPutTranslations( label17, "" );
	UxPutRecomputeSize( label17, "false" );
	UxPutAlignment( label17, "alignment_center" );
	UxPutBackground( label17, LabelBackground );
	UxPutForeground( label17, TextForeground );
	UxPutLabelString( label17, "SizeX" );
	UxPutHeight( label17, 25 );
	UxPutWidth( label17, 80 );
	UxPutY( label17, 49 );
	UxPutX( label17, 9 );

	UxPutFontList( label5, BoldTextFont );
	UxPutTranslations( label5, "" );
	UxPutRecomputeSize( label5, "false" );
	UxPutAlignment( label5, "alignment_center" );
	UxPutBackground( label5, LabelBackground );
	UxPutForeground( label5, TextForeground );
	UxPutLabelString( label5, "OffsetX" );
	UxPutHeight( label5, 25 );
	UxPutWidth( label5, 80 );
	UxPutY( label5, 89 );
	UxPutX( label5, 9 );

	UxPutFontList( label19, BoldTextFont );
	UxPutTranslations( label19, "" );
	UxPutRecomputeSize( label19, "false" );
	UxPutAlignment( label19, "alignment_center" );
	UxPutBackground( label19, LabelBackground );
	UxPutForeground( label19, TextForeground );
	UxPutLabelString( label19, "Alpha_Memory" );
	UxPutHeight( label19, 25 );
	UxPutWidth( label19, 120 );
	UxPutY( label19, 139 );
	UxPutX( label19, 9 );

	UxPutFontList( label20, BoldTextFont );
	UxPutTranslations( label20, "" );
	UxPutRecomputeSize( label20, "false" );
	UxPutAlignment( label20, "alignment_center" );
	UxPutBackground( label20, LabelBackground );
	UxPutForeground( label20, TextForeground );
	UxPutLabelString( label20, "Gsize" );
	UxPutHeight( label20, 25 );
	UxPutWidth( label20, 80 );
	UxPutY( label20, 189 );
	UxPutX( label20, 9 );

	UxPutFontList( label21, BoldTextFont );
	UxPutTranslations( label21, "" );
	UxPutRecomputeSize( label21, "false" );
	UxPutAlignment( label21, "alignment_center" );
	UxPutBackground( label21, LabelBackground );
	UxPutForeground( label21, TextForeground );
	UxPutLabelString( label21, "Xstation" );
	UxPutHeight( label21, 25 );
	UxPutWidth( label21, 80 );
	UxPutY( label21, 239 );
	UxPutX( label21, 9 );

	UxPutMarginWidth( text21, 1 );
	UxPutMarginHeight( text21, 2 );
	UxPutTopCharacter( text21, 0 );
	UxPutBorderWidth( text21, 0 );
	UxPutFontList( text21, TextFont );
	UxPutForeground( text21, TextForeground );
	UxPutBackground( text21, TextBackground );
	UxPutHeight( text21, 32 );
	UxPutWidth( text21, 160 );
	UxPutY( text21, 239 );
	UxPutX( text21, 109 );

	UxPutBackground( rowColumn1, ApplicBackground );
	UxPutNumColumns( rowColumn1, 2 );
	UxPutMarginWidth( rowColumn1, 0 );
	UxPutMarginHeight( rowColumn1, 0 );
	UxPutResizeWidth( rowColumn1, "false" );
	UxPutResizeHeight( rowColumn1, "false" );
	UxPutSpacing( rowColumn1, 0 );
	UxPutPacking( rowColumn1, "pack_none" );
	UxPutOrientation( rowColumn1, "horizontal" );
	UxPutHeight( rowColumn1, 44 );
	UxPutWidth( rowColumn1, 60 );
	UxPutY( rowColumn1, 0 );
	UxPutX( rowColumn1, 119 );

	UxPutEntryAlignment( rowColumn9, "alignment_center" );
	UxPutAdjustLast( rowColumn9, "true" );
	UxPutAdjustMargin( rowColumn9, "true" );
	UxPutSpacing( rowColumn9, 0 );
	UxPutPacking( rowColumn9, "pack_none" );
	UxPutMarginWidth( rowColumn9, 0 );
	UxPutMarginHeight( rowColumn9, 0 );
	UxPutBackground( rowColumn9, ApplicBackground );
	UxPutResizeWidth( rowColumn9, "false" );
	UxPutResizeHeight( rowColumn9, "false" );
	UxPutHeight( rowColumn9, 32 );
	UxPutWidth( rowColumn9, 23 );
	UxPutY( rowColumn9, 6 );
	UxPutX( rowColumn9, 30 );

	UxPutHighlightThickness( arrowButton7, 0 );
	UxPutForeground( arrowButton7, "PaleGreen" );
	UxPutTranslations( arrowButton7, Arrows6 );
	UxPutShadowThickness( arrowButton7, 0 );
	UxPutBackground( arrowButton7, TextBackground );
	UxPutHeight( arrowButton7, 15 );
	UxPutWidth( arrowButton7, 23 );
	UxPutY( arrowButton7, 0 );
	UxPutX( arrowButton7, 0 );

	UxPutHighlightThickness( arrowButton8, 0 );
	UxPutBorderColor( arrowButton8, ApplicBackground );
	UxPutForeground( arrowButton8, "PaleGreen" );
	UxPutTranslations( arrowButton8, Arrows6 );
	UxPutArrowDirection( arrowButton8, "arrow_down" );
	UxPutShadowThickness( arrowButton8, 0 );
	UxPutBackground( arrowButton8, TextBackground );
	UxPutHeight( arrowButton8, 15 );
	UxPutWidth( arrowButton8, 23 );
	UxPutY( arrowButton8, 16 );
	UxPutX( arrowButton8, 0 );

	UxPutColumns( text1, 1 );
	UxPutMaxLength( text1, 100 );
	UxPutMarginWidth( text1, 1 );
	UxPutMarginHeight( text1, 2 );
	UxPutShadowThickness( text1, 2 );
	UxPutHighlightThickness( text1, 2 );
	UxPutForeground( text1, TextForeground );
	UxPutFontList( text1, TextFont );
	UxPutBackground( text1, TextBackground );
	UxPutHeight( text1, 32 );
	UxPutWidth( text1, 25 );
	UxPutY( text1, 7 );
	UxPutX( text1, 5 );

	UxPutBackground( rowColumn2, ApplicBackground );
	UxPutNumColumns( rowColumn2, 2 );
	UxPutMarginWidth( rowColumn2, 0 );
	UxPutMarginHeight( rowColumn2, 0 );
	UxPutResizeWidth( rowColumn2, "false" );
	UxPutResizeHeight( rowColumn2, "false" );
	UxPutSpacing( rowColumn2, 0 );
	UxPutPacking( rowColumn2, "pack_none" );
	UxPutOrientation( rowColumn2, "horizontal" );
	UxPutHeight( rowColumn2, 44 );
	UxPutWidth( rowColumn2, 100 );
	UxPutY( rowColumn2, 15 );
	UxPutX( rowColumn2, 114 );

	UxPutEntryAlignment( rowColumn10, "alignment_center" );
	UxPutAdjustLast( rowColumn10, "true" );
	UxPutAdjustMargin( rowColumn10, "true" );
	UxPutSpacing( rowColumn10, 0 );
	UxPutPacking( rowColumn10, "pack_none" );
	UxPutMarginWidth( rowColumn10, 0 );
	UxPutMarginHeight( rowColumn10, 0 );
	UxPutBackground( rowColumn10, ApplicBackground );
	UxPutResizeWidth( rowColumn10, "false" );
	UxPutResizeHeight( rowColumn10, "false" );
	UxPutHeight( rowColumn10, 32 );
	UxPutWidth( rowColumn10, 23 );
	UxPutY( rowColumn10, 6 );
	UxPutX( rowColumn10, 65 );

	UxPutHighlightThickness( arrowButton3, 0 );
	UxPutForeground( arrowButton3, "PaleGreen" );
	UxPutTranslations( arrowButton3, Arrows6 );
	UxPutShadowThickness( arrowButton3, 0 );
	UxPutBackground( arrowButton3, TextBackground );
	UxPutHeight( arrowButton3, 15 );
	UxPutWidth( arrowButton3, 23 );
	UxPutY( arrowButton3, 0 );
	UxPutX( arrowButton3, 0 );

	UxPutHighlightThickness( arrowButton4, 0 );
	UxPutBorderColor( arrowButton4, ApplicBackground );
	UxPutForeground( arrowButton4, "PaleGreen" );
	UxPutTranslations( arrowButton4, Arrows6 );
	UxPutArrowDirection( arrowButton4, "arrow_down" );
	UxPutShadowThickness( arrowButton4, 0 );
	UxPutBackground( arrowButton4, TextBackground );
	UxPutHeight( arrowButton4, 15 );
	UxPutWidth( arrowButton4, 23 );
	UxPutY( arrowButton4, 16 );
	UxPutX( arrowButton4, 0 );

	UxPutMaxLength( text4, 100 );
	UxPutMarginWidth( text4, 2 );
	UxPutMarginHeight( text4, 2 );
	UxPutShadowThickness( text4, 2 );
	UxPutHighlightThickness( text4, 2 );
	UxPutForeground( text4, TextForeground );
	UxPutFontList( text4, TextFont );
	UxPutBackground( text4, TextBackground );
	UxPutHeight( text4, 32 );
	UxPutWidth( text4, 50 );
	UxPutY( text4, 7 );
	UxPutX( text4, 5 );

	UxPutFontList( label18, BoldTextFont );
	UxPutTranslations( label18, "" );
	UxPutRecomputeSize( label18, "false" );
	UxPutAlignment( label18, "alignment_center" );
	UxPutBackground( label18, LabelBackground );
	UxPutForeground( label18, TextForeground );
	UxPutLabelString( label18, "SizeY" );
	UxPutHeight( label18, 25 );
	UxPutWidth( label18, 80 );
	UxPutY( label18, 59 );
	UxPutX( label18, 209 );

	UxPutBackground( rowColumn3, ApplicBackground );
	UxPutNumColumns( rowColumn3, 2 );
	UxPutMarginWidth( rowColumn3, 0 );
	UxPutMarginHeight( rowColumn3, 0 );
	UxPutResizeWidth( rowColumn3, "false" );
	UxPutResizeHeight( rowColumn3, "false" );
	UxPutSpacing( rowColumn3, 0 );
	UxPutPacking( rowColumn3, "pack_none" );
	UxPutOrientation( rowColumn3, "horizontal" );
	UxPutHeight( rowColumn3, 44 );
	UxPutWidth( rowColumn3, 100 );
	UxPutY( rowColumn3, 60 );
	UxPutX( rowColumn3, 112 );

	UxPutEntryAlignment( rowColumn11, "alignment_center" );
	UxPutAdjustLast( rowColumn11, "true" );
	UxPutAdjustMargin( rowColumn11, "true" );
	UxPutSpacing( rowColumn11, 0 );
	UxPutPacking( rowColumn11, "pack_none" );
	UxPutMarginWidth( rowColumn11, 0 );
	UxPutMarginHeight( rowColumn11, 0 );
	UxPutBackground( rowColumn11, ApplicBackground );
	UxPutResizeWidth( rowColumn11, "false" );
	UxPutResizeHeight( rowColumn11, "false" );
	UxPutHeight( rowColumn11, 32 );
	UxPutWidth( rowColumn11, 23 );
	UxPutY( rowColumn11, 6 );
	UxPutX( rowColumn11, 65 );

	UxPutHighlightThickness( arrowButton9, 0 );
	UxPutForeground( arrowButton9, "PaleGreen" );
	UxPutTranslations( arrowButton9, Arrows6 );
	UxPutShadowThickness( arrowButton9, 0 );
	UxPutBackground( arrowButton9, TextBackground );
	UxPutHeight( arrowButton9, 15 );
	UxPutWidth( arrowButton9, 23 );
	UxPutY( arrowButton9, 0 );
	UxPutX( arrowButton9, 0 );

	UxPutHighlightThickness( arrowButton10, 0 );
	UxPutBorderColor( arrowButton10, ApplicBackground );
	UxPutForeground( arrowButton10, "PaleGreen" );
	UxPutTranslations( arrowButton10, Arrows6 );
	UxPutArrowDirection( arrowButton10, "arrow_down" );
	UxPutShadowThickness( arrowButton10, 0 );
	UxPutBackground( arrowButton10, TextBackground );
	UxPutHeight( arrowButton10, 15 );
	UxPutWidth( arrowButton10, 23 );
	UxPutY( arrowButton10, 16 );
	UxPutX( arrowButton10, 0 );

	UxPutMaxLength( text5, 100 );
	UxPutMarginWidth( text5, 2 );
	UxPutMarginHeight( text5, 2 );
	UxPutShadowThickness( text5, 2 );
	UxPutHighlightThickness( text5, 2 );
	UxPutForeground( text5, TextForeground );
	UxPutFontList( text5, TextFont );
	UxPutBackground( text5, TextBackground );
	UxPutHeight( text5, 32 );
	UxPutWidth( text5, 50 );
	UxPutY( text5, 7 );
	UxPutX( text5, 5 );

	UxPutBackground( rowColumn5, ApplicBackground );
	UxPutNumColumns( rowColumn5, 2 );
	UxPutMarginWidth( rowColumn5, 0 );
	UxPutMarginHeight( rowColumn5, 0 );
	UxPutResizeWidth( rowColumn5, "false" );
	UxPutResizeHeight( rowColumn5, "false" );
	UxPutSpacing( rowColumn5, 0 );
	UxPutPacking( rowColumn5, "pack_none" );
	UxPutOrientation( rowColumn5, "horizontal" );
	UxPutHeight( rowColumn5, 44 );
	UxPutWidth( rowColumn5, 100 );
	UxPutY( rowColumn5, 60 );
	UxPutX( rowColumn5, 112 );

	UxPutEntryAlignment( rowColumn12, "alignment_center" );
	UxPutAdjustLast( rowColumn12, "true" );
	UxPutAdjustMargin( rowColumn12, "true" );
	UxPutSpacing( rowColumn12, 0 );
	UxPutPacking( rowColumn12, "pack_none" );
	UxPutMarginWidth( rowColumn12, 0 );
	UxPutMarginHeight( rowColumn12, 0 );
	UxPutBackground( rowColumn12, ApplicBackground );
	UxPutResizeWidth( rowColumn12, "false" );
	UxPutResizeHeight( rowColumn12, "false" );
	UxPutHeight( rowColumn12, 32 );
	UxPutWidth( rowColumn12, 23 );
	UxPutY( rowColumn12, 6 );
	UxPutX( rowColumn12, 65 );

	UxPutHighlightThickness( arrowButton11, 0 );
	UxPutForeground( arrowButton11, "PaleGreen" );
	UxPutTranslations( arrowButton11, Arrows6 );
	UxPutShadowThickness( arrowButton11, 0 );
	UxPutBackground( arrowButton11, TextBackground );
	UxPutHeight( arrowButton11, 15 );
	UxPutWidth( arrowButton11, 23 );
	UxPutY( arrowButton11, 0 );
	UxPutX( arrowButton11, 0 );

	UxPutHighlightThickness( arrowButton12, 0 );
	UxPutBorderColor( arrowButton12, ApplicBackground );
	UxPutForeground( arrowButton12, "PaleGreen" );
	UxPutTranslations( arrowButton12, Arrows6 );
	UxPutArrowDirection( arrowButton12, "arrow_down" );
	UxPutShadowThickness( arrowButton12, 0 );
	UxPutBackground( arrowButton12, TextBackground );
	UxPutHeight( arrowButton12, 15 );
	UxPutWidth( arrowButton12, 23 );
	UxPutY( arrowButton12, 16 );
	UxPutX( arrowButton12, 0 );

	UxPutMaxLength( text6, 100 );
	UxPutMarginWidth( text6, 2 );
	UxPutMarginHeight( text6, 2 );
	UxPutShadowThickness( text6, 2 );
	UxPutHighlightThickness( text6, 2 );
	UxPutForeground( text6, TextForeground );
	UxPutFontList( text6, TextFont );
	UxPutBackground( text6, TextBackground );
	UxPutHeight( text6, 32 );
	UxPutWidth( text6, 50 );
	UxPutY( text6, 7 );
	UxPutX( text6, 5 );

	UxPutFontList( label22, BoldTextFont );
	UxPutTranslations( label22, "" );
	UxPutRecomputeSize( label22, "false" );
	UxPutAlignment( label22, "alignment_center" );
	UxPutBackground( label22, LabelBackground );
	UxPutForeground( label22, TextForeground );
	UxPutLabelString( label22, "OffsetY" );
	UxPutHeight( label22, 25 );
	UxPutWidth( label22, 80 );
	UxPutY( label22, 115 );
	UxPutX( label22, 20 );

	UxPutBackground( rowColumn4, ApplicBackground );
	UxPutNumColumns( rowColumn4, 2 );
	UxPutMarginWidth( rowColumn4, 0 );
	UxPutMarginHeight( rowColumn4, 0 );
	UxPutResizeWidth( rowColumn4, "false" );
	UxPutResizeHeight( rowColumn4, "false" );
	UxPutSpacing( rowColumn4, 0 );
	UxPutPacking( rowColumn4, "pack_none" );
	UxPutOrientation( rowColumn4, "horizontal" );
	UxPutHeight( rowColumn4, 44 );
	UxPutWidth( rowColumn4, 100 );
	UxPutY( rowColumn4, 105 );
	UxPutX( rowColumn4, 100 );

	UxPutEntryAlignment( rowColumn14, "alignment_center" );
	UxPutAdjustLast( rowColumn14, "true" );
	UxPutAdjustMargin( rowColumn14, "true" );
	UxPutSpacing( rowColumn14, 0 );
	UxPutPacking( rowColumn14, "pack_none" );
	UxPutMarginWidth( rowColumn14, 0 );
	UxPutMarginHeight( rowColumn14, 0 );
	UxPutBackground( rowColumn14, ApplicBackground );
	UxPutResizeWidth( rowColumn14, "false" );
	UxPutResizeHeight( rowColumn14, "false" );
	UxPutHeight( rowColumn14, 32 );
	UxPutWidth( rowColumn14, 23 );
	UxPutY( rowColumn14, 6 );
	UxPutX( rowColumn14, 65 );

	UxPutHighlightThickness( arrowButton17, 0 );
	UxPutForeground( arrowButton17, "PaleGreen" );
	UxPutTranslations( arrowButton17, Arrows6 );
	UxPutShadowThickness( arrowButton17, 0 );
	UxPutBackground( arrowButton17, TextBackground );
	UxPutHeight( arrowButton17, 15 );
	UxPutWidth( arrowButton17, 23 );
	UxPutY( arrowButton17, 0 );
	UxPutX( arrowButton17, 0 );

	UxPutHighlightThickness( arrowButton18, 0 );
	UxPutBorderColor( arrowButton18, ApplicBackground );
	UxPutForeground( arrowButton18, "PaleGreen" );
	UxPutTranslations( arrowButton18, Arrows6 );
	UxPutArrowDirection( arrowButton18, "arrow_down" );
	UxPutShadowThickness( arrowButton18, 0 );
	UxPutBackground( arrowButton18, TextBackground );
	UxPutHeight( arrowButton18, 15 );
	UxPutWidth( arrowButton18, 23 );
	UxPutY( arrowButton18, 16 );
	UxPutX( arrowButton18, 0 );

	UxPutMaxLength( text8, 100 );
	UxPutMarginWidth( text8, 2 );
	UxPutMarginHeight( text8, 2 );
	UxPutShadowThickness( text8, 2 );
	UxPutHighlightThickness( text8, 2 );
	UxPutForeground( text8, TextForeground );
	UxPutFontList( text8, TextFont );
	UxPutBackground( text8, TextBackground );
	UxPutHeight( text8, 32 );
	UxPutWidth( text8, 50 );
	UxPutY( text8, 7 );
	UxPutX( text8, 5 );

	UxPutSelectColor( toggleButton25, "Palegreen" );
	UxPutIndicatorSize( toggleButton25, 16 );
	UxPutIndicatorOn( toggleButton25, "true" );
	UxPutSet( toggleButton25, "true" );
	UxPutBorderWidth( toggleButton25, 1 );
	UxPutRecomputeSize( toggleButton25, "false" );
	UxPutForeground( toggleButton25, TextForeground );
	UxPutFontList( toggleButton25, BoldTextFont );
	UxPutBackground( toggleButton25, TextBackground );
	UxPutIndicatorType( toggleButton25, "one_of_many" );
	UxPutLabelString( toggleButton25, "Yes" );
	UxPutHeight( toggleButton25, 28 );
	UxPutWidth( toggleButton25, 70 );
	UxPutY( toggleButton25, 0 );
	UxPutX( toggleButton25, 0 );

	UxPutBackground( rowColumn6, ApplicBackground );
	UxPutNumColumns( rowColumn6, 2 );
	UxPutMarginWidth( rowColumn6, 0 );
	UxPutMarginHeight( rowColumn6, 0 );
	UxPutResizeWidth( rowColumn6, "false" );
	UxPutResizeHeight( rowColumn6, "false" );
	UxPutSpacing( rowColumn6, 0 );
	UxPutPacking( rowColumn6, "pack_none" );
	UxPutOrientation( rowColumn6, "horizontal" );
	UxPutHeight( rowColumn6, 35 );
	UxPutWidth( rowColumn6, 150 );
	UxPutY( rowColumn6, 110 );
	UxPutX( rowColumn6, 312 );

	UxPutEntryAlignment( rowColumn7, "alignment_center" );
	UxPutAdjustLast( rowColumn7, "true" );
	UxPutAdjustMargin( rowColumn7, "true" );
	UxPutSpacing( rowColumn7, 0 );
	UxPutPacking( rowColumn7, "pack_none" );
	UxPutMarginWidth( rowColumn7, 0 );
	UxPutMarginHeight( rowColumn7, 0 );
	UxPutBackground( rowColumn7, ApplicBackground );
	UxPutResizeWidth( rowColumn7, "false" );
	UxPutResizeHeight( rowColumn7, "false" );
	UxPutHeight( rowColumn7, 32 );
	UxPutWidth( rowColumn7, 23 );
	UxPutY( rowColumn7, 0 );
	UxPutX( rowColumn7, 100 );

	UxPutHighlightThickness( arrowButton13, 0 );
	UxPutForeground( arrowButton13, "PaleGreen" );
	UxPutTranslations( arrowButton13, Arrows6 );
	UxPutShadowThickness( arrowButton13, 0 );
	UxPutBackground( arrowButton13, TextBackground );
	UxPutHeight( arrowButton13, 15 );
	UxPutWidth( arrowButton13, 23 );
	UxPutY( arrowButton13, 0 );
	UxPutX( arrowButton13, 0 );

	UxPutHighlightThickness( arrowButton14, 0 );
	UxPutBorderColor( arrowButton14, ApplicBackground );
	UxPutForeground( arrowButton14, "PaleGreen" );
	UxPutTranslations( arrowButton14, Arrows6 );
	UxPutArrowDirection( arrowButton14, "arrow_down" );
	UxPutShadowThickness( arrowButton14, 0 );
	UxPutBackground( arrowButton14, TextBackground );
	UxPutHeight( arrowButton14, 15 );
	UxPutWidth( arrowButton14, 23 );
	UxPutY( arrowButton14, 16 );
	UxPutX( arrowButton14, 0 );

	UxPutColumns( text13, 10 );
	UxPutMaxLength( text13, 100 );
	UxPutMarginWidth( text13, 1 );
	UxPutMarginHeight( text13, 2 );
	UxPutShadowThickness( text13, 2 );
	UxPutHighlightThickness( text13, 2 );
	UxPutForeground( text13, TextForeground );
	UxPutFontList( text13, TextFont );
	UxPutBackground( text13, TextBackground );
	UxPutHeight( text13, 32 );
	UxPutWidth( text13, 90 );
	UxPutY( text13, 2 );
	UxPutX( text13, 5 );

	UxPutFontList( label54, BoldTextFont );
	UxPutTranslations( label54, "" );
	UxPutRecomputeSize( label54, "false" );
	UxPutAlignment( label54, "alignment_center" );
	UxPutBackground( label54, LabelBackground );
	UxPutForeground( label54, TextForeground );
	UxPutLabelString( label54, "Type" );
	UxPutHeight( label54, 25 );
	UxPutWidth( label54, 80 );
	UxPutY( label54, 180 );
	UxPutX( label54, 20 );

	UxPutMarginLeft( toggleButton15, 2 );
	UxPutSelectColor( toggleButton15, "PaleGreen" );
	UxPutIndicatorSize( toggleButton15, 16 );
	UxPutIndicatorOn( toggleButton15, "true" );
	UxPutSet( toggleButton15, "true" );
	UxPutBorderWidth( toggleButton15, 1 );
	UxPutRecomputeSize( toggleButton15, "false" );
	UxPutForeground( toggleButton15, TextForeground );
	UxPutFontList( toggleButton15, BoldTextFont );
	UxPutBackground( toggleButton15, TextBackground );
	UxPutIndicatorType( toggleButton15, "one_of_many" );
	UxPutLabelString( toggleButton15, "Display" );
	UxPutHeight( toggleButton15, 28 );
	UxPutWidth( toggleButton15, 85 );
	UxPutY( toggleButton15, 0 );
	UxPutX( toggleButton15, 0 );

	UxPutMappedWhenManaged( form7, "false" );
	UxPutBorderWidth( form7, 1 );
	UxPutBackground( form7, ApplicBackground );
	UxPutHeight( form7, 240 );
	UxPutWidth( form7, 430 );
	UxPutY( form7, 10 );
	UxPutX( form7, 10 );
	UxPutResizePolicy( form7, "resize_none" );

	UxPutFontList( label3, BoldTextFont );
	UxPutTranslations( label3, "" );
	UxPutRecomputeSize( label3, "false" );
	UxPutAlignment( label3, "alignment_center" );
	UxPutBackground( label3, LabelBackground );
	UxPutForeground( label3, TextForeground );
	UxPutLabelString( label3, "Out_specs" );
	UxPutHeight( label3, 25 );
	UxPutWidth( label3, 100 );
	UxPutY( label3, 10 );
	UxPutX( label3, 10 );

	UxPutMarginWidth( text3, 2 );
	UxPutMarginHeight( text3, 2 );
	UxPutColumns( text3, 20 );
	UxPutFontList( text3, TextFont );
	UxPutForeground( text3, TextForeground );
	UxPutBackground( text3, TextBackground );
	UxPutHeight( text3, 30 );
	UxPutWidth( text3, 379 );
	UxPutY( text3, 39 );
	UxPutX( text3, 10 );

	UxPutFontList( label7, BoldTextFont );
	UxPutTranslations( label7, "" );
	UxPutRecomputeSize( label7, "false" );
	UxPutAlignment( label7, "alignment_center" );
	UxPutBackground( label7, LabelBackground );
	UxPutForeground( label7, TextForeground );
	UxPutLabelString( label7, "Mark Position" );
	UxPutHeight( label7, 25 );
	UxPutWidth( label7, 100 );
	UxPutY( label7, 89 );
	UxPutX( label7, 9 );

	UxPutFontList( label8, BoldTextFont );
	UxPutTranslations( label8, "" );
	UxPutRecomputeSize( label8, "false" );
	UxPutAlignment( label8, "alignment_center" );
	UxPutBackground( label8, LabelBackground );
	UxPutForeground( label8, TextForeground );
	UxPutLabelString( label8, "No. of Curs" );
	UxPutHeight( label8, 25 );
	UxPutWidth( label8, 100 );
	UxPutY( label8, 129 );
	UxPutX( label8, 9 );

	UxPutFontList( label4, BoldTextFont );
	UxPutTranslations( label4, "" );
	UxPutRecomputeSize( label4, "false" );
	UxPutAlignment( label4, "alignment_center" );
	UxPutBackground( label4, LabelBackground );
	UxPutForeground( label4, TextForeground );
	UxPutLabelString( label4, "ZoomWindow" );
	UxPutHeight( label4, 25 );
	UxPutWidth( label4, 100 );
	UxPutY( label4, 169 );
	UxPutX( label4, 9 );

	UxPutMarginRight( toggleButton8, 2 );
	UxPutMarginLeft( toggleButton8, 10 );
	UxPutTopShadowColor( toggleButton8, LabelBackground );
	UxPutSelectColor( toggleButton8, "Palegreen" );
	UxPutIndicatorSize( toggleButton8, 16 );
	UxPutIndicatorOn( toggleButton8, "true" );
	UxPutSet( toggleButton8, "true" );
	UxPutBorderWidth( toggleButton8, 1 );
	UxPutRecomputeSize( toggleButton8, "false" );
	UxPutForeground( toggleButton8, TextForeground );
	UxPutFontList( toggleButton8, BoldTextFont );
	UxPutBackground( toggleButton8, TextBackground );
	UxPutIndicatorType( toggleButton8, "one_of_many" );
	UxPutLabelString( toggleButton8, "No" );
	UxPutHeight( toggleButton8, 28 );
	UxPutWidth( toggleButton8, 70 );
	UxPutY( toggleButton8, 10 );
	UxPutX( toggleButton8, 10 );

	UxPutTopShadowColor( toggleButton9, LabelBackground );
	UxPutSelectColor( toggleButton9, "Palegreen" );
	UxPutIndicatorSize( toggleButton9, 16 );
	UxPutIndicatorOn( toggleButton9, "true" );
	UxPutSet( toggleButton9, "true" );
	UxPutBorderWidth( toggleButton9, 1 );
	UxPutRecomputeSize( toggleButton9, "false" );
	UxPutForeground( toggleButton9, TextForeground );
	UxPutFontList( toggleButton9, BoldTextFont );
	UxPutBackground( toggleButton9, TextBackground );
	UxPutIndicatorType( toggleButton9, "one_of_many" );
	UxPutLabelString( toggleButton9, "1" );
	UxPutHeight( toggleButton9, 28 );
	UxPutWidth( toggleButton9, 40 );
	UxPutY( toggleButton9, 20 );
	UxPutX( toggleButton9, 20 );

	UxPutTopShadowColor( toggleButton14, LabelBackground );
	UxPutSelectColor( toggleButton14, "Palegreen" );
	UxPutIndicatorSize( toggleButton14, 16 );
	UxPutIndicatorOn( toggleButton14, "true" );
	UxPutSet( toggleButton14, "true" );
	UxPutBorderWidth( toggleButton14, 1 );
	UxPutRecomputeSize( toggleButton14, "false" );
	UxPutForeground( toggleButton14, TextForeground );
	UxPutFontList( toggleButton14, BoldTextFont );
	UxPutBackground( toggleButton14, TextBackground );
	UxPutIndicatorType( toggleButton14, "one_of_many" );
	UxPutLabelString( toggleButton14, "Yes" );
	UxPutHeight( toggleButton14, 28 );
	UxPutWidth( toggleButton14, 70 );
	UxPutY( toggleButton14, 30 );
	UxPutX( toggleButton14, 30 );

	UxPutRecomputeSize( pushButton6, "false" );
	UxPutForeground( pushButton6, ButtonForeground );
	UxPutFontList( pushButton6, BoldTextFont );
	UxPutTranslations( pushButton6, MapUnmap );
	UxPutLabelString( pushButton6, "Get Cursor" );
	UxPutBackground( pushButton6, ButtonBackground );
	UxPutHeight( pushButton6, 30 );
	UxPutWidth( pushButton6, 90 );
	UxPutY( pushButton6, 370 );
	UxPutX( pushButton6, 10 );

	UxPutMappedWhenManaged( form1, "false" );
	UxPutBorderWidth( form1, 1 );
	UxPutBackground( form1, ApplicBackground );
	UxPutHeight( form1, 240 );
	UxPutWidth( form1, 430 );
	UxPutY( form1, 10 );
	UxPutX( form1, 10 );
	UxPutResizePolicy( form1, "resize_none" );

	UxPutFontList( label6, BoldTextFont );
	UxPutTranslations( label6, "" );
	UxPutRecomputeSize( label6, "false" );
	UxPutAlignment( label6, "alignment_center" );
	UxPutBackground( label6, LabelBackground );
	UxPutForeground( label6, TextForeground );
	UxPutLabelString( label6, "Out_Frame" );
	UxPutHeight( label6, 25 );
	UxPutWidth( label6, 89 );
	UxPutY( label6, 10 );
	UxPutX( label6, 10 );

	UxPutMarginWidth( text7, 2 );
	UxPutMarginHeight( text7, 2 );
	UxPutFontList( text7, TextFont );
	UxPutForeground( text7, TextForeground );
	UxPutBackground( text7, TextBackground );
	UxPutHeight( text7, 30 );
	UxPutWidth( text7, 360 );
	UxPutY( text7, 39 );
	UxPutX( text7, 10 );

	UxPutFontList( label31, BoldTextFont );
	UxPutTranslations( label31, "" );
	UxPutRecomputeSize( label31, "false" );
	UxPutAlignment( label31, "alignment_center" );
	UxPutBackground( label31, LabelBackground );
	UxPutForeground( label31, TextForeground );
	UxPutLabelString( label31, "Plot_Flag" );
	UxPutHeight( label31, 25 );
	UxPutWidth( label31, 89 );
	UxPutY( label31, 79 );
	UxPutX( label31, 10 );

	UxPutY( plotflag_p1, 0 );
	UxPutX( plotflag_p1, 0 );
	UxPutLabelString( plotflag_p1, "" );
	UxPutForeground( plotflag_p1, TextForeground );
	UxPutBackground( plotflag_p1, TextBackground );
	UxPutRowColumnType( plotflag_p1, "menu_pulldown" );

	UxPutY( plotflag_p1_b1, 0 );
	UxPutX( plotflag_p1_b1, 0 );
	UxPutRecomputeSize( plotflag_p1_b1, "true" );
	UxPutFontList( plotflag_p1_b1, TextFont );
	UxPutLabelString( plotflag_p1_b1, "Draw" );

	UxPutY( plotflag_p1_b2, 0 );
	UxPutX( plotflag_p1_b2, 0 );
	UxPutRecomputeSize( plotflag_p1_b2, "true" );
	UxPutFontList( plotflag_p1_b2, TextFont );
	UxPutLabelString( plotflag_p1_b2, "Plot" );

	UxPutFontList( plotflag_p1_b3, TextFont );
	UxPutLabelString( plotflag_p1_b3, "None" );

	UxPutLabelString( plotflag, "" );
	UxPutBorderWidth( plotflag, 1 );
	UxPutTranslations( plotflag, "" );
	UxPutShadowThickness( plotflag, 1 );
	UxPutWidth( plotflag, 102 );
	UxPutResizeWidth( plotflag, "true" );
	UxPutResizeHeight( plotflag, "true" );
	UxPutForeground( plotflag, TextForeground );
	UxPutBackground( plotflag, TextBackground );
	UxPutY( plotflag, 79 );
	UxPutX( plotflag, 119 );
	UxPutRowColumnType( plotflag, "menu_option" );

	UxPutFontList( label29, BoldTextFont );
	UxPutTranslations( label29, "" );
	UxPutRecomputeSize( label29, "false" );
	UxPutAlignment( label29, "alignment_center" );
	UxPutBackground( label29, LabelBackground );
	UxPutForeground( label29, TextForeground );
	UxPutLabelString( label29, "Cut_Option" );
	UxPutHeight( label29, 25 );
	UxPutWidth( label29, 89 );
	UxPutY( label29, 119 );
	UxPutX( label29, 10 );

	UxPutFontList( label30, BoldTextFont );
	UxPutTranslations( label30, "" );
	UxPutRecomputeSize( label30, "false" );
	UxPutAlignment( label30, "alignment_center" );
	UxPutBackground( label30, LabelBackground );
	UxPutForeground( label30, TextForeground );
	UxPutLabelString( label30, "Stepsize" );
	UxPutHeight( label30, 25 );
	UxPutWidth( label30, 89 );
	UxPutY( label30, 159 );
	UxPutX( label30, 10 );

	UxPutTopCharacter( text14, 0 );
	UxPutMarginWidth( text14, 2 );
	UxPutMarginHeight( text14, 2 );
	UxPutFontList( text14, TextFont );
	UxPutForeground( text14, TextForeground );
	UxPutBackground( text14, TextBackground );
	UxPutHeight( text14, 30 );
	UxPutWidth( text14, 140 );
	UxPutY( text14, 159 );
	UxPutX( text14, 119 );

	UxPutMarginLeft( toggleButton6, 10 );
	UxPutMarginRight( toggleButton6, 2 );
	UxPutSelectColor( toggleButton6, "Palegreen" );
	UxPutIndicatorSize( toggleButton6, 16 );
	UxPutIndicatorOn( toggleButton6, "true" );
	UxPutSet( toggleButton6, "true" );
	UxPutBorderWidth( toggleButton6, 1 );
	UxPutRecomputeSize( toggleButton6, "false" );
	UxPutForeground( toggleButton6, TextForeground );
	UxPutFontList( toggleButton6, BoldTextFont );
	UxPutBackground( toggleButton6, TextBackground );
	UxPutIndicatorType( toggleButton6, "one_of_many" );
	UxPutLabelString( toggleButton6, "NoCut" );
	UxPutHeight( toggleButton6, 28 );
	UxPutWidth( toggleButton6, 78 );
	UxPutY( toggleButton6, 0 );
	UxPutX( toggleButton6, 0 );

	UxPutLabelFontList( form8, TextFont );
	UxPutButtonFontList( form8, TextFont );
	UxPutTextFontList( form8, TextFont );
	UxPutMappedWhenManaged( form8, "false" );
	UxPutBorderWidth( form8, 1 );
	UxPutBackground( form8, ApplicBackground );
	UxPutHeight( form8, 240 );
	UxPutWidth( form8, 430 );
	UxPutY( form8, 10 );
	UxPutX( form8, 10 );
	UxPutResizePolicy( form8, "resize_none" );

	UxPutFontList( label39, BoldTextFont );
	UxPutTranslations( label39, "" );
	UxPutRecomputeSize( label39, "false" );
	UxPutAlignment( label39, "alignment_center" );
	UxPutBackground( label39, LabelBackground );
	UxPutForeground( label39, TextForeground );
	UxPutLabelString( label39, "LUT Method" );
	UxPutHeight( label39, 25 );
	UxPutWidth( label39, 100 );
	UxPutY( label39, 169 );
	UxPutX( label39, 9 );

	UxPutFontList( label40, BoldTextFont );
	UxPutTranslations( label40, "" );
	UxPutRecomputeSize( label40, "false" );
	UxPutAlignment( label40, "alignment_center" );
	UxPutBackground( label40, LabelBackground );
	UxPutForeground( label40, TextForeground );
	UxPutLabelString( label40, "LUT Colour" );
	UxPutHeight( label40, 25 );
	UxPutWidth( label40, 100 );
	UxPutY( label40, 209 );
	UxPutX( label40, 9 );

	UxPutY( method_p3, 0 );
	UxPutX( method_p3, 0 );
	UxPutLabelString( method_p3, "" );
	UxPutForeground( method_p3, TextForeground );
	UxPutBackground( method_p3, TextBackground );
	UxPutRowColumnType( method_p3, "menu_pulldown" );

	UxPutY( method_p1_b9, 0 );
	UxPutX( method_p1_b9, 0 );
	UxPutRecomputeSize( method_p1_b9, "true" );
	UxPutFontList( method_p1_b9, TextFont );
	UxPutLabelString( method_p1_b9, "Band" );

	UxPutY( method_p1_b10, 0 );
	UxPutX( method_p1_b10, 0 );
	UxPutRecomputeSize( method_p1_b10, "true" );
	UxPutFontList( method_p1_b10, TextFont );
	UxPutLabelString( method_p1_b10, "Rotate" );

	UxPutY( method_p1_b11, 0 );
	UxPutX( method_p1_b11, 0 );
	UxPutRecomputeSize( method_p1_b11, "true" );
	UxPutFontList( method_p1_b11, TextFont );
	UxPutLabelString( method_p1_b11, "Squeeze" );

	UxPutY( method_p1_b12, 0 );
	UxPutX( method_p1_b12, 0 );
	UxPutRecomputeSize( method_p1_b12, "true" );
	UxPutFontList( method_p1_b12, TextFont );
	UxPutLabelString( method_p1_b12, "Graph" );

	UxPutLabelString( method2, "" );
	UxPutBorderWidth( method2, 1 );
	UxPutTranslations( method2, "" );
	UxPutWidth( method2, 102 );
	UxPutResizeWidth( method2, "true" );
	UxPutResizeHeight( method2, "true" );
	UxPutForeground( method2, TextForeground );
	UxPutBackground( method2, TextBackground );
	UxPutY( method2, 169 );
	UxPutX( method2, 149 );
	UxPutRowColumnType( method2, "menu_option" );

	UxPutY( color_p2, 0 );
	UxPutX( color_p2, 0 );
	UxPutResizeWidth( color_p2, "true" );
	UxPutResizeHeight( color_p2, "true" );
	UxPutWidth( color_p2, 74 );
	UxPutForeground( color_p2, TextForeground );
	UxPutBackground( color_p2, TextBackground );
	UxPutRowColumnType( color_p2, "menu_pulldown" );

	UxPutY( color_p1_b12, 0 );
	UxPutX( color_p1_b12, 0 );
	UxPutRecomputeSize( color_p1_b12, "true" );
	UxPutSensitive( color_p1_b12, "true" );
	UxPutFontList( color_p1_b12, TextFont );
	UxPutAccelerator( color_p1_b12, "" );
	UxPutLabelString( color_p1_b12, "Red" );

	UxPutY( color_p1_b13, 0 );
	UxPutX( color_p1_b13, 0 );
	UxPutRecomputeSize( color_p1_b13, "true" );
	UxPutFontList( color_p1_b13, TextFont );
	UxPutLabelString( color_p1_b13, "Green" );

	UxPutY( color_p1_b14, 0 );
	UxPutX( color_p1_b14, 0 );
	UxPutRecomputeSize( color_p1_b14, "true" );
	UxPutFontList( color_p1_b14, TextFont );
	UxPutLabelString( color_p1_b14, "Blue" );

	UxPutY( color_p1_b15, 0 );
	UxPutX( color_p1_b15, 0 );
	UxPutRecomputeSize( color_p1_b15, "true" );
	UxPutFontList( color_p1_b15, TextFont );
	UxPutLabelString( color_p1_b15, "All" );

	UxPutY( color_p1_b16, 0 );
	UxPutX( color_p1_b16, 0 );
	UxPutRecomputeSize( color_p1_b16, "true" );
	UxPutFontList( color_p1_b16, TextFont );
	UxPutLabelString( color_p1_b16, "White" );

	UxPutY( color_p1_b17, 0 );
	UxPutX( color_p1_b17, 0 );
	UxPutRecomputeSize( color_p1_b17, "true" );
	UxPutFontList( color_p1_b17, TextFont );
	UxPutLabelString( color_p1_b17, "Dark" );

	UxPutY( color_p1_b18, 0 );
	UxPutX( color_p1_b18, 0 );
	UxPutRecomputeSize( color_p1_b18, "true" );
	UxPutFontList( color_p1_b18, TextFont );
	UxPutLabelString( color_p1_b18, "Yellow" );

	UxPutY( color_p1_b19, 0 );
	UxPutX( color_p1_b19, 0 );
	UxPutRecomputeSize( color_p1_b19, "true" );
	UxPutFontList( color_p1_b19, TextFont );
	UxPutLabelString( color_p1_b19, "Pink" );

	UxPutY( color_p1_b20, 0 );
	UxPutX( color_p1_b20, 0 );
	UxPutRecomputeSize( color_p1_b20, "true" );
	UxPutFontList( color_p1_b20, TextFont );
	UxPutLabelString( color_p1_b20, "Brown" );

	UxPutY( color_p1_b21, 0 );
	UxPutX( color_p1_b21, 0 );
	UxPutRecomputeSize( color_p1_b21, "true" );
	UxPutFontList( color_p1_b21, TextFont );
	UxPutLabelString( color_p1_b21, "Orange" );

	UxPutY( color_p1_b22, 0 );
	UxPutX( color_p1_b22, 0 );
	UxPutRecomputeSize( color_p1_b22, "true" );
	UxPutFontList( color_p1_b22, TextFont );
	UxPutLabelString( color_p1_b22, "Violet" );

	UxPutLabelString( color1, "" );
	UxPutBorderWidth( color1, 1 );
	UxPutTranslations( color1, "" );
	UxPutTopShadowColor( color1, LabelBackground );
	UxPutWidth( color1, 102 );
	UxPutResizeWidth( color1, "true" );
	UxPutResizeHeight( color1, "true" );
	UxPutForeground( color1, TextForeground );
	UxPutBackground( color1, TextBackground );
	UxPutY( color1, 209 );
	UxPutX( color1, 149 );
	UxPutRowColumnType( color1, "menu_option" );

	UxPutForeground( separator1, TextForeground );
	UxPutBackground( separator1, ApplicBackground );
	UxPutHeight( separator1, 10 );
	UxPutWidth( separator1, 430 );
	UxPutY( separator1, 119 );
	UxPutX( separator1, -1 );

	UxPutFontList( label23, BoldTextFont );
	UxPutTranslations( label23, "" );
	UxPutRecomputeSize( label23, "false" );
	UxPutAlignment( label23, "alignment_center" );
	UxPutBackground( label23, LabelBackground );
	UxPutForeground( label23, TextForeground );
	UxPutLabelString( label23, "ITT Method" );
	UxPutHeight( label23, 25 );
	UxPutWidth( label23, 100 );
	UxPutY( label23, 169 );
	UxPutX( label23, 9 );

	UxPutY( method_p1, 0 );
	UxPutX( method_p1, 0 );
	UxPutLabelString( method_p1, "" );
	UxPutForeground( method_p1, TextForeground );
	UxPutBackground( method_p1, TextBackground );
	UxPutRowColumnType( method_p1, "menu_pulldown" );

	UxPutY( method_p1_b1, 0 );
	UxPutX( method_p1_b1, 0 );
	UxPutRecomputeSize( method_p1_b1, "true" );
	UxPutFontList( method_p1_b1, TextFont );
	UxPutLabelString( method_p1_b1, "Band" );

	UxPutY( method_p1_b2, 0 );
	UxPutX( method_p1_b2, 0 );
	UxPutRecomputeSize( method_p1_b2, "true" );
	UxPutFontList( method_p1_b2, TextFont );
	UxPutLabelString( method_p1_b2, "Rotate" );

	UxPutY( method_p1_b3, 0 );
	UxPutX( method_p1_b3, 0 );
	UxPutRecomputeSize( method_p1_b3, "true" );
	UxPutFontList( method_p1_b3, TextFont );
	UxPutLabelString( method_p1_b3, "Squeeze" );

	UxPutY( method_p1_b4, 0 );
	UxPutX( method_p1_b4, 0 );
	UxPutRecomputeSize( method_p1_b4, "true" );
	UxPutFontList( method_p1_b4, TextFont );
	UxPutLabelString( method_p1_b4, "Contrast" );

	UxPutLabelString( method1, "" );
	UxPutBorderWidth( method1, 1 );
	UxPutTranslations( method1, "" );
	UxPutWidth( method1, 102 );
	UxPutResizeWidth( method1, "true" );
	UxPutResizeHeight( method1, "true" );
	UxPutForeground( method1, TextForeground );
	UxPutBackground( method1, TextBackground );
	UxPutY( method1, 169 );
	UxPutX( method1, 149 );
	UxPutRowColumnType( method1, "menu_option" );

	UxPutFontList( label33, BoldTextFont );
	UxPutTranslations( label33, "" );
	UxPutRecomputeSize( label33, "false" );
	UxPutAlignment( label33, "alignment_center" );
	UxPutBackground( label33, LabelBackground );
	UxPutForeground( label33, TextForeground );
	UxPutLabelString( label33, "ITT Value" );
	UxPutHeight( label33, 25 );
	UxPutWidth( label33, 100 );
	UxPutY( label33, 169 );
	UxPutX( label33, 9 );

	UxPutLabelString( rowColumn19, "" );
	UxPutBackground( rowColumn19, ApplicBackground );
	UxPutNumColumns( rowColumn19, 2 );
	UxPutMarginWidth( rowColumn19, 0 );
	UxPutMarginHeight( rowColumn19, 0 );
	UxPutResizeWidth( rowColumn19, "false" );
	UxPutResizeHeight( rowColumn19, "false" );
	UxPutSpacing( rowColumn19, 0 );
	UxPutPacking( rowColumn19, "pack_none" );
	UxPutOrientation( rowColumn19, "horizontal" );
	UxPutHeight( rowColumn19, 44 );
	UxPutWidth( rowColumn19, 100 );
	UxPutY( rowColumn19, 15 );
	UxPutX( rowColumn19, 114 );

	UxPutEntryAlignment( rowColumn21, "alignment_center" );
	UxPutAdjustLast( rowColumn21, "true" );
	UxPutAdjustMargin( rowColumn21, "true" );
	UxPutSpacing( rowColumn21, 0 );
	UxPutPacking( rowColumn21, "pack_none" );
	UxPutMarginWidth( rowColumn21, 0 );
	UxPutMarginHeight( rowColumn21, 0 );
	UxPutBackground( rowColumn21, ApplicBackground );
	UxPutResizeWidth( rowColumn21, "false" );
	UxPutResizeHeight( rowColumn21, "false" );
	UxPutHeight( rowColumn21, 32 );
	UxPutWidth( rowColumn21, 23 );
	UxPutY( rowColumn21, 6 );
	UxPutX( rowColumn21, 65 );

	UxPutHighlightThickness( arrowButton1, 0 );
	UxPutForeground( arrowButton1, "PaleGreen" );
	UxPutTranslations( arrowButton1, Arrows8 );
	UxPutShadowThickness( arrowButton1, 0 );
	UxPutBackground( arrowButton1, TextBackground );
	UxPutHeight( arrowButton1, 15 );
	UxPutWidth( arrowButton1, 23 );
	UxPutY( arrowButton1, 0 );
	UxPutX( arrowButton1, 0 );

	UxPutHighlightThickness( arrowButton2, 0 );
	UxPutBorderColor( arrowButton2, ApplicBackground );
	UxPutForeground( arrowButton2, "PaleGreen" );
	UxPutTranslations( arrowButton2, Arrows8 );
	UxPutArrowDirection( arrowButton2, "arrow_down" );
	UxPutShadowThickness( arrowButton2, 0 );
	UxPutBackground( arrowButton2, TextBackground );
	UxPutHeight( arrowButton2, 15 );
	UxPutWidth( arrowButton2, 23 );
	UxPutY( arrowButton2, 16 );
	UxPutX( arrowButton2, 0 );

	UxPutMaxLength( text27, 100 );
	UxPutMarginWidth( text27, 2 );
	UxPutMarginHeight( text27, 2 );
	UxPutShadowThickness( text27, 2 );
	UxPutHighlightThickness( text27, 2 );
	UxPutForeground( text27, TextForeground );
	UxPutFontList( text27, TextFont );
	UxPutBackground( text27, TextBackground );
	UxPutHeight( text27, 32 );
	UxPutWidth( text27, 50 );
	UxPutY( text27, 7 );
	UxPutX( text27, 5 );

	UxPutSensitive( toggleButton3, "true" );
	UxPutMarginLeft( toggleButton3, 0 );
	UxPutSelectColor( toggleButton3, "PaleGreen" );
	UxPutIndicatorSize( toggleButton3, 16 );
	UxPutIndicatorOn( toggleButton3, "true" );
	UxPutSet( toggleButton3, "true" );
	UxPutBorderWidth( toggleButton3, 1 );
	UxPutRecomputeSize( toggleButton3, "false" );
	UxPutForeground( toggleButton3, TextForeground );
	UxPutFontList( toggleButton3, BoldTextFont );
	UxPutBackground( toggleButton3, ApplicBackground );
	UxPutIndicatorType( toggleButton3, "one_of_many" );
	UxPutLabelString( toggleButton3, "" );
	UxPutHeight( toggleButton3, 28 );
	UxPutWidth( toggleButton3, 25 );
	UxPutY( toggleButton3, 99 );
	UxPutX( toggleButton3, 129 );

	UxPutSensitive( toggleButton5, "true" );
	UxPutMarginLeft( toggleButton5, 0 );
	UxPutSelectColor( toggleButton5, "PaleGreen" );
	UxPutIndicatorSize( toggleButton5, 16 );
	UxPutIndicatorOn( toggleButton5, "true" );
	UxPutSet( toggleButton5, "false" );
	UxPutBorderWidth( toggleButton5, 1 );
	UxPutRecomputeSize( toggleButton5, "false" );
	UxPutForeground( toggleButton5, TextForeground );
	UxPutFontList( toggleButton5, BoldTextFont );
	UxPutBackground( toggleButton5, ApplicBackground );
	UxPutIndicatorType( toggleButton5, "one_of_many" );
	UxPutLabelString( toggleButton5, "" );
	UxPutHeight( toggleButton5, 28 );
	UxPutWidth( toggleButton5, 25 );
	UxPutY( toggleButton5, 99 );
	UxPutX( toggleButton5, 129 );

	UxPutRecomputeSize( pushButton2, "false" );
	UxPutForeground( pushButton2, ButtonForeground );
	UxPutFontList( pushButton2, BoldTextFont );
	UxPutTranslations( pushButton2, MapUnmap );
	UxPutLabelString( pushButton2, "Load LUT" );
	UxPutBackground( pushButton2, ButtonBackground );
	UxPutHeight( pushButton2, 30 );
	UxPutWidth( pushButton2, 90 );
	UxPutY( pushButton2, 370 );
	UxPutX( pushButton2, 10 );

	UxPutRecomputeSize( pushButton9, "false" );
	UxPutForeground( pushButton9, ButtonForeground );
	UxPutFontList( pushButton9, BoldTextFont );
	UxPutTranslations( pushButton9, MapUnmap );
	UxPutLabelString( pushButton9, "Extract Line" );
	UxPutBackground( pushButton9, ButtonBackground );
	UxPutHeight( pushButton9, 30 );
	UxPutWidth( pushButton9, 90 );
	UxPutY( pushButton9, 370 );
	UxPutX( pushButton9, 100 );

	UxPutRecomputeSize( pushButton4, "false" );
	UxPutForeground( pushButton4, ButtonForeground );
	UxPutFontList( pushButton4, BoldTextFont );
	UxPutTranslations( pushButton4, MapUnmap );
	UxPutLabelString( pushButton4, "Modify LUT" );
	UxPutBackground( pushButton4, ButtonBackground );
	UxPutHeight( pushButton4, 30 );
	UxPutWidth( pushButton4, 90 );
	UxPutY( pushButton4, 460 );
	UxPutX( pushButton4, 390 );

	UxPutRecomputeSize( pushButton7, "false" );
	UxPutForeground( pushButton7, ButtonForeground );
	UxPutFontList( pushButton7, BoldTextFont );
	UxPutTranslations( pushButton7, MapUnmap );
	UxPutLabelString( pushButton7, "Load Image" );
	UxPutBackground( pushButton7, ButtonBackground );
	UxPutHeight( pushButton7, 30 );
	UxPutWidth( pushButton7, 90 );
	UxPutY( pushButton7, 450 );
	UxPutX( pushButton7, 140 );

	UxPutRecomputeSize( pushButton3, "false" );
	UxPutForeground( pushButton3, ButtonForeground );
	UxPutFontList( pushButton3, BoldTextFont );
	UxPutTranslations( pushButton3, MapUnmap );
	UxPutLabelString( pushButton3, "Explore Ima" );
	UxPutBackground( pushButton3, ButtonBackground );
	UxPutHeight( pushButton3, 30 );
	UxPutWidth( pushButton3, 90 );
	UxPutY( pushButton3, 500 );
	UxPutX( pushButton3, 110 );

	UxPutRecomputeSize( pushButton8, "false" );
	UxPutForeground( pushButton8, ButtonForeground );
	UxPutFontList( pushButton8, BoldTextFont );
	UxPutTranslations( pushButton8, MapUnmap );
	UxPutLabelString( pushButton8, "Create  D/G" );
	UxPutBackground( pushButton8, ButtonBackground );
	UxPutHeight( pushButton8, 30 );
	UxPutWidth( pushButton8, 90 );
	UxPutY( pushButton8, 500 );
	UxPutX( pushButton8, 220 );

	UxPutRecomputeSize( pushButton5, "false" );
	UxPutForeground( pushButton5, ButtonForeground );
	UxPutFontList( pushButton5, BoldTextFont );
	UxPutTranslations( pushButton5, MapUnmap );
	UxPutLabelString( pushButton5, "Quick Load" );
	UxPutBackground( pushButton5, ButtonBackground );
	UxPutHeight( pushButton5, 30 );
	UxPutWidth( pushButton5, 90 );
	UxPutY( pushButton5, 380 );
	UxPutX( pushButton5, 210 );

	UxPutNoResize( form5, "true" );
	UxPutMappedWhenManaged( form5, "false" );
	UxPutBorderWidth( form5, 1 );
	UxPutBackground( form5, ApplicBackground );
	UxPutHeight( form5, 240 );
	UxPutWidth( form5, 430 );
	UxPutY( form5, 14 );
	UxPutX( form5, 14 );
	UxPutResizePolicy( form5, "resize_none" );

	UxPutFontList( label37, BoldTextFont );
	UxPutTranslations( label37, "" );
	UxPutRecomputeSize( label37, "false" );
	UxPutAlignment( label37, "alignment_center" );
	UxPutBackground( label37, LabelBackground );
	UxPutForeground( label37, TextForeground );
	UxPutLabelString( label37, "Frame" );
	UxPutHeight( label37, 25 );
	UxPutWidth( label37, 60 );
	UxPutY( label37, 10 );
	UxPutX( label37, 10 );

	UxPutMarginWidth( text28, 2 );
	UxPutMarginHeight( text28, 2 );
	UxPutColumns( text28, 30 );
	UxPutFontList( text28, TextFont );
	UxPutForeground( text28, TextForeground );
	UxPutTranslations( text28, SelectFile );
	UxPutBackground( text28, TextBackground );
	UxPutHeight( text28, 32 );
	UxPutWidth( text28, 250 );
	UxPutY( text28, 39 );
	UxPutX( text28, 10 );

	UxPutFontList( label41, BoldTextFont );
	UxPutTranslations( label41, "" );
	UxPutRecomputeSize( label41, "false" );
	UxPutAlignment( label41, "alignment_center" );
	UxPutBackground( label41, LabelBackground );
	UxPutForeground( label41, TextForeground );
	UxPutLabelString( label41, "ScaleX" );
	UxPutHeight( label41, 25 );
	UxPutWidth( label41, 60 );
	UxPutY( label41, 119 );
	UxPutX( label41, 10 );

	UxPutBackground( rowColumn35, ApplicBackground );
	UxPutNumColumns( rowColumn35, 2 );
	UxPutMarginWidth( rowColumn35, 0 );
	UxPutMarginHeight( rowColumn35, 0 );
	UxPutResizeWidth( rowColumn35, "false" );
	UxPutResizeHeight( rowColumn35, "false" );
	UxPutSpacing( rowColumn35, 0 );
	UxPutPacking( rowColumn35, "pack_none" );
	UxPutOrientation( rowColumn35, "horizontal" );
	UxPutHeight( rowColumn35, 40 );
	UxPutWidth( rowColumn35, 70 );
	UxPutY( rowColumn35, 55 );
	UxPutX( rowColumn35, 98 );

	UxPutEntryAlignment( rowColumn36, "alignment_center" );
	UxPutAdjustLast( rowColumn36, "true" );
	UxPutAdjustMargin( rowColumn36, "true" );
	UxPutSpacing( rowColumn36, 0 );
	UxPutPacking( rowColumn36, "pack_none" );
	UxPutMarginWidth( rowColumn36, 0 );
	UxPutMarginHeight( rowColumn36, 0 );
	UxPutBackground( rowColumn36, ApplicBackground );
	UxPutResizeWidth( rowColumn36, "false" );
	UxPutResizeHeight( rowColumn36, "false" );
	UxPutHeight( rowColumn36, 32 );
	UxPutWidth( rowColumn36, 23 );
	UxPutY( rowColumn36, 6 );
	UxPutX( rowColumn36, 44 );

	UxPutHighlightThickness( arrowButton35, 0 );
	UxPutForeground( arrowButton35, "PaleGreen" );
	UxPutTranslations( arrowButton35, Arrows5 );
	UxPutShadowThickness( arrowButton35, 0 );
	UxPutBackground( arrowButton35, TextBackground );
	UxPutHeight( arrowButton35, 15 );
	UxPutWidth( arrowButton35, 23 );
	UxPutY( arrowButton35, 0 );
	UxPutX( arrowButton35, 0 );

	UxPutHighlightThickness( arrowButton36, 0 );
	UxPutBorderColor( arrowButton36, ApplicBackground );
	UxPutForeground( arrowButton36, "PaleGreen" );
	UxPutTranslations( arrowButton36, Arrows5 );
	UxPutArrowDirection( arrowButton36, "arrow_down" );
	UxPutShadowThickness( arrowButton36, 0 );
	UxPutBackground( arrowButton36, TextBackground );
	UxPutHeight( arrowButton36, 15 );
	UxPutWidth( arrowButton36, 23 );
	UxPutY( arrowButton36, 16 );
	UxPutX( arrowButton36, 0 );

	UxPutMaxLength( text31, 100 );
	UxPutMarginWidth( text31, 2 );
	UxPutMarginHeight( text31, 2 );
	UxPutShadowThickness( text31, 2 );
	UxPutHighlightThickness( text31, 2 );
	UxPutForeground( text31, TextForeground );
	UxPutFontList( text31, TextFont );
	UxPutBackground( text31, TextBackground );
	UxPutHeight( text31, 32 );
	UxPutWidth( text31, 40 );
	UxPutY( text31, 7 );
	UxPutX( text31, 0 );

	UxPutFontList( label45, BoldTextFont );
	UxPutTranslations( label45, "" );
	UxPutRecomputeSize( label45, "false" );
	UxPutAlignment( label45, "alignment_center" );
	UxPutBackground( label45, LabelBackground );
	UxPutForeground( label45, TextForeground );
	UxPutLabelString( label45, "HighCut" );
	UxPutHeight( label45, 25 );
	UxPutWidth( label45, 60 );
	UxPutY( label45, 199 );
	UxPutX( label45, 10 );

	UxPutBackground( rowColumn40, ApplicBackground );
	UxPutNumColumns( rowColumn40, 2 );
	UxPutMarginWidth( rowColumn40, 0 );
	UxPutMarginHeight( rowColumn40, 0 );
	UxPutResizeWidth( rowColumn40, "false" );
	UxPutResizeHeight( rowColumn40, "false" );
	UxPutSpacing( rowColumn40, 0 );
	UxPutPacking( rowColumn40, "pack_none" );
	UxPutOrientation( rowColumn40, "horizontal" );
	UxPutHeight( rowColumn40, 40 );
	UxPutWidth( rowColumn40, 105 );
	UxPutY( rowColumn40, 100 );
	UxPutX( rowColumn40, 98 );

	UxPutMarginWidth( text35, 1 );
	UxPutMarginHeight( text35, 2 );
	UxPutColumns( text35, 8 );
	UxPutMaxLength( text35, 240 );
	UxPutFontList( text35, TextFont );
	UxPutForeground( text35, TextForeground );
	UxPutBackground( text35, TextBackground );
	UxPutHeight( text35, 32 );
	UxPutWidth( text35, 65 );
	UxPutY( text35, 7 );
	UxPutX( text35, 5 );

	UxPutEntryAlignment( rowColumn41, "alignment_center" );
	UxPutAdjustLast( rowColumn41, "true" );
	UxPutAdjustMargin( rowColumn41, "true" );
	UxPutSpacing( rowColumn41, 0 );
	UxPutPacking( rowColumn41, "pack_none" );
	UxPutMarginWidth( rowColumn41, 0 );
	UxPutMarginHeight( rowColumn41, 0 );
	UxPutBackground( rowColumn41, ApplicBackground );
	UxPutResizeWidth( rowColumn41, "false" );
	UxPutResizeHeight( rowColumn41, "false" );
	UxPutHeight( rowColumn41, 32 );
	UxPutWidth( rowColumn41, 23 );
	UxPutY( rowColumn41, 6 );
	UxPutX( rowColumn41, 72 );

	UxPutHighlightThickness( arrowButton43, 0 );
	UxPutForeground( arrowButton43, "PaleGreen" );
	UxPutTranslations( arrowButton43, Arrows5 );
	UxPutShadowThickness( arrowButton43, 0 );
	UxPutBackground( arrowButton43, TextBackground );
	UxPutHeight( arrowButton43, 15 );
	UxPutWidth( arrowButton43, 23 );
	UxPutY( arrowButton43, 0 );
	UxPutX( arrowButton43, 0 );

	UxPutHighlightThickness( arrowButton44, 0 );
	UxPutBorderColor( arrowButton44, ApplicBackground );
	UxPutForeground( arrowButton44, "PaleGreen" );
	UxPutTranslations( arrowButton44, Arrows5 );
	UxPutArrowDirection( arrowButton44, "arrow_down" );
	UxPutShadowThickness( arrowButton44, 0 );
	UxPutBackground( arrowButton44, TextBackground );
	UxPutHeight( arrowButton44, 15 );
	UxPutWidth( arrowButton44, 23 );
	UxPutY( arrowButton44, 16 );
	UxPutX( arrowButton44, 0 );

	UxPutFontList( label46, BoldTextFont );
	UxPutTranslations( label46, "" );
	UxPutRecomputeSize( label46, "false" );
	UxPutAlignment( label46, "alignment_center" );
	UxPutBackground( label46, LabelBackground );
	UxPutForeground( label46, TextForeground );
	UxPutLabelString( label46, "Descr Values" );
	UxPutHeight( label46, 25 );
	UxPutWidth( label46, 100 );
	UxPutY( label46, 110 );
	UxPutX( label46, 20 );

	UxPutSelectColor( toggleButton7, "Palegreen" );
	UxPutIndicatorSize( toggleButton7, 16 );
	UxPutIndicatorOn( toggleButton7, "true" );
	UxPutSet( toggleButton7, "true" );
	UxPutBorderWidth( toggleButton7, 1 );
	UxPutRecomputeSize( toggleButton7, "false" );
	UxPutForeground( toggleButton7, TextForeground );
	UxPutFontList( toggleButton7, BoldTextFont );
	UxPutBackground( toggleButton7, TextBackground );
	UxPutIndicatorType( toggleButton7, "one_of_many" );
	UxPutLabelString( toggleButton7, "Yes" );
	UxPutHeight( toggleButton7, 28 );
	UxPutWidth( toggleButton7, 70 );
	UxPutY( toggleButton7, 0 );
	UxPutX( toggleButton7, 0 );

	UxPutFontList( label47, BoldTextFont );
	UxPutTranslations( label47, "" );
	UxPutRecomputeSize( label47, "false" );
	UxPutAlignment( label47, "alignment_center" );
	UxPutBackground( label47, LabelBackground );
	UxPutForeground( label47, TextForeground );
	UxPutLabelString( label47, "CutDelta" );
	UxPutHeight( label47, 25 );
	UxPutWidth( label47, 62 );
	UxPutY( label47, 180 );
	UxPutX( label47, 15 );

	UxPutMarginWidth( text36, 1 );
	UxPutMarginHeight( text36, 2 );
	UxPutColumns( text36, 8 );
	UxPutMaxLength( text36, 240 );
	UxPutFontList( text36, TextFont );
	UxPutForeground( text36, TextForeground );
	UxPutBackground( text36, TextBackground );
	UxPutHeight( text36, 30 );
	UxPutWidth( text36, 65 );
	UxPutY( text36, 7 );
	UxPutX( text36, 5 );

	UxPutFontList( label48, BoldTextFont );
	UxPutTranslations( label48, "" );
	UxPutRecomputeSize( label48, "false" );
	UxPutAlignment( label48, "alignment_center" );
	UxPutBackground( label48, LabelBackground );
	UxPutForeground( label48, TextForeground );
	UxPutLabelString( label48, "MinMax" );
	UxPutHeight( label48, 25 );
	UxPutWidth( label48, 65 );
	UxPutY( label48, 119 );
	UxPutX( label48, 10 );

	UxPutBackground( rowColumn42, ApplicBackground );
	UxPutNumColumns( rowColumn42, 2 );
	UxPutMarginWidth( rowColumn42, 0 );
	UxPutMarginHeight( rowColumn42, 0 );
	UxPutResizeWidth( rowColumn42, "false" );
	UxPutResizeHeight( rowColumn42, "false" );
	UxPutSpacing( rowColumn42, 0 );
	UxPutPacking( rowColumn42, "pack_none" );
	UxPutOrientation( rowColumn42, "horizontal" );
	UxPutHeight( rowColumn42, 32 );
	UxPutWidth( rowColumn42, 170 );
	UxPutY( rowColumn42, 100 );
	UxPutX( rowColumn42, 98 );

	UxPutCursorPositionVisible( text37, "false" );
	UxPutHighlightThickness( text37, 0 );
	UxPutEditable( text37, "false" );
	UxPutMarginWidth( text37, 1 );
	UxPutMarginHeight( text37, 2 );
	UxPutColumns( text37, 8 );
	UxPutMaxLength( text37, 40 );
	UxPutFontList( text37, TextFont );
	UxPutForeground( text37, TextForeground );
	UxPutBackground( text37, SHelpBackground );
	UxPutHeight( text37, 32 );
	UxPutWidth( text37, 60 );
	UxPutY( text37, 2 );
	UxPutX( text37, 0 );

	UxPutCursorPositionVisible( text38, "false" );
	UxPutHighlightThickness( text38, 0 );
	UxPutEditable( text38, "false" );
	UxPutMarginWidth( text38, 1 );
	UxPutMarginHeight( text38, 2 );
	UxPutColumns( text38, 8 );
	UxPutMaxLength( text38, 40 );
	UxPutFontList( text38, TextFont );
	UxPutForeground( text38, TextForeground );
	UxPutBackground( text38, SHelpBackground );
	UxPutHeight( text38, 32 );
	UxPutWidth( text38, 60 );
	UxPutY( text38, 2 );
	UxPutX( text38, 85 );

	UxPutFontList( label38, BoldTextFont );
	UxPutTranslations( label38, "" );
	UxPutRecomputeSize( label38, "false" );
	UxPutAlignment( label38, "alignment_center" );
	UxPutBackground( label38, LabelBackground );
	UxPutForeground( label38, TextForeground );
	UxPutLabelString( label38, "LowCut" );
	UxPutHeight( label38, 25 );
	UxPutWidth( label38, 60 );
	UxPutY( label38, 199 );
	UxPutX( label38, 10 );

	UxPutBackground( rowColumn38, ApplicBackground );
	UxPutNumColumns( rowColumn38, 2 );
	UxPutMarginWidth( rowColumn38, 0 );
	UxPutMarginHeight( rowColumn38, 0 );
	UxPutResizeWidth( rowColumn38, "false" );
	UxPutResizeHeight( rowColumn38, "false" );
	UxPutSpacing( rowColumn38, 0 );
	UxPutPacking( rowColumn38, "pack_none" );
	UxPutOrientation( rowColumn38, "horizontal" );
	UxPutHeight( rowColumn38, 40 );
	UxPutWidth( rowColumn38, 105 );
	UxPutY( rowColumn38, 100 );
	UxPutX( rowColumn38, 98 );

	UxPutMarginWidth( text34, 1 );
	UxPutMarginHeight( text34, 2 );
	UxPutColumns( text34, 8 );
	UxPutMaxLength( text34, 240 );
	UxPutFontList( text34, TextFont );
	UxPutForeground( text34, TextForeground );
	UxPutBackground( text34, TextBackground );
	UxPutHeight( text34, 32 );
	UxPutWidth( text34, 65 );
	UxPutY( text34, 7 );
	UxPutX( text34, 5 );

	UxPutEntryAlignment( rowColumn39, "alignment_center" );
	UxPutAdjustLast( rowColumn39, "true" );
	UxPutAdjustMargin( rowColumn39, "true" );
	UxPutSpacing( rowColumn39, 0 );
	UxPutPacking( rowColumn39, "pack_none" );
	UxPutMarginWidth( rowColumn39, 0 );
	UxPutMarginHeight( rowColumn39, 0 );
	UxPutBackground( rowColumn39, ApplicBackground );
	UxPutResizeWidth( rowColumn39, "false" );
	UxPutResizeHeight( rowColumn39, "false" );
	UxPutHeight( rowColumn39, 32 );
	UxPutWidth( rowColumn39, 23 );
	UxPutY( rowColumn39, 4 );
	UxPutX( rowColumn39, 72 );

	UxPutHighlightThickness( arrowButton41, 0 );
	UxPutForeground( arrowButton41, "PaleGreen" );
	UxPutTranslations( arrowButton41, Arrows5 );
	UxPutShadowThickness( arrowButton41, 0 );
	UxPutBackground( arrowButton41, TextBackground );
	UxPutHeight( arrowButton41, 15 );
	UxPutWidth( arrowButton41, 23 );
	UxPutY( arrowButton41, 0 );
	UxPutX( arrowButton41, 0 );

	UxPutHighlightThickness( arrowButton42, 0 );
	UxPutBorderColor( arrowButton42, ApplicBackground );
	UxPutForeground( arrowButton42, "PaleGreen" );
	UxPutTranslations( arrowButton42, Arrows5 );
	UxPutArrowDirection( arrowButton42, "arrow_down" );
	UxPutShadowThickness( arrowButton42, 0 );
	UxPutBackground( arrowButton42, TextBackground );
	UxPutHeight( arrowButton42, 15 );
	UxPutWidth( arrowButton42, 23 );
	UxPutY( arrowButton42, 16 );
	UxPutX( arrowButton42, 0 );

	UxPutFontList( label42, BoldTextFont );
	UxPutTranslations( label42, "" );
	UxPutRecomputeSize( label42, "false" );
	UxPutAlignment( label42, "alignment_center" );
	UxPutBackground( label42, LabelBackground );
	UxPutForeground( label42, TextForeground );
	UxPutLabelString( label42, "LUT" );
	UxPutHeight( label42, 25 );
	UxPutWidth( label42, 70 );
	UxPutY( label42, 10 );
	UxPutX( label42, 10 );

	UxPutMarginWidth( text29, 2 );
	UxPutMarginHeight( text29, 2 );
	UxPutFontList( text29, TextFont );
	UxPutForeground( text29, TextForeground );
	UxPutTranslations( text29, SelectFile );
	UxPutBackground( text29, TextBackground );
	UxPutHeight( text29, 32 );
	UxPutWidth( text29, 200 );
	UxPutY( text29, 39 );
	UxPutX( text29, 10 );

	UxPutMarginHeight( label43, 0 );
	UxPutFontList( label43, BoldTextFont );
	UxPutTranslations( label43, "" );
	UxPutRecomputeSize( label43, "false" );
	UxPutAlignment( label43, "alignment_center" );
	UxPutBackground( label43, LabelBackground );
	UxPutForeground( label43, TextForeground );
	UxPutLabelString( label43, "Create" );
	UxPutHeight( label43, 20 );
	UxPutWidth( label43, 60 );
	UxPutY( label43, 110 );
	UxPutX( label43, 20 );

	UxPutAlignment( toggleButton13, "alignment_center" );
	UxPutMarginRight( toggleButton13, 2 );
	UxPutMarginLeft( toggleButton13, 1 );
	UxPutSelectColor( toggleButton13, "Palegreen" );
	UxPutIndicatorSize( toggleButton13, 16 );
	UxPutIndicatorOn( toggleButton13, "true" );
	UxPutSet( toggleButton13, "true" );
	UxPutBorderWidth( toggleButton13, 1 );
	UxPutRecomputeSize( toggleButton13, "false" );
	UxPutForeground( toggleButton13, TextForeground );
	UxPutFontList( toggleButton13, BoldTextFont );
	UxPutBackground( toggleButton13, TextBackground );
	UxPutIndicatorType( toggleButton13, "one_of_many" );
	UxPutLabelString( toggleButton13, "Yes" );
	UxPutHeight( toggleButton13, 28 );
	UxPutWidth( toggleButton13, 70 );
	UxPutY( toggleButton13, 0 );
	UxPutX( toggleButton13, 0 );

	UxPutFontList( label44, BoldTextFont );
	UxPutTranslations( label44, "" );
	UxPutRecomputeSize( label44, "false" );
	UxPutAlignment( label44, "alignment_center" );
	UxPutBackground( label44, LabelBackground );
	UxPutForeground( label44, TextForeground );
	UxPutLabelString( label44, "SizeX" );
	UxPutHeight( label44, 25 );
	UxPutWidth( label44, 45 );
	UxPutY( label44, 49 );
	UxPutX( label44, 9 );

	UxPutBackground( rowColumn23, ApplicBackground );
	UxPutNumColumns( rowColumn23, 2 );
	UxPutMarginWidth( rowColumn23, 0 );
	UxPutMarginHeight( rowColumn23, 0 );
	UxPutResizeWidth( rowColumn23, "false" );
	UxPutResizeHeight( rowColumn23, "false" );
	UxPutSpacing( rowColumn23, 0 );
	UxPutPacking( rowColumn23, "pack_none" );
	UxPutOrientation( rowColumn23, "horizontal" );
	UxPutHeight( rowColumn23, 44 );
	UxPutWidth( rowColumn23, 80 );
	UxPutY( rowColumn23, 15 );
	UxPutX( rowColumn23, 114 );

	UxPutEntryAlignment( rowColumn24, "alignment_center" );
	UxPutAdjustLast( rowColumn24, "true" );
	UxPutAdjustMargin( rowColumn24, "true" );
	UxPutSpacing( rowColumn24, 0 );
	UxPutPacking( rowColumn24, "pack_none" );
	UxPutMarginWidth( rowColumn24, 0 );
	UxPutMarginHeight( rowColumn24, 0 );
	UxPutBackground( rowColumn24, ApplicBackground );
	UxPutResizeWidth( rowColumn24, "false" );
	UxPutResizeHeight( rowColumn24, "false" );
	UxPutHeight( rowColumn24, 32 );
	UxPutWidth( rowColumn24, 23 );
	UxPutY( rowColumn24, 6 );
	UxPutX( rowColumn24, 57 );

	UxPutTranslations( arrowButton29, Arrows5 );
	UxPutHighlightThickness( arrowButton29, 0 );
	UxPutForeground( arrowButton29, "PaleGreen" );
	UxPutShadowThickness( arrowButton29, 0 );
	UxPutBackground( arrowButton29, TextBackground );
	UxPutHeight( arrowButton29, 15 );
	UxPutWidth( arrowButton29, 23 );
	UxPutY( arrowButton29, 0 );
	UxPutX( arrowButton29, 0 );

	UxPutTranslations( arrowButton30, Arrows5 );
	UxPutHighlightThickness( arrowButton30, 0 );
	UxPutBorderColor( arrowButton30, ApplicBackground );
	UxPutForeground( arrowButton30, "PaleGreen" );
	UxPutArrowDirection( arrowButton30, "arrow_down" );
	UxPutShadowThickness( arrowButton30, 0 );
	UxPutBackground( arrowButton30, TextBackground );
	UxPutHeight( arrowButton30, 15 );
	UxPutWidth( arrowButton30, 23 );
	UxPutY( arrowButton30, 16 );
	UxPutX( arrowButton30, 0 );

	UxPutX( text30, 1 );
	UxPutMaxLength( text30, 100 );
	UxPutMarginWidth( text30, 2 );
	UxPutMarginHeight( text30, 2 );
	UxPutShadowThickness( text30, 2 );
	UxPutHighlightThickness( text30, 2 );
	UxPutForeground( text30, TextForeground );
	UxPutFontList( text30, TextFont );
	UxPutBackground( text30, TextBackground );
	UxPutHeight( text30, 32 );
	UxPutWidth( text30, 50 );
	UxPutY( text30, 7 );

	UxPutFontList( label49, BoldTextFont );
	UxPutTranslations( label49, "" );
	UxPutRecomputeSize( label49, "false" );
	UxPutAlignment( label49, "alignment_center" );
	UxPutBackground( label49, LabelBackground );
	UxPutForeground( label49, TextForeground );
	UxPutLabelString( label49, "SizeY" );
	UxPutHeight( label49, 25 );
	UxPutWidth( label49, 50 );
	UxPutY( label49, 59 );
	UxPutX( label49, 209 );

	UxPutBackground( rowColumn25, ApplicBackground );
	UxPutNumColumns( rowColumn25, 2 );
	UxPutMarginWidth( rowColumn25, 0 );
	UxPutMarginHeight( rowColumn25, 0 );
	UxPutResizeWidth( rowColumn25, "false" );
	UxPutResizeHeight( rowColumn25, "false" );
	UxPutSpacing( rowColumn25, 0 );
	UxPutPacking( rowColumn25, "pack_none" );
	UxPutOrientation( rowColumn25, "horizontal" );
	UxPutHeight( rowColumn25, 44 );
	UxPutWidth( rowColumn25, 80 );
	UxPutY( rowColumn25, 60 );
	UxPutX( rowColumn25, 112 );

	UxPutEntryAlignment( rowColumn26, "alignment_center" );
	UxPutAdjustLast( rowColumn26, "true" );
	UxPutAdjustMargin( rowColumn26, "true" );
	UxPutSpacing( rowColumn26, 0 );
	UxPutPacking( rowColumn26, "pack_none" );
	UxPutMarginWidth( rowColumn26, 0 );
	UxPutMarginHeight( rowColumn26, 0 );
	UxPutBackground( rowColumn26, ApplicBackground );
	UxPutResizeWidth( rowColumn26, "false" );
	UxPutResizeHeight( rowColumn26, "false" );
	UxPutHeight( rowColumn26, 32 );
	UxPutWidth( rowColumn26, 23 );
	UxPutY( rowColumn26, 6 );
	UxPutX( rowColumn26, 57 );

	UxPutTranslations( arrowButton33, Arrows5 );
	UxPutHighlightThickness( arrowButton33, 0 );
	UxPutForeground( arrowButton33, "PaleGreen" );
	UxPutShadowThickness( arrowButton33, 0 );
	UxPutBackground( arrowButton33, TextBackground );
	UxPutHeight( arrowButton33, 15 );
	UxPutWidth( arrowButton33, 23 );
	UxPutY( arrowButton33, 0 );
	UxPutX( arrowButton33, 0 );

	UxPutTranslations( arrowButton34, Arrows5 );
	UxPutHighlightThickness( arrowButton34, 0 );
	UxPutBorderColor( arrowButton34, ApplicBackground );
	UxPutForeground( arrowButton34, "PaleGreen" );
	UxPutArrowDirection( arrowButton34, "arrow_down" );
	UxPutShadowThickness( arrowButton34, 0 );
	UxPutBackground( arrowButton34, TextBackground );
	UxPutHeight( arrowButton34, 15 );
	UxPutWidth( arrowButton34, 23 );
	UxPutY( arrowButton34, 16 );
	UxPutX( arrowButton34, 0 );

	UxPutMaxLength( text32, 100 );
	UxPutMarginWidth( text32, 2 );
	UxPutMarginHeight( text32, 2 );
	UxPutShadowThickness( text32, 2 );
	UxPutHighlightThickness( text32, 2 );
	UxPutForeground( text32, TextForeground );
	UxPutFontList( text32, TextFont );
	UxPutBackground( text32, TextBackground );
	UxPutHeight( text32, 32 );
	UxPutWidth( text32, 50 );
	UxPutY( text32, 7 );
	UxPutX( text32, 1 );

	UxPutMarginHeight( label50, 0 );
	UxPutFontList( label50, BoldTextFont );
	UxPutTranslations( label50, "" );
	UxPutRecomputeSize( label50, "false" );
	UxPutAlignment( label50, "alignment_center" );
	UxPutBackground( label50, LabelBackground );
	UxPutForeground( label50, TextForeground );
	UxPutLabelString( label50, "Display" );
	UxPutHeight( label50, 20 );
	UxPutWidth( label50, 60 );
	UxPutY( label50, 218 );
	UxPutX( label50, 15 );

	UxPutFontList( label51, BoldTextFont );
	UxPutTranslations( label51, "" );
	UxPutRecomputeSize( label51, "false" );
	UxPutAlignment( label51, "alignment_center" );
	UxPutBackground( label51, LabelBackground );
	UxPutForeground( label51, TextForeground );
	UxPutLabelString( label51, " ScaleY" );
	UxPutHeight( label51, 25 );
	UxPutWidth( label51, 55 );
	UxPutY( label51, 119 );
	UxPutX( label51, 10 );

	UxPutBackground( rowColumn37, ApplicBackground );
	UxPutNumColumns( rowColumn37, 2 );
	UxPutMarginWidth( rowColumn37, 0 );
	UxPutMarginHeight( rowColumn37, 0 );
	UxPutResizeWidth( rowColumn37, "false" );
	UxPutResizeHeight( rowColumn37, "false" );
	UxPutSpacing( rowColumn37, 0 );
	UxPutPacking( rowColumn37, "pack_none" );
	UxPutOrientation( rowColumn37, "horizontal" );
	UxPutHeight( rowColumn37, 40 );
	UxPutWidth( rowColumn37, 70 );
	UxPutY( rowColumn37, 55 );
	UxPutX( rowColumn37, 98 );

	UxPutEntryAlignment( rowColumn43, "alignment_center" );
	UxPutAdjustLast( rowColumn43, "true" );
	UxPutAdjustMargin( rowColumn43, "true" );
	UxPutSpacing( rowColumn43, 0 );
	UxPutPacking( rowColumn43, "pack_none" );
	UxPutMarginWidth( rowColumn43, 0 );
	UxPutMarginHeight( rowColumn43, 0 );
	UxPutBackground( rowColumn43, ApplicBackground );
	UxPutResizeWidth( rowColumn43, "false" );
	UxPutResizeHeight( rowColumn43, "false" );
	UxPutHeight( rowColumn43, 32 );
	UxPutWidth( rowColumn43, 23 );
	UxPutY( rowColumn43, 6 );
	UxPutX( rowColumn43, 44 );

	UxPutHighlightThickness( arrowButton37, 0 );
	UxPutForeground( arrowButton37, "PaleGreen" );
	UxPutTranslations( arrowButton37, Arrows5 );
	UxPutShadowThickness( arrowButton37, 0 );
	UxPutBackground( arrowButton37, TextBackground );
	UxPutHeight( arrowButton37, 15 );
	UxPutWidth( arrowButton37, 23 );
	UxPutY( arrowButton37, 0 );
	UxPutX( arrowButton37, 0 );

	UxPutHighlightThickness( arrowButton38, 0 );
	UxPutBorderColor( arrowButton38, ApplicBackground );
	UxPutForeground( arrowButton38, "PaleGreen" );
	UxPutTranslations( arrowButton38, Arrows5 );
	UxPutArrowDirection( arrowButton38, "arrow_down" );
	UxPutShadowThickness( arrowButton38, 0 );
	UxPutBackground( arrowButton38, TextBackground );
	UxPutHeight( arrowButton38, 15 );
	UxPutWidth( arrowButton38, 23 );
	UxPutY( arrowButton38, 16 );
	UxPutX( arrowButton38, 0 );

	UxPutMaxLength( text33, 100 );
	UxPutMarginWidth( text33, 2 );
	UxPutMarginHeight( text33, 2 );
	UxPutShadowThickness( text33, 2 );
	UxPutHighlightThickness( text33, 2 );
	UxPutForeground( text33, TextForeground );
	UxPutFontList( text33, TextFont );
	UxPutBackground( text33, TextBackground );
	UxPutHeight( text33, 32 );
	UxPutWidth( text33, 40 );
	UxPutY( text33, 7 );
	UxPutX( text33, 0 );

	UxPutForeground( XDMenus, MenuForeground );
	UxPutBackground( XDMenus, MenuBackground );
	UxPutMenuAccelerator( XDMenus, "<KeyUp>F10" );
	UxPutRowColumnType( XDMenus, "menu_bar" );
	UxPutBorderWidth( XDMenus, 0 );

	UxPutForeground( commandPane, MenuForeground );
	UxPutBackground( commandPane, MenuBackground );
	UxPutRowColumnType( commandPane, "menu_pulldown" );

	UxPutFontList( command_b2, BoldTextFont );
	UxPutLabelString( command_b2, "Clear Channel" );

	UxPutFontList( command_b3, BoldTextFont );
	UxPutLabelString( command_b3, "Clear Display" );

	UxPutFontList( commandPane_b17, BoldTextFont );
	UxPutLabelString( commandPane_b17, "Clear Overlay" );

	UxPutFontList( command_b9, BoldTextFont );
	UxPutLabelString( command_b9, "Disable Overlay" );

	UxPutFontList( command_b10, BoldTextFont );
	UxPutLabelString( command_b10, "Enable Overlay" );

	UxPutFontList( commandPane_b14, BoldTextFont );
	UxPutLabelString( commandPane_b14, "Delete Display" );

	UxPutFontList( commandPane_b15, BoldTextFont );
	UxPutLabelString( commandPane_b15, "Delete Graphics" );

	UxPutFontList( commandPane_b16, BoldTextFont );
	UxPutLabelString( commandPane_b16, "Reset Display" );

	UxPutX( commandsCascade, 101 );
	UxPutMnemonic( commandsCascade, "C" );
	UxPutForeground( commandsCascade, MenuForeground );
	UxPutFontList( commandsCascade, BoldTextFont );
	UxPutBackground( commandsCascade, MenuBackground );
	UxPutLabelString( commandsCascade, "Commands" );

	UxPutBackground( LUTCommsPane, MenuBackground );
	UxPutForeground( LUTCommsPane, MenuForeground );
	UxPutRowColumnType( LUTCommsPane, "menu_pulldown" );

	UxPutFontList( LUTcomm_b1, BoldTextFont );
	UxPutLabelString( LUTcomm_b1, "LUT Off" );

	UxPutFontList( LUTcomm_b2, BoldTextFont );
	UxPutLabelString( LUTcomm_b2, "LUT On" );

	UxPutFontList( LUTcomm_b3, BoldTextFont );
	UxPutLabelString( LUTcomm_b3, "LUT Display On" );

	UxPutFontList( LUTcomm_b4, BoldTextFont );
	UxPutLabelString( LUTcomm_b4, "LUT Display Off" );

	UxPutFontList( LUTcomm_b5, BoldTextFont );
	UxPutLabelString( LUTcomm_b5, "ITT Off" );

	UxPutFontList( LUTcomm_b6, BoldTextFont );
	UxPutLabelString( LUTcomm_b6, "ITT On" );

	UxPutBackground( LUTCommsCascade, MenuBackground );
	UxPutForeground( LUTCommsCascade, MenuForeground );
	UxPutFontList( LUTCommsCascade, BoldTextFont );
	UxPutMnemonic( LUTCommsCascade, "L" );
	UxPutLabelString( LUTCommsCascade, "LUTcmnds" );

	UxPutForeground( helpPane, MenuForeground );
	UxPutBackground( helpPane, MenuBackground );
	UxPutRowColumnType( helpPane, "menu_pulldown" );

	UxPutFontList( XDHelp, BoldTextFont );
	UxPutMnemonic( XDHelp, "H" );
	UxPutLabelString( XDHelp, "On XDisplay..." );

	UxPutFontList( InterfHelp, BoldTextFont );
	UxPutMnemonic( InterfHelp, "H" );
	UxPutLabelString( InterfHelp, "On this interface..." );

	UxPutFontList( EditHelp, BoldTextFont );
	UxPutMnemonic( EditHelp, "K" );
	UxPutLabelString( EditHelp, "On editing..." );

	UxPutFontList( SelectHelp, BoldTextFont );
	UxPutLabelString( SelectHelp, "On selecting files..." );

	UxPutBackground( helpCascade, MenuBackground );
	UxPutForeground( helpCascade, MenuForeground );
	UxPutFontList( helpCascade, BoldTextFont );
	UxPutMnemonic( helpCascade, "H" );
	UxPutLabelString( helpCascade, "Help" );

	UxPutForeground( quitPane, MenuForeground );
	UxPutBackground( quitPane, MenuBackground );
	UxPutRowColumnType( quitPane, "menu_pulldown" );

	UxPutFontList( quit_b1, BoldTextFont );
	UxPutLabelString( quit_b1, "Bye" );

	UxPutBackground( quitCascade, MenuBackground );
	UxPutX( quitCascade, 300 );
	UxPutForeground( quitCascade, MenuForeground );
	UxPutFontList( quitCascade, BoldTextFont );
	UxPutMnemonic( quitCascade, "Q" );
	UxPutLabelString( quitCascade, "Quit" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	mainWindow = UxCreateMainWindow( "mainWindow", ApplicWindow );
	workAreaForm = UxCreateForm( "workAreaForm", mainWindow );
	workAreaFrame = UxCreateFrame( "workAreaFrame", workAreaForm );
	workArea = UxCreateForm( "workArea", workAreaFrame );
	form2 = UxCreateForm( "form2", workArea );
	label14 = UxCreateLabel( "label14", form2 );
	text16 = UxCreateText( "text16", form2 );
	label27 = UxCreateLabel( "label27", form2 );
	label24 = UxCreateLabel( "label24", form2 );
	label25 = UxCreateLabel( "label25", form2 );
	label15 = UxCreateLabel( "label15", form2 );
	rowColumn13 = UxCreateRowColumn( "rowColumn13", form2 );
	rowColumn20 = UxCreateRowColumn( "rowColumn20", rowColumn13 );
	arrowButton5 = UxCreateArrowButton( "arrowButton5", rowColumn20 );
	arrowButton6 = UxCreateArrowButton( "arrowButton6", rowColumn20 );
	text2 = UxCreateText( "text2", rowColumn13 );
	label32 = UxCreateLabel( "label32", form2 );
	rowColumn18 = UxCreateRowColumn( "rowColumn18", form2 );
	text20 = UxCreateText( "text20", rowColumn18 );
	arrowButton22 = UxCreateArrowButton( "arrowButton22", rowColumn18 );
	arrowButton21 = UxCreateArrowButton( "arrowButton21", rowColumn18 );
	arrowButton23 = UxCreateArrowButton( "arrowButton23", rowColumn18 );
	arrowButton24 = UxCreateArrowButton( "arrowButton24", rowColumn18 );
	text24 = UxCreateText( "text24", rowColumn18 );
	rowColumn31 = UxCreateRowColumn( "rowColumn31", form2 );
	text15 = UxCreateText( "text15", rowColumn31 );
	rowColumn32 = UxCreateRowColumn( "rowColumn32", rowColumn31 );
	arrowButton25 = UxCreateArrowButton( "arrowButton25", rowColumn32 );
	arrowButton26 = UxCreateArrowButton( "arrowButton26", rowColumn32 );
	label28 = UxCreateLabel( "label28", form2 );
	rowColumn33 = UxCreateRowColumn( "rowColumn33", form2 );
	text22 = UxCreateText( "text22", rowColumn33 );
	rowColumn34 = UxCreateRowColumn( "rowColumn34", rowColumn33 );
	arrowButton27 = UxCreateArrowButton( "arrowButton27", rowColumn34 );
	arrowButton28 = UxCreateArrowButton( "arrowButton28", rowColumn34 );
	label35 = UxCreateLabel( "label35", form2 );
	toggleButton1 = UxCreateToggleButton( "toggleButton1", form2 );
	label36 = UxCreateLabel( "label36", form2 );
	text25 = UxCreateText( "text25", form2 );
	label10 = UxCreateLabel( "label10", form2 );
	rowColumn17 = UxCreateRowColumn( "rowColumn17", form2 );
	text11 = UxCreateText( "text11", rowColumn17 );
	text12 = UxCreateText( "text12", rowColumn17 );
	rowColumn29 = UxCreateRowColumn( "rowColumn29", form2 );
	rowColumn30 = UxCreateRowColumn( "rowColumn30", rowColumn29 );
	arrowButton39 = UxCreateArrowButton( "arrowButton39", rowColumn30 );
	arrowButton40 = UxCreateArrowButton( "arrowButton40", rowColumn30 );
	text39 = UxCreateText( "text39", rowColumn29 );
	label52 = UxCreateLabel( "label52", form2 );
	rowColumn44 = UxCreateRowColumn( "rowColumn44", form2 );
	rowColumn45 = UxCreateRowColumn( "rowColumn45", rowColumn44 );
	arrowButton45 = UxCreateArrowButton( "arrowButton45", rowColumn45 );
	arrowButton46 = UxCreateArrowButton( "arrowButton46", rowColumn45 );
	text40 = UxCreateText( "text40", rowColumn44 );
	rowColumn22 = UxCreateRowColumn( "rowColumn22", form2 );
	rowColumn28 = UxCreateRowColumn( "rowColumn28", rowColumn22 );
	arrowButton47 = UxCreateArrowButton( "arrowButton47", rowColumn28 );
	arrowButton48 = UxCreateArrowButton( "arrowButton48", rowColumn28 );
	text41 = UxCreateText( "text41", rowColumn22 );
	label53 = UxCreateLabel( "label53", form2 );
	SHelp = UxCreateText( "SHelp", workArea );
	pushButton1 = UxCreatePushButton( "pushButton1", workArea );
	form4 = UxCreateForm( "form4", workArea );
	label2 = UxCreateLabel( "label2", form4 );
	text9 = UxCreateText( "text9", form4 );
	label1 = UxCreateLabel( "label1", form4 );
	toggleButton10 = UxCreateToggleButton( "toggleButton10", form4 );
	label26 = UxCreateLabel( "label26", form4 );
	text23 = UxCreateText( "text23", form4 );
	form3 = UxCreateForm( "form3", workArea );
	label9 = UxCreateLabel( "label9", form3 );
	text10 = UxCreateText( "text10", form3 );
	label13 = UxCreateLabel( "label13", form3 );
	text17 = UxCreateText( "text17", form3 );
	label11 = UxCreateLabel( "label11", form3 );
	label12 = UxCreateLabel( "label12", form3 );
	toggleButton2 = UxCreateToggleButton( "toggleButton2", form3 );
	toggleButton4 = UxCreateToggleButton( "toggleButton4", form3 );
	form6 = UxCreateForm( "form6", workArea );
	label16 = UxCreateLabel( "label16", form6 );
	label17 = UxCreateLabel( "label17", form6 );
	label5 = UxCreateLabel( "label5", form6 );
	label19 = UxCreateLabel( "label19", form6 );
	label20 = UxCreateLabel( "label20", form6 );
	label21 = UxCreateLabel( "label21", form6 );
	text21 = UxCreateText( "text21", form6 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", form6 );
	rowColumn9 = UxCreateRowColumn( "rowColumn9", rowColumn1 );
	arrowButton7 = UxCreateArrowButton( "arrowButton7", rowColumn9 );
	arrowButton8 = UxCreateArrowButton( "arrowButton8", rowColumn9 );
	text1 = UxCreateText( "text1", rowColumn1 );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", form6 );
	rowColumn10 = UxCreateRowColumn( "rowColumn10", rowColumn2 );
	arrowButton3 = UxCreateArrowButton( "arrowButton3", rowColumn10 );
	arrowButton4 = UxCreateArrowButton( "arrowButton4", rowColumn10 );
	text4 = UxCreateText( "text4", rowColumn2 );
	label18 = UxCreateLabel( "label18", form6 );
	rowColumn3 = UxCreateRowColumn( "rowColumn3", form6 );
	rowColumn11 = UxCreateRowColumn( "rowColumn11", rowColumn3 );
	arrowButton9 = UxCreateArrowButton( "arrowButton9", rowColumn11 );
	arrowButton10 = UxCreateArrowButton( "arrowButton10", rowColumn11 );
	text5 = UxCreateText( "text5", rowColumn3 );
	rowColumn5 = UxCreateRowColumn( "rowColumn5", form6 );
	rowColumn12 = UxCreateRowColumn( "rowColumn12", rowColumn5 );
	arrowButton11 = UxCreateArrowButton( "arrowButton11", rowColumn12 );
	arrowButton12 = UxCreateArrowButton( "arrowButton12", rowColumn12 );
	text6 = UxCreateText( "text6", rowColumn5 );
	label22 = UxCreateLabel( "label22", form6 );
	rowColumn4 = UxCreateRowColumn( "rowColumn4", form6 );
	rowColumn14 = UxCreateRowColumn( "rowColumn14", rowColumn4 );
	arrowButton17 = UxCreateArrowButton( "arrowButton17", rowColumn14 );
	arrowButton18 = UxCreateArrowButton( "arrowButton18", rowColumn14 );
	text8 = UxCreateText( "text8", rowColumn4 );
	toggleButton25 = UxCreateToggleButton( "toggleButton25", form6 );
	rowColumn6 = UxCreateRowColumn( "rowColumn6", form6 );
	rowColumn7 = UxCreateRowColumn( "rowColumn7", rowColumn6 );
	arrowButton13 = UxCreateArrowButton( "arrowButton13", rowColumn7 );
	arrowButton14 = UxCreateArrowButton( "arrowButton14", rowColumn7 );
	text13 = UxCreateText( "text13", rowColumn6 );
	label54 = UxCreateLabel( "label54", form6 );
	toggleButton15 = UxCreateToggleButton( "toggleButton15", form6 );
	form7 = UxCreateForm( "form7", workArea );
	label3 = UxCreateLabel( "label3", form7 );
	text3 = UxCreateText( "text3", form7 );
	label7 = UxCreateLabel( "label7", form7 );
	label8 = UxCreateLabel( "label8", form7 );
	label4 = UxCreateLabel( "label4", form7 );
	toggleButton8 = UxCreateToggleButton( "toggleButton8", form7 );
	toggleButton9 = UxCreateToggleButton( "toggleButton9", form7 );
	toggleButton14 = UxCreateToggleButton( "toggleButton14", form7 );
	pushButton6 = UxCreatePushButton( "pushButton6", workArea );
	form1 = UxCreateForm( "form1", workArea );
	label6 = UxCreateLabel( "label6", form1 );
	text7 = UxCreateText( "text7", form1 );
	label31 = UxCreateLabel( "label31", form1 );
	plotflag_p1 = UxCreateRowColumn( "plotflag_p1", form1 );
	plotflag_p1_b1 = UxCreatePushButtonGadget( "plotflag_p1_b1", plotflag_p1 );
	plotflag_p1_b2 = UxCreatePushButtonGadget( "plotflag_p1_b2", plotflag_p1 );
	plotflag_p1_b3 = UxCreatePushButtonGadget( "plotflag_p1_b3", plotflag_p1 );
	plotflag = UxCreateRowColumn( "plotflag", form1 );
	label29 = UxCreateLabel( "label29", form1 );
	label30 = UxCreateLabel( "label30", form1 );
	text14 = UxCreateText( "text14", form1 );
	toggleButton6 = UxCreateToggleButton( "toggleButton6", form1 );
	form8 = UxCreateForm( "form8", workArea );
	label39 = UxCreateLabel( "label39", form8 );
	label40 = UxCreateLabel( "label40", form8 );
	method_p3 = UxCreateRowColumn( "method_p3", form8 );
	method_p1_b9 = UxCreatePushButtonGadget( "method_p1_b9", method_p3 );
	method_p1_b10 = UxCreatePushButtonGadget( "method_p1_b10", method_p3 );
	method_p1_b11 = UxCreatePushButtonGadget( "method_p1_b11", method_p3 );
	method_p1_b12 = UxCreatePushButtonGadget( "method_p1_b12", method_p3 );
	method2 = UxCreateRowColumn( "method2", form8 );
	color_p2 = UxCreateRowColumn( "color_p2", form8 );
	color_p1_b12 = UxCreatePushButtonGadget( "color_p1_b12", color_p2 );
	color_p1_b13 = UxCreatePushButtonGadget( "color_p1_b13", color_p2 );
	color_p1_b14 = UxCreatePushButtonGadget( "color_p1_b14", color_p2 );
	color_p1_b15 = UxCreatePushButtonGadget( "color_p1_b15", color_p2 );
	color_p1_b16 = UxCreatePushButtonGadget( "color_p1_b16", color_p2 );
	color_p1_b17 = UxCreatePushButtonGadget( "color_p1_b17", color_p2 );
	color_p1_b18 = UxCreatePushButtonGadget( "color_p1_b18", color_p2 );
	color_p1_b19 = UxCreatePushButtonGadget( "color_p1_b19", color_p2 );
	color_p1_b20 = UxCreatePushButtonGadget( "color_p1_b20", color_p2 );
	color_p1_b21 = UxCreatePushButtonGadget( "color_p1_b21", color_p2 );
	color_p1_b22 = UxCreatePushButtonGadget( "color_p1_b22", color_p2 );
	color1 = UxCreateRowColumn( "color1", form8 );
	separator1 = UxCreateSeparator( "separator1", form8 );
	label23 = UxCreateLabel( "label23", form8 );
	method_p1 = UxCreateRowColumn( "method_p1", form8 );
	method_p1_b1 = UxCreatePushButtonGadget( "method_p1_b1", method_p1 );
	method_p1_b2 = UxCreatePushButtonGadget( "method_p1_b2", method_p1 );
	method_p1_b3 = UxCreatePushButtonGadget( "method_p1_b3", method_p1 );
	method_p1_b4 = UxCreatePushButtonGadget( "method_p1_b4", method_p1 );
	method1 = UxCreateRowColumn( "method1", form8 );
	label33 = UxCreateLabel( "label33", form8 );
	rowColumn19 = UxCreateRowColumn( "rowColumn19", form8 );
	rowColumn21 = UxCreateRowColumn( "rowColumn21", rowColumn19 );
	arrowButton1 = UxCreateArrowButton( "arrowButton1", rowColumn21 );
	arrowButton2 = UxCreateArrowButton( "arrowButton2", rowColumn21 );
	text27 = UxCreateText( "text27", rowColumn19 );
	toggleButton3 = UxCreateToggleButton( "toggleButton3", form8 );
	toggleButton5 = UxCreateToggleButton( "toggleButton5", form8 );
	pushButton2 = UxCreatePushButton( "pushButton2", workArea );
	pushButton9 = UxCreatePushButton( "pushButton9", workArea );
	pushButton4 = UxCreatePushButton( "pushButton4", workArea );
	pushButton7 = UxCreatePushButton( "pushButton7", workArea );
	pushButton3 = UxCreatePushButton( "pushButton3", workArea );
	pushButton8 = UxCreatePushButton( "pushButton8", workArea );
	pushButton5 = UxCreatePushButton( "pushButton5", workArea );
	form5 = UxCreateForm( "form5", workArea );
	label37 = UxCreateLabel( "label37", form5 );
	text28 = UxCreateText( "text28", form5 );
	label41 = UxCreateLabel( "label41", form5 );
	rowColumn35 = UxCreateRowColumn( "rowColumn35", form5 );
	rowColumn36 = UxCreateRowColumn( "rowColumn36", rowColumn35 );
	arrowButton35 = UxCreateArrowButton( "arrowButton35", rowColumn36 );
	arrowButton36 = UxCreateArrowButton( "arrowButton36", rowColumn36 );
	text31 = UxCreateText( "text31", rowColumn35 );
	label45 = UxCreateLabel( "label45", form5 );
	rowColumn40 = UxCreateRowColumn( "rowColumn40", form5 );
	text35 = UxCreateText( "text35", rowColumn40 );
	rowColumn41 = UxCreateRowColumn( "rowColumn41", rowColumn40 );
	arrowButton43 = UxCreateArrowButton( "arrowButton43", rowColumn41 );
	arrowButton44 = UxCreateArrowButton( "arrowButton44", rowColumn41 );
	label46 = UxCreateLabel( "label46", form5 );
	toggleButton7 = UxCreateToggleButton( "toggleButton7", form5 );
	label47 = UxCreateLabel( "label47", form5 );
	text36 = UxCreateText( "text36", form5 );
	label48 = UxCreateLabel( "label48", form5 );
	rowColumn42 = UxCreateRowColumn( "rowColumn42", form5 );
	text37 = UxCreateText( "text37", rowColumn42 );
	text38 = UxCreateText( "text38", rowColumn42 );
	label38 = UxCreateLabel( "label38", form5 );
	rowColumn38 = UxCreateRowColumn( "rowColumn38", form5 );
	text34 = UxCreateText( "text34", rowColumn38 );
	rowColumn39 = UxCreateRowColumn( "rowColumn39", rowColumn38 );
	arrowButton41 = UxCreateArrowButton( "arrowButton41", rowColumn39 );
	arrowButton42 = UxCreateArrowButton( "arrowButton42", rowColumn39 );
	label42 = UxCreateLabel( "label42", form5 );
	text29 = UxCreateText( "text29", form5 );
	label43 = UxCreateLabel( "label43", form5 );
	toggleButton13 = UxCreateToggleButton( "toggleButton13", form5 );
	label44 = UxCreateLabel( "label44", form5 );
	rowColumn23 = UxCreateRowColumn( "rowColumn23", form5 );
	rowColumn24 = UxCreateRowColumn( "rowColumn24", rowColumn23 );
	arrowButton29 = UxCreateArrowButton( "arrowButton29", rowColumn24 );
	arrowButton30 = UxCreateArrowButton( "arrowButton30", rowColumn24 );
	text30 = UxCreateText( "text30", rowColumn23 );
	label49 = UxCreateLabel( "label49", form5 );
	rowColumn25 = UxCreateRowColumn( "rowColumn25", form5 );
	rowColumn26 = UxCreateRowColumn( "rowColumn26", rowColumn25 );
	arrowButton33 = UxCreateArrowButton( "arrowButton33", rowColumn26 );
	arrowButton34 = UxCreateArrowButton( "arrowButton34", rowColumn26 );
	text32 = UxCreateText( "text32", rowColumn25 );
	label50 = UxCreateLabel( "label50", form5 );
	label51 = UxCreateLabel( "label51", form5 );
	rowColumn37 = UxCreateRowColumn( "rowColumn37", form5 );
	rowColumn43 = UxCreateRowColumn( "rowColumn43", rowColumn37 );
	arrowButton37 = UxCreateArrowButton( "arrowButton37", rowColumn43 );
	arrowButton38 = UxCreateArrowButton( "arrowButton38", rowColumn43 );
	text33 = UxCreateText( "text33", rowColumn37 );
	XDMenus = UxCreateRowColumn( "XDMenus", mainWindow );
	commandPane = UxCreateRowColumn( "commandPane", XDMenus );
	command_b2 = UxCreatePushButtonGadget( "command_b2", commandPane );
	command_b3 = UxCreatePushButtonGadget( "command_b3", commandPane );
	commandPane_b17 = UxCreatePushButtonGadget( "commandPane_b17", commandPane );
	command_b9 = UxCreatePushButtonGadget( "command_b9", commandPane );
	command_b10 = UxCreatePushButtonGadget( "command_b10", commandPane );
	commandPane_b14 = UxCreatePushButtonGadget( "commandPane_b14", commandPane );
	commandPane_b15 = UxCreatePushButtonGadget( "commandPane_b15", commandPane );
	commandPane_b16 = UxCreatePushButtonGadget( "commandPane_b16", commandPane );
	commandsCascade = UxCreateCascadeButton( "commandsCascade", XDMenus );
	LUTCommsPane = UxCreateRowColumn( "LUTCommsPane", XDMenus );
	LUTcomm_b1 = UxCreatePushButtonGadget( "LUTcomm_b1", LUTCommsPane );
	LUTcomm_b2 = UxCreatePushButtonGadget( "LUTcomm_b2", LUTCommsPane );
	LUTcomm_b3 = UxCreatePushButtonGadget( "LUTcomm_b3", LUTCommsPane );
	LUTcomm_b4 = UxCreatePushButtonGadget( "LUTcomm_b4", LUTCommsPane );
	LUTcomm_b5 = UxCreatePushButtonGadget( "LUTcomm_b5", LUTCommsPane );
	LUTcomm_b6 = UxCreatePushButtonGadget( "LUTcomm_b6", LUTCommsPane );
	LUTCommsCascade = UxCreateCascadeButton( "LUTCommsCascade", XDMenus );
	helpPane = UxCreateRowColumn( "helpPane", XDMenus );
	XDHelp = UxCreatePushButtonGadget( "XDHelp", helpPane );
	InterfHelp = UxCreatePushButtonGadget( "InterfHelp", helpPane );
	EditHelp = UxCreatePushButtonGadget( "EditHelp", helpPane );
	SelectHelp = UxCreatePushButtonGadget( "SelectHelp", helpPane );
	helpCascade = UxCreateCascadeButton( "helpCascade", XDMenus );
	quitPane = UxCreateRowColumn( "quitPane", XDMenus );
	quit_b1 = UxCreatePushButtonGadget( "quit_b1", quitPane );
	quitCascade = UxCreateCascadeButton( "quitCascade", XDMenus );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( mainWindow );
	UxCreateWidget( workAreaForm );
	UxPutTopAttachment( workAreaFrame, "attach_form" );
	UxPutRightAttachment( workAreaFrame, "attach_form" );
	UxPutLeftAttachment( workAreaFrame, "attach_form" );
	UxPutBottomAttachment( workAreaFrame, "attach_form" );
	UxCreateWidget( workAreaFrame );

	UxCreateWidget( workArea );
	createCB_workArea( UxGetWidget( workArea ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( form2, 4 );
	UxPutTopAttachment( form2, "attach_form" );
	UxPutRightOffset( form2, 4 );
	UxPutRightAttachment( form2, "attach_form" );
	UxPutLeftOffset( form2, 4 );
	UxPutLeftAttachment( form2, "attach_form" );
	UxCreateWidget( form2 );
	createCB_form2( UxGetWidget( form2 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label14, 10 );
	UxPutTopAttachment( label14, "attach_form" );
	UxPutLeftOffset( label14, 5 );
	UxPutLeftAttachment( label14, "attach_form" );
	UxCreateWidget( label14 );

	UxPutLeftWidget( text16, "label14" );
	UxPutTopOffset( text16, 5 );
	UxPutTopAttachment( text16, "attach_form" );
	UxPutRightOffset( text16, 70 );
	UxPutRightAttachment( text16, "attach_form" );
	UxPutLeftOffset( text16, 2 );
	UxPutLeftAttachment( text16, "attach_widget" );
	UxCreateWidget( text16 );

	UxPutTopWidget( label27, "text16" );
	UxPutTopOffset( label27, 48 );
	UxPutTopAttachment( label27, "attach_form" );
	UxPutLeftOffset( label27, 5 );
	UxPutLeftAttachment( label27, "attach_form" );
	UxCreateWidget( label27 );

	UxPutTopWidget( label24, "" );
	UxPutTopOffset( label24, 135 );
	UxPutTopAttachment( label24, "attach_form" );
	UxPutLeftOffset( label24, 5 );
	UxPutLeftAttachment( label24, "attach_form" );
	UxCreateWidget( label24 );

	UxPutTopWidget( label25, "label24" );
	UxPutTopOffset( label25, 82 );
	UxPutTopAttachment( label25, "attach_form" );
	UxPutLeftOffset( label25, 50 );
	UxPutLeftAttachment( label25, "attach_form" );
	UxCreateWidget( label25 );

	UxPutTopWidget( label15, "label25" );
	UxPutTopOffset( label15, 135 );
	UxPutTopAttachment( label15, "attach_form" );
	UxPutLeftOffset( label15, 275 );
	UxPutLeftAttachment( label15, "attach_form" );
	UxCreateWidget( label15 );

	UxPutTopOffset( rowColumn13, 38 );
	UxPutTopAttachment( rowColumn13, "attach_form" );
	UxPutLeftWidget( rowColumn13, "label27" );
	UxPutLeftOffset( rowColumn13, 0 );
	UxPutLeftAttachment( rowColumn13, "attach_widget" );
	UxCreateWidget( rowColumn13 );

	UxCreateWidget( rowColumn20 );
	UxCreateWidget( arrowButton5 );
	UxCreateWidget( arrowButton6 );
	UxCreateWidget( text2 );
	UxPutTopOffset( label32, 48 );
	UxPutTopAttachment( label32, "attach_form" );
	UxPutLeftOffset( label32, 275 );
	UxPutLeftAttachment( label32, "attach_form" );
	UxCreateWidget( label32 );

	UxPutTopOffset( rowColumn18, 66 );
	UxPutTopAttachment( rowColumn18, "attach_form" );
	UxPutLeftWidget( rowColumn18, "label25" );
	UxPutLeftOffset( rowColumn18, 2 );
	UxPutLeftAttachment( rowColumn18, "attach_widget" );
	UxCreateWidget( rowColumn18 );

	UxCreateWidget( text20 );
	UxCreateWidget( arrowButton22 );
	UxCreateWidget( arrowButton21 );
	UxCreateWidget( arrowButton23 );
	UxCreateWidget( arrowButton24 );
	UxCreateWidget( text24 );
	UxPutTopOffset( rowColumn31, 125 );
	UxPutTopAttachment( rowColumn31, "attach_form" );
	UxPutLeftWidget( rowColumn31, "label15" );
	UxPutLeftOffset( rowColumn31, 0 );
	UxPutLeftAttachment( rowColumn31, "attach_widget" );
	UxCreateWidget( rowColumn31 );

	UxCreateWidget( text15 );
	UxCreateWidget( rowColumn32 );
	UxCreateWidget( arrowButton25 );
	UxCreateWidget( arrowButton26 );
	UxPutTopOffset( label28, 170 );
	UxPutTopAttachment( label28, "attach_form" );
	UxPutLeftOffset( label28, 275 );
	UxPutLeftAttachment( label28, "attach_form" );
	UxCreateWidget( label28 );

	UxPutTopOffset( rowColumn33, 160 );
	UxPutTopAttachment( rowColumn33, "attach_form" );
	UxPutLeftWidget( rowColumn33, "label28" );
	UxPutLeftOffset( rowColumn33, 0 );
	UxPutLeftAttachment( rowColumn33, "attach_widget" );
	UxCreateWidget( rowColumn33 );

	UxCreateWidget( text22 );
	UxCreateWidget( rowColumn34 );
	UxCreateWidget( arrowButton27 );
	UxCreateWidget( arrowButton28 );
	UxPutTopOffset( label35, 215 );
	UxPutTopAttachment( label35, "attach_form" );
	UxPutLeftOffset( label35, 5 );
	UxPutLeftAttachment( label35, "attach_form" );
	UxCreateWidget( label35 );

	UxPutLeftWidget( toggleButton1, "label35" );
	UxPutLeftOffset( toggleButton1, 5 );
	UxPutLeftAttachment( toggleButton1, "attach_widget" );
	UxPutTopOffset( toggleButton1, 203 );
	UxPutTopAttachment( toggleButton1, "attach_form" );
	UxCreateWidget( toggleButton1 );

	UxPutTopOffset( label36, 205 );
	UxPutTopAttachment( label36, "attach_form" );
	UxPutLeftOffset( label36, 274 );
	UxPutLeftAttachment( label36, "attach_form" );
	UxCreateWidget( label36 );

	UxPutTopOffset( text25, 201 );
	UxPutTopAttachment( text25, "attach_form" );
	UxPutLeftWidget( text25, "label36" );
	UxPutLeftOffset( text25, 3 );
	UxPutLeftAttachment( text25, "attach_widget" );
	UxCreateWidget( text25 );

	UxPutTopOffset( label10, 170 );
	UxPutTopAttachment( label10, "attach_form" );
	UxPutLeftOffset( label10, 5 );
	UxPutLeftAttachment( label10, "attach_form" );
	UxCreateWidget( label10 );

	UxPutTopOffset( rowColumn17, 167 );
	UxPutTopAttachment( rowColumn17, "attach_form" );
	UxPutLeftWidget( rowColumn17, "label10" );
	UxPutLeftOffset( rowColumn17, 0 );
	UxPutLeftAttachment( rowColumn17, "attach_widget" );
	UxCreateWidget( rowColumn17 );

	UxCreateWidget( text11 );
	UxCreateWidget( text12 );
	UxPutTopOffset( rowColumn29, 125 );
	UxPutTopAttachment( rowColumn29, "attach_form" );
	UxPutLeftWidget( rowColumn29, "label24" );
	UxPutLeftOffset( rowColumn29, 0 );
	UxPutLeftAttachment( rowColumn29, "attach_widget" );
	UxCreateWidget( rowColumn29 );

	UxCreateWidget( rowColumn30 );
	UxCreateWidget( arrowButton39 );
	UxCreateWidget( arrowButton40 );
	UxCreateWidget( text39 );
	UxPutTopOffset( label52, 135 );
	UxPutTopAttachment( label52, "attach_form" );
	UxPutLeftOffset( label52, 135 );
	UxPutLeftAttachment( label52, "attach_form" );
	UxCreateWidget( label52 );

	UxPutTopOffset( rowColumn44, 125 );
	UxPutTopAttachment( rowColumn44, "attach_form" );
	UxPutLeftWidget( rowColumn44, "label52" );
	UxPutLeftOffset( rowColumn44, 0 );
	UxPutLeftAttachment( rowColumn44, "attach_widget" );
	UxCreateWidget( rowColumn44 );

	UxCreateWidget( rowColumn45 );
	UxCreateWidget( arrowButton45 );
	UxCreateWidget( arrowButton46 );
	UxCreateWidget( text40 );
	UxPutTopOffset( rowColumn22, 38 );
	UxPutTopAttachment( rowColumn22, "attach_form" );
	UxPutLeftWidget( rowColumn22, "label32" );
	UxPutLeftOffset( rowColumn22, 10 );
	UxPutLeftAttachment( rowColumn22, "attach_widget" );
	UxCreateWidget( rowColumn22 );

	UxCreateWidget( rowColumn28 );
	UxCreateWidget( arrowButton47 );
	UxCreateWidget( arrowButton48 );
	UxCreateWidget( text41 );
	UxPutTopOffset( label53, 200 );
	UxPutTopAttachment( label53, "attach_form" );
	UxPutLeftOffset( label53, 10 );
	UxPutLeftAttachment( label53, "attach_form" );
	UxCreateWidget( label53 );

	UxPutTopWidget( SHelp, "form2" );
	UxPutTopPosition( SHelp, 0 );
	UxPutTopOffset( SHelp, 6 );
	UxPutTopAttachment( SHelp, "attach_widget" );
	UxPutRightOffset( SHelp, 10 );
	UxPutRightAttachment( SHelp, "attach_form" );
	UxPutLeftOffset( SHelp, 10 );
	UxPutLeftAttachment( SHelp, "attach_form" );
	UxPutBottomOffset( SHelp, 0 );
	UxPutBottomAttachment( SHelp, "attach_none" );
	UxCreateWidget( SHelp );

	UxPutTopWidget( pushButton1, "SHelp" );
	UxPutTopOffset( pushButton1, 10 );
	UxPutTopAttachment( pushButton1, "attach_widget" );
	UxPutRightOffset( pushButton1, 15 );
	UxPutRightAttachment( pushButton1, "attach_form" );
	UxPutBottomOffset( pushButton1, 110 );
	UxPutBottomAttachment( pushButton1, "attach_none" );
	UxCreateWidget( pushButton1 );

	UxPutTopOffset( form4, 4 );
	UxPutTopAttachment( form4, "attach_form" );
	UxPutRightOffset( form4, 4 );
	UxPutRightAttachment( form4, "attach_form" );
	UxPutLeftOffset( form4, 4 );
	UxPutLeftAttachment( form4, "attach_form" );
	UxCreateWidget( form4 );
	createCB_form4( UxGetWidget( form4 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label2, 20 );
	UxPutTopAttachment( label2, "attach_form" );
	UxPutLeftOffset( label2, 5 );
	UxPutLeftAttachment( label2, "attach_form" );
	UxCreateWidget( label2 );

	UxPutLeftWidget( text9, "label2" );
	UxPutRightOffset( text9, 60 );
	UxPutRightAttachment( text9, "attach_form" );
	UxPutLeftOffset( text9, 5 );
	UxPutLeftAttachment( text9, "attach_widget" );
	UxPutTopOffset( text9, 20 );
	UxPutTopAttachment( text9, "attach_form" );
	UxCreateWidget( text9 );

	UxPutTopWidget( label1, "text9" );
	UxPutTopOffset( label1, 65 );
	UxPutTopAttachment( label1, "attach_form" );
	UxPutLeftOffset( label1, 5 );
	UxPutLeftAttachment( label1, "attach_form" );
	UxCreateWidget( label1 );

	UxPutTopOffset( toggleButton10, 60 );
	UxPutTopAttachment( toggleButton10, "attach_form" );
	UxPutLeftWidget( toggleButton10, "label1" );
	UxPutLeftOffset( toggleButton10, 10 );
	UxPutLeftAttachment( toggleButton10, "attach_widget" );
	UxCreateWidget( toggleButton10 );

	UxPutTopOffset( label26, 140 );
	UxPutTopAttachment( label26, "attach_form" );
	UxPutLeftOffset( label26, 5 );
	UxPutLeftAttachment( label26, "attach_form" );
	UxCreateWidget( label26 );

	UxPutTopOffset( text23, 140 );
	UxPutTopAttachment( text23, "attach_form" );
	UxPutRightOffset( text23, 60 );
	UxPutRightAttachment( text23, "attach_form" );
	UxPutLeftWidget( text23, "label26" );
	UxPutLeftOffset( text23, 5 );
	UxPutLeftAttachment( text23, "attach_widget" );
	UxCreateWidget( text23 );

	UxPutLeftOffset( form3, 4 );
	UxPutLeftAttachment( form3, "attach_form" );
	UxPutRightOffset( form3, 4 );
	UxPutRightAttachment( form3, "attach_form" );
	UxPutTopOffset( form3, 4 );
	UxPutTopAttachment( form3, "attach_form" );
	UxCreateWidget( form3 );
	createCB_form3( UxGetWidget( form3 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label9, 10 );
	UxPutTopAttachment( label9, "attach_form" );
	UxPutLeftOffset( label9, 5 );
	UxPutLeftAttachment( label9, "attach_form" );
	UxCreateWidget( label9 );

	UxPutLeftWidget( text10, "label9" );
	UxPutTopOffset( text10, 5 );
	UxPutTopAttachment( text10, "attach_form" );
	UxPutRightOffset( text10, 60 );
	UxPutRightAttachment( text10, "attach_form" );
	UxPutLeftOffset( text10, 18 );
	UxPutLeftAttachment( text10, "attach_widget" );
	UxCreateWidget( text10 );

	UxPutTopWidget( label13, "text10" );
	UxPutTopOffset( label13, 145 );
	UxPutTopAttachment( label13, "attach_form" );
	UxPutLeftOffset( label13, 5 );
	UxPutLeftAttachment( label13, "attach_form" );
	UxCreateWidget( label13 );

	UxPutTopWidget( text17, "text10" );
	UxPutTopOffset( text17, 140 );
	UxPutTopAttachment( text17, "attach_form" );
	UxPutRightOffset( text17, 60 );
	UxPutRightAttachment( text17, "attach_form" );
	UxPutLeftWidget( text17, "label13" );
	UxPutLeftOffset( text17, 18 );
	UxPutLeftAttachment( text17, "attach_widget" );
	UxCreateWidget( text17 );

	UxPutTopWidget( label11, "label13" );
	UxPutTopOffset( label11, 55 );
	UxPutTopAttachment( label11, "attach_form" );
	UxPutLeftOffset( label11, 5 );
	UxPutLeftAttachment( label11, "attach_form" );
	UxCreateWidget( label11 );

	UxPutTopWidget( label12, "label11" );
	UxPutTopOffset( label12, 100 );
	UxPutTopAttachment( label12, "attach_form" );
	UxPutLeftOffset( label12, 5 );
	UxPutLeftAttachment( label12, "attach_form" );
	UxCreateWidget( label12 );

	UxPutTopOffset( toggleButton2, 50 );
	UxPutTopAttachment( toggleButton2, "attach_form" );
	UxPutLeftWidget( toggleButton2, "label13" );
	UxPutLeftOffset( toggleButton2, 20 );
	UxPutLeftAttachment( toggleButton2, "attach_widget" );
	UxCreateWidget( toggleButton2 );

	UxPutTopOffset( toggleButton4, 95 );
	UxPutTopAttachment( toggleButton4, "attach_form" );
	UxPutLeftWidget( toggleButton4, "label12" );
	UxPutLeftOffset( toggleButton4, 20 );
	UxPutLeftAttachment( toggleButton4, "attach_widget" );
	UxCreateWidget( toggleButton4 );

	UxPutTopOffset( form6, 4 );
	UxPutTopAttachment( form6, "attach_form" );
	UxPutRightOffset( form6, 4 );
	UxPutRightAttachment( form6, "attach_form" );
	UxPutLeftOffset( form6, 4 );
	UxPutLeftAttachment( form6, "attach_form" );
	UxCreateWidget( form6 );
	createCB_form6( UxGetWidget( form6 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label16, 10 );
	UxPutTopAttachment( label16, "attach_form" );
	UxPutLeftOffset( label16, 10 );
	UxPutLeftAttachment( label16, "attach_form" );
	UxCreateWidget( label16 );

	UxPutTopWidget( label17, "label16" );
	UxPutTopOffset( label17, 50 );
	UxPutTopAttachment( label17, "attach_form" );
	UxPutLeftOffset( label17, 10 );
	UxPutLeftAttachment( label17, "attach_form" );
	UxCreateWidget( label17 );

	UxPutTopWidget( label5, "label17" );
	UxPutTopOffset( label5, 90 );
	UxPutTopAttachment( label5, "attach_form" );
	UxPutLeftOffset( label5, 10 );
	UxPutLeftAttachment( label5, "attach_form" );
	UxCreateWidget( label5 );

	UxPutTopWidget( label19, "label5" );
	UxPutTopOffset( label19, 210 );
	UxPutTopAttachment( label19, "attach_form" );
	UxPutLeftOffset( label19, 7 );
	UxPutLeftAttachment( label19, "attach_form" );
	UxCreateWidget( label19 );

	UxPutTopWidget( label20, "label19" );
	UxPutTopOffset( label20, 128 );
	UxPutTopAttachment( label20, "attach_form" );
	UxPutLeftOffset( label20, 10 );
	UxPutLeftAttachment( label20, "attach_form" );
	UxCreateWidget( label20 );

	UxPutTopWidget( label21, "label20" );
	UxPutTopOffset( label21, 167 );
	UxPutTopAttachment( label21, "attach_form" );
	UxPutLeftOffset( label21, 10 );
	UxPutLeftAttachment( label21, "attach_form" );
	UxCreateWidget( label21 );

	UxPutTopOffset( text21, 162 );
	UxPutTopAttachment( text21, "attach_form" );
	UxPutRightOffset( text21, 60 );
	UxPutRightAttachment( text21, "attach_form" );
	UxPutLeftOffset( text21, 9 );
	UxPutLeftWidget( text21, "label21" );
	UxPutLeftAttachment( text21, "attach_widget" );
	UxCreateWidget( text21 );

	UxPutTopOffset( rowColumn1, 0 );
	UxPutResizable( rowColumn1, "false" );
	UxPutTopAttachment( rowColumn1, "attach_form" );
	UxPutLeftWidget( rowColumn1, "label16" );
	UxPutLeftOffset( rowColumn1, 3 );
	UxPutLeftAttachment( rowColumn1, "attach_widget" );
	UxCreateWidget( rowColumn1 );

	UxCreateWidget( rowColumn9 );
	UxCreateWidget( arrowButton7 );
	UxCreateWidget( arrowButton8 );
	UxCreateWidget( text1 );
	UxPutTopOffset( rowColumn2, 40 );
	UxPutTopAttachment( rowColumn2, "attach_form" );
	UxPutLeftWidget( rowColumn2, "label17" );
	UxPutLeftOffset( rowColumn2, 5 );
	UxPutLeftAttachment( rowColumn2, "attach_widget" );
	UxCreateWidget( rowColumn2 );

	UxCreateWidget( rowColumn10 );
	UxCreateWidget( arrowButton3 );
	UxCreateWidget( arrowButton4 );
	UxCreateWidget( text4 );
	UxPutTopOffset( label18, 50 );
	UxPutTopAttachment( label18, "attach_form" );
	UxPutLeftOffset( label18, 210 );
	UxPutLeftAttachment( label18, "attach_form" );
	UxCreateWidget( label18 );

	UxPutTopOffset( rowColumn3, 40 );
	UxPutTopAttachment( rowColumn3, "attach_form" );
	UxPutLeftWidget( rowColumn3, "label18" );
	UxPutLeftOffset( rowColumn3, 3 );
	UxPutLeftAttachment( rowColumn3, "attach_widget" );
	UxCreateWidget( rowColumn3 );

	UxCreateWidget( rowColumn11 );
	UxCreateWidget( arrowButton9 );
	UxCreateWidget( arrowButton10 );
	UxCreateWidget( text5 );
	UxPutTopOffset( rowColumn5, 80 );
	UxPutTopAttachment( rowColumn5, "attach_form" );
	UxPutLeftWidget( rowColumn5, "label5" );
	UxPutLeftOffset( rowColumn5, 3 );
	UxPutLeftAttachment( rowColumn5, "attach_widget" );
	UxCreateWidget( rowColumn5 );

	UxCreateWidget( rowColumn12 );
	UxCreateWidget( arrowButton11 );
	UxCreateWidget( arrowButton12 );
	UxCreateWidget( text6 );
	UxPutTopOffset( label22, 90 );
	UxPutTopAttachment( label22, "attach_form" );
	UxPutLeftOffset( label22, 210 );
	UxPutLeftAttachment( label22, "attach_form" );
	UxCreateWidget( label22 );

	UxPutTopOffset( rowColumn4, 80 );
	UxPutTopAttachment( rowColumn4, "attach_form" );
	UxPutLeftWidget( rowColumn4, "label22" );
	UxPutLeftOffset( rowColumn4, 3 );
	UxPutLeftAttachment( rowColumn4, "attach_widget" );
	UxCreateWidget( rowColumn4 );

	UxCreateWidget( rowColumn14 );
	UxCreateWidget( arrowButton17 );
	UxCreateWidget( arrowButton18 );
	UxCreateWidget( text8 );
	UxPutTopOffset( toggleButton25, 207 );
	UxPutTopAttachment( toggleButton25, "attach_form" );
	UxPutLeftWidget( toggleButton25, "label19" );
	UxPutLeftOffset( toggleButton25, 10 );
	UxPutLeftAttachment( toggleButton25, "attach_widget" );
	UxCreateWidget( toggleButton25 );

	UxPutTopOffset( rowColumn6, 123 );
	UxPutTopAttachment( rowColumn6, "attach_form" );
	UxPutLeftWidget( rowColumn6, "label20" );
	UxPutLeftOffset( rowColumn6, 3 );
	UxPutLeftAttachment( rowColumn6, "attach_widget" );
	UxCreateWidget( rowColumn6 );

	UxCreateWidget( rowColumn7 );
	UxCreateWidget( arrowButton13 );
	UxCreateWidget( arrowButton14 );
	UxCreateWidget( text13 );
	UxPutTopOffset( label54, 10 );
	UxPutTopAttachment( label54, "attach_form" );
	UxPutLeftOffset( label54, 210 );
	UxPutLeftAttachment( label54, "attach_form" );
	UxCreateWidget( label54 );

	UxPutLeftWidget( toggleButton15, "label54" );
	UxPutLeftOffset( toggleButton15, 9 );
	UxPutLeftAttachment( toggleButton15, "attach_widget" );
	UxPutTopOffset( toggleButton15, 8 );
	UxPutTopAttachment( toggleButton15, "attach_form" );
	UxCreateWidget( toggleButton15 );

	UxPutTopOffset( form7, 4 );
	UxPutTopAttachment( form7, "attach_form" );
	UxPutRightOffset( form7, 4 );
	UxPutRightAttachment( form7, "attach_form" );
	UxPutLeftOffset( form7, 4 );
	UxPutLeftAttachment( form7, "attach_form" );
	UxCreateWidget( form7 );
	createCB_form7( UxGetWidget( form7 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label3, 10 );
	UxPutTopAttachment( label3, "attach_form" );
	UxPutLeftOffset( label3, 5 );
	UxPutLeftAttachment( label3, "attach_form" );
	UxCreateWidget( label3 );

	UxPutRightOffset( text3, 60 );
	UxPutRightAttachment( text3, "attach_form" );
	UxPutLeftWidget( text3, "label3" );
	UxPutTopOffset( text3, 5 );
	UxPutTopAttachment( text3, "attach_form" );
	UxPutLeftOffset( text3, 13 );
	UxPutLeftAttachment( text3, "attach_widget" );
	UxCreateWidget( text3 );

	UxPutTopWidget( label7, "text3" );
	UxPutTopOffset( label7, 50 );
	UxPutTopAttachment( label7, "attach_form" );
	UxPutLeftOffset( label7, 5 );
	UxPutLeftAttachment( label7, "attach_form" );
	UxCreateWidget( label7 );

	UxPutTopWidget( label8, "label7" );
	UxPutTopOffset( label8, 95 );
	UxPutTopAttachment( label8, "attach_form" );
	UxPutLeftOffset( label8, 5 );
	UxPutLeftAttachment( label8, "attach_form" );
	UxCreateWidget( label8 );

	UxPutTopWidget( label4, "label8" );
	UxPutTopOffset( label4, 140 );
	UxPutTopAttachment( label4, "attach_form" );
	UxPutLeftOffset( label4, 5 );
	UxPutLeftAttachment( label4, "attach_form" );
	UxCreateWidget( label4 );

	UxPutTopOffset( toggleButton8, 135 );
	UxPutTopAttachment( toggleButton8, "attach_form" );
	UxPutLeftWidget( toggleButton8, "label4" );
	UxPutLeftOffset( toggleButton8, 15 );
	UxPutLeftAttachment( toggleButton8, "attach_widget" );
	UxCreateWidget( toggleButton8 );

	UxPutTopOffset( toggleButton9, 90 );
	UxPutTopAttachment( toggleButton9, "attach_form" );
	UxPutLeftWidget( toggleButton9, "label8" );
	UxPutLeftOffset( toggleButton9, 15 );
	UxPutLeftAttachment( toggleButton9, "attach_widget" );
	UxCreateWidget( toggleButton9 );

	UxPutTopOffset( toggleButton14, 45 );
	UxPutTopAttachment( toggleButton14, "attach_form" );
	UxPutLeftWidget( toggleButton14, "label7" );
	UxPutLeftOffset( toggleButton14, 15 );
	UxPutLeftAttachment( toggleButton14, "attach_widget" );
	UxCreateWidget( toggleButton14 );

	UxPutTopWidget( pushButton6, "SHelp" );
	UxPutTopOffset( pushButton6, 5 );
	UxPutTopAttachment( pushButton6, "attach_widget" );
	UxPutLeftOffset( pushButton6, 10 );
	UxPutLeftAttachment( pushButton6, "attach_form" );
	UxPutBottomWidget( pushButton6, "" );
	UxPutBottomOffset( pushButton6, 5 );
	UxPutBottomAttachment( pushButton6, "attach_none" );
	UxCreateWidget( pushButton6 );

	UxPutTopOffset( form1, 4 );
	UxPutTopAttachment( form1, "attach_form" );
	UxPutRightOffset( form1, 4 );
	UxPutRightAttachment( form1, "attach_form" );
	UxPutLeftOffset( form1, 4 );
	UxPutLeftAttachment( form1, "attach_form" );
	UxCreateWidget( form1 );
	createCB_form1( UxGetWidget( form1 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label6, 15 );
	UxPutTopAttachment( label6, "attach_form" );
	UxPutLeftOffset( label6, 5 );
	UxPutLeftAttachment( label6, "attach_form" );
	UxCreateWidget( label6 );

	UxPutLeftWidget( text7, "label6" );
	UxPutTopOffset( text7, 10 );
	UxPutTopAttachment( text7, "attach_form" );
	UxPutRightOffset( text7, 60 );
	UxPutRightAttachment( text7, "attach_form" );
	UxPutLeftOffset( text7, 16 );
	UxPutLeftAttachment( text7, "attach_widget" );
	UxCreateWidget( text7 );

	UxPutTopWidget( label31, "text7" );
	UxPutTopOffset( label31, 55 );
	UxPutTopAttachment( label31, "attach_form" );
	UxPutLeftOffset( label31, 5 );
	UxPutLeftAttachment( label31, "attach_form" );
	UxCreateWidget( label31 );

	UxCreateWidget( plotflag_p1 );
	UxCreateWidget( plotflag_p1_b1 );
	UxCreateWidget( plotflag_p1_b2 );
	UxCreateWidget( plotflag_p1_b3 );
	UxPutTopWidget( plotflag, "text7" );
	UxPutTopOffset( plotflag, 50 );
	UxPutTopAttachment( plotflag, "attach_form" );
	UxPutLeftWidget( plotflag, "label31" );
	UxPutLeftOffset( plotflag, 20 );
	UxPutLeftAttachment( plotflag, "attach_widget" );
	UxPutSubMenuId( plotflag, "plotflag_p1" );
	UxCreateWidget( plotflag );

	UxPutTopWidget( label29, "label31" );
	UxPutTopOffset( label29, 103 );
	UxPutTopAttachment( label29, "attach_form" );
	UxPutLeftOffset( label29, 5 );
	UxPutLeftAttachment( label29, "attach_form" );
	UxCreateWidget( label29 );

	UxPutTopWidget( label30, "label29" );
	UxPutTopOffset( label30, 150 );
	UxPutTopAttachment( label30, "attach_form" );
	UxPutLeftOffset( label30, 5 );
	UxPutLeftAttachment( label30, "attach_form" );
	UxCreateWidget( label30 );

	UxPutTopOffset( text14, 145 );
	UxPutTopAttachment( text14, "attach_form" );
	UxPutLeftWidget( text14, "label30" );
	UxPutLeftOffset( text14, 16 );
	UxPutLeftAttachment( text14, "attach_widget" );
	UxCreateWidget( text14 );

	UxPutTopOffset( toggleButton6, 100 );
	UxPutTopAttachment( toggleButton6, "attach_form" );
	UxPutLeftWidget( toggleButton6, "label29" );
	UxPutLeftOffset( toggleButton6, 20 );
	UxPutLeftAttachment( toggleButton6, "attach_widget" );
	UxCreateWidget( toggleButton6 );

	UxPutTopOffset( form8, 5 );
	UxPutTopAttachment( form8, "attach_form" );
	UxPutRightOffset( form8, 4 );
	UxPutRightAttachment( form8, "attach_form" );
	UxPutLeftOffset( form8, 4 );
	UxPutLeftAttachment( form8, "attach_form" );
	UxCreateWidget( form8 );
	createCB_form8( UxGetWidget( form8 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label39, 25 );
	UxPutTopAttachment( label39, "attach_form" );
	UxPutLeftOffset( label39, 10 );
	UxPutLeftAttachment( label39, "attach_form" );
	UxCreateWidget( label39 );

	UxPutTopWidget( label40, "label39" );
	UxPutTopOffset( label40, 70 );
	UxPutTopAttachment( label40, "attach_form" );
	UxPutLeftOffset( label40, 10 );
	UxPutLeftAttachment( label40, "attach_form" );
	UxCreateWidget( label40 );

	UxCreateWidget( method_p3 );
	UxCreateWidget( method_p1_b9 );
	UxCreateWidget( method_p1_b10 );
	UxCreateWidget( method_p1_b11 );
	UxCreateWidget( method_p1_b12 );
	UxPutTopOffset( method2, 20 );
	UxPutTopAttachment( method2, "attach_form" );
	UxPutLeftWidget( method2, "label39" );
	UxPutLeftOffset( method2, 20 );
	UxPutLeftAttachment( method2, "attach_widget" );
	UxPutSubMenuId( method2, "method_p3" );
	UxCreateWidget( method2 );

	UxCreateWidget( color_p2 );
	UxCreateWidget( color_p1_b12 );
	UxCreateWidget( color_p1_b13 );
	UxCreateWidget( color_p1_b14 );
	UxCreateWidget( color_p1_b15 );
	UxCreateWidget( color_p1_b16 );
	UxCreateWidget( color_p1_b17 );
	UxCreateWidget( color_p1_b18 );
	UxCreateWidget( color_p1_b19 );
	UxCreateWidget( color_p1_b20 );
	UxCreateWidget( color_p1_b21 );
	UxCreateWidget( color_p1_b22 );
	UxPutTopWidget( color1, "method2" );
	UxPutTopOffset( color1, 15 );
	UxPutTopAttachment( color1, "attach_widget" );
	UxPutLeftWidget( color1, "label40" );
	UxPutLeftOffset( color1, 20 );
	UxPutLeftAttachment( color1, "attach_widget" );
	UxPutSubMenuId( color1, "color_p2" );
	UxCreateWidget( color1 );

	UxPutTopOffset( separator1, 110 );
	UxPutTopAttachment( separator1, "attach_form" );
	UxPutRightOffset( separator1, 5 );
	UxPutRightAttachment( separator1, "attach_form" );
	UxPutLeftOffset( separator1, 5 );
	UxPutLeftAttachment( separator1, "attach_form" );
	UxCreateWidget( separator1 );

	UxPutTopOffset( label23, 140 );
	UxPutTopAttachment( label23, "attach_form" );
	UxPutLeftOffset( label23, 10 );
	UxPutLeftAttachment( label23, "attach_form" );
	UxCreateWidget( label23 );

	UxCreateWidget( method_p1 );
	UxCreateWidget( method_p1_b1 );
	UxCreateWidget( method_p1_b2 );
	UxCreateWidget( method_p1_b3 );
	UxCreateWidget( method_p1_b4 );
	UxPutTopOffset( method1, 135 );
	UxPutTopAttachment( method1, "attach_form" );
	UxPutLeftWidget( method1, "label23" );
	UxPutLeftOffset( method1, 20 );
	UxPutLeftAttachment( method1, "attach_widget" );
	UxPutSubMenuId( method1, "method_p1" );
	UxCreateWidget( method1 );

	UxPutTopOffset( label33, 185 );
	UxPutTopAttachment( label33, "attach_form" );
	UxPutLeftOffset( label33, 5 );
	UxPutLeftAttachment( label33, "attach_form" );
	UxCreateWidget( label33 );

	UxPutTopOffset( rowColumn19, 175 );
	UxPutTopAttachment( rowColumn19, "attach_form" );
	UxPutLeftWidget( rowColumn19, "label33" );
	UxPutLeftOffset( rowColumn19, 20 );
	UxPutLeftAttachment( rowColumn19, "attach_widget" );
	UxCreateWidget( rowColumn19 );

	UxCreateWidget( rowColumn21 );
	UxCreateWidget( arrowButton1 );
	UxCreateWidget( arrowButton2 );
	UxCreateWidget( text27 );
	UxPutTopOffset( toggleButton3, 40 );
	UxPutTopAttachment( toggleButton3, "attach_form" );
	UxPutRightOffset( toggleButton3, 80 );
	UxPutRightAttachment( toggleButton3, "attach_form" );
	UxCreateWidget( toggleButton3 );

	UxPutTopPosition( toggleButton5, 00 );
	UxPutTopOffset( toggleButton5, 150 );
	UxPutTopAttachment( toggleButton5, "attach_form" );
	UxPutRightOffset( toggleButton5, 80 );
	UxPutRightAttachment( toggleButton5, "attach_form" );
	UxCreateWidget( toggleButton5 );

	UxPutTopWidget( pushButton2, "pushButton6" );
	UxPutTopAttachment( pushButton2, "attach_widget" );
	UxPutTopOffset( pushButton2, 5 );
	UxPutBottomWidget( pushButton2, "" );
	UxPutLeftOffset( pushButton2, 10 );
	UxPutLeftAttachment( pushButton2, "attach_form" );
	UxPutBottomOffset( pushButton2, 5 );
	UxPutBottomAttachment( pushButton2, "attach_none" );
	UxCreateWidget( pushButton2 );

	UxPutTopWidget( pushButton9, "SHelp" );
	UxPutTopOffset( pushButton9, 5 );
	UxPutTopAttachment( pushButton9, "attach_widget" );
	UxPutBottomWidget( pushButton9, "" );
	UxPutBottomOffset( pushButton9, 5 );
	UxPutBottomAttachment( pushButton9, "attach_none" );
	UxPutLeftWidget( pushButton9, "pushButton6" );
	UxPutLeftOffset( pushButton9, 10 );
	UxPutLeftAttachment( pushButton9, "attach_widget" );
	UxCreateWidget( pushButton9 );
	createCB_pushButton9( UxGetWidget( pushButton9 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopWidget( pushButton4, "pushButton9" );
	UxPutTopOffset( pushButton4, 5 );
	UxPutTopAttachment( pushButton4, "attach_widget" );
	UxPutLeftOffset( pushButton4, 10 );
	UxPutLeftWidget( pushButton4, "pushButton2" );
	UxPutLeftAttachment( pushButton4, "attach_widget" );
	UxPutBottomWidget( pushButton4, "" );
	UxPutBottomOffset( pushButton4, 5 );
	UxPutBottomAttachment( pushButton4, "attach_none" );
	UxCreateWidget( pushButton4 );

	UxPutTopAttachment( pushButton7, "attach_widget" );
	UxPutTopWidget( pushButton7, "pushButton2" );
	UxPutTopOffset( pushButton7, 5 );
	UxPutLeftOffset( pushButton7, 10 );
	UxPutLeftAttachment( pushButton7, "attach_form" );
	UxPutBottomOffset( pushButton7, 10 );
	UxPutBottomAttachment( pushButton7, "attach_none" );
	UxCreateWidget( pushButton7 );

	UxPutTopWidget( pushButton3, "pushButton4" );
	UxPutTopOffset( pushButton3, 5 );
	UxPutTopAttachment( pushButton3, "attach_widget" );
	UxPutBottomOffset( pushButton3, 10 );
	UxPutBottomAttachment( pushButton3, "attach_none" );
	UxPutLeftOffset( pushButton3, 10 );
	UxPutLeftWidget( pushButton3, "pushButton7" );
	UxPutLeftAttachment( pushButton3, "attach_widget" );
	UxCreateWidget( pushButton3 );
	createCB_pushButton3( UxGetWidget( pushButton3 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopWidget( pushButton8, "SHelp" );
	UxPutTopOffset( pushButton8, 5 );
	UxPutTopAttachment( pushButton8, "attach_widget" );
	UxPutLeftOffset( pushButton8, 10 );
	UxPutLeftWidget( pushButton8, "pushButton3" );
	UxPutLeftAttachment( pushButton8, "attach_widget" );
	UxPutBottomOffset( pushButton8, 10 );
	UxPutBottomAttachment( pushButton8, "attach_none" );
	UxCreateWidget( pushButton8 );
	createCB_pushButton8( UxGetWidget( pushButton8 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopWidget( pushButton5, "pushButton8" );
	UxPutTopOffset( pushButton5, 5 );
	UxPutTopAttachment( pushButton5, "attach_widget" );
	UxPutLeftWidget( pushButton5, "pushButton3" );
	UxPutLeftOffset( pushButton5, 10 );
	UxPutLeftAttachment( pushButton5, "attach_widget" );
	UxCreateWidget( pushButton5 );

	UxPutRightOffset( form5, 4 );
	UxPutRightAttachment( form5, "attach_form" );
	UxPutLeftOffset( form5, 4 );
	UxPutLeftAttachment( form5, "attach_form" );
	UxPutTopOffset( form5, 4 );
	UxPutTopAttachment( form5, "attach_form" );
	UxCreateWidget( form5 );
	createCB_form5( UxGetWidget( form5 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( label37, 10 );
	UxPutTopAttachment( label37, "attach_form" );
	UxPutLeftOffset( label37, 5 );
	UxPutLeftAttachment( label37, "attach_form" );
	UxCreateWidget( label37 );

	UxPutLeftWidget( text28, "label37" );
	UxPutTopOffset( text28, 5 );
	UxPutTopAttachment( text28, "attach_form" );
	UxPutRightOffset( text28, 70 );
	UxPutRightAttachment( text28, "attach_form" );
	UxPutLeftOffset( text28, 3 );
	UxPutLeftAttachment( text28, "attach_widget" );
	UxCreateWidget( text28 );

	UxPutTopOffset( label41, 48 );
	UxPutTopAttachment( label41, "attach_form" );
	UxPutLeftOffset( label41, 5 );
	UxPutLeftAttachment( label41, "attach_form" );
	UxCreateWidget( label41 );

	UxPutTopOffset( rowColumn35, 38 );
	UxPutTopAttachment( rowColumn35, "attach_form" );
	UxPutLeftWidget( rowColumn35, "label41" );
	UxPutLeftOffset( rowColumn35, 2 );
	UxPutLeftAttachment( rowColumn35, "attach_widget" );
	UxCreateWidget( rowColumn35 );

	UxCreateWidget( rowColumn36 );
	UxCreateWidget( arrowButton35 );
	UxCreateWidget( arrowButton36 );
	UxCreateWidget( text31 );
	UxPutTopOffset( label45, 83 );
	UxPutTopAttachment( label45, "attach_form" );
	UxPutLeftOffset( label45, 275 );
	UxPutLeftAttachment( label45, "attach_form" );
	UxCreateWidget( label45 );

	UxPutTopOffset( rowColumn40, 73 );
	UxPutTopAttachment( rowColumn40, "attach_form" );
	UxPutLeftWidget( rowColumn40, "label45" );
	UxPutLeftOffset( rowColumn40, 0 );
	UxPutLeftAttachment( rowColumn40, "attach_widget" );
	UxCreateWidget( rowColumn40 );

	UxCreateWidget( text35 );
	UxCreateWidget( rowColumn41 );
	UxCreateWidget( arrowButton43 );
	UxCreateWidget( arrowButton44 );
	UxPutTopOffset( label46, 118 );
	UxPutTopAttachment( label46, "attach_form" );
	UxPutLeftOffset( label46, 5 );
	UxPutLeftAttachment( label46, "attach_form" );
	UxCreateWidget( label46 );

	UxPutLeftWidget( toggleButton7, "label46" );
	UxPutLeftOffset( toggleButton7, 5 );
	UxPutLeftAttachment( toggleButton7, "attach_widget" );
	UxPutTopOffset( toggleButton7, 115 );
	UxPutTopAttachment( toggleButton7, "attach_form" );
	UxCreateWidget( toggleButton7 );

	UxPutTopOffset( label47, 118 );
	UxPutTopAttachment( label47, "attach_form" );
	UxPutLeftOffset( label47, 275 );
	UxPutLeftAttachment( label47, "attach_form" );
	UxCreateWidget( label47 );

	UxPutTopOffset( text36, 115 );
	UxPutTopAttachment( text36, "attach_form" );
	UxPutLeftWidget( text36, "label47" );
	UxPutLeftOffset( text36, 3 );
	UxPutLeftAttachment( text36, "attach_widget" );
	UxCreateWidget( text36 );

	UxPutTopOffset( label48, 83 );
	UxPutTopAttachment( label48, "attach_form" );
	UxPutLeftOffset( label48, 5 );
	UxPutLeftAttachment( label48, "attach_form" );
	UxCreateWidget( label48 );

	UxPutTopOffset( rowColumn42, 80 );
	UxPutTopAttachment( rowColumn42, "attach_form" );
	UxPutLeftWidget( rowColumn42, "label48" );
	UxPutLeftOffset( rowColumn42, 0 );
	UxPutLeftAttachment( rowColumn42, "attach_widget" );
	UxCreateWidget( rowColumn42 );

	UxCreateWidget( text37 );
	UxCreateWidget( text38 );
	UxPutTopOffset( label38, 48 );
	UxPutTopAttachment( label38, "attach_form" );
	UxPutLeftOffset( label38, 275 );
	UxPutLeftAttachment( label38, "attach_form" );
	UxCreateWidget( label38 );

	UxPutTopOffset( rowColumn38, 38 );
	UxPutTopAttachment( rowColumn38, "attach_form" );
	UxPutLeftWidget( rowColumn38, "label38" );
	UxPutLeftOffset( rowColumn38, 0 );
	UxPutLeftAttachment( rowColumn38, "attach_widget" );
	UxCreateWidget( rowColumn38 );

	UxCreateWidget( text34 );
	UxCreateWidget( rowColumn39 );
	UxCreateWidget( arrowButton41 );
	UxCreateWidget( arrowButton42 );
	UxPutLeftOffset( label42, 5 );
	UxPutLeftAttachment( label42, "attach_form" );
	UxPutTopOffset( label42, 165 );
	UxPutTopAttachment( label42, "attach_form" );
	UxCreateWidget( label42 );

	UxPutRightOffset( text29, 60 );
	UxPutRightAttachment( text29, "attach_none" );
	UxPutTopOffset( text29, 160 );
	UxPutTopAttachment( text29, "attach_form" );
	UxPutLeftWidget( text29, "label42" );
	UxPutLeftOffset( text29, 5 );
	UxPutLeftAttachment( text29, "attach_widget" );
	UxCreateWidget( text29 );

	UxPutTopOffset( label43, 203 );
	UxPutTopAttachment( label43, "attach_form" );
	UxPutLeftOffset( label43, 5 );
	UxPutLeftAttachment( label43, "attach_form" );
	UxCreateWidget( label43 );

	UxPutTopPosition( toggleButton13, 0 );
	UxPutTopOffset( toggleButton13, 205 );
	UxPutTopAttachment( toggleButton13, "attach_form" );
	UxPutLeftWidget( toggleButton13, "label43" );
	UxPutLeftOffset( toggleButton13, 5 );
	UxPutLeftAttachment( toggleButton13, "attach_widget" );
	UxCreateWidget( toggleButton13 );

	UxPutTopOffset( label44, 208 );
	UxPutTopAttachment( label44, "attach_form" );
	UxPutLeftOffset( label44, 155 );
	UxPutLeftAttachment( label44, "attach_form" );
	UxCreateWidget( label44 );

	UxPutTopOffset( rowColumn23, 200 );
	UxPutTopAttachment( rowColumn23, "attach_form" );
	UxPutLeftWidget( rowColumn23, "label44" );
	UxPutLeftOffset( rowColumn23, 0 );
	UxPutLeftAttachment( rowColumn23, "attach_widget" );
	UxCreateWidget( rowColumn23 );

	UxCreateWidget( rowColumn24 );
	UxCreateWidget( arrowButton29 );
	UxCreateWidget( arrowButton30 );
	UxCreateWidget( text30 );
	UxPutTopOffset( label49, 208 );
	UxPutTopAttachment( label49, "attach_form" );
	UxPutLeftOffset( label49, 290 );
	UxPutLeftAttachment( label49, "attach_form" );
	UxCreateWidget( label49 );

	UxPutTopOffset( rowColumn25, 200 );
	UxPutTopAttachment( rowColumn25, "attach_form" );
	UxPutLeftWidget( rowColumn25, "label49" );
	UxPutLeftOffset( rowColumn25, 0 );
	UxPutLeftAttachment( rowColumn25, "attach_widget" );
	UxCreateWidget( rowColumn25 );

	UxCreateWidget( rowColumn26 );
	UxCreateWidget( arrowButton33 );
	UxCreateWidget( arrowButton34 );
	UxCreateWidget( text32 );
	UxPutTopOffset( label50, 220 );
	UxPutTopAttachment( label50, "attach_form" );
	UxPutLeftOffset( label50, 5 );
	UxPutLeftAttachment( label50, "attach_form" );
	UxCreateWidget( label50 );

	UxPutTopOffset( label51, 48 );
	UxPutTopAttachment( label51, "attach_form" );
	UxPutLeftOffset( label51, 135 );
	UxPutLeftAttachment( label51, "attach_form" );
	UxCreateWidget( label51 );

	UxPutTopOffset( rowColumn37, 38 );
	UxPutTopAttachment( rowColumn37, "attach_form" );
	UxPutLeftWidget( rowColumn37, "label51" );
	UxPutLeftOffset( rowColumn37, 2 );
	UxPutLeftAttachment( rowColumn37, "attach_widget" );
	UxCreateWidget( rowColumn37 );

	UxCreateWidget( rowColumn43 );
	UxCreateWidget( arrowButton37 );
	UxCreateWidget( arrowButton38 );
	UxCreateWidget( text33 );
	UxCreateWidget( XDMenus );
	UxCreateWidget( commandPane );
	UxCreateWidget( command_b2 );
	UxCreateWidget( command_b3 );
	UxCreateWidget( commandPane_b17 );
	UxCreateWidget( command_b9 );
	UxCreateWidget( command_b10 );
	UxCreateWidget( commandPane_b14 );
	UxCreateWidget( commandPane_b15 );
	UxCreateWidget( commandPane_b16 );
	UxPutSubMenuId( commandsCascade, "commandPane" );
	UxCreateWidget( commandsCascade );

	UxCreateWidget( LUTCommsPane );
	UxCreateWidget( LUTcomm_b1 );
	UxCreateWidget( LUTcomm_b2 );
	UxCreateWidget( LUTcomm_b3 );
	UxCreateWidget( LUTcomm_b4 );
	UxCreateWidget( LUTcomm_b5 );
	UxCreateWidget( LUTcomm_b6 );
	UxPutSubMenuId( LUTCommsCascade, "LUTCommsPane" );
	UxCreateWidget( LUTCommsCascade );

	UxCreateWidget( helpPane );
	UxCreateWidget( XDHelp );
	UxCreateWidget( InterfHelp );
	UxCreateWidget( EditHelp );
	UxCreateWidget( SelectHelp );
	UxPutSubMenuId( helpCascade, "helpPane" );
	UxCreateWidget( helpCascade );

	UxCreateWidget( quitPane );
	UxCreateWidget( quit_b1 );
	UxPutSubMenuId( quitCascade, "quitPane" );
	UxCreateWidget( quitCascade );


	UxAddCallback( ApplicWindow, XmNpopupCallback,
			popupCB_ApplicWindow,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( workArea, XmNmapCallback,
			mapCB_workArea,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( form2, XmNmapCallback,
			mapCB_form2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text15, XmNactivateCallback,
			activateCB_text15,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text22, XmNactivateCallback,
			activateCB_text22,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton1, XmNvalueChangedCallback,
			valueChangedCB_toggleButton1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text25, XmNactivateCallback,
			activateCB_text25,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text11, XmNactivateCallback,
			activateCB_text11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text12, XmNactivateCallback,
			activateCB_text12,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton1, XmNactivateCallback,
			activateCB_pushButton1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton10, XmNvalueChangedCallback,
			valueChangedCB_toggleButton10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( form3, XmNmapCallback,
			mapCB_form3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton2, XmNvalueChangedCallback,
			valueChangedCB_toggleButton2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton4, XmNvalueChangedCallback,
			valueChangedCB_toggleButton4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton25, XmNvalueChangedCallback,
			valueChangedCB_toggleButton25,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton15, XmNvalueChangedCallback,
			valueChangedCB_toggleButton15,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( form7, XmNmapCallback,
			mapCB_form7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton8, XmNvalueChangedCallback,
			valueChangedCB_toggleButton8,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton9, XmNvalueChangedCallback,
			valueChangedCB_toggleButton9,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton14, XmNvalueChangedCallback,
			valueChangedCB_toggleButton14,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton6, XmNactivateCallback,
			activateCB_pushButton6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( form1, XmNmapCallback,
			mapCB_form1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( plotflag_p1_b1, XmNactivateCallback,
			activateCB_plotflag_p1_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( plotflag_p1_b2, XmNactivateCallback,
			activateCB_plotflag_p1_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton6, XmNvalueChangedCallback,
			valueChangedCB_toggleButton6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p3, XmNentryCallback,
			entryCB_method_p3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b9, XmNactivateCallback,
			activateCB_method_p1_b9,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b10, XmNactivateCallback,
			activateCB_method_p1_b10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b11, XmNactivateCallback,
			activateCB_method_p1_b11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b12, XmNactivateCallback,
			activateCB_method_p1_b12,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method2, XmNentryCallback,
			entryCB_method2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p2, XmNentryCallback,
			entryCB_color_p2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b12, XmNactivateCallback,
			activateCB_color_p1_b12,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b13, XmNactivateCallback,
			activateCB_color_p1_b13,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b14, XmNactivateCallback,
			activateCB_color_p1_b14,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b15, XmNactivateCallback,
			activateCB_color_p1_b15,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b16, XmNactivateCallback,
			activateCB_color_p1_b16,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b17, XmNactivateCallback,
			activateCB_color_p1_b17,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b18, XmNactivateCallback,
			activateCB_color_p1_b18,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b19, XmNactivateCallback,
			activateCB_color_p1_b19,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b20, XmNactivateCallback,
			activateCB_color_p1_b20,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b21, XmNactivateCallback,
			activateCB_color_p1_b21,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( color_p1_b22, XmNactivateCallback,
			activateCB_color_p1_b22,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1, XmNentryCallback,
			entryCB_method_p1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b1, XmNactivateCallback,
			activateCB_method_p1_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b2, XmNactivateCallback,
			activateCB_method_p1_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b3, XmNactivateCallback,
			activateCB_method_p1_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method_p1_b4, XmNactivateCallback,
			activateCB_method_p1_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( method1, XmNentryCallback,
			entryCB_method1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton3, XmNvalueChangedCallback,
			valueChangedCB_toggleButton3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton5, XmNvalueChangedCallback,
			valueChangedCB_toggleButton5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton2, XmNactivateCallback,
			activateCB_pushButton2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton9, XmNactivateCallback,
			activateCB_pushButton9,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton4, XmNactivateCallback,
			activateCB_pushButton4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton7, XmNactivateCallback,
			activateCB_pushButton7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton3, XmNactivateCallback,
			activateCB_pushButton3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton8, XmNactivateCallback,
			activateCB_pushButton8,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pushButton5, XmNactivateCallback,
			activateCB_pushButton5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( form5, XmNmapCallback,
			mapCB_form5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text35, XmNactivateCallback,
			activateCB_text35,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton7, XmNvalueChangedCallback,
			valueChangedCB_toggleButton7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text36, XmNactivateCallback,
			activateCB_text36,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text37, XmNactivateCallback,
			activateCB_text37,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text38, XmNactivateCallback,
			activateCB_text38,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text34, XmNactivateCallback,
			activateCB_text34,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton13, XmNvalueChangedCallback,
			valueChangedCB_toggleButton13,
			(XtPointer) UxApplicWindowContext );

	UxPutMenuHelpWidget( XDMenus, "quitCascade" );

	UxAddCallback( command_b2, XmNactivateCallback,
			activateCB_command_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( command_b3, XmNactivateCallback,
			activateCB_command_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( commandPane_b17, XmNactivateCallback,
			activateCB_commandPane_b17,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( command_b9, XmNactivateCallback,
			activateCB_command_b9,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( command_b10, XmNactivateCallback,
			activateCB_command_b10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( commandPane_b14, XmNactivateCallback,
			activateCB_commandPane_b14,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( commandPane_b15, XmNactivateCallback,
			activateCB_commandPane_b15,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( commandPane_b16, XmNactivateCallback,
			activateCB_commandPane_b16,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( LUTcomm_b1, XmNactivateCallback,
			activateCB_LUTcomm_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( LUTcomm_b2, XmNactivateCallback,
			activateCB_LUTcomm_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( LUTcomm_b3, XmNactivateCallback,
			activateCB_LUTcomm_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( LUTcomm_b4, XmNactivateCallback,
			activateCB_LUTcomm_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( LUTcomm_b5, XmNactivateCallback,
			activateCB_LUTcomm_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( LUTcomm_b6, XmNactivateCallback,
			activateCB_LUTcomm_b6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( XDHelp, XmNactivateCallback,
			activateCB_XDHelp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( InterfHelp, XmNactivateCallback,
			activateCB_InterfHelp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( EditHelp, XmNactivateCallback,
			activateCB_EditHelp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( SelectHelp, XmNactivateCallback,
			activateCB_SelectHelp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( quit_b1, XmNactivateCallback,
			activateCB_quit_b1,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( mainWindow, XDMenus, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, workAreaForm );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_popup_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	rtrn = _Uxbuild_ApplicWindow();

	/*
	create_fileSelectionDialog(rtrn);
	create_exitDialog(rtrn);
	*/
	
	CreateWindowManagerProtocols(UxGetWidget(rtrn));
	CreateSessionManagerProtocols();
	
	UxPopupInterface(rtrn, no_grab);
	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	popup_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ArrowACT5", action_ArrowACT5 },
				{ "ArrowACT8", action_ArrowACT8 },
				{ "ArrowACT6", action_ArrowACT6 },
				{ "ArrowCHNG", action_ArrowCHNG },
				{ "ArrowACT2", action_ArrowACT2 },
				{ "FileSelectACT1", action_FileSelectACT1 },
				{ "ClearHelp", action_ClearHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "MapACTX", action_MapACTX },
				{ "MapACT", action_MapACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		UxLoadResources( "ApplicWindow.rf" );
		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_popup_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

