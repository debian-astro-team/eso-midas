/* @(#)Template.c	19.1 (ES0-DMD) 02/25/03 13:44:21 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*------------------------------------------------------------
 * This is the project main program file for Ux generated  
 * code. You may add application dependent source code
 * at the appropriate places. 
 * 			     
 * Do not modify the statements preceded by the dollar
 * sign ($), these statements will be replaced with
 * the appropriate source code when the main program is
 * automatically generated. 
 *			
 * $Date: 2003-06-04 14:49:20 $  		$Revision: 1.1.1.1 $ 
 *-----------------------------------------------------------*/

#ifdef XOPEN_CATALOG
#include <locale.h>
#endif

#include <UxLib.h>


/*----------------------------------------------
 * Insert application global declarations here
 *---------------------------------------------*/

#include <math.h>
#include <stdlib.h>
#include <midas_def.h>


char    mybuff[256];
int     MonitorPid;  /* MIDAS Process Id (first argument) */
char    session[3];         /* MIDAS Session Number (second argument) */

int  speedo = 0;
int  kick[5] = {0,0,0,0,0};			/* global flags we need */
int  menuact[8];

double  stepsize[2];
char  actdisp[8] = "--1";
char  dspinfo[4][8] = {"512","512","630","330"};
char  grainfo[4][8] = {"600","480","100","200"};
 
Widget  Widget_managed[2];	/* Id of the current layout */
swidget  pushbt[2];		/* connected pushbutton */
char    dir_specs[128];

struct  SAVE_STRUCT
        {
        int             act_display;
        };

struct  SAVE_STRUCT     SAVEBUF;


Widget  Button_insensitive; /* Id of the current insens. button */
Widget  FSel_id;            /* Id of the text field calling the 
                               File Selector */


#include <StdResources.h>

swidget  sw, swfs, swhelp, stxt, shtext;
Widget   wsens, filelist_widget, help_widget;
int  pushb;


main(argc,argv)
	int 	argc;
	char 	*argv[];
{


int econt, elog, edisp;


	/*-----------------------------------------------------------
	 * Declarations.
	 * The default identifier - mainIface will only be declared  
	 * if the interface function is global and of type swidget. 
	 * To change the identifier to a different name, modify the
	 * string mainIface in the file "main.dat".  If "mainIface"
	 * is declared, it will be used below where the return value 
	 * of  PJ_INTERFACE_FUNCTION_CALL will be assigned to it.
         *-----------------------------------------------------------*/ 

  	swidget mainIface;
        int fsmode = 0, vbmode = 0;

	/*--------------------------------
	 * Interface function declaration
	 *-------------------------------*/	

 	swidget popup_ApplicWindow();

	

	/*---------------------
	 * Initialize Program
	 *--------------------*/

#ifdef XOPEN_CATALOG
	setlocale(LC_ALL, "");
#endif

  	UxTopLevel = UxInitialize("Template", &argc, argv);

	/*------------------------------------------------------
	 * Insert initialization code for your application here
	 *-----------------------------------------------------*/

   strcpy(mybuff,argv[1]);

   if (argc < 2) 
      {
       printf("Syntax: app_demo  <Pid> \n");
       exit(1);
      }
   else 
      {
      MonitorPid = atoi(argv[1]);
      strcpy(session, argv[2]);
      session[2] = '\0';
      }


    /* Calls the routine SetResources to set global variables
       to user defined values */

    SetResources(argc,argv);
 

/*  open the MIDAS environment */

   SCSPRO("XDisplay");
   econt = 1; elog = 0; edisp = 0;	/* disable automatic abort */
   SCECNT("PUT",&econt,&elog,&edisp);
  
     {

	extern swidget create_file_list(), create_Helpwindow();

           swfs = create_file_list();

           swhelp = create_Helpwindow();


        }


	/*----------------------------------------------------------------
	 * Create and popup the first window of the interface.  The 	 
	 * return value can be used in the popdown or destroy functions.
	 * The swidget return value of  PJ_INTERFACE_FUNCTION_CALL will
	 * be assigned to "mainIface" from  PJ_INTERFACE_RETVAL_TYPE. 
	 *---------------------------------------------------------------*/

	mainIface = popup_ApplicWindow();

	UxPopupInterface(mainIface, no_grab);



	/*-------------------------
	 * Enter the event loop 
	 *------------------------*/

  	UxMainLoop();

}

