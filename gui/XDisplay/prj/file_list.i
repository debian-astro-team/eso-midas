! UIMX ascii 2.0 key: 741                                                       

*translation.table: Browse
*translation.parent: file_list
*translation.policy: augment
*translation.<Btn3Down>: ListBeginSelect()
*translation.<Btn3Up>: ListEndSelect()

*file_list.class: topLevelShell
*file_list.parent: NO_PARENT
*file_list.static: true
*file_list.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*file_list.ispecdecl:
*file_list.funcdecl: swidget create_file_list()\

*file_list.funcname: create_file_list
*file_list.funcdef: "swidget", "<create_file_list>(%)"
*file_list.icode:
*file_list.fcode: return(rtrn);\

*file_list.auxdecl:
*file_list.name: file_list
*file_list.x: 330
*file_list.y: 300
*file_list.width: 290
*file_list.height: 353
*file_list.iconName: "List selection"
*file_list.keyboardFocusPolicy: "pointer"
*file_list.background: TextBackground
*file_list.geometry: ""

*form12.class: form
*form12.parent: file_list
*form12.static: true
*form12.name: form12
*form12.resizePolicy: "resize_none"
*form12.unitType: "pixels"
*form12.x: 0
*form12.y: 0
*form12.width: 290
*form12.height: 240
*form12.background: ApplicBackground

*pushButton29.class: pushButton
*pushButton29.parent: form12
*pushButton29.static: true
*pushButton29.name: pushButton29
*pushButton29.x: 0
*pushButton29.y: 318
*pushButton29.width: 210
*pushButton29.height: 35
*pushButton29.background: ButtonBackground
*pushButton29.fontList: BoldTextFont
*pushButton29.labelString: "cancel"
*pushButton29.activateCallback: {\
extern swidget swfs;\
\
UxPopdownInterface(swfs);\
}
*pushButton29.foreground: CancelForeground
*pushButton29.rightAttachment: "attach_form"
*pushButton29.rightOffset: 10
*pushButton29.topAttachment: "attach_none"
*pushButton29.topOffset: 5
*pushButton29.bottomAttachment: "attach_form"
*pushButton29.bottomOffset: 5
*pushButton29.leftAttachment: "attach_form"
*pushButton29.leftOffset: 10

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form12
*scrolledWindow1.static: true
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "application_defined"
*scrolledWindow1.x: 0
*scrolledWindow1.y: 2
*scrolledWindow1.visualPolicy: "variable"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.shadowThickness: 0
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.background: ApplicBackground
*scrolledWindow1.height: 316
*scrolledWindow1.rightAttachment: "attach_form"
*scrolledWindow1.rightOffset: 2
*scrolledWindow1.leftAttachment: "attach_form"
*scrolledWindow1.leftOffset: 2
*scrolledWindow1.width: 270
*scrolledWindow1.bottomAttachment: "attach_form"
*scrolledWindow1.bottomOffset: 45
*scrolledWindow1.topAttachment: "attach_form"
*scrolledWindow1.topOffset: 0
*scrolledWindow1.translations: ""
*scrolledWindow1.foreground: TextBackground
*scrolledWindow1.scrolledWindowMarginHeight: 1
*scrolledWindow1.scrolledWindowMarginWidth: 1
*scrolledWindow1.spacing: 2

*ls_init.class: scrolledList
*ls_init.parent: scrolledWindow1
*ls_init.static: true
*ls_init.name: ls_init
*ls_init.width: 220
*ls_init.height: 184
*ls_init.scrollBarDisplayPolicy: "static"
*ls_init.listSizePolicy: "variable"
*ls_init.background: TextBackground
*ls_init.fontList: TextFont
*ls_init.visibleItemCount: 18
*ls_init.browseSelectionCallback: {\
\
/*  browselect.c' holds this Callback code */\
\
char *choice;\
char cbuf[80];\
\
\
XButtonEvent  *ev;\
XmListCallbackStruct *cbs;\
\
extern swidget swfs, stxt;\
\
\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
ev = (XButtonEvent *) cbs->event;	/* test originating event */\
\
\
UxPutValue(stxt,choice);\
XtFree(choice);\
\
UxPopdownInterface(swfs);\
\
utila(0);		/* if LOAD/IMAGE, get descriptors */\
\
if (ev->button == 3) applycom(0);\
\
}
*ls_init.createCallback: {\
extern Widget filelist_widget;\
\
filelist_widget = UxWidget;\
}
*ls_init.foreground: TextForeground
*ls_init.translations: Browse
*ls_init.highlightThickness: 0
*ls_init.listMarginHeight: 1
*ls_init.listMarginWidth: 2
*ls_init.listSpacing: 1

