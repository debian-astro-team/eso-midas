! UIMX ascii 2.0 key: 2817                                                      

*fileSelectionDialog.class: dialogShell
*fileSelectionDialog.parent: NO_PARENT
*fileSelectionDialog.parentExpression: parent
*fileSelectionDialog.static: false
*fileSelectionDialog.gbldecl: #include <stdio.h>\
Boolean  saving;
*fileSelectionDialog.ispecdecl:
*fileSelectionDialog.funcdecl: swidget create_fileSelectionDialog(parent)\
swidget  parent;
*fileSelectionDialog.funcname: create_fileSelectionDialog
*fileSelectionDialog.funcdef: "swidget", "<create_fileSelectionDialog>(%)"
*fileSelectionDialog.argdecl: swidget parent;
*fileSelectionDialog.arglist: parent
*fileSelectionDialog.arglist.parent: "swidget", "%parent%"
*fileSelectionDialog.icode:
*fileSelectionDialog.fcode: XtUnmanageChild(XmSelectionBoxGetChild(UxGetWidget(fileSelectionBox), XmDIALOG_HELP_BUTTON));\
\
return(rtrn);
*fileSelectionDialog.auxdecl:
*fileSelectionDialog.name: fileSelectionDialog

*fileSelectionBox.class: fileSelectionBox
*fileSelectionBox.parent: fileSelectionDialog
*fileSelectionBox.static: false
*fileSelectionBox.name: fileSelectionBox
*fileSelectionBox.dialogStyle: "dialog_primary_application_modal"
*fileSelectionBox.cancelCallback: {\
  UxPopdownInterface(fileSelectionDialog);\
}
*fileSelectionBox.okCallback: {\
  UxPopdownInterface(fileSelectionDialog);\
  if (saving)\
    {\
       /* Replace this printf with your code for saving */\
       printf("Save file: %s\n", UxGetTextString(fileSelectionBox));\
\
    }\
  else\
    {\
       /* Replace this printf with your code for loading */\
       printf("Open file: %s\n", UxGetTextString(fileSelectionBox));\
    }\
}

