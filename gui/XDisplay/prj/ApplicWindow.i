! UIMX ascii 2.0 key: 6147                                                      
*action.MapACT: {\
\
/*  saved in file  `mapact.c'  */\
\
extern  Widget Widget_managed[2];\
extern swidget pushbt[2];\
\
extern int speedo;\
extern int menuact[8], kick[5];\
\
swidget  swmap, swpush;\
\
\
\
speedo = 0;		/* reset arrow accelerator */\
\
if (Widget_managed[0] != 0)\
   {\
   UxUnmap(UxWidgetToSwidget(Widget_managed[0]));	/* unmap current form */\
   XtSetSensitive(UxGetWidget(pushbt[0]),TRUE);	/* connected pushButton -> sensitive */\
   }\
\
\
if (UxWidget == UxGetWidget(pushButton7))\
   {\
   swmap = form2;		/* LOAD/IMAGE */\
   swpush = pushButton7;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton8))\
   {\
   swmap = form6;		/* CREATE/DISPLAY + CREATE/GRAPH */  \
   swpush = pushButton8;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton2))\
   {\
   swmap = form4;		/* LOAD/LUT + DISP/LUT */\
   swpush = pushButton2;\
   }\
   \
else if (UxWidget == UxGetWidget(pushButton4))\
   {\
   swmap = form8;		/* MODIFY/LUT */  \
   swpush = pushButton4;\
   menuact[0] = 0;\
   menuact[1] = 0;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton3))\
   {\
   swmap = form3;		/* View Image */  \
   swpush = pushButton3;\
   }\
   \
else if (UxWidget == UxGetWidget(pushButton6))\
   {\
   swmap = form7;		/* Get Cursor */  \
   swpush = pushButton6;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton9))\
   {\
   swmap = form1;		/* EXTRACT/TRACE */  \
   swpush = pushButton9;\
   menuact[2] = 0;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton5))\
   {\
   swmap = form5;		/* Quick Load */  \
   swpush = pushButton5;\
   }\
\
\
\
else\
   {\
/*   printf("MapACT: UxWidget = %d\n",UxWidget); */\
   return;\
   }\
\
\
UxMap(swmap);\
Widget_managed[0] = UxGetWidget(swmap);\
\
XtSetSensitive(UxGetWidget(swpush),FALSE);\
pushbt[0] = swpush;			/* update current pushButton */\
\
if (kick[4] == 1)			/* if interface help, update it */\
   XD_Help(1);\
\
}\

*action.MapACTX: {\
\
/*  saved in file  `mapactx.c' (same as `mapact.c' but fires off the\
    command in the end via "applycom"\
 */\
\
extern  Widget Widget_managed[2];\
extern swidget pushbt[2];\
\
extern int speedo;\
extern int menuact[8], kick[5];\
\
swidget  swmap, swpush;\
\
\
\
speedo = 0;		/* reset arrow accelerator */\
\
if (Widget_managed[0] != 0)\
   {\
   UxUnmap(UxWidgetToSwidget(Widget_managed[0]));	/* unmap current form */\
   XtSetSensitive(UxGetWidget(pushbt[0]),TRUE);	/* connected pushButton -> sensitive */\
   }\
\
\
if (UxWidget == UxGetWidget(pushButton7))\
   {\
   swmap = form2;		/* LOAD/IMAGE */\
   swpush = pushButton7;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton8))\
   {\
   swmap = form6;		/* CREATE/DISPLAY + CREATE/GRAPH */  \
   swpush = pushButton8;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton2))\
   {\
   swmap = form4;		/* LOAD/LUT + DISP/LUT */\
   swpush = pushButton2;\
   }\
   \
else if (UxWidget == UxGetWidget(pushButton4))\
   {\
   swmap = form8;		/* MODIFY/LUT */  \
   swpush = pushButton4;\
   menuact[0] = 0;\
   menuact[1] = 0;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton3))\
   {\
   swmap = form3;		/* View Image */  \
   swpush = pushButton3;\
   }\
   \
else if (UxWidget == UxGetWidget(pushButton6))\
   {\
   swmap = form7;		/* Get Cursor */  \
   swpush = pushButton6;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton9))\
   {\
   swmap = form1;		/* EXTRACT/TRACE */  \
   swpush = pushButton9;\
   menuact[2] = 0;\
   }\
\
else if (UxWidget == UxGetWidget(pushButton5))\
   {\
   swmap = form5;		/* Quick Load */  \
   swpush = pushButton5;\
   }\
\
\
\
else\
   {\
/*   printf("MapACT: UxWidget = %d\n",UxWidget); */\
   return;\
   }\
\
\
UxMap(swmap);\
Widget_managed[0] = UxGetWidget(swmap);\
\
XtSetSensitive(UxGetWidget(swpush),FALSE);\
pushbt[0] = swpush;			/* update current pushButton */\
\
\
if (kick[4] == 1)\
   XD_Help(1);\
\
\
applycom(0);\
\
}\

*action.WriteHelp: { \
\
/*   `writehelp.c'  the action for displaying help  */\
\
\
extern Widget Widget_managed[2];\
\
char s[100], cbuf[8];\
Widget sw = UxWidget;\
\
strcpy(s," ");\
\
\
/* first treat the push buttons */\
\
if (sw == UxGetWidget(pushButton7))\
   {\
   strcpy(s,"Map interface for displaying an image");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton2))\
   {\
   strcpy(s,"Map interface for loading a LUT and/or an ITT");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton3))\
   {\
   strcpy(s,"Map interface for exploring an image");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton4))\
   {\
   strcpy(s,"Map interface for modifying the LUT and/or the ITT");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton8))\
   {\
   strcpy(s,"Map interface for creating display or graphics window");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton6))\
   {\
   strcpy(s,\
   "Map interface for retrieving image coordinates via the cursor");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton9))\
   {\
   strcpy(s,\
   "Map interface for extracting a trace from the displayed image");\
   goto Text_out;\
   }\
\
else if (sw == UxGetWidget(pushButton1))\
   {\
   strcpy(s,"Click here to execute the command in MIDAS");\
   goto Text_out;\
   }\
\
\
\
\
\
\
/*  Quick Load  */\
\
if (Widget_managed[0] == UxGetWidget(form5))\
   {\
   if (sw == UxGetWidget(label37) || sw == UxGetWidget(text28))\
      strcpy(s,"Name of the image to be loaded");\
   else if (sw == UxGetWidget(label41) || sw == UxGetWidget(text31))\
      strcpy(s,"Scaling factor in X");\
   else if (sw == UxGetWidget(label51) || sw == UxGetWidget(text33))\
      strcpy(s,"Scaling factor in Y");\
   else if (sw == UxGetWidget(label38) || sw == UxGetWidget(text34))\
      strcpy(s,"Low cut values for mapping data to colour range");\
   else if (sw == UxGetWidget(label45) || sw == UxGetWidget(text35))\
      strcpy(s,"High cut values for mapping data to colour range");\
   else if (sw == UxGetWidget(label47) || sw == UxGetWidget(text36))\
      strcpy(s,"Value for in/decreasing low and high cut values");\
   else if (sw == UxGetWidget(label46)) \
      strcpy(s,"Yes = use descr. values of image, No = do not use them");\
   else if (sw == UxGetWidget(label42) || sw == UxGetWidget(text29))  \
      strcpy(s,"Name of Colour Lookup Table");\
   else if (sw == UxGetWidget(label44) || sw == UxGetWidget(text30))\
      strcpy(s,"Display xsize in pixels");\
   else if (sw == UxGetWidget(label49) || sw == UxGetWidget(text32))\
      strcpy(s,"Display ysize in pixels");\
   else if (sw == UxGetWidget(label43) || sw == UxGetWidget(label50))\
      strcpy(s,"Yes = Also, create display, No = do not create display");\
   }\
\
\
/*  Load Image  */\
\
else if (Widget_managed[0] == UxGetWidget(form2))\
   {\
   if (sw == UxGetWidget(label14) || sw == UxGetWidget(text16))  \
      strcpy(s,"Name of the image to be loaded");\
   else if (sw == UxGetWidget(label27) || sw == UxGetWidget(text2))  \
      strcpy(s,"Channel no. of display");\
   else if (sw == UxGetWidget(label32) || sw == UxGetWidget(text41))  \
         strcpy(s,"Display ID as single digit in [0,9] ");\
   else if (sw == UxGetWidget(label24) || sw == UxGetWidget(text39))  \
      strcpy(s,"Scaling factor in X");\
   else if (sw == UxGetWidget(label52) || sw == UxGetWidget(text40))  \
      strcpy(s,"Scaling factor in Y");\
   else if (sw == UxGetWidget(label15) || sw == UxGetWidget(text15))\
      strcpy(s,"Low cut values for mapping data to colour range");\
   else if (sw == UxGetWidget(label28) || sw == UxGetWidget(text22))\
      strcpy(s,"High cut values for mapping data to colour range");\
   else if (sw == UxGetWidget(label36) || sw == UxGetWidget(text25))\
      strcpy(s,"Value for in/decreasing low and high cut values");\
   else if (sw == UxGetWidget(label25))\
      strcpy(s,\
      "Image center pixels as `@xpix' and `@ypix' or `xcoord' and `ycoord'");\
   else if (sw == UxGetWidget(text20))\
      strcpy(s, "Image x-center pixel as `@xpix' or `xcoord'");\
   else if (sw == UxGetWidget(text24))\
      strcpy(s, "Image y-center pixel as `@ypix' or `ycoord'");\
   else if (sw == UxGetWidget(label53) || sw == UxGetWidget(label35))\
      strcpy(s,"Yes = use descr. values of image, No = do not use them");\
   }\
\
\
/* CREATE/DISPLAY + CREATE/GRAPHICS */\
\
else if (Widget_managed[0] == UxGetWidget(form6))\
   {\
   if (sw == UxGetWidget(label54))  \
      strcpy(s,"Toggle between Display and Graphics Window");\
\
   strcpy(cbuf,UxGetSet(toggleButton15));\
   if (cbuf[0] == 't')			/* DISPLAY */\
      {\
      if (sw == UxGetWidget(label16) || sw == UxGetWidget(text1)) \
         strcpy(s,"Display ID as single digit in [0,9] ");\
      else if (sw == UxGetWidget(label17) || sw == UxGetWidget(text4))\
         strcpy(s,"Display xsize in pixels");\
      else if (sw == UxGetWidget(label18) || sw == UxGetWidget(text5))\
         strcpy(s,"Display ysize in pixels");\
      else if (sw == UxGetWidget(label5) || sw == UxGetWidget(text6))\
         strcpy(s,"Display xoffset- lower left corner = (0,0)");\
      else if (sw == UxGetWidget(label22) || sw == UxGetWidget(text8))\
         strcpy(s,"Display yoffset- lower left corner = (0,0)");\
      else if (sw == UxGetWidget(label19) || sw == UxGetWidget(toggleButton25))  \
         strcpy(s,"Yes or No for alpha memory or not");\
      }\
\
   else					/* GRAPHIC */\
      {\
      if (sw == UxGetWidget(label16) || sw == UxGetWidget(text1))  \
         strcpy(s,"Graphic ID as single digit in [0,3] ");\
      else if (sw == UxGetWidget(label17) || sw == UxGetWidget(text4))\
         strcpy(s,"Graphic xsize in pixels");\
      else if (sw == UxGetWidget(label18) || sw == UxGetWidget(text5))\
         strcpy(s,"Graphic ysize in pixels");\
      else if (sw == UxGetWidget(label5) || sw == UxGetWidget(text6))\
         strcpy(s,"Graphic xoffset- lower left corner = (0,0)");\
      else if (sw == UxGetWidget(label22) || sw == UxGetWidget(text8))\
         strcpy(s,"Graphic yoffset- lower left corner = (0,0)");\
      }\
\
   if (sw == UxGetWidget(label21) || sw == UxGetWidget(text21))  \
      strcpy(s,"Name of Xworkstation/Xterminal screen in X11 syntax");\
   else if (sw == UxGetWidget(label20) || sw == UxGetWidget(text13))  \
      strcpy(s,"No. of graphic segments used for refreshing of plots");\
   }\
\
\
/* LOAD/LUT + DISPLAY/LUT + LOAD/ITT */\
\
else if (Widget_managed[0] == UxGetWidget(form4))\
   {\
   if (sw == UxGetWidget(label2) || sw == UxGetWidget(text9))  \
      strcpy(s,"Name of Colour Lookup Table");\
   else if (sw == UxGetWidget(label1)) 	\
      strcpy(s,"Yes or No to display a colour bar with LUT");\
   else if (sw == UxGetWidget(label26) || sw == UxGetWidget(text23))  \
      strcpy(s,"Name of Intensity Transfer Table");\
   } \
\
\
/* MODIFY/LUT + MODIFY/ITT */\
\
else if (Widget_managed[0] == UxGetWidget(form8))\
   {\
   if (sw == UxGetWidget(label39))\
      strcpy(s,"method for LUT modification");\
   else if (sw == UxGetWidget(label40))\
      strcpy(s,"colour for LUT modification");\
   else if (sw == UxGetWidget(label23))\
      strcpy(s,"method for ITT modification");\
   else if (sw == UxGetWidget(label33))\
      strcpy(s,"value for ITT modification");\
   }\
  \
\
/* VIEW/IMAGE */\
\
else if (Widget_managed[0] == UxGetWidget(form3))\
   {\
   if (sw == UxGetWidget(label9) || sw == UxGetWidget(text10))  \
      strcpy(s,"Name of the image to be viewed (examined)");\
   else if (sw == UxGetWidget(label13) || sw == UxGetWidget(text17))  \
      strcpy(s,"Name of optional table for coords of visited regions");\
   else if (sw == UxGetWidget(label11))   \
      strcpy(s,"Plot or NoPlot for plotting extracted info in graph_window or not");\
   else if (sw == UxGetWidget(label12)) \
      strcpy(s,"FITS for FITS files or Midas for native Midas format");\
   }\
\
\
/* GET/CURSOR */\
\
else if (Widget_managed[0] == UxGetWidget(form7))\
   {\
   if (sw == UxGetWidget(label3) || sw == UxGetWidget(text3))\
      strcpy(s,"Specs for storing displayed info:\ntable name or descr,DESC");\
   else if (sw == UxGetWidget(label7))\
      strcpy(s,"`Yes' or `No' for for marking cursor position in overlay plane");\
   else if (sw == UxGetWidget(label8))\
      strcpy(s,"No. of cursors, 1 or 2 cursors");\
   else if (sw == UxGetWidget(label4))\
      strcpy(s,"Use zoom window or not");\
   }\
\
\
/* EXTRACT/TRACE */\
\
else if (Widget_managed[0] == UxGetWidget(form1))\
   {\
   if (sw == UxGetWidget(label6) || sw == UxGetWidget(text7))\
      strcpy(s,"Name of result frame");\
   else if (sw == UxGetWidget(label31) || sw == UxGetWidget(plotflag))\
      strcpy(s,\
  "`Draw', `Plot' or `None' for plotting in overlay, graph.window or no plots");\
   else if (sw == UxGetWidget(label29))\
      strcpy(s,"`NoCut' or `Cut' line after cursors");\
   else if (sw == UxGetWidget(label30) || sw == UxGetWidget(text14))\
      strcpy(s,"Stepsize for result frame if different from input frame");\
   }\
\
else if (sw == UxGetWidget(pushButton5))\
   {\
   strcpy(s,"Activate interface for displaying an image \n load a LUT");\
   strcat(s,"and create the display window");\
   }\
\
else if (sw == UxGetWidget(pushButton7))\
   strcpy(s,"Activate interface for displaying an image");\
\
else if (sw == UxGetWidget(pushButton4))\
   strcpy(s,"Activate interface for loading a LUT and/or an ITT");\
\
else if (sw == UxGetWidget(pushButton3))\
   strcpy(s,"Activate interface for exploring an image");\
\
else if (sw == UxGetWidget(pushButton4))\
   strcpy(s,"Activate interface for modifying the LUT and/or the ITT");\
\
else if (sw == UxGetWidget(pushButton8))\
   strcpy(s,"Activate interface for creating display or graphics window");\
\
else if (sw == UxGetWidget(pushButton7))\
   strcpy(s,\
   "Activate interface for retrieving image coordinates via the cursor");\
\
else if (sw == UxGetWidget(pushButton9))\
   strcpy(s,\
   "Activate interface for extracting a trace from the displayed image");\
\
else if (sw == UxGetWidget(pushButton1))\
   strcpy(s,"Click here to execute the command in MIDAS");\
\
\
\
\
Text_out:\
UxPutText(SHelp,s);\
\
}\
\
\
\

*action.ClearHelp: {\
UxPutText(SHelp,"");\
}
*action.FileSelectACT1: {\
\
/*  `fileselect.c'  the action routine for `FileSelectACT1'  */\
\
#include <fsydef.h>\
\
\
extern swidget swfs, stxt;\
extern Widget  filelist_widget;\
extern char  dir_specs[128];\
extern int  pushb;\
\
Widget  ww = UxWidget;\
int  strip, n, k;\
\
\
\
stxt = UxThisWidget;\
strip = 1;				/* default to stripping ,off */\
\
if ((ww == UxGetWidget(text9)) || (ww == UxGetWidget(text29)))\
  {\
   osfphname("MID_SYSTAB",dir_specs);\
   n = strlen(dir_specs);\
   k = n - 1;\
   if (dir_specs[k] != FSY_DIREND)\
      {\
      dir_specs[n] = FSY_DIREND;\
      n++;\
      }\
   strcpy(&dir_specs[n],"*.lut");	/* list the LUTs */\
   }\
 \
\
else if (ww == UxGetWidget(text23))\
   {\
   osfphname("MID_SYSTAB",dir_specs);\
   n = strlen(dir_specs);\
   k = n - 1;\
   if (dir_specs[k] != FSY_DIREND)\
      {\
      dir_specs[n] = FSY_DIREND;\
      n++;\
      }\
   strcpy(&dir_specs[n],"*.itt");	/* list the ITTs */\
   }\
\
\
else if ((ww == UxGetWidget(text16)) || (ww == UxGetWidget(text10)) ||\
         (ww == UxGetWidget(text28)))\
   strcpy(dir_specs,"*.bdf");		/* list all bedf files */\
\
\
else\
   {\
   strip = 0;			/* leave filenames as they are */\
   strcpy(dir_specs,"*.*");\
   }\
\
SetFileList(filelist_widget,strip,dir_specs);\
\
UxPopupInterface(swfs, no_grab);\
\
\
}\

*action.ArrowACT2: {\
\
\
/*  `arrows2.c'  the action for Button1 down in an arrow widget \
                 in "form2" (LOAD/IMAGE)                         */\
\
#include <stdio.h>\
\
\
extern int speedo;\
extern double  stepsize[2];\
\
char  cbuf[20], cd, *single;\
int   nh, szx, szy, dspid, kval[2];\
\
float cuthi, cutlo, cutdelt;\
\
\
 \
/* LOAD/IMAGE */\
\
\
if (speedo == 0)\
   nh = 1;\
else\
   nh = 5;\
\
\
if (UxWidget == UxGetWidget(arrowButton6))\
   {                                 /* Channel down arrow */\
   single = XmTextGetString(UxGetWidget(text2));\
   dspid = (*single) - 48;\
   if (dspid > 0) dspid --;\
   cbuf[0] = dspid + 48;\
   cbuf[1] = '\0';\
   UxPutText(text2,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton5))\
   {                                 /* Channel up arrow */\
   single = XmTextGetString(UxGetWidget(text2));\
   dspid = (*single) - 48;\
   if (dspid < 3) dspid ++;\
   cbuf[0] = dspid + 48;\
   cbuf[1] = '\0';\
   UxPutText(text2,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton48))\
   {                                 /* Display_Id down arrow */\
   single = XmTextGetString(UxGetWidget(text41));\
   dspid = (*single) - 48;\
   if (dspid > 0) dspid --;\
   cbuf[0] = dspid + 48;\
   cbuf[1] = '\0';\
   UxPutText(text41,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton47))\
   {                                 /* Display_Id up arrow */\
   single = XmTextGetString(UxGetWidget(text41));\
   dspid = (*single) - 48;\
   if (dspid < 9) dspid ++;\
   cbuf[0] = dspid + 48;\
   cbuf[1] = '\0';\
   UxPutText(text41,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton40))\
   {                                 /* ScaleX down arrow */\
   single = XmTextGetString(UxGetWidget(text39));\
   sscanf(single,"%d",&dspid);\
   dspid -= nh;\
   if (dspid < -100) dspid = -100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text39,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton39))\
   {                                 /* ScaleX up arrow */\
   single = XmTextGetString(UxGetWidget(text39));\
   sscanf(single,"%d",&dspid);\
   dspid += nh;\
   if (dspid > 100) dspid = 100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text39,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton46))\
   {                                 /* ScaleY down arrow */\
   single = XmTextGetString(UxGetWidget(text40));\
   sscanf(single,"%d",&dspid);\
   dspid -= nh;\
   if (dspid < -100) dspid = -100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text40,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton45))\
   {                                 /* ScaleY up arrow */\
   single = XmTextGetString(UxGetWidget(text40));\
   sscanf(single,"%d",&dspid);\
   dspid += nh;\
   if (dspid > 100) dspid = 100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text40,cbuf);\
   }\
\
\
\
\
else if (UxWidget == UxGetWidget(arrowButton23))\
   {                                 /* Center left arrow */\
   single = XmTextGetString(UxGetWidget(text20));\
   sscanf(single,"%d",&kval[0]);\
   kval[0] += nh*stepsize[0];\
   sprintf(cbuf,"%d",kval[0]);\
   UxPutText(text20,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton24))\
   {                                 /* Center right arrow */\
   single = XmTextGetString(UxGetWidget(text20));\
   sscanf(single,"%d",&kval[0]);\
   kval[0] -= nh*stepsize[0];\
   sprintf(cbuf,"%d",kval[0]);\
   UxPutText(text20,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton22))\
   {                                 /* Center down arrow */\
   single = XmTextGetString(UxGetWidget(text24));\
   sscanf(single,"%d",&kval[1]);\
   kval[1] += nh*stepsize[1];\
   sprintf(cbuf,"%d",kval[1]);\
   UxPutText(text24,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton21))\
   {                                 /* Center up arrow */\
   single = XmTextGetString(UxGetWidget(text24));\
   sscanf(single,"%d",&kval[1]);\
   kval[1] -= nh*stepsize[1];\
   sprintf(cbuf,"%d",kval[1]);\
   UxPutText(text24,cbuf);\
   }\
\
else \
    {\
   single = XmTextGetString(UxGetWidget(text25));\
   sscanf(single,"%f",&cutdelt);\
\
if (UxWidget == UxGetWidget(arrowButton26))\
   {                             /* LowCut down arrow */\
   single = XmTextGetString(UxGetWidget(text15));\
   sscanf(single,"%f",&cutlo);\
   cutlo -= nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cutlo);\
   UxPutText(text15,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton25))\
   {                                 /* LowCut up arrow */\
   single = XmTextGetString(UxGetWidget(text15));\
   sscanf(single,"%f",&cutlo);\
   cutlo += nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cutlo);\
   UxPutText(text15,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton28))\
   {                                 /* HighCut down arrow */\
   single = XmTextGetString(UxGetWidget(text22));\
   sscanf(single,"%f",&cuthi);\
   cuthi -= nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cuthi);\
   UxPutText(text22,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton27))\
   {                                 /* HighCut up arrow */\
   single = XmTextGetString(UxGetWidget(text22));\
   sscanf(single,"%f",&cuthi);\
   cuthi += nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cuthi);\
   UxPutText(text22,cbuf);\
   }\
   }\
\
XtFree(single);\
\
\
}\

*action.ArrowCHNG: {\
extern  int  speedo;\
\
if (speedo == 1)\
   speedo = 0;\
else\
   speedo = 1;\
\
}
*action.ArrowACT6: {\
\
\
/*  `arrows6.c'  the action for Button1 down in an arrow widget \
                 in "form6" (CREATE/DISPLAY)                    */\
\
\
\
extern int speedo;\
\
char  cbuf[20], cbo[2], cd, *single;\
int   nh, szx, szy, dspid, kdelta;\
\
\
/* CREATE/DISPLAY */\
\
strcpy(cbuf,UxGetSet(toggleButton15));\
if (cbuf[0] == 't') \
   cd = 'D';\
else\
   cd = 'G';\
\
if (speedo == 0)\
   kdelta = 10;\
else\
   kdelta = 50;\
\
if (UxWidget == UxGetWidget(arrowButton4))\
   {					/* DisplaySzX down arrow */\
   strcpy(cbuf,UxGetText(text4));\
   sscanf(cbuf,"%d",&szx);\
   nh = szx >> 1;\
   if (nh < 1) nh = 1;\
   szx -= nh;\
   if (szx < 2) szx = 2;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text4,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton3))\
   {					/* DisplaySzX up arrow */\
   strcpy(cbuf,UxGetText(text4));\
   sscanf(cbuf,"%d",&szx);\
   nh = szx >> 1;\
   if (nh < 1) nh = 1;\
   szx += nh;\
   if (szx >1024) szx = 1024;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text4,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton10))\
   {					/* DisplaySzY down arrow */\
   strcpy(cbuf,UxGetText(text5));\
   sscanf(cbuf,"%d",&szy);\
   nh = szy >> 1;\
   if (nh < 1) nh = 1;\
   szy -= nh;\
   if (szy < 2) szy = 2;\
   sprintf(cbuf,"%d",szy);\
   UxPutText(text5,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton9))\
   {					/* DisplaySzY up arrow */\
   strcpy(cbuf,UxGetText(text5));\
   sscanf(cbuf,"%d",&szy);\
   nh = szy >> 1;\
   if (nh < 1) nh = 1;\
   szy += nh;\
   if (szy > 1024) szy = 1024;\
   sprintf(cbuf,"%d",szy);\
   UxPutText(text5,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton12))\
   {					/* DisplayOffX down arrow */\
   strcpy(cbuf,UxGetText(text6));\
   sscanf(cbuf,"%d",&szx);\
   szx -= kdelta;\
   if (szx < 0) szx = 0;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text6,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton11))\
   {					/* DisplayOffX up arrow */\
   strcpy(cbuf,UxGetText(text6));\
   sscanf(cbuf,"%d",&szx);\
   szx += kdelta;\
   if (szx > 1020) szx = 1020;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text6,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton18))\
   {					/* DisplayOffY down arrow */\
   strcpy(cbuf,UxGetText(text8));\
   sscanf(cbuf,"%d",&szy);\
   szy -= kdelta;\
   if (szy < 0) szy = 0;\
   sprintf(cbuf,"%d",szy);\
   UxPutText(text8,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton17))\
   {					/* DisplayOffY up arrow */\
   strcpy(cbuf,UxGetText(text8));\
   sscanf(cbuf,"%d",&szy);\
   szy += kdelta;\
   if (szy > 1020) szy = 1020;\
   sprintf(cbuf,"%d",szy);\
   UxPutText(text8,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton8))\
   {					/* Display_Id down arrow */\
   single = XmTextGetString(UxGetWidget(text1));\
   dspid = (*single) - 48;\
   if (dspid > 0) dspid --;\
   cbo[0] = dspid + 48;\
   cbo[1] = '\0';\
   UxPutText(text1,cbo);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton7))\
   {					/* Display_Id up arrow */\
   single = XmTextGetString(UxGetWidget(text1));\
   dspid = (*single) - 48;\
   if (cd == 'D')\
      {\
      if (dspid < 9) dspid ++;\
      }\
   else\
      {\
      if (dspid < 3) dspid ++;\
      }\
   cbo[0] = dspid + 48;\
   cbo[1] = '\0';\
   UxPutText(text1,cbo);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton14))\
   {					/* Display Gsize down arrow */\
   strcpy(cbuf,UxGetText(text13));\
   sscanf(cbuf,"%d",&szx);\
   kdelta *= 1000;		/* 10, 50  =>  10000, 50000  */\
   szx -= kdelta;\
   if (szx < 10000) szx = 10000;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text13,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton13))\
   {					/* Display Gsize up arrow */\
   strcpy(cbuf,UxGetText(text13));\
   sscanf(cbuf,"%d",&szx);\
   kdelta *= 1000;		/* 10, 50  =>  10000, 50000  */\
   szx += kdelta;\
   if (szx > 2000000) szx = 2000000;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text13,cbuf);\
   }\
\
 \
XtFree(single);\
\
\
}\

*action.ArrowACT8: {\
\
\
/*  `arrows8.c'  the action for Button1 down in an arrow widget\
                 in "form8" (MODIFY/ITT)   */\
\
\
\
extern int speedo;\
\
char  cbuf[20], *single;\
int   nh, ittval;\
\
\
 \
/* MODIFY/ITT */\
\
if (speedo == 0)\
   nh = 1;\
else\
   nh = 5;\
\
\
if (UxWidget == UxGetWidget(arrowButton2))\
   {                                 /* ITT value down arrow */\
   single = XmTextGetString(UxGetWidget(text27));\
   sscanf(single,"%d",&ittval);\
   ittval -= nh;\
   if (ittval < 0) ittval = 0;\
   sprintf(cbuf,"%d",ittval);\
   UxPutText(text27,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton1))\
   {                                 /* ITT value up arrow */\
   single = XmTextGetString(UxGetWidget(text27));\
   sscanf(single,"%d",&ittval);\
   ittval += nh;\
   if (ittval > 255) ittval = 255;\
   sprintf(cbuf,"%d",ittval);\
   UxPutText(text27,cbuf);\
   }\
\
 \
XtFree(single);\
\
\
}\

*action.ArrowACT5: {\
\
\
/*  `arrows5.c'  the action for Button1 down in an arrow widget \
                 in "form5" (EZ Load)                         */\
\
#include <stdio.h>\
\
\
extern int speedo;\
extern double  stepsize[2];\
\
char  cbuf[20], cd, *single;\
int   nh, kdelta, szx, szy, dspid, kval[2];\
\
float cuthi, cutlo, cutdelt;\
\
\
 \
\
if (speedo == 0)\
   {\
   kdelta = 10;\
   nh = 1;\
   }\
else\
   {\
   kdelta = 50;\
   nh = 5;\
   }\
\
\
if (UxWidget == UxGetWidget(arrowButton36))\
   {                                 /* ScaleX down arrow */\
   single = XmTextGetString(UxGetWidget(text31));\
   sscanf(single,"%d",&dspid);\
   dspid -= nh;\
   if (dspid < -100) dspid = -100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text31,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton35))\
   {                                 /* ScaleX up arrow */\
   single = XmTextGetString(UxGetWidget(text31));\
   sscanf(single,"%d",&dspid);\
   dspid += nh;\
   if (dspid > 100) dspid = 100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text31,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton38))\
   {                                 /* ScaleY down arrow */\
   single = XmTextGetString(UxGetWidget(text33));\
   sscanf(single,"%d",&dspid);\
   dspid -= nh;\
   if (dspid < -100) dspid = -100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text33,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton37))\
   {                                 /* ScaleY up arrow */\
   single = XmTextGetString(UxGetWidget(text33));\
   sscanf(single,"%d",&dspid);\
   dspid += nh;\
   if (dspid > 100) dspid = 100;\
   sprintf(cbuf,"%d",dspid);\
   UxPutText(text33,cbuf);\
   }\
\
\
else \
   {\
   single = XmTextGetString(UxGetWidget(text36));\
   sscanf(single,"%f",&cutdelt);\
   }\
\
XtFree(single);\
\
if (UxWidget == UxGetWidget(arrowButton42))\
   {                             /* LowCut down arrow */\
   single = XmTextGetString(UxGetWidget(text34));\
   sscanf(single,"%f",&cutlo);\
   cutlo -= nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cutlo);\
   UxPutText(text34,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton41))\
   {                                 /* LowCut up arrow */\
   single = XmTextGetString(UxGetWidget(text34));\
   sscanf(single,"%f",&cutlo);\
   cutlo += nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cutlo);\
   UxPutText(text34,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton44))\
   {                                 /* HighCut down arrow */\
   single = XmTextGetString(UxGetWidget(text35));\
   sscanf(single,"%f",&cuthi);\
   cuthi -= nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cuthi);\
   UxPutText(text35,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton43))\
   {                                 /* HighCut up arrow */\
   single = XmTextGetString(UxGetWidget(text35));\
   sscanf(single,"%f",&cuthi);\
   cuthi += nh*cutdelt;\
   sprintf(cbuf,"%8.4f",cuthi);\
   UxPutText(text35,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton30))\
   {					/* DisplaySzX down arrow */\
   strcpy(cbuf,UxGetText(text30));\
   sscanf(cbuf,"%d",&szx);\
   nh = szx >> 1;\
   if (nh < 1) nh = 1;\
   szx -= nh;\
   if (szx < 2) szx = 2;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text30,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton29))\
   {					/* DisplaySzX up arrow */\
   strcpy(cbuf,UxGetText(text30));\
   sscanf(cbuf,"%d",&szx);\
   nh = szx >> 1;\
   if (nh < 1) nh = 1;\
   szx += nh;\
   if (szx >1024) szx = 1024;\
   sprintf(cbuf,"%d",szx);\
   UxPutText(text30,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton34))\
   {					/* DisplaySzY down arrow */\
   strcpy(cbuf,UxGetText(text32));\
   sscanf(cbuf,"%d",&szy);\
   nh = szy >> 1;\
   if (nh < 1) nh = 1;\
   szy -= nh;\
   if (szy < 2) szy = 2;\
   sprintf(cbuf,"%d",szy);\
   UxPutText(text32,cbuf);\
   }\
\
else if (UxWidget == UxGetWidget(arrowButton33))\
   {					/* DisplaySzY up arrow */\
   strcpy(cbuf,UxGetText(text32));\
   sscanf(cbuf,"%d",&szy);\
   nh = szy >> 1;\
   if (nh < 1) nh = 1;\
   szy += nh;\
   if (szy > 1024) szy = 1024;\
   sprintf(cbuf,"%d",szy);\
   UxPutText(text32,cbuf);\
   }\
\
 \
XtFree(single);\
\
}\


*translation.table: MapUnmap
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Down>: MapACT()
*translation.<Btn3Down>: MapACTX()
*translation.<EnterWindow>: WriteHelp()
*translation.<LeaveWindow>: ClearHelp()

*translation.table: SelectFile
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()
*translation.<Btn3Down>: FileSelectACT1()

*translation.table: Arrows2
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Down>: ArrowACT2()
*translation.<Btn1Down>(2): ArrowCHNG()

*translation.table: Arrows6
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Down>: ArrowACT6()
*translation.<Btn1Down>(2): ArrowCHNG()

*translation.table: Arrows9
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Down>: ArrowACT9()
*translation.<Btn1Down>(2): ArrowCHNG()

*translation.table: Arrows8
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Down>: ArrowACT8()
*translation.<Btn1Down>(2): ArrowCHNG()

*translation.table: Arrows5
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Down>: ArrowACT5()
*translation.<Btn1Down>(2): ArrowCHNG()

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: false
*ApplicWindow.gbldecl: #include <ExternResources.h>\
#include <stdio.h>\
#include <Xm/Protocols.h>\
#include "UxFsBox.h"\
\
extern Widget Widget_managed[2];\
\
extern Widget Button_insensitive;\
\
\
/* Include for UxPut calls on the fileSelectionBox */\
\
void CreateWindowManagerProtocols();\
/* void ExitCB(); */\
void CreateSessionManagerProtocols();\
void SaveSessionCB();\
\

*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget popup_ApplicWindow()\

*ApplicWindow.funcname: popup_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<popup_ApplicWindow>(%)"
*ApplicWindow.icode:
*ApplicWindow.fcode: /*\
create_fileSelectionDialog(rtrn);\
create_exitDialog(rtrn);\
*/\
\
CreateWindowManagerProtocols(UxGetWidget(rtrn));\
CreateSessionManagerProtocols();\
\
UxPopupInterface(rtrn, no_grab);\
return(rtrn);
*ApplicWindow.auxdecl: #include "myaux.c"\

*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 160
*ApplicWindow.y: 200
*ApplicWindow.width: 450
*ApplicWindow.height: 450
*ApplicWindow.title: "XDisplay"
*ApplicWindow.deleteResponse: "do_nothing"
*ApplicWindow.keyboardFocusPolicy.source: public
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.iconName: "XDisplay"
*ApplicWindow.popupCallback: {\
\
/*  `popup.c'   the Popup Callback of main ApplicWindow  */\
\
extern  Widget Widget_managed[2];\
extern swidget pushbt[2];\
\
extern char  actdisp[8];\
\
swidget  swmap;\
\
\
/*  map + unmap all forms in their order */\
\
UxPutText(text2,"0");\
UxPutText(text41,"0");\
UxUnmap(form2);		/* was mapped before */\
\
UxMap(form4);\
UxUnmap(form4);\
\
UxMap(form3);\
UxUnmap(form3);\
\
UxMap(form8);\
strcpy(actdisp,"255");\
UxPutText(text27,actdisp);\
UxUnmap(form8);\
\
UxMap(form6);\
strcpy(actdisp,"0");\
UxPutText(text1,actdisp);\
UxPutText(text4,"512");\
UxPutText(text5,"512");\
UxPutText(text6,"630");\
UxPutText(text8,"330");\
UxPutText(text21,"default");\
UxPutText(text13,"30000");\
UxUnmap(form6);\
\
UxMap(form7);\
UxUnmap(form7);\
\
UxMap(form5);\
UxPutText(text30,"512");\
UxPutText(text32,"512");\
UxPutText(text29,"rainbow");\
UxUnmap(form5);\
\
UxMap(form1);\
UxPutText(text7,"trace");\
UxUnmap(form1);\
\
swmap = form5;		/* begin with Quick Load */\
UxMap(swmap);\
Widget_managed[0] = UxGetWidget(swmap);\
\
pushbt[0] = pushButton5;\
XtSetSensitive(UxGetWidget(pushbt[0]),FALSE);\
\
UxMap(XDMenus);			/* map the menu bar */  \
\
\
}
*ApplicWindow.background: WindowBackground
*ApplicWindow.defaultFontList: TextFont

*mainWindow.class: mainWindow
*mainWindow.parent: ApplicWindow
*mainWindow.static: true
*mainWindow.name: mainWindow
*mainWindow.x: 200
*mainWindow.y: 180
*mainWindow.width: 100
*mainWindow.height: 100
*mainWindow.background: WindowBackground

*workAreaForm.class: form
*workAreaForm.parent: mainWindow
*workAreaForm.static: true
*workAreaForm.name: workAreaForm
*workAreaForm.width: 490
*workAreaForm.height: 340
*workAreaForm.borderWidth: 0
*workAreaForm.background: WindowBackground
*workAreaForm.textFontList: TextFont

*workAreaFrame.class: frame
*workAreaFrame.parent: workAreaForm
*workAreaFrame.static: true
*workAreaFrame.name: workAreaFrame
*workAreaFrame.x: 50
*workAreaFrame.y: 30
*workAreaFrame.width: 390
*workAreaFrame.height: 230
*workAreaFrame.bottomAttachment: "attach_form"
*workAreaFrame.bottomOffset.source: public
*workAreaFrame.bottomOffset: 0
*workAreaFrame.leftAttachment: "attach_form"
*workAreaFrame.leftOffset.source: public
*workAreaFrame.leftOffset: 0
*workAreaFrame.rightAttachment: "attach_form"
*workAreaFrame.rightOffset.source: public
*workAreaFrame.rightOffset: 0
*workAreaFrame.topAttachment: "attach_form"
*workAreaFrame.topOffset.source: public
*workAreaFrame.topOffset: 0
*workAreaFrame.background: WindowBackground

*workArea.class: form
*workArea.parent: workAreaFrame
*workArea.static: true
*workArea.name: workArea
*workArea.x: 20
*workArea.y: 2
*workArea.width: 408
*workArea.height: 498
*workArea.borderWidth: 0
*workArea.createCallback: {\
\
}
*workArea.mapCallback: {\
\
}
*workArea.background: WindowBackground
*workArea.textFontList: TextFont

*form2.class: form
*form2.parent: workArea
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.x: 0
*form2.y: 10
*form2.width: 430
*form2.height: 240
*form2.background: ApplicBackground
*form2.borderWidth: 1
*form2.mappedWhenManaged: "false"
*form2.leftAttachment: "attach_form"
*form2.leftOffset: 4
*form2.rightAttachment: "attach_form"
*form2.rightOffset: 4
*form2.topAttachment: "attach_form"
*form2.topOffset: 4
*form2.createCallback: {\
\
}
*form2.mapCallback: {\
\
}
*form2.noResize: "true"

*label14.class: label
*label14.parent: form2
*label14.static: true
*label14.name: label14
*label14.x: 10
*label14.y: 10
*label14.width: 60
*label14.height: 25
*label14.labelString: "Frame"
*label14.foreground: TextForeground
*label14.background: LabelBackground
*label14.alignment: "alignment_center"
*label14.recomputeSize: "false"
*label14.translations: ""
*label14.fontList: BoldTextFont
*label14.leftAttachment: "attach_form"
*label14.leftOffset: 5
*label14.topAttachment: "attach_form"
*label14.topOffset: 10

*text16.class: text
*text16.parent: form2
*text16.static: true
*text16.name: text16
*text16.x: 10
*text16.y: 39
*text16.width: 250
*text16.height: 32
*text16.background: TextBackground
*text16.translations: SelectFile
*text16.foreground: TextForeground
*text16.fontList: TextFont
*text16.columns: 30
*text16.leftAttachment: "attach_widget"
*text16.leftOffset: 2
*text16.rightAttachment: "attach_form"
*text16.rightOffset: 70
*text16.topAttachment: "attach_form"
*text16.topOffset: 5
*text16.leftWidget: "label14"
*text16.marginHeight: 2
*text16.marginWidth: 2

*label27.class: label
*label27.parent: form2
*label27.static: true
*label27.name: label27
*label27.x: 10
*label27.y: 79
*label27.width: 60
*label27.height: 25
*label27.labelString: "Channel"
*label27.foreground: TextForeground
*label27.background: LabelBackground
*label27.alignment: "alignment_center"
*label27.recomputeSize: "false"
*label27.translations: ""
*label27.fontList: BoldTextFont
*label27.leftAttachment: "attach_form"
*label27.leftOffset: 5
*label27.topAttachment: "attach_form"
*label27.topOffset: 48
*label27.topWidget: "text16"

*label24.class: label
*label24.parent: form2
*label24.static: true
*label24.name: label24
*label24.x: 10
*label24.y: 119
*label24.width: 60
*label24.height: 25
*label24.labelString: "ScaleX"
*label24.foreground: TextForeground
*label24.background: LabelBackground
*label24.alignment: "alignment_center"
*label24.recomputeSize: "false"
*label24.translations: ""
*label24.fontList: BoldTextFont
*label24.leftAttachment: "attach_form"
*label24.leftOffset: 5
*label24.topAttachment: "attach_form"
*label24.topOffset: 135
*label24.topWidget: ""

*label25.class: label
*label25.parent: form2
*label25.static: true
*label25.name: label25
*label25.x: 10
*label25.y: 159
*label25.width: 85
*label25.height: 25
*label25.labelString: "Center"
*label25.foreground: TextForeground
*label25.background: LabelBackground
*label25.alignment: "alignment_center"
*label25.recomputeSize: "false"
*label25.translations: ""
*label25.fontList: BoldTextFont
*label25.leftAttachment: "attach_form"
*label25.leftOffset: 50
*label25.topAttachment: "attach_form"
*label25.topOffset: 82
*label25.topWidget: "label24"

*label15.class: label
*label15.parent: form2
*label15.static: true
*label15.name: label15
*label15.x: 10
*label15.y: 199
*label15.width: 60
*label15.height: 25
*label15.labelString: "LowCut"
*label15.foreground: TextForeground
*label15.background: LabelBackground
*label15.alignment: "alignment_center"
*label15.recomputeSize: "false"
*label15.translations: ""
*label15.fontList: BoldTextFont
*label15.leftAttachment: "attach_form"
*label15.leftOffset: 275
*label15.topAttachment: "attach_form"
*label15.topOffset: 135
*label15.topWidget: "label25"

*rowColumn13.class: rowColumn
*rowColumn13.parent: form2
*rowColumn13.static: true
*rowColumn13.name: rowColumn13
*rowColumn13.x: 119
*rowColumn13.y: 0
*rowColumn13.width: 60
*rowColumn13.height: 44
*rowColumn13.orientation: "horizontal"
*rowColumn13.packing: "pack_none"
*rowColumn13.spacing: 0
*rowColumn13.resizeHeight: "false"
*rowColumn13.resizeWidth: "false"
*rowColumn13.marginHeight: 0
*rowColumn13.marginWidth: 0
*rowColumn13.numColumns: 2
*rowColumn13.background: ApplicBackground
*rowColumn13.leftAttachment: "attach_widget"
*rowColumn13.leftOffset: 0
*rowColumn13.leftWidget: "label27"
*rowColumn13.topAttachment: "attach_form"
*rowColumn13.topOffset: 38

*rowColumn20.class: rowColumn
*rowColumn20.parent: rowColumn13
*rowColumn20.static: true
*rowColumn20.name: rowColumn20
*rowColumn20.x: 25
*rowColumn20.y: 6
*rowColumn20.width: 23
*rowColumn20.height: 32
*rowColumn20.resizeHeight: "false"
*rowColumn20.resizeWidth: "false"
*rowColumn20.background: ApplicBackground
*rowColumn20.marginHeight: 0
*rowColumn20.marginWidth: 0
*rowColumn20.packing: "pack_none"
*rowColumn20.spacing: 0
*rowColumn20.adjustMargin: "true"
*rowColumn20.adjustLast: "true"
*rowColumn20.entryAlignment: "alignment_center"

*arrowButton5.class: arrowButton
*arrowButton5.parent: rowColumn20
*arrowButton5.static: true
*arrowButton5.name: arrowButton5
*arrowButton5.x: 0
*arrowButton5.y: 0
*arrowButton5.width: 23
*arrowButton5.height: 15
*arrowButton5.background: TextBackground
*arrowButton5.shadowThickness: 0
*arrowButton5.translations: Arrows2
*arrowButton5.foreground: "PaleGreen"
*arrowButton5.highlightThickness: 0

*arrowButton6.class: arrowButton
*arrowButton6.parent: rowColumn20
*arrowButton6.static: true
*arrowButton6.name: arrowButton6
*arrowButton6.y: 16
*arrowButton6.width: 23
*arrowButton6.height: 15
*arrowButton6.background: TextBackground
*arrowButton6.shadowThickness: 0
*arrowButton6.arrowDirection: "arrow_down"
*arrowButton6.translations: Arrows2
*arrowButton6.foreground: "PaleGreen"
*arrowButton6.borderColor: ApplicBackground
*arrowButton6.highlightThickness: 0

*text2.class: text
*text2.parent: rowColumn13
*text2.static: true
*text2.name: text2
*text2.x: 1
*text2.y: 7
*text2.width: 25
*text2.height: 32
*text2.background: TextBackground
*text2.fontList: TextFont
*text2.foreground: TextForeground
*text2.highlightThickness: 2
*text2.shadowThickness: 2
*text2.marginHeight: 2
*text2.marginWidth: 1
*text2.maxLength: 100
*text2.columns: 1

*label32.class: label
*label32.parent: form2
*label32.static: true
*label32.name: label32
*label32.x: 20
*label32.y: 65
*label32.width: 80
*label32.height: 25
*label32.labelString: "Window_ID"
*label32.foreground: TextForeground
*label32.background: LabelBackground
*label32.alignment: "alignment_center"
*label32.recomputeSize: "false"
*label32.translations: ""
*label32.fontList: BoldTextFont
*label32.leftAttachment: "attach_form"
*label32.leftOffset: 275
*label32.topAttachment: "attach_form"
*label32.topOffset: 48

*rowColumn18.class: rowColumn
*rowColumn18.parent: form2
*rowColumn18.static: true
*rowColumn18.name: rowColumn18
*rowColumn18.x: 108
*rowColumn18.y: 100
*rowColumn18.width: 210
*rowColumn18.height: 55
*rowColumn18.orientation: "horizontal"
*rowColumn18.packing: "pack_none"
*rowColumn18.spacing: 0
*rowColumn18.resizeHeight: "false"
*rowColumn18.resizeWidth: "false"
*rowColumn18.marginHeight: 0
*rowColumn18.marginWidth: 0
*rowColumn18.numColumns: 2
*rowColumn18.background: ApplicBackground
*rowColumn18.leftAttachment: "attach_widget"
*rowColumn18.leftOffset: 2
*rowColumn18.leftWidget: "label25"
*rowColumn18.topAttachment: "attach_form"
*rowColumn18.topOffset: 66

*text20.class: text
*text20.parent: rowColumn18
*text20.static: true
*text20.name: text20
*text20.x: 25
*text20.y: 14
*text20.width: 65
*text20.height: 32
*text20.background: TextBackground
*text20.fontList: TextFont
*text20.foreground: TextForeground
*text20.highlightThickness: 2
*text20.shadowThickness: 2
*text20.marginHeight: 2
*text20.marginWidth: 1
*text20.maxLength: 100
*text20.columns: 8

*arrowButton22.class: arrowButton
*arrowButton22.parent: rowColumn18
*arrowButton22.static: true
*arrowButton22.name: arrowButton22
*arrowButton22.x: 92
*arrowButton22.y: 40
*arrowButton22.width: 23
*arrowButton22.height: 15
*arrowButton22.background: TextBackground
*arrowButton22.shadowThickness: 0
*arrowButton22.arrowDirection: "arrow_down"
*arrowButton22.translations: Arrows2
*arrowButton22.foreground: "PaleGreen"
*arrowButton22.borderColor: ApplicBackground
*arrowButton22.highlightThickness: 0

*arrowButton21.class: arrowButton
*arrowButton21.parent: rowColumn18
*arrowButton21.static: true
*arrowButton21.name: arrowButton21
*arrowButton21.x: 92
*arrowButton21.y: 0
*arrowButton21.width: 23
*arrowButton21.height: 15
*arrowButton21.background: TextBackground
*arrowButton21.shadowThickness: 0
*arrowButton21.translations: Arrows2
*arrowButton21.foreground: "PaleGreen"
*arrowButton21.highlightThickness: 0

*arrowButton23.class: arrowButton
*arrowButton23.parent: rowColumn18
*arrowButton23.static: true
*arrowButton23.name: arrowButton23
*arrowButton23.x: 0
*arrowButton23.y: 16
*arrowButton23.width: 15
*arrowButton23.height: 23
*arrowButton23.arrowDirection: "arrow_left"
*arrowButton23.background: TextBackground
*arrowButton23.foreground: "PaleGreen"
*arrowButton23.highlightThickness: 0
*arrowButton23.shadowThickness: 0
*arrowButton23.translations: Arrows2

*arrowButton24.class: arrowButton
*arrowButton24.parent: rowColumn18
*arrowButton24.static: true
*arrowButton24.name: arrowButton24
*arrowButton24.x: 195
*arrowButton24.y: 16
*arrowButton24.width: 15
*arrowButton24.height: 23
*arrowButton24.arrowDirection: "arrow_right"
*arrowButton24.background: TextBackground
*arrowButton24.foreground: "PaleGreen"
*arrowButton24.highlightThickness: 0
*arrowButton24.shadowThickness: 0
*arrowButton24.translations: Arrows2

*text24.class: text
*text24.parent: rowColumn18
*text24.static: true
*text24.name: text24
*text24.x: 120
*text24.y: 14
*text24.width: 65
*text24.height: 32
*text24.background: TextBackground
*text24.fontList: TextFont
*text24.foreground: TextForeground
*text24.highlightThickness: 2
*text24.shadowThickness: 2
*text24.marginHeight: 2
*text24.marginWidth: 1
*text24.maxLength: 100
*text24.columns: 8

*rowColumn31.class: rowColumn
*rowColumn31.parent: form2
*rowColumn31.static: true
*rowColumn31.name: rowColumn31
*rowColumn31.x: 98
*rowColumn31.y: 100
*rowColumn31.width: 105
*rowColumn31.height: 40
*rowColumn31.orientation: "horizontal"
*rowColumn31.packing: "pack_none"
*rowColumn31.spacing: 0
*rowColumn31.resizeHeight: "false"
*rowColumn31.resizeWidth: "false"
*rowColumn31.marginHeight: 0
*rowColumn31.marginWidth: 0
*rowColumn31.numColumns: 2
*rowColumn31.background: ApplicBackground
*rowColumn31.leftAttachment: "attach_widget"
*rowColumn31.leftOffset: 0
*rowColumn31.leftWidget: "label15"
*rowColumn31.topAttachment: "attach_form"
*rowColumn31.topOffset: 125

*text15.class: text
*text15.parent: rowColumn31
*text15.static: true
*text15.name: text15
*text15.x: 5
*text15.y: 7
*text15.width: 65
*text15.height: 32
*text15.background: TextBackground
*text15.activateCallback: {\
\
}
*text15.foreground: TextForeground
*text15.fontList: TextFont
*text15.maxLength: 240
*text15.columns: 8
*text15.marginHeight: 2
*text15.marginWidth: 1

*rowColumn32.class: rowColumn
*rowColumn32.parent: rowColumn31
*rowColumn32.static: true
*rowColumn32.name: rowColumn32
*rowColumn32.x: 72
*rowColumn32.y: 4
*rowColumn32.width: 23
*rowColumn32.height: 32
*rowColumn32.resizeHeight: "false"
*rowColumn32.resizeWidth: "false"
*rowColumn32.background: ApplicBackground
*rowColumn32.marginHeight: 0
*rowColumn32.marginWidth: 0
*rowColumn32.packing: "pack_none"
*rowColumn32.spacing: 0
*rowColumn32.adjustMargin: "true"
*rowColumn32.adjustLast: "true"
*rowColumn32.entryAlignment: "alignment_center"

*arrowButton25.class: arrowButton
*arrowButton25.parent: rowColumn32
*arrowButton25.static: true
*arrowButton25.name: arrowButton25
*arrowButton25.x: 0
*arrowButton25.y: 0
*arrowButton25.width: 23
*arrowButton25.height: 15
*arrowButton25.background: TextBackground
*arrowButton25.shadowThickness: 0
*arrowButton25.translations: Arrows2
*arrowButton25.foreground: "PaleGreen"
*arrowButton25.highlightThickness: 0

*arrowButton26.class: arrowButton
*arrowButton26.parent: rowColumn32
*arrowButton26.static: true
*arrowButton26.name: arrowButton26
*arrowButton26.x: 0
*arrowButton26.y: 16
*arrowButton26.width: 23
*arrowButton26.height: 15
*arrowButton26.background: TextBackground
*arrowButton26.shadowThickness: 0
*arrowButton26.arrowDirection: "arrow_down"
*arrowButton26.translations: Arrows2
*arrowButton26.foreground: "PaleGreen"
*arrowButton26.borderColor: ApplicBackground
*arrowButton26.highlightThickness: 0

*label28.class: label
*label28.parent: form2
*label28.static: true
*label28.name: label28
*label28.x: 10
*label28.y: 199
*label28.width: 60
*label28.height: 25
*label28.labelString: "HighCut"
*label28.foreground: TextForeground
*label28.background: LabelBackground
*label28.alignment: "alignment_center"
*label28.recomputeSize: "false"
*label28.translations: ""
*label28.fontList: BoldTextFont
*label28.leftAttachment: "attach_form"
*label28.leftOffset: 275
*label28.topAttachment: "attach_form"
*label28.topOffset: 170

*rowColumn33.class: rowColumn
*rowColumn33.parent: form2
*rowColumn33.static: true
*rowColumn33.name: rowColumn33
*rowColumn33.x: 98
*rowColumn33.y: 100
*rowColumn33.width: 105
*rowColumn33.height: 40
*rowColumn33.orientation: "horizontal"
*rowColumn33.packing: "pack_none"
*rowColumn33.spacing: 0
*rowColumn33.resizeHeight: "false"
*rowColumn33.resizeWidth: "false"
*rowColumn33.marginHeight: 0
*rowColumn33.marginWidth: 0
*rowColumn33.numColumns: 2
*rowColumn33.background: ApplicBackground
*rowColumn33.leftAttachment: "attach_widget"
*rowColumn33.leftOffset: 0
*rowColumn33.leftWidget: "label28"
*rowColumn33.topAttachment: "attach_form"
*rowColumn33.topOffset: 160

*text22.class: text
*text22.parent: rowColumn33
*text22.static: true
*text22.name: text22
*text22.x: 5
*text22.y: 7
*text22.width: 65
*text22.height: 32
*text22.background: TextBackground
*text22.activateCallback: {\
\
}
*text22.foreground: TextForeground
*text22.fontList: TextFont
*text22.maxLength: 240
*text22.columns: 8
*text22.marginHeight: 2
*text22.marginWidth: 1

*rowColumn34.class: rowColumn
*rowColumn34.parent: rowColumn33
*rowColumn34.static: true
*rowColumn34.name: rowColumn34
*rowColumn34.x: 72
*rowColumn34.y: 6
*rowColumn34.width: 23
*rowColumn34.height: 32
*rowColumn34.resizeHeight: "false"
*rowColumn34.resizeWidth: "false"
*rowColumn34.background: ApplicBackground
*rowColumn34.marginHeight: 0
*rowColumn34.marginWidth: 0
*rowColumn34.packing: "pack_none"
*rowColumn34.spacing: 0
*rowColumn34.adjustMargin: "true"
*rowColumn34.adjustLast: "true"
*rowColumn34.entryAlignment: "alignment_center"

*arrowButton27.class: arrowButton
*arrowButton27.parent: rowColumn34
*arrowButton27.static: true
*arrowButton27.name: arrowButton27
*arrowButton27.x: 0
*arrowButton27.y: 0
*arrowButton27.width: 23
*arrowButton27.height: 15
*arrowButton27.background: TextBackground
*arrowButton27.shadowThickness: 0
*arrowButton27.translations: Arrows2
*arrowButton27.foreground: "PaleGreen"
*arrowButton27.highlightThickness: 0

*arrowButton28.class: arrowButton
*arrowButton28.parent: rowColumn34
*arrowButton28.static: true
*arrowButton28.name: arrowButton28
*arrowButton28.x: 0
*arrowButton28.y: 16
*arrowButton28.width: 23
*arrowButton28.height: 15
*arrowButton28.background: TextBackground
*arrowButton28.shadowThickness: 0
*arrowButton28.arrowDirection: "arrow_down"
*arrowButton28.translations: Arrows2
*arrowButton28.foreground: "PaleGreen"
*arrowButton28.borderColor: ApplicBackground
*arrowButton28.highlightThickness: 0

*label35.class: label
*label35.parent: form2
*label35.static: true
*label35.name: label35
*label35.x: 20
*label35.y: 110
*label35.width: 100
*label35.height: 20
*label35.labelString: "Descr Values"
*label35.foreground: TextForeground
*label35.background: LabelBackground
*label35.alignment: "alignment_center"
*label35.recomputeSize: "false"
*label35.translations: ""
*label35.fontList: BoldTextFont
*label35.leftAttachment: "attach_form"
*label35.leftOffset: 5
*label35.topAttachment: "attach_form"
*label35.topOffset: 215

*toggleButton1.class: toggleButton
*toggleButton1.parent: form2
*toggleButton1.static: true
*toggleButton1.name: toggleButton1
*toggleButton1.x: 0
*toggleButton1.y: 0
*toggleButton1.width: 70
*toggleButton1.height: 28
*toggleButton1.labelString: "Yes"
*toggleButton1.indicatorType: "one_of_many"
*toggleButton1.background: TextBackground
*toggleButton1.fontList: BoldTextFont
*toggleButton1.foreground: TextForeground
*toggleButton1.recomputeSize: "false"
*toggleButton1.borderWidth: 1
*toggleButton1.set: "true"
*toggleButton1.indicatorOn: "true"
*toggleButton1.indicatorSize: 16
*toggleButton1.selectColor: "Palegreen"
*toggleButton1.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"No");\
   }\
\
}
*toggleButton1.topAttachment: "attach_form"
*toggleButton1.topOffset: 203
*toggleButton1.leftAttachment: "attach_widget"
*toggleButton1.leftOffset: 5
*toggleButton1.leftWidget: "label35"

*label36.class: label
*label36.parent: form2
*label36.static: true
*label36.name: label36
*label36.x: 15
*label36.y: 180
*label36.width: 62
*label36.height: 25
*label36.labelString: "CutDelta"
*label36.foreground: TextForeground
*label36.background: LabelBackground
*label36.alignment: "alignment_center"
*label36.recomputeSize: "false"
*label36.translations: ""
*label36.fontList: BoldTextFont
*label36.leftAttachment: "attach_form"
*label36.leftOffset: 274
*label36.topAttachment: "attach_form"
*label36.topOffset: 205

*text25.class: text
*text25.parent: form2
*text25.static: true
*text25.name: text25
*text25.x: 5
*text25.y: 7
*text25.width: 65
*text25.height: 30
*text25.background: TextBackground
*text25.activateCallback: {\
\
}
*text25.foreground: TextForeground
*text25.fontList: TextFont
*text25.maxLength: 240
*text25.columns: 8
*text25.marginHeight: 2
*text25.marginWidth: 1
*text25.leftAttachment: "attach_widget"
*text25.leftOffset: 3
*text25.leftWidget: "label36"
*text25.topAttachment: "attach_form"
*text25.topOffset: 201

*label10.class: label
*label10.parent: form2
*label10.static: true
*label10.name: label10
*label10.x: 10
*label10.y: 119
*label10.width: 65
*label10.height: 25
*label10.labelString: "MinMax"
*label10.foreground: TextForeground
*label10.background: LabelBackground
*label10.alignment: "alignment_center"
*label10.recomputeSize: "false"
*label10.translations: ""
*label10.fontList: BoldTextFont
*label10.leftAttachment: "attach_form"
*label10.leftOffset: 5
*label10.topAttachment: "attach_form"
*label10.topOffset: 170

*rowColumn17.class: rowColumn
*rowColumn17.parent: form2
*rowColumn17.static: true
*rowColumn17.name: rowColumn17
*rowColumn17.x: 98
*rowColumn17.y: 100
*rowColumn17.width: 170
*rowColumn17.height: 32
*rowColumn17.orientation: "horizontal"
*rowColumn17.packing: "pack_none"
*rowColumn17.spacing: 0
*rowColumn17.resizeHeight: "false"
*rowColumn17.resizeWidth: "false"
*rowColumn17.marginHeight: 0
*rowColumn17.marginWidth: 0
*rowColumn17.numColumns: 2
*rowColumn17.background: ApplicBackground
*rowColumn17.leftAttachment: "attach_widget"
*rowColumn17.leftOffset: 0
*rowColumn17.leftWidget: "label10"
*rowColumn17.topAttachment: "attach_form"
*rowColumn17.topOffset: 167

*text11.class: text
*text11.parent: rowColumn17
*text11.static: true
*text11.name: text11
*text11.x: 0
*text11.y: 2
*text11.width: 60
*text11.height: 32
*text11.background: SHelpBackground
*text11.activateCallback: {\
\
}
*text11.foreground: TextForeground
*text11.fontList: TextFont
*text11.maxLength: 40
*text11.columns: 8
*text11.marginHeight: 2
*text11.marginWidth: 1
*text11.editable: "false"
*text11.highlightThickness: 0
*text11.cursorPositionVisible: "false"

*text12.class: text
*text12.parent: rowColumn17
*text12.static: true
*text12.name: text12
*text12.x: 85
*text12.y: 2
*text12.width: 60
*text12.height: 32
*text12.background: SHelpBackground
*text12.activateCallback: {\
\
}
*text12.foreground: TextForeground
*text12.fontList: TextFont
*text12.maxLength: 40
*text12.columns: 8
*text12.marginHeight: 2
*text12.marginWidth: 1
*text12.editable: "false"
*text12.highlightThickness: 0
*text12.cursorPositionVisible: "false"

*rowColumn29.class: rowColumn
*rowColumn29.parent: form2
*rowColumn29.static: true
*rowColumn29.name: rowColumn29
*rowColumn29.x: 98
*rowColumn29.y: 55
*rowColumn29.width: 70
*rowColumn29.height: 40
*rowColumn29.orientation: "horizontal"
*rowColumn29.packing: "pack_none"
*rowColumn29.spacing: 0
*rowColumn29.resizeHeight: "false"
*rowColumn29.resizeWidth: "false"
*rowColumn29.marginHeight: 0
*rowColumn29.marginWidth: 0
*rowColumn29.numColumns: 2
*rowColumn29.background: ApplicBackground
*rowColumn29.leftAttachment: "attach_widget"
*rowColumn29.leftOffset: 0
*rowColumn29.leftWidget: "label24"
*rowColumn29.topAttachment: "attach_form"
*rowColumn29.topOffset: 125

*rowColumn30.class: rowColumn
*rowColumn30.parent: rowColumn29
*rowColumn30.static: true
*rowColumn30.name: rowColumn30
*rowColumn30.x: 44
*rowColumn30.y: 6
*rowColumn30.width: 23
*rowColumn30.height: 32
*rowColumn30.resizeHeight: "false"
*rowColumn30.resizeWidth: "false"
*rowColumn30.background: ApplicBackground
*rowColumn30.marginHeight: 0
*rowColumn30.marginWidth: 0
*rowColumn30.packing: "pack_none"
*rowColumn30.spacing: 0
*rowColumn30.adjustMargin: "true"
*rowColumn30.adjustLast: "true"
*rowColumn30.entryAlignment: "alignment_center"

*arrowButton39.class: arrowButton
*arrowButton39.parent: rowColumn30
*arrowButton39.static: true
*arrowButton39.name: arrowButton39
*arrowButton39.x: 0
*arrowButton39.y: 0
*arrowButton39.width: 23
*arrowButton39.height: 15
*arrowButton39.background: TextBackground
*arrowButton39.shadowThickness: 0
*arrowButton39.translations: Arrows2
*arrowButton39.foreground: "PaleGreen"
*arrowButton39.highlightThickness: 0

*arrowButton40.class: arrowButton
*arrowButton40.parent: rowColumn30
*arrowButton40.static: true
*arrowButton40.name: arrowButton40
*arrowButton40.x: 0
*arrowButton40.y: 16
*arrowButton40.width: 23
*arrowButton40.height: 15
*arrowButton40.background: TextBackground
*arrowButton40.shadowThickness: 0
*arrowButton40.arrowDirection: "arrow_down"
*arrowButton40.translations: Arrows2
*arrowButton40.foreground: "PaleGreen"
*arrowButton40.borderColor: ApplicBackground
*arrowButton40.highlightThickness: 0

*text39.class: text
*text39.parent: rowColumn29
*text39.static: true
*text39.name: text39
*text39.x: 0
*text39.y: 7
*text39.width: 40
*text39.height: 32
*text39.background: TextBackground
*text39.fontList: TextFont
*text39.foreground: TextForeground
*text39.highlightThickness: 2
*text39.shadowThickness: 2
*text39.marginHeight: 2
*text39.marginWidth: 2
*text39.maxLength: 100

*label52.class: label
*label52.parent: form2
*label52.static: true
*label52.name: label52
*label52.x: 10
*label52.y: 119
*label52.width: 60
*label52.height: 25
*label52.labelString: "ScaleY"
*label52.foreground: TextForeground
*label52.background: LabelBackground
*label52.alignment: "alignment_center"
*label52.recomputeSize: "false"
*label52.translations: ""
*label52.fontList: BoldTextFont
*label52.leftAttachment: "attach_form"
*label52.leftOffset: 135
*label52.topAttachment: "attach_form"
*label52.topOffset: 135

*rowColumn44.class: rowColumn
*rowColumn44.parent: form2
*rowColumn44.static: true
*rowColumn44.name: rowColumn44
*rowColumn44.x: 98
*rowColumn44.y: 55
*rowColumn44.width: 70
*rowColumn44.height: 40
*rowColumn44.orientation: "horizontal"
*rowColumn44.packing: "pack_none"
*rowColumn44.spacing: 0
*rowColumn44.resizeHeight: "false"
*rowColumn44.resizeWidth: "false"
*rowColumn44.marginHeight: 0
*rowColumn44.marginWidth: 0
*rowColumn44.numColumns: 2
*rowColumn44.background: ApplicBackground
*rowColumn44.leftAttachment: "attach_widget"
*rowColumn44.leftOffset: 0
*rowColumn44.leftWidget: "label52"
*rowColumn44.topAttachment: "attach_form"
*rowColumn44.topOffset: 125

*rowColumn45.class: rowColumn
*rowColumn45.parent: rowColumn44
*rowColumn45.static: true
*rowColumn45.name: rowColumn45
*rowColumn45.x: 44
*rowColumn45.y: 6
*rowColumn45.width: 23
*rowColumn45.height: 32
*rowColumn45.resizeHeight: "false"
*rowColumn45.resizeWidth: "false"
*rowColumn45.background: ApplicBackground
*rowColumn45.marginHeight: 0
*rowColumn45.marginWidth: 0
*rowColumn45.packing: "pack_none"
*rowColumn45.spacing: 0
*rowColumn45.adjustMargin: "true"
*rowColumn45.adjustLast: "true"
*rowColumn45.entryAlignment: "alignment_center"

*arrowButton45.class: arrowButton
*arrowButton45.parent: rowColumn45
*arrowButton45.static: true
*arrowButton45.name: arrowButton45
*arrowButton45.x: 0
*arrowButton45.y: 0
*arrowButton45.width: 23
*arrowButton45.height: 15
*arrowButton45.background: TextBackground
*arrowButton45.shadowThickness: 0
*arrowButton45.translations: Arrows2
*arrowButton45.foreground: "PaleGreen"
*arrowButton45.highlightThickness: 0

*arrowButton46.class: arrowButton
*arrowButton46.parent: rowColumn45
*arrowButton46.static: true
*arrowButton46.name: arrowButton46
*arrowButton46.x: 0
*arrowButton46.y: 16
*arrowButton46.width: 23
*arrowButton46.height: 15
*arrowButton46.background: TextBackground
*arrowButton46.shadowThickness: 0
*arrowButton46.arrowDirection: "arrow_down"
*arrowButton46.translations: Arrows2
*arrowButton46.foreground: "PaleGreen"
*arrowButton46.borderColor: ApplicBackground
*arrowButton46.highlightThickness: 0

*text40.class: text
*text40.parent: rowColumn44
*text40.static: true
*text40.name: text40
*text40.x: 0
*text40.y: 7
*text40.width: 40
*text40.height: 32
*text40.background: TextBackground
*text40.fontList: TextFont
*text40.foreground: TextForeground
*text40.highlightThickness: 2
*text40.shadowThickness: 2
*text40.marginHeight: 2
*text40.marginWidth: 2
*text40.maxLength: 100

*rowColumn22.class: rowColumn
*rowColumn22.parent: form2
*rowColumn22.static: true
*rowColumn22.name: rowColumn22
*rowColumn22.x: 119
*rowColumn22.y: 0
*rowColumn22.width: 50
*rowColumn22.height: 44
*rowColumn22.orientation: "horizontal"
*rowColumn22.packing: "pack_none"
*rowColumn22.spacing: 0
*rowColumn22.resizeHeight: "false"
*rowColumn22.resizeWidth: "false"
*rowColumn22.marginHeight: 0
*rowColumn22.marginWidth: 0
*rowColumn22.numColumns: 2
*rowColumn22.background: ApplicBackground
*rowColumn22.leftAttachment: "attach_widget"
*rowColumn22.leftOffset: 10
*rowColumn22.leftWidget: "label32"
*rowColumn22.topAttachment: "attach_form"
*rowColumn22.topOffset: 38

*rowColumn28.class: rowColumn
*rowColumn28.parent: rowColumn22
*rowColumn28.static: true
*rowColumn28.name: rowColumn28
*rowColumn28.x: 25
*rowColumn28.y: 6
*rowColumn28.width: 23
*rowColumn28.height: 32
*rowColumn28.resizeHeight: "false"
*rowColumn28.resizeWidth: "false"
*rowColumn28.background: ApplicBackground
*rowColumn28.marginHeight: 0
*rowColumn28.marginWidth: 0
*rowColumn28.packing: "pack_none"
*rowColumn28.spacing: 0
*rowColumn28.adjustMargin: "true"
*rowColumn28.adjustLast: "true"
*rowColumn28.entryAlignment: "alignment_center"

*arrowButton47.class: arrowButton
*arrowButton47.parent: rowColumn28
*arrowButton47.static: true
*arrowButton47.name: arrowButton47
*arrowButton47.x: 0
*arrowButton47.y: 0
*arrowButton47.width: 23
*arrowButton47.height: 15
*arrowButton47.background: TextBackground
*arrowButton47.shadowThickness: 0
*arrowButton47.translations: Arrows2
*arrowButton47.foreground: "PaleGreen"
*arrowButton47.highlightThickness: 0

*arrowButton48.class: arrowButton
*arrowButton48.parent: rowColumn28
*arrowButton48.static: true
*arrowButton48.name: arrowButton48
*arrowButton48.y: 16
*arrowButton48.width: 23
*arrowButton48.height: 15
*arrowButton48.background: TextBackground
*arrowButton48.shadowThickness: 0
*arrowButton48.arrowDirection: "arrow_down"
*arrowButton48.translations: Arrows2
*arrowButton48.foreground: "PaleGreen"
*arrowButton48.borderColor: ApplicBackground
*arrowButton48.highlightThickness: 0

*text41.class: text
*text41.parent: rowColumn22
*text41.static: true
*text41.name: text41
*text41.x: 1
*text41.y: 7
*text41.width: 25
*text41.height: 32
*text41.background: TextBackground
*text41.fontList: TextFont
*text41.foreground: TextForeground
*text41.highlightThickness: 2
*text41.shadowThickness: 2
*text41.marginHeight: 2
*text41.marginWidth: 1
*text41.maxLength: 100
*text41.columns: 1

*label53.class: label
*label53.parent: form2
*label53.static: true
*label53.name: label53
*label53.x: 20
*label53.y: 110
*label53.width: 100
*label53.height: 20
*label53.labelString: "Use"
*label53.foreground: TextForeground
*label53.background: LabelBackground
*label53.alignment: "alignment_center"
*label53.recomputeSize: "false"
*label53.translations: ""
*label53.fontList: BoldTextFont
*label53.leftAttachment: "attach_form"
*label53.leftOffset: 10
*label53.topAttachment: "attach_form"
*label53.topOffset: 200

*SHelp.class: text
*SHelp.parent: workArea
*SHelp.static: true
*SHelp.name: SHelp
*SHelp.x: 10
*SHelp.y: 100
*SHelp.width: 410
*SHelp.height: 50
*SHelp.background: SHelpBackground
*SHelp.bottomAttachment: "attach_none"
*SHelp.bottomOffset: 0
*SHelp.leftAttachment: "attach_form"
*SHelp.leftOffset: 10
*SHelp.rightAttachment: "attach_form"
*SHelp.rightOffset: 10
*SHelp.foreground: TextForeground
*SHelp.topAttachment: "attach_widget"
*SHelp.topOffset: 6
*SHelp.fontList: TextFont
*SHelp.topPosition: 0
*SHelp.topWidget: "form2"

*pushButton1.class: pushButton
*pushButton1.parent: workArea
*pushButton1.static: true
*pushButton1.name: pushButton1
*pushButton1.x: 350
*pushButton1.y: 480
*pushButton1.width: 50
*pushButton1.height: 40
*pushButton1.background: ButtonBackground
*pushButton1.labelString: "Apply"
*pushButton1.activateCallback: {\
\
applycom(0);\
}
*pushButton1.translations: ""
*pushButton1.bottomAttachment: "attach_none"
*pushButton1.bottomOffset: 110
*pushButton1.rightAttachment: "attach_form"
*pushButton1.rightOffset: 15
*pushButton1.fontList: BoldTextFont
*pushButton1.foreground: "PaleGreen"
*pushButton1.recomputeSize: "false"
*pushButton1.topAttachment: "attach_widget"
*pushButton1.topOffset: 10
*pushButton1.topWidget: "SHelp"

*form4.class: form
*form4.parent: workArea
*form4.static: true
*form4.name: form4
*form4.resizePolicy: "resize_none"
*form4.x: 10
*form4.y: 10
*form4.width: 430
*form4.height: 240
*form4.background: ApplicBackground
*form4.borderWidth: 1
*form4.mappedWhenManaged: "false"
*form4.createCallback: {\
\
}
*form4.leftAttachment: "attach_form"
*form4.leftOffset: 4
*form4.rightAttachment: "attach_form"
*form4.rightOffset: 4
*form4.topAttachment: "attach_form"
*form4.topOffset: 4

*label2.class: label
*label2.parent: form4
*label2.static: true
*label2.name: label2
*label2.x: 10
*label2.y: 10
*label2.width: 89
*label2.height: 25
*label2.labelString: "LUT"
*label2.foreground: TextForeground
*label2.background: LabelBackground
*label2.alignment: "alignment_center"
*label2.recomputeSize: "false"
*label2.translations: ""
*label2.fontList: BoldTextFont
*label2.leftAttachment: "attach_form"
*label2.leftOffset: 5
*label2.topAttachment: "attach_form"
*label2.topOffset: 20

*text9.class: text
*text9.parent: form4
*text9.static: true
*text9.name: text9
*text9.x: 10
*text9.y: 39
*text9.width: 379
*text9.height: 30
*text9.background: TextBackground
*text9.translations: SelectFile
*text9.foreground: TextForeground
*text9.fontList: TextFont
*text9.topAttachment: "attach_form"
*text9.topOffset: 20
*text9.leftAttachment: "attach_widget"
*text9.leftOffset: 5
*text9.rightAttachment: "attach_form"
*text9.rightOffset: 60
*text9.leftWidget: "label2"
*text9.marginHeight: 2
*text9.marginWidth: 2

*label1.class: label
*label1.parent: form4
*label1.static: true
*label1.name: label1
*label1.x: 9
*label1.y: 99
*label1.width: 120
*label1.height: 25
*label1.labelString: "DisplayLUT"
*label1.foreground: TextForeground
*label1.background: LabelBackground
*label1.alignment: "alignment_center"
*label1.recomputeSize: "false"
*label1.translations: ""
*label1.fontList: BoldTextFont
*label1.leftAttachment: "attach_form"
*label1.leftOffset: 5
*label1.topAttachment: "attach_form"
*label1.topOffset: 65
*label1.topWidget: "text9"

*toggleButton10.class: toggleButton
*toggleButton10.parent: form4
*toggleButton10.static: true
*toggleButton10.name: toggleButton10
*toggleButton10.x: 129
*toggleButton10.y: 99
*toggleButton10.width: 70
*toggleButton10.height: 28
*toggleButton10.labelString: "Yes"
*toggleButton10.indicatorType: "one_of_many"
*toggleButton10.background: TextBackground
*toggleButton10.fontList: BoldTextFont
*toggleButton10.foreground: TextForeground
*toggleButton10.recomputeSize: "false"
*toggleButton10.borderWidth: 1
*toggleButton10.set: "true"
*toggleButton10.indicatorOn: "true"
*toggleButton10.indicatorSize: 16
*toggleButton10.selectColor: "Palegreen"
*toggleButton10.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"No");\
   }\
\
}
*toggleButton10.leftAttachment: "attach_widget"
*toggleButton10.leftOffset: 10
*toggleButton10.leftWidget: "label1"
*toggleButton10.topAttachment: "attach_form"
*toggleButton10.topOffset: 60

*label26.class: label
*label26.parent: form4
*label26.static: true
*label26.name: label26
*label26.x: 15
*label26.y: 30
*label26.width: 89
*label26.height: 25
*label26.labelString: "ITT"
*label26.foreground: TextForeground
*label26.background: LabelBackground
*label26.alignment: "alignment_center"
*label26.recomputeSize: "false"
*label26.translations: ""
*label26.fontList: BoldTextFont
*label26.leftAttachment: "attach_form"
*label26.leftOffset: 5
*label26.topAttachment: "attach_form"
*label26.topOffset: 140

*text23.class: text
*text23.parent: form4
*text23.static: true
*text23.name: text23
*text23.x: 10
*text23.y: 39
*text23.width: 379
*text23.height: 30
*text23.background: TextBackground
*text23.translations: SelectFile
*text23.foreground: TextForeground
*text23.fontList: TextFont
*text23.marginHeight: 2
*text23.marginWidth: 2
*text23.leftAttachment: "attach_widget"
*text23.leftOffset: 5
*text23.leftWidget: "label26"
*text23.rightAttachment: "attach_form"
*text23.rightOffset: 60
*text23.topAttachment: "attach_form"
*text23.topCharacter: 1
*text23.topOffset: 140

*form3.class: form
*form3.parent: workArea
*form3.static: true
*form3.name: form3
*form3.resizePolicy: "resize_none"
*form3.x: 10
*form3.y: 10
*form3.width: 430
*form3.height: 240
*form3.background: ApplicBackground
*form3.borderWidth: 1
*form3.mappedWhenManaged: "false"
*form3.createCallback: {\
\
}
*form3.mapCallback: {\
\
}
*form3.topAttachment: "attach_form"
*form3.topOffset: 4
*form3.rightAttachment: "attach_form"
*form3.rightOffset: 4
*form3.leftAttachment: "attach_form"
*form3.leftOffset: 4

*label9.class: label
*label9.parent: form3
*label9.static: true
*label9.name: label9
*label9.x: 10
*label9.y: 10
*label9.width: 89
*label9.height: 25
*label9.labelString: "Frame"
*label9.foreground: TextForeground
*label9.background: LabelBackground
*label9.alignment: "alignment_center"
*label9.recomputeSize: "false"
*label9.translations: ""
*label9.fontList: BoldTextFont
*label9.leftAttachment: "attach_form"
*label9.leftOffset: 5
*label9.topAttachment: "attach_form"
*label9.topOffset: 10

*text10.class: text
*text10.parent: form3
*text10.static: true
*text10.name: text10
*text10.x: 9
*text10.y: 39
*text10.width: 250
*text10.height: 30
*text10.background: TextBackground
*text10.translations: SelectFile
*text10.foreground: TextForeground
*text10.fontList: TextFont
*text10.columns: 30
*text10.leftAttachment: "attach_widget"
*text10.leftOffset: 18
*text10.rightAttachment: "attach_form"
*text10.rightOffset: 60
*text10.topAttachment: "attach_form"
*text10.topOffset: 5
*text10.leftWidget: "label9"
*text10.marginHeight: 2
*text10.marginWidth: 2

*label13.class: label
*label13.parent: form3
*label13.static: true
*label13.name: label13
*label13.x: 10
*label13.y: 79
*label13.width: 89
*label13.height: 25
*label13.labelString: "Out_Table"
*label13.foreground: TextForeground
*label13.background: LabelBackground
*label13.alignment: "alignment_center"
*label13.recomputeSize: "false"
*label13.translations: ""
*label13.fontList: BoldTextFont
*label13.leftAttachment: "attach_form"
*label13.leftOffset: 5
*label13.topAttachment: "attach_form"
*label13.topOffset: 145
*label13.topWidget: "text10"

*text17.class: text
*text17.parent: form3
*text17.static: true
*text17.name: text17
*text17.x: 119
*text17.y: 79
*text17.width: 340
*text17.height: 32
*text17.background: TextBackground
*text17.foreground: TextForeground
*text17.fontList: TextFont
*text17.columns: 25
*text17.leftAttachment: "attach_widget"
*text17.leftOffset: 18
*text17.leftWidget: "label13"
*text17.rightAttachment: "attach_form"
*text17.rightOffset: 60
*text17.topAttachment: "attach_form"
*text17.topOffset: 140
*text17.topWidget: "text10"
*text17.marginHeight: 2
*text17.marginWidth: 2

*label11.class: label
*label11.parent: form3
*label11.static: true
*label11.name: label11
*label11.x: 10
*label11.y: 119
*label11.width: 89
*label11.height: 25
*label11.labelString: "Plot_Option"
*label11.foreground: TextForeground
*label11.background: LabelBackground
*label11.alignment: "alignment_center"
*label11.recomputeSize: "false"
*label11.translations: ""
*label11.fontList: BoldTextFont
*label11.leftAttachment: "attach_form"
*label11.leftOffset: 5
*label11.topAttachment: "attach_form"
*label11.topOffset: 55
*label11.topWidget: "label13"

*label12.class: label
*label12.parent: form3
*label12.static: true
*label12.name: label12
*label12.x: 10
*label12.y: 159
*label12.width: 89
*label12.height: 25
*label12.labelString: "File_Format"
*label12.foreground: TextForeground
*label12.background: LabelBackground
*label12.alignment: "alignment_center"
*label12.recomputeSize: "false"
*label12.translations: ""
*label12.fontList: BoldTextFont
*label12.leftAttachment: "attach_form"
*label12.leftOffset: 5
*label12.topAttachment: "attach_form"
*label12.topOffset: 100
*label12.topWidget: "label11"

*toggleButton2.class: toggleButton
*toggleButton2.parent: form3
*toggleButton2.static: true
*toggleButton2.name: toggleButton2
*toggleButton2.x: 10
*toggleButton2.y: 10
*toggleButton2.width: 75
*toggleButton2.height: 28
*toggleButton2.labelString: "Plot"
*toggleButton2.indicatorType: "one_of_many"
*toggleButton2.background: TextBackground
*toggleButton2.fontList: BoldTextFont
*toggleButton2.foreground: TextForeground
*toggleButton2.recomputeSize: "false"
*toggleButton2.borderWidth: 1
*toggleButton2.set: "true"
*toggleButton2.indicatorOn: "true"
*toggleButton2.indicatorSize: 16
*toggleButton2.selectColor: "Palegreen"
*toggleButton2.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Plot");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"NoPlot");\
   }\
\
}
*toggleButton2.leftAttachment: "attach_widget"
*toggleButton2.leftOffset: 20
*toggleButton2.leftWidget: "label13"
*toggleButton2.topAttachment: "attach_form"
*toggleButton2.topOffset: 50
*toggleButton2.marginLeft: 25
*toggleButton2.marginRight: 0

*toggleButton4.class: toggleButton
*toggleButton4.parent: form3
*toggleButton4.static: true
*toggleButton4.name: toggleButton4
*toggleButton4.x: 10
*toggleButton4.y: 10
*toggleButton4.width: 75
*toggleButton4.height: 28
*toggleButton4.labelString: "Midas"
*toggleButton4.indicatorType: "one_of_many"
*toggleButton4.background: TextBackground
*toggleButton4.fontList: BoldTextFont
*toggleButton4.foreground: TextForeground
*toggleButton4.recomputeSize: "false"
*toggleButton4.borderWidth: 1
*toggleButton4.set: "true"
*toggleButton4.indicatorOn: "true"
*toggleButton4.indicatorSize: 16
*toggleButton4.selectColor: "Palegreen"
*toggleButton4.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Midas");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"FITS");\
   }\
\
}
*toggleButton4.leftAttachment: "attach_widget"
*toggleButton4.leftOffset: 20
*toggleButton4.leftWidget: "label12"
*toggleButton4.topAttachment: "attach_form"
*toggleButton4.topOffset: 95
*toggleButton4.topShadowColor: LabelBackground

*form6.class: form
*form6.parent: workArea
*form6.static: true
*form6.name: form6
*form6.resizePolicy: "resize_none"
*form6.x: 10
*form6.y: 10
*form6.width: 430
*form6.height: 240
*form6.background: ApplicBackground
*form6.borderWidth: 1
*form6.createCallback: {\
\
}
*form6.mappedWhenManaged: "true"
*form6.leftAttachment: "attach_form"
*form6.leftOffset: 4
*form6.rightAttachment: "attach_form"
*form6.rightOffset: 4
*form6.topAttachment: "attach_form"
*form6.topOffset: 4

*label16.class: label
*label16.parent: form6
*label16.static: true
*label16.name: label16
*label16.x: 9
*label16.y: 9
*label16.width: 84
*label16.height: 25
*label16.labelString: "Window_ID"
*label16.foreground: TextForeground
*label16.background: LabelBackground
*label16.alignment: "alignment_center"
*label16.recomputeSize: "false"
*label16.translations: ""
*label16.fontList: BoldTextFont
*label16.leftAttachment: "attach_form"
*label16.leftOffset: 10
*label16.topAttachment: "attach_form"
*label16.topOffset: 10

*label17.class: label
*label17.parent: form6
*label17.static: true
*label17.name: label17
*label17.x: 9
*label17.y: 49
*label17.width: 80
*label17.height: 25
*label17.labelString: "SizeX"
*label17.foreground: TextForeground
*label17.background: LabelBackground
*label17.alignment: "alignment_center"
*label17.recomputeSize: "false"
*label17.translations: ""
*label17.fontList: BoldTextFont
*label17.leftAttachment: "attach_form"
*label17.leftOffset: 10
*label17.topAttachment: "attach_form"
*label17.topOffset: 50
*label17.topWidget: "label16"

*label5.class: label
*label5.parent: form6
*label5.static: true
*label5.name: label5
*label5.x: 9
*label5.y: 89
*label5.width: 80
*label5.height: 25
*label5.labelString: "OffsetX"
*label5.foreground: TextForeground
*label5.background: LabelBackground
*label5.alignment: "alignment_center"
*label5.recomputeSize: "false"
*label5.translations: ""
*label5.fontList: BoldTextFont
*label5.leftAttachment: "attach_form"
*label5.leftOffset: 10
*label5.topAttachment: "attach_form"
*label5.topOffset: 90
*label5.topWidget: "label17"

*label19.class: label
*label19.parent: form6
*label19.static: true
*label19.name: label19
*label19.x: 9
*label19.y: 139
*label19.width: 120
*label19.height: 25
*label19.labelString: "Alpha_Memory"
*label19.foreground: TextForeground
*label19.background: LabelBackground
*label19.alignment: "alignment_center"
*label19.recomputeSize: "false"
*label19.translations: ""
*label19.fontList: BoldTextFont
*label19.leftAttachment: "attach_form"
*label19.leftOffset: 7
*label19.topAttachment: "attach_form"
*label19.topOffset: 210
*label19.topWidget: "label5"

*label20.class: label
*label20.parent: form6
*label20.static: true
*label20.name: label20
*label20.x: 9
*label20.y: 189
*label20.width: 80
*label20.height: 25
*label20.labelString: "Gsize"
*label20.foreground: TextForeground
*label20.background: LabelBackground
*label20.alignment: "alignment_center"
*label20.recomputeSize: "false"
*label20.translations: ""
*label20.fontList: BoldTextFont
*label20.leftAttachment: "attach_form"
*label20.leftOffset: 10
*label20.topAttachment: "attach_form"
*label20.topOffset: 128
*label20.topWidget: "label19"

*label21.class: label
*label21.parent: form6
*label21.static: true
*label21.name: label21
*label21.x: 9
*label21.y: 239
*label21.width: 80
*label21.height: 25
*label21.labelString: "Xstation"
*label21.foreground: TextForeground
*label21.background: LabelBackground
*label21.alignment: "alignment_center"
*label21.recomputeSize: "false"
*label21.translations: ""
*label21.fontList: BoldTextFont
*label21.leftAttachment: "attach_form"
*label21.leftOffset: 10
*label21.topAttachment: "attach_form"
*label21.topOffset: 167
*label21.topWidget: "label20"

*text21.class: text
*text21.parent: form6
*text21.static: true
*text21.name: text21
*text21.x: 109
*text21.y: 239
*text21.width: 160
*text21.height: 32
*text21.background: TextBackground
*text21.foreground: TextForeground
*text21.fontList: TextFont
*text21.borderWidth: 0
*text21.leftAttachment: "attach_widget"
*text21.leftWidget: "label21"
*text21.leftOffset: 9
*text21.rightAttachment: "attach_form"
*text21.rightOffset: 60
*text21.topAttachment: "attach_form"
*text21.topCharacter: 0
*text21.topOffset: 162
*text21.marginHeight: 2
*text21.marginWidth: 1

*rowColumn1.class: rowColumn
*rowColumn1.parent: form6
*rowColumn1.static: true
*rowColumn1.name: rowColumn1
*rowColumn1.x: 119
*rowColumn1.y: 0
*rowColumn1.width: 60
*rowColumn1.height: 44
*rowColumn1.orientation: "horizontal"
*rowColumn1.packing: "pack_none"
*rowColumn1.spacing: 0
*rowColumn1.leftAttachment: "attach_widget"
*rowColumn1.leftOffset: 3
*rowColumn1.leftWidget: "label16"
*rowColumn1.topAttachment: "attach_form"
*rowColumn1.resizable: "false"
*rowColumn1.resizeHeight: "false"
*rowColumn1.resizeWidth: "false"
*rowColumn1.marginHeight: 0
*rowColumn1.marginWidth: 0
*rowColumn1.numColumns: 2
*rowColumn1.background: ApplicBackground
*rowColumn1.topOffset: 0

*rowColumn9.class: rowColumn
*rowColumn9.parent: rowColumn1
*rowColumn9.static: true
*rowColumn9.name: rowColumn9
*rowColumn9.x: 30
*rowColumn9.y: 6
*rowColumn9.width: 23
*rowColumn9.height: 32
*rowColumn9.resizeHeight: "false"
*rowColumn9.resizeWidth: "false"
*rowColumn9.background: ApplicBackground
*rowColumn9.marginHeight: 0
*rowColumn9.marginWidth: 0
*rowColumn9.packing: "pack_none"
*rowColumn9.spacing: 0
*rowColumn9.adjustMargin: "true"
*rowColumn9.adjustLast: "true"
*rowColumn9.entryAlignment: "alignment_center"

*arrowButton7.class: arrowButton
*arrowButton7.parent: rowColumn9
*arrowButton7.static: true
*arrowButton7.name: arrowButton7
*arrowButton7.x: 0
*arrowButton7.y: 0
*arrowButton7.width: 23
*arrowButton7.height: 15
*arrowButton7.background: TextBackground
*arrowButton7.shadowThickness: 0
*arrowButton7.translations: Arrows6
*arrowButton7.foreground: "PaleGreen"
*arrowButton7.highlightThickness: 0

*arrowButton8.class: arrowButton
*arrowButton8.parent: rowColumn9
*arrowButton8.static: true
*arrowButton8.name: arrowButton8
*arrowButton8.x: 0
*arrowButton8.y: 16
*arrowButton8.width: 23
*arrowButton8.height: 15
*arrowButton8.background: TextBackground
*arrowButton8.shadowThickness: 0
*arrowButton8.arrowDirection: "arrow_down"
*arrowButton8.translations: Arrows6
*arrowButton8.foreground: "PaleGreen"
*arrowButton8.borderColor: ApplicBackground
*arrowButton8.highlightThickness: 0

*text1.class: text
*text1.parent: rowColumn1
*text1.static: true
*text1.name: text1
*text1.x: 5
*text1.y: 7
*text1.width: 25
*text1.height: 32
*text1.background: TextBackground
*text1.fontList: TextFont
*text1.foreground: TextForeground
*text1.highlightThickness: 2
*text1.shadowThickness: 2
*text1.marginHeight: 2
*text1.marginWidth: 1
*text1.maxLength: 100
*text1.columns: 1

*rowColumn2.class: rowColumn
*rowColumn2.parent: form6
*rowColumn2.static: true
*rowColumn2.name: rowColumn2
*rowColumn2.x: 114
*rowColumn2.y: 15
*rowColumn2.width: 100
*rowColumn2.height: 44
*rowColumn2.orientation: "horizontal"
*rowColumn2.packing: "pack_none"
*rowColumn2.spacing: 0
*rowColumn2.resizeHeight: "false"
*rowColumn2.resizeWidth: "false"
*rowColumn2.marginHeight: 0
*rowColumn2.marginWidth: 0
*rowColumn2.numColumns: 2
*rowColumn2.background: ApplicBackground
*rowColumn2.leftAttachment: "attach_widget"
*rowColumn2.leftOffset: 5
*rowColumn2.leftWidget: "label17"
*rowColumn2.topAttachment: "attach_form"
*rowColumn2.topOffset: 40

*rowColumn10.class: rowColumn
*rowColumn10.parent: rowColumn2
*rowColumn10.static: true
*rowColumn10.name: rowColumn10
*rowColumn10.x: 65
*rowColumn10.y: 6
*rowColumn10.width: 23
*rowColumn10.height: 32
*rowColumn10.resizeHeight: "false"
*rowColumn10.resizeWidth: "false"
*rowColumn10.background: ApplicBackground
*rowColumn10.marginHeight: 0
*rowColumn10.marginWidth: 0
*rowColumn10.packing: "pack_none"
*rowColumn10.spacing: 0
*rowColumn10.adjustMargin: "true"
*rowColumn10.adjustLast: "true"
*rowColumn10.entryAlignment: "alignment_center"

*arrowButton3.class: arrowButton
*arrowButton3.parent: rowColumn10
*arrowButton3.static: true
*arrowButton3.name: arrowButton3
*arrowButton3.x: 0
*arrowButton3.y: 0
*arrowButton3.width: 23
*arrowButton3.height: 15
*arrowButton3.background: TextBackground
*arrowButton3.shadowThickness: 0
*arrowButton3.translations: Arrows6
*arrowButton3.foreground: "PaleGreen"
*arrowButton3.highlightThickness: 0

*arrowButton4.class: arrowButton
*arrowButton4.parent: rowColumn10
*arrowButton4.static: true
*arrowButton4.name: arrowButton4
*arrowButton4.x: 0
*arrowButton4.y: 16
*arrowButton4.width: 23
*arrowButton4.height: 15
*arrowButton4.background: TextBackground
*arrowButton4.shadowThickness: 0
*arrowButton4.arrowDirection: "arrow_down"
*arrowButton4.translations: Arrows6
*arrowButton4.foreground: "PaleGreen"
*arrowButton4.borderColor: ApplicBackground
*arrowButton4.highlightThickness: 0

*text4.class: text
*text4.parent: rowColumn2
*text4.static: true
*text4.name: text4
*text4.x: 5
*text4.y: 7
*text4.width: 50
*text4.height: 32
*text4.background: TextBackground
*text4.fontList: TextFont
*text4.foreground: TextForeground
*text4.highlightThickness: 2
*text4.shadowThickness: 2
*text4.marginHeight: 2
*text4.marginWidth: 2
*text4.maxLength: 100

*label18.class: label
*label18.parent: form6
*label18.static: true
*label18.name: label18
*label18.x: 209
*label18.y: 59
*label18.width: 80
*label18.height: 25
*label18.labelString: "SizeY"
*label18.foreground: TextForeground
*label18.background: LabelBackground
*label18.alignment: "alignment_center"
*label18.recomputeSize: "false"
*label18.translations: ""
*label18.fontList: BoldTextFont
*label18.leftAttachment: "attach_form"
*label18.leftOffset: 210
*label18.topAttachment: "attach_form"
*label18.topOffset: 50

*rowColumn3.class: rowColumn
*rowColumn3.parent: form6
*rowColumn3.static: true
*rowColumn3.name: rowColumn3
*rowColumn3.x: 112
*rowColumn3.y: 60
*rowColumn3.width: 100
*rowColumn3.height: 44
*rowColumn3.orientation: "horizontal"
*rowColumn3.packing: "pack_none"
*rowColumn3.spacing: 0
*rowColumn3.resizeHeight: "false"
*rowColumn3.resizeWidth: "false"
*rowColumn3.marginHeight: 0
*rowColumn3.marginWidth: 0
*rowColumn3.numColumns: 2
*rowColumn3.background: ApplicBackground
*rowColumn3.leftAttachment: "attach_widget"
*rowColumn3.leftOffset: 3
*rowColumn3.leftWidget: "label18"
*rowColumn3.topAttachment: "attach_form"
*rowColumn3.topOffset: 40

*rowColumn11.class: rowColumn
*rowColumn11.parent: rowColumn3
*rowColumn11.static: true
*rowColumn11.name: rowColumn11
*rowColumn11.x: 65
*rowColumn11.y: 6
*rowColumn11.width: 23
*rowColumn11.height: 32
*rowColumn11.resizeHeight: "false"
*rowColumn11.resizeWidth: "false"
*rowColumn11.background: ApplicBackground
*rowColumn11.marginHeight: 0
*rowColumn11.marginWidth: 0
*rowColumn11.packing: "pack_none"
*rowColumn11.spacing: 0
*rowColumn11.adjustMargin: "true"
*rowColumn11.adjustLast: "true"
*rowColumn11.entryAlignment: "alignment_center"

*arrowButton9.class: arrowButton
*arrowButton9.parent: rowColumn11
*arrowButton9.static: true
*arrowButton9.name: arrowButton9
*arrowButton9.x: 0
*arrowButton9.y: 0
*arrowButton9.width: 23
*arrowButton9.height: 15
*arrowButton9.background: TextBackground
*arrowButton9.shadowThickness: 0
*arrowButton9.translations: Arrows6
*arrowButton9.foreground: "PaleGreen"
*arrowButton9.highlightThickness: 0

*arrowButton10.class: arrowButton
*arrowButton10.parent: rowColumn11
*arrowButton10.static: true
*arrowButton10.name: arrowButton10
*arrowButton10.x: 0
*arrowButton10.y: 16
*arrowButton10.width: 23
*arrowButton10.height: 15
*arrowButton10.background: TextBackground
*arrowButton10.shadowThickness: 0
*arrowButton10.arrowDirection: "arrow_down"
*arrowButton10.translations: Arrows6
*arrowButton10.foreground: "PaleGreen"
*arrowButton10.borderColor: ApplicBackground
*arrowButton10.highlightThickness: 0

*text5.class: text
*text5.parent: rowColumn3
*text5.static: true
*text5.name: text5
*text5.x: 5
*text5.y: 7
*text5.width: 50
*text5.height: 32
*text5.background: TextBackground
*text5.fontList: TextFont
*text5.foreground: TextForeground
*text5.highlightThickness: 2
*text5.shadowThickness: 2
*text5.marginHeight: 2
*text5.marginWidth: 2
*text5.maxLength: 100

*rowColumn5.class: rowColumn
*rowColumn5.parent: form6
*rowColumn5.static: true
*rowColumn5.name: rowColumn5
*rowColumn5.x: 112
*rowColumn5.y: 60
*rowColumn5.width: 100
*rowColumn5.height: 44
*rowColumn5.orientation: "horizontal"
*rowColumn5.packing: "pack_none"
*rowColumn5.spacing: 0
*rowColumn5.resizeHeight: "false"
*rowColumn5.resizeWidth: "false"
*rowColumn5.marginHeight: 0
*rowColumn5.marginWidth: 0
*rowColumn5.numColumns: 2
*rowColumn5.background: ApplicBackground
*rowColumn5.leftAttachment: "attach_widget"
*rowColumn5.leftOffset: 3
*rowColumn5.leftWidget: "label5"
*rowColumn5.topAttachment: "attach_form"
*rowColumn5.topOffset: 80

*rowColumn12.class: rowColumn
*rowColumn12.parent: rowColumn5
*rowColumn12.static: true
*rowColumn12.name: rowColumn12
*rowColumn12.x: 65
*rowColumn12.y: 6
*rowColumn12.width: 23
*rowColumn12.height: 32
*rowColumn12.resizeHeight: "false"
*rowColumn12.resizeWidth: "false"
*rowColumn12.background: ApplicBackground
*rowColumn12.marginHeight: 0
*rowColumn12.marginWidth: 0
*rowColumn12.packing: "pack_none"
*rowColumn12.spacing: 0
*rowColumn12.adjustMargin: "true"
*rowColumn12.adjustLast: "true"
*rowColumn12.entryAlignment: "alignment_center"

*arrowButton11.class: arrowButton
*arrowButton11.parent: rowColumn12
*arrowButton11.static: true
*arrowButton11.name: arrowButton11
*arrowButton11.x: 0
*arrowButton11.y: 0
*arrowButton11.width: 23
*arrowButton11.height: 15
*arrowButton11.background: TextBackground
*arrowButton11.shadowThickness: 0
*arrowButton11.translations: Arrows6
*arrowButton11.foreground: "PaleGreen"
*arrowButton11.highlightThickness: 0

*arrowButton12.class: arrowButton
*arrowButton12.parent: rowColumn12
*arrowButton12.static: true
*arrowButton12.name: arrowButton12
*arrowButton12.x: 0
*arrowButton12.y: 16
*arrowButton12.width: 23
*arrowButton12.height: 15
*arrowButton12.background: TextBackground
*arrowButton12.shadowThickness: 0
*arrowButton12.arrowDirection: "arrow_down"
*arrowButton12.translations: Arrows6
*arrowButton12.foreground: "PaleGreen"
*arrowButton12.borderColor: ApplicBackground
*arrowButton12.highlightThickness: 0

*text6.class: text
*text6.parent: rowColumn5
*text6.static: true
*text6.name: text6
*text6.x: 5
*text6.y: 7
*text6.width: 50
*text6.height: 32
*text6.background: TextBackground
*text6.fontList: TextFont
*text6.foreground: TextForeground
*text6.highlightThickness: 2
*text6.shadowThickness: 2
*text6.marginHeight: 2
*text6.marginWidth: 2
*text6.maxLength: 100

*label22.class: label
*label22.parent: form6
*label22.static: true
*label22.name: label22
*label22.x: 20
*label22.y: 115
*label22.width: 80
*label22.height: 25
*label22.labelString: "OffsetY"
*label22.foreground: TextForeground
*label22.background: LabelBackground
*label22.alignment: "alignment_center"
*label22.recomputeSize: "false"
*label22.translations: ""
*label22.fontList: BoldTextFont
*label22.leftAttachment: "attach_form"
*label22.leftOffset: 210
*label22.topAttachment: "attach_form"
*label22.topOffset: 90

*rowColumn4.class: rowColumn
*rowColumn4.parent: form6
*rowColumn4.static: true
*rowColumn4.name: rowColumn4
*rowColumn4.x: 100
*rowColumn4.y: 105
*rowColumn4.width: 100
*rowColumn4.height: 44
*rowColumn4.orientation: "horizontal"
*rowColumn4.packing: "pack_none"
*rowColumn4.spacing: 0
*rowColumn4.resizeHeight: "false"
*rowColumn4.resizeWidth: "false"
*rowColumn4.marginHeight: 0
*rowColumn4.marginWidth: 0
*rowColumn4.numColumns: 2
*rowColumn4.background: ApplicBackground
*rowColumn4.leftAttachment: "attach_widget"
*rowColumn4.leftOffset: 3
*rowColumn4.leftWidget: "label22"
*rowColumn4.topAttachment: "attach_form"
*rowColumn4.topOffset: 80

*rowColumn14.class: rowColumn
*rowColumn14.parent: rowColumn4
*rowColumn14.static: true
*rowColumn14.name: rowColumn14
*rowColumn14.x: 65
*rowColumn14.y: 6
*rowColumn14.width: 23
*rowColumn14.height: 32
*rowColumn14.resizeHeight: "false"
*rowColumn14.resizeWidth: "false"
*rowColumn14.background: ApplicBackground
*rowColumn14.marginHeight: 0
*rowColumn14.marginWidth: 0
*rowColumn14.packing: "pack_none"
*rowColumn14.spacing: 0
*rowColumn14.adjustMargin: "true"
*rowColumn14.adjustLast: "true"
*rowColumn14.entryAlignment: "alignment_center"

*arrowButton17.class: arrowButton
*arrowButton17.parent: rowColumn14
*arrowButton17.static: true
*arrowButton17.name: arrowButton17
*arrowButton17.x: 0
*arrowButton17.y: 0
*arrowButton17.width: 23
*arrowButton17.height: 15
*arrowButton17.background: TextBackground
*arrowButton17.shadowThickness: 0
*arrowButton17.translations: Arrows6
*arrowButton17.foreground: "PaleGreen"
*arrowButton17.highlightThickness: 0

*arrowButton18.class: arrowButton
*arrowButton18.parent: rowColumn14
*arrowButton18.static: true
*arrowButton18.name: arrowButton18
*arrowButton18.x: 0
*arrowButton18.y: 16
*arrowButton18.width: 23
*arrowButton18.height: 15
*arrowButton18.background: TextBackground
*arrowButton18.shadowThickness: 0
*arrowButton18.arrowDirection: "arrow_down"
*arrowButton18.translations: Arrows6
*arrowButton18.foreground: "PaleGreen"
*arrowButton18.borderColor: ApplicBackground
*arrowButton18.highlightThickness: 0

*text8.class: text
*text8.parent: rowColumn4
*text8.static: true
*text8.name: text8
*text8.x: 5
*text8.y: 7
*text8.width: 50
*text8.height: 32
*text8.background: TextBackground
*text8.fontList: TextFont
*text8.foreground: TextForeground
*text8.highlightThickness: 2
*text8.shadowThickness: 2
*text8.marginHeight: 2
*text8.marginWidth: 2
*text8.maxLength: 100

*toggleButton25.class: toggleButton
*toggleButton25.parent: form6
*toggleButton25.static: true
*toggleButton25.name: toggleButton25
*toggleButton25.x: 0
*toggleButton25.y: 0
*toggleButton25.width: 70
*toggleButton25.height: 28
*toggleButton25.labelString: "Yes"
*toggleButton25.indicatorType: "one_of_many"
*toggleButton25.background: TextBackground
*toggleButton25.fontList: BoldTextFont
*toggleButton25.foreground: TextForeground
*toggleButton25.recomputeSize: "false"
*toggleButton25.borderWidth: 1
*toggleButton25.set: "true"
*toggleButton25.indicatorOn: "true"
*toggleButton25.indicatorSize: 16
*toggleButton25.selectColor: "Palegreen"
*toggleButton25.leftAttachment: "attach_widget"
*toggleButton25.leftOffset: 10
*toggleButton25.leftWidget: "label19"
*toggleButton25.topAttachment: "attach_form"
*toggleButton25.topOffset: 207
*toggleButton25.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"No");\
   }\
\
}

*rowColumn6.class: rowColumn
*rowColumn6.parent: form6
*rowColumn6.static: true
*rowColumn6.name: rowColumn6
*rowColumn6.x: 312
*rowColumn6.y: 110
*rowColumn6.width: 150
*rowColumn6.height: 35
*rowColumn6.orientation: "horizontal"
*rowColumn6.packing: "pack_none"
*rowColumn6.spacing: 0
*rowColumn6.resizeHeight: "false"
*rowColumn6.resizeWidth: "false"
*rowColumn6.marginHeight: 0
*rowColumn6.marginWidth: 0
*rowColumn6.numColumns: 2
*rowColumn6.background: ApplicBackground
*rowColumn6.leftAttachment: "attach_widget"
*rowColumn6.leftOffset: 3
*rowColumn6.leftWidget: "label20"
*rowColumn6.topAttachment: "attach_form"
*rowColumn6.topOffset: 123

*rowColumn7.class: rowColumn
*rowColumn7.parent: rowColumn6
*rowColumn7.static: true
*rowColumn7.name: rowColumn7
*rowColumn7.x: 100
*rowColumn7.y: 0
*rowColumn7.width: 23
*rowColumn7.height: 32
*rowColumn7.resizeHeight: "false"
*rowColumn7.resizeWidth: "false"
*rowColumn7.background: ApplicBackground
*rowColumn7.marginHeight: 0
*rowColumn7.marginWidth: 0
*rowColumn7.packing: "pack_none"
*rowColumn7.spacing: 0
*rowColumn7.adjustMargin: "true"
*rowColumn7.adjustLast: "true"
*rowColumn7.entryAlignment: "alignment_center"

*arrowButton13.class: arrowButton
*arrowButton13.parent: rowColumn7
*arrowButton13.static: true
*arrowButton13.name: arrowButton13
*arrowButton13.x: 0
*arrowButton13.y: 0
*arrowButton13.width: 23
*arrowButton13.height: 15
*arrowButton13.background: TextBackground
*arrowButton13.shadowThickness: 0
*arrowButton13.translations: Arrows6
*arrowButton13.foreground: "PaleGreen"
*arrowButton13.highlightThickness: 0

*arrowButton14.class: arrowButton
*arrowButton14.parent: rowColumn7
*arrowButton14.static: true
*arrowButton14.name: arrowButton14
*arrowButton14.x: 0
*arrowButton14.y: 16
*arrowButton14.width: 23
*arrowButton14.height: 15
*arrowButton14.background: TextBackground
*arrowButton14.shadowThickness: 0
*arrowButton14.arrowDirection: "arrow_down"
*arrowButton14.translations: Arrows6
*arrowButton14.foreground: "PaleGreen"
*arrowButton14.borderColor: ApplicBackground
*arrowButton14.highlightThickness: 0

*text13.class: text
*text13.parent: rowColumn6
*text13.static: true
*text13.name: text13
*text13.x: 5
*text13.y: 2
*text13.width: 90
*text13.height: 32
*text13.background: TextBackground
*text13.fontList: TextFont
*text13.foreground: TextForeground
*text13.highlightThickness: 2
*text13.shadowThickness: 2
*text13.marginHeight: 2
*text13.marginWidth: 1
*text13.maxLength: 100
*text13.columns: 10

*label54.class: label
*label54.parent: form6
*label54.static: true
*label54.name: label54
*label54.x: 20
*label54.y: 180
*label54.width: 80
*label54.height: 25
*label54.labelString: "Type"
*label54.foreground: TextForeground
*label54.background: LabelBackground
*label54.alignment: "alignment_center"
*label54.recomputeSize: "false"
*label54.translations: ""
*label54.fontList: BoldTextFont
*label54.leftAttachment: "attach_form"
*label54.leftOffset: 210
*label54.topAttachment: "attach_form"
*label54.topOffset: 10

*toggleButton15.class: toggleButton
*toggleButton15.parent: form6
*toggleButton15.static: true
*toggleButton15.name: toggleButton15
*toggleButton15.x: 0
*toggleButton15.y: 0
*toggleButton15.width: 85
*toggleButton15.height: 28
*toggleButton15.labelString: "Display"
*toggleButton15.indicatorType: "one_of_many"
*toggleButton15.background: TextBackground
*toggleButton15.fontList: BoldTextFont
*toggleButton15.foreground: TextForeground
*toggleButton15.recomputeSize: "false"
*toggleButton15.borderWidth: 1
*toggleButton15.set: "true"
*toggleButton15.indicatorOn: "true"
*toggleButton15.indicatorSize: 16
*toggleButton15.selectColor: "PaleGreen"
*toggleButton15.marginLeft: 2
*toggleButton15.topAttachment: "attach_form"
*toggleButton15.topOffset: 8
*toggleButton15.leftAttachment: "attach_widget"
*toggleButton15.leftOffset: 9
*toggleButton15.leftWidget: "label54"
*toggleButton15.valueChangedCallback: {\
 \
/* \
 `toggle6.c'  the Value change callback for toggle Button `Display/Graphics'  \
*/\
\
\
extern char dspinfo[4][8], grainfo[4][8];\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Display");\
   XtSetSensitive(UxGetWidget(label19),TRUE);\
   XtSetSensitive(UxGetWidget(toggleButton25),TRUE);\
\
   strcpy(grainfo[0],UxGetText(text4));\
   strcpy(grainfo[1],UxGetText(text5));\
   strcpy(grainfo[2],UxGetText(text6));\
   strcpy(grainfo[3],UxGetText(text8));\
 \
   UxPutText(text4,dspinfo[0]);\
   UxPutText(text5,dspinfo[1]);\
   UxPutText(text6,dspinfo[2]);\
   UxPutText(text8,dspinfo[3]);\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"Graphics");\
   XtSetSensitive(UxGetWidget(label19),FALSE);\
   XtSetSensitive(UxGetWidget(toggleButton25),FALSE);\
\
   strcpy(dspinfo[0],UxGetText(text4));\
   strcpy(dspinfo[1],UxGetText(text5));\
   strcpy(dspinfo[2],UxGetText(text6));\
   strcpy(dspinfo[3],UxGetText(text8));\
 \
   UxPutText(text4,grainfo[0]);\
   UxPutText(text5,grainfo[1]);\
   UxPutText(text6,grainfo[2]);\
   UxPutText(text8,grainfo[3]);\
   }\
\
\
}

*form7.class: form
*form7.parent: workArea
*form7.static: true
*form7.name: form7
*form7.resizePolicy: "resize_none"
*form7.x: 10
*form7.y: 10
*form7.width: 430
*form7.height: 240
*form7.background: ApplicBackground
*form7.borderWidth: 1
*form7.mappedWhenManaged: "false"
*form7.createCallback: {\
\
}
*form7.mapCallback: {\
\
}
*form7.leftAttachment: "attach_form"
*form7.leftOffset: 4
*form7.rightAttachment: "attach_form"
*form7.rightOffset: 4
*form7.topAttachment: "attach_form"
*form7.topOffset: 4

*label3.class: label
*label3.parent: form7
*label3.static: true
*label3.name: label3
*label3.x: 10
*label3.y: 10
*label3.width: 100
*label3.height: 25
*label3.labelString: "Out_specs"
*label3.foreground: TextForeground
*label3.background: LabelBackground
*label3.alignment: "alignment_center"
*label3.recomputeSize: "false"
*label3.translations: ""
*label3.fontList: BoldTextFont
*label3.leftAttachment: "attach_form"
*label3.leftOffset: 5
*label3.topAttachment: "attach_form"
*label3.topOffset: 10

*text3.class: text
*text3.parent: form7
*text3.static: true
*text3.name: text3
*text3.x: 10
*text3.y: 39
*text3.width: 379
*text3.height: 30
*text3.background: TextBackground
*text3.foreground: TextForeground
*text3.fontList: TextFont
*text3.columns: 20
*text3.leftAttachment: "attach_widget"
*text3.leftOffset: 13
*text3.topAttachment: "attach_form"
*text3.topOffset: 5
*text3.leftWidget: "label3"
*text3.rightAttachment: "attach_form"
*text3.rightOffset: 60
*text3.marginHeight: 2
*text3.marginWidth: 2

*label7.class: label
*label7.parent: form7
*label7.static: true
*label7.name: label7
*label7.x: 9
*label7.y: 89
*label7.width: 100
*label7.height: 25
*label7.labelString: "Mark Position"
*label7.foreground: TextForeground
*label7.background: LabelBackground
*label7.alignment: "alignment_center"
*label7.recomputeSize: "false"
*label7.translations: ""
*label7.fontList: BoldTextFont
*label7.leftAttachment: "attach_form"
*label7.leftOffset: 5
*label7.topAttachment: "attach_form"
*label7.topOffset: 50
*label7.topWidget: "text3"

*label8.class: label
*label8.parent: form7
*label8.static: true
*label8.name: label8
*label8.x: 9
*label8.y: 129
*label8.width: 100
*label8.height: 25
*label8.labelString: "No. of Curs"
*label8.foreground: TextForeground
*label8.background: LabelBackground
*label8.alignment: "alignment_center"
*label8.recomputeSize: "false"
*label8.translations: ""
*label8.fontList: BoldTextFont
*label8.leftAttachment: "attach_form"
*label8.leftOffset: 5
*label8.topAttachment: "attach_form"
*label8.topOffset: 95
*label8.topWidget: "label7"

*label4.class: label
*label4.parent: form7
*label4.static: true
*label4.name: label4
*label4.x: 9
*label4.y: 169
*label4.width: 100
*label4.height: 25
*label4.labelString: "ZoomWindow"
*label4.foreground: TextForeground
*label4.background: LabelBackground
*label4.alignment: "alignment_center"
*label4.recomputeSize: "false"
*label4.translations: ""
*label4.fontList: BoldTextFont
*label4.leftAttachment: "attach_form"
*label4.leftOffset: 5
*label4.topAttachment: "attach_form"
*label4.topOffset: 140
*label4.topWidget: "label8"

*toggleButton8.class: toggleButton
*toggleButton8.parent: form7
*toggleButton8.static: true
*toggleButton8.name: toggleButton8
*toggleButton8.x: 10
*toggleButton8.y: 10
*toggleButton8.width: 70
*toggleButton8.height: 28
*toggleButton8.labelString: "No"
*toggleButton8.indicatorType: "one_of_many"
*toggleButton8.background: TextBackground
*toggleButton8.fontList: BoldTextFont
*toggleButton8.foreground: TextForeground
*toggleButton8.recomputeSize: "false"
*toggleButton8.borderWidth: 1
*toggleButton8.set: "true"
*toggleButton8.indicatorOn: "true"
*toggleButton8.indicatorSize: 16
*toggleButton8.selectColor: "Palegreen"
*toggleButton8.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"No");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
   }\
\
}
*toggleButton8.topShadowColor: LabelBackground
*toggleButton8.leftAttachment: "attach_widget"
*toggleButton8.leftOffset: 15
*toggleButton8.leftWidget: "label4"
*toggleButton8.topAttachment: "attach_form"
*toggleButton8.topOffset: 135
*toggleButton8.marginLeft: 10
*toggleButton8.marginRight: 2

*toggleButton9.class: toggleButton
*toggleButton9.parent: form7
*toggleButton9.static: true
*toggleButton9.name: toggleButton9
*toggleButton9.x: 20
*toggleButton9.y: 20
*toggleButton9.width: 40
*toggleButton9.height: 28
*toggleButton9.labelString: "1"
*toggleButton9.indicatorType: "one_of_many"
*toggleButton9.background: TextBackground
*toggleButton9.fontList: BoldTextFont
*toggleButton9.foreground: TextForeground
*toggleButton9.recomputeSize: "false"
*toggleButton9.borderWidth: 1
*toggleButton9.set: "true"
*toggleButton9.indicatorOn: "true"
*toggleButton9.indicatorSize: 16
*toggleButton9.selectColor: "Palegreen"
*toggleButton9.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"1");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"2");\
   }\
\
}
*toggleButton9.topShadowColor: LabelBackground
*toggleButton9.leftAttachment: "attach_widget"
*toggleButton9.leftOffset: 15
*toggleButton9.leftWidget: "label8"
*toggleButton9.topAttachment: "attach_form"
*toggleButton9.topOffset: 90

*toggleButton14.class: toggleButton
*toggleButton14.parent: form7
*toggleButton14.static: true
*toggleButton14.name: toggleButton14
*toggleButton14.x: 30
*toggleButton14.y: 30
*toggleButton14.width: 70
*toggleButton14.height: 28
*toggleButton14.labelString: "Yes"
*toggleButton14.indicatorType: "one_of_many"
*toggleButton14.background: TextBackground
*toggleButton14.fontList: BoldTextFont
*toggleButton14.foreground: TextForeground
*toggleButton14.recomputeSize: "false"
*toggleButton14.borderWidth: 1
*toggleButton14.set: "true"
*toggleButton14.indicatorOn: "true"
*toggleButton14.indicatorSize: 16
*toggleButton14.selectColor: "Palegreen"
*toggleButton14.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"No");\
   }\
\
}
*toggleButton14.topShadowColor: LabelBackground
*toggleButton14.leftAttachment: "attach_widget"
*toggleButton14.leftOffset: 15
*toggleButton14.leftWidget: "label7"
*toggleButton14.topAttachment: "attach_form"
*toggleButton14.topOffset: 45

*pushButton6.class: pushButton
*pushButton6.parent: workArea
*pushButton6.static: true
*pushButton6.name: pushButton6
*pushButton6.x: 10
*pushButton6.y: 370
*pushButton6.width: 90
*pushButton6.height: 30
*pushButton6.background: ButtonBackground
*pushButton6.labelString: "Get Cursor"
*pushButton6.activateCallback: {\
\
}
*pushButton6.translations: MapUnmap
*pushButton6.fontList: BoldTextFont
*pushButton6.foreground: ButtonForeground
*pushButton6.recomputeSize: "false"
*pushButton6.bottomAttachment: "attach_none"
*pushButton6.bottomOffset: 5
*pushButton6.bottomWidget: ""
*pushButton6.leftAttachment: "attach_form"
*pushButton6.leftOffset: 10
*pushButton6.topAttachment: "attach_widget"
*pushButton6.topOffset: 5
*pushButton6.topWidget: "SHelp"

*form1.class: form
*form1.parent: workArea
*form1.static: true
*form1.name: form1
*form1.resizePolicy: "resize_none"
*form1.x: 10
*form1.y: 10
*form1.width: 430
*form1.height: 240
*form1.background: ApplicBackground
*form1.borderWidth: 1
*form1.mappedWhenManaged: "false"
*form1.createCallback: {\
\
}
*form1.mapCallback: {\
\
}
*form1.leftAttachment: "attach_form"
*form1.leftOffset: 4
*form1.rightAttachment: "attach_form"
*form1.rightOffset: 4
*form1.topAttachment: "attach_form"
*form1.topOffset: 4

*label6.class: label
*label6.parent: form1
*label6.static: true
*label6.name: label6
*label6.x: 10
*label6.y: 10
*label6.width: 89
*label6.height: 25
*label6.labelString: "Out_Frame"
*label6.foreground: TextForeground
*label6.background: LabelBackground
*label6.alignment: "alignment_center"
*label6.recomputeSize: "false"
*label6.translations: ""
*label6.fontList: BoldTextFont
*label6.leftAttachment: "attach_form"
*label6.leftOffset: 5
*label6.topAttachment: "attach_form"
*label6.topOffset: 15

*text7.class: text
*text7.parent: form1
*text7.static: true
*text7.name: text7
*text7.x: 10
*text7.y: 39
*text7.width: 360
*text7.height: 30
*text7.background: TextBackground
*text7.foreground: TextForeground
*text7.fontList: TextFont
*text7.leftAttachment: "attach_widget"
*text7.leftOffset: 16
*text7.rightAttachment: "attach_form"
*text7.rightOffset: 60
*text7.topAttachment: "attach_form"
*text7.topOffset: 10
*text7.leftWidget: "label6"
*text7.marginHeight: 2
*text7.marginWidth: 2

*label31.class: label
*label31.parent: form1
*label31.static: true
*label31.name: label31
*label31.x: 10
*label31.y: 79
*label31.width: 89
*label31.height: 25
*label31.labelString: "Plot_Flag"
*label31.foreground: TextForeground
*label31.background: LabelBackground
*label31.alignment: "alignment_center"
*label31.recomputeSize: "false"
*label31.translations: ""
*label31.fontList: BoldTextFont
*label31.leftAttachment: "attach_form"
*label31.leftOffset: 5
*label31.topAttachment: "attach_form"
*label31.topOffset: 55
*label31.topWidget: "text7"

*plotflag.class: rowColumn
*plotflag.parent: form1
*plotflag.static: true
*plotflag.name: plotflag
*plotflag.rowColumnType: "menu_option"
*plotflag.subMenuId: "plotflag_p1"
*plotflag.x: 119
*plotflag.y: 79
*plotflag.background: TextBackground
*plotflag.foreground: TextForeground
*plotflag.resizeHeight: "true"
*plotflag.resizeWidth: "true"
*plotflag.width: 102
*plotflag.leftAttachment: "attach_widget"
*plotflag.leftOffset: 20
*plotflag.leftWidget: "label31"
*plotflag.topAttachment: "attach_form"
*plotflag.topOffset: 50
*plotflag.topWidget: "text7"
*plotflag.shadowThickness: 1
*plotflag.translations: ""
*plotflag.borderWidth: 1
*plotflag.labelString: ""

*plotflag_p1.class: rowColumn
*plotflag_p1.parent: plotflag
*plotflag_p1.static: true
*plotflag_p1.name: plotflag_p1
*plotflag_p1.rowColumnType: "menu_pulldown"
*plotflag_p1.background: TextBackground
*plotflag_p1.foreground: TextForeground
*plotflag_p1.labelString: ""
*plotflag_p1.x: 0
*plotflag_p1.y: 0

*plotflag_p1_b1.class: pushButtonGadget
*plotflag_p1_b1.parent: plotflag_p1
*plotflag_p1_b1.static: true
*plotflag_p1_b1.name: plotflag_p1_b1
*plotflag_p1_b1.labelString: "Draw"
*plotflag_p1_b1.fontList: TextFont
*plotflag_p1_b1.activateCallback: {\
\
}
*plotflag_p1_b1.recomputeSize: "true"
*plotflag_p1_b1.x: 0
*plotflag_p1_b1.y: 0

*plotflag_p1_b2.class: pushButtonGadget
*plotflag_p1_b2.parent: plotflag_p1
*plotflag_p1_b2.static: true
*plotflag_p1_b2.name: plotflag_p1_b2
*plotflag_p1_b2.labelString: "Plot"
*plotflag_p1_b2.fontList: TextFont
*plotflag_p1_b2.activateCallback: {\
\
\
}
*plotflag_p1_b2.recomputeSize: "true"
*plotflag_p1_b2.x: 0
*plotflag_p1_b2.y: 0

*plotflag_p1_b3.class: pushButtonGadget
*plotflag_p1_b3.parent: plotflag_p1
*plotflag_p1_b3.static: true
*plotflag_p1_b3.name: plotflag_p1_b3
*plotflag_p1_b3.labelString: "None"
*plotflag_p1_b3.fontList: TextFont

*label29.class: label
*label29.parent: form1
*label29.static: true
*label29.name: label29
*label29.x: 10
*label29.y: 119
*label29.width: 89
*label29.height: 25
*label29.labelString: "Cut_Option"
*label29.foreground: TextForeground
*label29.background: LabelBackground
*label29.alignment: "alignment_center"
*label29.recomputeSize: "false"
*label29.translations: ""
*label29.fontList: BoldTextFont
*label29.leftAttachment: "attach_form"
*label29.leftOffset: 5
*label29.topAttachment: "attach_form"
*label29.topOffset: 103
*label29.topWidget: "label31"

*label30.class: label
*label30.parent: form1
*label30.static: true
*label30.name: label30
*label30.x: 10
*label30.y: 159
*label30.width: 89
*label30.height: 25
*label30.labelString: "Stepsize"
*label30.foreground: TextForeground
*label30.background: LabelBackground
*label30.alignment: "alignment_center"
*label30.recomputeSize: "false"
*label30.translations: ""
*label30.fontList: BoldTextFont
*label30.leftAttachment: "attach_form"
*label30.leftOffset: 5
*label30.topAttachment: "attach_form"
*label30.topOffset: 150
*label30.topWidget: "label29"

*text14.class: text
*text14.parent: form1
*text14.static: true
*text14.name: text14
*text14.x: 119
*text14.y: 159
*text14.width: 140
*text14.height: 30
*text14.background: TextBackground
*text14.foreground: TextForeground
*text14.fontList: TextFont
*text14.leftAttachment: "attach_widget"
*text14.leftOffset: 16
*text14.leftWidget: "label30"
*text14.marginHeight: 2
*text14.marginWidth: 2
*text14.topAttachment: "attach_form"
*text14.topCharacter: 0
*text14.topOffset: 145

*toggleButton6.class: toggleButton
*toggleButton6.parent: form1
*toggleButton6.static: true
*toggleButton6.name: toggleButton6
*toggleButton6.x: 0
*toggleButton6.y: 0
*toggleButton6.width: 78
*toggleButton6.height: 28
*toggleButton6.labelString: "NoCut"
*toggleButton6.indicatorType: "one_of_many"
*toggleButton6.background: TextBackground
*toggleButton6.fontList: BoldTextFont
*toggleButton6.foreground: TextForeground
*toggleButton6.recomputeSize: "false"
*toggleButton6.borderWidth: 1
*toggleButton6.set: "true"
*toggleButton6.indicatorOn: "true"
*toggleButton6.indicatorSize: 16
*toggleButton6.selectColor: "Palegreen"
*toggleButton6.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"NoCut");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"Cut");\
   }\
\
}
*toggleButton6.leftAttachment: "attach_widget"
*toggleButton6.leftOffset: 20
*toggleButton6.leftWidget: "label29"
*toggleButton6.topAttachment: "attach_form"
*toggleButton6.topOffset: 100
*toggleButton6.marginRight: 2
*toggleButton6.marginLeft: 10

*form8.class: form
*form8.parent: workArea
*form8.static: true
*form8.name: form8
*form8.resizePolicy: "resize_none"
*form8.x: 10
*form8.y: 10
*form8.width: 430
*form8.height: 240
*form8.background: ApplicBackground
*form8.borderWidth: 1
*form8.mappedWhenManaged: "false"
*form8.createCallback: {\
\
}
*form8.leftAttachment: "attach_form"
*form8.leftOffset: 4
*form8.rightAttachment: "attach_form"
*form8.rightOffset: 4
*form8.topAttachment: "attach_form"
*form8.topOffset: 5
*form8.textFontList: TextFont
*form8.buttonFontList: TextFont
*form8.labelFontList: TextFont

*label39.class: label
*label39.parent: form8
*label39.static: true
*label39.name: label39
*label39.x: 9
*label39.y: 169
*label39.width: 100
*label39.height: 25
*label39.labelString: "LUT Method"
*label39.foreground: TextForeground
*label39.background: LabelBackground
*label39.alignment: "alignment_center"
*label39.recomputeSize: "false"
*label39.translations: ""
*label39.fontList: BoldTextFont
*label39.leftAttachment: "attach_form"
*label39.leftOffset: 10
*label39.topAttachment: "attach_form"
*label39.topOffset: 25

*label40.class: label
*label40.parent: form8
*label40.static: true
*label40.name: label40
*label40.x: 9
*label40.y: 209
*label40.width: 100
*label40.height: 25
*label40.labelString: "LUT Colour"
*label40.foreground: TextForeground
*label40.background: LabelBackground
*label40.alignment: "alignment_center"
*label40.recomputeSize: "false"
*label40.translations: ""
*label40.fontList: BoldTextFont
*label40.leftAttachment: "attach_form"
*label40.leftOffset: 10
*label40.topAttachment: "attach_form"
*label40.topOffset: 70
*label40.topWidget: "label39"

*method2.class: rowColumn
*method2.parent: form8
*method2.static: true
*method2.name: method2
*method2.rowColumnType: "menu_option"
*method2.subMenuId: "method_p3"
*method2.x: 149
*method2.y: 169
*method2.background: TextBackground
*method2.foreground: TextForeground
*method2.resizeHeight: "true"
*method2.resizeWidth: "true"
*method2.width: 102
*method2.leftAttachment: "attach_widget"
*method2.leftOffset: 20
*method2.leftWidget: "label39"
*method2.topAttachment: "attach_form"
*method2.topOffset: 20
*method2.translations: ""
*method2.entryCallback: {\
\
}
*method2.borderWidth: 1
*method2.labelString: ""

*method_p3.class: rowColumn
*method_p3.parent: method2
*method_p3.static: true
*method_p3.name: method_p3
*method_p3.rowColumnType: "menu_pulldown"
*method_p3.background: TextBackground
*method_p3.foreground: TextForeground
*method_p3.labelString: ""
*method_p3.x: 0
*method_p3.y: 0
*method_p3.entryCallback: {\
\
extern int menuact[8];\
\
menuact[0] = 0;		/* reset ITT activator */\
\
XmToggleButtonSetState(UxGetWidget(toggleButton3),TRUE,FALSE);\
\
XmToggleButtonSetState(UxGetWidget(toggleButton5),FALSE,FALSE);\
\
}

*method_p1_b9.class: pushButtonGadget
*method_p1_b9.parent: method_p3
*method_p1_b9.static: true
*method_p1_b9.name: method_p1_b9
*method_p1_b9.labelString: "Band"
*method_p1_b9.fontList: TextFont
*method_p1_b9.activateCallback: {\
\
/*  `actmeth2a.c'  the Activate Callback of the 1. button of (BAND)\
    `method2' menu in MODIFY/LUT form                           */\
\
\
\
char  cur_color[24];\
\
XtSetSensitive(UxGetWidget(color1),TRUE);\
\
strcpy(cur_color,UxGetMenuHistory(color1));\
if (cur_color[10] == '4')			/* ALL -> RED */\
   UxPutMenuHistory(color1,"color_p1_b12");\
\
XtSetSensitive(UxGetWidget(color_p1_b15),FALSE);\
\
\
XtSetSensitive(UxGetWidget(color_p1_b12),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b13),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b14),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b16),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b17),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b18),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b19),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b20),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b21),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b22),TRUE);\
\
\
\
\
}
*method_p1_b9.recomputeSize: "true"
*method_p1_b9.x: 0
*method_p1_b9.y: 0

*method_p1_b10.class: pushButtonGadget
*method_p1_b10.parent: method_p3
*method_p1_b10.static: true
*method_p1_b10.name: method_p1_b10
*method_p1_b10.labelString: "Rotate"
*method_p1_b10.fontList: TextFont
*method_p1_b10.activateCallback: {\
\
/* `actmeth2b.c'  the Activate Callback of the 2. button (ROTATE) of\
   `method2' menu in MODIFY/LUT form */\
\
\
char  cur_color[24];\
\
XtSetSensitive(UxGetWidget(color1),TRUE);\
\
strcpy(cur_color,UxGetMenuHistory(color1));\
if ( (cur_color[10] == '2') ||			/* b20, ... */\
     (cur_color[11] > '5') )			/* b16 -> b19 */\
   UxPutMenuHistory(color1,"color_p1_b15");\
\
XtSetSensitive(UxGetWidget(color_p1_b15),TRUE);\
\
XtSetSensitive(UxGetWidget(color_p1_b16),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b17),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b18),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b19),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b20),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b21),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b22),FALSE);\
\
\
\
\
\
\
\
}
*method_p1_b10.recomputeSize: "true"
*method_p1_b10.x: 0
*method_p1_b10.y: 0

*method_p1_b11.class: pushButtonGadget
*method_p1_b11.parent: method_p3
*method_p1_b11.static: true
*method_p1_b11.name: method_p1_b11
*method_p1_b11.labelString: "Squeeze"
*method_p1_b11.fontList: TextFont
*method_p1_b11.activateCallback: {\
\
/*  `actmeth2c.c' the Activate Callback of the 3. button (SQEEZE) of\
    `method2' menu in MODIFY/LUT */\
\
\
UxPutMenuHistory(color1,"color_p1_b15");		/* force to ALL */\
\
XtSetSensitive(UxGetWidget(color1),FALSE);\
\
\
\
\
\
\
\
}
*method_p1_b11.recomputeSize: "true"
*method_p1_b11.x: 0
*method_p1_b11.y: 0

*method_p1_b12.class: pushButtonGadget
*method_p1_b12.parent: method_p3
*method_p1_b12.static: true
*method_p1_b12.name: method_p1_b12
*method_p1_b12.labelString: "Graph"
*method_p1_b12.fontList: TextFont
*method_p1_b12.activateCallback: {\
\
/*  `actmeth2d.c' the Activate Callback of the 4. button (GRAPH) of\
    `method2' menu in MODIFY/LUT  */\
\
char  cur_color[24];\
\
\
XtSetSensitive(UxGetWidget(color1),TRUE);\
\
strcpy(cur_color,UxGetMenuHistory(color1));	/* get current color1 */\
if ( (cur_color[10] == '2') ||\
     (cur_color[11] > '5') )\
   UxPutMenuHistory(color1,"color_p1_b15");\
\
\
XtSetSensitive(UxGetWidget(color_p1_b12),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b13),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b14),TRUE);\
XtSetSensitive(UxGetWidget(color_p1_b15),TRUE);\
\
XtSetSensitive(UxGetWidget(color_p1_b16),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b17),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b18),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b19),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b20),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b21),FALSE);\
XtSetSensitive(UxGetWidget(color_p1_b22),FALSE);\
\
\
\
\
\
\
\
}
*method_p1_b12.recomputeSize: "true"
*method_p1_b12.x: 0
*method_p1_b12.y: 0

*color1.class: rowColumn
*color1.parent: form8
*color1.static: true
*color1.name: color1
*color1.rowColumnType: "menu_option"
*color1.subMenuId: "color_p2"
*color1.x: 149
*color1.y: 209
*color1.background: TextBackground
*color1.foreground: TextForeground
*color1.resizeHeight: "true"
*color1.resizeWidth: "true"
*color1.width: 102
*color1.leftAttachment: "attach_widget"
*color1.leftOffset: 20
*color1.leftWidget: "label40"
*color1.topAttachment: "attach_widget"
*color1.topOffset: 15
*color1.topWidget: "method2"
*color1.topShadowColor: LabelBackground
*color1.translations: ""
*color1.borderWidth: 1
*color1.labelString: ""

*color_p2.class: rowColumn
*color_p2.parent: color1
*color_p2.static: true
*color_p2.name: color_p2
*color_p2.rowColumnType: "menu_pulldown"
*color_p2.background: TextBackground
*color_p2.foreground: TextForeground
*color_p2.width: 74
*color_p2.resizeHeight: "true"
*color_p2.resizeWidth: "true"
*color_p2.x: 0
*color_p2.y: 0
*color_p2.entryCallback: {\
\
extern int menuact[8];\
\
menuact[0] = 0;		/* reset ITT activator */\
\
XmToggleButtonSetState(UxGetWidget(toggleButton3),TRUE,FALSE);\
\
XmToggleButtonSetState(UxGetWidget(toggleButton5),FALSE,FALSE);\
\
}

*color_p1_b12.class: pushButtonGadget
*color_p1_b12.parent: color_p2
*color_p1_b12.static: true
*color_p1_b12.name: color_p1_b12
*color_p1_b12.labelString: "Red"
*color_p1_b12.accelerator: ""
*color_p1_b12.fontList: TextFont
*color_p1_b12.sensitive: "true"
*color_p1_b12.recomputeSize: "true"
*color_p1_b12.x: 0
*color_p1_b12.y: 0
*color_p1_b12.activateCallback: {\
\
}

*color_p1_b13.class: pushButtonGadget
*color_p1_b13.parent: color_p2
*color_p1_b13.static: true
*color_p1_b13.name: color_p1_b13
*color_p1_b13.labelString: "Green"
*color_p1_b13.fontList: TextFont
*color_p1_b13.recomputeSize: "true"
*color_p1_b13.x: 0
*color_p1_b13.y: 0
*color_p1_b13.activateCallback: {\
\
}

*color_p1_b14.class: pushButtonGadget
*color_p1_b14.parent: color_p2
*color_p1_b14.static: true
*color_p1_b14.name: color_p1_b14
*color_p1_b14.labelString: "Blue"
*color_p1_b14.fontList: TextFont
*color_p1_b14.recomputeSize: "true"
*color_p1_b14.x: 0
*color_p1_b14.y: 0
*color_p1_b14.activateCallback: {\
\
}

*color_p1_b15.class: pushButtonGadget
*color_p1_b15.parent: color_p2
*color_p1_b15.static: true
*color_p1_b15.name: color_p1_b15
*color_p1_b15.labelString: "All"
*color_p1_b15.fontList: TextFont
*color_p1_b15.recomputeSize: "true"
*color_p1_b15.x: 0
*color_p1_b15.y: 0
*color_p1_b15.activateCallback: {\
\
}

*color_p1_b16.class: pushButtonGadget
*color_p1_b16.parent: color_p2
*color_p1_b16.static: true
*color_p1_b16.name: color_p1_b16
*color_p1_b16.labelString: "White"
*color_p1_b16.fontList: TextFont
*color_p1_b16.recomputeSize: "true"
*color_p1_b16.x: 0
*color_p1_b16.y: 0
*color_p1_b16.activateCallback: {\
\
}

*color_p1_b17.class: pushButtonGadget
*color_p1_b17.parent: color_p2
*color_p1_b17.static: true
*color_p1_b17.name: color_p1_b17
*color_p1_b17.labelString: "Dark"
*color_p1_b17.fontList: TextFont
*color_p1_b17.recomputeSize: "true"
*color_p1_b17.x: 0
*color_p1_b17.y: 0
*color_p1_b17.activateCallback: {\
\
}

*color_p1_b18.class: pushButtonGadget
*color_p1_b18.parent: color_p2
*color_p1_b18.static: true
*color_p1_b18.name: color_p1_b18
*color_p1_b18.labelString: "Yellow"
*color_p1_b18.fontList: TextFont
*color_p1_b18.recomputeSize: "true"
*color_p1_b18.x: 0
*color_p1_b18.y: 0
*color_p1_b18.activateCallback: {\
\
}

*color_p1_b19.class: pushButtonGadget
*color_p1_b19.parent: color_p2
*color_p1_b19.static: true
*color_p1_b19.name: color_p1_b19
*color_p1_b19.labelString: "Pink"
*color_p1_b19.fontList: TextFont
*color_p1_b19.recomputeSize: "true"
*color_p1_b19.x: 0
*color_p1_b19.y: 0
*color_p1_b19.activateCallback: {\
\
}

*color_p1_b20.class: pushButtonGadget
*color_p1_b20.parent: color_p2
*color_p1_b20.static: true
*color_p1_b20.name: color_p1_b20
*color_p1_b20.labelString: "Brown"
*color_p1_b20.fontList: TextFont
*color_p1_b20.recomputeSize: "true"
*color_p1_b20.x: 0
*color_p1_b20.y: 0
*color_p1_b20.activateCallback: {\
\
}

*color_p1_b21.class: pushButtonGadget
*color_p1_b21.parent: color_p2
*color_p1_b21.static: true
*color_p1_b21.name: color_p1_b21
*color_p1_b21.labelString: "Orange"
*color_p1_b21.fontList: TextFont
*color_p1_b21.recomputeSize: "true"
*color_p1_b21.x: 0
*color_p1_b21.y: 0
*color_p1_b21.activateCallback: {\
\
}

*color_p1_b22.class: pushButtonGadget
*color_p1_b22.parent: color_p2
*color_p1_b22.static: true
*color_p1_b22.name: color_p1_b22
*color_p1_b22.labelString: "Violet"
*color_p1_b22.fontList: TextFont
*color_p1_b22.recomputeSize: "true"
*color_p1_b22.x: 0
*color_p1_b22.y: 0
*color_p1_b22.activateCallback: {\
\
}

*separator1.class: separator
*separator1.parent: form8
*separator1.static: true
*separator1.name: separator1
*separator1.x: -1
*separator1.y: 119
*separator1.width: 430
*separator1.height: 10
*separator1.background: ApplicBackground
*separator1.foreground: TextForeground
*separator1.leftAttachment: "attach_form"
*separator1.leftOffset: 5
*separator1.rightAttachment: "attach_form"
*separator1.rightOffset: 5
*separator1.topAttachment: "attach_form"
*separator1.topOffset: 110

*label23.class: label
*label23.parent: form8
*label23.static: true
*label23.name: label23
*label23.x: 9
*label23.y: 169
*label23.width: 100
*label23.height: 25
*label23.labelString: "ITT Method"
*label23.foreground: TextForeground
*label23.background: LabelBackground
*label23.alignment: "alignment_center"
*label23.recomputeSize: "false"
*label23.translations: ""
*label23.fontList: BoldTextFont
*label23.leftAttachment: "attach_form"
*label23.leftOffset: 10
*label23.topAttachment: "attach_form"
*label23.topOffset: 140

*method1.class: rowColumn
*method1.parent: form8
*method1.static: true
*method1.name: method1
*method1.rowColumnType: "menu_option"
*method1.subMenuId: "method_p1"
*method1.x: 149
*method1.y: 169
*method1.background: TextBackground
*method1.foreground: TextForeground
*method1.resizeHeight: "true"
*method1.resizeWidth: "true"
*method1.width: 102
*method1.translations: ""
*method1.entryCallback: {\
\
}
*method1.leftAttachment: "attach_widget"
*method1.leftOffset: 20
*method1.leftWidget: "label23"
*method1.topAttachment: "attach_form"
*method1.topOffset: 135
*method1.borderWidth: 1
*method1.labelString: ""

*method_p1.class: rowColumn
*method_p1.parent: method1
*method_p1.static: true
*method_p1.name: method_p1
*method_p1.rowColumnType: "menu_pulldown"
*method_p1.background: TextBackground
*method_p1.foreground: TextForeground
*method_p1.labelString: ""
*method_p1.x: 0
*method_p1.y: 0
*method_p1.entryCallback: {\
extern int menuact[8];\
\
menuact[0] = 1;\
\
XmToggleButtonSetState(UxGetWidget(toggleButton3),FALSE,FALSE);\
XmToggleButtonSetState(UxGetWidget(toggleButton5),TRUE,FALSE);\
}

*method_p1_b1.class: pushButtonGadget
*method_p1_b1.parent: method_p1
*method_p1_b1.static: true
*method_p1_b1.name: method_p1_b1
*method_p1_b1.labelString: "Band"
*method_p1_b1.fontList: TextFont
*method_p1_b1.activateCallback: {\
\
/*  `actmeth1a.c'  the Activate Callback of the 1. button of (BAND)\
    `method1' menu in MODIFY/ITT form                           */\
\
\
\
XtSetSensitive(UxGetWidget(label33),TRUE);\
XtSetSensitive(UxGetWidget(rowColumn19),TRUE);\
\
\
\
}
*method_p1_b1.recomputeSize: "true"
*method_p1_b1.x: 0
*method_p1_b1.y: 0

*method_p1_b2.class: pushButtonGadget
*method_p1_b2.parent: method_p1
*method_p1_b2.static: true
*method_p1_b2.name: method_p1_b2
*method_p1_b2.labelString: "Rotate"
*method_p1_b2.fontList: TextFont
*method_p1_b2.activateCallback: {\
\
/* `actmeth1b.c'  the Activate Callback of the 2. button (ROTATE) of\
   `method1' menu in MODIFY/ITT form */\
\
\
\
XtSetSensitive(UxGetWidget(label33),FALSE);\
XtSetSensitive(UxGetWidget(rowColumn19),FALSE);\
\
\
}
*method_p1_b2.recomputeSize: "true"
*method_p1_b2.x: 0
*method_p1_b2.y: 0

*method_p1_b3.class: pushButtonGadget
*method_p1_b3.parent: method_p1
*method_p1_b3.static: true
*method_p1_b3.name: method_p1_b3
*method_p1_b3.labelString: "Squeeze"
*method_p1_b3.fontList: TextFont
*method_p1_b3.activateCallback: {\
\
/* `actmeth1c.c'  the Activate Callback of the 3. button (SQUEEZE) of\
   `method1' menu in MODIFY/ITT form */\
\
\
\
XtSetSensitive(UxGetWidget(label33),FALSE);\
XtSetSensitive(UxGetWidget(rowColumn19),FALSE);\
\
\
}
*method_p1_b3.recomputeSize: "true"
*method_p1_b3.x: 0
*method_p1_b3.y: 0

*method_p1_b4.class: pushButtonGadget
*method_p1_b4.parent: method_p1
*method_p1_b4.static: true
*method_p1_b4.name: method_p1_b4
*method_p1_b4.labelString: "Contrast"
*method_p1_b4.fontList: TextFont
*method_p1_b4.activateCallback: {\
\
/* `actmeth1d.c'  the Activate Callback of the 4. button (CONTRAST) of\
   `method1' menu in MODIFY/ITT form */\
\
\
\
XtSetSensitive(UxGetWidget(label33),FALSE);\
XtSetSensitive(UxGetWidget(rowColumn19),FALSE);\
\
\
}
*method_p1_b4.recomputeSize: "true"
*method_p1_b4.x: 0
*method_p1_b4.y: 0

*label33.class: label
*label33.parent: form8
*label33.static: true
*label33.name: label33
*label33.x: 9
*label33.y: 169
*label33.width: 100
*label33.height: 25
*label33.labelString: "ITT Value"
*label33.foreground: TextForeground
*label33.background: LabelBackground
*label33.alignment: "alignment_center"
*label33.recomputeSize: "false"
*label33.translations: ""
*label33.fontList: BoldTextFont
*label33.leftAttachment: "attach_form"
*label33.leftOffset: 5
*label33.topAttachment: "attach_form"
*label33.topOffset: 185

*rowColumn19.class: rowColumn
*rowColumn19.parent: form8
*rowColumn19.static: true
*rowColumn19.name: rowColumn19
*rowColumn19.x: 114
*rowColumn19.y: 15
*rowColumn19.width: 100
*rowColumn19.height: 44
*rowColumn19.orientation: "horizontal"
*rowColumn19.packing: "pack_none"
*rowColumn19.spacing: 0
*rowColumn19.resizeHeight: "false"
*rowColumn19.resizeWidth: "false"
*rowColumn19.marginHeight: 0
*rowColumn19.marginWidth: 0
*rowColumn19.numColumns: 2
*rowColumn19.background: ApplicBackground
*rowColumn19.leftAttachment: "attach_widget"
*rowColumn19.leftOffset: 20
*rowColumn19.leftWidget: "label33"
*rowColumn19.topAttachment: "attach_form"
*rowColumn19.topOffset: 175
*rowColumn19.labelString: ""

*rowColumn21.class: rowColumn
*rowColumn21.parent: rowColumn19
*rowColumn21.static: true
*rowColumn21.name: rowColumn21
*rowColumn21.x: 65
*rowColumn21.y: 6
*rowColumn21.width: 23
*rowColumn21.height: 32
*rowColumn21.resizeHeight: "false"
*rowColumn21.resizeWidth: "false"
*rowColumn21.background: ApplicBackground
*rowColumn21.marginHeight: 0
*rowColumn21.marginWidth: 0
*rowColumn21.packing: "pack_none"
*rowColumn21.spacing: 0
*rowColumn21.adjustMargin: "true"
*rowColumn21.adjustLast: "true"
*rowColumn21.entryAlignment: "alignment_center"

*arrowButton1.class: arrowButton
*arrowButton1.parent: rowColumn21
*arrowButton1.static: true
*arrowButton1.name: arrowButton1
*arrowButton1.x: 0
*arrowButton1.y: 0
*arrowButton1.width: 23
*arrowButton1.height: 15
*arrowButton1.background: TextBackground
*arrowButton1.shadowThickness: 0
*arrowButton1.translations: Arrows8
*arrowButton1.foreground: "PaleGreen"
*arrowButton1.highlightThickness: 0

*arrowButton2.class: arrowButton
*arrowButton2.parent: rowColumn21
*arrowButton2.static: true
*arrowButton2.name: arrowButton2
*arrowButton2.x: 0
*arrowButton2.y: 16
*arrowButton2.width: 23
*arrowButton2.height: 15
*arrowButton2.background: TextBackground
*arrowButton2.shadowThickness: 0
*arrowButton2.arrowDirection: "arrow_down"
*arrowButton2.translations: Arrows8
*arrowButton2.foreground: "PaleGreen"
*arrowButton2.borderColor: ApplicBackground
*arrowButton2.highlightThickness: 0

*text27.class: text
*text27.parent: rowColumn19
*text27.static: true
*text27.name: text27
*text27.x: 5
*text27.y: 7
*text27.width: 50
*text27.height: 32
*text27.background: TextBackground
*text27.fontList: TextFont
*text27.foreground: TextForeground
*text27.highlightThickness: 2
*text27.shadowThickness: 2
*text27.marginHeight: 2
*text27.marginWidth: 2
*text27.maxLength: 100

*toggleButton3.class: toggleButton
*toggleButton3.parent: form8
*toggleButton3.static: true
*toggleButton3.name: toggleButton3
*toggleButton3.x: 129
*toggleButton3.y: 99
*toggleButton3.width: 25
*toggleButton3.height: 28
*toggleButton3.labelString: ""
*toggleButton3.indicatorType: "one_of_many"
*toggleButton3.background: ApplicBackground
*toggleButton3.fontList: BoldTextFont
*toggleButton3.foreground: TextForeground
*toggleButton3.recomputeSize: "false"
*toggleButton3.borderWidth: 1
*toggleButton3.set: "true"
*toggleButton3.indicatorOn: "true"
*toggleButton3.indicatorSize: 16
*toggleButton3.selectColor: "PaleGreen"
*toggleButton3.valueChangedCallback: {\
\
}
*toggleButton3.rightAttachment: "attach_form"
*toggleButton3.rightOffset: 80
*toggleButton3.topAttachment: "attach_form"
*toggleButton3.topOffset: 40
*toggleButton3.marginLeft: 0
*toggleButton3.sensitive: "true"

*toggleButton5.class: toggleButton
*toggleButton5.parent: form8
*toggleButton5.static: true
*toggleButton5.name: toggleButton5
*toggleButton5.x: 129
*toggleButton5.y: 99
*toggleButton5.width: 25
*toggleButton5.height: 28
*toggleButton5.labelString: ""
*toggleButton5.indicatorType: "one_of_many"
*toggleButton5.background: ApplicBackground
*toggleButton5.fontList: BoldTextFont
*toggleButton5.foreground: TextForeground
*toggleButton5.recomputeSize: "false"
*toggleButton5.borderWidth: 1
*toggleButton5.set: "false"
*toggleButton5.indicatorOn: "true"
*toggleButton5.indicatorSize: 16
*toggleButton5.selectColor: "PaleGreen"
*toggleButton5.valueChangedCallback: {\
\
}
*toggleButton5.marginLeft: 0
*toggleButton5.sensitive: "true"
*toggleButton5.rightAttachment: "attach_form"
*toggleButton5.rightOffset: 80
*toggleButton5.topAttachment: "attach_form"
*toggleButton5.topOffset: 150
*toggleButton5.topPosition: 00

*pushButton2.class: pushButton
*pushButton2.parent: workArea
*pushButton2.static: true
*pushButton2.name: pushButton2
*pushButton2.x: 10
*pushButton2.y: 370
*pushButton2.width: 90
*pushButton2.height: 30
*pushButton2.background: ButtonBackground
*pushButton2.labelString: "Load LUT"
*pushButton2.activateCallback: {\
\
}
*pushButton2.translations: MapUnmap
*pushButton2.bottomAttachment: "attach_none"
*pushButton2.bottomOffset: 5
*pushButton2.leftAttachment: "attach_form"
*pushButton2.leftOffset: 10
*pushButton2.bottomWidget: ""
*pushButton2.fontList: BoldTextFont
*pushButton2.foreground: ButtonForeground
*pushButton2.recomputeSize: "false"
*pushButton2.topOffset: 5
*pushButton2.topAttachment: "attach_widget"
*pushButton2.topWidget: "pushButton6"

*pushButton9.class: pushButton
*pushButton9.parent: workArea
*pushButton9.static: true
*pushButton9.name: pushButton9
*pushButton9.x: 100
*pushButton9.y: 370
*pushButton9.width: 90
*pushButton9.height: 30
*pushButton9.background: ButtonBackground
*pushButton9.labelString: "Extract Line"
*pushButton9.activateCallback: {\
\
}
*pushButton9.translations: MapUnmap
*pushButton9.createCallback: {\
\
}
*pushButton9.fontList: BoldTextFont
*pushButton9.foreground: ButtonForeground
*pushButton9.recomputeSize: "false"
*pushButton9.leftAttachment: "attach_widget"
*pushButton9.leftOffset: 10
*pushButton9.leftWidget: "pushButton6"
*pushButton9.bottomAttachment: "attach_none"
*pushButton9.bottomOffset: 5
*pushButton9.bottomWidget: ""
*pushButton9.topAttachment: "attach_widget"
*pushButton9.topOffset: 5
*pushButton9.topWidget: "SHelp"

*pushButton4.class: pushButton
*pushButton4.parent: workArea
*pushButton4.static: true
*pushButton4.name: pushButton4
*pushButton4.x: 390
*pushButton4.y: 460
*pushButton4.width: 90
*pushButton4.height: 30
*pushButton4.background: ButtonBackground
*pushButton4.labelString: "Modify LUT"
*pushButton4.activateCallback: {\
\
}
*pushButton4.translations: MapUnmap
*pushButton4.fontList: BoldTextFont
*pushButton4.foreground: ButtonForeground
*pushButton4.recomputeSize: "false"
*pushButton4.bottomAttachment: "attach_none"
*pushButton4.bottomOffset: 5
*pushButton4.bottomWidget: ""
*pushButton4.leftAttachment: "attach_widget"
*pushButton4.leftWidget: "pushButton2"
*pushButton4.leftOffset: 10
*pushButton4.topAttachment: "attach_widget"
*pushButton4.topOffset: 5
*pushButton4.topWidget: "pushButton9"

*pushButton7.class: pushButton
*pushButton7.parent: workArea
*pushButton7.static: true
*pushButton7.name: pushButton7
*pushButton7.x: 140
*pushButton7.y: 450
*pushButton7.width: 90
*pushButton7.height: 30
*pushButton7.background: ButtonBackground
*pushButton7.labelString: "Load Image"
*pushButton7.activateCallback: {\
\
}
*pushButton7.bottomAttachment: "attach_none"
*pushButton7.bottomOffset: 10
*pushButton7.leftAttachment: "attach_form"
*pushButton7.leftOffset: 10
*pushButton7.translations: MapUnmap
*pushButton7.topOffset: 5
*pushButton7.topWidget: "pushButton2"
*pushButton7.fontList: BoldTextFont
*pushButton7.foreground: ButtonForeground
*pushButton7.recomputeSize: "false"
*pushButton7.topAttachment: "attach_widget"

*pushButton3.class: pushButton
*pushButton3.parent: workArea
*pushButton3.static: true
*pushButton3.name: pushButton3
*pushButton3.x: 110
*pushButton3.y: 500
*pushButton3.width: 90
*pushButton3.height: 30
*pushButton3.background: ButtonBackground
*pushButton3.labelString: "Explore Ima"
*pushButton3.activateCallback: {\
\
}
*pushButton3.translations: MapUnmap
*pushButton3.createCallback: {\
\
}
*pushButton3.fontList: BoldTextFont
*pushButton3.foreground: ButtonForeground
*pushButton3.recomputeSize: "false"
*pushButton3.leftAttachment: "attach_widget"
*pushButton3.leftWidget: "pushButton7"
*pushButton3.leftOffset: 10
*pushButton3.bottomAttachment: "attach_none"
*pushButton3.bottomOffset: 10
*pushButton3.topAttachment: "attach_widget"
*pushButton3.topOffset: 5
*pushButton3.topWidget: "pushButton4"

*pushButton8.class: pushButton
*pushButton8.parent: workArea
*pushButton8.static: true
*pushButton8.name: pushButton8
*pushButton8.x: 220
*pushButton8.y: 500
*pushButton8.width: 90
*pushButton8.height: 30
*pushButton8.background: ButtonBackground
*pushButton8.labelString: "Create  D/G"
*pushButton8.activateCallback: {\
\
}
*pushButton8.bottomAttachment: "attach_none"
*pushButton8.bottomOffset: 10
*pushButton8.leftAttachment: "attach_widget"
*pushButton8.leftWidget: "pushButton3"
*pushButton8.leftOffset: 10
*pushButton8.translations: MapUnmap
*pushButton8.createCallback: {\
\
}
*pushButton8.fontList: BoldTextFont
*pushButton8.foreground: ButtonForeground
*pushButton8.recomputeSize: "false"
*pushButton8.topAttachment: "attach_widget"
*pushButton8.topOffset: 5
*pushButton8.topWidget: "SHelp"

*pushButton5.class: pushButton
*pushButton5.parent: workArea
*pushButton5.static: true
*pushButton5.name: pushButton5
*pushButton5.x: 210
*pushButton5.y: 380
*pushButton5.width: 90
*pushButton5.height: 30
*pushButton5.background: ButtonBackground
*pushButton5.labelString: "Quick Load"
*pushButton5.activateCallback: {\
\
}
*pushButton5.translations: MapUnmap
*pushButton5.fontList: BoldTextFont
*pushButton5.foreground: ButtonForeground
*pushButton5.recomputeSize: "false"
*pushButton5.leftAttachment: "attach_widget"
*pushButton5.leftOffset: 10
*pushButton5.leftWidget: "pushButton3"
*pushButton5.topAttachment: "attach_widget"
*pushButton5.topOffset: 5
*pushButton5.topWidget: "pushButton8"

*form5.class: form
*form5.parent: workArea
*form5.static: true
*form5.name: form5
*form5.resizePolicy: "resize_none"
*form5.x: 14
*form5.y: 14
*form5.width: 430
*form5.height: 240
*form5.background: ApplicBackground
*form5.borderWidth: 1
*form5.mappedWhenManaged: "false"
*form5.createCallback: {\
\
}
*form5.mapCallback: {\
\
}
*form5.noResize: "true"
*form5.topAttachment: "attach_form"
*form5.topOffset: 4
*form5.leftAttachment: "attach_form"
*form5.leftOffset: 4
*form5.rightAttachment: "attach_form"
*form5.rightOffset: 4

*label37.class: label
*label37.parent: form5
*label37.static: true
*label37.name: label37
*label37.x: 10
*label37.y: 10
*label37.width: 60
*label37.height: 25
*label37.labelString: "Frame"
*label37.foreground: TextForeground
*label37.background: LabelBackground
*label37.alignment: "alignment_center"
*label37.recomputeSize: "false"
*label37.translations: ""
*label37.fontList: BoldTextFont
*label37.leftAttachment: "attach_form"
*label37.leftOffset: 5
*label37.topAttachment: "attach_form"
*label37.topOffset: 10

*text28.class: text
*text28.parent: form5
*text28.static: true
*text28.name: text28
*text28.x: 10
*text28.y: 39
*text28.width: 250
*text28.height: 32
*text28.background: TextBackground
*text28.translations: SelectFile
*text28.foreground: TextForeground
*text28.fontList: TextFont
*text28.columns: 30
*text28.leftAttachment: "attach_widget"
*text28.leftOffset: 3
*text28.rightAttachment: "attach_form"
*text28.rightOffset: 70
*text28.topAttachment: "attach_form"
*text28.topOffset: 5
*text28.leftWidget: "label37"
*text28.marginHeight: 2
*text28.marginWidth: 2

*label41.class: label
*label41.parent: form5
*label41.static: true
*label41.name: label41
*label41.x: 10
*label41.y: 119
*label41.width: 60
*label41.height: 25
*label41.labelString: "ScaleX"
*label41.foreground: TextForeground
*label41.background: LabelBackground
*label41.alignment: "alignment_center"
*label41.recomputeSize: "false"
*label41.translations: ""
*label41.fontList: BoldTextFont
*label41.leftAttachment: "attach_form"
*label41.leftOffset: 5
*label41.topAttachment: "attach_form"
*label41.topOffset: 48

*rowColumn35.class: rowColumn
*rowColumn35.parent: form5
*rowColumn35.static: true
*rowColumn35.name: rowColumn35
*rowColumn35.x: 98
*rowColumn35.y: 55
*rowColumn35.width: 70
*rowColumn35.height: 40
*rowColumn35.orientation: "horizontal"
*rowColumn35.packing: "pack_none"
*rowColumn35.spacing: 0
*rowColumn35.resizeHeight: "false"
*rowColumn35.resizeWidth: "false"
*rowColumn35.marginHeight: 0
*rowColumn35.marginWidth: 0
*rowColumn35.numColumns: 2
*rowColumn35.background: ApplicBackground
*rowColumn35.leftAttachment: "attach_widget"
*rowColumn35.leftOffset: 2
*rowColumn35.leftWidget: "label41"
*rowColumn35.topAttachment: "attach_form"
*rowColumn35.topOffset: 38

*rowColumn36.class: rowColumn
*rowColumn36.parent: rowColumn35
*rowColumn36.static: true
*rowColumn36.name: rowColumn36
*rowColumn36.x: 44
*rowColumn36.y: 6
*rowColumn36.width: 23
*rowColumn36.height: 32
*rowColumn36.resizeHeight: "false"
*rowColumn36.resizeWidth: "false"
*rowColumn36.background: ApplicBackground
*rowColumn36.marginHeight: 0
*rowColumn36.marginWidth: 0
*rowColumn36.packing: "pack_none"
*rowColumn36.spacing: 0
*rowColumn36.adjustMargin: "true"
*rowColumn36.adjustLast: "true"
*rowColumn36.entryAlignment: "alignment_center"

*arrowButton35.class: arrowButton
*arrowButton35.parent: rowColumn36
*arrowButton35.static: true
*arrowButton35.name: arrowButton35
*arrowButton35.x: 0
*arrowButton35.y: 0
*arrowButton35.width: 23
*arrowButton35.height: 15
*arrowButton35.background: TextBackground
*arrowButton35.shadowThickness: 0
*arrowButton35.translations: Arrows5
*arrowButton35.foreground: "PaleGreen"
*arrowButton35.highlightThickness: 0

*arrowButton36.class: arrowButton
*arrowButton36.parent: rowColumn36
*arrowButton36.static: true
*arrowButton36.name: arrowButton36
*arrowButton36.x: 0
*arrowButton36.y: 16
*arrowButton36.width: 23
*arrowButton36.height: 15
*arrowButton36.background: TextBackground
*arrowButton36.shadowThickness: 0
*arrowButton36.arrowDirection: "arrow_down"
*arrowButton36.translations: Arrows5
*arrowButton36.foreground: "PaleGreen"
*arrowButton36.borderColor: ApplicBackground
*arrowButton36.highlightThickness: 0

*text31.class: text
*text31.parent: rowColumn35
*text31.static: true
*text31.name: text31
*text31.x: 0
*text31.y: 7
*text31.width: 40
*text31.height: 32
*text31.background: TextBackground
*text31.fontList: TextFont
*text31.foreground: TextForeground
*text31.highlightThickness: 2
*text31.shadowThickness: 2
*text31.marginHeight: 2
*text31.marginWidth: 2
*text31.maxLength: 100

*label45.class: label
*label45.parent: form5
*label45.static: true
*label45.name: label45
*label45.x: 10
*label45.y: 199
*label45.width: 60
*label45.height: 25
*label45.labelString: "HighCut"
*label45.foreground: TextForeground
*label45.background: LabelBackground
*label45.alignment: "alignment_center"
*label45.recomputeSize: "false"
*label45.translations: ""
*label45.fontList: BoldTextFont
*label45.leftAttachment: "attach_form"
*label45.leftOffset: 275
*label45.topAttachment: "attach_form"
*label45.topOffset: 83

*rowColumn40.class: rowColumn
*rowColumn40.parent: form5
*rowColumn40.static: true
*rowColumn40.name: rowColumn40
*rowColumn40.x: 98
*rowColumn40.y: 100
*rowColumn40.width: 105
*rowColumn40.height: 40
*rowColumn40.orientation: "horizontal"
*rowColumn40.packing: "pack_none"
*rowColumn40.spacing: 0
*rowColumn40.resizeHeight: "false"
*rowColumn40.resizeWidth: "false"
*rowColumn40.marginHeight: 0
*rowColumn40.marginWidth: 0
*rowColumn40.numColumns: 2
*rowColumn40.background: ApplicBackground
*rowColumn40.leftAttachment: "attach_widget"
*rowColumn40.leftOffset: 0
*rowColumn40.leftWidget: "label45"
*rowColumn40.topAttachment: "attach_form"
*rowColumn40.topOffset: 73

*text35.class: text
*text35.parent: rowColumn40
*text35.static: true
*text35.name: text35
*text35.x: 5
*text35.y: 7
*text35.width: 65
*text35.height: 32
*text35.background: TextBackground
*text35.activateCallback: {\
\
}
*text35.foreground: TextForeground
*text35.fontList: TextFont
*text35.maxLength: 240
*text35.columns: 8
*text35.marginHeight: 2
*text35.marginWidth: 1

*rowColumn41.class: rowColumn
*rowColumn41.parent: rowColumn40
*rowColumn41.static: true
*rowColumn41.name: rowColumn41
*rowColumn41.x: 72
*rowColumn41.y: 6
*rowColumn41.width: 23
*rowColumn41.height: 32
*rowColumn41.resizeHeight: "false"
*rowColumn41.resizeWidth: "false"
*rowColumn41.background: ApplicBackground
*rowColumn41.marginHeight: 0
*rowColumn41.marginWidth: 0
*rowColumn41.packing: "pack_none"
*rowColumn41.spacing: 0
*rowColumn41.adjustMargin: "true"
*rowColumn41.adjustLast: "true"
*rowColumn41.entryAlignment: "alignment_center"

*arrowButton43.class: arrowButton
*arrowButton43.parent: rowColumn41
*arrowButton43.static: true
*arrowButton43.name: arrowButton43
*arrowButton43.x: 0
*arrowButton43.y: 0
*arrowButton43.width: 23
*arrowButton43.height: 15
*arrowButton43.background: TextBackground
*arrowButton43.shadowThickness: 0
*arrowButton43.translations: Arrows5
*arrowButton43.foreground: "PaleGreen"
*arrowButton43.highlightThickness: 0

*arrowButton44.class: arrowButton
*arrowButton44.parent: rowColumn41
*arrowButton44.static: true
*arrowButton44.name: arrowButton44
*arrowButton44.x: 0
*arrowButton44.y: 16
*arrowButton44.width: 23
*arrowButton44.height: 15
*arrowButton44.background: TextBackground
*arrowButton44.shadowThickness: 0
*arrowButton44.arrowDirection: "arrow_down"
*arrowButton44.translations: Arrows5
*arrowButton44.foreground: "PaleGreen"
*arrowButton44.borderColor: ApplicBackground
*arrowButton44.highlightThickness: 0

*label46.class: label
*label46.parent: form5
*label46.static: true
*label46.name: label46
*label46.x: 20
*label46.y: 110
*label46.width: 100
*label46.height: 25
*label46.labelString: "Descr Values"
*label46.foreground: TextForeground
*label46.background: LabelBackground
*label46.alignment: "alignment_center"
*label46.recomputeSize: "false"
*label46.translations: ""
*label46.fontList: BoldTextFont
*label46.leftAttachment: "attach_form"
*label46.leftOffset: 5
*label46.topAttachment: "attach_form"
*label46.topOffset: 118

*toggleButton7.class: toggleButton
*toggleButton7.parent: form5
*toggleButton7.static: true
*toggleButton7.name: toggleButton7
*toggleButton7.x: 0
*toggleButton7.y: 0
*toggleButton7.width: 70
*toggleButton7.height: 28
*toggleButton7.labelString: "Yes"
*toggleButton7.indicatorType: "one_of_many"
*toggleButton7.background: TextBackground
*toggleButton7.fontList: BoldTextFont
*toggleButton7.foreground: TextForeground
*toggleButton7.recomputeSize: "false"
*toggleButton7.borderWidth: 1
*toggleButton7.set: "true"
*toggleButton7.indicatorOn: "true"
*toggleButton7.indicatorSize: 16
*toggleButton7.selectColor: "Palegreen"
*toggleButton7.valueChangedCallback: {\
 \
/*  `toggle.c'  the Value change callback for toggle Button  */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"No");\
   }\
\
}
*toggleButton7.topAttachment: "attach_form"
*toggleButton7.topOffset: 115
*toggleButton7.leftAttachment: "attach_widget"
*toggleButton7.leftOffset: 5
*toggleButton7.leftWidget: "label46"

*label47.class: label
*label47.parent: form5
*label47.static: true
*label47.name: label47
*label47.x: 15
*label47.y: 180
*label47.width: 62
*label47.height: 25
*label47.labelString: "CutDelta"
*label47.foreground: TextForeground
*label47.background: LabelBackground
*label47.alignment: "alignment_center"
*label47.recomputeSize: "false"
*label47.translations: ""
*label47.fontList: BoldTextFont
*label47.leftAttachment: "attach_form"
*label47.leftOffset: 275
*label47.topAttachment: "attach_form"
*label47.topOffset: 118

*text36.class: text
*text36.parent: form5
*text36.static: true
*text36.name: text36
*text36.x: 5
*text36.y: 7
*text36.width: 65
*text36.height: 30
*text36.background: TextBackground
*text36.activateCallback: {\
\
}
*text36.foreground: TextForeground
*text36.fontList: TextFont
*text36.maxLength: 240
*text36.columns: 8
*text36.marginHeight: 2
*text36.marginWidth: 1
*text36.leftAttachment: "attach_widget"
*text36.leftOffset: 3
*text36.leftWidget: "label47"
*text36.topAttachment: "attach_form"
*text36.topOffset: 115

*label48.class: label
*label48.parent: form5
*label48.static: true
*label48.name: label48
*label48.x: 10
*label48.y: 119
*label48.width: 65
*label48.height: 25
*label48.labelString: "MinMax"
*label48.foreground: TextForeground
*label48.background: LabelBackground
*label48.alignment: "alignment_center"
*label48.recomputeSize: "false"
*label48.translations: ""
*label48.fontList: BoldTextFont
*label48.leftAttachment: "attach_form"
*label48.leftOffset: 5
*label48.topAttachment: "attach_form"
*label48.topOffset: 83

*rowColumn42.class: rowColumn
*rowColumn42.parent: form5
*rowColumn42.static: true
*rowColumn42.name: rowColumn42
*rowColumn42.x: 98
*rowColumn42.y: 100
*rowColumn42.width: 170
*rowColumn42.height: 32
*rowColumn42.orientation: "horizontal"
*rowColumn42.packing: "pack_none"
*rowColumn42.spacing: 0
*rowColumn42.resizeHeight: "false"
*rowColumn42.resizeWidth: "false"
*rowColumn42.marginHeight: 0
*rowColumn42.marginWidth: 0
*rowColumn42.numColumns: 2
*rowColumn42.background: ApplicBackground
*rowColumn42.leftAttachment: "attach_widget"
*rowColumn42.leftOffset: 0
*rowColumn42.leftWidget: "label48"
*rowColumn42.topAttachment: "attach_form"
*rowColumn42.topOffset: 80

*text37.class: text
*text37.parent: rowColumn42
*text37.static: true
*text37.name: text37
*text37.x: 0
*text37.y: 2
*text37.width: 60
*text37.height: 32
*text37.background: SHelpBackground
*text37.activateCallback: {\
\
}
*text37.foreground: TextForeground
*text37.fontList: TextFont
*text37.maxLength: 40
*text37.columns: 8
*text37.marginHeight: 2
*text37.marginWidth: 1
*text37.editable: "false"
*text37.highlightThickness: 0
*text37.cursorPositionVisible: "false"

*text38.class: text
*text38.parent: rowColumn42
*text38.static: true
*text38.name: text38
*text38.x: 85
*text38.y: 2
*text38.width: 60
*text38.height: 32
*text38.background: SHelpBackground
*text38.activateCallback: {\
\
}
*text38.foreground: TextForeground
*text38.fontList: TextFont
*text38.maxLength: 40
*text38.columns: 8
*text38.marginHeight: 2
*text38.marginWidth: 1
*text38.editable: "false"
*text38.highlightThickness: 0
*text38.cursorPositionVisible: "false"

*label38.class: label
*label38.parent: form5
*label38.static: true
*label38.name: label38
*label38.x: 10
*label38.y: 199
*label38.width: 60
*label38.height: 25
*label38.labelString: "LowCut"
*label38.foreground: TextForeground
*label38.background: LabelBackground
*label38.alignment: "alignment_center"
*label38.recomputeSize: "false"
*label38.translations: ""
*label38.fontList: BoldTextFont
*label38.leftAttachment: "attach_form"
*label38.leftOffset: 275
*label38.topAttachment: "attach_form"
*label38.topOffset: 48

*rowColumn38.class: rowColumn
*rowColumn38.parent: form5
*rowColumn38.static: true
*rowColumn38.name: rowColumn38
*rowColumn38.x: 98
*rowColumn38.y: 100
*rowColumn38.width: 105
*rowColumn38.height: 40
*rowColumn38.orientation: "horizontal"
*rowColumn38.packing: "pack_none"
*rowColumn38.spacing: 0
*rowColumn38.resizeHeight: "false"
*rowColumn38.resizeWidth: "false"
*rowColumn38.marginHeight: 0
*rowColumn38.marginWidth: 0
*rowColumn38.numColumns: 2
*rowColumn38.background: ApplicBackground
*rowColumn38.leftAttachment: "attach_widget"
*rowColumn38.leftOffset: 0
*rowColumn38.leftWidget: "label38"
*rowColumn38.topAttachment: "attach_form"
*rowColumn38.topOffset: 38

*text34.class: text
*text34.parent: rowColumn38
*text34.static: true
*text34.name: text34
*text34.x: 5
*text34.y: 7
*text34.width: 65
*text34.height: 32
*text34.background: TextBackground
*text34.activateCallback: {\
\
}
*text34.foreground: TextForeground
*text34.fontList: TextFont
*text34.maxLength: 240
*text34.columns: 8
*text34.marginHeight: 2
*text34.marginWidth: 1

*rowColumn39.class: rowColumn
*rowColumn39.parent: rowColumn38
*rowColumn39.static: true
*rowColumn39.name: rowColumn39
*rowColumn39.x: 72
*rowColumn39.y: 4
*rowColumn39.width: 23
*rowColumn39.height: 32
*rowColumn39.resizeHeight: "false"
*rowColumn39.resizeWidth: "false"
*rowColumn39.background: ApplicBackground
*rowColumn39.marginHeight: 0
*rowColumn39.marginWidth: 0
*rowColumn39.packing: "pack_none"
*rowColumn39.spacing: 0
*rowColumn39.adjustMargin: "true"
*rowColumn39.adjustLast: "true"
*rowColumn39.entryAlignment: "alignment_center"

*arrowButton41.class: arrowButton
*arrowButton41.parent: rowColumn39
*arrowButton41.static: true
*arrowButton41.name: arrowButton41
*arrowButton41.x: 0
*arrowButton41.y: 0
*arrowButton41.width: 23
*arrowButton41.height: 15
*arrowButton41.background: TextBackground
*arrowButton41.shadowThickness: 0
*arrowButton41.translations: Arrows5
*arrowButton41.foreground: "PaleGreen"
*arrowButton41.highlightThickness: 0

*arrowButton42.class: arrowButton
*arrowButton42.parent: rowColumn39
*arrowButton42.static: true
*arrowButton42.name: arrowButton42
*arrowButton42.x: 0
*arrowButton42.y: 16
*arrowButton42.width: 23
*arrowButton42.height: 15
*arrowButton42.background: TextBackground
*arrowButton42.shadowThickness: 0
*arrowButton42.arrowDirection: "arrow_down"
*arrowButton42.translations: Arrows5
*arrowButton42.foreground: "PaleGreen"
*arrowButton42.borderColor: ApplicBackground
*arrowButton42.highlightThickness: 0

*label42.class: label
*label42.parent: form5
*label42.static: true
*label42.name: label42
*label42.x: 10
*label42.y: 10
*label42.width: 70
*label42.height: 25
*label42.labelString: "LUT"
*label42.foreground: TextForeground
*label42.background: LabelBackground
*label42.alignment: "alignment_center"
*label42.recomputeSize: "false"
*label42.translations: ""
*label42.fontList: BoldTextFont
*label42.topAttachment: "attach_form"
*label42.topOffset: 165
*label42.leftAttachment: "attach_form"
*label42.leftOffset: 5

*text29.class: text
*text29.parent: form5
*text29.static: true
*text29.name: text29
*text29.x: 10
*text29.y: 39
*text29.width: 200
*text29.height: 32
*text29.background: TextBackground
*text29.translations: SelectFile
*text29.foreground: TextForeground
*text29.fontList: TextFont
*text29.marginHeight: 2
*text29.marginWidth: 2
*text29.leftAttachment: "attach_widget"
*text29.leftOffset: 5
*text29.leftWidget: "label42"
*text29.topAttachment: "attach_form"
*text29.topOffset: 160
*text29.rightAttachment: "attach_none"
*text29.rightOffset: 60

*label43.class: label
*label43.parent: form5
*label43.static: true
*label43.name: label43
*label43.x: 20
*label43.y: 110
*label43.width: 60
*label43.height: 20
*label43.labelString: "Create"
*label43.foreground: TextForeground
*label43.background: LabelBackground
*label43.alignment: "alignment_center"
*label43.recomputeSize: "false"
*label43.translations: ""
*label43.fontList: BoldTextFont
*label43.leftAttachment: "attach_form"
*label43.leftOffset: 5
*label43.topAttachment: "attach_form"
*label43.topOffset: 203
*label43.marginHeight: 0

*toggleButton13.class: toggleButton
*toggleButton13.parent: form5
*toggleButton13.static: true
*toggleButton13.name: toggleButton13
*toggleButton13.x: 0
*toggleButton13.y: 0
*toggleButton13.width: 70
*toggleButton13.height: 28
*toggleButton13.labelString: "Yes"
*toggleButton13.indicatorType: "one_of_many"
*toggleButton13.background: TextBackground
*toggleButton13.fontList: BoldTextFont
*toggleButton13.foreground: TextForeground
*toggleButton13.recomputeSize: "false"
*toggleButton13.borderWidth: 1
*toggleButton13.set: "true"
*toggleButton13.indicatorOn: "true"
*toggleButton13.indicatorSize: 16
*toggleButton13.selectColor: "Palegreen"
*toggleButton13.valueChangedCallback: {\
 \
/*  `toggle5b.c'  the Value change callback for \
    toggle Button:  Create Display                 */\
\
\
char  cbuf[8];\
\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
   {\
   UxPutLabelString(UxThisWidget,"Yes");\
\
   XtSetSensitive(UxGetWidget(label44),TRUE);\
   XtSetSensitive(UxGetWidget(label49),TRUE);\
\
   XtSetSensitive(UxGetWidget(rowColumn23),TRUE);\
   XtSetSensitive(UxGetWidget(rowColumn25),TRUE);\
   }\
else\
   {\
   UxPutLabelString(UxThisWidget,"No");\
\
   XtSetSensitive(UxGetWidget(label44),FALSE);\
   XtSetSensitive(UxGetWidget(label49),FALSE);\
\
   XtSetSensitive(UxGetWidget(rowColumn23),FALSE);\
   XtSetSensitive(UxGetWidget(rowColumn25),FALSE);\
   }\
\
}
*toggleButton13.leftAttachment: "attach_widget"
*toggleButton13.leftOffset: 5
*toggleButton13.leftWidget: "label43"
*toggleButton13.topAttachment: "attach_form"
*toggleButton13.topOffset: 205
*toggleButton13.topPosition: 0
*toggleButton13.marginLeft: 1
*toggleButton13.marginRight: 2
*toggleButton13.alignment: "alignment_center"

*label44.class: label
*label44.parent: form5
*label44.static: true
*label44.name: label44
*label44.x: 9
*label44.y: 49
*label44.width: 45
*label44.height: 25
*label44.labelString: "SizeX"
*label44.foreground: TextForeground
*label44.background: LabelBackground
*label44.alignment: "alignment_center"
*label44.recomputeSize: "false"
*label44.translations: ""
*label44.fontList: BoldTextFont
*label44.leftAttachment: "attach_form"
*label44.leftOffset: 155
*label44.topAttachment: "attach_form"
*label44.topOffset: 208

*rowColumn23.class: rowColumn
*rowColumn23.parent: form5
*rowColumn23.static: true
*rowColumn23.name: rowColumn23
*rowColumn23.x: 114
*rowColumn23.y: 15
*rowColumn23.width: 80
*rowColumn23.height: 44
*rowColumn23.orientation: "horizontal"
*rowColumn23.packing: "pack_none"
*rowColumn23.spacing: 0
*rowColumn23.resizeHeight: "false"
*rowColumn23.resizeWidth: "false"
*rowColumn23.marginHeight: 0
*rowColumn23.marginWidth: 0
*rowColumn23.numColumns: 2
*rowColumn23.background: ApplicBackground
*rowColumn23.leftAttachment: "attach_widget"
*rowColumn23.leftOffset: 0
*rowColumn23.leftWidget: "label44"
*rowColumn23.topAttachment: "attach_form"
*rowColumn23.topOffset: 200

*rowColumn24.class: rowColumn
*rowColumn24.parent: rowColumn23
*rowColumn24.static: true
*rowColumn24.name: rowColumn24
*rowColumn24.x: 57
*rowColumn24.y: 6
*rowColumn24.width: 23
*rowColumn24.height: 32
*rowColumn24.resizeHeight: "false"
*rowColumn24.resizeWidth: "false"
*rowColumn24.background: ApplicBackground
*rowColumn24.marginHeight: 0
*rowColumn24.marginWidth: 0
*rowColumn24.packing: "pack_none"
*rowColumn24.spacing: 0
*rowColumn24.adjustMargin: "true"
*rowColumn24.adjustLast: "true"
*rowColumn24.entryAlignment: "alignment_center"

*arrowButton29.class: arrowButton
*arrowButton29.parent: rowColumn24
*arrowButton29.static: true
*arrowButton29.name: arrowButton29
*arrowButton29.x: 0
*arrowButton29.y: 0
*arrowButton29.width: 23
*arrowButton29.height: 15
*arrowButton29.background: TextBackground
*arrowButton29.shadowThickness: 0
*arrowButton29.foreground: "PaleGreen"
*arrowButton29.highlightThickness: 0
*arrowButton29.translations: Arrows5

*arrowButton30.class: arrowButton
*arrowButton30.parent: rowColumn24
*arrowButton30.static: true
*arrowButton30.name: arrowButton30
*arrowButton30.x: 0
*arrowButton30.y: 16
*arrowButton30.width: 23
*arrowButton30.height: 15
*arrowButton30.background: TextBackground
*arrowButton30.shadowThickness: 0
*arrowButton30.arrowDirection: "arrow_down"
*arrowButton30.foreground: "PaleGreen"
*arrowButton30.borderColor: ApplicBackground
*arrowButton30.highlightThickness: 0
*arrowButton30.translations: Arrows5

*text30.class: text
*text30.parent: rowColumn23
*text30.static: true
*text30.name: text30
*text30.y: 7
*text30.width: 50
*text30.height: 32
*text30.background: TextBackground
*text30.fontList: TextFont
*text30.foreground: TextForeground
*text30.highlightThickness: 2
*text30.shadowThickness: 2
*text30.marginHeight: 2
*text30.marginWidth: 2
*text30.maxLength: 100
*text30.x: 1

*label49.class: label
*label49.parent: form5
*label49.static: true
*label49.name: label49
*label49.x: 209
*label49.y: 59
*label49.width: 50
*label49.height: 25
*label49.labelString: "SizeY"
*label49.foreground: TextForeground
*label49.background: LabelBackground
*label49.alignment: "alignment_center"
*label49.recomputeSize: "false"
*label49.translations: ""
*label49.fontList: BoldTextFont
*label49.leftAttachment: "attach_form"
*label49.leftOffset: 290
*label49.topAttachment: "attach_form"
*label49.topOffset: 208

*rowColumn25.class: rowColumn
*rowColumn25.parent: form5
*rowColumn25.static: true
*rowColumn25.name: rowColumn25
*rowColumn25.x: 112
*rowColumn25.y: 60
*rowColumn25.width: 80
*rowColumn25.height: 44
*rowColumn25.orientation: "horizontal"
*rowColumn25.packing: "pack_none"
*rowColumn25.spacing: 0
*rowColumn25.resizeHeight: "false"
*rowColumn25.resizeWidth: "false"
*rowColumn25.marginHeight: 0
*rowColumn25.marginWidth: 0
*rowColumn25.numColumns: 2
*rowColumn25.background: ApplicBackground
*rowColumn25.leftAttachment: "attach_widget"
*rowColumn25.leftOffset: 0
*rowColumn25.leftWidget: "label49"
*rowColumn25.topAttachment: "attach_form"
*rowColumn25.topOffset: 200

*rowColumn26.class: rowColumn
*rowColumn26.parent: rowColumn25
*rowColumn26.static: true
*rowColumn26.name: rowColumn26
*rowColumn26.x: 57
*rowColumn26.y: 6
*rowColumn26.width: 23
*rowColumn26.height: 32
*rowColumn26.resizeHeight: "false"
*rowColumn26.resizeWidth: "false"
*rowColumn26.background: ApplicBackground
*rowColumn26.marginHeight: 0
*rowColumn26.marginWidth: 0
*rowColumn26.packing: "pack_none"
*rowColumn26.spacing: 0
*rowColumn26.adjustMargin: "true"
*rowColumn26.adjustLast: "true"
*rowColumn26.entryAlignment: "alignment_center"

*arrowButton33.class: arrowButton
*arrowButton33.parent: rowColumn26
*arrowButton33.static: true
*arrowButton33.name: arrowButton33
*arrowButton33.x: 0
*arrowButton33.y: 0
*arrowButton33.width: 23
*arrowButton33.height: 15
*arrowButton33.background: TextBackground
*arrowButton33.shadowThickness: 0
*arrowButton33.foreground: "PaleGreen"
*arrowButton33.highlightThickness: 0
*arrowButton33.translations: Arrows5

*arrowButton34.class: arrowButton
*arrowButton34.parent: rowColumn26
*arrowButton34.static: true
*arrowButton34.name: arrowButton34
*arrowButton34.x: 0
*arrowButton34.y: 16
*arrowButton34.width: 23
*arrowButton34.height: 15
*arrowButton34.background: TextBackground
*arrowButton34.shadowThickness: 0
*arrowButton34.arrowDirection: "arrow_down"
*arrowButton34.foreground: "PaleGreen"
*arrowButton34.borderColor: ApplicBackground
*arrowButton34.highlightThickness: 0
*arrowButton34.translations: Arrows5

*text32.class: text
*text32.parent: rowColumn25
*text32.static: true
*text32.name: text32
*text32.x: 1
*text32.y: 7
*text32.width: 50
*text32.height: 32
*text32.background: TextBackground
*text32.fontList: TextFont
*text32.foreground: TextForeground
*text32.highlightThickness: 2
*text32.shadowThickness: 2
*text32.marginHeight: 2
*text32.marginWidth: 2
*text32.maxLength: 100

*label50.class: label
*label50.parent: form5
*label50.static: true
*label50.name: label50
*label50.x: 15
*label50.y: 218
*label50.width: 60
*label50.height: 20
*label50.labelString: "Display"
*label50.foreground: TextForeground
*label50.background: LabelBackground
*label50.alignment: "alignment_center"
*label50.recomputeSize: "false"
*label50.translations: ""
*label50.fontList: BoldTextFont
*label50.marginHeight: 0
*label50.leftAttachment: "attach_form"
*label50.leftOffset: 5
*label50.topAttachment: "attach_form"
*label50.topOffset: 220

*label51.class: label
*label51.parent: form5
*label51.static: true
*label51.name: label51
*label51.x: 10
*label51.y: 119
*label51.width: 55
*label51.height: 25
*label51.labelString: " ScaleY"
*label51.foreground: TextForeground
*label51.background: LabelBackground
*label51.alignment: "alignment_center"
*label51.recomputeSize: "false"
*label51.translations: ""
*label51.fontList: BoldTextFont
*label51.leftAttachment: "attach_form"
*label51.leftOffset: 135
*label51.topAttachment: "attach_form"
*label51.topOffset: 48

*rowColumn37.class: rowColumn
*rowColumn37.parent: form5
*rowColumn37.static: true
*rowColumn37.name: rowColumn37
*rowColumn37.x: 98
*rowColumn37.y: 55
*rowColumn37.width: 70
*rowColumn37.height: 40
*rowColumn37.orientation: "horizontal"
*rowColumn37.packing: "pack_none"
*rowColumn37.spacing: 0
*rowColumn37.resizeHeight: "false"
*rowColumn37.resizeWidth: "false"
*rowColumn37.marginHeight: 0
*rowColumn37.marginWidth: 0
*rowColumn37.numColumns: 2
*rowColumn37.background: ApplicBackground
*rowColumn37.leftAttachment: "attach_widget"
*rowColumn37.leftOffset: 2
*rowColumn37.leftWidget: "label51"
*rowColumn37.topAttachment: "attach_form"
*rowColumn37.topOffset: 38

*rowColumn43.class: rowColumn
*rowColumn43.parent: rowColumn37
*rowColumn43.static: true
*rowColumn43.name: rowColumn43
*rowColumn43.x: 44
*rowColumn43.y: 6
*rowColumn43.width: 23
*rowColumn43.height: 32
*rowColumn43.resizeHeight: "false"
*rowColumn43.resizeWidth: "false"
*rowColumn43.background: ApplicBackground
*rowColumn43.marginHeight: 0
*rowColumn43.marginWidth: 0
*rowColumn43.packing: "pack_none"
*rowColumn43.spacing: 0
*rowColumn43.adjustMargin: "true"
*rowColumn43.adjustLast: "true"
*rowColumn43.entryAlignment: "alignment_center"

*arrowButton37.class: arrowButton
*arrowButton37.parent: rowColumn43
*arrowButton37.static: true
*arrowButton37.name: arrowButton37
*arrowButton37.x: 0
*arrowButton37.y: 0
*arrowButton37.width: 23
*arrowButton37.height: 15
*arrowButton37.background: TextBackground
*arrowButton37.shadowThickness: 0
*arrowButton37.translations: Arrows5
*arrowButton37.foreground: "PaleGreen"
*arrowButton37.highlightThickness: 0

*arrowButton38.class: arrowButton
*arrowButton38.parent: rowColumn43
*arrowButton38.static: true
*arrowButton38.name: arrowButton38
*arrowButton38.x: 0
*arrowButton38.y: 16
*arrowButton38.width: 23
*arrowButton38.height: 15
*arrowButton38.background: TextBackground
*arrowButton38.shadowThickness: 0
*arrowButton38.arrowDirection: "arrow_down"
*arrowButton38.translations: Arrows5
*arrowButton38.foreground: "PaleGreen"
*arrowButton38.borderColor: ApplicBackground
*arrowButton38.highlightThickness: 0

*text33.class: text
*text33.parent: rowColumn37
*text33.static: true
*text33.name: text33
*text33.x: 0
*text33.y: 7
*text33.width: 40
*text33.height: 32
*text33.background: TextBackground
*text33.fontList: TextFont
*text33.foreground: TextForeground
*text33.highlightThickness: 2
*text33.shadowThickness: 2
*text33.marginHeight: 2
*text33.marginWidth: 2
*text33.maxLength: 100

*XDMenus.class: rowColumn
*XDMenus.parent: mainWindow
*XDMenus.static: true
*XDMenus.name: XDMenus
*XDMenus.borderWidth: 0
*XDMenus.menuHelpWidget: "quitCascade"
*XDMenus.rowColumnType: "menu_bar"
*XDMenus.menuAccelerator: "<KeyUp>F10"
*XDMenus.background: MenuBackground
*XDMenus.foreground: MenuForeground

*commandPane.class: rowColumn
*commandPane.parent: XDMenus
*commandPane.static: true
*commandPane.name: commandPane
*commandPane.rowColumnType: "menu_pulldown"
*commandPane.background: MenuBackground
*commandPane.foreground: MenuForeground

*command_b2.class: pushButtonGadget
*command_b2.parent: commandPane
*command_b2.static: true
*command_b2.name: command_b2
*command_b2.labelString: "Clear Channel"
*command_b2.activateCallback: {\
\
/* for `ClearChannel' button (CLEAR/CHAN) */\
\
applycom(7);\
\
}
*command_b2.fontList: BoldTextFont

*command_b3.class: pushButtonGadget
*command_b3.parent: commandPane
*command_b3.static: true
*command_b3.name: command_b3
*command_b3.labelString: "Clear Display"
*command_b3.activateCallback: {\
\
/* `ClearDisplay' button (CLEAR/DISPLAY) */\
\
applycom(6);\
\
}
*command_b3.fontList: BoldTextFont

*commandPane_b17.class: pushButtonGadget
*commandPane_b17.parent: commandPane
*commandPane_b17.static: true
*commandPane_b17.name: commandPane_b17
*commandPane_b17.labelString: "Clear Overlay"
*commandPane_b17.activateCallback: {applycom(1);}
*commandPane_b17.fontList: BoldTextFont

*command_b9.class: pushButtonGadget
*command_b9.parent: commandPane
*command_b9.static: true
*command_b9.name: command_b9
*command_b9.labelString: "Disable Overlay"
*command_b9.activateCallback: {\
\
/*  `DisableOverlay'  button  (CLEAR/OVER)  */\
\
applycom(9);\
\
}
*command_b9.fontList: BoldTextFont

*command_b10.class: pushButtonGadget
*command_b10.parent: commandPane
*command_b10.static: true
*command_b10.name: command_b10
*command_b10.labelString: "Enable Overlay"
*command_b10.activateCallback: {\
\
/*  `EnableOverlay'  button  (SET/OVER)  */\
\
applycom(10);\
\
}\
\

*command_b10.fontList: BoldTextFont

*commandPane_b14.class: pushButtonGadget
*commandPane_b14.parent: commandPane
*commandPane_b14.static: true
*commandPane_b14.name: commandPane_b14
*commandPane_b14.labelString: "Delete Display"
*commandPane_b14.activateCallback: {applycom(13);}
*commandPane_b14.fontList: BoldTextFont

*commandPane_b15.class: pushButtonGadget
*commandPane_b15.parent: commandPane
*commandPane_b15.static: true
*commandPane_b15.name: commandPane_b15
*commandPane_b15.labelString: "Delete Graphics"
*commandPane_b15.activateCallback: {applycom(14);}
*commandPane_b15.fontList: BoldTextFont

*commandPane_b16.class: pushButtonGadget
*commandPane_b16.parent: commandPane
*commandPane_b16.static: true
*commandPane_b16.name: commandPane_b16
*commandPane_b16.labelString: "Reset Display"
*commandPane_b16.activateCallback: {applycom(8);}
*commandPane_b16.fontList: BoldTextFont

*LUTCommsPane.class: rowColumn
*LUTCommsPane.parent: XDMenus
*LUTCommsPane.static: true
*LUTCommsPane.name: LUTCommsPane
*LUTCommsPane.rowColumnType: "menu_pulldown"
*LUTCommsPane.foreground: MenuForeground
*LUTCommsPane.background: MenuBackground

*LUTcomm_b1.class: pushButtonGadget
*LUTcomm_b1.parent: LUTCommsPane
*LUTcomm_b1.static: true
*LUTcomm_b1.name: LUTcomm_b1
*LUTcomm_b1.labelString: "LUT Off"
*LUTcomm_b1.activateCallback: {\
\
/* `LUT_Off' button (CLEAR/LUT)   */\
\
applycom(5);\
\
}\
\

*LUTcomm_b1.fontList: BoldTextFont

*LUTcomm_b2.class: pushButtonGadget
*LUTcomm_b2.parent: LUTCommsPane
*LUTcomm_b2.static: true
*LUTcomm_b2.name: LUTcomm_b2
*LUTcomm_b2.labelString: "LUT On"
*LUTcomm_b2.activateCallback: {\
\
/* `LUT_On' button (SET/LUT)  */\
\
applycom(4);\
\
}
*LUTcomm_b2.fontList: BoldTextFont

*LUTcomm_b3.class: pushButtonGadget
*LUTcomm_b3.parent: LUTCommsPane
*LUTcomm_b3.static: true
*LUTcomm_b3.name: LUTcomm_b3
*LUTcomm_b3.labelString: "LUT Display On"
*LUTcomm_b3.activateCallback: {\
\
/* `LUT Display On' button (DISPLAY/LUT)  */\
\
applycom(11);\
\
}
*LUTcomm_b3.fontList: BoldTextFont

*LUTcomm_b4.class: pushButtonGadget
*LUTcomm_b4.parent: LUTCommsPane
*LUTcomm_b4.static: true
*LUTcomm_b4.name: LUTcomm_b4
*LUTcomm_b4.labelString: "LUT Display Off"
*LUTcomm_b4.activateCallback: {\
\
/* `LUT Display Offn' button (DISPLAY/LUT OFF)  */\
\
applycom(12);\
\
}
*LUTcomm_b4.fontList: BoldTextFont

*LUTcomm_b5.class: pushButtonGadget
*LUTcomm_b5.parent: LUTCommsPane
*LUTcomm_b5.static: true
*LUTcomm_b5.name: LUTcomm_b5
*LUTcomm_b5.labelString: "ITT Off"
*LUTcomm_b5.activateCallback: {\
\
/* `ITT_Off'  button (CLEAR/ITT) */\
\
applycom(2);\
\
}
*LUTcomm_b5.fontList: BoldTextFont

*LUTcomm_b6.class: pushButtonGadget
*LUTcomm_b6.parent: LUTCommsPane
*LUTcomm_b6.static: true
*LUTcomm_b6.name: LUTcomm_b6
*LUTcomm_b6.labelString: "ITT On"
*LUTcomm_b6.activateCallback: {\
\
/* `ITT_On' button (SET/ITT)  */\
\
applycom(3);\
\
}
*LUTcomm_b6.fontList: BoldTextFont

*helpPane.class: rowColumn
*helpPane.parent: XDMenus
*helpPane.static: true
*helpPane.name: helpPane
*helpPane.rowColumnType: "menu_pulldown"
*helpPane.background: MenuBackground
*helpPane.foreground: MenuForeground

*XDHelp.class: pushButtonGadget
*XDHelp.parent: helpPane
*XDHelp.static: true
*XDHelp.name: XDHelp
*XDHelp.labelString: "On XDisplay..."
*XDHelp.mnemonic: "H"
*XDHelp.activateCallback: {XD_Help(0);}
*XDHelp.fontList: BoldTextFont

*InterfHelp.class: pushButtonGadget
*InterfHelp.parent: helpPane
*InterfHelp.static: true
*InterfHelp.name: InterfHelp
*InterfHelp.labelString: "On this interface..."
*InterfHelp.mnemonic: "H"
*InterfHelp.activateCallback: {XD_Help(1);}
*InterfHelp.fontList: BoldTextFont

*EditHelp.class: pushButtonGadget
*EditHelp.parent: helpPane
*EditHelp.static: true
*EditHelp.name: EditHelp
*EditHelp.labelString: "On editing..."
*EditHelp.mnemonic: "K"
*EditHelp.activateCallback: {XD_Help(2);}
*EditHelp.fontList: BoldTextFont

*SelectHelp.class: pushButtonGadget
*SelectHelp.parent: helpPane
*SelectHelp.static: true
*SelectHelp.name: SelectHelp
*SelectHelp.labelString: "On selecting files..."
*SelectHelp.activateCallback: {XD_Help(3);}
*SelectHelp.fontList: BoldTextFont

*quitPane.class: rowColumn
*quitPane.parent: XDMenus
*quitPane.static: true
*quitPane.name: quitPane
*quitPane.rowColumnType: "menu_pulldown"
*quitPane.background: MenuBackground
*quitPane.foreground: MenuForeground

*quit_b1.class: pushButtonGadget
*quit_b1.parent: quitPane
*quit_b1.static: true
*quit_b1.name: quit_b1
*quit_b1.labelString: "Bye"
*quit_b1.activateCallback: {exit(0);}
*quit_b1.fontList: BoldTextFont

*commandsCascade.class: cascadeButton
*commandsCascade.parent: XDMenus
*commandsCascade.static: true
*commandsCascade.name: commandsCascade
*commandsCascade.labelString: "Commands"
*commandsCascade.subMenuId: "commandPane"
*commandsCascade.background: MenuBackground
*commandsCascade.fontList: BoldTextFont
*commandsCascade.foreground: MenuForeground
*commandsCascade.mnemonic: "C"
*commandsCascade.x: 101

*LUTCommsCascade.class: cascadeButton
*LUTCommsCascade.parent: XDMenus
*LUTCommsCascade.static: true
*LUTCommsCascade.name: LUTCommsCascade
*LUTCommsCascade.labelString: "LUTcmnds"
*LUTCommsCascade.mnemonic: "L"
*LUTCommsCascade.subMenuId: "LUTCommsPane"
*LUTCommsCascade.fontList: BoldTextFont
*LUTCommsCascade.foreground: MenuForeground
*LUTCommsCascade.background: MenuBackground

*helpCascade.class: cascadeButton
*helpCascade.parent: XDMenus
*helpCascade.static: true
*helpCascade.name: helpCascade
*helpCascade.labelString: "Help"
*helpCascade.mnemonic: "H"
*helpCascade.subMenuId: "helpPane"
*helpCascade.fontList: BoldTextFont
*helpCascade.foreground: MenuForeground
*helpCascade.background: MenuBackground

*quitCascade.class: cascadeButton
*quitCascade.parent: XDMenus
*quitCascade.static: true
*quitCascade.name: quitCascade
*quitCascade.labelString: "Quit"
*quitCascade.mnemonic: "Q"
*quitCascade.subMenuId: "quitPane"
*quitCascade.fontList: BoldTextFont
*quitCascade.foreground: MenuForeground
*quitCascade.x: 300
*quitCascade.background: MenuBackground

